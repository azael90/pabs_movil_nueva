package com.jaguarlabs.pabs.components;

import android.content.Context;
import android.content.SharedPreferences;

import com.jaguarlabs.pabs.MyWallet.WalletSyncedDate;
import com.jaguarlabs.pabs.util.Util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Class in charge of managing days when working with
 * wallet synchronization
 *
 * @author Jordan Alvarez
 * @version 03/09/15
 */
public class PreferencesToShowWallet {

    public static final String PREFERENCES_FILE = "MY_WALLET_FILE";
    public static final String KEY_DAY_COUNTER = "DAY_COUNTER";
    public static final String KEY_DAY_OF_YEAR = "DAY_OF_YEAR";
    public static final String KEY_DAY_OF_WEEK = "DAY_OF_WEEK";
    public static final String KEY_DAY_OF_MONTH = "DAY_OF_MONTH";
    public static final String KEY_DAY_MAXIMUM_MONTH = "DAY_MAXIMUM_MONTH";
    public static final String KEY_YEAR = "YEAR";
    public static final String KEY_RESET_CONTRACTS = "RESET_CONTRACTS";
    public static final String KEY_RESET_CONTRACTS_PER_WEEK = "RESET_CONTRACTS_PER_WEEK";

    private Context context;

    public PreferencesToShowWallet(Context context){
        this.context = context;
    }

    /**
     * gets sharedpreferences file
     * @return
     *      SharedPreferences file
     */
    private SharedPreferences getSharedPreferences(){
        return context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
    }

    /**
     * gets SharedPreferences editor
     * @return
     *      SharedPreferences editor
     */
    private SharedPreferences.Editor getSharedPreferencesEditor(){
        return getSharedPreferences().edit();
    }

    public void saveDayCounter(){
        //Calendar calendar = Calendar.getInstance();
        //if (calendar.get(Calendar.DAY_OF_YEAR) != getDayOfYear())
        getSharedPreferencesEditor()
                .putLong(
                        KEY_DAY_COUNTER,
                        getSharedPreferences().getLong(KEY_DAY_COUNTER, 1) + 1
                ).apply();
    }

    public long getDayCounter(){
        return getSharedPreferences().getLong(KEY_DAY_COUNTER, 1);
    }

    public void saveDayOfYear(int dayOfYear){
        getSharedPreferencesEditor().putInt(KEY_DAY_OF_YEAR, dayOfYear).apply();
    }

    public void saveYear(int year){
        getSharedPreferencesEditor().putInt(KEY_YEAR, year).apply();
    }

    public int getYear(){
        return getSharedPreferences().getInt(KEY_YEAR, 0);
    }

    public int getDayOfYear(){
        return getSharedPreferences().getInt(KEY_DAY_OF_YEAR, 0);
    }

    public void saveDayOfWeek(int dayOfYear){
        getSharedPreferencesEditor().putInt(KEY_DAY_OF_WEEK, dayOfYear).apply();
    }

    public int getDayOfWeek(){
        return getSharedPreferences().getInt(KEY_DAY_OF_WEEK, 0);
    }

    public void saveDayOfMonth(int dayOfYear){
        getSharedPreferencesEditor().putInt(KEY_DAY_OF_MONTH, dayOfYear).apply();
    }

    public int getDayOfMonth(){
        return getSharedPreferences().getInt(KEY_DAY_OF_MONTH, 0);
    }

    public void saveMaximunDayMonth(int maximumDayOfMonth){
        getSharedPreferencesEditor().putInt(KEY_DAY_MAXIMUM_MONTH, maximumDayOfMonth).apply();
    }

    public int getMaximunDayMonth(){
        return getSharedPreferences().getInt(KEY_DAY_MAXIMUM_MONTH, 0);
    }

    public int lastSynchronizationToReset(){
        return getSharedPreferences().getInt(KEY_RESET_CONTRACTS, 0);
    }

    public void setResetContractsSynced(int dayOfYear){
        getSharedPreferencesEditor().putInt(KEY_RESET_CONTRACTS, dayOfYear).apply();
    }

    public boolean lastSynchronizationToResetContractsPerWeek(){
        boolean flag = getSharedPreferences().getBoolean(KEY_RESET_CONTRACTS_PER_WEEK, false);
        if (flag){
            Calendar calendar = Calendar.getInstance();
            boolean setKeyFalse = calendar.get(Calendar.DAY_OF_WEEK) != 6;
            if (setKeyFalse) {
                getSharedPreferencesEditor().putBoolean(KEY_RESET_CONTRACTS_PER_WEEK, false).apply();
                return true;
            }
        }
        return false;
    }

    public void lastSynchronizationToResetContractsPerWeekAfterFewDaysWithoutCollecting() {
        getSharedPreferencesEditor().putBoolean(KEY_RESET_CONTRACTS_PER_WEEK, false).apply();
    }

    public void setResetContractsPerWeek(){
        if ( !getSharedPreferences().getBoolean(KEY_RESET_CONTRACTS_PER_WEEK, false) ) {
            Calendar calendar = Calendar.getInstance();
            //wednesday
            boolean flag = calendar.get(Calendar.DAY_OF_WEEK) == 6;
            Util.Log.ih("calendar.get(Calendar.DAY_OF_WEEK) = " + calendar.get(Calendar.DAY_OF_WEEK));
            Util.Log.ih("flag = " + flag);

            getSharedPreferencesEditor().putBoolean(KEY_RESET_CONTRACTS_PER_WEEK, flag).apply();
        }
    }

    public void setResetContractsPerWeekAfterFewDaysWithoutCollecting() {
        getSharedPreferencesEditor().putBoolean(KEY_RESET_CONTRACTS_PER_WEEK, true).apply();
    }

}
