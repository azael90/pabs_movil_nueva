package com.jaguarlabs.pabs.components;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by jordan on 7/09/16.
 */
public class PreferenceSettings {

    private static final String PREFERENCES_FILE = "APP_SETTINGS";
    private static final String KEY_SHOW_COLOR_CONTRACTS = "SHOW_COLOR_CONTRACTS";

    private Context context;

    public PreferenceSettings(Context context){
        this.context = context;
    }

    /**
     * gets sharedpreferences file
     * @return
     *      SharedPreferences file
     */
    private SharedPreferences getSharedPreferences(){
        return context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
    }

    /**
     * gets SharedPreferences editor
     * @return
     *      SharedPreferences editor
     */
    private SharedPreferences.Editor getSharedPreferencesEditor(){
        return getSharedPreferences().edit();
    }

    public void turnColorContracts(boolean showColors) {
        getSharedPreferencesEditor().putBoolean(KEY_SHOW_COLOR_CONTRACTS, showColors).apply();
    }

    public boolean isColorContractsActive(){
        return getSharedPreferences().getBoolean(KEY_SHOW_COLOR_CONTRACTS, true);
    }

}
