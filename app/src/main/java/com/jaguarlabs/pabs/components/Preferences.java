package com.jaguarlabs.pabs.components;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.jaguarlabs.pabs.util.BuildConfig;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class will be in charge of saving a payment that was registered.
 * Will help when payment's ticket fail to print.
 *
 * @author Jordan Alvarez
 * @version 17/06/15
 */
public class Preferences {

    public static final String PREFERENCES_FILE = "payment_made_file";
    private static final String KEY_FOLIO = "key_folio";
    private static final String KET_CONTRACT_NUMBER = "key_contract_number";
    private static final String KEY_AMOUNT = "key_amount";
    private static final String KEY_SYNC = "key_sync";
    private static final String KEY_URL = "key_url";
    private static final String KEY_IP = "key_ip";
    public static final String KEY_CARTERA_ACTUALIZADA ="key_actualizar_cartera";
    private static final String PREFERENCES_FILE_DATAERASED = "data_erased_file";
    private static final String KEY_COMPLETEAMOUNTS = "key_folio";

    private Context context;

    public Preferences(Context context){
        this.context = context;
    }

    /**
     * Saves payment that was just registered
     * @param folio payment's folio
     * @param amount payment's amount
     * @param contractNumber payment's contract number
     */
    public void savePayment(String folio, String amount, String contractNumber ){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();

        editor.putString(KEY_FOLIO, folio);
        editor.putString(KET_CONTRACT_NUMBER, contractNumber);
        editor.putString(KEY_AMOUNT, amount);
        editor.putString(KEY_SYNC, "false");

        editor.apply();
    }

    /**
     * Syncs payment
     * Payment was made and printed
     */
    public void setPaymentSync(){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();

        editor.putString(KEY_SYNC, "true");
        editor.apply();
    }

    /**
     * Syncs payment
     * Payment was made and printed
     */
    public void setPaymentNotSync(){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();

        editor.putString(KEY_SYNC, "false");
        editor.apply();
    }

    /**
     * checks if payment is synced
     * @return true if it's synced
     *         false otherwise
     */
    public boolean isSynchronized(){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);

        return preferences.getString(KEY_SYNC, "false").equals( "true" ) ? true : false;
    }

    /**
     * Checks if payment was made and printed (is synchronized)
     * if so, clean information
     * @return true or false if it is synchronized
     */
    public boolean clearPaymentIfSync(){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);

        if ( preferences.getString(KEY_SYNC, "false").equals( "true" ) ){
            Editor editor = preferences.edit();
            editor.putString(KEY_FOLIO, "");
            editor.putString(KET_CONTRACT_NUMBER, "");
            editor.putString(KEY_AMOUNT, "");
            editor.putString(KEY_SYNC, "false");

            editor.apply();

            return true;
        }
        else {
            return false;
        }

    }

    /**
     * Checks if a payment was made
     * receives false: was not made
     * @return true or false if there is a payment
     */
    public boolean isThereAPaymentAndIsSynchronized(){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);

        if ( preferences.getString(KEY_FOLIO, "").equals( "" ) ){
            return true;
        }
        else{
            return preferences.getString(KEY_SYNC, "false").equals( "true" ) ? true : false;
        }
    }

    /**
     * Gets last payment saved into SharedPreferences file
     * @return payment: JSONObject with last payment saved
     */
    public JSONObject getPayment(){
        JSONObject payment = new JSONObject();

        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);

        try {
            payment.put(KEY_FOLIO, preferences.getString(KEY_FOLIO, ""));
            payment.put(KET_CONTRACT_NUMBER, preferences.getString(KET_CONTRACT_NUMBER, ""));
            payment.put(KEY_AMOUNT, preferences.getString(KEY_AMOUNT, ""));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return payment;

    }

    public void saveURLTEST(){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();

        editor.putString(KEY_URL, "TEST");
        editor.apply();
    }

    public void clearURL(){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();

        editor.putString(KEY_URL, "");
        editor.apply();
    }

    public String getServerURL(){
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);

        return preferences.getString(KEY_URL, "");
    }

    public void changeServerIP( String newIP ) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();

        editor.putString(KEY_IP, newIP);
        editor.apply();
    }

    public String getServerIP() {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);

        //return preferences.getString(KEY_IP, "50.62.56.182");
        //return preferences.getString(KEY_IP, "35.167.149.196");
        //return preferences.getString(KEY_IP, "192.168.0.107");

        switch (BuildConfig.getTargetBranch()) {
            case NAYARIT: case MEXICALI:case MORELIA: case TORREON:
                return preferences.getString(KEY_IP, "54.70.207.245");
            case QUERETARO:case IRAPUATO:case SALAMANCA:case CELAYA: case LEON:
                return preferences.getString(KEY_IP, "54.203.201.17");
            default:
                return preferences.getString(KEY_IP, "35.167.149.196");
        }
    }

    public static void savePreferenceBoolean(Context c, boolean bandera, String key)
    {
        SharedPreferences preferences= c.getSharedPreferences(PREFERENCES_FILE, c.MODE_PRIVATE);
        preferences.edit().putBoolean(key, bandera).apply();
    }

    public static boolean obtenerPreferenceBoolean(Context c, String key)
    {
        SharedPreferences preferences= c.getSharedPreferences(PREFERENCES_FILE, c.MODE_PRIVATE);
        return preferences.getBoolean(key, false  );
    }








}
