package com.jaguarlabs.pabs.database;

import com.orm.SugarRecord;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Jordan Alvarez on 4/21/2017.
 */

public class Osticket extends SugarRecord {

    private String topic;
    private String message;
    private String date;
    private long dateMilliseconds;
    private boolean status;
    private String latitud;
    private String longitud;

    public Osticket(){}

    public Osticket( String topic, String message, String latitud, String longitud ){
        this.topic = topic;
        this.message = message;
        this.date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format( new Date() );
        this.dateMilliseconds = new Date().getTime();
        this.status = false;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getDateMilliseconds() {
        return dateMilliseconds;
    }

    public void setDateMilliseconds(long dateMilliseconds) {
        this.dateMilliseconds = dateMilliseconds;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
}
