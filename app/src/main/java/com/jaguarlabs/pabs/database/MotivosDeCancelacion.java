package com.jaguarlabs.pabs.database;

import com.orm.SugarRecord;

public class MotivosDeCancelacion extends SugarRecord
{
    String motivo, id_web;

    public MotivosDeCancelacion() {
    }

    public MotivosDeCancelacion(String motivo, String id_web)
    {
        this.motivo = motivo;
        this.id_web = id_web;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getId_web() {
        return id_web;
    }

    public void setId_web(String id_web) {
        this.id_web = id_web;
    }
}
