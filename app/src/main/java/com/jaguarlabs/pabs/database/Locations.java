package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by jordan on 03/03/16.
 */
public class Locations extends SugarRecord {

    @SerializedName("no_ubicacion")
    int numberLocation;
    @SerializedName("fecha")
    String date;
    @SerializedName("latitud")
    String latitude;
    @SerializedName("longitud")
    String longitude;
    @SerializedName("sync")
    boolean sync;

    public Locations(){}

    public Locations(String date, String latitude, String longitude, boolean sync){
        this.date = date;
        this.latitude = latitude;
        this.longitude = longitude;
        this.sync = sync;
    }

    public int getNumberLocation() {
        return numberLocation;
    }

    public void setNumberLocation(int numberLocation) {
        this.numberLocation = numberLocation;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }
}
