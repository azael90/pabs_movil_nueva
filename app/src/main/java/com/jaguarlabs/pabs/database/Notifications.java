package com.jaguarlabs.pabs.database;

import com.orm.SugarRecord;

/**
 * Created by jordan on 03/03/16.
 */
public class Notifications extends SugarRecord {

    String idNotification;
    String creationDate;
    String message;
    String numberCollector;
    int status;

    public Notifications(){}

    public Notifications(String idNotification, String creationDate, String message, String numberCollector, int status){
        this.idNotification = idNotification;
        this.creationDate = creationDate;
        this.message = message;
        this.numberCollector = numberCollector;
        this.status = status;
    }

    public String getIdNotification() {
        return idNotification;
    }

    public void setIdNotification(String idNotification) {
        this.idNotification = idNotification;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNumberCollector() {
        return numberCollector;
    }

    public void setNumberCollector(String numberCollector) {
        this.numberCollector = numberCollector;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
