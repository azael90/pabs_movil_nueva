package com.jaguarlabs.pabs.database;

import com.orm.SugarRecord;

public class Configuraciones extends SugarRecord
{
    String serie_impresora, mac_fisica;

    public Configuraciones() {
    }

    public Configuraciones(String serie_impresora, String mac_fisica) {
        this.serie_impresora = serie_impresora;
        this.mac_fisica = mac_fisica;
    }

    public String getSerie_impresora() {
        return serie_impresora;
    }

    public void setSerie_impresora(String serie_impresora) {
        this.serie_impresora = serie_impresora;
    }

    public String getMac_fisica() {
        return mac_fisica;
    }

    public void setMac_fisica(String mac_fisica) {
        this.mac_fisica = mac_fisica;
    }
}
