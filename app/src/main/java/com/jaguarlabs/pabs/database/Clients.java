package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Unique;

/**
 * Created by jordan on 03/03/16.
 */
public class Clients extends SugarRecord {

    @Unique
    @SerializedName("id_contrato")
    String idContract;
    @SerializedName("nombre")
    String name;
    @SerializedName("apellido_pat")
    String firstLastName;
    @SerializedName("apellido_mat")
    String secondLastName;
    @SerializedName("localidad")
    String locality;
    @SerializedName("no_contrato")
    String numberContract;
    @SerializedName("no_cobrador")
    String numberCollector;
    @SerializedName("calle_cobro")
    String streetToPay;
    @SerializedName("no_ext_cobro")
    String numberExt;
    @SerializedName("entre_calles")
    String betweenStreets;
    @SerializedName("colonia")
    String neighborhood;
    @SerializedName("abono")
    String payment;
    @SerializedName("saldo")
    String balance;
    @SerializedName("monto_pago_actual")
    String actualPayment;
    @SerializedName("serie")
    String serie;
    @SerializedName("forma_pago_actual")
    String paymentOption;
    @SerializedName("id_grupo_base")
    String idBaseGroup;
    @SerializedName("tipo_bd")
    String typeBD;
    @SerializedName("sync")
    boolean sync;
    @SerializedName("latitude")
    String latitude;
    @SerializedName("longitude")
    String longitude;
    @SerializedName("estatus")
    String status;
    @SerializedName("fecha_ultimo_abono")
    String lastPaymentDate;
    @SerializedName("saldo_atrasado")
    String overdueBalance;
    @SerializedName("distancia")
    String distance;
    @SerializedName("Comentario")
    String comment;
    @SerializedName("dia_pago")
    String payDay;
    @SerializedName("primer_dia_pago")
    String firstPayday;
    @SerializedName("segundo_dia_pago")
    String secondPayday;
    @SerializedName("fecha_primer_abono")
    String firstPaymentDate;



    @SerializedName("phone")
    String phone;
    @SerializedName("contrato_estatus")
    String contractStatus;
    @SerializedName("fecha_ultima_actualizacion")
    String lastupdate;
    @SerializedName("email")
    String email;
    @SerializedName("numerointerior")
    String numerointerior;
    @SerializedName("fechareactiva")
    String fechareactiva;


    public Clients(){}

    public Clients(
            String idContract, String name, String firstLastName, String secondLastName, String locality,
            String numberContract, String numberCollector, String streetToPay, String numberExt,
            String betweenStreets, String neighborhood, String payment, String balance, String actualPayment,
            String serie, String paymentOption, String idBaseGroup, String typeBD, boolean sync,
            String latitude, String longitude, String status, String lastPaymentDate, String overdueBalance,
            String comentario, String firstPaymentDate, String phone, String contractStatus, String lastupdate,
            String email, String numerointerior, String fechaReactiva
    ){
        this.idContract = idContract;
        this.name = name;
        this.firstLastName = firstLastName;
        this.secondLastName = secondLastName;
        this.locality = locality;
        this.numberContract = numberContract;
        this.numberCollector = numberCollector;
        this.streetToPay = streetToPay;
        this.numberExt = numberExt;
        this.betweenStreets = betweenStreets;
        this.neighborhood = neighborhood;
        this.payment = payment;
        this.balance = balance;
        this.actualPayment = actualPayment;
        this.serie = serie;
        this.paymentOption = paymentOption;
        this.idBaseGroup = idBaseGroup;
        this.typeBD = typeBD;
        this.sync = sync;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
        this.lastPaymentDate = lastPaymentDate;
        this.overdueBalance = overdueBalance;
        this.comment = comentario;
        this.payDay = "0";
        this.firstPayday = "0";
        this.secondPayday = "0";
        this.firstPaymentDate = firstPaymentDate;
        this.phone = phone;
        this.contractStatus = contractStatus;
        this.lastupdate=lastupdate;
        this.email=email;
        this.numerointerior=numerointerior;
        this.fechareactiva=fechaReactiva;
    }

    public Clients(
            String idContract, String name, String firstLastName, String secondLastName, String locality,
            String numberContract, String numberCollector, String streetToPay, String numberExt,
            String betweenStreets, String neighborhood, String payment, String balance, String actualPayment,
            String serie, String paymentOption, String idBaseGroup, String typeBD, boolean sync,
            String latitude, String longitude, String status, String lastPaymentDate, String comentario,
            String firstPaymentDate, String phone, String contractStatus, String lastupdate, String email,
            String numerointerior, String fechaReactiva
    ){
        this.idContract = idContract;
        this.name = name;
        this.firstLastName = firstLastName;
        this.secondLastName = secondLastName;
        this.locality = locality;
        this.numberContract = numberContract;
        this.numberCollector = numberCollector;
        this.streetToPay = streetToPay;
        this.numberExt = numberExt;
        this.betweenStreets = betweenStreets;
        this.neighborhood = neighborhood;
        this.payment = payment;
        this.balance = balance;
        this.actualPayment = actualPayment;
        this.serie = serie;
        this.paymentOption = paymentOption;
        this.idBaseGroup = idBaseGroup;
        this.typeBD = typeBD;
        this.sync = sync;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
        this.lastPaymentDate = lastPaymentDate;
        this.comment = comentario;
        this.payDay = "0";
        this.firstPayday = "0";
        this.secondPayday = "0";
        this.firstPaymentDate = firstPaymentDate;
        this.phone= phone;
        this.contractStatus = contractStatus;
        this.lastupdate=lastupdate;
        this.email=email;
        this.numerointerior=numerointerior;
        this.fechareactiva=fechaReactiva;
    }

    public String getIdContract() {
        return idContract;
    }

    public void setIdContract(String idContract) {
        this.idContract = idContract;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstLastName() {
        return firstLastName;
    }

    public void setFirstLastName(String firstLastName) {
        this.firstLastName = firstLastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getNumberContract() {
        return numberContract;
    }

    public void setNumberContract(String numberContract) {
        this.numberContract = numberContract;
    }

    public String getNumberCollector() {
        return numberCollector;
    }

    public void setNumberCollector(String numberCollector) {
        this.numberCollector = numberCollector;
    }

    public String getStreetToPay() {
        return streetToPay;
    }

    public void setStreetToPay(String streetToPay) {
        this.streetToPay = streetToPay;
    }

    public String getNumberExt() {
        return numberExt;
    }

    public void setNumberExt(String numberExt) {
        this.numberExt = numberExt;
    }

    public String getBetweenStreets() {
        return betweenStreets;
    }

    public void setBetweenStreets(String betweenStreets) {
        this.betweenStreets = betweenStreets;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getActualPayment() {
        return actualPayment;
    }

    public void setActualPayment(String actualPayment) {
        this.actualPayment = actualPayment;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }

    public String getIdBaseGroup() {
        return idBaseGroup;
    }

    public void setIdBaseGroup(String idBaseGroup) {
        this.idBaseGroup = idBaseGroup;
    }

    public String getTypeBD() {
        return typeBD;
    }

    public void setTypeBD(String typeBD) {
        this.typeBD = typeBD;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastPaymentDate() {
        return lastPaymentDate;
    }

    public void setLastPaymentDate(String lastPaymentDate) {
        this.lastPaymentDate = lastPaymentDate;
    }

    public String getOverdueBalance() {
        return overdueBalance;
    }

    public void setOverdueBalance(String overdueBalance) {
        this.overdueBalance = overdueBalance;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comments) {
        this.comment = comments;
    }

    public String getPayDay() {
        return payDay;
    }

    public void setPayDay(String payDay) {
        this.payDay = payDay;
    }

    public String getFirstPayday() {
        return firstPayday;
    }

    public void setFirstPayday(String firstPayday) {
        this.firstPayday = firstPayday;
    }

    public String getSecondPayday() {
        return secondPayday;
    }

    public void setSecondPayday(String secondPayday) {
        this.secondPayday = secondPayday;
    }

    public String getFirstPaymentDate() {
        return firstPaymentDate;
    }

    public void setFirstPaymentDate(String firstPaymentDate) {
        this.firstPaymentDate = firstPaymentDate;
    }


    public String getTelefono() {
        return phone;
    }

    public void setTelefono(String phone) {
        this.phone = phone;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }


    public String getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(String lastupdate) {
        this.lastupdate = lastupdate;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getNumerointerior() {
        return numerointerior;
    }

    public void setNumerointerior(String numerointerior) {
        this.numerointerior = numerointerior;
    }

    public String getFechareactiva() {
        return fechareactiva;
    }

    public void setFechareactiva(String fechareactiva) {
        this.fechareactiva = fechareactiva;
    }
}
