package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by Emmanuel Rodriguez on 02/04/2019.
 */
public class ClientsCellPhoneNumbers extends SugarRecord {

    @SerializedName("id_contrato")
    private long contractID;
    @SerializedName("serie")
    private String contractSerie;
    @SerializedName("no_contrato")
    private String contractNumber;
    private String cellPhoneNumber;
    private boolean sync;

    public ClientsCellPhoneNumbers(){}

    public ClientsCellPhoneNumbers(long contractID, String serie, String no_contrato, String cellPhoneNumber){
        this.contractID = contractID;
        this.contractSerie = serie;
        this.contractNumber = no_contrato;
        this.cellPhoneNumber = cellPhoneNumber;
        this.sync = false;
    }

    public long getContractID() {
        return contractID;
    }

    public void setContractID(long contractID) {
        this.contractID = contractID;
    }

    public String getContractSerie() {
        return contractSerie;
    }

    public void setContractSerie(String contractSerie) {
        this.contractSerie = contractSerie;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }

    public boolean getSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }
}
