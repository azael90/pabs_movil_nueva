package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by jordan on 03/03/16.
 */
public class Companies extends SugarRecord {

    @SerializedName("id")
    String idCompany;
    @SerializedName("nombre")
    String name;
    @SerializedName("rfc")
    String rfc;
    @SerializedName("razon_social")
    String businessName;
    @SerializedName("direccion")
    String address;
    @SerializedName("telefono")
    String phone;

    public Companies(){}

    public Companies(String idCompany, String name, String rfc, String businessName, String address, String phone){
        this.idCompany = idCompany;
        this.name = name;
        this.rfc = rfc;
        this.businessName = businessName;
        this.address = address;
        this.phone = phone;
    }

    public String getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(String idCompany) {
        this.idCompany = idCompany;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
