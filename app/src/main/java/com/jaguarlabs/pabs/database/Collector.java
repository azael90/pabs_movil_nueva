package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by jordan on 26/08/16.
 */
@SuppressWarnings("SpellCheckingInspection")
public class Collector extends SugarRecord {

    @SerializedName("codigo")
    private String code;
    @SerializedName("nombre")
    private String name;
    @SerializedName("no_cobrador")
    private String collectorNumber;

    public Collector(){}

    public Collector(String code, String name, String collectorNumber){
        this.code = code;
        this.name = name;
        this.collectorNumber = collectorNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCollectorNumber() {
        return collectorNumber;
    }

    public void setCollectorNumber(String collectorNumber) {
        this.collectorNumber = collectorNumber;
    }
}
