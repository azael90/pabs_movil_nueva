package com.jaguarlabs.pabs.database;

import com.orm.SugarRecord;

public class Actualizados extends SugarRecord
{
    public String codigocobrador, numerocontrato, calle, numexterior, colonia, localidad, entrecalles, correo, telefono, interior;
    public int sync;

    public Actualizados()
    {

    }

    public Actualizados(String codigocobrador, String numerocontrato, String calle, String numexterior, String colonia,
                        String localidad, String entrecalles, String correo, String telefono, int sync, String interior) {
        this.codigocobrador = codigocobrador;
        this.numerocontrato = numerocontrato;
        this.calle = calle;
        this.numexterior = numexterior;
        this.colonia = colonia;
        this.localidad = localidad;
        this.entrecalles = entrecalles;
        this.correo = correo;
        this.telefono = telefono;
        this.sync=sync;
        this.interior=interior;
    }

    public String getCodigocobrador() {
        return codigocobrador;
    }

    public void setCodigocobrador(String codigocobrador) {
        this.codigocobrador = codigocobrador;
    }

    public String getNumerocontrato() {
        return numerocontrato;
    }

    public void setNumerocontrato(String numerocontrato) {
        this.numerocontrato = numerocontrato;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumexterior() {
        return numexterior;
    }

    public void setNumexterior(String numexterior) {
        this.numexterior = numexterior;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getEntrecalles() {
        return entrecalles;
    }

    public void setEntrecalles(String entrecalles) {
        this.entrecalles = entrecalles;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getSync() {
        return sync;
    }

    public void setJsongeneral(int sync) {
        this.sync = sync;
    }


    public String getInterior() {
        return interior;
    }

    public void setInterior(String interior) {
        this.interior = interior;
    }
}
