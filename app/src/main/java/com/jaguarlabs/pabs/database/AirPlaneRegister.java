package com.jaguarlabs.pabs.database;

import com.orm.SugarRecord;

public class AirPlaneRegister extends SugarRecord
{
    String no_cobrador="", fecha="", activado="";
    int sync;

    public AirPlaneRegister() {
    }

    public AirPlaneRegister(String no_cobrador, String fecha, String activado, int sync) {
        this.no_cobrador = no_cobrador;
        this.fecha = fecha;
        this.activado = activado;
        this.sync = sync;
    }

    public String getNo_cobrador() {
        return no_cobrador;
    }

    public void setNo_cobrador(String no_cobrador) {
        this.no_cobrador = no_cobrador;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getActivado() {
        return activado;
    }

    public void setActivado(String activado) {
        this.activado = activado;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }
}
