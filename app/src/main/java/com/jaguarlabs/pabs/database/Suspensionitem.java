package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class Suspensionitem extends SugarRecord
{
    @SerializedName("descripcion")
    String descripcion="";

    public Suspensionitem() {
    }

    public Suspensionitem(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
