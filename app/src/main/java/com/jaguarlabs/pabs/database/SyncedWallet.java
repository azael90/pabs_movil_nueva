package com.jaguarlabs.pabs.database;

import android.content.SyncContext;

import com.google.gson.Gson;
import com.jaguarlabs.pabs.models.ModelClient;
import com.jaguarlabs.pabs.util.LogRegister;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.dsl.Unique;

import java.util.Calendar;
import java.util.List;

/**
 * Created by jordan on 4/03/16.
 */
public class SyncedWallet extends SugarRecord {

    //blue -> app base color
    public static final String COLOR_DEFAULT_CONTRACT = "default";
    //yellow
    public static final String COLOR_PENDING_CONTRACT = "pending contract";
    //red
    public static final String COLOR_PENDING_LEVEL_2_CONTRACT = "pending contract level two";
    //green
    public static final String COLOR_TODAY_CONTRACT = "today contract";
    //grey
    public static final String COLOR_WITHOUT_ANALYSIS_CONTRACT = "without analysis contract";
    //white
    public static final String COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT = "without analysis_suspended_contract";

    @Unique
    String contractID;
    String payDay;//client table
    String newPayday;
    int visitCounter;
    boolean visitMade;
    String paymentOption;//client table
    boolean paymentMade;
    boolean sync;
    boolean notVisited;
    boolean showAgain;
    int dayOfYear;
    boolean showColor;
    int dayOfWeekInMonth;
    String firstPaydayWeekly;
    String secondPaydayWeekly;
    long dayCounter;
    int secondVisitDay;
    String color;
    String colorWithoutAnalysisContractAux;

    //without analysis contracts fields
    String paydayWA;
    String firstPaydayWA;
    String secondPaydayWA;

    boolean paymentMadeBefore;
    int paydayOfPaymentMadeBeforeWeekly;
    int paydayOfPaymentMadeBeforeWeeklyNumberOfWeek;
    int paydayOfPaymentMadeBeforeFortnightly;
    int paydayOfPaymentMadeBeforeMonthly;
    int paydayOfPaymentMadeBeforeFortnightlyAndMonthly;
    boolean compareToFirstPayday;

    String status;

    @Ignore
    ModelClient client;

    String serializedClient;

    boolean FPA;
    boolean FPAPending;

    //String firstPaymentDate;

    public SyncedWallet(){ //LogRegister.registrarSemaforo("Objeto creado");
    }

    /*
    public SyncedWallet(SyncedWallet asd ){
        deserializeClient();
    }
    */

    public SyncedWallet(String contractID, String payDay, String paymentOption, String firstPayday, String secondPayday, ModelClient client, String status){
        this.contractID = contractID;
        this.payDay = payDay;
        this.paymentOption = paymentOption;
        this.visitCounter = 0;
        this.notVisited = false;
        this.showColor = true;
        this.dayOfWeekInMonth = 0;
        this.firstPaydayWeekly = firstPayday;
        this.secondPaydayWeekly = secondPayday;
        this.color = COLOR_DEFAULT_CONTRACT;
        this.colorWithoutAnalysisContractAux = "";
        this.paymentMadeBefore = false;

        this.paydayWA = "";
        this.firstPaydayWA = "";
        this.secondPaydayWA = "";

        this.serializedClient = new Gson().toJson(client);

        this.status = status;

        //this.firstPaymentDate = firstPaymentDate;

        this.dayCounter = 0;
        this.paymentMade = false;

        this.FPA = false;
        this.FPAPending = false;

        this.secondVisitDay = 0;


        this.newPayday = "";
    }

    public SyncedWallet(String contractID, String payDay, String paymentOption, String firstPayday, String secondPayday, ModelClient client, String status, String newPayday, long dayCounter){
        this.contractID = contractID;
        this.payDay = payDay;
        this.paymentOption = paymentOption;
        this.visitCounter = 0;
        this.notVisited = false;
        this.showColor = true;
        this.dayOfWeekInMonth = 0;
        this.firstPaydayWeekly = firstPayday;
        this.secondPaydayWeekly = secondPayday;
        this.color = COLOR_DEFAULT_CONTRACT;
        this.colorWithoutAnalysisContractAux = "";
        this.paymentMadeBefore = false;

        this.paydayWA = "";
        this.firstPaydayWA = "";
        this.secondPaydayWA = "";

        this.serializedClient = new Gson().toJson(client);

        this.status = status;

        //this.firstPaymentDate = firstPaymentDate;

        this.dayCounter = dayCounter;
        this.paymentMade = false;

        this.FPA = false;
        this.FPAPending = false;

        this.secondVisitDay = 0;


        this.newPayday = newPayday;

        //LogRegister.registrarSemaforo(contractID + " creado");
    }



    public boolean isShowColor() {
        return showColor;
    }

    public void setShowColor(boolean showColor) {
        this.showColor = showColor;
        //LogRegister.registrarSemaforo("Mostrar color: " + showColor);
    }

    public long getDayCounter() {
        return dayCounter;
    }

    public void setDayCounter(long dayCounter) {
        this.dayCounter = dayCounter;
        //LogRegister.registrarSemaforo("Contador de dias: " + dayCounter);
    }

    public boolean isNotVisited() {
        return notVisited;
    }

    public void setNotVisited(boolean notVisited) {
        this.notVisited = notVisited;
        //LogRegister.registrarSemaforo("Set no visitado: " + notVisited);
    }

    public boolean isShowAgain() {
        //LogRegister.registrarSemaforo("Mostrar de nuevo: " + showAgain);
        return showAgain;
    }

    public void setShowAgain(boolean showAgain) {
        this.showAgain = showAgain;
        //LogRegister.registrarSemaforo("Set mostrar de nuevo: " + showAgain);
    }

    public boolean isSync() {
        //LogRegister.registrarSemaforo("Sincronizado: " + sync);
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
        //LogRegister.registrarSemaforo("Set sincronizado: " + sync);
    }

    public String getContractID() {
        //LogRegister.registrarSemaforo("Contrato: " + contractID);
        return contractID;
    }

    public void setContractID(String contractID) {
        this.contractID = contractID;
        //LogRegister.registrarSemaforo("Set contrato: " + contractID);
    }

    public String getPaymentOption() {
        //LogRegister.registrarSemaforo("Opcion de pago: " + paymentOption);
        return paymentOption;
    }

    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }

    public String getPayDay() {
        return payDay;
    }

    public void setPayDay(String payDay) {
        this.payDay = payDay;
        //LogRegister.registrarSemaforo("Set dia de pago: " + payDay);
    }

    public int getVisitCounter() {
        //LogRegister.registrarSemaforo("Contador de visitas: " + visitCounter);
        return visitCounter;
    }

    public void setVisitCounter(int visitCounter) {
        this.visitCounter = visitCounter;
        //LogRegister.registrarSemaforo("Set contador de visitas: " + visitCounter);
    }

    public boolean isVisitMade() {
        return visitMade;
    }

    public void setVisitMade(boolean visitMade) {
        this.visitMade = visitMade;
        //LogRegister.registrarSemaforo("Set visita hecha: " + visitMade);
    }

    public boolean isPaymentMade()
    {
        //LogRegister.registrarSemaforo("Pago hecho: " + paymentMade);
        return paymentMade;
    }

    public void setPaymentMade(boolean paymentMade) {
        this.paymentMade = paymentMade;
        //LogRegister.registrarSemaforo("Set pago hecho: " + paymentMade);
    }

    public String getNewPayday()
    {
        //LogRegister.registrarSemaforo("Nuevo dia de pago: " + newPayday);
        return newPayday;
    }

    public void setNewPayday(String newPayday) {
        this.newPayday = newPayday;
        //LogRegister.registrarSemaforo("Set nuevo dia de pago: " + newPayday);
    }

    public int getDayOfWeekInMonth() {
        return dayOfWeekInMonth;
    }

    public void setDayOfWeekInMonth(int dayOfWeekInMonth) {
        this.dayOfWeekInMonth = dayOfWeekInMonth;
    }

    public String getFirstPaydayWeekly() {
        return firstPaydayWeekly;
    }

    public void setFirstPaydayWeekly(String firstPaydayWeekly) {
        this.firstPaydayWeekly = firstPaydayWeekly;
        //LogRegister.registrarSemaforo("Set primer dia de pago semanal: " + firstPaydayWeekly);
    }

    public String getSecondPaydayWeekly() {
        return secondPaydayWeekly;
    }

    public void setSecondPaydayWeekly(String secondPaydayWeekly) {
        this.secondPaydayWeekly = secondPaydayWeekly;
        //LogRegister.registrarSemaforo("Set segundo dia de pago semanal: " + secondPaydayWeekly);
    }

    public int getDayOfYear()
    {
        //LogRegister.registrarSemaforo("Dia del año: " + dayOfYear);
        return dayOfYear;
    }

    public void setDayOfYear(int dayOfYear) {
        this.dayOfYear = dayOfYear;
        //LogRegister.registrarSemaforo("Set dia del año: " + dayOfYear);
    }

    public int getSecondVisitDay() {
        //LogRegister.registrarSemaforo("Segundo dia de visita: " + secondVisitDay);
        return secondVisitDay;
    }

    public void setSecondVisitDay(int secondVisitDay) {
        this.secondVisitDay = secondVisitDay;
        //LogRegister.registrarSemaforo("Set segundo dia de visita: " + secondVisitDay);
    }

    public String getColor()
    {
        //LogRegister.registrarSemaforo("Color: " + color);
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        //LogRegister.registrarSemaforo("Set color: " + color);
    }

    public String getColorWithoutAnalysisContractAux() {
        return colorWithoutAnalysisContractAux;
    }

    public void setColorWithoutAnalysisContractAux(String colorWithoutAnalysisContractAux) {
        this.colorWithoutAnalysisContractAux = colorWithoutAnalysisContractAux;
    }

    private void serializeClient(ModelClient client) {
        //this.client = client;
        this.serializedClient = new Gson().toJson(client);
        //LogRegister.registrarSemaforo("Serializar cliente: " + client.getNumberContract());
    }

    public SyncedWallet deserializeClient() {
        this.client = new Gson().fromJson(serializedClient, ModelClient.class);
        //LogRegister.registrarSemaforo("Deserializar cliente: " + client.getNumberContract());

        return this;
    }

    public ModelClient getClient()
    {
        //LogRegister.registrarSemaforo("Cliente: " + client.getNumberContract());
        return client;
    }

    public void setClientAndSerialize(ModelClient client) {
        //this.client = client;
        serializeClient(client);
    }

    public void setClient(ModelClient client) {
        this.client = client;
        //LogRegister.registrarSemaforo("Set cliente: " + client.getNumberContract());
        //serializeClient(client);
    }

    public String getSerializedClient() {
        //LogRegister.registrarSemaforo("Cliente serializado: " + new Gson().fromJson(serializedClient, ModelClient.class).getNumberContract());
        return serializedClient;
    }

    public void setSerializedClient(String serializedClient) {
        this.serializedClient = serializedClient;
    }

    public String getPaydayWA() {
        return paydayWA;
    }

    public void setPaydayWA(String paydayWA) {
        this.paydayWA = paydayWA;
    }

    public String getFirstPaydayWA() {
        return firstPaydayWA;
    }

    public void setFirstPaydayWA(String firstPaydayWA) {
        this.firstPaydayWA = firstPaydayWA;
    }

    public String getSecondPaydayWA() {
        return secondPaydayWA;
    }

    public void setSecondPaydayWA(String secondPaydayWA) {
        this.secondPaydayWA = secondPaydayWA;
    }

    public boolean isPaymentMadeBefore()
    {
        //LogRegister.registrarSemaforo("Pago hecho antes: " + paymentMadeBefore);
        return paymentMadeBefore;
    }

    public void setPaymentMadeBefore(boolean paymentMadeBefore) {
        this.paymentMadeBefore = paymentMadeBefore;
        //LogRegister.registrarSemaforo("Set pago hecho antes: " + paymentMadeBefore);
    }

    public int getPaydayOfPaymentMadeBeforeWeekly() {
        return paydayOfPaymentMadeBeforeWeekly;
    }

    public void setPaydayOfPaymentMadeBeforeWeekly(int paydayOfPaymentMadeBeforeWeekly) {
        this.paydayOfPaymentMadeBeforeWeekly = paydayOfPaymentMadeBeforeWeekly;
    }

    public int getPaydayOfPaymentMadeBeforeWeeklyNumberOfWeek() {
        return paydayOfPaymentMadeBeforeWeeklyNumberOfWeek;
    }

    public void setPaydayOfPaymentMadeBeforeWeeklyNumberOfWeek(int paydayOfPaymentMadeBeforeWeeklyNumberOfWeek) {
        this.paydayOfPaymentMadeBeforeWeeklyNumberOfWeek = paydayOfPaymentMadeBeforeWeeklyNumberOfWeek;
    }

    public int getPaydayOfPaymentMadeBeforeFortnightly() {
        return paydayOfPaymentMadeBeforeFortnightly;
    }

    public void setPaydayOfPaymentMadeBeforeFortnightly(int paydayOfPaymentMadeBeforeFortnightly) {
        this.paydayOfPaymentMadeBeforeFortnightly = paydayOfPaymentMadeBeforeFortnightly;
    }

    public int getPaydayOfPaymentMadeBeforeMonthly() {
        return paydayOfPaymentMadeBeforeMonthly;
    }

    public void setPaydayOfPaymentMadeBeforeMonthly(int paydayOfPaymentMadeBeforeMonthly) {
        this.paydayOfPaymentMadeBeforeMonthly = paydayOfPaymentMadeBeforeMonthly;
    }

    public int getPaydayOfPaymentMadeBeforeFortnightlyAndMonthly() {
        return paydayOfPaymentMadeBeforeFortnightlyAndMonthly;
    }

    public void setPaydayOfPaymentMadeBeforeFortnightlyAndMonthly(int paydayOfPaymentMadeBeforeFortnightlyAndMonthly) {
        this.paydayOfPaymentMadeBeforeFortnightlyAndMonthly = paydayOfPaymentMadeBeforeFortnightlyAndMonthly;
    }

    public boolean isCompareToFirstPayday() {
        return compareToFirstPayday;
    }

    public void setCompareToFirstPayday(boolean compareToFirstPayday) {
        this.compareToFirstPayday = compareToFirstPayday;
    }

    public String getStatus()
    {
        //LogRegister.registrarSemaforo("Status: " + status);
        return status;
    }

    public void setStatus(String status)
    {
        //LogRegister.registrarSemaforo("set status: " + status);
        this.status = status;
    }

    public boolean isFPA()
    {
        //LogRegister.registrarSemaforo("FPA: " + FPA);
        return FPA;
    }

    public void setFPA(boolean FPA)
    {
        this.FPA = FPA;
        //LogRegister.registrarSemaforo("set FPA: " + FPA);
    }

    public boolean isFPAPending()
    {
        //LogRegister.registrarSemaforo("FPA pendiente: " + FPAPending);
        return FPAPending;
    }

    public void setFPAPending(boolean FPAPending)
    {
        this.FPAPending = FPAPending;
        //LogRegister.registrarSemaforo("set FPA pendiente: " + FPAPending);
    }

    /*
    public String getFirstPaymentDate() {
        return firstPaymentDate;
    }

    public void setFirstPaymentDate(String firstPaymentDate) {
        this.firstPaymentDate = firstPaymentDate;
    }
    */
}
