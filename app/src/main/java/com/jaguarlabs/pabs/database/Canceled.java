package com.jaguarlabs.pabs.database;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

/**
 * Created by jordan on 03/03/16.
 */
public class Canceled extends SugarRecord {

    @Ignore
    String idCanceled;

    String amount;
    String company;
    String numberClient;
    boolean sync;
    String cancelDate;
    int period;
    String motivocancelacion;

    public Canceled(){}

    public Canceled(String amount, String company, String numberClient, boolean sync, String cancelDate, int period, String motivocancelacion){
        this.amount = amount;
        this.company = company;
        this.numberClient = numberClient;
        this.sync = sync;
        this.cancelDate = cancelDate;
        this.period = period;
        this.motivocancelacion=motivocancelacion;
    }

    public String getIdCanceled() {
        return idCanceled;
    }

    public void setIdCanceled(String idCanceled) {
        this.idCanceled = idCanceled;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getNumberClient() {
        return numberClient;
    }

    public void setNumberClient(String numberClient) {
        this.numberClient = numberClient;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getMotivocancelacion() {
        return motivocancelacion;
    }

    public void setMotivocancelacion(String motivocancelacion) {
        this.motivocancelacion = motivocancelacion;
    }
}
