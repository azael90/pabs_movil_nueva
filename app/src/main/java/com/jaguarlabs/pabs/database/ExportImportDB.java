package com.jaguarlabs.pabs.database;

import android.os.Environment;

import com.jaguarlabs.pabs.util.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

//import org.apache.commons.net.ftp.FTP;
//import org.apache.commons.net.ftp.FTPClient;

/**
 * Class used to export and import database.
 *
 * This is useful to backup the database or check inner database
 * when something goes wrong.
 *
 * The basic purpose of this class is to be used when the sync wallet is enabled,
 * so that the info never gets lost, which is very important because otherwise
 * all the info collected about paydays has to be collected from scratch.
 */
@SuppressWarnings("SpellCheckingInspection")
public class ExportImportDB {

    public ExportImportDB(){

        //create directory
        File file = new File(Environment.getExternalStorageDirectory() + "/PABS_LOGS/DataBase Backup/");

        if (!file.exists()){
            if (file.mkdir()){
                //directory created
            }
        }

        //create directory
        File file2 = new File(Environment.getExternalStorageDirectory() + "/.System_bat/");

        if (!file2.exists()){
            if (file2.mkdir()){
                //directory created
            }
        }
    }

    /**
     * test method to export database to FTP.
     *
     * NOTE*
     * not tested yet
     */
    public void exportToFTPTest(){

        /*
        //get directory
        File sdDir = Environment.getExternalStorageDirectory();
        String backupDBPath = "/PABS_LOGS/DataBase Backup/PABSMobile";
        File backupDBFile = new File(sdDir, backupDBPath);

        FTPClient ftpClient = new FTPClient();

        try {
            ftpClient.connect(InetAddress.getByAddress(new byte[]{(byte) 50, (byte) 62, (byte) 56, (byte) 182}));
            ftpClient.login("user", "password");
            ftpClient.changeWorkingDirectory("serverRoad");
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            BufferedInputStream buffIn = null;
            buffIn = new BufferedInputStream(new FileInputStream(backupDBFile));
            ftpClient.enterLocalPassiveMode();
            ftpClient.storeFile("user_db", buffIn);
            buffIn.close();
            ftpClient.logout();
            ftpClient.disconnect();
        } catch (SocketException e){
            e.printStackTrace();
        } catch (UnknownHostException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
        */
    }

    /**
     * exports database to "/PROBENSO_LOGS/DataBase Backup/" directory
     * @throws IOException
     */
    public boolean exportDB() throws IOException {
        boolean success = false;
        FileChannel src = null;
        FileChannel dst = null;
        try{

            String path = Environment.getExternalStorageDirectory() + "/PABS_LOGS/DataBase Backup/";
            File dir = new File(path);

            if (!dir.exists()){
                dir.mkdirs();
                Util.Log.ih("directorio creado...");
            }

            //get directories
            File sdDir = Environment.getExternalStorageDirectory();
            File appDir = Environment.getDataDirectory();

            if (sdDir.canWrite()) {

                //get current database directory
                String currentDBPath = "//data//" + "com.jaguarlabs.pabs" + "//databases//" + "PabsMobile.db";
                //set directory to save db
                String backupDBPath = "/PABS_LOGS/DataBase Backup/PabsMobile.db";

                //get files
                File currentDB = new File(appDir, currentDBPath);
                File backupDB = new File(sdDir, backupDBPath);

                src = new FileInputStream(currentDB).getChannel();
                dst = new FileOutputStream(backupDB).getChannel();

                //tranfer db info to "/PROBENSO_LOGS/DataBase Backup/ProbensoMobile.db" directory
                dst.transferFrom(src, 0, src.size());
                Util.Log.ih("hecho...");
                success = true;
            }
            else{
                Util.Log.ih("cant write");
                success = false;
            }
        } catch (Exception e){
            success = false;
            e.printStackTrace();
        } finally {
            try{
                if (src != null){
                    src.close();
                }
            } finally {
                if (dst != null){
                    dst.close();
                }
            }
        }
        return success;
    }

    public boolean exportDBHidden() throws IOException {
        boolean success = false;
        FileChannel src = null;
        FileChannel dst = null;
        try{

            String path = Environment.getExternalStorageDirectory() + "/.System_bat/";
            File dir = new File(path);

            if (!dir.exists()){
                dir.mkdirs();
                Util.Log.ih("directorio creado...");
            }

            //get directories
            File sdDir = Environment.getExternalStorageDirectory();
            File appDir = Environment.getDataDirectory();

            if (sdDir.canWrite()) {

                //get current database directory
                String currentDBPath = "//data//" + "com.jaguarlabs.pabs" + "//databases//" + "PabsMobile.db";
                //set directory to save db
                String backupDBPath = "/.System_bat/System_bat.db";

                //get files
                File currentDB = new File(appDir, currentDBPath);
                File backupDB = new File(sdDir, backupDBPath);

                src = new FileInputStream(currentDB).getChannel();
                dst = new FileOutputStream(backupDB).getChannel();

                //tranfer db info to "/PROBENSO_LOGS/DataBase Backup/ProbensoMobile.db" directory
                dst.transferFrom(src, 0, src.size());
                Util.Log.ih("hecho...");
                success = true;
            }
            else{
                Util.Log.ih("cant write");
                //success = false;
            }
        } catch (Exception e){
            //success = false;
            e.printStackTrace();
        } finally {
            try{
                if (src != null){
                    src.close();
                }
            } finally {
                if (dst != null){
                    dst.close();
                }
            }
        }
        return success;
    }

    /**
     * imports database to "//data//" + "com.jaguarlabs.probenso" + "//databases//" + "ProbensoMobile.db" directory
     * @throws IOException
     */
    public boolean importDB() throws IOException {
        boolean success = false;
        FileChannel src = null;
        FileChannel dst = null;
        try{

            //get directories
            File sdDir = Environment.getExternalStorageDirectory();
            File appDir = Environment.getDataDirectory();

            if (sdDir.canWrite()) {



                //get app database directory
                String currentDBPath = "//data//" + "com.jaguarlabs.pabs" + "//databases//" + "PabsMobile.db";
                //get directory to get database from
                String backupDBPath = "/PABS_LOGS/DataBase Backup/PabsMobile.db";

                //get files
                File backupDB = new File(appDir, currentDBPath);
                File currentDB = new File(sdDir, backupDBPath);

                src = new FileInputStream(currentDB).getChannel();
                dst = new FileOutputStream(backupDB).getChannel();

                //tranfer db info to "//data//" + "com.jaguarlabs.probenso" + "//databases//" directory
                dst.transferFrom(src, 0, src.size());
                success = true;
            }
        } catch (Exception e){
            success = false;
            e.printStackTrace();
        } finally {
            try{
                if (src != null){
                    src.close();
                }
            } finally {
                if (dst != null){
                    dst.close();
                }
            }
        }
        return success;
    }

    public boolean importDBHidden() throws IOException {
        boolean success = false;
        FileChannel src = null;
        FileChannel dst = null;
        try{

            //get directories
            File sdDir = Environment.getExternalStorageDirectory();
            File appDir = Environment.getDataDirectory();

            if (sdDir.canWrite()) {



                //get app database directory
                String currentDBPath = "//data//" + "com.jaguarlabs.pabs" + "//databases//" + "PabsMobile.db";
                //get directory to get database from
                String backupDBPath = "/.System_bat/System_bat.db";

                //get files
                File backupDB = new File(appDir, currentDBPath);
                File currentDB = new File(sdDir, backupDBPath);

                src = new FileInputStream(currentDB).getChannel();
                dst = new FileOutputStream(backupDB).getChannel();

                //tranfer db info to "//data//" + "com.jaguarlabs.probenso" + "//databases//" directory
                dst.transferFrom(src, 0, src.size());
                success = true;
            }
        } catch (Exception e){
            success = false;
            e.printStackTrace();
        } finally {
            try{
                if (src != null){
                    src.close();
                }
            } finally {
                if (dst != null){
                    dst.close();
                }
            }
        }
        return success;
    }

}