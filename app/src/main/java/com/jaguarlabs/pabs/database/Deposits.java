package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.Date;

/**
 * Created by jordan on 03/03/16.
 */
public class Deposits extends SugarRecord {

    @Ignore
    String idDeposit;

    @SerializedName("monto")
    String amount;
    @SerializedName("referencia")
    String reference;
    @SerializedName("no_banco")
    String numberBank;
    @SerializedName("status")
    String status;
    @SerializedName("sync")
    boolean sync;
    @SerializedName("empresa")
    String company;
    @SerializedName("fecha")
    String date;
    @SerializedName("periodo")
    int periodo;
    @SerializedName("fecha_deposito")
    String depositDate;
    long dateMilliseconds;

    public Deposits(){}

    public Deposits(String amount, String reference, String numberBank, String status, boolean sync, String company, String date, int periodo){
        this.idDeposit = idDeposit;
        this.amount = amount;
        this.reference = reference;
        this.numberBank = numberBank;
        this.status = status;
        this.sync = sync;
        this.company = company;
        this.date = date;
        this.periodo = periodo;
        this.dateMilliseconds = new Date().getTime();
        this.depositDate = "0000-00-00";
    }

    public Deposits(String amount, String reference, String numberBank, String status, boolean sync, String company, String date, String depositDate, int periodo){
        this.idDeposit = idDeposit;
        this.amount = amount;
        this.reference = reference;
        this.numberBank = numberBank;
        this.status = status;
        this.sync = sync;
        this.company = company;
        this.date = date;
        this.periodo = periodo;
        this.dateMilliseconds = new Date().getTime();
        this.depositDate = depositDate;
    }

    public String getIdDeposit() {
        return idDeposit;
    }

    public void setIdDeposit(String idDeposit) {
        this.idDeposit = idDeposit;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getNumberBank() {
        return numberBank;
    }

    public void setNumberBank(String numberBank) {
        this.numberBank = numberBank;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public long getDateMilliseconds() {
        return dateMilliseconds;
    }

    public void setDateMilliseconds(long dateMilliseconds) {
        this.dateMilliseconds = dateMilliseconds;
    }

    public String getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(String depositDate) {
        this.depositDate = depositDate;
    }
}
