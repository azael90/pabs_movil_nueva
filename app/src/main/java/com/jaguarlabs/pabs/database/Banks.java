package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Unique;

/**
 * Created by Emmanuel Rodriguez on 26/03/2019.
 */
public class Banks extends SugarRecord {

    @Unique
    @SerializedName("no_banco")
    String bankID;
    @SerializedName("nombre")
    String name;

    public Banks(){}

    public Banks(String bankID, String name){
        this.bankID = bankID;
        this.name = name;
    }

    public String getBankID() {
        return bankID;
    }

    public void setBankID(String bankID) {
        this.bankID = bankID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
