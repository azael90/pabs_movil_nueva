package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by jordan on 03/03/16.
 */
public class ViewedNotifications extends SugarRecord {

    @SerializedName("id_leida")
    int idNotification;

    public ViewedNotifications(){}

    public ViewedNotifications(int idNotification){
        this.idNotification = idNotification;
    }

    public int getIdNotification() {
        return idNotification;
    }

    public void setIdNotification(int idNotification) {
        this.idNotification = idNotification;
    }
}
