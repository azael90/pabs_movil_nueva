package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.Date;

/**
 * Created by jordan on 03/03/16.
 */
@SuppressWarnings("SpellCheckingInspection")
public class Payments extends SugarRecord {

    @SerializedName("empresa")
    String company;
    @SerializedName("latitud")
    String latitude;
    @SerializedName("longitud")
    String longitude;
    @SerializedName("status")
    String status;
    @SerializedName("no_cobrador")
    String idCollector;
    @SerializedName("monto")
    String amount;
    @SerializedName("no_cliente")
    String idClient;
    @SerializedName("fecha")
    String date;
    @SerializedName("sync")
    boolean sync; // 0 - 1
    @SerializedName("contador")
    int counter;
    @SerializedName("serie")
    String serie;
    @SerializedName("")
    String seriePayment;
    @SerializedName("folio")
    String folio;
    @SerializedName("copias")
    String copies;
    @SerializedName("tipo_bd")
    String typeBD;
    int periodo;
    long dateMilliseconds;
    String paymenrReference;
    @SerializedName("saldoPorCobro")
    String saldoPorCobro;
    @SerializedName("tipoCobro")
    String tipoCobro;



    public Payments(){}

    public Payments(String company, String latitude, String longitude, String status, String idCollector, String amount, String idClient, String date, boolean sync, String serie, String seriePayment, String folio, String copies, String typeBD, int periodo, String saldoPorCobro){
        this.company = company;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
        this.idCollector = idCollector;
        this.amount = amount;
        this.idClient = idClient;
        this.date = date;
        this.sync = sync;
        this.serie = serie;
        this.seriePayment = seriePayment;
        this.folio = folio;
        this.copies = copies;
        this.typeBD = typeBD;
        this.counter = 0;
        this.periodo = periodo;
        this.dateMilliseconds = new Date().getTime();
        this.paymenrReference = "";
        this.saldoPorCobro=saldoPorCobro;
    }

    public Payments(String company, String latitude, String longitude, String status, String idCollector, String amount, String idClient, String date, boolean sync, String serie, String seriePayment, String folio, String copies, String typeBD, int periodo, String paymentReference, String saldoPorCobro, String tipoCobro){
        this.company = company;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
        this.idCollector = idCollector;
        this.amount = amount;
        this.idClient = idClient;
        this.date = date;
        this.sync = sync;
        this.serie = serie;
        this.seriePayment = seriePayment;
        this.folio = folio;
        this.copies = copies;
        this.typeBD = typeBD;
        this.counter = 0;
        this.periodo = periodo;
        this.dateMilliseconds = new Date().getTime();
        this.paymenrReference = paymentReference;
        this.saldoPorCobro=saldoPorCobro;
        this.tipoCobro = tipoCobro;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdCollector() {
        return idCollector;
    }

    public void setIdCollector(String idCollector) {
        this.idCollector = idCollector;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getSeriePayment() {
        return seriePayment;
    }

    public void setSeriePayment(String seriePayment) {
        this.seriePayment = seriePayment;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCopies() {
        return copies;
    }

    public void setCopies(String copies) {
        this.copies = copies;
    }

    public String getTypeBD() {
        return typeBD;
    }

    public void setTypeBD(String typeBD) {
        this.typeBD = typeBD;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public long getDateMilliseconds() {
        return dateMilliseconds;
    }

    public void setDateMilliseconds(long dateMilliseconds) {
        this.dateMilliseconds = dateMilliseconds;
    }

    public String getPaymenrReference() {
        return paymenrReference;
    }

    public void setPaymenrReference(String paymenrReference) {
        this.paymenrReference = paymenrReference;
    }

    public String getSaldoPorCobro() {
        return saldoPorCobro;
    }

    public void setSaldoPorCobro(String saldoPorCobro) {
        this.saldoPorCobro = saldoPorCobro;
    }

    public String getTipoCobro() {
        return tipoCobro;
    }

    public void setTipoCobro(String tipoCobro) {
        this.tipoCobro = tipoCobro;
    }
}
