package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by Jordan Alvarez on 3/10/2017.
 */

public class CompanyAmounts extends SugarRecord {

    @SerializedName("nombre_empresa")
    private String companyName;
    @SerializedName("monto")
    private float amount;
    @SerializedName("periodo")
    private int period;

    public CompanyAmounts(){}

    public CompanyAmounts(String companyName, float amount, int period) {
        this.companyName = companyName;
        this.amount = amount;
        this.period = period;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }
}
