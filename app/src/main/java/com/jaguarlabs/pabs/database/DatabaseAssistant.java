package com.jaguarlabs.pabs.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.util.Log;

import com.google.gson.Gson;
import com.jaguarlabs.pabs.components.PreferencesToShowWallet;
import com.jaguarlabs.pabs.models.ModelClient;
import com.jaguarlabs.pabs.models.ModelCobrosRealizadosPorPeriodo;
import com.jaguarlabs.pabs.models.ModelDeposit;
import com.jaguarlabs.pabs.models.ModelPayment;
import com.jaguarlabs.pabs.models.ModelPeriod;
import com.jaguarlabs.pabs.rest.models.ModelCellPhoneNumbersRequest;
import com.jaguarlabs.pabs.rest.models.ModelCellPhoneNumbersResponse;
import com.jaguarlabs.pabs.rest.models.ModelGetCollectorsResponse;
import com.jaguarlabs.pabs.rest.models.ModelOsticketReponse;
import com.jaguarlabs.pabs.rest.models.ModelOsticketRequest;
import com.jaguarlabs.pabs.rest.models.ModelSemaforoRequest;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.Util;
import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;

import org.joda.time.LocalDate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Class that works as a database assistant.
 * Basically contains methods that performs operations in the database.
 */
@SuppressWarnings({"SpellCheckingInspection", "JavaDoc", "unused", "ConstantConditions"})
public class DatabaseAssistant {

    private static String COBRADOR = null;

    public static void setCobrador(String cobrador)
    {
        COBRADOR = cobrador;
    }
    /**
     * inserts collector's info
     * @param idCollector
     * @param numberCollector
     * @param macAddress
     * @param lastFolioMalva
     * @param lastFolioPrograma
     * @param name
     * @param message
     * @param radio
     * @param ndias
     * @param mensajeaviso
     * @param numeropagos
     * @param titulopercapita
     * @param mensajeCobrador
     * @param minutosTimerUbicaciones
     * @param mostrar_actualizar_datos
     * @param fecha_sistema_servidor
     * @param empresas_excluidas_de_captura_de_informacion
     */
    public static void insertCollectorInfo(String idCollector, String numberCollector, String macAddress, String lastFolioMalva,
                                           String lastFolioPrograma, String name, String message, String radio, String ndias, String mensajeaviso,
                                           String numeropagos, String titulopercapita, String mensajeCobrador, String minutosTimerUbicaciones,
                                           String mostrar_actualizar_datos, String fecha_sistema_servidor, String empresas_excluidas_de_captura_de_informacion)
    {
        String fechaSistema="";
        List<Collectors> Listcollector = Collectors.listAll(Collectors.class);
        if (Listcollector.size() > 0)
        {
            fechaSistema = Listcollector.get(0).getFecha_sistema_servidor();
        }
        Collectors.deleteAll(Collectors.class);
        Collectors collector = new Collectors(numberCollector, idCollector, macAddress, lastFolioMalva, lastFolioPrograma, name, message, radio, ndias,
                mensajeaviso, numeropagos, titulopercapita, mensajeCobrador, minutosTimerUbicaciones, mostrar_actualizar_datos, fechaSistema,
                empresas_excluidas_de_captura_de_informacion);
        collector.save();
        Util.Log.d("Uno");


    }

    public static void insertCollectorsBySupervisor(List<ModelGetCollectorsResponse.SingleCollector> collectors){
        try {
            for (int i = 0; i < collectors.size(); i++) {
                Collector collector = new Collector();
                collector.setName(collectors.get(i).getName());
                collector.setCode(collectors.get(i).getCode());
                collector.setCollectorNumber(collectors.get(i).getCollectorNumber());
                collector.save();
            }
        } catch ( NullPointerException e ) {
            e.printStackTrace();
        }
    }

    /**
     * inserts a client
     * @param idContract
     * @param name
     * @param firstLastName
     * @param secondLastName
     * @param locality
     * @param numberContract
     * @param numberCollector
     * @param streetToPay
     * @param numberExt
     * @param betweenStreets
     * @param neighborhood
     * @param payment
     * @param balance
     * @param actualPayment
     * @param serie
     * @param paymentOption
     * @param idBaseGroup
     * @param typeBD
     * @param latitude
     * @param longitude
     * @param status
     * @param fecha_ultimo_abono
     * @param overdueBalance
     * @param phone
     * @param lastUpdate
     * @param email
     * @param numerointerior
     * @param fechaReactiva
     */
    public static void insertClient(String idContract, String name, String firstLastName, String secondLastName, String locality,
                                    String numberContract, String numberCollector, String streetToPay, String numberExt,
                                    String betweenStreets, String neighborhood, String payment, String balance, String actualPayment,
                                    String serie, String paymentOption, String idBaseGroup, String typeBD,
                                    String latitude, String longitude, String status, String fecha_ultimo_abono, String overdueBalance, String comments,
                                    String firstPaymentDate, String phone, String contractStatus, String lastUpdate, String email, String numerointerior,
                                    String fechaReactiva){

        Clients client = new Clients(idContract, name, firstLastName, secondLastName, locality, numberContract,
                numberCollector, streetToPay, numberExt, betweenStreets, neighborhood, payment, balance,
                actualPayment, serie, paymentOption, idBaseGroup, typeBD, true, latitude, longitude, status,
                fecha_ultimo_abono, overdueBalance, comments, firstPaymentDate, phone, contractStatus, lastUpdate, email,
                numerointerior, fechaReactiva);
        client.save();
        Util.Log.ih("uno");
        //SyncedWallet wallet = new SyncedWallet(idContract, paymentDate, paymentOption, client, firstPayday, secondPayday);
        //wallet.save();
    }

    public static void insertComment(String idContract, String comment){
        List<Clients> clients = Clients.find(Clients.class, "id_contract = ?", idContract);
        if (clients.size() > 0){
            clients.get(0).comment = comment;
            clients.get(0).save();
        }
    }

    /**
     * inserts a client
     * @param idContract
     * @param name
     * @param firstLastName
     * @param secondLastName
     * @param locality
     * @param numberContract
     * @param numberCollector
     * @param streetToPay
     * @param numberExt
     * @param betweenStreets
     * @param neighborhood
     * @param payment
     * @param balance
     * @param actualPayment
     * @param serie
     * @param paymentOption
     * @param idBaseGroup
     * @param typeBD
     * @param latitude
     * @param longitude
     * @param status
     * @param lastPaymentDate
     * @param phone
     * @param lastUpdate
     * @param email
     * @param numerointerior
     * @param fechaReactiva
     */
    public static void insertClient(String idContract, String name, String firstLastName, String secondLastName, String locality,
                                    String numberContract, String numberCollector, String streetToPay, String numberExt,
                                    String betweenStreets, String neighborhood, String payment, String balance, String actualPayment,
                                    String serie, String paymentOption, String idBaseGroup, String typeBD,
                                    String latitude, String longitude, String status, String lastPaymentDate, String comments, String firstPaymentDate, String phone,
                                    String contractStatus, String lastUpdate, String email, String numerointerior, String fechaReactiva){

        Clients client = new Clients(idContract, name, firstLastName, secondLastName, locality, numberContract,
                numberCollector, streetToPay, numberExt, betweenStreets, neighborhood, payment, balance,
                actualPayment, serie, paymentOption, idBaseGroup, typeBD, true, latitude, longitude,
                status, lastPaymentDate, comments, firstPaymentDate, phone, contractStatus, lastUpdate, email,
                numerointerior, fechaReactiva);
        client.save();
        Util.Log.ih("dos");
        //SyncedWallet wallet = new SyncedWallet(idContract, paymentDate, paymentOption, client, firstPayday, secondPayday);
        //wallet.save();
    }

    public static void updateClientsInfoFromSyncedWallet(boolean updatePaydays){
        //String query = "UPDATE SYNCED_WALLET SET pay_day = i.pay_day FROM ( SELECT id_contract, pay_day FROM Clients ) i WHERE i.id_contract = SYNCED_WALLET.contract_id";

        String queryToUpdatePaydays = "UPDATE SYNCED_WALLET " +
                        "SET " +
                        "pay_day = (SELECT Clients.pay_day FROM Clients WHERE Clients.id_contract = SYNCED_WALLET.contract_id), " +
                        "first_payday_weekly = (SELECT Clients.first_payday FROM Clients WHERE Clients.id_contract = SYNCED_WALLET.contract_id), " +
                        "second_payday_weekly = (SELECT Clients.second_payday FROM Clients WHERE Clients.id_contract = SYNCED_WALLET.contract_id), " +
                        "status = (SELECT Clients.status FROM Clients WHERE Clients.id_contract = SYNCED_WALLET.contract_id), " +
                        "payment_option = (SELECT Clients.payment_option FROM Clients WHERE Clients.id_contract = SYNCED_WALLET.contract_id)";

        String queryWithoutUpdatingPaydays = "UPDATE SYNCED_WALLET " +
                "SET " +
                "status = (SELECT Clients.status FROM Clients WHERE Clients.id_contract = SYNCED_WALLET.contract_id)";

        SyncedWallet.executeQuery(updatePaydays ? queryToUpdatePaydays : queryWithoutUpdatingPaydays);

        Util.Log.ih("updating serializedclient...");

        List<Clients> clients = Clients.listAll(Clients.class);

        if (clients.size() > 0){

            String queryUpdateClient;
            String serializedClient;

            for (int i = 0; i < clients.size(); i++){

                String newPayday = "";
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    formatter.setLenient(false);
                    String lastPaymentDateString = clients.get(i).getLastPaymentDate();
                    Date lastPaymentDateFormat = formatter.parse(lastPaymentDateString);
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastPaymentDateFormat);
                    newPayday = "" + c.get(Calendar.DAY_OF_YEAR);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Util.Log.ih("no_contrato = " + clients.get(i).getNumberContract());
                Util.Log.ih("new_payday = " + newPayday);


                ModelClient client = new ModelClient(
                        clients.get(i).getIdContract(), clients.get(i).getName(), clients.get(i).getFirstLastName(), clients.get(i).getSecondLastName(), clients.get(i).getLocality(), clients.get(i).getNumberContract(),
                        clients.get(i).getNumberCollector(), clients.get(i).getStreetToPay(), clients.get(i).getNumberExt(), clients.get(i).getBetweenStreets(), clients.get(i).getNeighborhood(),
                        clients.get(i).getPayment(), clients.get(i).getBalance(), clients.get(i).getActualPayment(), clients.get(i).getSerie(), clients.get(i).getPaymentOption(), clients.get(i).getIdBaseGroup(), clients.get(i).getTypeBD(),
                        true, clients.get(i).getLatitude(), clients.get(i).getLongitude(), clients.get(i).getStatus(), clients.get(i).getLastPaymentDate(), clients.get(i).getOverdueBalance(), clients.get(i).getComment(),
                        clients.get(i).getPayDay(), clients.get(i).getFirstPayday(), clients.get(i).getSecondPayday(), clients.get(i).getFirstPaymentDate(), clients.get(i).getTelefono());
                serializedClient = new Gson().toJson(client);
                queryUpdateClient = "UPDATE SYNCED_WALLET SET new_payday = '" + newPayday + "', serialized_client = '" + serializedClient + "' WHERE contract_id = '" + clients.get(i).getIdContract() + "'";
                SyncedWallet.executeQuery(queryUpdateClient);
            }
        }

        Util.Log.ih("finish updating serializedclient");

    }

    public static void insertPaydaysIntoClients(String contractID, String paymentOption, String semanal, String quincenal1, String quincenal2, String mensual){
        //List<Clients> client = Clients.find(Clients.class, "id_contract = ?", contractID);

        //if (client.size() > 0){
            switch (paymentOption){
                case "1":
                    //semanal
                    Clients.executeQuery("UPDATE CLIENTS SET pay_day = ?, first_payday = ?, second_payday = ? WHERE id_contract = ?",
                            semanal, "", "", contractID);
                    //client.get(0).setPayDay(semanal);
                    //client.get(0).setFirstPayday("");
                    //client.get(0).setSecondPayday("");
                    //client.get(0).save();
                    break;
                case "2":
                    //quincenal
                    //client.get(0).setPayDay("");

                    if (quincenal1.equals("0")){
                        Clients.executeQuery("UPDATE CLIENTS SET pay_day = ?, second_payday = ? WHERE id_contract = ?",
                                "", quincenal2, contractID);
                        //client.get(0).setFirstPayday("0");
                        //client.get(0).setSecondPayday(quincenal2);
                    } else {
                        Clients.executeQuery("UPDATE CLIENTS SET pay_day = ?, first_payday = ? WHERE id_contract = ?",
                                "", quincenal1, contractID);
                        //client.get(0).setFirstPayday(quincenal1);
                        //client.get(0).setSecondPayday("0");
                    }

                    //client.get(0).save();
                    break;
                case "3":
                    //mensual
                    Clients.executeQuery("UPDATE CLIENTS SET pay_day = ?, first_payday = ?, second_payday = ? WHERE id_contract = ?",
                            mensual, "", "", contractID);
                    //client.get(0).setPayDay(mensual);
                    //client.get(0).setFirstPayday("");
                    //client.get(0).setSecondPayday("");
                    //client.get(0).save();
                    break;
                default:
                    //nothing here...
            }
        //}
    }

    public static void insertFirstPaymentDateIntoClients(String contractID, String firstPaymentDate){
        List<Clients> client = Clients.find(Clients.class, "id_contract = ?", contractID);

        Util.Log.ih("inserting payday...");

        if (client.size() > 0){
            client.get(0).setFirstPaymentDate(firstPaymentDate);
            client.get(0).save();
        }
    }

    public static void insertBanks(String bankID, String name){
        Banks banks = new Banks(bankID, name);
        banks.save();
        Util.Log.ih("bank saved");
    }

    public static void insertLastPaymentDate(String contractID, String lastPaymentDate){
        List<Clients> client = Clients.find(Clients.class, "id_contract = ?", contractID);

        Util.Log.ih("inserting payday...");

        if (client.size() > 0){
            client.get(0).setLastPaymentDate(lastPaymentDate);
            client.get(0).save();
        }
    }

    public static void insertNewClientsIntoSyncedWallet(){
        /*
        String querySelect = "SELECT * FROM CLIENTS " +
                "WHERE id_contract = " +
                "( " +
                "SELECT t1.id_contract " +
                "FROM CLIENTS AS t1 " +
                "WHERE NOT EXISTS (SELECT 1 FROM SYNCED_WALLET AS t2 WHERE t2.contract_id = t1.id_contract) " +
                ")";
        */

        String query = "SELECT * " +
                "FROM CLIENTS AS t1 " +
                "WHERE NOT EXISTS " +
                "( " +
                "SELECT * " +
                "FROM SYNCED_WALLET AS t2 " +
                "WHERE t1.id_contract = t2.contract_id " +
                ")";

        boolean stop = false;
        Util.Log.ih("STARTING INSERTING CONTRACTS ===============================");

        List<Clients> clients = Clients.findWithQuery(Clients.class,
                query);

        List<SyncedWallet> syncedWalletClients = SyncedWallet.listAll(SyncedWallet.class);

        if (clients.size() > 0) {

            if (syncedWalletClients.size() > 0) {

                for (int i = 0; i < clients.size(); i++) {

                    PreferencesToShowWallet preferencesToShowWallet = new PreferencesToShowWallet(ApplicationResourcesProvider.getContext());
                    long dayCounter = preferencesToShowWallet.getDayCounter();

                    ModelClient client = new ModelClient(
                            clients.get(i).getIdContract(), clients.get(i).getName(), clients.get(i).getFirstLastName(), clients.get(i).getSecondLastName(), clients.get(i).getLocality(), clients.get(i).getNumberContract(),
                            clients.get(i).getNumberCollector(), clients.get(i).getStreetToPay(), clients.get(i).getNumberExt(), clients.get(i).getBetweenStreets(), clients.get(i).getNeighborhood(),
                            clients.get(i).getPayment(), clients.get(i).getBalance(), clients.get(i).getActualPayment(), clients.get(i).getSerie(), clients.get(i).getPaymentOption(), clients.get(i).getIdBaseGroup(), clients.get(i).getTypeBD(),
                            true, clients.get(i).getLatitude(), clients.get(i).getLongitude(), clients.get(i).getStatus(), clients.get(i).getLastPaymentDate(), clients.get(i).getOverdueBalance(), clients.get(i).getComment(),
                            clients.get(i).getPayDay(), clients.get(i).getFirstPayday(), clients.get(i).getSecondPayday(), clients.get(i).getFirstPaymentDate(), clients.get(i).getTelefono());


                    String newPayday = "";
                    try {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        formatter.setLenient(false);
                        String lastPaymentDateString = clients.get(i).getLastPaymentDate();
                        Date lastPaymentDateFormat = formatter.parse(lastPaymentDateString);
                        Calendar c = Calendar.getInstance();
                        c.setTime(lastPaymentDateFormat);
                        newPayday = "" + c.get(Calendar.DAY_OF_YEAR);
                    } catch (ParseException e) {
                    }

                    SyncedWallet wallet = new SyncedWallet(
                            clients.get(i).getIdContract(),
                            clients.get(i).getPayDay(),
                            clients.get(i).getPaymentOption(),
                            clients.get(i).getFirstPayday(),
                            clients.get(i).getSecondPayday(),
                            client,
                            clients.get(i).getStatus(),
                            newPayday,
                            dayCounter);

                    wallet.save();
                }
            } else {


                //add newPayday which will be the last payment date as a day of year
                //so that contracts won't show on current wallet (won't show semaforo color)
                for (int i = 0; i < clients.size(); i++) {

                    ModelClient client = new ModelClient(
                            clients.get(i).getIdContract(), clients.get(i).getName(), clients.get(i).getFirstLastName(), clients.get(i).getSecondLastName(), clients.get(i).getLocality(), clients.get(i).getNumberContract(),
                            clients.get(i).getNumberCollector(), clients.get(i).getStreetToPay(), clients.get(i).getNumberExt(), clients.get(i).getBetweenStreets(), clients.get(i).getNeighborhood(),
                            clients.get(i).getPayment(), clients.get(i).getBalance(), clients.get(i).getActualPayment(), clients.get(i).getSerie(), clients.get(i).getPaymentOption(), clients.get(i).getIdBaseGroup(), clients.get(i).getTypeBD(),
                            true, clients.get(i).getLatitude(), clients.get(i).getLongitude(), clients.get(i).getStatus(), clients.get(i).getLastPaymentDate(), clients.get(i).getOverdueBalance(), clients.get(i).getComment(),
                            clients.get(i).getPayDay(), clients.get(i).getFirstPayday(), clients.get(i).getSecondPayday(), clients.get(i).getFirstPaymentDate(), clients.get(i).getTelefono());

                    String newPayday = "";
                    try {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        formatter.setLenient(false);
                        String lastPaymentDateString = clients.get(i).getLastPaymentDate();
                        Date lastPaymentDateFormat = formatter.parse(lastPaymentDateString);
                        Calendar c = Calendar.getInstance();
                        c.setTime(lastPaymentDateFormat);
                        newPayday = "" + c.get(Calendar.DAY_OF_YEAR);
                    } catch (ParseException e) {
                    }


                    SyncedWallet wallet = new SyncedWallet(
                            clients.get(i).getIdContract(),
                            clients.get(i).getPayDay(),
                            clients.get(i).getPaymentOption(),
                            clients.get(i).getFirstPayday(),
                            clients.get(i).getSecondPayday(),
                            client,
                            clients.get(i).getStatus(),
                            newPayday,
                            2);

                    wallet.save();
                }
            }
        }
        Util.Log.ih("FINISH INSERTING CONTRACTS ===============================");

    }

    /**
     * inserts a payment
     * @param company
     * @param latitude
     * @param longitude
     * @param status
     * @param idCollector
     * @param amount
     * @param idClient
     * @param date
     * @param serie
     * @param folio
     * @param seriePayment
     * @param typeBD
     */
    public static void insertPayment(String company, String latitude, String longitude, String status, String idCollector, String amount, String idClient, String date, String serie, String folio, String seriePayment, String typeBD, int periodo, String paymentReference, String saldoPorCobro, String tipoCobro){
        Payments payment = new Payments(company, latitude, longitude, status, idCollector, amount, idClient, date, false, serie, seriePayment, folio, "0", typeBD, periodo, paymentReference, saldoPorCobro, tipoCobro);
        payment.save();

        if ( !amount.equals("0") ) {

            Util.Log.ih("idClient = " + idClient);

            List<Clients> clientToUpdate = Clients.find(Clients.class, "number_contract = ? and serie = ?", idClient, serie);
            if (clientToUpdate.size() > 0) {

                Util.Log.ih("actualizando saldo de cliente...");

                float abonado = Float.parseFloat(clientToUpdate.get(0).getPayment());
                float balance = Float.parseFloat(clientToUpdate.get(0).getBalance());

                abonado += Float.parseFloat(amount);
                balance -= Float.parseFloat(amount);

                clientToUpdate.get(0).setPayment( "" + abonado );
                clientToUpdate.get(0).setBalance( "" + balance );
                clientToUpdate.get(0).save();

                Util.Log.ih("hecho");
            } else {
                Util.Log.ih("No se encontró el cliente para actualizar saldo");
            }
        }
    }

    public static void insertPayment(String company, String latitude, String longitude, String status, String idCollector, String amount, String idClient, String date, String serie, String folio, String seriePayment, String typeBD, int periodo, String saldoPorCobro){
        Payments payment = new Payments(company, latitude, longitude, status, idCollector, amount, idClient, date, false, serie, seriePayment, folio, "0", typeBD, periodo, saldoPorCobro);
        payment.save();

        if ( !amount.equals("0") )
        {

            Util.Log.ih("idClient = " + idClient);

            List<Clients> clientToUpdate = Clients.find(Clients.class, "number_contract = ? and serie = ?", idClient, serie);
            if (clientToUpdate.size() > 0) {

                Util.Log.ih("actualizando saldo de cliente...");

                float abonado = Float.parseFloat(clientToUpdate.get(0).getPayment());
                float balance = Float.parseFloat(clientToUpdate.get(0).getBalance());

                abonado += Float.parseFloat(amount);
                balance -= Float.parseFloat(amount);

                clientToUpdate.get(0).setPayment( "" + abonado );
                clientToUpdate.get(0).setBalance( "" + balance );
                clientToUpdate.get(0).save();

                Util.Log.ih("hecho");
            } else {
                Util.Log.ih("No se encontró el cliente para actualizar saldo");
            }
        }
    }

    /**
     * inserts a deposit
     * @param amount
     * @param date
     * @param reference
     * @param numberBank
     * @param status
     * @param company
     */
    public static void insertDeposit(String amount, String date, String reference, String numberBank, String status, String company, int periodo){
        Deposits deposit = new Deposits(amount, reference, numberBank, status, false, company, date, periodo);
        deposit.save();
    }

    public static void insertDeposit(String amount, String date, String depositDate, String reference, String numberBank, String status, String company, int periodo){
        Deposits deposit = new Deposits(amount, reference, numberBank, status, false, company, date, depositDate,periodo);
        deposit.save();
    }

    /**
     * inserts a deposit
     * @param amount
     */
    public static void insertDeposit(double amount, int periodo){
        Deposits deposit = new Deposits(Double.toString( amount), "", "", "", false, "00", "", periodo);
        deposit.save();
    }

    /**
     * inserts a deposit and set it as synced
     * @param amount
     * @param date
     * @param reference
     * @param numberBank
     * @param status
     * @param company
     */

    public static void insertDepositFromCierre(String amount, String date, String reference, String numberBank, String status, String company, int periodo){
        Deposits deposit = new Deposits(amount, reference, numberBank, status, true, company, date, periodo);
        deposit.save();
    }

    /**
     * inserts a company
     * @param idCompany
     * @param rfc
     * @param name
     * @param businessName
     * @param address
     * @param phone
     */
    public static void insertCompany(String idCompany, String rfc, String name, String businessName, String address, String phone){
        Companies company = new Companies(idCompany, name, rfc, businessName, address, phone);
        company.save();
    }

    /**
     * inserts a notification
     * @param creationDate
     * @param idNotification
     * @param message
     * @param numberCollector
     * @param status
     */
    public static void insertNotification(String creationDate, String idNotification, String message, String numberCollector, int status){
        Notifications notification = new Notifications(idNotification, creationDate, message, numberCollector, status);
        notification.save();
    }

    /**
     * inserts a viewed notification
     * @param idNotification
     */
    public static void insertViewedNotification(int idNotification){
        ViewedNotifications viewedNotification = new ViewedNotifications(idNotification);
        viewedNotification.save();
    }

    /**
     * inserts a canceled payment
     * @param amount
     * @param company
     * @param numberClient
     */
    public static void insertCanceledPaymentFromCierre(String amount, String company, String numberClient, String cancelDate, int periodo, String motivoCancelacion){
        //SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        //int periodo = efectivoPreference.getInt("periodo", 0);

        Canceled canceledpayment = new Canceled(amount, company, numberClient, false, cancelDate, periodo, motivoCancelacion);
        canceledpayment.save();
    }

    public static void insertCanceledPayment(String amount, String company, String numberClient, String cancelDate, int periodo, String motivoCancelacion){
        //SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        //int periodo = efectivoPreference.getInt("periodo", 0);

        Canceled canceledpayment = new Canceled(amount, company, numberClient, false, cancelDate, periodo, motivoCancelacion);
        canceledpayment.save();

        if ( !amount.equals("0") ) {

            Util.Log.ih("idClient = " + numberClient);

            //List<Clients> clientToUpdate = Clients.find(Clients.class, "number_contract = ?", "" + idClient);
            List<Clients> clientToUpdate = Clients.findWithQuery(Clients.class, "SELECT * FROM CLIENTS WHERE serie||number_contract = ?", numberClient);
            if (clientToUpdate.size() > 0) {

                Util.Log.ih("actualizando saldo de cliente...");

                float abonado = Float.parseFloat(clientToUpdate.get(0).getPayment());
                float balance = Float.parseFloat(clientToUpdate.get(0).getBalance());

                Util.Log.ih("amount = " + amount);
                Util.Log.ih("abonado = " + abonado);
                Util.Log.ih("balance = " + balance);

                abonado -= Float.parseFloat(amount);
                balance += Float.parseFloat(amount);

                Util.Log.ih("abonado = " + abonado);
                Util.Log.ih("balance = " + balance);

                clientToUpdate.get(0).setPayment("" + abonado);
                clientToUpdate.get(0).setBalance("" + balance);
                clientToUpdate.get(0).save();

                Util.Log.ih("hecho");
            } else {
                Util.Log.ih("No se encontró el cliente para actualizar saldo");
            }
        }
    }

    /**
     * inserts a location
     * @param date
     * @param latitude
     * @param longitude
     */
    public static void insertLocation(String date, String latitude, String longitude){
        Locations location = new Locations(date, latitude, longitude, false);
        location.save();
    }

    public static void deleteAllOsticketTopics() {
        OsticketTopics.deleteAll(OsticketTopics.class);
    }

    public static void insertOsticketTopics( String topic ) {
        OsticketTopics osticketTopics = new OsticketTopics( topic );
        osticketTopics.save();
    }

    public static String[] getAllOsticketTopics() {
        List<OsticketTopics> osticketTopics = OsticketTopics.listAll(OsticketTopics.class);

        if ( osticketTopics.size() > 0 ) {

            String[] items = new String[osticketTopics.size()];

            for ( int i = 0; i < osticketTopics.size(); i++ ) {
                items[i] = osticketTopics.get(i).getTopic();
            }

            return items;
        } else {
            return new String[]{};
        }
    }

    public static void insertOsticket(String topic, String message, Location userLocation) {
        Osticket osticket = new Osticket( topic, message, Double.toString(userLocation.getLatitude()), Double.toString(userLocation.getLongitude()) );
        osticket.save();
    }

    public static void insertOsticket(String topic, String message, String lat, String lang) {
        Osticket osticket = new Osticket( topic, message, lat, lang );
        osticket.save();
    }

    public static void updateOsticketAsSent( List<ModelOsticketReponse.OsticketTicketsAdded> result ) {

        for ( int i = 0; i < result.size(); i++ ) {
            List<Osticket> osticketPending = Osticket.find(Osticket.class, "date_milliseconds = ?", "" + result.get(i).getDateMilliseconds());

            if (osticketPending.size() > 0) {
                osticketPending.get(0).setStatus(true);
                osticketPending.get(0).save();
            }
        }
    }

    public static void updateCellPhoneNumberAsSent( List<ModelCellPhoneNumbersResponse.CellPhoneNumberAdded> result ){
        for ( int i = 0; i < result.size(); i++ ) {
            List<ClientsCellPhoneNumbers> clientsCellPhoneNumbersAdded = ClientsCellPhoneNumbers.find(ClientsCellPhoneNumbers.class, "id = ?", "" + result.get(i).getCellPhoneNumberID());

            if (clientsCellPhoneNumbersAdded.size() > 0) {
                clientsCellPhoneNumbersAdded.get(0).setSync(true);
                clientsCellPhoneNumbersAdded.get(0).save();
            }
        }
    }

    public static ArrayList<ModelOsticketRequest.OsticketTickets> getAllOsticketsPendingToSend() {
        List<Osticket> osticketPending =  Osticket.find(Osticket.class, "status = ?", "0");

        if ( osticketPending.size() > 0 ) {

            ArrayList<ModelOsticketRequest.OsticketTickets> osticketList = new ArrayList<>();

            for (Osticket osticket : osticketPending) {
                ModelOsticketRequest.OsticketTickets modelOsticket = new ModelOsticketRequest.OsticketTickets();
                modelOsticket.setTopic( osticket.getTopic() );
                modelOsticket.setMessage( osticket.getMessage() );
                modelOsticket.setDateMilliseconds( osticket.getDateMilliseconds() );
                modelOsticket.setLatitude(osticket.getLatitud());
                modelOsticket.setLongitude(osticket.getLongitud());

                osticketList.add( modelOsticket );
            }

            return osticketList;
        }

        return new ArrayList<>();
    }

    public static ArrayList<ModelCellPhoneNumbersRequest.ContractsCellPhoneNumbers> getAllCellPhoneNumbersPendingToSend() {
        List<ClientsCellPhoneNumbers> clientsCellPhoneNumbers =  ClientsCellPhoneNumbers.find(ClientsCellPhoneNumbers.class, "sync = ?", "0");

        if ( clientsCellPhoneNumbers.size() > 0 ) {

            ArrayList<ModelCellPhoneNumbersRequest.ContractsCellPhoneNumbers> cellPhoneNumberList = new ArrayList<>();

            for (ClientsCellPhoneNumbers cellPhoneNumber : clientsCellPhoneNumbers) {
                ModelCellPhoneNumbersRequest.ContractsCellPhoneNumbers modelCellPhoneNumber = new ModelCellPhoneNumbersRequest.ContractsCellPhoneNumbers();
                modelCellPhoneNumber.setCellPhoneNumberID( cellPhoneNumber.getId() );
                modelCellPhoneNumber.setContractID( cellPhoneNumber.getContractID() );
                modelCellPhoneNumber.setCellPhoneNumber( cellPhoneNumber.getCellPhoneNumber() );
                modelCellPhoneNumber.setContractSerie( cellPhoneNumber.getContractSerie() );
                modelCellPhoneNumber.setContractNumber( cellPhoneNumber.getContractNumber() );

                cellPhoneNumberList.add( modelCellPhoneNumber );
            }

            return cellPhoneNumberList;
        }

        return new ArrayList<>();
    }

    public static void insertNewPeriodFromScratchIfNeeded( int no_periodo ) {
        List<Periods> periods =  Periods.listAll(Periods.class);
        if ( periods.size() <= 0 ) {
            Periods period = new Periods( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), no_periodo );
            period.save();
        }
    }

    public static void insertNewPeriodFromScratchIfNeeded( String fechaInicio, int no_periodo) {
        List<Periods> periods =  Periods.listAll(Periods.class);
        if ( periods.size() <= 0 ) {
            Periods period = new Periods( fechaInicio, no_periodo );
            period.save();
        }
    }

    public static void insertNewPeriodIfNeeded(int no_periodo, boolean... finishAllOtherPendingPeriods) {
        List<Periods> periods =  Periods.find(Periods.class, "period_number = ?", "" + no_periodo);

        if (periods.size() <= 0) {
            Periods period = new Periods( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), no_periodo );
            period.save();

        }

        if ( finishAllOtherPendingPeriods.length > 0 ) {
            List<Periods> pendingPeriods = Periods.find(Periods.class, "period_number < ? AND status = ?", "" + no_periodo, "" + 0);

            if (pendingPeriods.size() > 0) {
                pendingPeriods.get(0).setStatus(1);
                if (pendingPeriods.get(0).getEndDate().equals("")) {
                    pendingPeriods.get(0).setEndDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                    pendingPeriods.get(0).setEndDateMilliseconds(new Date().getTime());
                }
                pendingPeriods.get(0).save();
            }
        }
    }

    public static void updateEndDatePeriodButKeepActive(int no_periodo) {
        List<Periods> periods =  Periods.find(Periods.class, "period_number = ?", "" + no_periodo);

        if ( periods.size() > 0 ) {
            String endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            periods.get(0).setEndDate( endDate );
            periods.get(0).setEndDateMilliseconds( new Date().getTime() );
            periods.get(0).save();
        }
    }

    public static void finishPeriod(int no_periodo) {
        List<Periods> periods =  Periods.find(Periods.class, "period_number = ?", "" + no_periodo);

        if ( periods.size() > 0 ) {
            periods.get(0).setStatus(1);
            if ( periods.get(0).getEndDate().equals("") ) {
                periods.get(0).setEndDate( new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) );
                periods.get(0).setEndDateMilliseconds( new Date().getTime() );
            }
            periods.get(0).save();
        }
    }

    public static void closePeriod( String no_periodo ) {
        Periods.executeQuery("UPDATE PERIODS SET status = 1 WHERE period_number = " + no_periodo);
    }

    public static boolean isThereMoreThanOnePeriodActive() {

        List<Periods> periods =  Periods.find(Periods.class, "status = ?", "0");
        //periodo estatus 0 es activo
        if (periods.size() > 0)
        {
            LocalDate today = LocalDate.now();
            LocalDate startDate = LocalDate.parse(periods.get(0).getStartDate().split(" ")[0]);
            LocalDate lastDayOfTheMonth = startDate.dayOfMonth().withMaximumValue();

            return periods.size() > 1 && today.isBefore(lastDayOfTheMonth.plusDays(6));
        }

        return false;
    }

    public static int getNumberOfPeriodsActive() {
        List<Periods> periodsActive = Periods.findWithQuery(Periods.class, "SELECT * FROM PERIODS WHERE status = ?", "0");
        return periodsActive.size();
    }

    public static Periods getOnePeriodOfAllActive( int periodCounter ) {
        List<Periods> periods = Periods.findWithQuery(Periods.class, "SELECT * FROM PERIODS WHERE status = ? ORDER BY id ASC", "0");

        if ( periods.size() > 0 ) {
            return periods.get(periodCounter - 1);
        }
        return null;
    }

    public static List<ModelPeriod> getAllPeriodsActive( ) {
        List<Periods> periods = Periods.findWithQuery(Periods.class, "SELECT * FROM PERIODS WHERE status = ? ORDER BY id ASC", "0");

        if ( periods.size() > 0 ) {

            List<ModelPeriod> periodsList = new ArrayList<>();

            for (Periods p: periods){
                ModelPeriod modelPeriod = new ModelPeriod( p.getPeriodNumber() );

                periodsList.add( modelPeriod );
            }
            return periodsList;
        }
        return new ArrayList<>();
    }

    public static boolean checkThatPeriodWasClosedOnLastDayOfMonth(int no_periodo) {


        Calendar calendar = Calendar.getInstance();

        if ( (calendar.get(Calendar.DAY_OF_MONTH)) >= 1 && (calendar.get(Calendar.DAY_OF_MONTH)) <= 5 ) {

            List<Periods> periods = Periods.findWithQuery(Periods.class, "SELECT * FROM PERIODS ORDER BY id DESC LIMIT 1");

            if (periods.size() > 0) {
                Periods period = periods.get(0);

                int dayOfMonth = Integer.parseInt(period.getStartDate().split(" ")[0].split("-")[2]);
                Util.Log.ih("dayOfMonth = " + dayOfMonth);

                boolean anyPaymentWithinCurrentPeriodMadeLastMonth = checkIfThereIsAnyPaymentWithinCurrentPeriodAndMadeLastMonth(no_periodo);

                //get number of days of startDate of period
                //and check that its almost the end of the month
                //if ( getNumberOfDaysOfMonth( Integer.parseInt( period.getStartDate().split(" ")[0].split("-")[1] ) - 1 ) != dayOfMonth
                //        && (dayOfMonth > 5) ) {
                if ( anyPaymentWithinCurrentPeriodMadeLastMonth && (dayOfMonth > 5) ) {
                    updateEndDatePeriodButKeepActive( no_periodo );
                    insertNewPeriodIfNeeded( no_periodo + 1 );
                    return true;
                }
            }
        }


        return false;
    }

    public static String checkLastThreePeriodsStatus(){
        List<Periods> periods = Periods.findWithQuery(Periods.class, "SELECT * FROM PERIODS ORDER BY id DESC LIMIT 3");
        StringBuilder mString = new StringBuilder();

        if ( periods.size() > 0 ) {

            List<ModelPeriod> periodsList = new ArrayList<>();

            for (Periods p: periods){

                mString.append("periodo: ").append(p.getPeriodNumber()).append(" status: ").append(p.getStatus()).append("\n");
            }
            return mString.toString();
        }
        return mString.toString();
    }

    private static boolean checkIfThereIsAnyPaymentWithinCurrentPeriodAndMadeLastMonth(int no_periodo) {

        Date initialDate = new Date();
        Date finalDate = new Date();

        try {
            Calendar c = Calendar.getInstance();
            int numberOfMonth = c.get(Calendar.MONTH) + 1;//from 1 - 12
            int year = c.get(Calendar.YEAR);

            //validation for new year
            if (numberOfMonth == 0) {
                numberOfMonth = 12;
                year -= 1;
            }

            int numberOfDaysOfMonth = getNumberOfDaysOfMonth(numberOfMonth - 2);

            String initialDateString = "" + year + "-" + ((numberOfMonth <= 10) ? "0"+(numberOfMonth-1) : ""+(numberOfMonth-1)) + "-01 00:00:01";
            String finalDateString = "" + year + "-" + ((numberOfMonth <= 10) ? "0"+(numberOfMonth-1) : ""+(numberOfMonth-1)) + "-" + numberOfDaysOfMonth + " 23:59:59";

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setLenient(false);
            initialDate = formatter.parse(initialDateString);
            finalDate = formatter.parse(finalDateString);

            Util.Log.ih("aaa = " + initialDate.getTime());
            Util.Log.ih("bbb = " + finalDate.getTime());
            Util.Log.ih("aaadate = " + new Date(initialDate.getTime()));
            Util.Log.ih("bbbdate = " + new Date(finalDate.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        List<Payments> payments = Payments.findWithQuery(Payments.class, "SELECT * FROM PAYMENTS WHERE periodo = ? AND date_milliseconds BETWEEN ? AND ?", ""+no_periodo, ""+initialDate.getTime(), ""+finalDate.getTime());

        return payments.size() > 0;
    }

    public static void insertCompanyAmountsByPeriodWhenMoreThanOnePeriodIsActive(String companyName, float amount, int periodo) {
        List<CompanyAmounts> companyAmounts = CompanyAmounts.find(CompanyAmounts.class, "period = ? AND company_name = ?", ""+periodo, companyName);
        if (companyAmounts.size() > 0) {
            companyAmounts.get(0).setAmount(amount);
            companyAmounts.get(0).save();
        } else {
            CompanyAmounts insertCompanyAmounts = new CompanyAmounts(companyName, amount, periodo);
            insertCompanyAmounts.save();
        }
    }

    /*public static float getCompaniesTotalCashAmountWhenMoreThanOnePeriodIsActive(int period) {
        List<CompanyAmounts> companyAmounts = CompanyAmounts.findWithQuery(CompanyAmounts.class, "SELECT * FROM COMPANY_AMOUNTS WHERE period = ?", ""+period);
        float total = 0;
        if (companyAmounts.size() > 0) {
            for (CompanyAmounts ca : companyAmounts) {
                total += ca.getAmount();
            }
        }
        return total;
    }*/

    public static float getCompaniesTotalCashAmountWhenMoreThanOnePeriodIsActive(int period) {
        List<MontosTotalesDeEfectivo> companyAmounts = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, "SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE periodo = ?", ""+period);
        float total = 0;
        if (companyAmounts.size() > 0) {
            for (MontosTotalesDeEfectivo ca : companyAmounts)
            {
                total += Float.valueOf(ca.cobros_menos_depositos);
            }
        }
        return total; //
    }


    public static float getTotalCobrosPorCompania(String idCompany)
    {
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);

        String query = "SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE empresa='"+ idCompany +"' and periodo ='"+ periodo +"'";
        List<MontosTotalesDeEfectivo> companyAmounts = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
        float total = 0;
        if (companyAmounts.size() > 0) {
            for (MontosTotalesDeEfectivo ca : companyAmounts)
            {
                total += Float.valueOf(ca.cobros_mas_cancelados);
            }
        }
        return total;
    }

    public static float getTotalCobrosMenosDepositosPorEmpresa(String idCompany)
    {
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);

        String query = "SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE empresa='"+ idCompany +"' and periodo ='"+ periodo +"'";
        List<MontosTotalesDeEfectivo> companyAmounts = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
        float total = 0;
        if (companyAmounts.size() > 0) {
            for (MontosTotalesDeEfectivo ca : companyAmounts)
            {
                total += Float.valueOf(ca.getTotal_cobros_menos_depositos());
            }
        }
        return total;
    }


    public static float getCobrosMenosDepositosPorEmpresa(String idCompany)
    {
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);

        String query = "SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE empresa='"+ idCompany +"' and periodo ='"+ periodo +"'";
        List<MontosTotalesDeEfectivo> companyAmounts = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
        float total = 0;
        if (companyAmounts.size() > 0) {
            for (MontosTotalesDeEfectivo ca : companyAmounts)
            {
                total += Float.valueOf(ca.getCobros_menos_depositos());
            }
        }
        return total;
    }

    public static float getTotalDepositosPorPeriodoYEmpresa(String idCompany)
    {
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);

        String query = "SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE empresa='"+ idCompany +"' and periodo ='"+ periodo +"'";
        List<MontosTotalesDeEfectivo> companyAmounts = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
        float total = 0;
        if (companyAmounts.size() > 0) {
            for (MontosTotalesDeEfectivo ca : companyAmounts)
            {
                total += Float.valueOf(ca.getCobros_menos_depositos());
            }
        }
        return total;
    }


    public static List<CompanyAmounts> getCompanyAmountWhenMoreThanOnePeriodIsActive(int periodo) {
        List<CompanyAmounts> companyAmounts = CompanyAmounts.find(CompanyAmounts.class, "period = ?", ""+periodo);
        if (companyAmounts.size() > 0) {
            return companyAmounts;
        }
        return new ArrayList<>();
    }

    /*public static float getTotalCompanyWhenMoreThanOnePeriodIsActive(String companyName, int period) {
        List<CompanyAmounts> companyAmounts = CompanyAmounts.find(CompanyAmounts.class, "company_name = ? AND period = ?", companyName, ""+period);
        if (companyAmounts.size() > 0) {
            return companyAmounts.get(0).getAmount();
        }
        return 0;
    }*/

    public static float getTotalCompanyWhenMoreThanOnePeriodIsActive(String idCompany, int period)
    {
        float total = 0;
        String query = "SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE empresa='"+ idCompany +"' and periodo ='"+ period +"'";
        List<MontosTotalesDeEfectivo> companyAmounts = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
        if (companyAmounts.size() > 0) {
            total = Float.valueOf(companyAmounts.get(0).getCobros_menos_depositos());
        }
        return total;
    }

    /*public static float getTotalCanceledWhenMoreThanOnePeriodIsActive(int period) {
        List<Canceled> canceled = Canceled.find(Canceled.class, "period = ?", ""+period);
        float total = 0;
        if (canceled.size() > 0) {
            for (Canceled ca : canceled) {
                total += Float.parseFloat(ca.amount);
            }
        }
        return total;
    }*/

    public static float getTotalCanceledWhenMoreThanOnePeriodIsActive(int period)
    {
        List<MontosTotalesDeEfectivo> canceled = MontosTotalesDeEfectivo.find(MontosTotalesDeEfectivo.class, "periodo = ?", ""+period);
        float total = 0;
        if (canceled.size() > 0)
        {
            for (MontosTotalesDeEfectivo ca : canceled)
            {
                total += Float.parseFloat(ca.getCancelados());
            }
        }
        return total;

    }
    public static boolean hayRegistrosConElPeriodoActual(int period, String empresa)
    {
        boolean flag = false;
        String query = "SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE empresa='"+ empresa +"' and periodo ='"+ period +"'";
        List<MontosTotalesDeEfectivo> listMontos = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
        if (listMontos.size() > 0)
        {
            flag = true;
        }

        return flag;
    }


    public static void updateTotalByCompanyWhenMoreThanOnePeriodIsActive(String companyName, float canceledAmountint, int periodo)
    {
        List<CompanyAmounts> companyAmounts = CompanyAmounts.find(CompanyAmounts.class, "company_name = ? and period = ?", companyName, ""+periodo);
        if (companyAmounts.size() > 0) {
            float currentAmount = companyAmounts.get(0).getAmount();
            currentAmount = currentAmount + canceledAmountint;
            companyAmounts.get(0).setAmount(currentAmount);
            companyAmounts.get(0).save();
        }
    }

    /**
     * gets clients filtered by string given
     * @param filter
     */
    public static List<Clients> getFilteredClients(String filter){
        List<Clients> clients = Clients.findWithQuery(Clients.class,
                "SELECT * FROM CLIENTS WHERE name||' '||first_last_name||' '||second_last_name LIKE '%"
                        + filter
                        + "%' "
                        + "OR serie||number_contract LIKE '%"
                        + filter
                        + "%' "
                        + "OR street_to_pay||' '||number_ext||' '||' '||between_streets LIKE '%"
                        + filter
                        + "%' group by id_contract, serie limit 20"
        );
        if (clients.size() > 0){
            return clients;
        }
        return null;
    }


    public static String getLastEmail()
    {
        List<Actualizados> listaCliente = Actualizados.find(Actualizados.class, null, null, null, "id DESC", "1");
        if ( listaCliente.size() > 0 ) {
            return listaCliente.get(0).getCorreo();
        }
        return "-";
    }



    /**
     * gets collectors filtered by string given
     * @param filter
     */
    public static List<Collector> getFilteredCollectors(String filter){
        List<Collector> collector = Collector.findWithQuery(Collector.class,
                "SELECT * FROM COLLECTOR " +
                        "WHERE name " +
                        "LIKE " +
                        "'%" + filter + "%' "
                        + "OR code " +
                        "LIKE " +
                        "'%" + filter + "%'"
        );
        if (collector.size() > 0){
            return collector;
        }
        return new ArrayList<>();
    }

    /**
     * get collector's info
     * @return
     *      Collectors instance
     */
    public static Collectors getCollector(){
        List<Collectors> collector = Collectors.listAll(Collectors.class);
        if (collector.size() > 0){
            return collector.get(0);
        }
        return new Collectors();
    }

    /**
     * get collector's info
     * @return
     *      Collectors instance
     */
    public static List<Collector> getCollectorsBySupervisor(){
        List<Collector> collector = Collector.listAll(Collector.class);
        if (collector.size() > 0){
            return collector;
        }
        return new ArrayList<>();
    }

    public static Clients getSingleClient(String contractID){
        List<Clients> client = Clients.find(Clients.class, "id_contract = ?", contractID);
        if (client.size() > 0){
            return client.get(0);
        }
        else {
            return null;
        }
    }

    /**
     * gets a single company
     * @param idCompany
     * @return
     */
    public static Companies getSingleCompany(String idCompany){
        List<Companies> company = Companies.find(Companies.class, "id_company = ?", idCompany);

        if (company.size() > 0){
            return company.get(0);
        }
        else {
            return getSingleCompanyFromScratch(idCompany);
        }
    }

    private static Companies getSingleCompanyFromScratch(String companyID){
        Companies company = new Companies();
        //TODO: GUADALAJARA
        //TODO: DO THE SAME FOR OTHER CITIES
        switch (BuildConfig.getTargetBranch()) {
            case GUADALAJARA:
            {
                switch (companyID) {
                    case "01":
                        company.setName("Programa");
                        company.setIdCompany("01");
                        company.setAddress("Av. Federalismo Norte 1485 Piso 1 Oficina 3 Colonia San Miguel de Mezquitan CP 44260 Guadalajara, Jalisco, México");
                        company.setPhone("36154330 y 36150217");
                        company.setRfc("RFC : PBP 140220 TN2");
                        company.setBusinessName("PROGRAMA DE BENEFICIO PABS SC DE P DE RL DE CV");
                        break;
                    case "03":
                        company.setName("PBJ");
                        company.setIdCompany("03");
                        company.setAddress("Av. Federalismo Norte 1485 Piso 1 Oficina 3 Colonia San Miguel de Mezquitan CP 44260 Guadalajara, Jalisco, México");
                        company.setPhone("36503695 y 38232155");
                        company.setRfc("RFC : PBJ 080818 EL5");
                        company.setBusinessName("PROGRAMA DE BENEFICIO JALISCO, S.A. DE C.V.");
                        break;
                    case "04":
                        company.setName("Cooperativa");
                        company.setIdCompany("04");
                        company.setAddress("Av. Federalismo Norte 1485 Piso 1 Oficina 3 Colonia San Miguel de Mezquitan CP 44260 Guadalajara, Jalisco, México");
                        company.setPhone("36503695 y 38232155");
                        company.setRfc("RFC : PBP 140220 TN2");
                        company.setBusinessName("Programa de Beneficio PABS S.C. de P. de R.L. de C.V.");
                        break;
                    case "05":
                        company.setName("PABS LATINO");
                        company.setIdCompany("05");
                        company.setAddress("WASHINGTON No. 404, COL. LA AURORA C.P.44460");
                        company.setPhone("36503695");
                        company.setRfc("RFC: AURA-781005-TL3");
                        company.setBusinessName("LATINOAMERICANA RECINTO FUNERAL");
                        break;
                    default:
                        //nothing here...
                }
                break;
            }
            case TOLUCA:
            {
                switch (companyID) {
                    case "01":
                        company.setName("PROGRAMA");
                        company.setIdCompany("01");
                        company.setAddress("FRANCISCO I. MADERO No. 428 COL. SAN BALTAZAR CAMPECHE C.P. 72550. PUEBLA PUEBLA, MÉXICO");
                        company.setPhone("2406040 y 2374560");
                        company.setRfc("RFC: PAB100615KS9");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    case "03":
                        company.setName("COOPERATIVA");
                        company.setIdCompany("03");
                        company.setAddress("FRANCISCO I. MADERO No. 428 COL. SAN BALTAZAR CAMPECHE C.P. 72550. PUEBLA PUEBLA, MÉXICO");
                        company.setPhone("2406040 y 2374560");
                        company.setRfc("RFC: PBP140408QJ7");
                        company.setBusinessName("PROGRAMA DE BENEFICIO PABS PUEBLA S.C.P. DE R.L. DE C.V.");
                        break;
                    case "05":
                        company.setName("PABS LATINO");
                        company.setIdCompany("05");
                        company.setAddress("WASHINGTON No. 404, COL. LA AURORA C.P.44460");
                        company.setPhone("36503695");
                        company.setRfc("RFC: AURA-781005-TL3");
                        company.setBusinessName("LATINOAMERICANA RECINTO FUNERAL");
                        break;
                    default:
                        //nothing here...
                }
                break;
            }
            case QUERETARO:
            {
                switch (companyID) {
                    case "01":
                        company.setName("PROGRAMA");
                        company.setIdCompany("01");
                        company.setAddress("2DA PRIVADA DE 20 DE NOVIEMBRE No. 15; 2DO PISO COL. SAN FRANCISQUITO; C.P. 76058; QUERÉTARO QUERÉTARO, MÉXICO");
                        company.setPhone("4422134884 y 4422427739");
                        company.setRfc("");
                        company.setBusinessName("Cooperativa Memory Servicios SCP de RL de C.V.");
                        break;
                    case "03":
                        company.setName("COOPERATIVA");
                        company.setIdCompany("03");
                        company.setAddress("2DA PRIVADA DE 20 DE NOVIEMBRE No. 15; 2DO PISO; COL. SAN FRANCISQUITO; C.P. 76058; QUERÉTARO QUERÉTARO, MÉXICO");
                        company.setPhone("4422134884 y 4422427739");
                        company.setRfc("");
                        company.setBusinessName("Cooperativa Memory Servicios SCP de RL de C.V.");
                        break;
                    default:
                        //nothing here...
                }
                break;
            }

            case IRAPUATO:
            {
                switch (companyID) {
                    case "01":
                        company.setName("PROGRAMA");
                        company.setIdCompany("01");
                        company.setAddress("BLVD. DIAZ ORDAZ # 1370; M2; INTERIOR 2, C.P. 36660, JARDINES DE IRAPUATO, IRAPUATO GUANAJUATO, MÉXICO");
                        company.setPhone("4626253705 y 4626248002");
                        company.setRfc("");
                        company.setBusinessName("GRUPO ASISTENCIAL MEMORY SCP DE RL DE CV.");
                        break;
                    case "03":
                        company.setName("COOPERATIVA");
                        company.setIdCompany("03");
                        company.setAddress("BLVD. DIAZ ORDAZ # 1370; M2; INTERIOR 2, C.P. 36660, JARDINES DE IRAPUATO, IRAPUATO GUANAJUATO, MÉXICO");
                        company.setPhone("4626253705 y 4626248002");
                        company.setRfc("");
                        company.setBusinessName("GRUPO ASISTENCIAL MEMORY SCP DE RL DE CV.");
                        break;
                    default:
                        //nothing here...
                }
                break;
            }


            case LEON:
            {
                switch (companyID) {
                    case "01":
                        company.setName("PROGRAMA");
                        company.setIdCompany("01");
                        company.setAddress("NICARAGUA #917, LOCAL 12, 2DO PISO, VILLA RESIDENCIAL ARBIDE, C.P. 37366, LEON, GUANAJUATO, MÉXICO");
                        company.setPhone("4777186035 y 4777795781");
                        company.setRfc("");
                        company.setBusinessName("COOPERATIVA MEMORY SERVICIOS SCP DE RL DE CV.");
                        break;
                    case "03":
                        company.setName("COOPERATIVA");
                        company.setIdCompany("03");
                        company.setAddress("NICARAGUA #917, LOCAL 12, 2DO PISO, VILLA RESIDENCIAL ARBIDE, C.P. 37366, LEON, GUANAJUATO, MÉXICO");
                        company.setPhone("4777186035 y 4777795781");
                        company.setRfc("");
                        company.setBusinessName("COOPERATIVA MEMORY SERVICIOS SCP DE RL DE CV.");
                        break;
                    default:
                        //nothing here...
                }
                break;
            }



            case SALAMANCA:
            {
                switch (companyID) {
                    case "01":
                        company.setName("PROGRAMA");
                        company.setIdCompany("01");
                        company.setAddress("JUAN ALDAMA #1008; 2DO PISO, C.P. 36860, CENTRO, SALAMANCA, GUANAJUATO, MÉXICO");
                        company.setPhone("4646482086 y 4646416958");
                        company.setRfc("");
                        company.setBusinessName("COOPERATIVA MEMORY SERVICIOS SCP de RL de C.V.");
                        break;
                    case "03":
                        company.setName("COOPERATIVA");
                        company.setIdCompany("03");
                        company.setAddress("JUAN ALDAMA #1008; 2DO PISO, C.P. 36860, CENTRO, SALAMANCA, GUANAJUATO, MÉXICO");
                        company.setPhone("4646482086 y 4646416958");
                        company.setRfc("");
                        company.setBusinessName("COOPERATIVA MEMORY SERVICIOS SCP de RL de C.V.");
                        break;
                    default:
                        //nothing here...
                }
                break;
            }

            case CELAYA:
            {
                switch (companyID) {
                    case "01":
                        company.setName("PROGRAMA");
                        company.setIdCompany("01");
                        company.setAddress("PROLONGACIÓN HIDALGO #705-A, C.P. 38040, CENTRO, CELAYA, GUANAJUATO, MÉXICO");
                        company.setPhone("4616093529 y 4616144232");
                        company.setRfc("");
                        company.setBusinessName("GRUPO ASISTENCIAL MEMORY SCP de RL de C.V.");
                        break;
                    case "03":
                        company.setName("COOPERATIVA");
                        company.setIdCompany("03");
                        company.setAddress("PROLONGACIÓN HIDALGO #705-A, C.P. 38040, CENTRO, CELAYA, GUANAJUATO, MÉXICO");
                        company.setPhone("4616093529 y 4616144232");
                        company.setRfc("");
                        company.setBusinessName("GRUPO ASISTENCIAL MEMORY SCP de RL de C.V.");
                        break;
                    default:
                        //nothing here...
                }
                break;
            }

            case PUEBLA:
            {
                switch (companyID) {
                    case "01":
                        company.setName("PROGRAMA");
                        company.setIdCompany("01");
                        company.setAddress("FRANCISCO I. MADERO No. 428 COL. SAN BALTAZAR CAMPECHE C.P. 72550. PUEBLA PUEBLA, MÉXICO");
                        company.setPhone("2406040 y 2374560");
                        company.setRfc("RFC: PAB100615KS9");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    case "03":
                        company.setName("COOPERATIVA");
                        company.setIdCompany("03");
                        company.setAddress("FRANCISCO I. MADERO No. 428 COL. SAN BALTAZAR CAMPECHE C.P. 72550. PUEBLA PUEBLA, MÉXICO");
                        company.setPhone("2406040 y 2374560");
                        company.setRfc("RFC: PBP140408QJ7");
                        company.setBusinessName("PROGRAMA DE BENEFICIO PABS PUEBLA S.C.P. DE R.L. DE C.V.");
                        break;

                    case "05":
                        company.setName("PABS LATINO");
                        company.setIdCompany("05");
                        company.setAddress("FRANCISCO I. MADERO No. 428 COL. SAN BALTAZAR CAMPECHE C.P. 72550. PUEBLA PUEBLA, MÉXICO");
                        company.setPhone("22224309967");
                        company.setRfc("");
                        company.setBusinessName("PROGRAMA DE BENEFICIO PABS PUEBLA S.C.P. DE R.L. DE C.V.");
                        break;

                    default:
                        //nothing here...
                }
                break;
            }
            case CUERNAVACA:
            {
                switch (companyID) {
                    case "01":
                        company.setName("PROGRAMA");
                        company.setIdCompany("01");
                        company.setAddress("FRANCISCO I. MADERO No. 719 COL. MIRAVAL C.P. 62270. DELEGACIÓN BENITO JUÁREZ, CUERNAVACA, MÉXICO");
                        company.setPhone("7773119299 y 7771704870");
                        company.setRfc("RFC: PAB100615KS9");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    case "04":
                        company.setName("COOPERATIVA");
                        company.setIdCompany("04");
                        company.setAddress("FRANCISCO I. MADERO No. 719 COL. MIRAVAL C.P. 62270. DELEGACIÓN BENITO JUÁREZ, CUERNAVACA, MÉXICO");
                        company.setPhone("7773119299 y 7771704870");
                        company.setRfc("RFC: PBP140408QJ7");
                        company.setBusinessName("PROGRAMA DE BENEFICIO PABS PUEBLA S.C.P. DE R.L. DE C.V.");
                        break;

                    case "05":
                        company.setName("PABS LATINO");
                        company.setIdCompany("05");
                        company.setAddress("FRANCISCO I. MADERO No. 719 COL. MIRAVAL C.P. 62270. DELEGACIÓN BENITO JUÁREZ, CUERNAVACA, MÉXICO");
                        company.setPhone("7773119299 y 7771704870");
                        company.setRfc("RFC: AURA-781005-TL3");
                        company.setBusinessName("LATINOAMERICANA RECINTO FUNERAL");
                        break;
                    default:
                        //nothing here...
                }
                break;
            }
            case NAYARIT:
            {
                switch (companyID) {
                    case "01":
                        company.setName("Programa");
                        company.setIdCompany("01");
                        company.setAddress("AV. MEXICO N° 393 NTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO");
                        company.setPhone("2121080 Y 2169102");
                        company.setRfc("");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    case "03":
                        company.setName("Apoyo");
                        company.setIdCompany("03");
                        company.setAddress("AV. MEXICO N° 393 NTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO");
                        company.setPhone("2121080 Y 2169102");
                        company.setRfc("");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    case "04":
                        company.setName("Cooperativa");
                        company.setIdCompany("04");
                        company.setAddress("AV. MEXICO N° 393 NTE CALLE NIÑOS HEROES Nº 82 JAVIER MINA Nº69 ORIENTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO");
                        company.setPhone("2121080 Y 2169102");
                        company.setRfc("RFC: CAHN 800216 7M9");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    default:
                        //nothing here...
                }
                break;
            }
            case MERIDA:
            {
                switch (companyID) {
                    case "01":
                        company.setName("Cooperativa");
                        company.setIdCompany("01");
                        company.setAddress("Calle 33 No. 503C Col. Alcala Martin CP 97000 Merida Yucatan, México.");
                        company.setPhone("9201269 y 9201268");
                        company.setRfc("RFC: PBP 140220 TN2");
                        company.setBusinessName("PROGRAMA DE BENEFICIO PABS SC DE RL DE CV");
                        break;


                    case "05":
                        company.setName("PABS LATINO");
                        company.setIdCompany("05");
                        company.setAddress("WASHINGTON No. 404, COL. LA AURORA C.P.44460");
                        company.setPhone("36503695");
                        company.setRfc("RFC: AURA-781005-TL3");
                        company.setBusinessName("LATINOAMERICANA RECINTO FUNERAL");
                        break;

                    default:
                        //nothing here...
                }
            }
            case SALTILLO:
            {
                switch (companyID) {
                    case "01":
                        company.setName("Cooperativa");
                        company.setIdCompany("01");
                        company.setAddress("BOULEVARD NAZARIO ORTIZ GARZA NO 1265  ( ESQUINA ABASOLO ) COL ALPES CP 25270 SALTILLO COAHUILA.");
                        company.setPhone("(844) 4 15 92 99  y (844) 4 14 88 36");
                        company.setRfc("");
                        company.setBusinessName("PROGRAMA DE BENEFICIO PABS SC DE RL DE CV");
                        break;


                    case "05":
                        company.setName("PABS LATINO");
                        company.setIdCompany("05");
                        company.setAddress("WASHINGTON No. 404, COL. LA AURORA C.P.44460");
                        company.setPhone("36503695");
                        company.setRfc("RFC: AURA-781005-TL3");
                        company.setBusinessName("LATINOAMERICANA RECINTO FUNERAL");
                        break;

                    default:
                        //nothing here...
                }
            }
            case CANCUN:
            {
                switch (companyID) {
                    case "01":
                        company.setName("Cooperativa");
                        company.setIdCompany("01");
                        company.setAddress("CALLE CEDRO MZ. 41 LOTE 1-08 SM. 311 CANCÚN, QUINTANA ROO");
                        company.setPhone("(998) 251.6530/31");
                        company.setRfc("");
                        company.setBusinessName("PROGRAMA DE BENEFICIO PABS SC DE RL DE CV");
                        break;


                    case "05":
                        company.setName("PABS LATINO");
                        company.setIdCompany("05");
                        company.setAddress("CALLE CEDRO MZ. 41 LOTE 1-08 SM. 311 CANCÚN, QUINTANA ROO");
                        company.setPhone("(998) 251.6530/31");
                        company.setRfc("");
                        company.setBusinessName("LATINOAMERICANA RECINTO FUNERAL");
                        break;

                    default:
                        //nothing here...
                }
            }
            case MEXICALI:
            {
                switch (companyID) {
                    case "01":
                        company.setName("Programa");
                        company.setIdCompany("01");
                        company.setAddress("Blvd Anahuac #800 Int.A, Col. Esperanza, C.P. 21140 Mexicali B.C., MÉXICO");
                        company.setPhone("561-2592 Y 556-8660");
                        company.setRfc("RFC:");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    case "03":
                        company.setName("Apoyo");
                        company.setIdCompany("03");
                        company.setAddress("Blvd Anahuac #800 Int.A, Col. Esperanza, C.P. 21140 Mexicali B.C., MÉXICO");
                        company.setPhone("561-2592 Y 556-8660");
                        company.setRfc("RFC:");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    case "04":
                        company.setName("Cooperativa");
                        company.setIdCompany("04");
                        company.setAddress("Blvd Anahuac #800 Int.A, Col. Esperanza, C.P. 21140 Mexicali B.C., MÉXICO");
                        company.setPhone("561-2592 Y 556-8660");
                        company.setRfc("RFC:");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    default:
                        //nothing here...
                }
                break;
            }





            case TORREON:
            {
                switch (companyID) {
                    case "01":
                        company.setName("Programa");
                        company.setIdCompany("01");
                        company.setAddress("CLZ. MATIAS ROMAN RIOS # 425, L-17, C.P 27000, PLAZA PABELLON HIDALGO, TORREON COAHUILA, MÉXICO");
                        company.setPhone("8712852043 Y 8712852060");
                        company.setRfc("RFC:");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    case "03":
                        company.setName("Apoyo");
                        company.setIdCompany("03");
                        company.setAddress("CLZ. MATIAS ROMAN RIOS # 425, L-17, C.P 27000, PLAZA PABELLON HIDALGO, TORREON COAHUILA, MÉXICO");
                        company.setPhone("8712852043 Y 8712852060");
                        company.setRfc("RFC:");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    case "04":
                        company.setName("Cooperativa");
                        company.setIdCompany("04");
                        company.setAddress("CLZ. MATIAS ROMAN RIOS # 425, L-17, C.P 27000, PLAZA PABELLON HIDALGO, TORREON COAHUILA, MÉXICO");
                        company.setPhone("8712852043 Y 8712852060");
                        company.setRfc("RFC:");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    default:
                        //nothing here...
                }
                break;
            }


            case MORELIA:
            {
                switch (companyID) {
                    case "01":
                        company.setName("Programa");
                        company.setIdCompany("01");
                        company.setAddress("AV. GUADALUPE VICTORIA NO. 364 COL. CENTRO C.P. 58000 MORELIA, MICHOACAN");
                        company.setPhone("443 1761069 y 3163897");
                        company.setRfc("RFC:");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    case "03":
                        company.setName("PBJ");
                        company.setIdCompany("03");
                        company.setAddress("BELLA AURORA No. 600 FRACC. JARDINES DE LA ASUNCION C.P. 58195 MORELIA, MICHOACAN");
                        company.setPhone("443 176 1069");
                        company.setRfc("RFC:");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    case "04":
                        company.setName("Cooperativa");
                        company.setIdCompany("04");
                        company.setAddress("AV. GUADALUPE VICTORIA NO. 364 COL. CENTRO C.P. 58000 MORELIA, MICHOACAN");
                        company.setPhone("443 1761069 y 3163897");
                        company.setRfc("RFC:");
                        company.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                        break;
                    default:
                        //nothing here...
                }
                break;
            }
        }
        return company;
    }

    private static List<Companies> getCompaniesFromScratchForSpinner(){
        List<Companies> companies = new ArrayList<>();

        //TODO: GUADALAJARA
        //TODO: DO THE SAME FOR OTHER CITIES

        switch (BuildConfig.getTargetBranch())
        {
            case GUADALAJARA:
                /*Companies company2 = new Companies();
                company2.setName("PBJ");
                company2.setIdCompany("03");
                company2.setAddress("Av. Federalismo Norte 1485 Piso 1 Oficina 3 Colonia San Miguel de Mezquitan CP 44260 Guadalajara, Jalisco, México");
                company2.setPhone("36503695 y 38232155");
                company2.setRfc("RFC : PBJ 080818 EL5");
                company2.setBusinessName("PROGRAMA DE BENEFICIO JALISCO, S.A. DE C.V.");*/
                Companies company3 = new Companies();
                company3.setName("Cooperativa");
                company3.setIdCompany("04");
                company3.setAddress("Av. Federalismo Norte 1485 Piso 1 Oficina 3 Colonia San Miguel de Mezquitan CP 44260 Guadalajara, Jalisco, México");
                company3.setPhone("36503695 y 38232155");
                company3.setRfc("RFC : PBP 140220 TN2");
                company3.setBusinessName("Programa de Beneficio PABS S.C. de P. de R.L. de C.V.");

                //companies.add(company2);
                companies.add(company3);

                if (COBRADOR != null && COBRADOR.equals("35148"))
                {
                    Companies programa = new Companies();
                    programa.setName("Programa");
                    programa.setIdCompany("01");
                    programa.setAddress("Av. Federalismo Norte 1485 Piso 1 Oficina 3 Colonia San Miguel de Mezquitan CP 44260 Guadalajara, Jalisco, México");
                    programa.setPhone("36154330 y 36150217");
                    programa.setRfc("RFC : PBP 140220 TN2");
                    programa.setBusinessName("PROGRAMA DE BENEFICIO PABS SC DE P DE RL DE CV");

                    companies.add(programa);
                }
                break;
            case TOLUCA:
                Companies company1Tol = new Companies();
                company1Tol.setName("Programa");
                company1Tol.setIdCompany("01");
                company1Tol.setAddress("PASEO TOYOCAN # 101, COL. SANTA MARIA DE LAS ROSAS, TOLUCA, EDO D MEX");
                company1Tol.setPhone("722 219 5658 Y 722 219 5632");
                company1Tol.setRfc("RFC PBS 130731 5A9");
                company1Tol.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL TOLUCA SA DE CV");
                Companies company3Tol = new Companies();
                company3Tol.setName("Cooperativa");
                company3Tol.setIdCompany("03");
                company3Tol.setAddress("PASEO TOYOCAN # 101, COL. SANTA MARIA DE LAS ROSAS, TOLUCA, EDO D MEX");
                company3Tol.setPhone("722 219 5658 Y 722 219 5632");
                company3Tol.setRfc("RFC: PBP 140324 Q37");
                company3Tol.setBusinessName("PROGRAMA DE BENEFICIO PABS TOLUCA SC DE P DE RL DE CV");

                Companies company5Tol = new Companies();
                company5Tol.setName("PABS LATINO");
                company5Tol.setIdCompany("05");
                company5Tol.setAddress("WASHINGTON No. 404, COL. LA AURORA C.P.44460");
                company5Tol.setPhone("36503695");
                company5Tol.setRfc("RFC: AURA-781005-TL3");
                company5Tol.setBusinessName("LATINOAMERICANA RECINTO FUNERAL");

                companies.add(company1Tol);
                companies.add(company3Tol);
                companies.add(company5Tol);
                break;
            case QUERETARO:

                Companies company1Qro = new Companies();
                company1Qro.setName("Programa");
                company1Qro.setIdCompany("01");
                company1Qro.setAddress("2DA PRIVADA DE 20 DE NOVIEMBRE No. 15; 2DO PISO COL. SAN FRANCISQUITO; C.P. 76058; QUERÉTARO QUERÉTARO, MÉXICO");
                company1Qro.setPhone("4422134884 y 4422427739");
                company1Qro.setRfc("");
                company1Qro.setBusinessName("Cooperativa Memory Servicios SCP de RL de C.V.");
                Companies company3Qro = new Companies();
                company3Qro.setName("Cooperativa");
                company3Qro.setIdCompany("03");
                company3Qro.setAddress("2DA PRIVADA DE 20 DE NOVIEMBRE No. 15; 2DO PISO COL. SAN FRANCISQUITO; C.P. 76058; QUERÉTARO QUERÉTARO, MÉXICO");
                company3Qro.setPhone("4422134884 y 4422427739");
                company3Qro.setRfc("");
                company3Qro.setBusinessName("Cooperativa Memory Servicios SCP de RL de C.V.");

                companies.add(company1Qro);
                companies.add(company3Qro);
                break;


            case IRAPUATO:

                Companies company1Irapuato = new Companies();
                company1Irapuato.setName("Programa");
                company1Irapuato.setIdCompany("01");
                company1Irapuato.setAddress("BLVD. DIAZ ORDAZ # 1370; M2; INTERIOR 2, C.P. 36660, JARDINES DE IRAPUATO, IRAPUATO GUANAJUATO, MÉXICO");
                company1Irapuato.setPhone("4626253705 y 4626248002");
                company1Irapuato.setRfc("");
                company1Irapuato.setBusinessName("GRUPO ASISTENCIAL MEMORY SCP DE RL DE CV.");

                Companies company3Irapuato = new Companies();
                company3Irapuato.setName("Cooperativa");
                company3Irapuato.setIdCompany("03");
                company3Irapuato.setAddress("BLVD. DIAZ ORDAZ # 1370; M2; INTERIOR 2, C.P. 36660, JARDINES DE IRAPUATO, IRAPUATO GUANAJUATO, MÉXICO");
                company3Irapuato.setPhone("4626253705 y 4626248002");
                company3Irapuato.setRfc("");
                company3Irapuato.setBusinessName("GRUPO ASISTENCIAL MEMORY SCP DE RL DE CV.");



                companies.add(company1Irapuato);
                companies.add(company3Irapuato);
                break;

            case LEON:

                Companies companyLeon1 = new Companies();
                companyLeon1.setName("Programa");
                companyLeon1.setIdCompany("01");
                companyLeon1.setAddress("NICARAGUA #917, LOCAL 12, 2DO NIVEL, VILLA RESIDENCIAL ARBIDE, C.P. 37366, LEON, GUANAJUATO, MÉXICO");
                companyLeon1.setPhone("4777186035 y 4777795781");
                companyLeon1.setRfc("");
                companyLeon1.setBusinessName("COOPERATIVA MEMORY SERVICIOS SCP DE RL DE CV.");

                Companies companyLeon2 = new Companies();
                companyLeon2.setName("Cooperativa");
                companyLeon2.setIdCompany("03");
                companyLeon2.setAddress("NICARAGUA #917, LOCAL 12, 2DO NIVEL, VILLA RESIDENCIAL ARBIDE, C.P. 37366, LEON, GUANAJUATO, MÉXICO");
                companyLeon2.setPhone("4777186035 y 4777795781");
                companyLeon2.setRfc("");
                companyLeon2.setBusinessName("COOPERATIVA MEMORY SERVICIOS SCP DE RL DE CV.");



                companies.add(companyLeon1);
                companies.add(companyLeon2);
                break;


            case SALAMANCA:

                Companies companySalamanca = new Companies();
                companySalamanca.setName("Programa");
                companySalamanca.setIdCompany("01");
                companySalamanca.setAddress("JUAN ALDAMA #1008; 2DO PISO, C.P. 36860, CENTRO, SALAMANCA, GUANAJUATO, MÉXICO");
                companySalamanca.setPhone("4646482086 y 4646416958");
                companySalamanca.setRfc("");
                companySalamanca.setBusinessName("COOPERATIVA MEMORY SERVICIOS SCP de RL de C.V.");

                Companies companySalamanca2 = new Companies();
                companySalamanca2.setName("Cooperativa");
                companySalamanca2.setIdCompany("03");
                companySalamanca2.setAddress("JUAN ALDAMA #1008; 2DO PISO, C.P. 36860, CENTRO, SALAMANCA, GUANAJUATO, MÉXICO");
                companySalamanca2.setPhone("4646482086 y 4646416958");
                companySalamanca2.setRfc("");
                companySalamanca2.setBusinessName("COOPERATIVA MEMORY SERVICIOS SCP de RL de C.V.");
                companies.add(companySalamanca);
                companies.add(companySalamanca2);
                break;

            case CELAYA:

                /*
                DatabaseAssistant.insertCompany(
						"03",
						"",
						"Cooperativa",
						"GRUPO ASISTENCIAL MEMORY SCP de RL de C.V.",
						"PROLONGACIÓN HIDALGO #705-A, C.P. 38040, CENTRO, CELAYA, GUANAJUATO, MÉXICO",
						"4616158702 - 4616158703");
                 */
                Companies companyCelaya = new Companies();
                companyCelaya.setName("Programa");
                companyCelaya.setIdCompany("01");
                companyCelaya.setAddress("PROLONGACIÓN HIDALGO #705-A, C.P. 38040, CENTRO, CELAYA, GUANAJUATO, MÉXICO");
                companyCelaya.setPhone("4616093529 y 4616144232");
                companyCelaya.setRfc("");
                companyCelaya.setBusinessName("GRUPO ASISTENCIAL MEMORY SCP de RL de C.V.");

                Companies companyCelaya2 = new Companies();
                companyCelaya2.setName("Cooperativa");
                companyCelaya2.setIdCompany("03");
                companyCelaya2.setAddress("PROLONGACIÓN HIDALGO #705-A, C.P. 38040, CENTRO, CELAYA, GUANAJUATO, MÉXICO");
                companyCelaya2.setPhone("4616093529 y 4616144232");
                companyCelaya2.setRfc("");
                companyCelaya2.setBusinessName("GRUPO ASISTENCIAL MEMORY SCP de RL de C.V.");

                companies.add(companyCelaya);
                companies.add(companyCelaya2);
                break;


            case PUEBLA:
                Companies company1Pue = new Companies();
                company1Pue.setName("PROGRAMA");
                company1Pue.setIdCompany("01");
                company1Pue.setAddress("FRANCISCO I. MADERO No. 428 COL. SAN BALTAZAR CAMPECHE C.P. 72550. PUEBLA PUEBLA, MÉXICO");
                company1Pue.setPhone("2406040 y 2374560");
                company1Pue.setRfc("RFC: PAB100615KS9");
                company1Pue.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                Companies company3Pue = new Companies();
                company3Pue.setName("COOPERATIVA");
                company3Pue.setIdCompany("03");
                company3Pue.setAddress("FRANCISCO I. MADERO No. 428 COL. SAN BALTAZAR CAMPECHE C.P. 72550. PUEBLA PUEBLA, MÉXICO");
                company3Pue.setPhone("2406040 y 2374560");
                company3Pue.setRfc("RFC: PBP140408QJ7");
                company3Pue.setBusinessName("PROGRAMA DE BENEFICIO PABS PUEBLA S.C.P. DE R.L. DE C.V.");

                Companies company5Pue = new Companies();
                company5Pue.setName("PABS LATINO");
                company5Pue.setIdCompany("05");
                company5Pue.setAddress("FRANCISCO I. MADERO No. 428 COL. SAN BALTAZAR CAMPECHE C.P. 72550. PUEBLA PUEBLA, MÉXICO");
                company5Pue.setPhone("22224309967");
                company5Pue.setRfc("");
                company5Pue.setBusinessName("PROGRAMA DE BENEFICIO PABS PUEBLA S.C.P. DE R.L. DE C.V.");

                companies.add(company1Pue);
                companies.add(company3Pue);
                companies.add(company5Pue);
                break;
            case CUERNAVACA:
                Companies company1Cue = new Companies();
                company1Cue.setName("PROGRAMA");
                company1Cue.setIdCompany("01");
                company1Cue.setAddress("FRANCISCO I. MADERO No. 719 COL. MIRAVAL C.P. 62270. DELEGACIÓN BENITO JUÁREZ, CUERNAVACA, MÉXICO");
                company1Cue.setPhone("7773119299 y 7771704870");
                company1Cue.setRfc("RFC: PAB100615KS9");
                company1Cue.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                Companies company3Cue = new Companies();
                company3Cue.setName("COOPERATIVA");
                company3Cue.setIdCompany("04");
                company3Cue.setAddress("FRANCISCO I. MADERO No. 719 COL. MIRAVAL C.P. 62270. DELEGACIÓN BENITO JUÁREZ, CUERNAVACA, MÉXICO");
                company3Cue.setPhone("7773119299 y 7771704870");
                company3Cue.setRfc("RFC: PBP140408QJ7");
                company3Cue.setBusinessName("PROGRAMA DE BENEFICIO PABS PUEBLA S.C.P. DE R.L. DE C.V.");

                Companies company5Cue = new Companies();
                company5Cue.setName("PABS LATINO");
                company5Cue.setIdCompany("05");
                company5Cue.setAddress("FRANCISCO I. MADERO No. 719 COL. MIRAVAL C.P. 62270. DELEGACIÓN BENITO JUÁREZ, CUERNAVACA, MÉXICO");
                company5Cue.setPhone("7773119299 y 7771704870");
                company5Cue.setRfc("RFC: AURA-781005-TL3");
                company5Cue.setBusinessName("LATINOAMERICANA RECINTO FUNERAL");


                companies.add(company1Cue);
                companies.add(company3Cue);
                companies.add(company5Cue);
                break;
            case NAYARIT:
                Companies company1Nay = new Companies();
                company1Nay.setName("Programa");
                company1Nay.setIdCompany("01");
                company1Nay.setAddress("AV. MEXICO N° 393 NTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO");
                company1Nay.setPhone("2121080 Y 2169102");
                company1Nay.setRfc("");
                company1Nay.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                Companies company2Nay = new Companies();
                company2Nay.setName("Apoyo");
                company2Nay.setIdCompany("03");
                company2Nay.setAddress("AV. MEXICO N° 393 NTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO");
                company2Nay.setPhone("2121080 Y 2169102");
                company2Nay.setRfc("");
                company2Nay.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                Companies company4Nay = new Companies();
                company4Nay.setName("Cooperativa");
                company4Nay.setIdCompany("04");
                company4Nay.setAddress("AV. MEXICO N° 393 NTE CALLE NIÑOS HEROES Nº 82 JAVIER MINA Nº69 ORIENTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO");
                company4Nay.setPhone("2121080 Y 2169102");
                company4Nay.setRfc("RFC: CAHN 800216 7M9");
                company4Nay.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");


                companies.add(company1Nay);
                companies.add(company2Nay);
                companies.add(company4Nay);
                break;
            case MERIDA:
                Companies company1Mer = new Companies();
                company1Mer.setName("Cooperativa");
                company1Mer.setIdCompany("01");
                company1Mer.setAddress("Calle 33 No. 503C Col. Alcala Martin CP 97000 Merida Yucatan, México.");
                company1Mer.setPhone("9201269 y 9201268");
                company1Mer.setRfc("RFC: PBP 140220 TN2");
                company1Mer.setBusinessName("PROGRAMA DE BENEFICIO PABS SC DE RL DE CV");


                Companies company5Mer = new Companies();
                company5Mer.setName("PABS LATINO");
                company5Mer.setIdCompany("05");
                company5Mer.setAddress("WASHINGTON No. 404, COL. LA AURORA C.P.44460");
                company5Mer.setPhone("36503695");
                company5Mer.setRfc("RFC: AURA-781005-TL3");
                company5Mer.setBusinessName("LATINOAMERICANA RECINTO FUNERAL");

                companies.add(company1Mer);
                companies.add(company5Mer);

                break;
            case SALTILLO:
                Companies company1Salt = new Companies();
                company1Salt.setName("Cooperativa");
                company1Salt.setIdCompany("01");
                company1Salt.setAddress("BOULEVARD NAZARIO ORTIZ GARZA NO 1265  ( ESQUINA ABASOLO ) COL ALPES CP 25270 SALTILLO COAHUILA.");
                company1Salt.setPhone("(844) 4 15 92 99  y (844) 4 14 88 36");
                company1Salt.setRfc("");
                company1Salt.setBusinessName("PROGRAMA DE BENEFICIO PABS SC DE RL DE CV");


                Companies company5Salt = new Companies();
                company5Salt.setName("PABS LATINO");
                company5Salt.setIdCompany("05");
                company5Salt.setAddress("WASHINGTON No. 404, COL. LA AURORA C.P.44460");
                company5Salt.setPhone("36503695");
                company5Salt.setRfc("RFC: AURA-781005-TL3");
                company5Salt.setBusinessName("LATINOAMERICANA RECINTO FUNERAL");


                companies.add(company1Salt);
                companies.add(company5Salt);
                break;
            case CANCUN:
                Companies company1SCancun = new Companies();
                company1SCancun.setName("Cooperativa");
                company1SCancun.setIdCompany("01");
                company1SCancun.setAddress("CALLE CEDRO MZ. 41 LOTE 1-08 SM. 311 CANCÚN, QUINTANA ROO");
                company1SCancun.setPhone("(998) 251.6530/31");
                company1SCancun.setRfc("");
                company1SCancun.setBusinessName("PROGRAMA DE BENEFICIO PABS SC DE RL DE CV");


                Companies company5SCancun = new Companies();
                company5SCancun.setName("PABS LATINO");
                company5SCancun.setIdCompany("05");
                company5SCancun.setAddress("CALLE CEDRO MZ. 41 LOTE 1-08 SM. 311 CANCÚN, QUINTANA ROO");
                company5SCancun.setPhone("(998) 251.6530/31");
                company5SCancun.setRfc("");
                company5SCancun.setBusinessName("LATINOAMERICANA RECINTO FUNERAL");

                companies.add(company1SCancun);
                companies.add(company5SCancun);
                break;
            case MEXICALI:
                Companies company1Mxl = new Companies();
                company1Mxl.setName("Programa");
                company1Mxl.setIdCompany("01");
                company1Mxl.setAddress("Blvd Anahuac #800 Int.A, Col. Esperanza, C.P. 21140 Mexicali B.C., MÉXICO");
                company1Mxl.setPhone("561-2592 Y 556-8660");
                company1Mxl.setRfc("");
                company1Mxl.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                Companies company2Mxl = new Companies();
                company2Mxl.setName("Apoyo");
                company2Mxl.setIdCompany("03");
                company2Mxl.setAddress("Blvd Anahuac #800 Int.A, Col. Esperanza, C.P. 21140 Mexicali B.C., MÉXICO");
                company2Mxl.setPhone("561-2592 Y 556-8660");
                company2Mxl.setRfc("");
                company2Mxl.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                Companies company4Mxl = new Companies();
                company4Mxl.setName("Cooperativa");
                company4Mxl.setIdCompany("04");
                company4Mxl.setAddress("Blvd Anahuac #800 Int.A, Col. Esperanza, C.P. 21140 Mexicali B.C., MÉXICO");
                company4Mxl.setPhone("561-2592 Y 556-8660");
                company4Mxl.setRfc("");
                company4Mxl.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");

                companies.add(company1Mxl);
                companies.add(company2Mxl);
                companies.add(company4Mxl);
                break;



            case TORREON:
                Companies companyTORREON1 = new Companies();
                companyTORREON1.setName("Programa");
                companyTORREON1.setIdCompany("01");
                companyTORREON1.setAddress("CLZ. MATIAS ROMAN RIOS # 425, L-17, C.P 27000, PLAZA PABELLON HIDALGO, TORREON COAHUILA, MEXICO");
                companyTORREON1.setPhone("8712852043 Y 8712852060");
                companyTORREON1.setRfc("");
                companyTORREON1.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");

                Companies companyTORREON3 = new Companies();
                companyTORREON3.setName("Apoyo");
                companyTORREON3.setIdCompany("03");
                companyTORREON3.setAddress("CLZ. MATIAS ROMAN RIOS # 425, L-17, C.P 27000, PLAZA PABELLON HIDALGO, TORREON COAHUILA, MEXICO");
                companyTORREON3.setPhone("8712852043 Y 8712852060");
                companyTORREON3.setRfc("");
                companyTORREON3.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");

                Companies companyTORREON4 = new Companies();
                companyTORREON4.setName("Cooperativa");
                companyTORREON4.setIdCompany("04");
                companyTORREON4.setAddress("CLZ. MATIAS ROMAN RIOS # 425, L-17, C.P 27000, PLAZA PABELLON HIDALGO, TORREON COAHUILA, MEXICO");
                companyTORREON4.setPhone("8712852043 Y 8712852060");
                companyTORREON4.setRfc("");
                companyTORREON4.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");

                companies.add(companyTORREON1);
                companies.add(companyTORREON3);
                companies.add(companyTORREON4);
                break;


            case MORELIA:
                Companies company1Mor = new Companies();
                company1Mor.setName("Programa");
                company1Mor.setIdCompany("01");
                company1Mor.setAddress("AV. GUADALUPE VICTORIA NO. 364 COL. CENTRO C.P. 58000 MORELIA, MICHOACAN");
                company1Mor.setPhone("443 1761069 y 3163897");
                company1Mor.setRfc("");
                company1Mor.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                Companies company2Mor = new Companies();
                company2Mor.setName("PBJ");
                company2Mor.setIdCompany("03");
                company2Mor.setAddress("BELLA AURORA No. 600 FRACC. JARDINES DE LA ASUNCION C.P. 58195 MORELIA, MICHOACAN");
                company2Mor.setPhone("443 176 1069");
                company2Mor.setRfc("");
                company2Mor.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");
                Companies company4Mor = new Companies();
                company4Mor.setName("Cooperativa");
                company4Mor.setIdCompany("04");
                company4Mor.setAddress("AV. GUADALUPE VICTORIA NO. 364 COL. CENTRO C.P. 58000 MORELIA, MICHOACAN");
                company4Mor.setPhone("443 1761069 y 3163897");
                company4Mor.setRfc("");
                company4Mor.setBusinessName("PROGRAMA DE APOYO DE BENEFICIO SOCIAL");

                companies.add(company1Mor);
                companies.add(company2Mor);
                companies.add(company4Mor);
                break;
        }

        return companies;
    }

    /**
     * gets all companies info
     * @return
     */
    public static List<Companies> getCompanies(){
        //only show one company
        List<Companies> companies = Companies.find(Companies.class, "1", null, null, "id_company", null);
        if (companies.size() > 0){
            return companies;
        }
        return new ArrayList<>();
    }

    public static List<Companies> getCompaniesForSpinner(boolean... getAll)
    {
        List<Companies> companies;
        if (BuildConfig.getTargetBranch() == BuildConfig.Branches.GUADALAJARA)
        {
            if (getAll.length > 0 && getAll[0]){
                Util.Log.ih(" getAll[0] = " + getAll[0]);
                companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '03' OR id_company = '04'");
            } else {
                if (COBRADOR != null && COBRADOR.equals("35148"))
                    companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '04'");
                else
                    companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '04'");
            }
        }
        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.TOLUCA) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '03' OR id_company = '05'");
        }
        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.QUERETARO) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '03'");
        }


        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.IRAPUATO) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '03'");
        }
        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.LEON) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '03'");
        }

        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.SALAMANCA) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '03'");
        }

        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.CELAYA) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '03'");
        }

        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.PUEBLA) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '03' OR id_company = '05'");
        }
        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.CUERNAVACA) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '04' OR id_company = '05'");
        }
        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.MERIDA) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '05'");
        }
        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.SALTILLO) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '05'");
        }
        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.CANCUN) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '05'");
        }
        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.NAYARIT) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '03' OR id_company = '04'");
        }
        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.MEXICALI) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '03' OR id_company = '04'");
        }
        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.TORREON) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '03' OR id_company = '04'");
        }
        else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.MORELIA) {
            companies = Companies.findWithQuery(Companies.class, "SELECT * FROM COMPANIES WHERE id_company = '01' OR id_company = '03' OR id_company = '04'");
        }
        else {
            companies = Companies.listAll(Companies.class);
        }
        Companies selectCompany = new Companies();
        selectCompany.setName("Favor de seleccionar una empresa");
        selectCompany.setAddress("");
        selectCompany.setBusinessName("");
        selectCompany.setIdCompany("00");

        if (companies.size() > 0){
            companies.add(0, selectCompany);
            return companies;
        }
        else {
            return getCompaniesFromScratchForSpinner();
        }
    }

    /**
     * gets all notifications
     * @return
     */
    public static List<Notifications> getNotifications(){
        List<Notifications> notifications = Notifications.listAll(Notifications.class);
        if (notifications.size() > 0){
            return notifications;
        }
        return null;
    }

    /**
     * gets all viewed notifications
     * @return
     */
    public static String getViewedNotifications(){
        String notificationsId = "[";

        List<ViewedNotifications> viewedNotifications = ViewedNotifications.listAll(ViewedNotifications.class);

        if (viewedNotifications.size() > 0){

            notificationsId += viewedNotifications.get(0).getIdNotification();

            for (int i = 1; i < viewedNotifications.size(); i++){
                notificationsId += "," + viewedNotifications.get(i).getIdNotification();
            }

            notificationsId += "]";

            return notificationsId;
        }
        return "";
    }

    /**
     * gets all locations
     * @return
     */
    public static List<Locations> getLocations(){
        List<Locations> locations = Locations.listAll(Locations.class);
        if (locations.size() > 0){
            return locations;
        }
        return new ArrayList<>();
    }

    /**
     * gets all clients
     * @return
     */
    public static List<Clients> getAllClients(){
        //List<Clients> clients = Clients.listAll(Clients.class);
        List<Clients> clients = Clients.find(Clients.class, null, null, null, "id_contract", "500");
        //List<Clients> clients = Clients.listAll(Clients.class);
        if (clients.size() > 0){
            return clients;
        }
        return new ArrayList<>();
    }

    public static List<SyncedWallet> getAllClientsFromSyncedWallet(){
        //List<Clients> clients = Clients.listAll(Clients.class);
        //List<Clients> clients = SyncedWallet.list(Clients.class, null, null, null, "id_contract", "500");
        List<SyncedWallet> clients = SyncedWallet.listAll(SyncedWallet.class);
        if (clients.size() > 0){
            //deserialize client info
            Util.Log.ih("size " + clients.size());
            for (int i = 0; i < clients.size(); i++) {
                Util.Log.ih("deserializing " + i);
                clients.get(i).deserializeClient();
            }

            return clients;
        }
        return new ArrayList<>();
    }

    public static List<SyncedWallet> getClientsFromSyncedWallet()
    {
                PreferencesToShowWallet preferencesToShowWallet = new PreferencesToShowWallet(ApplicationResourcesProvider.getContext());
                Calendar calendar = Calendar.getInstance();

                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                int year = calendar.get(Calendar.YEAR);


                //increase day counter only if it's another day
                int dayOfYearPreferencesFile = preferencesToShowWallet.getDayOfYear();
                int yearPreferencesFile = preferencesToShowWallet.getYear();


                if (preferencesToShowWallet.lastSynchronizationToResetContractsPerWeek()) {
                    resetContractsPerWeek();
                }


                if (dayOfYear != dayOfYearPreferencesFile) {

                    if (dayOfYearPreferencesFile != 0 && yearPreferencesFile != 0) {

                        if (!(year == yearPreferencesFile && dayOfYear < dayOfYearPreferencesFile)) {
                            Util.Log.ih("dayOfYear != dayOfYearPreferencesFile");
                            //
                            int counter = 0;
                            int aux = dayOfYearPreferencesFile;
                            for (; ; counter++) {
                                if (aux == dayOfYear) {
                                    break;
                                }
                                if (aux > checkLeapYear()) {
                                    aux = 0;
                                }
                                aux++;
                            }
                            Util.Log.ih("counter = " + counter);
                            if (counter > 1) {
                                setContractsToShowAfterFewDaysWithoutCollecting(preferencesToShowWallet, counter - 1);
                                if (preferencesToShowWallet.lastSynchronizationToResetContractsPerWeek()) {
                                    resetContractsPerWeek();
                                }
                            }
                        }
                    }
                }


                dayOfYearPreferencesFile = preferencesToShowWallet.getDayOfYear();

                if (dayOfYear != dayOfYearPreferencesFile) {
                    preferencesToShowWallet.saveDayOfYear(dayOfYear);
                    preferencesToShowWallet.saveYear(year);
                    preferencesToShowWallet.saveDayOfWeek(dayOfWeek);
                    preferencesToShowWallet.saveDayOfMonth(dayOfMonth);
                    preferencesToShowWallet.saveMaximunDayMonth(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                    preferencesToShowWallet.saveDayCounter();
                }
                long dayCounter = preferencesToShowWallet.getDayCounter();
                changeVisitedOrPaidStatusFromContracts(dayOfWeek, dayOfMonth, dayOfYear);



                //contracts payday analysis that changed their first payment date
                //show as grey with black fpa
                List<SyncedWallet> clientsWithNewFirstPaymentDate = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE " +
                                "show_again = 1 " +
                                "AND status == 1");
                for (int i = 0; i < clientsWithNewFirstPaymentDate.size(); i++) {
                    try {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        formatter.setLenient(false);

                        //get milliseconds of first payment date
                        String firstPaymentDateString = clientsWithNewFirstPaymentDate.get(i).deserializeClient().getClient().getFirstPaymentDate();
                        Date firstPaymentDateDateFormat = formatter.parse(firstPaymentDateString);
                        long firstPaymentDateMilliseconds = firstPaymentDateDateFormat.getTime();

                        //get millisecond of today date
                        long date = new Date().getTime();

                        long difference = firstPaymentDateMilliseconds - date;
                        boolean firstPaymentDateIsGreater = difference < 0;

                        if (!firstPaymentDateIsGreater) {
                            clientsWithNewFirstPaymentDate.get(i).setVisitCounter(0);
                            clientsWithNewFirstPaymentDate.get(i).setPaymentMade(false);
                            clientsWithNewFirstPaymentDate.get(i).setPaymentMadeBefore(false);
                            clientsWithNewFirstPaymentDate.get(i).setNewPayday("");
                            clientsWithNewFirstPaymentDate.get(i).setColor(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT);
                            clientsWithNewFirstPaymentDate.get(i).setShowAgain(false);
                            clientsWithNewFirstPaymentDate.get(i).setPayDay("0");
                            clientsWithNewFirstPaymentDate.get(i).setFirstPaydayWeekly("0");
                            clientsWithNewFirstPaymentDate.get(i).setSecondPaydayWeekly("0");
                            clientsWithNewFirstPaymentDate.get(i).save();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }


                //weekly
                List<SyncedWallet> clients = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE pay_day = " + dayOfWeek +
                                " AND payment_option = '1'" +
                                " AND show_color = '1'" +
                                " AND status == '1'");
                //" AND new_payday != " + dayOfYear);
                for (int i = 0; i < clients.size(); i++)
                {

                    //this is need it when a contract is paid before its payday (within paymentOption range
                    // weekly, fortnightly or monthly)
                    //so that it won't show again when its payday comes
                    boolean removeContract = false;
                    if (clients.get(i).isPaymentMadeBefore()) {

                        /*
                        Util.Log.ih("payment made before...........................");

                        int newPayday = Integer.valueOf(clients.get(i).getNewPayday());

                        //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                        int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                        int a = 1;
                        for (; ; a++) {
                            newPayday++;
                            if (newPayday == checkLeapYear()) {
                                newPayday = 1;
                            }
                            if (newPayday == newDayOfYear) {
                                break;
                            }
                        }

                        removeContract = a <= 6;
                        */
                        //TODO: IF THE PAYMENT IS MADE BEFORE
                        //TODO: DO ALWAYS REMOVE CONTRACT BECAUSE WEEKLY CONTRACTS ARE
                        //TODO: RESETED EVERY WEEK
                        removeContract = true;
                    }
                    //}


                    //TODO: SEPARETE PROCESS TO CHECK RELIABILITY
                    /**
                     * payment made in the same day than semaforo day
                     * check how many days have passed since that day
                     */
                /*
                    boolean removeContractLessThanSevenDays = false;
                    //if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).isPaymentMadeBefore()) {
                    {

                        //Util.Log.ih("payment made before...........................");

                        String newPaydayString = clients.get(i).getNewPayday();
                        if ( !newPaydayString.equals("") ) {

                            int newPayday = Integer.valueOf(clients.get(i).getNewPayday());

                            //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                            int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                            int a = 1;
                            for (; ; a++) {
                                //newPayday++;
                                //Util.Log.ih("newpayday = " + newPayday);
                                if (newPayday == checkLeapYear()) {
                                    newPayday = 1;
                                }
                                if (newPayday == newDayOfYear) {
                                    break;
                                }
                                newPayday++;
                            }

                            removeContractLessThanSevenDays = a <= 7;
                            //Util.Log.ih("remove contract = " + removeContract);
                        }
                    }
                    */



                    //when 'j' is less than 7 means that this contract was paid before payday
                    //and does not have to be shown on today's list
                    if (removeContract) {
                        Util.Log.ih("removeContract ----------> removing i = " + i);

                        /******************************************
                         * TODO: DELETE THIS WHEN FINISH DEBUGGING
                         * ****************************************
                         */
                        //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - green clients - removeContract", true, clients.get(i));

                        clients.remove(i);
                        i--;
                    } /*else if (removeContractLessThanSevenDays) {
                        Util.Log.ih("removeContract removeContractLessThanSevenDays ----------> removing i " + clients.get(i).getContractID() + " = " + i);

                                /******************************************
                                 * TODO: DELETE THIS WHEN FINISH DEBUGGING
                                 * ****************************************
                                 *//*
                                LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - green clients - removeContract", true, clients.get(i));
                        clients.get(i).setShowColor(false);
                        clients.get(i).save();
                        clients.remove(i);
                        i--;
                    }*/ else {
                        Util.Log.ih("not removing i = " + i);

                        /******************************************
                         * TODO: DELETE THIS WHEN FINISH DEBUGGING
                         * ****************************************
                         */
                        //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - green clients - not removing - before changing", true, clients.get(i));


                        clients.get(i).setFPA(false);
                        clients.get(i).setFPAPending(false);
                        clients.get(i).setPaymentMadeBefore(false);
                        clients.get(i).setShowAgain(true);
                        clients.get(i).setNotVisited(true);
                        clients.get(i).setDayCounter(dayCounter);
                        clients.get(i).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                        clients.get(i).save();

                        /******************************************
                         * TODO: DELETE THIS WHEN FINISH DEBUGGING
                         * ****************************************
                         */
                        //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - green clients - not removing - after changing", true, clients.get(i));

                    }
                }

                //todo: add at botton of list
                List<SyncedWallet> todayClientsWithVisitsOrPaymentMadeWeekly = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE pay_day = " + dayOfWeek +
                                " AND payment_option = '1'" +
                                " AND show_color = '0' " +
                                " AND status == '1'");

                for (int i = 0; i < todayClientsWithVisitsOrPaymentMadeWeekly.size(); i++) {
                    todayClientsWithVisitsOrPaymentMadeWeekly.get(i).setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);

                    /******************************************
                     * TODO: DELETE THIS WHEN FINISH DEBUGGING
                     * ****************************************
                     */
                    //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - todayClientsWithVisitsOrPaymentMadeWeekly - blue - after changing", true, todayClientsWithVisitsOrPaymentMadeWeekly.get(i));

                }








        /*
        //fortnightly
        List<SyncedWallet> clientsFortnightly = SyncedWallet.findWithQuery(SyncedWallet.class,
                "SELECT * FROM SYNCED_WALLET " +
                        "WHERE payment_option = '2'" +
                        " AND " +
                        "(" +
                        "first_payday_weekly = " + dayOfMonth +
                        " OR second_payday_weekly = " + dayOfMonth +
                        ") " +
                        "AND show_color = '1' " +
                        " AND status == '1'" );
                        //" AND new_payday != " + dayOfYear );
        */


                //TODO: REPLACE
                //TODO: clientsFortnightly AND
                //TODO: clientsMontly
                //TODO: WITH THIS QUERY

                List<SyncedWallet> clientsFortnightlyAndMontly = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE" +
                                " (" +
                                " payment_option = '2'" +
                                " OR payment_option = '3'" +
                                " )" +
                                " AND" +
                                " (" +
                                " first_payday_weekly = " + dayOfMonth +
                                " OR second_payday_weekly = " + dayOfMonth +
                                " OR pay_day = " + dayOfMonth +
                                " )" +
                                " AND show_color = '1' " +
                                " AND status == '1'");

                //" AND new_payday != " + dayOfYear );

                for (int i = 0; i < clientsFortnightlyAndMontly.size(); i++) {

                    Util.Log.ih("FORTNIGHTLYMONTHLY size: " + clientsFortnightlyAndMontly.size());
                    Util.Log.ih("FORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLY");
                    Util.Log.ih("MONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLY");

                    //this is need it when a contract is paid before its payday (within paymentOption range
                    // weekly, fortnightly or monthly)
                    //so that it won't show again when its payday comes
                    boolean removeContract = false;
                    if (clientsFortnightlyAndMontly.get(i).isPaymentMadeBefore()) {

                        Util.Log.ih("payment made before...........................");

                        int newPayday = Integer.valueOf(clientsFortnightlyAndMontly.get(i).getNewPayday());
                        //int firstPayday = Integer.valueOf( clientsFortnightly.get(i).getFirstPaydayWeekly() );
                        //int secondPayday = Integer.valueOf( clientsFortnightly.get(i).getSecondPaydayWeekly() );


                        //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                        int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                        int a = 1;
                        //Util.Log.ih("newPayday = " + newPayday);
                        //Util.Log.ih("newDayOfYear = " + newDayOfYear);
                        //Util.Log.ih("firstPayday = " + firstPayday);
                        //Util.Log.ih("secondPayday = " + secondPayday);
                        for (; ; a++) {
                            Util.Log.ih("AAAAA = " + a);
                            newPayday++;

                            //if (newPayday == 366)
                            //    newPayday = 365;

                            if (newPayday > checkLeapYear()) {
                                newPayday = 1;
                            }
                            if (newPayday == newDayOfYear) {
                                //Util.Log.ih("newPayday == newDayOfYear");
                                break;
                            }
                        }
                        Util.Log.ih("AAAAA = " + a);
                        if (clientsFortnightlyAndMontly.get(i).getPaymentOption().equals("2")) {
                            removeContract = a <= 13;
                        } else if (clientsFortnightlyAndMontly.get(i).getPaymentOption().equals("3")) {
                            removeContract = a <= 25;
                        }
                        Util.Log.ih("remove contract = " + removeContract);
                    }


                    //TODO: SEPARETE PROCESS TO CHECK RELIABILITY
                    /**
                     * payment made in the same day than semaforo day
                     * check how many days have passed since that day
                     */
                    boolean removeContractLessThanFifteenOrThirtyDays = false;
                    //if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).isPaymentMadeBefore()) {
                    {

                        //Util.Log.ih("payment made before...........................");

                        String newPaydayString = clientsFortnightlyAndMontly.get(i).getNewPayday();
                        if ( !newPaydayString.equals("") ) {

                            int newPayday = Integer.valueOf(clientsFortnightlyAndMontly.get(i).getNewPayday());

                            if (newPayday == 366) {
                                Util.Log.ih("new payday is equal to 366" + clientsFortnightlyAndMontly.get(i).getSerializedClient());
                                //newPayday = 365;
                            }

                            //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                            int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                            int a = 1;
                            for (; ; a++) {
                                Util.Log.ih("AAAAA = " + a);

                                /*
                                if (newPayday == 366) {
                                    Util.Log.ih("new payday is equal to 366");
                                    Util.Log.ih("new payday is equal to 366" + clientsFortnightlyAndMontly.get(i).getSerializedClient());
                                    //newPayday = 365;
                                }
                                */

                                if (newPayday > checkLeapYear()) {
                                    newPayday = 1;
                                }
                                if (newPayday == newDayOfYear) {
                                    break;
                                }
                                newPayday++;
                            }
                            //Util.Log.ih("AAAAA = " + a);
                            if (clientsFortnightlyAndMontly.get(i).getPaymentOption().equals("2")) {
                                //removeContract = a <= 15;
                                removeContractLessThanFifteenOrThirtyDays = a <= 14;
                            } else if (clientsFortnightlyAndMontly.get(i).getPaymentOption().equals("3")) {
                                removeContractLessThanFifteenOrThirtyDays = a <= 25;
                            }
                            //Util.Log.ih("remove contract = " + removeContract);
                        }
                    }



                    //if removeContract is true, means that this contract was paid before payday
                    //and does not have to be shown on today's list
                    if (removeContract) {//if ( (j > 0 && j < 31) || (j == 0 && paymentMadeBefore) ){
                        Util.Log.ih("removeContract ----------> removing i = " + i);

                        /******************************************
                         * TODO: DELETE THIS WHEN FINISH DEBUGGING
                         * ****************************************
                         */
                        //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - green clientsFortnightlyAndMontly - removeContract - after changing", true, clientsFortnightlyAndMontly.get(i));


                        clientsFortnightlyAndMontly.remove(i);
                        i--;
                        removeContractLessThanFifteenOrThirtyDays = false;
                    } else if (removeContractLessThanFifteenOrThirtyDays) {
                        Util.Log.ih("removeContract ----------> removing i removeContractLessThanFifteenOrThirtyDays= " + i);

                        /******************************************
                         * TODO: DELETE THIS WHEN FINISH DEBUGGING
                         * ****************************************
                         */
                        //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - green clientsFortnightlyAndMontly - removeContract - after changing", true, clientsFortnightlyAndMontly.get(i));

                        clientsFortnightlyAndMontly.get(i).setShowColor(false);
                        clientsFortnightlyAndMontly.get(i).save();
                        clientsFortnightlyAndMontly.remove(i);
                        i--;
                    } else {
                        Util.Log.ih("not removing i = " + i);

                        /******************************************
                         * TODO: DELETE THIS WHEN FINISH DEBUGGING
                         * ****************************************
                         */
                        //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - green clientsFortnightlyAndMontly - not removing - before changing", true, clientsFortnightlyAndMontly.get(i));


                        clientsFortnightlyAndMontly.get(i).setFPA(false);
                        clientsFortnightlyAndMontly.get(i).setFPAPending(false);
                        clientsFortnightlyAndMontly.get(i).setPaymentMadeBefore(false);
                        clientsFortnightlyAndMontly.get(i).setShowAgain(true);
                        clientsFortnightlyAndMontly.get(i).setNotVisited(true);
                        clientsFortnightlyAndMontly.get(i).setDayCounter(dayCounter);
                        clientsFortnightlyAndMontly.get(i).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                        clientsFortnightlyAndMontly.get(i).save();

                        /******************************************
                         * TODO: DELETE THIS WHEN FINISH DEBUGGING
                         * ****************************************
                         */
                        //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - green clientsFortnightlyAndMontly - not removing - after changing", true, clientsFortnightlyAndMontly.get(i));

                    }
                }

                //TODO: ADD TO BOTTOM OF LIST
        /*
        List<SyncedWallet> todayClientsWithVisitsOrPaymentMadeFortnightly = SyncedWallet.findWithQuery(SyncedWallet.class,
                "SELECT * FROM SYNCED_WALLET " +
                        "WHERE payment_option = '2'" +
                        " AND " +
                        "(" +
                        "first_payday_weekly = " + dayOfMonth +
                        " OR second_payday_weekly = " + dayOfMonth +
                        ") " +
                        "AND show_color = '0' " +
                        "AND status == '1'" );
        */


                //TODO: REPLACE
                //TODO: todayClientsWithVisitsOrPaymentMadeFortnightly AND
                //TODO: todayClientsWithVisitsOrPaymentMadeMontly
                //TODO: WITH THIS QUERY

                List<SyncedWallet> todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE" +
                                " (" +
                                " payment_option = '2'" +
                                " OR payment_option = '3'" +
                                " )" +
                                " AND" +
                                " (" +
                                " first_payday_weekly = " + dayOfMonth +
                                " OR second_payday_weekly = " + dayOfMonth +
                                " OR pay_day = " + dayOfMonth +
                                " )" +
                                " AND show_color = '0' " +
                                " AND status == '1'");

                for (int i = 0; i < todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.size(); i++) {
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);

                    /******************************************
                     * TODO: DELETE THIS WHEN FINISH DEBUGGING
                     * ****************************************
                     */
                    //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - blue todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly - blue - after changing", true, todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i));

                }








        /*
        //monthly
        List<SyncedWallet> clientsMontly = SyncedWallet.findWithQuery(SyncedWallet.class,
                "SELECT * FROM SYNCED_WALLET " +
                        "WHERE pay_day = " + dayOfMonth +
                        " AND payment_option = '3' " +
                        "AND show_color = '1' " +
                        "AND status == '1'" );
                        //" AND new_payday != " + dayOfYear );
        */
        /*
        for (int i = 0; i < clientsMontly.size(); i++){

            Util.Log.ih("MONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLY");

            //this is need it when a contract is paid before its payday (within paymentOption range
            // weekly, fortnightly or monthly)
            //so that it won't show again when its payday comes
            boolean removeContract = false;
            if (clientsMontly.get(i).isPaymentMadeBefore()){

                Util.Log.ih("payment made before...........................");

                int newPayday = Integer.valueOf( clientsMontly.get(i).getNewPayday() );

                //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                int a = 1;
                for ( ; ; a++){
                    newPayday++;
                    if (newPayday == checkLeapYear()){
                        newPayday = 1;
                    }
                    if (newPayday == newDayOfYear){
                        break;
                    }
                }

                removeContract = a <= 26;
            }

            //if removeContract is true, means that this contract was paid before payday
            //and does not have to be shown on today's list
            if ( removeContract ){//if ( (j > 0 && j < 31) || (j == 0 && paymentMadeBefore) ){
                Util.Log.ih("removeContract ----------> removing i = " + i);
                clientsMontly.remove(i);
                i--;
            } else {
                Util.Log.ih("not removing i = " + i);
                clientsMontly.get(i).setPaymentMadeBefore(false);
                clientsMontly.get(i).setShowAgain(true);
                clientsMontly.get(i).setNotVisited(true);
                clientsMontly.get(i).setDayCounter(dayCounter);
                clientsMontly.get(i).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                clientsMontly.get(i).save();
            }
        }
        */

        /*
        List<SyncedWallet> todayClientsWithVisitsOrPaymentMadeMontly = SyncedWallet.findWithQuery(SyncedWallet.class,
                "SELECT * FROM SYNCED_WALLET " +
                        "WHERE pay_day = " + dayOfMonth +
                        " AND payment_option = '3' " +
                        "AND show_color = '0' " +
                        "AND status == '1'" );
        */
                int dayOfWeekAux = dayOfWeek;

                dayOfWeekAux = dayOfWeekAux == 1 ? 7 : dayOfWeekAux - 1;

                //TODO: NEW PROCESS WEEKLY
                //without paying from other days
                //gotten in different queries so that they're shown by day (yellow and red) on screen
                List<SyncedWallet> resetCientsToShowAgainYesterdayWeekly = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE show_again = " + "1" +
                                " AND day_counter == " + (dayCounter - 1) + " " +
                                " AND payment_option = '1'" +
                                " AND " +
                                " ( " +
                                " pay_day != " + (dayOfWeekAux) +
                                " AND pay_day != " + "0" +
                                " )" +
                                " AND status == '1' " +
                                " AND " +
                                " ( " +
                                " color = '" + SyncedWallet.COLOR_TODAY_CONTRACT + "' " +
                                " OR color = '" + SyncedWallet.COLOR_PENDING_CONTRACT + "' " +
                                " )" +
                                "ORDER BY payment_option DESC");
                for (SyncedWallet sw: resetCientsToShowAgainYesterdayWeekly){
                    Util.Log.ih("resetCientsToShowAgainYesterdayWeekly - dentro de for");
                    sw.setVisitCounter(0);
                    sw.setDayCounter(0);
                    sw.setVisitMade(false);
                    sw.setShowAgain(false);
                    sw.setShowColor(false);
                    sw.setNotVisited(true);
                    sw.setPaymentMade(false);
                    sw.setPaymentMadeBefore(false);
                    sw.setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);
                    sw.save();
                }

                //TODO: NEW PROCESS WEEKLY
                //without paying from other days
                //gotten in different queries so that they're shown by day (yellow and red) on screen
                List<SyncedWallet> clientsToShowAgainYesterdayWeekly = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE show_again = " + "1" +
                                " AND day_counter == " + (dayCounter - 1) + " " +
                                " AND payment_option = '1'" +
                                " AND " +
                                " ( " +
                                " pay_day = " + (dayOfWeekAux) +
                                " OR pay_day = " + "0" +
                                " )" +
                                " AND status == '1' " +
                                " AND " +
                                " ( " +
                                " color = '" + SyncedWallet.COLOR_TODAY_CONTRACT + "' " +
                                " OR color = '" + SyncedWallet.COLOR_PENDING_CONTRACT + "' " +
                                " )" +
                                "ORDER BY payment_option DESC");
                for (int i = 0; i < clientsToShowAgainYesterdayWeekly.size(); i++) {


                    boolean showAsPending = true;
                    /*
                    if ( clientsToShowAgainYesterdayWeekly.get(i).getPaymentOption().equals("2") || clientsToShowAgainYesterdayWeekly.get(i).getPaymentOption().equals("3") ) {


                        //TODO: REMOVE YELLOWS AND REDS FROM PENDING IF THEY ARE PAID ALREADY
                        int newDayOfYearA = calendar.get(Calendar.DAY_OF_YEAR);

                        int lastPaymentDay = 0;
                        try {
                            lastPaymentDay = Integer.valueOf(clientsToShowAgainYesterdayWeekly.get(i).getNewPayday());
                        } catch (NumberFormatException e) {
                            continue;
                        }

                        if (lastPaymentDay != 0) {
                            int b = 1;
                            for (; ; b++, lastPaymentDay++) {
                                //secondVisitDay++;
                                if (lastPaymentDay == newDayOfYearA) {
                                    showAsPending = false;
                                    break;
                                }
                                if (lastPaymentDay == checkLeapYear()) {
                                    lastPaymentDay = 0;
                                }
                                if (clientsToShowAgainYesterdayWeekly.get(i).getPaymentOption().equals("2")) {
                                    if (b > 10) {
                                        break;
                                    }
                                } else if (clientsToShowAgainYesterdayWeekly.get(i).getPaymentOption().equals("3")) {
                                    if (b > 20) {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    */

                    if (showAsPending) {

                        Util.Log.eh("SHOWING YELLOW WEEKLY " + clientsToShowAgainYesterdayWeekly.get(i).getSerializedClient());

                        clientsToShowAgainYesterdayWeekly.get(i).setFPA(false);
                        clientsToShowAgainYesterdayWeekly.get(i).setFPAPending(false);
                        clientsToShowAgainYesterdayWeekly.get(i).setColor(SyncedWallet.COLOR_PENDING_CONTRACT);
                        clientsToShowAgainYesterdayWeekly.get(i).save();

                        /******************************************
                         * TODO: DELETE THIS WHEN FINISH DEBUGGING
                         * ****************************************
                         */
                        //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - clientsToShowAgainYesterday - yellowWEEKLY - after changing", true, clientsToShowAgainYesterdayWeekly.get(i));
                    } else {
                        clientsToShowAgainYesterdayWeekly.remove(i);
                        i--;
                        Util.Log.eh("NOT SHOWING YELLOW WEEKLY");
                    }

                }

                Calendar cal = Calendar.getInstance();
                int dayOfMonthAux = dayOfMonth;
                int month = cal.get(Calendar.MONTH);
                dayOfMonthAux = dayOfMonthAux == 1 ? getNumberOfDaysOfMonth( ( month == 0 ? 12 : (month - 1) ) ) : dayOfMonthAux - 1;

                //TODO: NEW PROCESS FORTNIGHTLY AND MONTHLY
                //without paying from other days
                //gotten in different queries so that they're shown by day (yellow and red) on screen
                List<SyncedWallet> resetClientsToShowAgainYesterdayFortnightlyAndMonthly = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE show_again = " + "1" +
                                " AND day_counter == " + (dayCounter - 1) + " " +
                                " AND " +
                                " (" +
                                " payment_option = '2'" +
                                " OR payment_option = '3'" +
                                " )" +
                                " AND" +
                                " (" +
                                " first_payday_weekly != " + (dayOfMonthAux) +
                                " OR second_payday_weekly != " + (dayOfMonthAux) +
                                " OR pay_day != " + (dayOfMonthAux) +
                                " )" +
                                " AND color = '" + SyncedWallet.COLOR_TODAY_CONTRACT + "' " +
                                " AND status == '1' " +
                                "ORDER BY payment_option DESC");
                for (SyncedWallet sw: resetClientsToShowAgainYesterdayFortnightlyAndMonthly){
                    sw.setVisitCounter(0);
                    sw.setDayCounter(0);
                    sw.setVisitMade(false);
                    sw.setShowAgain(false);
                    sw.setShowColor(false);
                    sw.setNotVisited(true);
                    sw.setPaymentMade(false);
                    sw.setPaymentMadeBefore(false);
                    sw.save();
                }

                //TODO: NEW PROCESS FORTNIGHTLY AND MONTHLY
                //without paying from other days
                //gotten in different queries so that they're shown by day (yellow and red) on screen
                List<SyncedWallet> clientsToShowAgainYesterdayFortnightlyAndMonthly = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE show_again = " + "1" +
                                " AND day_counter == " + (dayCounter - 1) + " " +
                                " AND " +
                                " (" +
                                " payment_option = '2'" +
                                " OR payment_option = '3'" +
                                " )" +
                                " AND" +
                                " (" +
                                " first_payday_weekly = " + (dayOfMonthAux) +
                                " OR second_payday_weekly = " + (dayOfMonthAux) +
                                " OR pay_day = " + (dayOfMonthAux) +
                                " )" +
                                " AND color = '" + SyncedWallet.COLOR_TODAY_CONTRACT + "' " +
                                " AND status == '1' " +
                                "ORDER BY payment_option DESC");
                for (int i = 0; i < clientsToShowAgainYesterdayFortnightlyAndMonthly.size(); i++) {


                    boolean showAsPending = true;
                    if ( clientsToShowAgainYesterdayFortnightlyAndMonthly.get(i).getPaymentOption().equals("2") || clientsToShowAgainYesterdayFortnightlyAndMonthly.get(i).getPaymentOption().equals("3") ) {


                        //TODO: REMOVE YELLOWS AND REDS FROM PENDING IF THEY ARE PAID ALREADY
                        int newDayOfYearA = calendar.get(Calendar.DAY_OF_YEAR);

                        int lastPaymentDay = 0;
                        try {
                            lastPaymentDay = Integer.valueOf(clientsToShowAgainYesterdayFortnightlyAndMonthly.get(i).getNewPayday());
                        } catch (NumberFormatException e) {
                            continue;
                        }

                        if (lastPaymentDay != 0) {
                            int b = 1;
                            for (; ; b++, lastPaymentDay++) {
                                //secondVisitDay++;
                                if (lastPaymentDay == newDayOfYearA) {
                                    showAsPending = false;
                                    break;
                                }
                                if (lastPaymentDay == checkLeapYear()) {
                                    lastPaymentDay = 0;
                                }
                                if (clientsToShowAgainYesterdayFortnightlyAndMonthly.get(i).getPaymentOption().equals("2")) {
                                    if (b > 10) {
                                        break;
                                    }
                                } else if (clientsToShowAgainYesterdayFortnightlyAndMonthly.get(i).getPaymentOption().equals("3")) {
                                    if (b > 20) {
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if (showAsPending) {

                        Util.Log.eh("SHOWING YELLOW " + clientsToShowAgainYesterdayFortnightlyAndMonthly.get(i).getSerializedClient());

                        clientsToShowAgainYesterdayFortnightlyAndMonthly.get(i).setFPA(false);
                        clientsToShowAgainYesterdayFortnightlyAndMonthly.get(i).setFPAPending(false);
                        clientsToShowAgainYesterdayFortnightlyAndMonthly.get(i).setColor(SyncedWallet.COLOR_PENDING_CONTRACT);
                        clientsToShowAgainYesterdayFortnightlyAndMonthly.get(i).save();

                        /******************************************
                         * TODO: DELETE THIS WHEN FINISH DEBUGGING
                         * ****************************************
                         */
                        //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - clientsToShowAgainYesterday - yellowFortnightlyAndMonthly - after changing", true, clientsToShowAgainYesterdayFortnightlyAndMonthly.get(i));
                    } else {
                        clientsToShowAgainYesterdayFortnightlyAndMonthly.remove(i);
                        i--;
                        Util.Log.eh("NOT SHOWING YELLOW FortnightlyAndMonthly ");
                    }

                }


                /*
                //TODO: CHANGE PROCESS IN TWO PARTS (WEEKLY AND FORTNIGHTLY AND MONTHLY)
                //without paying from other days
                //gotten in different queries so that they're shown by day (yellow and red) on screen
                List<SyncedWallet> clientsToShowAgainYesterday = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE show_again = " + "1" +
                                " AND day_counter == " + (dayCounter - 1) + " " +
                                " AND status == '1' " +
                                "ORDER BY payment_option DESC");
                for (int i = 0; i < clientsToShowAgainYesterday.size(); i++) {


                    boolean showAsPending = true;
                    if ( clientsToShowAgainYesterday.get(i).getPaymentOption().equals("2") || clientsToShowAgainYesterday.get(i).getPaymentOption().equals("3") ) {


                        //TODO: REMOVE YELLOWS AND REDS FROM PENDING IF THEY ARE PAID ALREADY
                        int newDayOfYearA = calendar.get(Calendar.DAY_OF_YEAR);

                        int lastPaymentDay = 0;
                        try {
                            lastPaymentDay = Integer.valueOf(clientsToShowAgainYesterday.get(i).getNewPayday());
                        } catch (NumberFormatException e) {
                            continue;
                        }

                        if (lastPaymentDay != 0) {
                            int b = 1;
                            for (; ; b++, lastPaymentDay++) {
                                //secondVisitDay++;
                                if (lastPaymentDay == newDayOfYearA) {
                                    showAsPending = false;
                                    break;
                                }
                                if (lastPaymentDay == checkLeapYear()) {
                                    lastPaymentDay = 0;
                                }
                                if (clientsToShowAgainYesterday.get(i).getPaymentOption().equals("2")) {
                                    if (b > 10) {
                                        break;
                                    }
                                } else if (clientsToShowAgainYesterday.get(i).getPaymentOption().equals("3")) {
                                    if (b > 20) {
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if (showAsPending) {

                        Util.Log.eh("SHOWING YELLOW " + clientsToShowAgainYesterday.get(i).getSerializedClient());

                        clientsToShowAgainYesterday.get(i).setFPA(false);
                        clientsToShowAgainYesterday.get(i).setFPAPending(false);
                        clientsToShowAgainYesterday.get(i).setColor(SyncedWallet.COLOR_PENDING_CONTRACT);
                        clientsToShowAgainYesterday.get(i).save();

                        /******************************************
                         * TODO: DELETE THIS WHEN FINISH DEBUGGING
                         * ****************************************
                         *//*
                        LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - clientsToShowAgainYesterday - yellow - after changing", true, clientsToShowAgainYesterday.get(i));
                    } else {
                        clientsToShowAgainYesterday.remove(i);
                        i--;
                        Util.Log.eh("NOT SHOWING YELLOW ");
                    }

                }
                */

        /*
        Util.Log.eh( "size = " + clientsToShowAgainYesterday.size() );
        for (int i = 0; i < clientsToShowAgainYesterday.size(); i++) {
            Util.Log.eh("YELLOW");
            Util.Log.eh(" " + clientsToShowAgainYesterday.get(i).getContractID());
        }
        */


                List<SyncedWallet> clientsToShowAgainBeforeYesterday = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE show_again = 1 " +
                                "AND day_counter < " + (dayCounter - 1) + "" +
                                " AND status == '1'" +
                                " ORDER BY payment_option DESC");
                for (int i = 0; i < clientsToShowAgainBeforeYesterday.size(); i++) {

                    Util.Log.ih("clientsToShowAgainBeforeYesterday.size() = " + clientsToShowAgainBeforeYesterday.size());

                    boolean showAsPending = true;
                    if ( clientsToShowAgainBeforeYesterday.get(i).getPaymentOption().equals("2") || clientsToShowAgainBeforeYesterday.get(i).getPaymentOption().equals("3") ) {

                        //TODO: REMOVE YELLOWS AND REDS FROM PENDING IF THEY ARE PAID ALREADY
                        int newDayOfYearA = calendar.get(Calendar.DAY_OF_YEAR);

                        int lastPaymentDay = 0;
                        try {
                            lastPaymentDay = Integer.valueOf(clientsToShowAgainBeforeYesterday.get(i).getNewPayday());
                        } catch (NumberFormatException e) {
                            continue;
                        }

                        if (lastPaymentDay != 0) {
                            Util.Log.ih("if");
                            int b = 1;
                            for (; ; b++, lastPaymentDay++) {
                                Util.Log.ih("for");
                                Util.Log.ih("b = " + b + " lastPaymentDay = " + lastPaymentDay + " getPaymentOption = " + clientsToShowAgainBeforeYesterday.get(i).getPaymentOption());
                                //secondVisitDay++;
                                if (lastPaymentDay == newDayOfYearA) {
                                    showAsPending = false;
                                    break;
                                }
                                if (lastPaymentDay == checkLeapYear()) {
                                    lastPaymentDay = 0;
                                }
                                if (clientsToShowAgainBeforeYesterday.get(i).getPaymentOption().equals("2")) {
                                    if (b > 10) {
                                        break;
                                    }
                                } else if (clientsToShowAgainBeforeYesterday.get(i).getPaymentOption().equals("3")) {
                                    if (b > 20) {
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if (showAsPending) {
                        clientsToShowAgainBeforeYesterday.get(i).setFPA(false);
                        clientsToShowAgainBeforeYesterday.get(i).setFPAPending(false);
                        clientsToShowAgainBeforeYesterday.get(i).setColor(SyncedWallet.COLOR_PENDING_LEVEL_2_CONTRACT);
                        clientsToShowAgainBeforeYesterday.get(i).save();

                        /******************************************
                         * TODO: DELETE THIS WHEN FINISH DEBUGGING
                         * ****************************************
                         */
                        //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - clientsToShowAgainBeforeYesterday - red - after changing", true, clientsToShowAgainBeforeYesterday.get(i));
                    } else {
                        clientsToShowAgainBeforeYesterday.remove(i);
                        i--;
                        Util.Log.eh("NOT SHOWING RED ");
                    }
                }

        /*
        Util.Log.eh( "size = " + clientsToShowAgainBeforeYesterday.size() );
        for (int i = 0; i < clientsToShowAgainBeforeYesterday.size(); i++) {
            Util.Log.eh("RED");
            Util.Log.eh(" " + clientsToShowAgainBeforeYesterday.get(i).getContractID());
        }
        */


                List<SyncedWallet> clientsFromOtherDayWithPay = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET WHERE day_counter < " + dayCounter + " " +
                                "AND show_color = '0'" +
                                "AND " +
                                "(" +
                                "new_payday = " + dayOfYear +
                                " OR second_visit_day = " + dayOfYear +
                                ") " +
                                "AND status == '1'");

                for (int i = 0; i < clientsFromOtherDayWithPay.size(); i++) {
                    clientsFromOtherDayWithPay.get(i).setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);

                    /******************************************
                     * TODO: DELETE THIS WHEN FINISH DEBUGGING
                     * ****************************************
                     */
                    //LogRegister.saveContractsIntoFile("", "getClientsFromSyncedWallet - clientsFromOtherDayWithPay - blue other day - after changing", true, clientsFromOtherDayWithPay.get(i));

                }

                //contracts without payday analysis
                List<SyncedWallet> clientsWithoutPaydayAnalysisReCheckFirstPaymentDate = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE " +
                                "pay_day = '0' " +
                                "AND first_payday_weekly = '0' " +
                                "AND second_payday_weekly = '0' " +
                                "AND status == 1");
                for (int i = 0; i < clientsWithoutPaydayAnalysisReCheckFirstPaymentDate.size(); i++) {
                    try {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        formatter.setLenient(false);

                        //get milliseconds of first payment date
                        String firstPaymentDateString = clientsWithoutPaydayAnalysisReCheckFirstPaymentDate.get(i).deserializeClient().getClient().getFirstPaymentDate();
                        Date firstPaymentDateDateFormat = formatter.parse(firstPaymentDateString);
                        long firstPaymentDateMilliseconds = firstPaymentDateDateFormat.getTime();

                        //get millisecond of today date
                        long date = new Date().getTime();

                        long difference = firstPaymentDateMilliseconds - date;
                        boolean firstPaymentDateIsGreater = difference < 0;

                        if (!firstPaymentDateIsGreater) {
                            clientsWithoutPaydayAnalysisReCheckFirstPaymentDate.get(i).setVisitCounter(0);
                            clientsWithoutPaydayAnalysisReCheckFirstPaymentDate.get(i).setPaymentMade(false);
                            clientsWithoutPaydayAnalysisReCheckFirstPaymentDate.get(i).setPaymentMadeBefore(false);
                            clientsWithoutPaydayAnalysisReCheckFirstPaymentDate.get(i).setNewPayday("");
                            clientsWithoutPaydayAnalysisReCheckFirstPaymentDate.get(i).save();
                        }
                    } catch (ParseException e) {
                    }
                }


                //contracts without payday analysis
                List<SyncedWallet> clientsWithoutPaydayAnalysis = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE " +
                                "(pay_day = '0' " +
                                "AND first_payday_weekly = '0' " +
                                "AND second_payday_weekly = '0') " +
                                "AND visit_counter == '0' " +
                                "AND " +
                                "status == 1");
                for (int i = 0; i < clientsWithoutPaydayAnalysis.size(); i++) {

            /*
             * FPA
             * First Payment Date
             * shows FPA if needed
             */
                    try {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        formatter.setLenient(false);

                        //get milliseconds of first payment date
                        String firstPaymentDateString = clientsWithoutPaydayAnalysis.get(i).deserializeClient().getClient().getFirstPaymentDate();

                        //Util.Log.ih("FPA = " + firstPaymentDateString);

                        Date firstPaymentDateDateFormat = formatter.parse(firstPaymentDateString);

                        long firstPaymentDateMilliseconds = firstPaymentDateDateFormat.getTime();

                        //get millisecond of today date
                        long date = new Date().getTime();

                        long difference = firstPaymentDateMilliseconds - date;
                        boolean deleteFPA = difference < 0;


                        //Util.Log.ih("difference = " + difference);

                        //check if first payment date is greater or equal to today's date
                        //if so, show first payment date mask

                        //if (firstPaymentDateMilliseconds >= date) -> greater validation (not equal)

                        //when deleteFPA is true, means that FPA hasn't come yet
                        //so show FPA mask without checking if a payment was made or if
                        //contract has a last payment date
                        if (!deleteFPA) {
                            Util.Log.ih("adding date... black FPA");
                            clientsWithoutPaydayAnalysis.get(i).setFPA(true);
                            clientsWithoutPaydayAnalysis.get(i).setFPAPending(false);
                        } else {
                            long maxDifference = -difference;


                            Util.Log.ih("maxDifference = " + maxDifference);

                            Util.Log.ih("name: " + clientsWithoutPaydayAnalysis.get(i).getClient().getName());
                            Util.Log.ih("isPaymentMade = " + !clientsWithoutPaydayAnalysis.get(i).isPaymentMade());
                            Util.Log.ih("isPaymentMade and secondVisitDay = " + !(clientsWithoutPaydayAnalysis.get(i).isPaymentMade() || clientsWithoutPaydayAnalysis.get(i).getSecondVisitDay() > 0));
                            Util.Log.ih("getLastPaymentDate().equals(\"0000-00-00 00:00:00\") = " + clientsWithoutPaydayAnalysis.get(i).getClient().getLastPaymentDate().equals("0000-00-00"));
                            Util.Log.ih("fecha ultimo abono = " + clientsWithoutPaydayAnalysis.get(i).getClient().getLastPaymentDate());


                            if (!(clientsWithoutPaydayAnalysis.get(i).isPaymentMade() || clientsWithoutPaydayAnalysis.get(i).getSecondVisitDay() > 0)
                                    && clientsWithoutPaydayAnalysis.get(i).getClient().getLastPaymentDate().equals("0000-00-00 00:00:00")
                            /*
                            ( clientsWithoutPaydayAnalysis.get(i).getClient().getLastPaymentDate().equals("0000-00-00") &&
                              !clientsWithoutPaydayAnalysis.get(i).getClient().getOverdueBalance().equals("0.00")
                            )
                             */
                                    ) {
                                if (maxDifference < 432000000) {
                                    Util.Log.ih("adding date... red FPA");
                                    clientsWithoutPaydayAnalysis.get(i).setFPA(true);
                                    clientsWithoutPaydayAnalysis.get(i).setFPAPending(true);
                                } else {
                                    Util.Log.ih("do not adding date, more than 5 days...");
                                    clientsWithoutPaydayAnalysis.get(i).setFPA(false);
                                    clientsWithoutPaydayAnalysis.get(i).setFPAPending(false);
                                }
                            } else {
                                Util.Log.ih("do not adding date...");
                                clientsWithoutPaydayAnalysis.get(i).setFPA(false);
                                clientsWithoutPaydayAnalysis.get(i).setFPAPending(false);
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    clientsWithoutPaydayAnalysis.get(i).setColor(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT);

                    if (clientsWithoutPaydayAnalysis.get(i).getVisitCounter() >= 1 || clientsWithoutPaydayAnalysis.get(i).isPaymentMade()) {
                        //clientsWithoutPaydayAnalysis.get(i).setVisitCounter(0);
                        //clientsWithoutPaydayAnalysis.get(i).save();
                        clientsWithoutPaydayAnalysis.remove(i);
                        i--;
                    } else {
                        clientsWithoutPaydayAnalysis.get(i).setPaymentMadeBefore(false);
                        clientsWithoutPaydayAnalysis.get(i).setShowAgain(true);
                        clientsWithoutPaydayAnalysis.get(i).setDayCounter(dayCounter);

                        clientsWithoutPaydayAnalysis.get(i).setNotVisited(false);
                        clientsWithoutPaydayAnalysis.get(i).save();
                    }
                }

                List<SyncedWallet> clientsWithoutPaydayAnalysisVisitedOnceWeekly = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE " +
                                "(pay_day = '0' " +
                                "AND first_payday_weekly = '0' " +
                                "AND second_payday_weekly = '0') " +
                                "AND visit_counter == '1' " +
                                "AND payment_option = '1' " +
                                "AND new_payday = '" + dayOfYear + "' " +
                                "AND status == 1 ");

                List<SyncedWallet> clientsWithoutPaydayAnalysisVisitedOnceForthnightlyOrMonthly = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE " +
                                "(pay_day = '0' " +
                                "AND first_payday_weekly = '0' " +
                                "AND second_payday_weekly = '0') " +
                                "AND visit_counter == '1' " +
                                "AND " +
                                "(payment_option = '2' " +
                                "OR payment_option = '3') " +
                                "AND new_payday = '" + dayOfYear + "' " +
                                "AND status == 1 ");

                List<SyncedWallet> clientsWithoutPaydayAnalysisPaidOrVisitedWeekly = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE " +
                                "(pay_day = '0' " +
                                "AND first_payday_weekly = '0' " +
                                "AND second_payday_weekly = '0') " +
                                "AND (visit_counter >= '2' " +
                                "OR payment_made = '1') " +
                                "AND payment_option = '1' " +
                                "AND new_payday = '" + dayOfYear + "' " +
                                "AND status == 1 ");

                List<SyncedWallet> clientsWithoutPaydayAnalysisPaidOrVisitedForthnightlyOrMonthly = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE " +
                                "(pay_day = '0' " +
                                "AND first_payday_weekly = '0' " +
                                "AND second_payday_weekly = '0') " +
                                "AND (visit_counter >= '2' " +
                                "OR payment_made = '1') " +
                                "AND " +
                                "(payment_option = '2' " +
                                "OR payment_option = '3') " +
                                "AND new_payday = '" + dayOfYear + "' " +
                                "AND status == 1 ");


                List<SyncedWallet> clientsWithoutPaydayAnalysisSuspended = SyncedWallet.findWithQuery(SyncedWallet.class,
                        "SELECT * FROM SYNCED_WALLET " +
                                "WHERE " +
                                "status != 1");
                for (int i = 0; i < clientsWithoutPaydayAnalysisSuspended.size(); i++) {
                    clientsWithoutPaydayAnalysisSuspended.get(i).setColor(SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT);
                    //if ( clientsWithoutPaydayAnalysisSuspended.get(i).getVisitCounter() >= 2 ) {
                    clientsWithoutPaydayAnalysisSuspended.get(i).setVisitCounter(0);
                    //}
                    //clientsWithoutPaydayAnalysisSuspended.get(i).setNotVisited(true);
                    clientsWithoutPaydayAnalysisSuspended.get(i).setFPA(false);
                    clientsWithoutPaydayAnalysisSuspended.get(i).setFPAPending(false);
                    clientsWithoutPaydayAnalysisSuspended.get(i).setPaymentMadeBefore(false);
                    clientsWithoutPaydayAnalysisSuspended.get(i).setShowAgain(true);
                    clientsWithoutPaydayAnalysisSuspended.get(i).setDayCounter(dayCounter);
                    clientsWithoutPaydayAnalysisSuspended.get(i).setPaymentMade(false);


                    clientsWithoutPaydayAnalysisSuspended.get(i).save();
                    if (clientsWithoutPaydayAnalysisSuspended.get(i).isPaymentMade()) {
                        clientsWithoutPaydayAnalysisSuspended.remove(i);
                        i--;
                    }
                }

        /*
        List<SyncedWallet> clientsWithoutAnalysisButPaydayWeekly = SyncedWallet.findWithQuery(SyncedWallet.class,
                "SELECT * FROM SYNCED_WALLET " +
                        "WHERE " +
                        "payday_wa = " + dayOfWeek +
                        " AND payment_option = '1'");
        for (int i = 0; i < clientsWithoutAnalysisButPaydayWeekly.size(); i++){
            clientsWithoutAnalysisButPaydayWeekly.get(i).setColor(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT);
            clientsWithoutAnalysisButPaydayWeekly.get(i).save();
        }

        List<SyncedWallet> clientsWithoutAnalysisButPaydayFortnighltyAndMonthly = SyncedWallet.findWithQuery(SyncedWallet.class,
                "SELECT * FROM SYNCED_WALLET " +
                        "WHERE " +
                        "payday_wa = " + dayOfMonth +
                        " AND payment_option != '1'");
        for (int i = 0; i < clientsWithoutAnalysisButPaydayFortnighltyAndMonthly.size(); i++){
            clientsWithoutAnalysisButPaydayFortnighltyAndMonthly.get(i).setColor(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT);
            clientsWithoutAnalysisButPaydayFortnighltyAndMonthly.get(i).save();
        }
        */


        /*
        //contracts without payday nor analysis to appear again (they were paid at least once)
        List<SyncedWallet> clientsWithoutPaydayAnalysisPaid = SyncedWallet.findWithQuery(SyncedWallet.class,
                "SELECT * FROM SYNCED_WALLET " +
                        "WHERE " +
                        "new_payday = " + dayOfYear +
                        "AND pay_day = '' " +
                        "AND first_payday_weekly = '' " +
                        "AND second_payday_weekly = ''");
        for (int i = 0; i < clientsWithoutPaydayAnalysisPaid.size(); i++){
            clientsWithoutPaydayAnalysisPaid.get(i).setColor(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT);
            clientsWithoutPaydayAnalysisPaid.get(i).save();
        }
        */


                Util.Log.ih("clientsWeekly size: " + clients.size());

        //TODO
                Util.Log.ih("clientsWithoutPaydayAnalysisVisitedOnceWeekly size: " + clientsWithoutPaydayAnalysisVisitedOnceWeekly.size());
                //add contracts from other days to clients array
                /*for (int i = 0; i < clientsWithoutPaydayAnalysisVisitedOnceWeekly.size(); i++) {
                    clients.add(clientsWithoutPaydayAnalysisVisitedOnceWeekly.get(i));
                }*/
                clients.addAll(clientsWithoutPaydayAnalysisVisitedOnceWeekly);

                Util.Log.ih("clientsFortnightlyAndMontly size: " + clientsFortnightlyAndMontly.size());
                //add fortnightly and monthly contract to clients array
                /*for (int i = 0; i < clientsFortnightlyAndMontly.size(); i++) {
                    //Util.Log.ih("nombre: " + clientsFortnightlyAndMontly.get(i).getContractID());
                    clients.add(clientsFortnightlyAndMontly.get(i));
                }*/
                clients.addAll(clientsFortnightlyAndMontly);

                //TODO
                Util.Log.ih("clientsWithoutPaydayAnalysisVisitedOnceForthnightlyOrMonthly size: " + clientsWithoutPaydayAnalysisVisitedOnceForthnightlyOrMonthly.size());
                //add contracts from other days to clients array
                /*for (int i = 0; i < clientsWithoutPaydayAnalysisVisitedOnceForthnightlyOrMonthly.size(); i++) {
                    clients.add(clientsWithoutPaydayAnalysisVisitedOnceForthnightlyOrMonthly.get(i));
                }*/
                clients.addAll(clientsWithoutPaydayAnalysisVisitedOnceForthnightlyOrMonthly);

                Util.Log.ih("clientsWithoutPaydayAnalysisPaidOrVisitedWeekly size: " + clientsWithoutPaydayAnalysisPaidOrVisitedWeekly.size());
                //add weekly contracts that has no payday analysis with payment or visit to clients array
                /*for (int i = 0; i < clientsWithoutPaydayAnalysisPaidOrVisitedWeekly.size(); i++) {
                    //Util.Log.ih("nombre: " + clientsWithoutPaydayAnalysisPaidOrVisitedWeekly.get(i).getContractID());
                    clients.add(clientsWithoutPaydayAnalysisPaidOrVisitedWeekly.get(i));
                }*/
                clients.addAll(clientsWithoutPaydayAnalysisPaidOrVisitedWeekly);


                Util.Log.ih("clientsWithoutPaydayAnalysisPaidOrVisitedForthnightlyOrMonthly size: " + clientsWithoutPaydayAnalysisPaidOrVisitedForthnightlyOrMonthly.size());
                //add forthnightly or monthly contracts that has no payday analysis with payment or visit to clients array
                /*for (int i = 0; i < clientsWithoutPaydayAnalysisPaidOrVisitedForthnightlyOrMonthly.size(); i++) {
                    //Util.Log.ih("nombre: " + clientsWithoutPaydayAnalysisPaidOrVisitedForthnightlyOrMonthly.get(i).getContractID());
                    clients.add(clientsWithoutPaydayAnalysisPaidOrVisitedForthnightlyOrMonthly.get(i));
                }*/
                clients.addAll(clientsWithoutPaydayAnalysisPaidOrVisitedForthnightlyOrMonthly);

        /*
        for (int i = 0; i < clientsMontly.size(); i++) {
            clients.add(clientsMontly.get(i));
        }
        */

                Util.Log.ih("clientsToShowAgainYesterdayWeekly size: " + clientsToShowAgainYesterdayWeekly.size());
                //add contracts from other days to clients array
                for (int i = 0; i < clientsToShowAgainYesterdayWeekly.size(); i++) {
                    clients.add(0, clientsToShowAgainYesterdayWeekly.get(i));
                }

                Util.Log.ih("clientsToShowAgainYesterdayFortnightlyAndMonthly size: " + clientsToShowAgainYesterdayFortnightlyAndMonthly.size());
                //add contracts from other days to clients array
                for (int i = 0; i < clientsToShowAgainYesterdayFortnightlyAndMonthly.size(); i++) {
                    clients.add(0, clientsToShowAgainYesterdayFortnightlyAndMonthly.get(i));
                }

                /*
                Util.Log.ih("clientsToShowAgainYesterday size: " + clientsToShowAgainYesterday.size());
                //add contracts from other days to clients array
                for (int i = 0; i < clientsToShowAgainYesterday.size(); i++) {
                    clients.add(0, clientsToShowAgainYesterday.get(i));
                }
                */

                Util.Log.ih("clientsToShowAgainBeforeYesterday size: " + clientsToShowAgainBeforeYesterday.size());
                for (int i = 0; i < clientsToShowAgainBeforeYesterday.size(); i++) {
                    clients.add(0, clientsToShowAgainBeforeYesterday.get(i));
                }

                //order contracts
                //show visited and paid at the bottom of screen
        /*
        for (int j = 0; j < clients.size(); j++) {
            for (int i = 0; i < clients.size(); i++) {
                if (!clients.get(i).isShowColor()) {
                    SyncedWallet sw = clients.get(i);

                    Util.Log.ih("isShowColor = " + sw.getClient().getName());

                    clients.remove(i);
                    clients.add(sw);
                }
            }
        }
        */

                Util.Log.ih("todayClientsWithVisitsOrPaymentMadeWeekly size: " + todayClientsWithVisitsOrPaymentMadeWeekly.size());
                //add today clients that have paid or have been visited at least 2 times
                /*for (int i = 0; i < todayClientsWithVisitsOrPaymentMadeWeekly.size(); i++) {
                    clients.add(todayClientsWithVisitsOrPaymentMadeWeekly.get(i));
                }*/
                clients.addAll(todayClientsWithVisitsOrPaymentMadeWeekly);

                Util.Log.ih("todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly size: " + todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.size());
                /*for (int i = 0; i < todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.size(); i++) {
                    clients.add(todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i));
                }*/
                clients.addAll(todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly);
        /*
        for (int i = 0; i < todayClientsWithVisitsOrPaymentMadeMontly.size(); i++){
            clients.add(todayClientsWithVisitsOrPaymentMadeMontly.get(i));
        }
        */


                Util.Log.ih("clientsFromOtherDayWithPay size: " + clientsFromOtherDayWithPay.size());
                //add contracts from other day that were paid today
                /*for (int i = 0; i < clientsFromOtherDayWithPay.size(); i++) {
                    clients.add(clientsFromOtherDayWithPay.get(i));
                }*/
                clients.addAll(clientsFromOtherDayWithPay);


                Util.Log.ih("clientsWithoutPaydayAnalysis size: " + clientsWithoutPaydayAnalysis.size());
                //add contracts that have no payday analysis
                for (int i = 0; i < clientsWithoutPaydayAnalysis.size(); i++) {
                    for (int j = 0; j < clients.size(); j++) {
                        if (clients.get(j).getContractID().equals(clientsWithoutPaydayAnalysis.get(i).getContractID())) {
                            clients.remove(j);
                            break;
                        }
                    }
                    clients.add(clientsWithoutPaydayAnalysis.get(i));
                }
                //clients.addAll(clientsWithoutPaydayAnalysis);

                Util.Log.ih("clientsWithoutPaydayAnalysisSuspended size: " + clientsWithoutPaydayAnalysisSuspended.size());
                //add contracts that have no payday analysis
                for (int i = 0; i < clientsWithoutPaydayAnalysisSuspended.size(); i++) {
                    for (int j = 0; j < clients.size(); j++) {
                        if (clients.get(j).getContractID().equals(clientsWithoutPaydayAnalysisSuspended.get(i).getContractID())) {
                            clients.remove(j);
                            break;
                        }
                    }
                    clients.add(clientsWithoutPaydayAnalysisSuspended.get(i));
                }
                //clients.addAll(clientsWithoutPaydayAnalysisSuspended);

        /*
        for (int i = 0; i < clientsWithoutAnalysisButPaydayWeekly.size(); i++){
            clients.add(clientsWithoutAnalysisButPaydayWeekly.get(i));
        }

        for (int i = 0; i < clientsWithoutAnalysisButPaydayFortnighltyAndMonthly.size(); i++){
            clients.add(clientsWithoutAnalysisButPaydayFortnighltyAndMonthly.get(i));
        }
        */

                Util.Log.ih("clients size = " + clients.size());

                if (clients.size() <= 0) {
                    return new ArrayList<>();
                } else {

                    //deserialize client info
                    Util.Log.ih("size " + clients.size());
                    for (int i = 0; i < clients.size(); i++) {
                        Util.Log.ih("deserializing " + i);
                        clients.get(i).deserializeClient();
                    }

                    return clients;
                }
    }

    public static void resetSemaforo() {
        SyncedWallet.deleteAll(SyncedWallet.class);
    }

    public static void resetContractsPerWeek(){
        Util.Log.ih("resetContractsPerWeek");
        /*
        List<SyncedWallet> contractsToResetPerWeekVisited = SyncedWallet.find(SyncedWallet.class,
                "visit_counter > ? AND payment_option = ?", "0", "1");
        if (contractsToResetPerWeekVisited.size() > 0){
            Util.Log.ih("contractsToResetPerWeekVisited - dentro de if");
            for (SyncedWallet sw: contractsToResetPerWeekVisited){
                Util.Log.ih("contractsToResetPerWeekVisited - dentro de for");
                sw.setVisitCounter(0);
                sw.setVisitMade(false);
                sw.setShowAgain(false);
                sw.setShowColor(false);
                sw.setNotVisited(true);
                sw.setPaymentMadeBefore(false);
                sw.save();
            }
        }

        List<SyncedWallet> contractsToResetPerWeekShowingColor = SyncedWallet.find(SyncedWallet.class,
                "show_again = ? AND payment_option = ?", "1", "1");
        if (contractsToResetPerWeekShowingColor.size() > 0){
            Util.Log.ih("contractsToResetPerWeekShowingColor - dentro de if");
            for (SyncedWallet sw: contractsToResetPerWeekShowingColor){
                Util.Log.ih("contractsToResetPerWeekShowingColor - dentro de for");
                sw.setVisitCounter(0);
                sw.setVisitMade(false);
                sw.setShowAgain(false);
                sw.setShowColor(false);
                sw.setNotVisited(true);
                sw.setPaymentMadeBefore(false);
                sw.save();
            }
        }

        List<SyncedWallet> contractsPaidAlready = SyncedWallet.find(SyncedWallet.class,
                "payment_made = ? AND payment_option = ?", "1, 1");
        if (contractsPaidAlready.size() > 0){
            Util.Log.ih("contractsPaidAlready - dentro de if");
            for (SyncedWallet sw: contractsPaidAlready){
                Util.Log.ih("contractsPaidAlready - dentro de for");
                sw.setVisitCounter(0);
                sw.setVisitMade(false);
                sw.setShowAgain(false);
                sw.setShowColor(false);
                sw.setNotVisited(true);
                sw.setPaymentMadeBefore(false);
                sw.save();
            }
        }
        */
        List<SyncedWallet> contractsToResetPerWeek = SyncedWallet.find(SyncedWallet.class,
                "payment_option = ?", "1");
        if (contractsToResetPerWeek.size() > 0){

            Util.Log.ih("contractsToResetPerWeekVisited - dentro de if");
            for (SyncedWallet sw: contractsToResetPerWeek){
                Util.Log.ih("contractsToResetPerWeekVisited - dentro de for");
                sw.setVisitCounter(0);
                sw.setVisitMade(false);
                sw.setShowAgain(false);
                sw.setShowColor(false);
                sw.setNotVisited(true);
                sw.setPaymentMade(false);
                sw.setPaymentMadeBefore(false);
                sw.setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);
                sw.save();
            }
        }
    }

    public static ArrayList<ModelSemaforoRequest.semaforoContract> getContractsForSemaforoEcobro(){
        String whereClause = "color = ? OR color = ? OR color = ? OR color = ?";
        String[] whereArgs = new String[]{SyncedWallet.COLOR_TODAY_CONTRACT, SyncedWallet.COLOR_PENDING_CONTRACT, SyncedWallet.COLOR_PENDING_LEVEL_2_CONTRACT, SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT};

        List<SyncedWallet> semaforo = SyncedWallet.find(SyncedWallet.class, whereClause, whereArgs);
        List<Collectors> collector = Collectors.listAll(Collectors.class);

        String collectorNumber = collector.get(0).getNumberCollector();

        if (semaforo.size() > 0 && collector.size() > 0){

            for (int i = 0; i < semaforo.size(); i++){
                semaforo.get(i).deserializeClient();
            }

            ArrayList<ModelSemaforoRequest.semaforoContract> modelSemaforo = new ArrayList<>();
            int status;

            for (SyncedWallet s: semaforo){

                switch (s.getColor()){
                    case SyncedWallet.COLOR_TODAY_CONTRACT:
                        status = 1;
                        break;
                    case SyncedWallet.COLOR_PENDING_CONTRACT:
                        status = 2;
                        break;
                    case SyncedWallet.COLOR_PENDING_LEVEL_2_CONTRACT:
                        status = 3;
                        break;
                    case SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT:
                        //only add contracts with red fpa or without fpa
                        //do not add contracts with black fpa
                        if ( !s.isFPA() && !s.isFPAPending() ) {
                            status = 4;
                        } else if ( s.isFPA() && s.isFPAPending() ) {
                            status = 4;
                        } else {
                            continue;
                        }
                        break;
                    default:
                        status = 1;
                }

                ModelSemaforoRequest.semaforoContract singleContract = new ModelSemaforoRequest.semaforoContract()
                        .setContractNumber(s.getClient().getSerie() + padLeft(s.getClient().getNumberContract(), 6, "0"))
                        .setContractID( Integer.parseInt( s.getClient().getIdContract() ) )
                        .setStatusSemaforo(status);

                modelSemaforo.add(singleContract);
            }

            return modelSemaforo;
        }
        return new ArrayList<>();
    }

    private static void setContractsToShowAfterFewDaysWithoutCollecting(PreferencesToShowWallet preferencesToShowWallet, int counter){

        Util.Log.ih("setContractsToShowAfterFewDaysWithoutCollecting");

        Calendar calendar = Calendar.getInstance();

        int dayOfWeek = preferencesToShowWallet.getDayOfWeek();
        //dayOfWeek -= 1;
        //if (dayOfWeek == 0){
        //    dayOfWeek = 7;
        //}




        int dayOfMonth = preferencesToShowWallet.getDayOfMonth();
        int dayOfYear = preferencesToShowWallet.getDayOfYear();
        int maximunDayMonth = preferencesToShowWallet.getMaximunDayMonth();

        long dayCounter = 0;

        for ( ; counter > 0 ; counter-- ){

            Util.Log.ih("dentro de for = " + counter);

            preferencesToShowWallet.saveDayOfYear(dayOfYear);
            preferencesToShowWallet.saveDayOfWeek(dayOfWeek);
            preferencesToShowWallet.saveDayOfMonth(dayOfMonth);
            preferencesToShowWallet.saveMaximunDayMonth(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            preferencesToShowWallet.saveDayCounter();

            dayCounter = preferencesToShowWallet.getDayCounter();



            if (dayOfWeek == 7) {
                dayOfWeek = 1;
            } else {
                dayOfWeek++;
            }

            if ( dayOfMonth == maximunDayMonth ){
                dayOfMonth = 1;
            } else {
                dayOfMonth++;
            }

            if ( dayOfYear > checkLeapYear() ){
                dayOfYear = 1;
            } else {
                dayOfYear++;
            }

            Util.Log.ih("dayOfWeek = " + dayOfWeek);
            Util.Log.ih("dayOfMonth = " + dayOfMonth);
            Util.Log.ih("dayOfYear = " + dayOfYear);


            /*

             */
            if ( dayOfWeek == 6 ) {
                //PreferencesToShowWallet preferencesToShowWallet = new PreferencesToShowWallet(ApplicationResourcesProvider.getContext());
                preferencesToShowWallet.setResetContractsPerWeekAfterFewDaysWithoutCollecting();
            } else if ( dayOfWeek == 7 ) {
                preferencesToShowWallet.lastSynchronizationToResetContractsPerWeekAfterFewDaysWithoutCollecting();
                resetContractsPerWeek();
            }





            changeVisitedOrPaidStatusFromContracts(dayOfWeek, dayOfMonth, dayOfYear);





            //weekly
            List<SyncedWallet> clients = SyncedWallet.findWithQuery(SyncedWallet.class,
                    "SELECT * FROM SYNCED_WALLET " +
                            "WHERE pay_day = " + dayOfWeek +
                            " AND payment_option = '1'" +
                            " AND show_color = '1'" +
                            " AND status == '1'" );

            for (int i = 0; i < clients.size(); i++){
                //this is need it when a contract is paid before its payday (within paymentOption range
                // weekly, fortnightly or monthly)
                //so that it won't show again when its payday comes
                boolean removeContract = false;
                if (clients.get(i).isPaymentMadeBefore()){

                    /*
                    Util.Log.ih("payment made before...........................");

                    int newPayday = Integer.valueOf( clients.get(i).getNewPayday() );

                    //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                    int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                    int a = 1;
                    for ( ; ; a++){
                        newPayday++;
                        if (newPayday == checkLeapYear()){
                            newPayday = 1;
                        }
                        if (newPayday == newDayOfYear){
                            break;
                        }
                    }
                    removeContract = a <= 7;
                    */
                    //TODO: IF THE PAYMENT IS MADE BEFORE
                    //TODO: DO ALWAYS REMOVE CONTRACT BECAUSE WEEKLY CONTRACTS ARE
                    //TODO: RESETED EVERY WEEK
                    removeContract = true;
                }

                //TODO: SEPARETE PROCESS TO CHECK RELIABILITY
                /**
                 * payment made in the same day than semaforo day
                 * check how many days have passed since that day
                 */
            /*
                boolean removeContractLessThanSevenDays = false;
                //if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).isPaymentMadeBefore()) {
                {

                    //Util.Log.ih("payment made before...........................");

                    String newPaydayString = clients.get(i).getNewPayday();
                    if ( !newPaydayString.equals("") ) {

                        int newPayday = Integer.valueOf(clients.get(i).getNewPayday());

                        //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                        int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                        int a = 1;
                        for (; ; a++) {
                            newPayday++;
                            if (newPayday == checkLeapYear()) {
                                newPayday = 1;
                            }
                            if (newPayday == newDayOfYear) {
                                break;
                            }
                        }

                        removeContractLessThanSevenDays = a <= 6;
                        //Util.Log.ih("remove contract = " + removeContract);
                    }
                }
                */

                //when 'j' is less than 7 means that this contract was paid before payday
                //and does not have to be shown on today's list
                if ( removeContract ){
                    Util.Log.ih("removeContract ----------> removing i = " + i);
                    clients.remove(i);
                    i--;
                }/* else if (removeContractLessThanSevenDays) {
                    Util.Log.ih("removeContract ----------> removing i = " + i);
                    clients.remove(i);
                    i--;
                }*/ else {
                    Util.Log.ih("not removing i = " + i);
                    clients.get(i).setFPA(false);
                    clients.get(i).setFPAPending(false);
                    clients.get(i).setPaymentMade(false);
                    clients.get(i).setPaymentMadeBefore(false);
                    clients.get(i).setShowColor(true);
                    clients.get(i).setVisitMade(false);
                    //clients.get(i).setVisitCounter(0);
                    clients.get(i).setSecondVisitDay(0);
                    clients.get(i).setNewPayday("");
                    clients.get(i).setShowAgain(true);
                    clients.get(i).setNotVisited(true);
                    clients.get(i).setDayCounter(dayCounter);
                    clients.get(i).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                    clients.get(i).save();
                }
            }
            /*
            for (int i = 0; i < clients.size(); i++){
                clients.get(i).setShowAgain(true);
                clients.get(i).setNotVisited(true);
                clients.get(i).setDayCounter(dayCounter);
                clients.get(i).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                clients.get(i).save();
            }
            */

            /*
            Util.Log.ih("WEEKLY size: " + clients.size());
            for (int i = 0; i < clients.size(); i++){
                clients.get(i).deserializeClient();
                Util.Log.ih("cliente: " + clients.get(i).getClient().getName());
            }
            */

            /*
            //todo: add at botton of list
            List<SyncedWallet> todayClientsWithVisitsOrPaymentMadeWeekly = SyncedWallet.findWithQuery(SyncedWallet.class,
                    "SELECT * FROM SYNCED_WALLET " +
                            "WHERE pay_day = " + dayOfWeek +
                            " AND payment_option = '1'" +
                            " AND show_color = '0'");
            //for (int i = 0; i < clients.size(); i++){
            //clients.get(i).setShowAgain(true);
            //clients.get(i).setNotVisited(true);
            //clients.get(i).setDayCounter(dayCounter);
            //clients.get(i).save();
            //}
            */








            //fortnightly
            List<SyncedWallet> clientsFortnightlyAndMonthly = SyncedWallet.findWithQuery(SyncedWallet.class,
                    "SELECT * FROM SYNCED_WALLET " +
                            "WHERE" +
                            " (" +
                            " payment_option = '2'" +
                            " OR payment_option = '3'" +
                            " )" +
                            " AND" +
                            " (" +
                            " first_payday_weekly = " + dayOfMonth +
                            " OR second_payday_weekly = " + dayOfMonth +
                            " OR pay_day = " + dayOfMonth +
                            " )" +
                            " AND show_color = '1' " +
                            " AND status == '1'" );
            for (int i = 0; i < clientsFortnightlyAndMonthly.size(); i++){

                //Util.Log.ih("FORTNIGHTLYMONTHLY size: " + clientsFortnightlyAndMonthly.size());
                Util.Log.ih("FORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLY");
                Util.Log.ih("MONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLY");

                //this is need it when a contract is paid before its payday (within paymentOption range
                // weekly, fortnightly or monthly)
                //so that it won't show again when its payday comes
                boolean removeContract = false;
                if (clientsFortnightlyAndMonthly.get(i).isPaymentMadeBefore()){

                    Util.Log.ih("payment made before...........................");

                    int newPayday = Integer.valueOf( clientsFortnightlyAndMonthly.get(i).getNewPayday() );
                    //int firstPayday = Integer.valueOf( clientsFortnightly.get(i).getFirstPaydayWeekly() );
                    //int secondPayday = Integer.valueOf( clientsFortnightly.get(i).getSecondPaydayWeekly() );


                    //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                    int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                    int a = 1;
                    //Util.Log.ih("newPayday = " + newPayday);
                    //Util.Log.ih("newDayOfYear = " + newDayOfYear);
                    //Util.Log.ih("firstPayday = " + firstPayday);
                    //Util.Log.ih("secondPayday = " + secondPayday);
                    for ( ; ; a++){
                        newPayday++;
                        Util.Log.ih("AAAAA = " + a);
                        if (newPayday > checkLeapYear()){
                            newPayday = 1;
                        }
                        if ( newPayday == newDayOfYear ){
                            //Util.Log.ih("newPayday == newDayOfYear");
                            break;
                        }
                    }
                    Util.Log.ih("AAAAA = " + a);
                    if ( clientsFortnightlyAndMonthly.get(i).getPaymentOption().equals("2") ) {
                        removeContract = a <= 13;
                    } else if ( clientsFortnightlyAndMonthly.get(i).getPaymentOption().equals("3") ){
                        removeContract = a <= 25;
                    }
                    Util.Log.ih("remove contract = " + removeContract);
                }

                //TODO: SEPARETE PROCESS TO CHECK RELIABILITY
                /**
                 * payment made in the same day than semaforo day
                 * check how many days have passed since that day
                 */
                boolean removeContractLessThanFifteenOrThirtyDays = false;
                //if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).isPaymentMadeBefore()) {
                {

                    //Util.Log.ih("payment made before...........................");

                    String newPaydayString = clientsFortnightlyAndMonthly.get(i).getNewPayday();
                    if ( !newPaydayString.equals("") ) {

                        int newPayday = Integer.valueOf(clientsFortnightlyAndMonthly.get(i).getNewPayday());

                        //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                        int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                        int a = 1;
                        for (; ; a++) {
                            newPayday++;

                            //if (newPayday == 366)
                            //    newPayday = 365;

                            if (newPayday > checkLeapYear()) {
                                newPayday = 1;
                            }
                            if (newPayday == newDayOfYear) {
                                break;
                            }
                        }
                        //Util.Log.ih("AAAAA = " + a);
                        if (clientsFortnightlyAndMonthly.get(i).getPaymentOption().equals("2")) {
                            //removeContract = a <= 15;
                            removeContractLessThanFifteenOrThirtyDays = a <= 13;
                        } else if (clientsFortnightlyAndMonthly.get(i).getPaymentOption().equals("3")) {
                            removeContractLessThanFifteenOrThirtyDays = a <= 25;
                        }
                        //Util.Log.ih("remove contract = " + removeContract);
                    }
                }

                //if removeContract is true, means that this contract was paid before payday
                //and does not have to be shown on today's list
                if ( removeContract ){//if ( (j > 0 && j < 31) || (j == 0 && paymentMadeBefore) ){
                    Util.Log.ih("removeContract ----------> removing i = " + i);
                    clientsFortnightlyAndMonthly.remove(i);
                    i--;
                    removeContractLessThanFifteenOrThirtyDays = false;
                } else if (removeContractLessThanFifteenOrThirtyDays) {
                    Util.Log.ih("removeContract ----------> removing i = " + i);
                    clientsFortnightlyAndMonthly.remove(i);
                    i--;
                } else {
                    Util.Log.ih("not removing i = " + i);
                    clientsFortnightlyAndMonthly.get(i).setFPA(false);
                    clientsFortnightlyAndMonthly.get(i).setFPAPending(false);
                    clientsFortnightlyAndMonthly.get(i).setPaymentMadeBefore(false);
                    clientsFortnightlyAndMonthly.get(i).setPaymentMade(false);
                    clientsFortnightlyAndMonthly.get(i).setShowColor(true);
                    clientsFortnightlyAndMonthly.get(i).setVisitMade(false);
                    //clientsFortnightlyAndMonthly.get(i).setVisitCounter(0);
                    clientsFortnightlyAndMonthly.get(i).setSecondVisitDay(0);
                    clientsFortnightlyAndMonthly.get(i).setNewPayday("");
                    clientsFortnightlyAndMonthly.get(i).setShowAgain(true);
                    clientsFortnightlyAndMonthly.get(i).setNotVisited(true);
                    clientsFortnightlyAndMonthly.get(i).setDayCounter(dayCounter);
                    clientsFortnightlyAndMonthly.get(i).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                    clientsFortnightlyAndMonthly.get(i).save();
                }
            }
            /*
            for (int i = 0; i < clientsFortnightlyAndMonthly.size(); i++){
                clientsFortnightlyAndMonthly.get(i).setShowAgain(true);
                clientsFortnightlyAndMonthly.get(i).setNotVisited(true);
                clientsFortnightlyAndMonthly.get(i).setDayCounter(dayCounter);
                clientsFortnightlyAndMonthly.get(i).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                clientsFortnightlyAndMonthly.get(i).save();
            }
            */

            /*
            Util.Log.ih("FORTNIGHTLY size: " + clientsFortnightlyAndMonthly.size());
            for (int i = 0; i < clientsFortnightlyAndMonthly.size(); i++){
                clientsFortnightlyAndMonthly.get(i).deserializeClient();
                Util.Log.ih("cliente: " + clientsFortnightlyAndMonthly.get(i).getClient().getName());
            }
            */
        }
    }

    /**
     * restart contracts fields when it's their payday again
     * *** NOTE ***
     * this process should only run once per day
     * ************
     */
    public static void changeVisitedOrPaidStatusFromContracts( int... daysOfCalendar){
        PreferencesToShowWallet preferencesToShowWallet = new PreferencesToShowWallet(ApplicationResourcesProvider.getContext());
        Calendar calendar = Calendar.getInstance();

        int dayOfWeek = daysOfCalendar[0];
        int dayOfMonth = daysOfCalendar[1];
        int dayOfYear = daysOfCalendar[2];
        //int year = calendar.get(Calendar.YEAR);
        long dayCounter = preferencesToShowWallet.getDayCounter();

        //Util.Log.ih("validation - preferencesToShowWallet.lastSynchronizationToReset() == " + preferencesToShowWallet.lastSynchronizationToReset());
        //Util.Log.ih("validation - dayOfYear == " + dayOfYear);
        //Util.Log.ih("validation - preferencesToShowWallet.lastSynchronizationToReset() < dayOfYear == " + (preferencesToShowWallet.lastSynchronizationToReset() < dayOfYear));

        //if ( preferencesToShowWallet.lastSynchronizationToReset() < dayOfYear || preferencesToShowWallet.lastSynchronizationToReset() > dayOfYear) {

            Util.Log.ih("synchronizing contracts that were visited or paid before and have to be shown today");

        //TODO: DO NOT USE THIS CODE SINCE WEEKLY CONTRACTS ARE RESETED EVERY WEEK
        //TODO: THEY DO NOT HAVE TO CHANGE THEIR STATUS (PAID OR VISITED)


            List<SyncedWallet> todayClients = SyncedWallet.findWithQuery(SyncedWallet.class,
                    "SELECT * FROM SYNCED_WALLET " +
                            "WHERE " +
                            "new_payday != " + dayOfYear +
                            " AND pay_day = " + dayOfWeek +
                            " AND payment_option = '1'" +
                            " AND show_color = '0' " +
                            " AND status == '1'");

            for (int i = 0; i < todayClients.size(); i++) {

                //this is need it when a contract is paid before its payday (within paymentOption range
                // weekly, fortnightly or monthly)
                //so that it won't show again when its payday comes

                boolean removeContract = false;
                if (todayClients.get(i).isPaymentMadeBefore()) {


                    Util.Log.ih("payment made before...........................");
                    /*
                    int newPayday = Integer.valueOf(todayClients.get(i).getNewPayday());

                    //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                    int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                    int a = 1;
                    for (; ; a++) {
                        newPayday++;
                        if (newPayday == checkLeapYear()) {
                            newPayday = 1;
                        }
                        if (newPayday == newDayOfYear) {
                            break;
                        }
                    }

                    Util.Log.ih("a = " + a);

                    removeContract = a <= 7;
                    */

                    //TODO: IF THE PAYMENT IS MADE BEFORE
                    //TODO: DO ALWAYS REMOVE CONTRACT BECAUSE WEEKLY CONTRACTS ARE
                    //TODO: RESETED EVERY WEEK

                    removeContract = true;
                }


                //TODO: SEPARETE PROCESS TO CHECK RELIABILITY
                /**
                 * payment made in the same day than semaforo day
                 * check how many days have passed since that day
                 */
            /*
                boolean removeContractLessThanSevenDays = false;
                //if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).isPaymentMadeBefore()) {
                {

                    //Util.Log.ih("payment made before...........................");

                    String newPaydayString = todayClients.get(i).getNewPayday();
                    if ( !newPaydayString.equals("") ) {

                        int newPayday = Integer.valueOf(todayClients.get(i).getNewPayday());

                        //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                        int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                        int a = 1;
                        for (; ; a++) {
                            newPayday++;
                            if (newPayday == checkLeapYear()) {
                                newPayday = 1;
                            }
                            if (newPayday == newDayOfYear) {
                                break;
                            }
                        }

                        removeContractLessThanSevenDays = a <= 6;
                        //Util.Log.ih("remove contract = " + removeContract);
                    }
                }
                */

                //when 'j' is less than 7 means that this contract was paid before payday
                //and does not have to be shown on today's list
                if (removeContract) {
                    Util.Log.ih("removeContract ----------> removing i = " + i);

                    /******************************************
                     * TODO: DELETE THIS WHEN FINISH DEBUGGING
                     * ****************************************
                     */
                    //LogRegister.saveContractsIntoFile("", "changeVisitedOrPaidStatusFromContracts - green clients semanal - removeContract", true, todayClients.get(i));

                    todayClients.remove(i);
                    i--;
                } else {
                    Util.Log.ih("not removing i = " + i);

                    /******************************************
                     * TODO: DELETE THIS WHEN FINISH DEBUGGING
                     * ****************************************
                     */
                    //LogRegister.saveContractsIntoFile("", "changeVisitedOrPaidStatusFromContracts - green clients semanal - not removing", true, todayClients.get(i));

                    todayClients.get(i).setFPA(false);
                    todayClients.get(i).setFPAPending(false);
                    todayClients.get(i).setPaymentMadeBefore(false);
                    todayClients.get(i).setPaymentMade(false);
                    todayClients.get(i).setShowColor(true);
                    todayClients.get(i).setVisitMade(false);
                    todayClients.get(i).setNotVisited(true);
                    //todayClients.get(i).setVisitCounter(0);
                    todayClients.get(i).setSecondVisitDay(0);
                    todayClients.get(i).setNewPayday("");
                    todayClients.get(i).setShowAgain(false);
                    //todayClients.get(i).setNotVisited(true);
                    todayClients.get(i).setDayCounter(dayCounter);
                    todayClients.get(i).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                    todayClients.get(i).save();

                }
            }



            /*
            for (int i = 0; i < todayClients.size(); i++) {
                Util.Log.ih("todayClients " + i + " = " + todayClients.get(i).getContractID());
            }
            */


            List<SyncedWallet> todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly = SyncedWallet.findWithQuery(SyncedWallet.class,
                    "SELECT * FROM SYNCED_WALLET " +
                            "WHERE " +
                            "new_payday != " + dayOfYear +
                            " AND" +
                            " (" +
                            " payment_option = '2'" +
                            " OR payment_option = '3'" +
                            " )" +
                            " AND" +
                            " (" +
                            " first_payday_weekly = " + dayOfMonth +
                            " OR second_payday_weekly = " + dayOfMonth +
                            " OR pay_day = " + dayOfMonth +
                            " )" +
                            " AND show_color = '0' " +
                            " AND status == '1'");

            for (int i = 0; i < todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.size(); i++) {

                //Util.Log.ih("FORTNIGHTLYMONTHLY size: " + todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.size());
                Util.Log.ih("FORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLY");
                Util.Log.ih("MONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLY");



                //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                int newDayOfYearA = calendar.get(Calendar.DAY_OF_YEAR);

                int secondVisitDay = 0;
                try {
                    secondVisitDay = todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getSecondVisitDay();
                } catch (NumberFormatException e) {
                    continue;
                }

                boolean checkLastPaymentDayFlag = true;
                if ( secondVisitDay != 0 ) {
                    int b = 1;
                    for (; ; b++, secondVisitDay++) {
                        //secondVisitDay++;
                        if (secondVisitDay == newDayOfYearA) {
                            checkLastPaymentDayFlag = false;
                            break;
                        }
                        if (secondVisitDay == checkLeapYear()) {
                            secondVisitDay = 0;
                        }
                        if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getPaymentOption().equals("2")) {
                            if (b > 10) {
                                break;
                            }
                        } else if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getPaymentOption().equals("3")) {
                            if (b > 20) {
                                break;
                            }
                        }
                    }
                }

                Util.Log.ih("checkLastPaymentDayFlag " + checkLastPaymentDayFlag);

                boolean removeContractPaymentMadeBefore = false;
                boolean removeContractLessThanFifteenOrThirtyDays = false;
                if (checkLastPaymentDayFlag) {

                    //this is need it when a contract is paid before its payday (within paymentOption range
                    // weekly, fortnightly or monthly)
                    //so that it won't show again when its payday comes
                    //boolean removeContractPaymentMadeBefore = false;
                    if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).isPaymentMadeBefore()) {

                        Util.Log.ih("payment made before...........................");

                        int newPayday = 0;
                        try {
                            newPayday = Integer.valueOf(todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getNewPayday());
                        } catch (NumberFormatException e) {
                            continue;
                        }

                        //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                        int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                        int a = 1;
                        for (; ; a++) {
                            newPayday++;
                            Util.Log.ih("a = " + a);

                            //if (newPayday == 366)
                            //    newPayday = 365;

                            if (newPayday > checkLeapYear()) {
                                newPayday = 1;
                            }
                            if (newPayday == newDayOfYear) {
                                break;
                            }
                        }
                        //Util.Log.ih("AAAAA = " + a);
                        if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getPaymentOption().equals("2")) {
                            //removeContract = a <= 15;
                            removeContractPaymentMadeBefore = a <= 13;
                        } else if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getPaymentOption().equals("3")) {
                            removeContractPaymentMadeBefore = a <= 25;
                        }
                        //Util.Log.ih("remove contract = " + removeContract);
                    }


                    //TODO: SEPARETE PROCESS TO CHECK RELIABILITY
                    /**
                     * payment made in the same day than semaforo day
                     * check how many days have passed since that day
                     */
                    //boolean removeContractLessThanFifteenOrThirtyDays = false;
                    //if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).isPaymentMadeBefore()) {
                    {

                        //Util.Log.ih("payment made before...........................");

                        String newPaydayString = todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getNewPayday();
                        if (!newPaydayString.equals("")) {

                            int newPayday = Integer.valueOf(todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getNewPayday());

                            //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                            int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                            int a = 1;
                            for (; ; a++) {
                                newPayday++;

                                //if (newPayday == 366)
                                //    newPayday = 365;

                                if (newPayday > checkLeapYear()) {
                                    newPayday = 1;
                                }
                                if (newPayday == newDayOfYear) {
                                    break;
                                }
                            }
                            Util.Log.ih("AAAAA quincenal = " + a);
                            if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getPaymentOption().equals("2")) {
                                //removeContract = a <= 15;
                                removeContractLessThanFifteenOrThirtyDays = a <= 13;
                            } else if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getPaymentOption().equals("3")) {
                                removeContractLessThanFifteenOrThirtyDays = a <= 25;
                            }
                            //Util.Log.ih("remove contract = " + removeContract);
                        }
                    }
                }




                //if removeContract is true, means that this contract was paid before payday
                //and does not have to be shown on today's list
                if (removeContractPaymentMadeBefore) {//if ( (j > 0 && j < 31) || (j == 0 && paymentMadeBefore) ){
                    Util.Log.ih("removeContract ----------> dsd removing i = " + i);

                    /******************************************
                     * TODO: DELETE THIS WHEN FINISH DEBUGGING
                     * ****************************************
                     */
                    //LogRegister.saveContractsIntoFile("", "changeVisitedOrPaisStatus - removeContract", true, todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i));

                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.remove(i);
                    i--;
                    removeContractLessThanFifteenOrThirtyDays = false;
                } else if (removeContractLessThanFifteenOrThirtyDays) {
                    Util.Log.ih("removeContract ----------> removing i = " + i);

                    /******************************************
                     * TODO: DELETE THIS WHEN FINISH DEBUGGING
                     * ****************************************
                     */
                    //LogRegister.saveContractsIntoFile("", "changeVisitedOrPaisStatus - removeContract", true, todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i));

                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.remove(i);
                    i--;
                } else if (!checkLastPaymentDayFlag) {
                    Util.Log.ih("removeContract ----------> removing i = " + i);

                    /******************************************
                     * TODO: DELETE THIS WHEN FINISH DEBUGGING
                     * ****************************************
                     */
                    //LogRegister.saveContractsIntoFile("", "changeVisitedOrPaisStatus - removeContract", true, todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i));

                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.remove(i);
                    i--;
                } else {
                    Util.Log.ih("not removing i = " + i);

                    /******************************************
                     * TODO: DELETE THIS WHEN FINISH DEBUGGING
                     * ****************************************
                     */
                    //LogRegister.saveContractsIntoFile("", "changeVisitedOrPaisStatus - not removing - before changing", true, todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i));


                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setFPA(false);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setFPAPending(false);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setPaymentMadeBefore(false);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setPaymentMade(false);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setShowColor(true);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setVisitMade(false);
                    //todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setNotVisited(true);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setVisitCounter(0);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setSecondVisitDay(0);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setNewPayday("");
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setShowAgain(false);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setNotVisited(true);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setDayCounter(dayCounter);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).save();


                    /******************************************
                     * TODO: DELETE THIS WHEN FINISH DEBUGGING
                     * ****************************************
                     */
                    //LogRegister.saveContractsIntoFile("", "changeVisitedOrPaisStatus - not removing - after changing", true, todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i));


                }
            }

            /*
            for (int i = 0; i < todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.size(); i++) {
                Util.Log.ih("todayClients " + i + " = " + todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getContractID());
            }
            */

            //preferencesToShowWallet.setResetContractsSynced(dayOfYear);
        //} else {
        //    Util.Log.ih("do not synchronizing");
        //}
    }

    /**
     * MANUAL
     * restart contracts fields when it's their payday again
     * *** NOTE ***
     * this process should only run once per day
     * ************
     */
    public static void changeVisitedOrPaidStatusFromContractsManual( int... daysOfCalendar){
        PreferencesToShowWallet preferencesToShowWallet = new PreferencesToShowWallet(ApplicationResourcesProvider.getContext());
        Calendar calendar = Calendar.getInstance();

        int dayOfWeek = daysOfCalendar[0];
        int dayOfMonth = daysOfCalendar[1];
        int dayOfYear = daysOfCalendar[2];
        //int year = calendar.get(Calendar.YEAR);
        long dayCounter = preferencesToShowWallet.getDayCounter();

        //Util.Log.ih("validation - preferencesToShowWallet.lastSynchronizationToReset() == " + preferencesToShowWallet.lastSynchronizationToReset());
        //Util.Log.ih("validation - dayOfYear == " + dayOfYear);
        //Util.Log.ih("validation - preferencesToShowWallet.lastSynchronizationToReset() < dayOfYear == " + (preferencesToShowWallet.lastSynchronizationToReset() < dayOfYear));

        //if ( preferencesToShowWallet.lastSynchronizationToReset() < dayOfYear || preferencesToShowWallet.lastSynchronizationToReset() > dayOfYear) {

            Util.Log.ih("synchronizing contracts that were visited or paid before and have to be shown today");

        //TODO: DO NOT USE THIS CODE SINCE WEEKLY CONTRACTS ARE RESETED EVERY WEEK
        //TODO: THEY DO NOT HAVE TO CHANGE THEIR STATUS (PAID OR VISITED)
        /*
            List<SyncedWallet> todayClients = SyncedWallet.findWithQuery(SyncedWallet.class,
                    "SELECT * FROM SYNCED_WALLET " +
                            "WHERE " +
                            "new_payday != " + dayOfYear +
                            " AND pay_day = " + dayOfWeek +
                            " AND payment_option = '1'" +
                            " AND show_color = '0' " +
                            " AND status == '1'");

            for (int i = 0; i < todayClients.size(); i++) {

                //this is need it when a contract is paid before its payday (within paymentOption range
                // weekly, fortnightly or monthly)
                //so that it won't show again when its payday comes
                boolean removeContract = false;
                if (todayClients.get(i).isPaymentMadeBefore()) {

                    Util.Log.ih("payment made before...........................");

                    int newPayday = Integer.valueOf(todayClients.get(i).getNewPayday());

                    //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                    int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                    int a = 1;
                    for (; ; a++) {
                        newPayday++;
                        if (newPayday == checkLeapYear()) {
                            newPayday = 1;
                        }
                        if (newPayday == newDayOfYear) {
                            break;
                        }
                    }

                    Util.Log.ih("a = " + a);

                    removeContract = a <= 6;
                }

                //when 'j' is less than 7 means that this contract was paid before payday
                //and does not have to be shown on today's list
                if (removeContract) {
                    Util.Log.ih("removeContract ----------> removing i = " + i);
                    todayClients.remove(i);
                    i--;
                } else {
                    Util.Log.ih("not removing i = " + i);

                    todayClients.get(i).setFPA(false);
                    todayClients.get(i).setFPAPending(false);
                    todayClients.get(i).setPaymentMadeBefore(false);
                    todayClients.get(i).setPaymentMade(false);
                    todayClients.get(i).setShowColor(true);
                    todayClients.get(i).setVisitMade(false);
                    todayClients.get(i).setNotVisited(true);
                    //todayClients.get(i).setVisitCounter(0);
                    todayClients.get(i).setSecondVisitDay(0);
                    todayClients.get(i).setNewPayday("");
                    todayClients.get(i).setShowAgain(false);
                    //todayClients.get(i).setNotVisited(true);
                    todayClients.get(i).setDayCounter(dayCounter);
                    todayClients.get(i).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                    todayClients.get(i).save();

                }
            }
        */

            /*
            for (int i = 0; i < todayClients.size(); i++) {
                Util.Log.ih("todayClients " + i + " = " + todayClients.get(i).getContractID());
            }
            */


            List<SyncedWallet> todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly = SyncedWallet.findWithQuery(SyncedWallet.class,
                    "SELECT * FROM SYNCED_WALLET " +
                            "WHERE " +
                            "new_payday != " + dayOfYear +
                            " AND" +
                            " (" +
                            " payment_option = '2'" +
                            " OR payment_option = '3'" +
                            " )" +
                            " AND" +
                            " (" +
                            " first_payday_weekly = " + dayOfMonth +
                            " OR second_payday_weekly = " + dayOfMonth +
                            " OR pay_day = " + dayOfMonth +
                            " )" +
                            " AND show_color = '0' " +
                            " AND status == '1'");

            for (int i = 0; i < todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.size(); i++) {

                Util.Log.ih("FORTNIGHTLYMONTHLY size: " + todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.size());
                Util.Log.ih("FORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLYFORTNIGHTLY");
                Util.Log.ih("MONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLYMONTHLY");

                //this is need it when a contract is paid before its payday (within paymentOption range
                // weekly, fortnightly or monthly)
                //so that it won't show again when its payday comes
                boolean removeContract = false;
                if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).isPaymentMadeBefore()) {

                    Util.Log.ih("payment made before...........................");

                    int newPayday = Integer.valueOf(todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getNewPayday());

                    //TODO: ADD VALIDATION FOR ANOTHER YEAR, WHEN MATH IS LESS THAN ZERO --> DONE
                    int newDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
                    int a = 1;
                    for (; ; a++) {
                        newPayday++;
                        Util.Log.ih("AAAAA = " + a);

                        //if (newPayday == 366)
                        //    newPayday = 365;

                        if (newPayday > checkLeapYear()) {
                            newPayday = 1;
                        }
                        if (newPayday == newDayOfYear) {
                            break;
                        }
                    }
                    Util.Log.ih("AAAAA = " + a);
                    if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getPaymentOption().equals("2")) {
                        //removeContract = a <= 15;
                        removeContract = a <= 7;
                    } else if (todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getPaymentOption().equals("3")) {
                        removeContract = a <= 20;
                    }
                    Util.Log.ih("remove contract = " + removeContract);
                }

                //if removeContract is true, means that this contract was paid before payday
                //and does not have to be shown on today's list
                if (removeContract) {//if ( (j > 0 && j < 31) || (j == 0 && paymentMadeBefore) ){
                    Util.Log.ih("removeContract ----------> removing i = " + i);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.remove(i);
                    i--;
                } else {
                    Util.Log.ih("not removing i = " + i);

                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setFPA(false);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setFPAPending(false);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setPaymentMadeBefore(false);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setPaymentMade(false);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setShowColor(true);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setVisitMade(false);
                    //todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setNotVisited(true);
                    //todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setVisitCounter(0);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setSecondVisitDay(0);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setNewPayday("");
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setShowAgain(false);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setNotVisited(true);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setDayCounter(dayCounter);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                    todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).save();

                }
            }

            /*
            for (int i = 0; i < todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.size(); i++) {
                Util.Log.ih("todayClients " + i + " = " + todayClientsWithVisitsOrPaymentMadeFortnightlyAndMontly.get(i).getContractID());
            }
            */

            //preferencesToShowWallet.setResetContractsSynced(dayOfYear);
        //} else {
        //    Util.Log.ih("do not synchronizing");
        //}
    }

    /**
     * call this not to show contracts that have not been visited from yesterday (yellow)
     * and before yesterday (red)
     */
    public static void resetContractsNotVisitedFromYesterdayAndBeforeYesterday(){

        PreferencesToShowWallet preferencesToShowWallet = new PreferencesToShowWallet(ApplicationResourcesProvider.getContext());
        long dayCounter = preferencesToShowWallet.getDayCounter();

        List<SyncedWallet> clientsToShowAgainYesterday = SyncedWallet.findWithQuery(SyncedWallet.class,
                "SELECT * FROM SYNCED_WALLET " +
                        "WHERE show_again = " + "1" +
                        " AND day_counter == " + (dayCounter - 1) + " " +
                        " AND status == '1' " +
                        "ORDER BY payment_option DESC");
        for (int i = 0; i < clientsToShowAgainYesterday.size(); i++){
            clientsToShowAgainYesterday.get(i).setPaymentMadeBefore(false);
            clientsToShowAgainYesterday.get(i).setShowAgain(false);
            clientsToShowAgainYesterday.get(i).setNotVisited(false);
            clientsToShowAgainYesterday.get(i).setDayCounter(dayCounter);
            clientsToShowAgainYesterday.get(i).setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);
            clientsToShowAgainYesterday.get(i).setPaymentMade(false);
            clientsToShowAgainYesterday.get(i).setShowColor(true);
            clientsToShowAgainYesterday.get(i).setVisitMade(false);
            clientsToShowAgainYesterday.get(i).setVisitCounter(0);
            clientsToShowAgainYesterday.get(i).setNewPayday("");
            clientsToShowAgainYesterday.get(i).save();
        }



        List<SyncedWallet> clientsToShowAgainBeforeYesterday = SyncedWallet.findWithQuery(SyncedWallet.class,
                "SELECT * FROM SYNCED_WALLET WHERE show_again = 1 AND day_counter < " + (dayCounter - 1) + "" +
                        " AND status == '1'" +
                        " ORDER BY payment_option DESC");
        for (int i = 0; i < clientsToShowAgainBeforeYesterday.size(); i++){
            clientsToShowAgainBeforeYesterday.get(i).setPaymentMadeBefore(false);
            clientsToShowAgainBeforeYesterday.get(i).setShowAgain(false);
            clientsToShowAgainBeforeYesterday.get(i).setNotVisited(false);
            clientsToShowAgainBeforeYesterday.get(i).setDayCounter(dayCounter);
            clientsToShowAgainBeforeYesterday.get(i).setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);
            clientsToShowAgainBeforeYesterday.get(i).setPaymentMade(false);
            clientsToShowAgainBeforeYesterday.get(i).setShowColor(true);
            clientsToShowAgainBeforeYesterday.get(i).setVisitMade(false);
            clientsToShowAgainBeforeYesterday.get(i).setVisitCounter(0);
            clientsToShowAgainBeforeYesterday.get(i).setNewPayday("");
            clientsToShowAgainBeforeYesterday.get(i).save();
        }
    }

    public static List<SyncedWallet> getClientsFromSyncedWalletWithoutAnalysis(){
        //contracts without payday analysis
        List<SyncedWallet> clientsWithoutPaydayAnalysis = SyncedWallet.findWithQuery(SyncedWallet.class,
                "SELECT * FROM SYNCED_WALLET " +
                        "WHERE " +
                        "pay_day = '' " +
                        "AND first_payday_weekly = '' " +
                        "AND second_payday_weekly = ''");
        for (int i = 0; i < clientsWithoutPaydayAnalysis.size(); i++){
            clientsWithoutPaydayAnalysis.get(i).setColor(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT);
            clientsWithoutPaydayAnalysis.get(i).save();
        }

        return clientsWithoutPaydayAnalysis;
    }

    public static String[] getContractsNotVisited() {

        /*
        long greenYellowOrRedsWeekly = SyncedWallet.count(SyncedWallet.class, "not_visited = ? AND payment_option = ? AND status = ? AND fpa = ? AND color != ?", new String[]{"1", "1", "1", "0", SyncedWallet.COLOR_DEFAULT_CONTRACT});
        Util.Log.ih("not visited greenYellowOrReds = " + greenYellowOrRedsWeekly);
        long redFPAWeekly = SyncedWallet.count(SyncedWallet.class, "payment_option = ? AND fpa = ? AND fpa_pending = ?", new String[]{"1", "1", "1"});
        Util.Log.ih("not visited redFPA = " + redFPAWeekly);
        long graysWeekly = SyncedWallet.count(SyncedWallet.class, "payment_option = ? AND color = ? AND fpa = ? AND fpa_pending = ?", new String[]{"1", SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT, "0", "0"});
        Util.Log.ih("not visited weekly = " + graysWeekly);
        greenYellowOrRedsWeekly = greenYellowOrRedsWeekly + redFPAWeekly + graysWeekly;
        */

        long weekly = SyncedWallet.count(SyncedWallet.class, "payment_option = ? AND color != ? AND color != ? AND ((fpa = ? AND fpa_pending = ?) OR (fpa = ? AND fpa_pending = ?))", new String[]{"1", SyncedWallet.COLOR_DEFAULT_CONTRACT, SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT, "1", "1", "0", "0"});
        Util.Log.ih("semanales = " + weekly);

        long forthnightly = SyncedWallet.count(SyncedWallet.class, "payment_option = ? AND color != ? AND color != ? AND ((fpa = ? AND fpa_pending = ?) OR (fpa = ? AND fpa_pending = ?))", new String[]{"2", SyncedWallet.COLOR_DEFAULT_CONTRACT, SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT, "1", "1", "0", "0"});
        Util.Log.ih("quincenales = " + forthnightly);

        long monthly = SyncedWallet.count(SyncedWallet.class, "payment_option = ? AND color != ? AND color != ? AND ((fpa = ? AND fpa_pending = ?) OR (fpa = ? AND fpa_pending = ?))", new String[]{"3", SyncedWallet.COLOR_DEFAULT_CONTRACT, SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT, "1", "1", "0", "0"});
        Util.Log.ih("mensuales = " + monthly);




        /*
        long greenYellowOrRedsFortnightly = SyncedWallet.count(SyncedWallet.class, "not_visited = ? AND payment_option = ? AND status = ? AND fpa = ?", new String[]{"1", "2", "1", "0"});
        Util.Log.ih("not visited fortnightly = " + greenYellowOrRedsFortnightly);
        long redFPAFortnightly = SyncedWallet.count(SyncedWallet.class, "payment_option = ? AND fpa = ? AND fpa_pending = ?", new String[]{"2", "1", "1"});
        Util.Log.ih("not visited fortnightly = " + redFPAFortnightly);
        long graysFortnightly = SyncedWallet.count(SyncedWallet.class, "payment_option = ? AND color = ? AND fpa = ? AND fpa_pending = ?", new String[]{"2", SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT, "0", "0"});
        Util.Log.ih("not visited fortnightly = " + graysFortnightly);
        greenYellowOrRedsFortnightly = greenYellowOrRedsFortnightly + redFPAFortnightly + graysFortnightly;

        long greenYellowOrRedsMonthly = SyncedWallet.count(SyncedWallet.class, "not_visited = ? AND payment_option = ? AND status = ? AND fpa = ?", new String[]{"1", "3", "1", "0"});
        Util.Log.ih("not visited monthly = " + greenYellowOrRedsMonthly);
        long redFPAMonthly = SyncedWallet.count(SyncedWallet.class, "payment_option = ? AND fpa = ? AND fpa_pending = ?", new String[]{"3", "1", "1"});
        Util.Log.ih("not visited monthly = " + redFPAMonthly);
        long graysMonthly = SyncedWallet.count(SyncedWallet.class, "payment_option = ? AND color = ? AND fpa = ? AND fpa_pending = ?", new String[]{"3", SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT, "0", "0"});
        Util.Log.ih("not visited monthly = " + graysMonthly);
        greenYellowOrRedsMonthly = greenYellowOrRedsMonthly + redFPAMonthly + graysMonthly;
        */

        return new String[]{""+weekly, ""+forthnightly, ""+monthly};
    }

    public static String getContractsNotVisitedFullInfo() {

        String info = "";

        List<SyncedWallet> weekly = SyncedWallet.find(SyncedWallet.class, "payment_option = ? AND color != ? AND color != ? AND ((fpa = ? AND fpa_pending = ?) OR (fpa = ? AND fpa_pending = ?))", "1", SyncedWallet.COLOR_DEFAULT_CONTRACT, SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT, "1", "1", "0", "0");
        Util.Log.ih("semanales = " + weekly);

        List<SyncedWallet> forthnightly = SyncedWallet.find(SyncedWallet.class, "payment_option = ? AND color != ? AND color != ? AND ((fpa = ? AND fpa_pending = ?) OR (fpa = ? AND fpa_pending = ?))", "2", SyncedWallet.COLOR_DEFAULT_CONTRACT, SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT, "1", "1", "0", "0");
        Util.Log.ih("quincenales = " + forthnightly);

        List<SyncedWallet> monthly = SyncedWallet.find(SyncedWallet.class, "payment_option = ? AND color != ? AND color != ? AND ((fpa = ? AND fpa_pending = ?) OR (fpa = ? AND fpa_pending = ?))", "3", SyncedWallet.COLOR_DEFAULT_CONTRACT, SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT, "1", "1", "0", "0");
        Util.Log.ih("mensuales = " + monthly);

        for ( int i = 0; i < weekly.size(); i++ ) {
            weekly.get(i).deserializeClient();
            info += "Contrato: " + weekly.get(i).getClient().getSerie() + weekly.get(i).getClient().getNumberContract() + "\n";
        }

        for ( int i = 0; i < forthnightly.size(); i++ ) {
            forthnightly.get(i).deserializeClient();
            info += "Contrato: " + forthnightly.get(i).getClient().getSerie() + forthnightly.get(i).getClient().getNumberContract() + "\n";
        }

        for ( int i = 0; i < monthly.size(); i++ ) {
            monthly.get(i).deserializeClient();
            info += "Contrato: " + monthly.get(i).getClient().getSerie() + monthly.get(i).getClient().getNumberContract() + "\n";
        }

        return info;
    }

    /**
     * gets all deposits not synced yet
     * @return
     */
    public static List<ModelDeposit> getAllDepositsNotSynced() {
        List<Deposits> depositsTemp = Deposits.find(Deposits.class, "sync = ?", "0");

        if (depositsTemp.size() > 0){

            List<ModelDeposit> deposits = new ArrayList<>();

            for (Deposits d: depositsTemp){
                ModelDeposit singleDeposit = new ModelDeposit();
                singleDeposit.setAmount(d.getAmount());
                singleDeposit.setCompany(d.getCompany());
                singleDeposit.setDate(d.getDate());
                singleDeposit.setNumberBank(d.getNumberBank());
                singleDeposit.setReference(d.getReference());
                singleDeposit.setPeriodo(d.getPeriodo());
                singleDeposit.setDepositDate(d.getDepositDate());

                deposits.add(singleDeposit);
            }

            return deposits;
        }
        return null;
    }

    /**
     * gets all payments not synced yet
     * @return
     */
    public static List<ModelPayment> getAllPaymentsNotSynced(){

        List<Payments> paymentsTemp = Payments.find(Payments.class, "sync = ?", "0");
        if (paymentsTemp.size() > 0)
        {
            List<ModelPayment> payments = new ArrayList<>();

            for (Payments p: paymentsTemp){

                ModelPayment singlePayment = new ModelPayment();
                int status = Integer.parseInt(p.getStatus());

                singlePayment.setCompany(p.getCompany());
                singlePayment.setLatitude(p.getLatitude());
                singlePayment.setLongitude(p.getLongitude());
                singlePayment.setStatus(p.getStatus());
                singlePayment.setIdCollector(p.getIdCollector());
                singlePayment.setAmount(p.getAmount());
                singlePayment.setIdClient(p.getIdClient());
                singlePayment.setDate(p.getDate());
                singlePayment.setSerie(p.getSerie());
                singlePayment.setTypeBD(p.getTypeBD());
                singlePayment.setPeriodo(p.getPeriodo());
                if (status == 1) {
                    singlePayment.setFolio(p.getSeriePayment() + p.getFolio());
                } else {
                    singlePayment.setFolio(p.getFolio());
                }
                singlePayment.setCopies(p.getCopies());
                singlePayment.setPaymentReference(p.getPaymenrReference());
                singlePayment.setTipo_cobro(p.getTipoCobro());


                payments.add(singlePayment);
            }

            return payments;
        }
        return new ArrayList<>();
    }

    /**
     * gets printer's mac address
     * @return
     */
    public static String getPrinterMacAddress(){
        List<Collectors> collector = Collectors.listAll(Collectors.class);
        if (collector.size() > 0){
            return collector.get(0).macAddress;
            //return "00:19:0E:A4:16:E0";
        }
        return null;
    }

    /**
     * sets client cellphone number
     * @return
     */
    public static void saveClientCellPhoneNumber(long contractID, String contractSerie, String contractNumber, String cellPhoneNumber){
        ClientsCellPhoneNumbers clientsCellPhoneNumbers = new ClientsCellPhoneNumbers(contractID, contractSerie, contractNumber, cellPhoneNumber);
        clientsCellPhoneNumbers.save();
    }

    /**
     * gets radio range
     * @return
     */
    public static String getRadioToShowNearbyClients(){
        List<Collectors> collector = Collectors.listAll(Collectors.class);
        if (collector.size() > 0){
            return collector.get(0).radio;
        }
        return "0.0";
    }

    /**
     * gets total canceled
     * @return
     */
    public static float getTotalCanceled(int period){
        float amount = 0;
        List<Canceled> canceled = Canceled.find(Canceled.class, "period = ?", ""+period);
        if (canceled.size() > 0){
            for (Canceled c: canceled){
                amount += Float.parseFloat(c.amount);
            }
        }
        return amount;
    }

    /**
     * gets total canceled by company given
     * @param idCompany
     * @return
     */
    /*public static float getTotalCanceledByCompany(String idCompany)
    {
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);

        float amount = 0;
        List<Canceled> canceled = Canceled.find(Canceled.class, "company = ? AND period = ?", idCompany, ""+periodo);
        if (canceled.size() > 0){
            for (Canceled c: canceled){
                amount += Float.parseFloat(c.amount);
            }
        }
        return amount;
    }*/

    public static float getTotalCanceledByCompany(String idCompany)
    {
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);

        float amount = 0;
        String query = "SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE empresa= '"+ idCompany +"' and periodo = '" + periodo +"'";
        List<MontosTotalesDeEfectivo> canceled = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
        if (canceled.size() > 0)
        {
            amount = Float.parseFloat(canceled.get(0).getCancelados());
        }
        return amount;
    }

    /**
     * gets total deposited
     * @return
     */
    /*public static double getTotalDeposits(){
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);

        double amount = 0.0;
        List<Deposits> deposits = Deposits.find(Deposits.class, "periodo = ?", ""+periodo);
        if (deposits.size() > 0){
            for (Deposits d: deposits){
                amount += Double.parseDouble(d.amount);
            }
        }
        return amount;
    }*/

    /**
     * gets total deposited
     * @return
     */
    public static double getTotalDeposits(){
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);

        double amount = 0.0;
        List<MontosTotalesDeEfectivo> deposits = MontosTotalesDeEfectivo.find(MontosTotalesDeEfectivo.class, "periodo = ?", ""+periodo);
        if (deposits.size() > 0)
        {
            //verificar problema con parseo de datos
            for (MontosTotalesDeEfectivo c: deposits){
                amount += Double.parseDouble(c.depositos);
            }
        }
        return amount;
    }

    public static double getTotalEfectivoAcumulado()
    {
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);
        double total = 0.0;
        String query = "SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE periodo ='"+ periodo + "'";
        List<MontosTotalesDeEfectivo> listaMontos = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
        if (listaMontos.size() > 0)
        {
            for (MontosTotalesDeEfectivo m: listaMontos)
            {
                total += Double.parseDouble(m.cobros_menos_depositos);
            }
        }
        return total;
    }

    public static double getTotalEfectivoAcumuladoPorPeriodo(int periodo)
    {
        double total = 0.0;
        String query = "SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE periodo ='"+ periodo + "'";
        List<MontosTotalesDeEfectivo> listaMontos = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
        if (listaMontos.size() > 0)
        {
            for (MontosTotalesDeEfectivo m: listaMontos)
            {
                total += Double.parseDouble(m.cobros_menos_depositos);
            }
        }
        return total;
    }

    public static double getTotalEfectivoAcumuladoPorEmpresa(String idCompany)
    {
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);

        double totalCobros = 0.0;
        double totalCancelados = 0.0;
        double totalDepositos = 0.0;
        double total = 0.0;
        String query ="SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE periodo='"+ periodo +"' and empresa ='"+ idCompany + "'";

        List<MontosTotalesDeEfectivo> listaMontos = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
        if (listaMontos.size() > 0)
        {
            total = Double.parseDouble(listaMontos.get(0).getCobros_menos_depositos());
        }
        return total;
    }



    /*public static double getTotalDepositsByPeriodo(int period){
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        //int periodo = efectivoPreference.getInt("periodo", 0);

        double amount = 0.0;
        List<Deposits> deposits = Deposits.find(Deposits.class, "periodo = ?", ""+period);
        if (deposits.size() > 0){
            for (Deposits d: deposits){
                amount += Double.parseDouble(d.amount);
            }
        }
        return amount;
    }*/


    /**
     * gets all deposits by company given
     * @param idCompany
     * @return
     */
    /*public static double getTotalDepositsByCompany(String idCompany){
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);

        double amount = 0.0;
        List<Deposits> deposits = Deposits.find(Deposits.class, "company = ? AND periodo = ?", idCompany, ""+periodo);
        if (deposits.size() > 0){
            for (Deposits c: deposits){
                amount += Double.parseDouble(c.amount);
            }
        }
        return amount;
    }*/


    public static double getTotalDepositsByCompany(String idCompany){
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);

        double amount = 0.0;
        String query ="SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE empresa='" + idCompany + "' and periodo ='"+periodo+"'";

        List<MontosTotalesDeEfectivo> listaMontoDepositos = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
        if (listaMontoDepositos.size() > 0)
        {
            amount = Float.valueOf(listaMontoDepositos.get(0).getDepositos());
        }
        return amount;
    }




    public static double montosCobrosMasCancelados(String idCompany){
        SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        int periodo = efectivoPreference.getInt("periodo", 0);

        double amount = 0.0;
        String query ="SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE empresa='" + idCompany + "' and periodo ='"+periodo+"'";

        List<MontosTotalesDeEfectivo> listaMontoDepositos = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
        if (listaMontoDepositos.size() > 0)
        {
            amount = Float.valueOf(listaMontoDepositos.get(0).getCobros_mas_cancelados());
        }
        return amount;
    }







    /**
     * gets total by company
     * @param idCompany
     * @return
     */
    public static double getTotalByCompany(String idCompany){
        double amount = 0.0;
        List<Payments> payments = Payments.find(Payments.class, "company = ?", idCompany);
        if (payments.size() > 0){
            for (Payments c: payments){
                amount += Double.parseDouble(c.amount);
            }
        }
        return amount;
    }

    public static boolean getSinglePayment(String folio, String date){
        List<Payments> singlePayment = Payments.find(Payments.class,
                "date = ? AND folio = ?", date, folio);

        return singlePayment.size() > 0;
    }

    public static boolean getSingleVisit(String date){
        List<Payments> singleVisit = Payments.find(Payments.class,
                "date = ?", date);

        return singleVisit.size() > 0;
    }

    public static boolean getSinglePayment(String date){
        List<Payments> singlePayment = Payments.find(Payments.class,
                "date = ?", date);

        return singlePayment.size() > 0;
    }

    public static String getCommentsByContractID(String contractID){
        List<Clients> clients = Clients.find(Clients.class, "id_contract = ?", contractID);
        if (clients.size() > 0){
            return clients.get(0).getComment();
        }
        return "";
    }

    /**
     * checks if any client is saved in db
     * @return
     */
    public static boolean isThereAnyCollector(){
        return Collector.count(Collector.class, null, null) > 0;
    }

    public static boolean clientsFromSameCollector(String collectorNumber){
        return Clients.count(Clients.class, "number_collector = ?", new String[]{collectorNumber}) > 0;
    }

    /**
     * checks if any collector is saved in db
     * @return
     */
    public static boolean isThereAnyClient(){
        long total;
        total = Clients.count(Clients.class, null, null);
        return total > 0;
    }

    /**
     * checks if any deposit is not synced yet
     * @return
     */
    public static boolean isThereAnyDepositNotSynced(){
        List<Deposits> deposits = Deposits.find(Deposits.class, "sync = ?", "0");
        return deposits.size() > 0;
    }

    /**
     * marks as synced a deposit after
     * have been sent to server
     * @param date
     */
    public static void updateDepositsAsSynced(String date){
        List<Deposits> deposits = Deposits.find(Deposits.class, "date = ?", date);
        if (deposits.size() > 0){
            deposits.get(0).sync = true;
            deposits.get(0).save();
        }
    }

    /**
     * marks as synced a payment after
     * have been sent to server
     * @param date
     */
    public static void updatePaymentsAsSynced(JSONObject json)
    {
        //String fecha ="";
            try
            {
                if (json.has("success"))
                {
                    JSONArray jsonSuccess = json.getJSONArray("success");
                    if(jsonSuccess.length()>0)
                    {
                        for(int i=0; i<=jsonSuccess.length()-1; i++)
                        {
                            JSONObject object = jsonSuccess.getJSONObject(i);
                            //fecha = object.getString("fecha");
                            String query="UPDATE PAYMENTS SET sync=1 WHERE date='" + object.getString("fecha") +"'";
                            Payments.executeQuery(query);
                        }
                    }
                    else
                        Log.i("SIN DATOS", "success no contiene datos json");
                }
                else
                {
                    Log.i("ENTRO A -->", "result, pagos no sincronizados");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    public static boolean updatePaymentAsNotSyncedToSendAgain(String serie, String folio){
        List<Payments> payments = Payments.find(Payments.class, "serie = ? AND folio = ?", serie, folio);
        if (payments.size() > 0){
            payments.get(0).sync = false;
            payments.get(0).save();
            return true;
        }
        return false;
    }

    public static String updateAllPaymentAsNotSyncedToSendAgain(){
        List<Payments> payments = Payments.listAll( Payments.class );

        String paymentString = "";

        if (payments.size() > 0){
            for (Payments p: payments) {
                p.setSync( false );
                p.save();

                paymentString += p.getSeriePayment() + p.getFolio() + " - " + p.getPeriodo() + "\n";
            }
            return paymentString;
        }
        return paymentString;
    }

    public static List<ModelPayment> getAllPayments(){
        List<Payments> paymentsTemp = Payments.listAll(Payments.class);
        if (paymentsTemp.size() > 0){

            List<ModelPayment> payments = new ArrayList<>();

            for (Payments p: paymentsTemp){

                ModelPayment singlePayment = new ModelPayment();
                int status = Integer.parseInt(p.getStatus());

                singlePayment.setCompany(p.getCompany());
                singlePayment.setLatitude(p.getLatitude());
                singlePayment.setLongitude(p.getLongitude());
                singlePayment.setStatus(p.getStatus());
                singlePayment.setIdCollector(p.getIdCollector());
                singlePayment.setAmount(p.getAmount());
                singlePayment.setIdClient(p.getIdClient());
                singlePayment.setDate(p.getDate());
                singlePayment.setSerie(p.getSerie());
                singlePayment.setTypeBD(p.getTypeBD());
                if (status == 1) {
                    singlePayment.setFolio(p.getSeriePayment() + p.getFolio());
                } else {
                    singlePayment.setFolio(p.getFolio());
                }
                singlePayment.setCopies(p.getCopies());
                singlePayment.setPeriodo(p.getPeriodo());

                payments.add(singlePayment);
            }

            return payments;
        }
        return new ArrayList<>();
    }

    /**
     * sets notification status = 1
     * which means that a notification was viewed
     * @param idNotification
     */
    public static void updateNotificationAsViewed(int idNotification){
        List<Notifications> notification = Notifications.find(Notifications.class, "id_notification = ?", Integer.toString(idNotification));
        if (notification.size() > 0){
            notification.get(0).status = 1;
            notification.get(0).save();
        }
    }

    public static boolean updateVisitMade(String contractID){
        List<SyncedWallet> client = SyncedWallet.find(SyncedWallet.class, "contract_id = ?", contractID);

        Calendar calendar = Calendar.getInstance();

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        /*
        dayOfWeek -= 1;
        if (dayOfWeek == 0){
            dayOfWeek = 7;
        }
        */
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);




        if (client.size() > 0) {

            if ( (client.get(0).isFPAPending() && !client.get(0).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT) )
                    || (!client.get(0).isFPA() && !client.get(0).isFPAPending() && !client.get(0).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT))
                    ) {
                Util.Log.ih("making visit...");
                client.get(0).setVisitMade(true);
                client.get(0).setNotVisited(true);
                //if it's blue and is paid, reset visit counter
                /*
                if ( client.get(0).isPaymentMade() && !client.get(0).isShowColor() ){
                    client.get(0).setVisitCounter(0);
                }
                */
                client.get(0).setVisitCounter(client.get(0).getVisitCounter() + 1);


                //if (Integer.valueOf(client.get(0).getPaymentOption()) == 1){
                //    client.get(0).setNewPayday( "" + calendar.get(Calendar.DAY_OF_WEEK) );
                //}
                //else {
                //    client.get(0).setNewPayday("" + calendar.get(Calendar.DAY_OF_MONTH));
                //}
                client.get(0).setNewPayday("" + calendar.get(Calendar.DAY_OF_YEAR));

                //set next payday for contracts without payday analysis
                if (client.get(0).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT)) {
                    if (client.get(0).getPaymentOption().equals("1")) {
                        client.get(0).setPayDay("" + dayOfWeek);
                    } else if (client.get(0).getPaymentOption().equals("2")) {
                        int a = calendar.get(Calendar.DAY_OF_MONTH);
                        //sum 15 to actual day number
                        for (int i = 0; i < 15; i++) {
                            a += 1;
                            if (a == calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
                                a = 0;
                            }
                        }
                        //client.get(0).setPayDay("" + a);
                        client.get(0).setFirstPaydayWeekly("" + dayOfMonth);
                        client.get(0).setSecondPaydayWeekly("" + a);
                    } else {
                        client.get(0).setPayDay("" + dayOfMonth);
                    }

                    client.get(0).setFPA(false);
                    client.get(0).setFPAPending(false);
                    client.get(0).setDayCounter(new PreferencesToShowWallet(ApplicationResourcesProvider.getContext()).getDayCounter());
                    client.get(0).setStatus("1");
                    client.get(0).setShowColor(true);
                    client.get(0).setPaymentMadeBefore(false);
                    client.get(0).setShowAgain(true);
                    client.get(0).setDayCounter(new PreferencesToShowWallet(ApplicationResourcesProvider.getContext()).getDayCounter());
                    client.get(0).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                }


                if (client.get(0).getVisitCounter() >= 2) {
                    client.get(0).setShowColor(false);
                    client.get(0).setNotVisited(false);
                    client.get(0).setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);
                    client.get(0).setSecondVisitDay(calendar.get(Calendar.DAY_OF_YEAR));

                    //if a semanal contract is paid, always set as paid before
                    //it'll be reseted every week either

                    //if ( client.get(0).getPaymentOption().equals("1")) {
                        client.get(0).setPaymentMadeBefore(true);
                    //} else if ( client.get(0).getPaymentOption().equals("2")) {
                    //    client.get(0).setPaymentMadeBefore(true);
                    //} else if ( client.get(0).getPaymentOption().equals("3")) {
                    //    client.get(0).setPaymentMadeBefore(true);
                    //}


                    if ( !client.get(0).isShowAgain() ){
                        Util.Log.ih("making payment before payday...");
                        client.get(0).setPaymentMadeBefore(true);
                    }
                    client.get(0).setShowAgain(false);
                }


                client.get(0).save();

                /******************************************
                 * TODO: DELETE THIS WHEN FINISH DEBUGGING
                 * ****************************************
                 */
                //LogRegister.saveContractsIntoFile(contractID, "updatevisitmade", true, client.get(0));
            } else {
                Util.Log.ih("not making visit...");

                /******************************************
                 * TODO: DELETE THIS WHEN FINISH DEBUGGING
                 * ****************************************
                 */
                //LogRegister.saveContractsIntoFile(contractID, "updatevisitmade but not modified becasuse condition", true, client.get(0));
            }
            return true;
        }
        else {
            Util.Log.ih("NO SE PUDO ENCONTRAR EL CONTRATO : " + contractID + " ===================================================");

            /******************************************
             * TODO: DELETE THIS WHEN FINISH DEBUGGING
             * ****************************************
             */
            //LogRegister.saveContractsIntoFile(contractID, "updatevisitmade", false);
        }
        return false;
    }

    public static boolean checkIfPaymentExists(String folio, String serie)
    {
        List<Payments> payments = Payments.find(Payments.class, "folio = ? and serie_payment = ?", folio, serie);

        return payments.size() > 0;
    }

    public static String getContractID(String contractNumber){
        List<Clients> clientFromClients = Clients.find(Clients.class, "number_contract = ?", contractNumber);

        if (clientFromClients.size() > 0) {
            return clientFromClients.get(0).getIdContract();
        }
        return null;
    }

    public static boolean isthereAnyVisitMade30MinutesAgo(String clientID){
        //List<Payments> visitMade30MinutesAgo = Payments.findWithQuery(Payments.class, "SELECT id_client FROM PAYMENTS WHERE id_client = ? and status >= 2 and status != 7 and DATE BETWEEN datetime('now','LocalTime','-30 minutes') and datetime('now','LocalTime')", clientID);
        List<Payments> visitMade30MinutesAgo = Payments.find(Payments.class, "id_client = ? and status >= 2 and status != 7 and DATE BETWEEN datetime('now','LocalTime','-30 minutes') and datetime('now','LocalTime')", clientID);
        return visitMade30MinutesAgo.size() > 0;
        //return false;
    }

    public static void getVisitOrPaymentMadeAfterCancellation(String date, String concatSerieContractNumber){
        //List<Payments> visitOrPaymentMadeAfterCancellation = Payments.findWithQuery(Payments.class,
        //        "SELECT * FROM PAYMENTS where date > ? and serie || id_client = ?", date, concatSerieContractNumber);

        List<Payments> payments = Payments.findWithQuery(Payments.class,
                "SELECT * FROM PAYMENTS where serie || id_client = "
                        + "'" + concatSerieContractNumber + "'" +
                        " ORDER BY id DESC");

        int visitCounter = 0;
        boolean paymentMade = false;

        if (payments.size() > 0){
            for (Payments p: payments){
                if (p.getFolio().equals("----")){
                    visitCounter++;
                } else {
                    String[] whereArgs = {concatSerieContractNumber, p.getAmount(), p.getDate()};
                    boolean isCancelled = Canceled.count(Canceled.class, "number_client = ? AND amount = ? AND cancel_date = ?", whereArgs) > 0;
                    if ( !isCancelled ) {
                        paymentMade = true;
                    }
                }
            }

            Util.Log.ih("payment made = " + paymentMade);
            Util.Log.ih("visit counter = " + visitCounter);

            if ( !paymentMade ) {

                Util.Log.ih("payment not made - cheking number of visits");

                List<Clients> client = Clients.find(Clients.class,
                        "serie || number_contract = ?", concatSerieContractNumber);

                if (client.size() > 0) {

                    List<SyncedWallet> contractFromSemaforo = SyncedWallet.find(SyncedWallet.class,
                            "contract_id = ?", client.get(0).getIdContract());

                    contractFromSemaforo.get(0).setPaymentMadeBefore(false);
                    contractFromSemaforo.get(0).setPaymentMade(false);
                    contractFromSemaforo.get(0).setNewPayday("");
                    contractFromSemaforo.get(0).setShowAgain(true);
                    contractFromSemaforo.get(0).setVisitMade(false);
                    contractFromSemaforo.get(0).setVisitCounter(visitCounter);

                    if (visitCounter < 2) {

                        Util.Log.ih("payment not made - changing variables to be shown on semaforo");

                        contractFromSemaforo.get(0).setShowColor(true);
                        contractFromSemaforo.get(0).setNotVisited(true);
                    } else {
                        contractFromSemaforo.get(0).setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                    }
                    contractFromSemaforo.get(0).save();
                }
            }
        }
    }

    public static boolean updatePaymentMade(String contractID){
        List<SyncedWallet> client = SyncedWallet.find(SyncedWallet.class, "contract_id = ?", contractID);

        Calendar calendar = Calendar.getInstance();

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        /*
        dayOfWeek -= 1;
        if (dayOfWeek == 0){
            dayOfWeek = 7;
        }
        */
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);



        Util.Log.ih("updatePaymentMade");
        if (client.size() > 0){
            Util.Log.ih("size > 0");


            //this is needed when a contract is paid before its payday (within paymentOption range>:
            //weekly, fortnightly or monthly)
            //so that it won't show again when its payday comes
            client.get(0).setPaymentMadeBefore(true);
            //if it's a new contract that hasn't been shown on wallet
            //is blue and has no visits
            //or data was erased

            //if a semanal contract is paid, always set as paid before
            //it'll be reseted every week either

            if ( client.get(0).getPaymentOption().equals("1")) {
                client.get(0).setPaymentMadeBefore(true);
            }

            /*TODO THING ADDED AT VERSION 3.5.5.1
             */
            //client.get(0).setPaymentMadeBefore(true);
            /*TODO REMOVE IF DOESNT WORK 3.5.5.1
             */



            if ( !client.get(0).isShowAgain() && client.get(0).getVisitCounter() < 2 ){
                Util.Log.ih("making payment before payday...");
                client.get(0).setPaymentMadeBefore(true);
            }
            //if a payment is made when is already blue (paid o visited twice)

            else if ( !client.get(0).isShowAgain() && client.get(0).isPaymentMade() ){
                Util.Log.ih("making payment before payday...");
                client.get(0).setPaymentMadeBefore(true);
            }


            //the contrat is blue either because was paid or visited twice
            /*
            if ( !client.get(0).isShowAgain() ){
                Util.Log.ih("making payment before payday...");
                client.get(0).setPaymentMadeBefore(true);
            }
            */

            //payment stuff
            client.get(0).setPaymentMade(true);
            client.get(0).setShowAgain(false);
            client.get(0).setShowColor(false);
            //client.get(0).setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);


            //visit stuff
            //client.get(0).setVisitMade(true);
            client.get(0).setNotVisited(false);
            //client.get(0).setVisitCounter(client.get(0).getVisitCounter() + 1);
            client.get(0).setVisitCounter(0);

            /*
            if (Integer.valueOf(client.get(0).getPaymentOption()) == 1){
                client.get(0).setNewPayday( "" + calendar.get(Calendar.DAY_OF_WEEK) );
            }
            else {
                client.get(0).setNewPayday( "" + calendar.get(Calendar.DAY_OF_MONTH) );
            }
            */
            client.get(0).setNewPayday( "" + calendar.get(Calendar.DAY_OF_YEAR) );


            //set next payday for contracts without payday analysis
            if (client.get(0).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT) || client.get(0).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT)){
                //weekly
                if (client.get(0).getPaymentOption().equals("1")) {
                    client.get(0).setPayDay("" + dayOfWeek);
                }
                //fortnighlty
                else if (client.get(0).getPaymentOption().equals("2")){
                    int a = calendar.get(Calendar.DAY_OF_MONTH);
                    //sum 15 to actual day number
                    for (int i = 0; i < 15 ; i++){
                        a += 1;
                        if (a == calendar.getActualMaximum(Calendar.DAY_OF_MONTH)){
                            a = 0;
                        }
                    }
                    //client.get(0).setPayDay("" + a);
                    client.get(0).setFirstPaydayWeekly("" + dayOfMonth);
                    client.get(0).setSecondPaydayWeekly("" + a);
                }
                //monthly
                else {
                    client.get(0).setPayDay("" + dayOfMonth);
                }

                client.get(0).setDayCounter(new PreferencesToShowWallet(ApplicationResourcesProvider.getContext()).getDayCounter());
                client.get(0).setStatus("1");
                client.get(0).setFPA(false);
                client.get(0).setFPAPending(false);
            }



            client.get(0).setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);



            client.get(0).save();
            Util.Log.ih("CONTRACTO ACTUALIZADO: " + contractID + "  ===============");

            /******************************************
             * TODO: DELETE THIS WHEN FINISH DEBUGGING
             * ****************************************
             */
            //LogRegister.saveContractsIntoFile(contractID, "updatePaymentMade", true, client.get(0));
            return true;
        }
        else {
            Util.Log.ih("NO SE PUDO ACTUALIZAR EL CONTRATO : " + contractID + " ===================================================");

            /******************************************
             * TODO: DELETE THIS WHEN FINISH DEBUGGING
             * ****************************************
             */
            //LogRegister.saveContractsIntoFile(contractID, "updatePaymentMade", false);
        }
        return false;
    }

    /**
     * sets number of copies made of a specific payments
     * @param folio
     */
    public static void setNumberOfCopies(String folio){
        List<Payments> payments = Payments.find(Payments.class, "folio = ?", folio);
        if (payments.size() > 0){
            payments.get(0).copies = "1";
            payments.get(0).save();
        }
    }

    public static void setClientsAsNotSynced(){
        List<Clients> clients = Clients.findWithQuery(Clients.class,
                "UPDATE Clients " +
                        "SET sync = '0'" +
                        "WHERE 1");
    }

    /**
     * deletes collector's info
     */
    public static void deleteCollector(){
        Collectors.deleteAll(Collectors.class);
    }

    /**
     * deletes collector's info
     */
    public static void deleteCollectorsBySupervisor(){
        Collector.deleteAll(Collector.class);
    }

    /**
     * deletes canceled payments
     */
    public static void deleteAllCanceled(){
        //Canceled.deleteAll(Canceled.class);
    }

    /**
     * deletes all payments
     */
    public static void deleteAllPayments(){
        Payments.deleteAll(Payments.class);
    }

    /**
     * deletes all deposits
     */
    public static void deleteAllDeposits(){
        //Deposits.deleteAll(Deposits.class);
        System.out.println("Se mando llamar la funcion de borrar depositos");
    }

    /**
     * deletes all notifications
     */
    public static void deleteNotifications(){
        Notifications.deleteAll(Notifications.class);
    }

    /**
     * deletes all viewed notifications
     */
    public static void deleteViewedNotifications(){
        ViewedNotifications.deleteAll(ViewedNotifications.class);
    }

    /**
     * deletes all locations
     */
    public static void deleteLocations(){
        Locations.deleteAll(Locations.class);
    }

    /**
     * deletes all clients
     */
    public static void deleteClients(){
        Clients.deleteAll(Clients.class);
        //SyncedWallet.deleteAll(SyncedWallet.class);
    }

    /**
     * deletes all banks
     */
    public static void deleteBanks(){
        Banks.deleteAll(Banks.class);
    }

    /**
     * deletes all companies
     */
    public static void deleteCompanies(){
        Companies.deleteAll(Companies.class);
    }

    public static void deleteClientsNotSynced(){
        Clients.deleteAll(Clients.class, "sync = ?", "0");
    }

    public static void deleteContractsNotAssigned(){
        Util.Log.ih("deleting contracts not assigned");
        /*
        String queryDelete = "DELETE FROM SYNCED_WALLET " +
                "WHERE contract_id = " +
                "( " +
                "SELECT t1.contract_id " +
                "FROM SYNCED_WALLET AS t1 " +
                "WHERE NOT EXISTS (SELECT 1 FROM CLIENTS AS t2 WHERE t2.id_contract = t1.contract_id) " +
                ")";
        String querySelect = "SELECT * FROM SYNCED_WALLET " +
                "WHERE contract_id = " +
                "( " +
                "SELECT t1.contract_id " +
                "FROM SYNCED_WALLET AS t1 " +
                "WHERE NOT EXISTS (SELECT 1 FROM CLIENTS AS t2 WHERE t2.id_contract = t1.contract_id) " +
                ")";
        */

        /*
        String queryDeleteAux = "DELETE " +
                "FROM SYNCED_WALLET AS t1 " +
                "WHERE NOT EXISTS " +
                "( " +
                "SELECT * " +
                "FROM CLIENTS AS t2 " +
                "WHERE t1.contract_id = t2.id_contract " +
                ")";
        */

        /*
        String queryDeleteTest = "DELETE FROM SYNCED_WALLET " +
                "WHERE contract_id = " +
                "(" +
                "SELECT t1.contract_id " +
                "FROM SYNCED_WALLET AS t1 " +
                "WHERE NOT EXISTS " +
                "(" +
                " SELECT t2.id_contract " +
                " FROM CLIENTS AS t2 " +
                " WHERE t1.contract_id = t2.id_contract " +
                ")" +
                ")";
        */

        String queryToSelectContractsDeletedFromEcobro =
                "SELECT * FROM SYNCED_WALLET AS t1 " +
                "WHERE NOT EXISTS " +
                "(" +
                " SELECT * FROM CLIENTS AS t2" +
                " WHERE t1.contract_id = t2.id_contract " +
                ")";
        String queryToDelete = "";


        //TODO: SELECT CONOTRACTS THAT WILL BE DELETED
        //List<SyncedWallet> deletedContracts = SyncedWallet.findWithQuery(SyncedWallet.class,
        //        querySelect);
        //TODO: DELETE CONTRACTS
        //String[] asd = new String[]{};

        //SyncedWallet.deleteAll(SyncedWallet.class, "contract_id = ?", );
        //SyncedWallet.executeQuery(queryDeleteTest);
        List<SyncedWallet> contractsToDelete = SyncedWallet.findWithQuery(SyncedWallet.class, queryToSelectContractsDeletedFromEcobro);
        if (contractsToDelete.size() > 0){
            for (int i = 0; i < contractsToDelete.size(); i++) {
                queryToDelete = "DELETE FROM SYNCED_WALLET WHERE contract_id = " + contractsToDelete.get(i).getContractID();
                SyncedWallet.executeQuery(queryToDelete);
            }
        }
    }

    /**
     * erase all database info
     */
    public static void eraseDatabaseAfterChangingPeriod(){
        //deleteAllPayments();
        deleteAllDeposits();
        deleteAllCanceled();
    }

    /**
     * erase all database info
     */
    public static void eraseDatabaseAfterDoingCierre(){
        //deleteCollector();
        deleteAllPayments();
        deleteAllDeposits();
        deleteAllCanceled();
    }

    /**
     * erase all database info after blocking the app
     */
    public static void eraseDatabaseToBlock(){
        deleteAllPayments();
        deleteNotifications();
        deleteViewedNotifications();
        deleteLocations();
        deleteClients();
    }

    /**
     * method that returns number of days in current year
     *
     * @return
     * 		integer type - number of days in current year
     */
    private static int checkLeapYear(){
        int year = Calendar.getInstance().get(Calendar.YEAR);
        if (year%4==0){
            if (year%100==0){
                if (year%400==0){
                    return 366;
                }
                else{
                    return 365;
                }
            }
            else{
                return 366;
            }
        }
        else{
            return 365;
        }
    }

    public static String padLeft(String mString, int pad){
        return String.format("%" + pad + "s", mString);
    }

    public static String padLeft(String mString, int pad, String padChar){
        return String.format("%" + pad + "s", mString).replace(" ", padChar);
    }

    //static List<SyncedWallet> clientsTest;

    public static List<SyncedWallet> threadTest() {

        List<SyncedWallet> clientsTest = new ArrayList<>();

        Runnable runnable = new Runnable() {

            List<SyncedWallet> clientsTest;

            @Override
            public void run() {
                clientsTest = DatabaseAssistant.getClientsFromSyncedWallet();
                //DatabaseAssistant.getAllClients();
            }

            public Runnable setData(List<SyncedWallet> clientsTest){
                this.clientsTest = clientsTest;
                return this;
            }

        }.setData(clientsTest);

        return clientsTest;

        /*
        Thread thread = new Thread(runnable);
        thread.start();
        */

        /*
        Runnable runnable2 = new Runnable() {
            @Override
            public void run() {
                DatabaseAssistant.getClientsFromSyncedWallet();
            }
        };
        Thread thread2 = new Thread(runnable2);
        thread2.start();
        */
        //DatabaseAssistant.getClientsFromSyncedWallet();
        //DatabaseAssistant.getAllClients();
        /*
        int folio = 12;
        for (int i = 0; i < 700; i++) {
            folio += 1;
            String folioString = "" + folio;
            DatabaseAssistant.insertPayment(
                    "01",
                    "0",
                    "0",
                    "1",
                    "135",
                    "1",
                    "010294",
                    new Date().toString(),
                    "1AF",
                    folioString,
                    "Z",
                    "11"
            );
        }
        */
    }

    private static OnGetClientsFromSyncedWalletListener getClientsFromSyncedWalletListener;

    public static void setOnGetClientsFromSyncedWalletListener(OnGetClientsFromSyncedWalletListener listener){
        getClientsFromSyncedWalletListener = listener;
    }

    public interface OnGetClientsFromSyncedWalletListener{
        public void onGetClientsFromSyncedWallet(List<SyncedWallet> clientListFromSyncedWallet);
    }

    private static int getNumberOfDaysOfMonth(int month) {
        switch (month) {
            case 0:case 2:case 4:case 6:case 7:case 9:case 11:
                return 31;
            case 3:case 5:case 8:case 10:
                return 30;
            case 1:
                return checkLeapYear() == 365 ? 28 : 29;
        }
        return 0;
    }

    public static void resetDatabase( Context context ) {
        SugarContext.terminate();
        SchemaGenerator schemaGenerator = new SchemaGenerator( context );
        schemaGenerator.deleteTables(new SugarDb( context ).getDB());
        SugarContext.init( context );
        schemaGenerator.createDatabase(new SugarDb( context ).getDB());
    }


    public static void insertDatosActualizados(String codigo, String contrato, String calle, String numeroexterior, String colonia,
                                               String localidad, String entrecalles, String correo, String telefono, int sync, String interior)
    {
        Actualizados datos = new Actualizados(
                codigo,
                contrato,
                calle,
                numeroexterior,
                colonia,
                localidad,
                entrecalles,
                correo,
                telefono,
                sync,
                interior);
        datos.save();
    }

    public static String[] consultarDatosDeCobrador()
    {
        String [] datosCobrador = new String[10];
        List<Collectors> lista = Collectors.findWithQuery(Collectors.class, "SELECT * FROM COLLECTORS");
        if (lista.size() > 0)
        {
            datosCobrador[0]=lista.get(0).getMensajeaviso();
            datosCobrador[1]=lista.get(0).getTitulopercapita();
            datosCobrador[2]=lista.get(0).getMensajecobrador();
            datosCobrador[3]=lista.get(0).getNumberCollector(); //no_cobrador
            datosCobrador[4]=lista.get(0).getIdCollector(); //codigoCobrador
            datosCobrador[5]=lista.get(0).getNdias(); // nDias_de_pago
            datosCobrador[6]=lista.get(0).getNumeropagos();
            datosCobrador[7]=lista.get(0).getName();
            datosCobrador[8]=lista.get(0).getMostrar_actualizar_datos();
            datosCobrador[9]=lista.get(0).getEmpresas_excluidas_de_captura_de_informacion();

        }
        return datosCobrador;
    }
    public static String[] consultaDatosCliente(String idContrato)
    {
        String [] datosCliente = new String[8];
        List<Clients> lista = Clients.findWithQuery(Clients.class, "SELECT * FROM CLIENTS WHERE id_Contract = '" + idContrato + "'");
        if (lista.size() > 0)
        {
            datosCliente[0]=lista.get(0).getIdContract();
            datosCliente[1]=lista.get(0).getName();
            datosCliente[2]=lista.get(0).getFirstLastName();
            datosCliente[3]=lista.get(0).getSecondLastName();
            datosCliente[4]=lista.get(0).getLocality();
            datosCliente[5]=lista.get(0).getNumberContract();
            datosCliente[6]=lista.get(0).getNumberCollector();
            datosCliente[7]=lista.get(0).getSerie();

        }
        return datosCliente;
    }

    public static void insertarDatosTicket(String tiketsuspencion)
    {
        Notice.deleteAll(Notice.class);
        Notice notice = new Notice(tiketsuspencion);
        notice.save();
    }

    public static void insertarMensajeSuspensiones(String descripcion)
    {
        //Suspensionitem.deleteAll(Suspensionitem.class);
        Suspensionitem suspensionitem = new Suspensionitem(descripcion);
        suspensionitem.save();
    }

    public static void insertarMotivoDeCancelacionDePago(String descripcion, String id_web)
    {
        MotivosDeCancelacion motivoItem = new MotivosDeCancelacion(descripcion, id_web);
        motivoItem.save();
    }


    public static void insertarNoVisitados(String semanales, String quincenales, String mensuales, String fecha)
    {
        Novisitados novisitados = new Novisitados(semanales, quincenales, mensuales, fecha);
        novisitados.save();
    }


    public static void insertarLocationsNow(String no_cobrador, String tiempo, String latitud, String longitud)
    {
        try {
            LocationNow logationNow = new LocationNow(no_cobrador, tiempo, latitud, longitud, 1);
            logationNow.save();
        }
        catch (Exception ex)
        {
            Util.Log.d("Error de insercion de datos, LOCATIONS_NOW: "+ ex.toString());
        }
    }


    public static String[] obtenerDepositoPorPeriodo(String periodo)
    {
        String [] datosDeposito = new String[9];
        List<Deposits> lista = Deposits.findWithQuery(Deposits.class, "SELECT * FROM DEPOSITS WHERE periodo = '" + periodo + "'");
        if (lista.size() > 0)
        {
            datosDeposito[0]=lista.get(0).getCompany();
            datosDeposito[1]=lista.get(0).getAmount();
            datosDeposito[2]=lista.get(0).getDate();
            datosDeposito[3]=lista.get(0).getDepositDate();
            datosDeposito[4]=lista.get(0).getIdDeposit();
            datosDeposito[5]=lista.get(0).getNumberBank();
            datosDeposito[6]=lista.get(0).getReference();
            datosDeposito[7]=lista.get(0).getStatus();
            datosDeposito[8]=String.valueOf(lista.get(0).getPeriodo());
        }
        return datosDeposito;
    }

    public static void insertarConfiguracion(String serie_impresora,String mac_fisica)
    {
        Configuraciones.deleteAll(Configuraciones.class);
        Configuraciones configuraciones = new Configuraciones(serie_impresora, mac_fisica);
        configuraciones.save();
    }

    public static void insertarMontosTotalesPorEmpresaYPeriodo(String empresa, String cobros, String depositos, String cancelados, String cobros_mas_cancelados, String total_cobros_menos_depositos, String cobros_menos_depositos, String periodo)
    {
        MontosTotalesDeEfectivo montos = new MontosTotalesDeEfectivo(empresa, cobros, depositos, cancelados, cobros_mas_cancelados, total_cobros_menos_depositos, cobros_menos_depositos, periodo);
        montos.save();
    }

    public static double getDepositoDeOficinaPorPeriodo(int periodo)
    {
        double amount = 0;
        String query="SELECT * FROM DEPOSITS WHERE periodo ='"+ periodo +"' and company = '00'";
        //List<Deposits> deposits = Deposits.find(Deposits.class, "periodo = ?", ""+periodo);
        List<Deposits> deposits = Deposits.findWithQuery(Deposits.class, query);
        if (deposits.size() > 0){
            for (Deposits listDeposits: deposits)
            {
                amount += Float.parseFloat(listDeposits.amount);
            }
        }
        return amount;
    }

    public static double getTotalDepositosPorPeriodo(int periodo)
    {
        double amount = 0;
        List<Deposits> deposits = Deposits.find(Deposits.class, "periodo = ?", ""+periodo);
        if (deposits.size() > 0){
            for (Deposits listDeposits: deposits)
            {
                amount += Float.parseFloat(listDeposits.amount);
            }
        }
        return amount;
    }

    public static double getTotalDepositosPorPeriodoAndCompany(int periodo, String compania)
    {
        double amount = 0;
        String query="SELECT * FROM DEPOSITS WHERE periodo='"+ periodo +"' and company = '"+ compania +"'";
        List<Deposits> deposits = Deposits.findWithQuery(Deposits.class, query);
        if (deposits.size() > 0)
        {
            for (Deposits listDeposits: deposits)
            {
                amount += Float.parseFloat(listDeposits.amount);
            }
        }
        else
        {
            System.out.println("asda");
        }
        return amount;
    }

    public static double setSaldoPorCobro(double saldo, float monto)
    {
        double saldoGral=0;
        try {
            saldoGral = saldo - monto;
        }catch (Exception ex)
        {
            Log.i("Error", "Error");
            ex.printStackTrace();
        }
        return saldoGral;
    }

    public static String getSerialPrinter()
    {
        String serial="";
        List<Configuraciones> listaConfiguraciones = Configuraciones.findWithQuery(Configuraciones.class, "SELECT * FROM CONFIGURACIONES");
        if (listaConfiguraciones.size() > 0)
             serial = listaConfiguraciones.get(0).getSerie_impresora();

        return serial;
    }

    public static void insertarLocalidadColonia(String locColLocalidad, String locColColonia, int locCol_id_web)
    {
        Localidad_Colonia localidad = new Localidad_Colonia(locColLocalidad, locColColonia, locCol_id_web, 0);
        localidad.save();
    }

    public static String getIdWebForCancelationReason(String motivoString)
    {
        List<MotivosDeCancelacion> motivosDeCancelacionList = MotivosDeCancelacion.findWithQuery(MotivosDeCancelacion.class, "SELECT * FROM MOTIVOS_DE_CANCELACION where motivo = '" + motivoString +"'");
        if (motivosDeCancelacionList.size() > 0)
            return motivosDeCancelacionList.get(0).getId_web();
        else
            return "";
    }

    public static double getTotalCobrosPorContratoDentroDelPeriodoActivo(String contrato, String periodo)
    {
        double amount = 0;
        String query="SELECT * FROM PAYMENTS WHERE id_client='"+ contrato +"' and periodo = '"+ periodo +"'";
        List<Payments> paymentsListSum = Payments.findWithQuery(Payments.class, query);
        if (paymentsListSum.size() > 0)
        {
            for (Payments listPayments: paymentsListSum)
            {
                amount += Float.parseFloat(listPayments.amount);
            }
        }
        else
            amount=0;

        return amount;
    }


    public static List<ModelCobrosRealizadosPorPeriodo> getCobrosRealizadosEnElPeriodo(String contrato, String periodo)
    {
        //List<ModelCobrosRealizadosPorPeriodo> cobros = null;
        List<ModelCobrosRealizadosPorPeriodo> issues = new ArrayList<>();
        String query="SELECT * FROM PAYMENTS WHERE id_client='"+ contrato +"' and periodo = '"+ periodo +"'";
        List<Payments> paymentsListSum = Payments.findWithQuery(Payments.class, query);
        if (paymentsListSum.size() > 0) {
            for (int i = 0; i < paymentsListSum.size(); i++) {
                ModelCobrosRealizadosPorPeriodo product = new ModelCobrosRealizadosPorPeriodo(paymentsListSum.get(i).getSeriePayment() + paymentsListSum.get(i).getFolio(), paymentsListSum.get(i).getDate(), paymentsListSum.get(i).getAmount());
                issues.add(product);
            }
        }
        return issues;
    }


    /*
    String query = "SELECT * FROM SOLICITUDCLIENTE ORDER BY id DESC limit 5";
        List<Solicitud_Cliente> listaSolicitudes = Solicitud_Cliente.findWithQuery(Solicitud_Cliente.class, query);
        if(listaSolicitudes.size()>0)
        {
            for (int i = 0; i < listaSolicitudes.size(); i++){

                try
                {
                    Ultimas product = new Ultimas(listaSolicitudes.get(i).getSolCtle_CodigoActivacion(), listaSolicitudes.get(i).getSolCtle_SolicitudSerie());
                    productListA.add(product);
                } catch (Exception e)
                {
                    Log.d("ErrorSHOW-->", e.toString());
                }
            }
            adapterSol= new AdapterSolicitudes(getApplicationContext(), productListA);
            rvAf.setAdapter(adapterSol);
        }
        else
        {
            Log.d("NO HAY REGISTROS-->", "CONSULTA DE SOLICITUDES LOCALES");
        }
     */
    public static int getLastLocalidadColoniaInserted()
    {
        List<Localidad_Colonia> localidadColonia = Localidad_Colonia.find(Localidad_Colonia.class, null, null, null, "idweb DESC", "1");
        if ( localidadColonia.size() > 0 ) {
            return localidadColonia.get(0).getIdweb();
        }
        return 0;
    }

    public static void insertAirPlaneActivated(String no_cobrador, String fecha, String activado)
    {
        AirPlaneRegister registerAirPlane = new AirPlaneRegister(no_cobrador, fecha, activado, 0);
        registerAirPlane.save();
    }
}
