package com.jaguarlabs.pabs.database;

import com.orm.SugarRecord;

public class Novisitados extends SugarRecord
{
    String semanales="", quincenales="", mensuales="",fecha="";

    public Novisitados() {
    }

    public Novisitados(String semanales, String quincenales, String mensuales, String fecha) {
        this.semanales = semanales;
        this.quincenales = quincenales;
        this.mensuales = mensuales;
        this.fecha=fecha;
    }

    public String getSemanales() {
        return semanales;
    }

    public void setSemanales(String semanales) {
        this.semanales = semanales;
    }

    public String getQuincenales() {
        return quincenales;
    }

    public void setQuincenales(String quincenales) {
        this.quincenales = quincenales;
    }

    public String getMensuales() {
        return mensuales;
    }

    public void setMensuales(String mensuales) {
        this.mensuales = mensuales;
    }


    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
