package com.jaguarlabs.pabs.database;

import com.orm.SugarRecord;

public class LocationNow extends SugarRecord
{
    String no_cobrador="", tiempo="", latitud="", longitud="";
    int sync;

    public LocationNow() {
    }

    public LocationNow(String no_cobrador, String tiempo, String latitud, String longitud, int sync) {
        this.no_cobrador = no_cobrador;
        this.tiempo = tiempo;
        this.latitud = latitud;
        this.longitud = longitud;
        this.sync = sync;
    }

    public String getNo_cobrador() {
        return no_cobrador;
    }

    public void setNo_cobrador(String no_cobrador) {
        this.no_cobrador = no_cobrador;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }
}
