package com.jaguarlabs.pabs.database;

import com.orm.SugarRecord;

/**
 * Created by Jordan Alvarez on 4/21/2017.
 */

public class OsticketTopics extends SugarRecord {

    private String topic;

    public OsticketTopics(){}

    public OsticketTopics( String topic ) {
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
}
