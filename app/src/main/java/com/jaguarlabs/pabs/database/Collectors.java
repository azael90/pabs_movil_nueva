package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by jordan on 03/03/16.
 */
public class Collectors extends SugarRecord {

    @SerializedName("no_cobrador")
    String numberCollector;
    @SerializedName("codigo_cobrador")
    String idCollector;
    @SerializedName("macaddress")
    String macAddress;
    @SerializedName("ult_folio_malva")
    String lastFolioMalva;
    @SerializedName("ult_folio_programa")
    String lastFolioPrograma;
    @SerializedName("nombre")
    String name;
    @SerializedName("mensaje")
    String message;
    @SerializedName("radio")
    String radio;

    @SerializedName("ndias")
    String ndias ;
    @SerializedName("mensaje_aviso_credito_percapita")
    String mensajeaviso;
    @SerializedName("numero_de_pagos")
    String numeropagos;
    @SerializedName("titulo_percapita")
    String titulopercapita;
    @SerializedName("mensaje_percapita_cobrador")
    String mensajecobrador;

    @SerializedName("minutos_timer_ubicaciones")
    String minutosTimerUbicaciones;
    @SerializedName("mostrar_actualizar_datos")
    String mostrar_actualizar_datos;

    @SerializedName("fecha_sistema_servidor")
    String fecha_sistema_servidor;

    @SerializedName("empresas_excluidas_de_captura_de_informacion")
    String empresas_excluidas_de_captura_de_informacion;




    public Collectors(){}

    public Collectors(String numberCollector, String idCollector, String macAddress, String lastFolioMalva, String lastFolioPrograma, String name, String message, String radio,
                      String ndias, String mensajeaviso, String numeropagos, String titulopercapita, String mensajecobrador,
                      String minutosTimerUbicaciones, String mostrar_actualizar_datos, String fecha_sistema_servidor,
                      String empresas_excluidas_de_captura_de_informacion){
        this.numberCollector = numberCollector;
        this.idCollector = idCollector;
        this.macAddress = macAddress;
        this.lastFolioMalva = lastFolioMalva;
        this.lastFolioPrograma = lastFolioPrograma;
        this.name = name;
        this.message = message;
        this.radio = radio;
        this.ndias=ndias;
        this.mensajeaviso=mensajeaviso;
        this.numeropagos=numeropagos;
        this.titulopercapita=titulopercapita;
        this.mensajecobrador= mensajecobrador;
        this.minutosTimerUbicaciones= minutosTimerUbicaciones;
        this.mostrar_actualizar_datos=mostrar_actualizar_datos;
        this.fecha_sistema_servidor=fecha_sistema_servidor;
        this.empresas_excluidas_de_captura_de_informacion=empresas_excluidas_de_captura_de_informacion;
    }

    public String getNumberCollector() {
        return numberCollector;
    }

    public void setNumberCollector(String numberCollector) {
        this.numberCollector = numberCollector;
    }

    public String getIdCollector() {
        return idCollector;
    }

    public void setIdCollector(String idCollector) {
        this.idCollector = idCollector;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getLastFolioMalva() {
        return lastFolioMalva;
    }

    public void setLastFolioMalva(String lastFolioMalva) {
        this.lastFolioMalva = lastFolioMalva;
    }

    public String getLastFolioPrograma() {
        return lastFolioPrograma;
    }

    public void setLastFolioPrograma(String lastFolioPrograma) {
        this.lastFolioPrograma = lastFolioPrograma;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRadio() {
        return radio;
    }

    public void setRadio(String radio) {
        this.radio = radio;
    }

    public String getNdias() {
        return ndias;
    }

    public void setNdias(String ndias) {
        this.ndias = ndias;
    }

    public String getMensajeaviso() {
        return mensajeaviso;
    }

    public void setMensajeaviso(String mensajeaviso) {
        this.mensajeaviso = mensajeaviso;
    }

    public String getNumeropagos() {
        return numeropagos;
    }

    public void setNumeropagos(String numeropagos) {
        this.numeropagos = numeropagos;
    }

    public String getTitulopercapita() {
        return titulopercapita;
    }

    public void setTitulopercapita(String titulopercapita) {
        this.titulopercapita = titulopercapita;
    }


    public String getMensajecobrador() {
        return mensajecobrador;
    }

    public void setMensajecobrador(String mensajecobrador) {
        this.mensajecobrador = mensajecobrador;
    }

    public String getMinutosTimerUbicaciones() {
        return minutosTimerUbicaciones;
    }

    public void setMinutosTimerUbicaciones(String minutosTimerUbicaciones) {
        this.minutosTimerUbicaciones = minutosTimerUbicaciones;
    }

    public String getMostrar_actualizar_datos() {
        return mostrar_actualizar_datos;
    }

    public void setMostrar_actualizar_datos(String mostrar_actualizar_datos) {
        this.mostrar_actualizar_datos = mostrar_actualizar_datos;
    }

    public String getFecha_sistema_servidor() {
        return fecha_sistema_servidor;
    }

    public void setFecha_sistema_servidor(String fecha_sistema_servidor) {
        this.fecha_sistema_servidor = fecha_sistema_servidor;
    }

    public String getEmpresas_excluidas_de_captura_de_informacion() {
        return empresas_excluidas_de_captura_de_informacion;
    }

    public void setEmpresas_excluidas_de_captura_de_informacion(String empresas_excluidas_de_captura_de_informacion) {
        this.empresas_excluidas_de_captura_de_informacion = empresas_excluidas_de_captura_de_informacion;
    }
}
