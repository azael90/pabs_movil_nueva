package com.jaguarlabs.pabs.database;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by Jordan Alvarez on 3/6/2017.
 */

@SuppressWarnings("WeakerAccess")
public class Periods extends SugarRecord {

    @SerializedName("fecha_inicio")
    private String startDate;
    @SerializedName("fecha_fin")
    private String endDate;
    @SerializedName("no_periodo")
    private int periodNumber;
    private int status;//0 -> active (not close), 1 -> inactive (closed)
    @SerializedName("startDateMilliseconds")
    private long startDateMilliseconds;
    @SerializedName("endDateMilliseconds")
    private long endDateMilliseconds;

    public Periods(){}

    public Periods( String startDate, String endDate, int periodNumber ) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.periodNumber = periodNumber;
        this.status = 0;
        this.startDateMilliseconds = new Date().getTime();
        this.endDateMilliseconds = 0;
    }

    public Periods( String startDate, int periodNumber ) {
        this.startDate = startDate;
        this.endDate = "";
        this.periodNumber = periodNumber;
        this.status = 0;
        this.startDateMilliseconds = new Date().getTime();
        this.endDateMilliseconds = 0;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getPeriodNumber() {
        return periodNumber;
    }

    public void setPeriodNumber(int periodNumber) {
        this.periodNumber = periodNumber;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getStartDateMilliseconds() {
        return startDateMilliseconds;
    }

    public void setStartDateMilliseconds(long startDateMilliseconds) {
        this.startDateMilliseconds = startDateMilliseconds;
    }

    public long getEndDateMilliseconds() {
        return endDateMilliseconds;
    }

    public void setEndDateMilliseconds(long endDateMilliseconds) {
        this.endDateMilliseconds = endDateMilliseconds;
    }
}
