package com.jaguarlabs.pabs.database;

import com.orm.SugarRecord;

public class Notice extends SugarRecord
{
    String tiketsuspencion="";

    public Notice() {
    }

    public Notice(String tiketsuspencion) {
        this.tiketsuspencion = tiketsuspencion;
    }

    public String getTiketsuspencion() {
        return tiketsuspencion;
    }

    public void setTiketsuspencion(String tiketsuspencion) {
        this.tiketsuspencion = tiketsuspencion;
    }

}
