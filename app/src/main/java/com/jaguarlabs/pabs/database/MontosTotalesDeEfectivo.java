package com.jaguarlabs.pabs.database;

import com.orm.SugarRecord;

public class MontosTotalesDeEfectivo extends SugarRecord
{
    String empresa="", cobros="", depositos="", cancelados="", cobros_mas_cancelados="",
            total_cobros_menos_depositos="", cobros_menos_depositos="", periodo="";

    // total_cobros_menos_depositos = (cobros - cancelados) - depositos

    public MontosTotalesDeEfectivo() {
    }

    public MontosTotalesDeEfectivo(String empresa, String cobros, String depositos, String cancelados, String cobros_mas_cancelados, String total_cobros_menos_depositos, String cobros_menos_depositos, String periodo) {
        this.empresa = empresa;
        this.cobros = cobros;
        this.depositos = depositos;
        this.cancelados = cancelados;
        this.cobros_mas_cancelados = cobros_mas_cancelados;
        this.total_cobros_menos_depositos = total_cobros_menos_depositos;
        this.cobros_menos_depositos = cobros_menos_depositos;
        this.periodo = periodo;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCobros() {
        return cobros;
    }

    public void setCobros(String cobros) {
        this.cobros = cobros;
    }

    public String getDepositos() {
        return depositos;
    }

    public void setDepositos(String depositos) {
        this.depositos = depositos;
    }

    public String getCancelados() {
        return cancelados;
    }

    public void setCancelados(String cancelados) {
        this.cancelados = cancelados;
    }

    public String getCobros_mas_cancelados() {
        return cobros_mas_cancelados;
    }

    public void setCobros_mas_cancelados(String cobros_mas_cancelados) {
        this.cobros_mas_cancelados = cobros_mas_cancelados;
    }

    public String getTotal_cobros_menos_depositos() {
        return total_cobros_menos_depositos;
    }

    public void setTotal_cobros_menos_depositos(String total_cobros_menos_depositos) {
        this.total_cobros_menos_depositos = total_cobros_menos_depositos;
    }

    public String getCobros_menos_depositos() {
        return cobros_menos_depositos;
    }

    public void setCobros_menos_depositos(String cobros_menos_depositos) {
        this.cobros_menos_depositos = cobros_menos_depositos;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }
}
