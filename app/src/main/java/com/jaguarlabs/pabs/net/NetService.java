//TODO put copyright
package com.jaguarlabs.pabs.net;

import org.json.JSONObject;

/**
 * This class generates an object that is used to store the information of a
 * Service, you can execute a service with the Service Executer of the Net
 * Helper class.
 * 
 * @author joserenesignoretbecerra
 * @see com.jaguarlabs.probenso.net.NetHelper
 */
public class NetService {
	private String url;
	RESTClient.RequestMethod requestMethod = RESTClient.RequestMethod.POST;
	public RESTClient.RequestMethod getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(RESTClient.RequestMethod requestMethod) {
		this.requestMethod = requestMethod;
	}
	String contentType = RESTClient.content_typeJSON;

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	private String data;


	
	public NetService(String url, JSONObject data) {
		super();
		this.url = url;
		this.data = data.toString();
	}

	public NetService(String url, String Service, String data) {
		super();
		this.url = url + Service;
		this.data = data;
	}
	/**
	 * 
	 * @param url
	 * @param data
	 */
	public NetService(String Service, String data) {
		super();
		this.url = NetHelper.baseUrl + Service;
		this.data = data;
	}

	/**
	 * gets url
	 * @return url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * sets service
	 * @param Service
	 */
	public void setService(String Service) {
		this.url = NetHelper.baseUrl + Service;
	}

	/**
	 * get data
	 * @return data
	 */
	public String getData() {
		return data;
	}

	/**
	 * sets data
	 * @param data
	 */
	public void setData(String data) {
		this.data = data;
	}

}