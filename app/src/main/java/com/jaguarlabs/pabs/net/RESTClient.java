/* *************************************************************************
 *
 *   Copyright (c)  2012 by Jaguar Labs.
 *   Confidential and Proprietary
 *   All Rights Reserved
 *
 *   This software is furnished under license and may be used and copied
 *   only in accordance with the terms of its license and with the
 *   inclusion of the above copyright notice. This software and any other
 *   copies thereof may not be provided or otherwise made available to any
 *   other party. No title to and/or ownership of the software is hereby
 *   transferred.
 *
 *   The information in this software is subject to change without notice and
 *   should not be construed as a commitment by JaguarLabs.
 *
 * @(#)$Id: $
 * Last Revised By   : $Author:efren campillo
 * Last Checked In   : $Date: $
 * Last Version      : $Revision:  $
 *
 * Original Author   : efren campillo  -- efren.campillo@jaguarlabs.com
 * Origin            : SEnE -- november 2 @ 11:00 (PST)
 * Notes             :
 *
 * *************************************************************************/

package com.jaguarlabs.pabs.net;

import android.util.Log;

import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.util.ArrayList;

/**
 * RestClient class which is used for web services
 */
//TODO update this class from template, with inner class mysslsocket
public class RESTClient {
	public final static String content_typeJSON = "application/json";
	public final static String content_typeWWWForm = "application/x-www-form-urlencoded";

	public static enum RequestMethod {
		GET, POST
	}

	private final ArrayList<NameValuePair> params;
	private final ArrayList<NameValuePair> headers;
	public String post;
	private final String url;
	private String content_type = "application/json";
	private int responseCode;
	private String message;
	private String response;

	/*
	 * default methods to manage the restclient
	 */
	public String getResponse() {
		return response;
	}

	public String getErrorMessage() {
		return message;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public RESTClient(String url) {
		this.url = url;
		params = new ArrayList<NameValuePair>();
		headers = new ArrayList<NameValuePair>();
	}

	public void AddParam(String name, String value) {
		params.add(new BasicNameValuePair(name, value));
	}

	public void AddHeader(String name, String value) {
		headers.add(new BasicNameValuePair(name, value));
	}

	/*
	 * exceute the post defined to the url defined
	 */
	public void Execute(RequestMethod method) throws ClientProtocolException, IOException  {
		switch (method) {
		case GET: {
			// add parameters
			String combinedParams = "";
			if (!params.isEmpty()) {
				combinedParams += "?";
				for (NameValuePair p : params) {
					String paramString = p.getName() + "="
							+ URLEncoder.encode(p.getValue(), "UTF-8");
					if (combinedParams.length() > 1) {
						combinedParams += "&" + paramString;
					} else {
						combinedParams += paramString;
					}
				}
			}
			Log.i("RestClient", "GET  :" + url + combinedParams);
			HttpGet request = new HttpGet(url + combinedParams);
			// add headers
			for (NameValuePair h : headers) {
				request.addHeader(h.getName(), h.getValue());
			}
			executeRequest(request, url);
			break;
		}
		case POST: {
			HttpPost request = new HttpPost(url);
			request.setHeader("Content-type", content_type);
			// request.setHeader("Content-type","application/x-www-form-urlencoded");
			// add headers
			for (NameValuePair h : headers) {
				request.addHeader(h.getName(), h.getValue());
				// Log.i("headeradding",""+h.getName()+","+h.getValue());
			}
			// if (!params.isEmpty()) {
			request.setEntity(new ByteArrayEntity(post.toString().getBytes(
					"UTF8")));
			// request.setEntity(new UrlEncodedFormEntity(post));
			Log.i("posting", post + request.getEntity().toString() + " in "
					+ url + " with this content-type: " + content_type);
			// }

			executeRequest(request, url);
			break;
		}
		}
	}

	/*
	 * executing recuest from post in execute restclient
	 */
	private void executeRequest(HttpUriRequest request, String url) throws ClientProtocolException, IOException {
		URL endpoint = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) endpoint.openConnection();
		connection.setDoInput(true);
		connection.setConnectTimeout(1000 * 10);
		connection.setReadTimeout( (1000 * 60) * 3 );
		connection.setDoOutput(true);
		connection.setUseCaches(false);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type","application/json");



		byte[] outputInBytes = post.getBytes("UTF-8");
		OutputStream os = connection.getOutputStream();
		os.write(outputInBytes);
		os.close();
		try {
			connection.connect();

			try {
				DataInputStream inStream = new DataInputStream(connection.getInputStream());
				String str;
				response = new String();
				while ((str = inStream.readLine()) != null) {
					response += str;
				}
				inStream.close();
				connection.disconnect();
			} catch (Exception e) {
				e.printStackTrace();
				//          Log.e("Debug", "error: " + ioex.getMessage(), ioex);
			}
		} catch (IOException e){
			connection.disconnect();
		}


		String result = "";

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost requestt = new HttpPost(url);
		//request.addHeader("deviceId", deviceId);
		ResponseHandler<String> handler = new BasicResponseHandler();
		try {
			result = httpclient.execute(requestt, handler);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		httpclient.getConnectionManager().shutdown();
		Log.i("PABS", result);



//		HttpClient client = this.getNewHttpClient();// ; new
//													// DefaultHttpClient(httpParams);
//		if (client == null){
//			throw new RuntimeException();
//		}
//		HttpResponse httpResponse;
//		httpResponse = client.execute(request);
//		responseCode = httpResponse.getStatusLine().getStatusCode();
//		message = httpResponse.getStatusLine().getReasonPhrase();
//		HttpEntity entity = httpResponse.getEntity();
//		if (entity != null) {
//			InputStream instream = entity.getContent();
//			response = convertStreamToString(instream);
//			instream.close();
//		}

	}

	/*
	 * 
	 * convert the Stream from the request in a String readeable to the rest
	 * client
	 */
	private static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public HttpClient getNewHttpClient() {
		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore
					.getDefaultType());
			trustStore.load(null, null);
			MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
			// Time Out Exception
			HttpConnectionParams.setConnectionTimeout(params, 1500);
			HttpConnectionParams.setSoTimeout(params, 1500);
			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));
			ClientConnectionManager ccm = new ThreadSafeClientConnManager(
					params, registry);
			return new DefaultHttpClient(ccm, params);
		} catch (Exception e) {
			return null;
		}
	}

	public String getContent_type() {
		return content_type;
	}

	public void setContent_type(String content_type) {
		this.content_type = content_type;
	}
}
