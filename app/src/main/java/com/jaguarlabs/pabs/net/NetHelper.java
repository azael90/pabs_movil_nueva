//TODO put copyright
package com.jaguarlabs.pabs.net;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.jaguarlabs.pabs.SampleSpiceService;
import com.octo.android.robospice.SpiceManager;

//import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * This class makes and manages all of the connections to the Services, it uses
 * the RESTClient and the NetService class, and has all of the basic data in
 * static variables.
 * 
 * It can't be instantiated, you need to get the instance with the static method
 * getInstance().
 * 
 * To execute a Service, just call the Service Executer.
 * 
 * @author joserenesignoretbecerra
 * @see NetService
 */

/**
 * @author joserenesignoretbecerra
 * 
 */
public final class NetHelper {
	final static String baseUrl = "http://ncs-diamond.herokuapp.com/";
	public static final String BusinessService = "merchants/textquery?textstring=&targetattribute=name";
	public static final String CouponsService = "coupons/idquery?targetid=9&targetattribute=merchant_id";
	private SpiceManager manager;
	public static enum enumMsgTypeRequest {
		TEXT, ID, ACTION, LOCATION
	};

	public static enum enumGroupTypeRequest {
		CATEGORY, COUPON, REWARD, CUSTOMER, MERCHANT, SENSOR, LOCATION, ZONE, SENSOR_STATS, COUPON_STATS, REWARD_STATS
	};

	private String[] arrayTypeRequest = { "textquery/", "idquery/", "action/","locationquery/" };

	private String[] arrayGroupRequest = { "categorys/", "coupons/",
			"rewards/", "customers/", "merchants/", "sensors/", "locations/",
			"zones/", "sensor_stats/", "coupon_stats/", "rewards_stats" };

	private static NetHelper instance;
	onWorkCompleteListener listener;
	Map<String, onWorkCompleteListener> aMap = new HashMap<String, onWorkCompleteListener>();

	/**
	 * This interface is needed to use the ServiceExecuter, it receives the
	 * result of the task.
	 * 
	 * @author joserenesignoretbecerra
	 * 
	 */
	public interface onWorkCompleteListener {
		public void onCompletion(String result);
		public void onError(Exception e);
	}

	public interface onImageDownloaded {
		public void downloadComplete(Object object, int position);
		public void onError(Exception e);
	}
	
	public NetService createPostService(String version, enumGroupTypeRequest group,
			enumMsgTypeRequest msgType, JSONObject params) {
		String sParams = "";
		Iterator<?> keys = params.keys();
		while (keys.hasNext()) {
			String key = (String) keys.next();

			try {
				sParams += (key + "=" + URLEncoder.encode(
						params.getString(key), "UTF-8"));
				if (keys.hasNext()) {
					sParams += "&";
				}

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}

		String url = version + (version.contains("/") ? "" : "/")
				+ arrayGroupRequest[group.ordinal()]
				+ arrayTypeRequest[msgType.ordinal()] ;
		
		NetService serviceToExecute = new NetService(url, sParams);
		serviceToExecute.setContentType(RESTClient.content_typeWWWForm);
		serviceToExecute.setRequestMethod(RESTClient.RequestMethod.POST);
		return serviceToExecute;
	}
	
	/**
	 * Create a NetService object for a diamond service
	 * 
	 * @param version
	 *            Api version
	 * @param group
	 *            Kind of group request
	 * @param msgType
	 *            Kind of Message type,
	 * @param params
	 * @return
	 */
	public NetService createService(String version, enumGroupTypeRequest group,
			enumMsgTypeRequest msgType, JSONObject params) {
		String sParams = "";
		Iterator<?> keys = params.keys();
		while (keys.hasNext()) {
			String key = (String) keys.next();

			try {
				sParams += (key + "=" + URLEncoder.encode(
						params.getString(key), "UTF-8"));
				if (keys.hasNext()) {
					sParams += "&";
				}

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}

		String url = version + (version.contains("/") ? "" : "/")
				+ arrayGroupRequest[group.ordinal()]
				+ arrayTypeRequest[msgType.ordinal()] + "?" + sParams;
		NetService serviceToExecute = new NetService(url, "");
		return serviceToExecute;
	}

	protected NetHelper() {
		manager  = new SpiceManager(SampleSpiceService.class);
		
	}

	/**
	 * Use this method to get an Instance from the NetHelper.
	 * 
	 * @return
	 */
	public static NetHelper getInstance() {

		if (instance == null) {
			instance = new NetHelper();
		}
		return instance;
	}

	public void downloadImage(final String url,
			final onImageDownloaded listener, final int position) {

		Runnable run = () -> {
			InputStream is;
			try {
				is = (InputStream) new URL(url).getContent();
				Bitmap d = BitmapFactory.decodeStream(is);
				is.close();
				listener.downloadComplete(d, position);
			} catch (MalformedURLException e) {
				listener.onError(e);
				e.printStackTrace();
			} catch (IOException e) {
				listener.onError(e);
				e.printStackTrace();
			}
		};
		Thread thread = new Thread(run);
		thread.start();
	}

	/**
	 * This method execute a service, it need to have a listener to handle the
	 * result.
	 * 
	 * @param service
	 * @param context
	 * @param listener
	 */
	public void ServiceExecuter(NetService service, Context context,
			onWorkCompleteListener listener) {
			this.listener = listener;
			if (!manager.isStarted()){
				manager.start(context);
			}
			aMap.put(service.getUrl(), listener);
			PABSRequest request = new PABSRequest(service);
			Log.e("URL",service.getUrl());
			manager.execute(request, new PABSRequestListener(listener));
		
		
	}
	
	public void ServiceExecuterSplit(NetService service, Context context,
			onWorkCompleteListener listener) {
		this.listener = listener;
		PostTask poster = new PostTask(service,context,listener);
		poster.execute();
	}

	/**
	 * This method catches the result of the Service Executer and sends it to
	 * the handler, it should not be called outside of the PostTask.
	 * 
	 * @param message
	 * @throws JSONException
	 */
	void getResultOfExecuter(NetService service,String message) throws JSONException {
		Log.i("message", message);
		if (service == null) {
			Log.e("the service", "is null");
		}
		aMap.get(service.getUrl()).onCompletion(message);
	}

	void callErrorHander(Exception e){
		listener.onError(e);
	}

	/**
	 * This method make the RESTClient in condition to connect with the
	 * NetServices in this NetHelper, it should not be called outside the
	 * PostTask.
     *
	 * @return RESTclient post
	 * @throws IOException
	 * @throws Exception
	 */
	RESTClient post(NetService service) throws IOException {
		RESTClient post = new RESTClient(service.getUrl());
		post.post = service.getData();
		post.setContent_type(service.contentType);
		post.Execute(service.getRequestMethod());
		return post;
	}

	

	// TODO put method declaration
	static String bin2hex(byte[] bytes) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(0xFF & bytes[i]);
			if (hex.length() == 1) {
				sb.append('0');
			}
			sb.append(hex);
		}
		return sb.toString();
	}

}