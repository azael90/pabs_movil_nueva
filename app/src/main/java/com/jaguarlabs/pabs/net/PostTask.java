//TODO put copyright
package com.jaguarlabs.pabs.net;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.client.ClientProtocolException;

import java.io.IOException;

/**
 * Class that performs network operations in a different thread
 * Deprecated in 3.0
 */
class PostTask extends AsyncTask<String, Integer, Object> {

	/**
	 * @param service
	 * @param context
	 */

	private NetHelper.onWorkCompleteListener listener;

	public PostTask(NetService service, Context context) {
		super();
		this.service = service;
		this.context = context;
		this.listener = null;
	}

	public PostTask(NetService service, Context context,
			NetHelper.onWorkCompleteListener listener) {
		super();
		this.service = service;
		this.context = context;
		this.listener = listener;
	}

	NetService service;
	Context context;

	@Override
	protected Object doInBackground(String... params) {

		try {
			return NetHelper.getInstance().post(service);
		} catch (ClientProtocolException e) {
			Log.e("protocol", "exception");

			e.printStackTrace();

		} catch (IOException e) {
			Log.e("io", "exception");
			e.printStackTrace();

		} catch (Exception e){
			e.printStackTrace();
			
		}
		return null;

	}

	@Override
	protected void onPostExecute(Object result) {
		Log.e("post execute",""+result);
		if (result != null && result.getClass() != RuntimeException.class
				&& result.getClass() != Exception.class) {
			try {
				Log.i("receive", (((RESTClient) result).getResponse()));
				if (this.listener == null)
					NetHelper.getInstance().getResultOfExecuter(service,
							((RESTClient) result).getResponse());
				else
					listener.onCompletion(((RESTClient) result).getResponse());
			} catch (Exception e) {
				e.printStackTrace();
				showNetworkProblem();
				NetHelper.getInstance().callErrorHander(e);
			}
		} else {
			showNetworkProblem();
			if (result == null){
				NetHelper.getInstance().callErrorHander(null);
			}else
			if (result.getClass() == RuntimeException.class) {
				// listener.onError(null);
				NetHelper.getInstance().callErrorHander((Exception) result);
			} else
				NetHelper.getInstance().callErrorHander(null);

		}

	}

	/**
	 * show a error message
	 */
	public void showNetworkProblem() {
		((Activity) context).runOnUiThread(() -> {
			Toast.makeText(context, "Problema en la conexión.",
					Toast.LENGTH_LONG).show();
		});
	}

}