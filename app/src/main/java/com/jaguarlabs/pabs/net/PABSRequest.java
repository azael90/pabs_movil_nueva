package com.jaguarlabs.pabs.net;

import android.util.Log;

import com.octo.android.robospice.request.SpiceRequest;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Class used to perform a network call
 */
public class PABSRequest extends SpiceRequest<JSONObject> {
	NetService service;
	public PABSRequest( NetService service) {
		super(JSONObject.class);
		this.service = service;
		
	}

	@Override
	public JSONObject loadDataFromNetwork() throws Exception {
		Log.e("starting","dowloading from network");
		JSONObject object = new JSONObject();
		try{
		object = new JSONObject( NetHelper.getInstance().post(service).getResponse());
		}catch(JSONException e){
			return new JSONObject();
		}
		return object;
	}

}
