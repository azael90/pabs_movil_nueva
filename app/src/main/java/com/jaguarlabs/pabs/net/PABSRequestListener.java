package com.jaguarlabs.pabs.net;

import com.jaguarlabs.pabs.net.NetHelper.onWorkCompleteListener;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.json.JSONObject;

/**
 * Request listener class that will update UI
 */
public class PABSRequestListener implements RequestListener<JSONObject> {
	private onWorkCompleteListener listener;
	
	public PABSRequestListener (onWorkCompleteListener listener){
		this.listener = listener;
	}
	@Override
	public void onRequestFailure(SpiceException arg0) {
		listener.onError(null);
	}


	@Override
	public void onRequestSuccess(JSONObject arg0) {
		listener.onCompletion(arg0.toString());
	}

}
