package com.jaguarlabs.pabs.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.RazonesAdapter;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.NoAportoCallback;
import com.jaguarlabs.pabs.util.Util;

/**
 * This class manages to show a reasons dialog as well as set its properties.
 * Reasons dialog where all reason about why the collector is making
 * a visit are listed.
 * You can notice this dialog when pressing 'No aporto'
 * {@link android.widget.Button} on 'ClienteDetalle' and 'Aportacion' screen.
 */
@SuppressWarnings("SpellCheckingInspection")
public class RazonesDialog extends Dialog implements View.OnClickListener {

	private NoAportoCallback callback;
	public RazonesDialog(Context context, NoAportoCallback callback) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_no_aporto);
		this.callback = callback;
		//get array with reasons
		String[] razones;// = getContext().getResources().getStringArray(R.array.razones_array);

		SharedPreferences preferences = ApplicationResourcesProvider.getContext().getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
		if ( preferences.getBoolean("isSupervisor", false) ){
			razones = getContext().getResources().getStringArray(R.array.razones_array_supervisor);
		} else {
			razones = getContext().getResources().getStringArray(R.array.razones_array);
		}


		RazonesAdapter adapter = new RazonesAdapter(getContext(), razones);

		//set ListView and its properties
		ListView lvRazones = (ListView)findViewById(R.id.lv_razon);
		lvRazones.setAdapter(adapter);
		lvRazones.setItemChecked(0, true);
		((FrameLayout)findViewById(R.id.fl_cancelar)).setOnClickListener(this);
		((FrameLayout)findViewById(R.id.fl_aceptar)).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.fl_cancelar:
				dismiss();
				break;
			case R.id.fl_aceptar:
				try
				{
					ListView lvRazones = (ListView)findViewById(R.id.lv_razon);
					Log.e("posicion tocada", "" + lvRazones.getCheckedItemPosition());
					//set behave after choosing a reason
					Util.Log.ih("lvRazones.getCheckedItemPosition() -> " + lvRazones.getCheckedItemPosition());
					callback.posicionSeleccionada(lvRazones.getCheckedItemPosition());
					dismiss();
					break;
				}catch (Exception ex)
				{
					Log.d("IMPRESION -->", "Ocurrio un error en razones Dialog");
				}
			default:
				//nothing
		}
	}
}
