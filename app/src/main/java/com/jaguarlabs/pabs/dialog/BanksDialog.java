package com.jaguarlabs.pabs.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.activities.Bloqueo;
import com.jaguarlabs.pabs.activities.Deposito;
import com.jaguarlabs.pabs.adapter.BanksArrayAdapter;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.Util;

import java.util.ArrayList;

/**
 * This class manages to show a banks dialog as well as set its properties.
 * Banks dialog where all bank names are listed.
 * You can notice this dialog when pressing 'Banco' TextField on
 * 'Deposito' screen.
 *
 */
@SuppressWarnings("SpellCheckingInspection")
public class BanksDialog extends Dialog implements OnItemClickListener{

	private int tipoUso;
	public BanksDialog(Context context, ArrayList<String> array, int tipoUso) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_banks);
		this.tipoUso = tipoUso;
		//set ListView
		ListView lv = (ListView)findViewById(R.id.lv_banco);
		BanksArrayAdapter adapter = new BanksArrayAdapter(getContext(), array);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		Util.Log.ih("bancs click");
		TextView tvTocada = (TextView)view;
		/*
		 * tipoUSo
		 * 0: Bloqueo
		 * 1: Deposito
		 */
		if (tipoUso == 0)
		{

			switch (BuildConfig.getTargetBranch())
			{
				case GUADALAJARA:
					switch (position)
					{
						case 1:
						case 3:
						case 5:
						case 7:
						case 9:
						case 11:
							//available banks

							//set 'banco' TextField with the name of bank selected
							TextView tvBanco = (TextView) Bloqueo.getMthis().findViewById(R.id.tv_banco);
							tvBanco.setText(tvTocada.getText().toString());
							//set behave after choosing a bank
							Bloqueo.getMthis().setBancoPosicion(position);
							dismiss();
							break;
						default:
							//unavailable banks
							//nothing...
					}
					break;
				default:
					//set 'banco' TextField with the name of bank selected
					TextView tvBanco = (TextView) Bloqueo.getMthis().findViewById(R.id.tv_banco);
					tvBanco.setText(tvTocada.getText().toString());
					//set behave after choosing a bank
					Bloqueo.getMthis().setBancoPosicion(position);
					dismiss();
			}
		}
		
		if (tipoUso == 1)
		{

			switch (BuildConfig.getTargetBranch())
			{
				case GUADALAJARA:
					switch (position)
					{
						case 1:
						case 3:
						case 5:
						case 7:
						case 9:
						case 11:
						case 12:
							//available banks

							//set 'banco' TextField with the name of bank selected
							TextView tvBanco = (TextView) Deposito.getMthis().getView().findViewById(R.id.tv_banco);
							tvBanco.setText(tvTocada.getText().toString());
							//set behave after choosing a bank
							Deposito.getMthis().setBancoPosicion(position);
							dismiss();
							break;
						default:
							//unavailable banks
							//nothing...
					}
					break;

					//----------------------------------------------------------------------------------------------
				/*case QUERETARO:
					//Validar el texto del JSON
					switch (tvTocada.getText().toString())
					{
						case "Oficina":
							TextView tvBanco = (TextView) Deposito.getMthis().getView().findViewById(R.id.tv_banco);
							tvBanco.setText(tvTocada.getText().toString());
							//set behave after choosing a bank
							Deposito.getMthis().setBancoPosicion(position);
							Toast.makeText(getContext(), "Click despues de seleccionar Oficina", Toast.LENGTH_SHORT).show();
							dismiss();
							break;
						/*case 1:
						case 2:
						case 3:
						case 4:
						case 5:
						case 6:
						case 7:
						case 8:
						case 9:
						case 11:
						case 12:
							//available banks
							//set 'banco' TextField with the name of bank selected
							TextView tvBanco = (TextView) Deposito.getMthis().getView().findViewById(R.id.tv_banco);
							tvBanco.setText(tvTocada.getText().toString());
							//set behave after choosing a bank
							Deposito.getMthis().setBancoPosicion(position);
							dismiss();
							break;
						case 10:
							//available banks
							//set 'banco' TextField with the name of bank selected
							TextView tvBanco2 = (TextView) Deposito.getMthis().getView().findViewById(R.id.tv_banco);
							tvBanco2.setText(tvTocada.getText().toString());
							//set behave after choosing a bank
							Toast.makeText(getContext(), "Posicion: "+position + " Texto: "+ tvTocada.getText().toString(), Toast.LENGTH_SHORT).show();
							Deposito.getMthis().setBancoPosicion(position);
							dismiss();
							break;
						default:
							Toast.makeText(getContext(), "Seleccionada: "+tvTocada.getText().toString(), Toast.LENGTH_SHORT).show();
							//unavailable banks
							//nothing...
					}
					break;*/

				//--------------------------------------------------------------------------------------------------
				default:
					//set 'banco' TextField with the name of bank selected
					TextView tvBanco = (TextView) Deposito.getMthis().getView().findViewById(R.id.tv_banco);
					tvBanco.setText(tvTocada.getText().toString());
					//set behave after choosing a bank
					Deposito.getMthis().setBancoPosicion(position);
					dismiss();
			}
		}
	}

}
