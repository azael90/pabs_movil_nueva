package com.jaguarlabs.pabs.networkingUtility;

import android.util.Log;

import com.jaguarlabs.pabs.util.ConstantsPabs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * With this class you can test if device has internet access.
 *
 * This class is used to test the reachability of a host
 * by sending a ping to the given host (ip address or domain).
 * After completing the ping, it summarizes its results,
 * to end up showing statistics about pings sent.
 *
 * if number of packets transmitted is greater than
 * {@code packetsNumberToSetAvailable}, will return true to
 * {@code reachable}, false otherwise.
 *
 * @author Jordan Alvarez
 * @version 26/10/15
 */
public class Ping {

    //host
    //could be either an ip address or a domain
    private static final String ipAddress = ConstantsPabs.IP;

    //number of packets to send to host
    private static final int packetsToSend = 5;

    //number of packet to receive to set as internet available
    private static final int packetsNumberToSetAvailable = 3;

    //TAG to use with log cat
    private static final String TAG = "PABS";

    /**
     * the callback that will run
     * @see {@link #setOnCheckInternetReachabilityListener}
     */
    private static OnCheckInternetReachabilityListener onCheckInternetReachability;

    /**
     * Method in charge to ping if device has internet access.
     * Everthing runs in another {@link Thread} using a
     * {@link Runnable}
     *
     * @see {@link Thread}
     * @see {@link Runnable}
     */
    public static void checkInternetReachability(){

        Runnable runnable = () -> {
            final Runtime runtime = Runtime.getRuntime();

            try
            {
                Log.i(TAG, "starting ping...");

                //ping
                final Process mIpAddrProcess = runtime
                        .exec("/system/bin/ping -c "
                                + packetsToSend
                                + " "
                                + ipAddress);

                final BufferedReader reader = new BufferedReader(
                        new InputStreamReader(mIpAddrProcess.getInputStream()));

                //get info of each ping and save to output
                int i;
                char[] buffer = new char[4096];
                StringBuilder output = new StringBuilder();
                while ((i = reader.read(buffer)) > 0)
                    output.append(buffer, 0, i);
                reader.close();

                Log.i(TAG, "ping finished");

                //get number of packets sent
                String data = output.toString();
                data = data.substring(
                        data.indexOf(",") + 2,
                        data.indexOf(",") + 4);

                data = data.replace(" ", "");

                final int internetAvailable;
                internetAvailable = Integer.valueOf(data);

                //internet connection is available
                if (internetAvailable >= packetsNumberToSetAvailable){
                    onCheckInternetReachability.onCheckInternetReachability(true);
                }
                //internet connection is not available
                else{
                    onCheckInternetReachability.onCheckInternetReachability(false);
                }

                Log.i(TAG, "ping info\n" + output.toString());

            }
            catch (IOException | IndexOutOfBoundsException e) {
                onCheckInternetReachability.onCheckInternetReachability(false);
                e.printStackTrace();
            } catch (Exception e){
                onCheckInternetReachability.onCheckInternetReachability(false);
                e.printStackTrace();
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    /**
     * Register a callback to invoke {@link #checkInternetReachability()}
     * right away
     *
     * @param listener
     *              The callback that will run
     */
    public static void setOnCheckInternetReachabilityListener(OnCheckInternetReachabilityListener listener){
        onCheckInternetReachability = listener;
        checkInternetReachability();
    }

    /**
     * Interface definition for a callback when internet access test
     * has ended
     */
    public interface OnCheckInternetReachabilityListener {

        /**
         * Called when internet access test has finished.
         *
         * @param reachable
         *          boolean type to report if internet access
         *          succeeded
         */
        void onCheckInternetReachability(boolean reachable);
    }
}
