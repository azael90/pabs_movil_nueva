package com.jaguarlabs.pabs.util;

import android.os.Build;

import com.jaguarlabs.pabs.activities.Configuracion;

/**
 * This class defines flags that depending on their
 * values, the app behaves differently
 *
 * @autor Created by adria_000 on 24/02/2015.
 * @author Modified by Jordan Alvarez
 * @version 3.0
 */
@SuppressWarnings("FieldCanBeLocal")
public final class BuildConfig {

    public static Branches targetBranch = Branches.GUADALAJARA;
    private static boolean isForSplunkMintMonitoring = true;//!Build.MODEL.equalsIgnoreCase("F600");
    private static boolean autoPrintingEnable = false;
    private static boolean isAutoLoginEnable = false;
    private static boolean printingCopiesEnabled = false;
    private static boolean connectivityTestWithPingEnabled = true;
    private static boolean walletSynchronizationEnabled = true;
    /*
	 this flag is intended to be used as a check for debug-only functions.
	 set to false, when not debugging anymore
	 */
    public static boolean DEBUG = false;

    @SuppressWarnings("SpellCheckingInspection")
    public static enum Branches{GUADALAJARA, TOLUCA, PUEBLA, NAYARIT, MERIDA, SALTILLO, CANCUN, QUERETARO, CUERNAVACA, MEXICALI, IRAPUATO, MORELIA, SALAMANCA, TORREON, TAMPICO, CELAYA, LEON}

    private BuildConfig(){}

    /**
     * retrieves target branch of application
     *
     * @return
     *      enum data type
     */
    public static Branches getTargetBranch(){
        return targetBranch;
    }

    /**
     * retrieves isForSplunkMintMonitoring flag, telling if it's enabled
     *
     * @return
     *      boolean data type
     */
    public static boolean isIsForSplunkMintMonitoring() {
        return isForSplunkMintMonitoring;
    }

    /**
     * retrieves autoPrintingEnable flag, telling if it's enabled
     *
     * @return
     *      boolean data type
     */
    public static boolean isAutoPrintingEnable() {
        return autoPrintingEnable;
    }

    /**
     * retrieves isAutoLoginEnable flag, telling if it's enabled
     *
     * @return
     *      boolean data type
     */
    public static boolean isAutoLoginEnable() {
        return isAutoLoginEnable;
    }

    /**
     * retrieves isPrintingCopiesEnabled flag, telling if it's enabled
     *
     * @return
     *      boolean data type
     */
    public static boolean isPrintingCopiesEnabled(){
        return printingCopiesEnabled;
    }

    /**
     * retrieves connectivityTestWithPingEnabled flag, telling if it's enabled
     *
     * @return
     *      boolean data type
     */
    public static boolean isConnectivityTestWithPingEnabled(){
        return connectivityTestWithPingEnabled;
    }

    public static boolean isWalletSynchronizationEnabled(){
        return walletSynchronizationEnabled;
    }

    public static void setWalletSynchronizationEnabled(boolean flag){
        walletSynchronizationEnabled = flag;
    }

    public static void setIsForSplunkMintMonitoring(boolean flag){
        isForSplunkMintMonitoring = flag;
    }

    public static boolean isDEBUG() {
        return DEBUG;
    }

    public static void setDEBUG(boolean DEBUG) {
        BuildConfig.DEBUG = DEBUG;
    }
}
