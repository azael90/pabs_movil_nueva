package com.jaguarlabs.pabs.util;

public interface CerrarSesionCallback {

	/**
	 * This method finishes all activities loaded by reference 'mthis'
	 * on each activity if they weren't finished yet.
	 * calls cerrarSesion() right after done that.
	 */
	public void cerrarSesionTimeOut();
}
