package com.jaguarlabs.pabs.util;

import android.location.Location;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.activities.Clientes;
import com.jaguarlabs.pabs.database.Clients;
import com.jaguarlabs.pabs.database.SyncedWallet;
import com.jaguarlabs.pabs.models.ModelLocations;
import com.jaguarlabs.pabs.models.ModelPayment;

import org.apache.commons.io.FileUtils;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

/**
 * Created by adrian on 29/04/15.
 *
 * This class writes into '.txt' files movements as
 * - payments
 * - visits
 * - deposits
 * - cancellations
 */
public class LogRegister {

    /**
     * registers a payment or visit made
     *
     * @param contrato
     *          contract number
     * @param monto
     *          amount
     * @param empresa
     *          company
     * @param folio
     *          folio
     * @param periodo
     *          period
     */
    public static void registrarCobro(String contrato, String monto, String empresa, String folio, String periodo){

        try {
            //instantiates variables
            String fecha = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
            Calendar cal = Calendar.getInstance();
            //Calendar.MONTH begins from 0
            String month = "" + (Integer.parseInt(""+cal.get(Calendar.MONTH)) + 1);
            String data = "";
            String path = Environment.getExternalStorageDirectory()+"/PABS_LOGS/Cobros/";
            File dir = new File(path);

            //check if path exists already
            if (!dir.exists()){
                dir.mkdirs();
            }

            //format amount when is less than 10 pesos
            if (month.length() == 1){
                month = "0"+month;
            }

            File file = new File(path
                    , "cobros "
                    + cal.get(Calendar.DAY_OF_MONTH) + "-" + month
                    + "-" + cal.get(Calendar.YEAR) + ".txt");

            if (!file.exists()){StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                data+=formatter.format("%10s;%10s;%8s;%5s;%20s;%40s\n", "Folio", "Contrato", "Monto", "Periodo", "Fecha log", "Empresa\n").toString();
            }

            {
                StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                data+=formatter.format("%10s;%10s;%8s;%5s;%20s;%40s\n", folio, contrato, monto, periodo, fecha, empresa).toString();
            }

            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);

            outputStreamWriter.write(data);

            outputStreamWriter.close();
        } catch (IOException e) {
            Log.d("Probenso", "File write failed: " + e.toString());
        }
    }

    /**
     * registers a deposit made
     *
     * @param deposito
     *          Log Deposito instance with values to be written into file
     */
    public static void registrarDeposito(LogDeposito deposito){

        try {
            //instantiates variables
            String fecha = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
            Calendar cal = Calendar.getInstance();
            //Calendar.MONTH begins from 0
            String month = "" + (Integer.parseInt(""+cal.get(Calendar.MONTH)) + 1);
            String esOffline = deposito.esOffline ? "Si":"No";
            String estadoDelDeposito = deposito.yaFueEnviadoAlServidor ? "Intentando enviar" : "Recibido por servidor";
            String data = "";
            String path = Environment.getExternalStorageDirectory()+"/PABS_LOGS/Depositos/";
            File dir = new File(path);

            //check if path exists already
            if (!dir.exists()){
                dir.mkdirs();
            }

            //format amount when is less than 10 pesos
            if (month.length() == 1){
                month = "0"+month;
            }

            File file = new File(path
                    , "depositos "
                    + cal.get(Calendar.DAY_OF_MONTH) + "-" + month
                    + "-" + cal.get(Calendar.YEAR) + ".txt");

            if (!file.exists()){StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                data+=formatter.format("%13s;%10s;%10s;%10s;%10s;%10s;%25s;%20s;%40s\n", "¿Es offline?", "Periodo", "Monto", "Cobrador", "Banco", "Referencia", "Estado", "Fecha log", "Empresa\n").toString();
            }

            {
                StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                data+=formatter.format("%13s;%10s;%10s;%10s;%10s;%25s;%20s;%40s\n", esOffline, deposito.periodo, deposito.monto, deposito.cobrador, deposito.banco, deposito.referencia, estadoDelDeposito, fecha, deposito.empresa).toString();
            }

            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);

            outputStreamWriter.write(data);

            outputStreamWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("Probenso", "File write failed: " + e.toString());
        }
    }

    /**
     * registers a cancellation made
     *
     * @param cancelacion
     *          Log Deposito instance with values to be written into file
     */
    public static void registrarCancelacion(LogCancelacion cancelacion){

        try {
            //instantiates variables
            String fecha = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
            Calendar cal = Calendar.getInstance();
            //Calendar.MONTH begins from 0
            String month = "" + (Integer.parseInt(""+cal.get(Calendar.MONTH)) + 1);
            String estadoDeLaCancelacion = cancelacion.fueEnviadoAlServidor ? "Recibido del servidor" : "Enviado al servidor";
            String data = "";
            String path = Environment.getExternalStorageDirectory()+"/PABS_LOGS/Cancelaciones/";
            File dir = new File(path);

            //check if path exists already
            if (!dir.exists()){
                dir.mkdirs();
            }

            //format amount when is less than 10 pesos
            if (month.length() == 1){
                month = "0"+month;
            }

            File file = new File(path
                    , "cancelaciones "
                    + cal.get(Calendar.DAY_OF_MONTH) + "-" + month
                    + "-" + cal.get(Calendar.YEAR) + ".txt");

            if (!file.exists()){StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                data+=formatter.format("%10s;%20s;%25s;%20s\n", "Numero de cobro", "Fecha de operación", "Estado", "Fecha log\n").toString();
            }

            {
                StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                data+=formatter.format("%%10s;%20s;%25s;%20s\n", cancelacion.no_cobro, cancelacion.fechaDeOperacion, estadoDeLaCancelacion, fecha).toString();
            }

            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);

            outputStreamWriter.write(data);

            outputStreamWriter.close();
        } catch (IOException e) {
            Log.d("Probenso", "File write failed: " + e.toString());
        }
    }

    /**
     * Class with values that will be used with {@link #registrarDeposito(LogDeposito)}
     * has to be set first, then send to {@link #registrarDeposito(LogDeposito)}
     */
    public static class LogDeposito {
        public String periodo, monto, empresa, cobrador, banco, referencia; public boolean yaFueEnviadoAlServidor, esOffline;

        public LogDeposito(String periodo, String monto, String empresa, String cobrador, String banco, String referencia, boolean yaFueEnviadoAlServidor){
            this.monto = monto;
            this.periodo = periodo;
            this.empresa = empresa;
            this.cobrador = cobrador;
            this.banco = banco;
            this.referencia = referencia;
            this.yaFueEnviadoAlServidor = yaFueEnviadoAlServidor;
        }
    }

    /**
     * Class with values that will be used with {@link #registrarCancelacion(LogCancelacion)}
     * has to be set first, then send to {@link #registrarCancelacion(LogCancelacion)}
     */
    public static class LogCancelacion{
        public String no_cobro, fechaDeOperacion; public boolean fueEnviadoAlServidor = false;
    }

    /**
     * This method will make a register of any payment made.
     * Will work as a backup for payments sent to server.
     *
     * @author Jordan Alvarez
     * @version 03/06/2015
     */
    public static void paymentRegistrationBackup(String folio, String contrato, String monto, String empresa, String periodo, String tipoDeCobro, String saldoPorCobro){

        try {
            //instantiates variables
            String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

            Calendar cal = Calendar.getInstance();
            //Calendar.MONTH begins from 0
            String month = "" + (Integer.parseInt(""+cal.get(Calendar.MONTH)) + 1);
            String data = "";
            String path = Environment.getExternalStorageDirectory()+"/PABS_LOGS/CobrosBackup/";
            File dir = new File(path);

            //check if path exists already
            if (!dir.exists()){
                dir.mkdirs();
            }

            //format amount when is less than 10 pesos
            if (month.length() == 1){
                month = "0"+month;
            }

            File file = new File(path
                    , "cobrosBackup "
                    + cal.get(Calendar.DAY_OF_MONTH) + "-" + month
                    + "-" + cal.get(Calendar.YEAR) + ".txt");

            if (!file.exists()){StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                data+=formatter.format("%7s;%10s;%7s;%9s;%24s;%10s;%27s;%10s;\n", "Folio", "Contrato", "Monto", "Periodo", "Fecha", "Empresa", "Tipo de Cobro", "SaldoPorCobro\n").toString();
            }

            {
                StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                data+=formatter.format("%7s;%10s;%7s;%9s;%24s;%10s;%27s;%10s;\n", folio, contrato, monto, periodo, fecha, empresa, tipoDeCobro, saldoPorCobro).toString();
            }

            //FileInputStream fIn = new FileInputStream(file);
            //InputStreamReader inputStreamReader = new InputStreamReader(fIn);

            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);

            outputStreamWriter.write(data);

            outputStreamWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
            Log.d("Probenso", "File write failed: " + e.toString());
        }
    }

    /**
     * registers when a session is ended by broadcast
     *
     * @param oldTime
     *          time when broadcast started
     * @param newTime
     *          time when broadcast ended session
     * @author Jordan Alvarez
     */
    public static void endSessionByBroadcast( long oldTime, long newTime ){

        try {
            //instantiates variables
            String fecha = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
            String data = "";
            String path = Environment.getExternalStorageDirectory()+"/PABS_LOGS/Broadcast/";
            File dir = new File(path);

            //check if path exists already
            if (!dir.exists()){
                dir.mkdirs();
            }

            File file = new File(path
                    , "EndedSession "
                    + fecha
                    + ".txt");

            if (!file.exists()){StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                data+=formatter.format("%15s;%22s\n", "oldTime", "newTime\n").toString();
            }

            {
                StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                data+=formatter.format("%20s;%20s;\n", oldTime, newTime).toString();
            }

            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);

            outputStreamWriter.write(data);

            outputStreamWriter.close();

        } catch (IOException e) {
            Log.d("Probenso", "File write failed: " + e.toString());
        }

    }

    public static String toGeoString(ArrayList<ModelLocations.GeoLocations> geoLocations, String collectorNumber) {
        String geoString = "";
        for (ModelLocations.GeoLocations location: geoLocations) {
            geoString += location.getDate() + "\t" + location.getLatitude() + "\t" + location.getLongitude() + "\t" + collectorNumber + "\n";
        }
        return geoString;
    }

    public static void createGeoJSONDataFile(ArrayList<ModelLocations.GeoLocations> geoLocations, String periodo, String collectorNumber){

        try {
            String path = Environment.getExternalStorageDirectory()+"/PABS_LOGS/Locations/";
            File dir = new File(path);

            //check if path exists already
            if (!dir.exists()){
                dir.mkdirs();
            }

            File file = new File(path
                    , "periodo "
                    + periodo
                    + ".json");

            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);

            outputStreamWriter.write( toGeoString( geoLocations, collectorNumber ) );
            outputStreamWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
            Log.d("PABS", "File write failed: " + e.toString());
        }
    }

    public static String readGeoJSONDataFile(String periodo)
    {
        //int periodo = 4;
        //Calendar cal = Calendar.getInstance();
        //String month = "" + (Integer.parseInt(""+cal.get(Calendar.MONTH)) + 1);
        String path = Environment.getExternalStorageDirectory()+"/PABS_LOGS/Locations/";
        File file = new File(path, "periodo " + periodo + ".json");
        String string = null;
        try {
            string = FileUtils.readFileToString(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Util.Log.ih("Read in: " + string);
        return string;
    }

    public static void deleteGeoJsonFile(String periodo) {
        String path = Environment.getExternalStorageDirectory()+"/PABS_LOGS/Locations/";
        File file = new File(path
                , "periodo "
                + periodo
                + ".json");
        file.delete();
    }

    public static void saveContractsIntoFile(final String contractID, final String option, final boolean contractFound, final SyncedWallet... contractInfo) {


        new Thread(() -> {
            try {
                //instantiates variables
                //String fecha = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
                Calendar cal = Calendar.getInstance();
                //Calendar.MONTH begins from 0
                String month = "" + (Integer.parseInt(""+cal.get(Calendar.MONTH)) + 1);
                String data = "";
                String path = Environment.getExternalStorageDirectory()+"/PABS_LOGS/contractsBackup/";
                File dir = new File(path);

                //check if path exists already
                if (!dir.exists()){
                    dir.mkdirs();
                }

                //format amount when is less than 10 pesos
                if (month.length() == 1){
                    month = "0"+month;
                }

                File file = new File(path
                        , "contractUpdates "
                        + cal.get(Calendar.DAY_OF_MONTH) + "-" + month
                        + "-" + cal.get(Calendar.YEAR) + ".txt");

            /*
            if (!file.exists()){
                StringBuilder sb = new StringBuilder();
                Formatter formatter = new Formatter(sb, Locale.US);
                data+=formatter.format("%7s;%10s;%7s;%9s;%24s;%10s;%20s;\n", "Folio", "Contrato", "Monto", "Periodo", "Fecha", "Empresa", "Tipo de Cobro\n").toString();
            }
            */

                if (contractFound){
                    data = option + " -> contractID " + contractID + "\n";
                    data += new Gson().toJson(contractInfo[0]);
                    data += "\n";
                } else {
                    data = option + " -> contractID " + contractID + "\n";
                    data += "contractID not found -> " + contractID;
                    data += "\n";
                }

                //FileInputStream fIn = new FileInputStream(file);
                //InputStreamReader inputStreamReader = new InputStreamReader(fIn);

                FileOutputStream fOut = new FileOutputStream(file, true);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fOut);

                outputStreamWriter.write(data);

                outputStreamWriter.close();

            } catch (IOException e) {
                e.printStackTrace();
                Log.d("PABS", "File write failed: " + e.toString());
            }
        }).start();

    }

    public static List<ModelPayment> getPaymentsFromTxt(String collectorNumber){
        String paymentRaw = Clientes.getMthis().getString(R.string.payments_from_txt);
        String[] paymentstxt = paymentRaw.split(";");

        List<ModelPayment> payments = new ArrayList<>();

        for (int a = 0, b = 0; a < paymentstxt.length / 7; a++, b+=7) {

            ModelPayment singlePayment = new ModelPayment();

            paymentstxt[b] = paymentstxt[b].trim();
            paymentstxt[b+1] = paymentstxt[b+1].trim();
            paymentstxt[b+2] = paymentstxt[b+2].trim();
            paymentstxt[b+3] = paymentstxt[b+3].trim();
            paymentstxt[b+4] = paymentstxt[b+4].trim();
            paymentstxt[b+5] = paymentstxt[b+5].trim();

            if (paymentstxt[b+2].equals("----")) {
                singlePayment.setFolio("----");
                singlePayment.setStatus("2");
                singlePayment.setAmount("0");
            } else {
                singlePayment.setFolio(paymentstxt[b]);
                singlePayment.setStatus("1");
                singlePayment.setAmount(paymentstxt[b+2]);
            }
            singlePayment.setIdClient(paymentstxt[b+1]);

            String serieAux = "";
            /*
            serieAux.
            for (int m = 0; m < paymentstxt[b+1].length(); m++) {
                if ( Integer.parseInt( paymentstxt[b+1][m] )  )
            }
            */
            serieAux = paymentstxt[b+1].substring( 0, paymentstxt[b+1].indexOf("0") );
            Util.Log.ih("serieAux = " + serieAux);

            String clientNumberAux = "";
            clientNumberAux = paymentstxt[b+1].substring( paymentstxt[b+1].indexOf("0")  );
            Util.Log.ih("clientNumberAux = " + clientNumberAux);

            String typeBDAux = "";
            List<Clients> clients = Clients.find(Clients.class, "number_contract = ?", clientNumberAux);
            if ( clients.size() > 0 ) {
                typeBDAux = clients.get(0).getTypeBD();
            }

            singlePayment.setSerie(serieAux);

            singlePayment.setDate(paymentstxt[b+4]);
            singlePayment.setCompany(paymentstxt[b+5]);
            singlePayment.setLatitude("20.679868");
            singlePayment.setLongitude("-103.383170");
            singlePayment.setIdCollector(collectorNumber);
            singlePayment.setTypeBD(  typeBDAux  );
            singlePayment.setCopies("0");
            singlePayment.setPeriodo( Integer.parseInt(paymentstxt[b+3]) );

            payments.add(singlePayment);
        }

        return payments;
    }

    public static JSONObject getPaymentsFromTxt(String collectorNumber, int periodo, String no_cobrador){
        String paymentRaw = Clientes.getMthis().getString(R.string.payments_from_txt);
        String[] paymentstxt = paymentRaw.split(";");

        List<ModelPayment> payments = new ArrayList<>();

        for (int a = 0, b = 0; a < paymentstxt.length / 7; a++, b+=7) {

            ModelPayment singlePayment = new ModelPayment();

            paymentstxt[b] = paymentstxt[b].trim();
            paymentstxt[b+1] = paymentstxt[b+1].trim();
            paymentstxt[b+2] = paymentstxt[b+2].trim();
            paymentstxt[b+3] = paymentstxt[b+3].trim();
            paymentstxt[b+4] = paymentstxt[b+4].trim();
            paymentstxt[b+5] = paymentstxt[b+5].trim();

            if (paymentstxt[b+2].equals("----")) {
                singlePayment.setFolio("----");
                singlePayment.setStatus("2");
                singlePayment.setAmount("0");
            } else {
                singlePayment.setFolio(paymentstxt[b]);
                singlePayment.setStatus("1");
                singlePayment.setAmount(paymentstxt[b+2]);
            }
            singlePayment.setIdClient(paymentstxt[b+1]);
            singlePayment.setSerie("");

            singlePayment.setDate(paymentstxt[b+4]);
            singlePayment.setCompany(paymentstxt[b+5]);
            singlePayment.setLatitude("20.679868");
            singlePayment.setLongitude("-103.383170");
            singlePayment.setIdCollector(collectorNumber);
            singlePayment.setTypeBD("01");
            singlePayment.setCopies("0");

            payments.add(singlePayment);
        }


        JSONObject json = new JSONObject();

        try {
            json.put("no_cobrador", no_cobrador);
            json.put("pagos", new JSONArray( new Gson().toJson( payments ) ));
            json.put("periodo", periodo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return json;
    }

    public static void registrarCheat(String cheat)
    {
        try {
            //LocalDateTime hoy = LocalDateTime.now();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            String path = Environment.getExternalStorageDirectory() + "/PABS_LOGS/.Cheats/";
            File dir = new File(path);

            if (!dir.exists())
                dir.mkdirs();

            //File file = new File(path, "cheats " + hoy.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + ".txt");
            File file = new File(path, "cheats " + dateFormat.format(date) + ".txt");
            String result = "";

            if (!file.exists()) {
                result  += "Cheat;\tFecha;\n\r";
            }

            FileOutputStream fileOutputStream = new FileOutputStream(file, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);

            //result += cheat + "\t" + hoy.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")) + ";\n\r";
            result += cheat + ";\t" + dateFormat.format(date) + ";\n\r";

            outputStreamWriter.write(result);

            outputStreamWriter.close();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    public static void registrarSemaforo(String task)
    {

        //return;
        try {
            LocalDateTime today = LocalDateTime.now();
            String path = Environment.getExternalStorageDirectory() + "/PABS_LOGS/.Semaforo/";
            File dir = new File(path);

            if (!dir.exists())
                dir.mkdirs();

            File file = new File(path, "semaforo-" + today.toString("dd-MM-yyyy") + ".txt");

            String result = "";

            if (!file.exists())
                result += "Accion;\tFecha;\n\r";

            FileOutputStream outputStream = new FileOutputStream(file, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);

            result += task + ";\t" + today.toString("dd-mm-yyyy HH:mm:ss") + ";\n\r";

            outputStreamWriter.write(result);

            outputStreamWriter.close();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    public static void registrarActualizacion(String choice)
    {
        try {
            LocalDateTime today = LocalDateTime.now();
            /*SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();*/
            String path = Environment.getExternalStorageDirectory() + "/PABS_LOGS/.Update/";
            File dir = new File(path);

            if (!dir.exists())
                dir.mkdirs();

            File file = new File(path, "update " + today.toString("dd-MM-yyyy") + ".txt");
            //File file = new File(path, "update " + dateFormat.format(date) + ".txt");
            String result = "";

            if (!file.exists()) {
                result  += "Accion;\tFecha;\n\r";
            }

            FileOutputStream fileOutputStream = new FileOutputStream(file, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);

            result += choice + ";\t" + today.toString("dd-MM-yyyy HH:mm:ss") + ";\n\r";
            //result += choice + "\t" + dateFormat.format(date) + ";\n\r";

            outputStreamWriter.write(result);

            outputStreamWriter.close();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    /*public static void registrarUltimaUbicacion(Location location, LocalDateTime date)
    {
        try {
            String path = Environment.getExternalStorageDirectory() + "/PABS_LOGS/.location/";
            File dir = new File(path);

            if (!dir.exists())
                dir.mkdirs();

            File file = new File(path, "location.pabs");
            String data = Double.toString(location.getLatitude()) + "," + Double.toString(location.getLongitude()) + ";" + date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

            FileOutputStream fileOutputStream = new FileOutputStream(file);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);

            outputStreamWriter.write(data);

            outputStreamWriter.close();
            fileOutputStream.close();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }*/

    public static Location obtenerUltimaUbicacion()
    {
        Location location = new Location("");
        try {
            String path = Environment.getExternalStorageDirectory() + "/PABS_LOGS/.location/";
            File dir = new File(path);

            if (dir.exists())
            {
                File file = new File(dir, "location.pabs");

                if (file.exists())
                {
                    BufferedReader reader = new BufferedReader(new FileReader(file));

                    String coordinates = reader.readLine().trim().split(";")[0];

                    location.setLatitude(Double.parseDouble(coordinates.split(",")[0]));
                    location.setLongitude(Double.parseDouble(coordinates.split(",")[1]));
                }
            }
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }

        return location;
    }




}