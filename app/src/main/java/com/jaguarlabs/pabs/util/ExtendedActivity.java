package com.jaguarlabs.pabs.util;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidadvance.topsnackbar.TSnackbar;
import com.google.gson.Gson;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.activities.BaseSpiceActivity;
import com.jaguarlabs.pabs.activities.Bloqueo;
import com.jaguarlabs.pabs.activities.Cierre;
import com.jaguarlabs.pabs.activities.CierreInformativo;
import com.jaguarlabs.pabs.activities.Clientes;
import com.jaguarlabs.pabs.activities.Deposito;
import com.jaguarlabs.pabs.activities.Login;
import com.jaguarlabs.pabs.activities.MainActivity;
import com.jaguarlabs.pabs.activities.Splash;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinterHelper;
import com.jaguarlabs.pabs.components.Preferences;
import com.jaguarlabs.pabs.components.PreferencesToShowWallet;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.ExportImportDB;
import com.jaguarlabs.pabs.database.MontosTotalesDeEfectivo;
import com.jaguarlabs.pabs.database.Periods;
import com.jaguarlabs.pabs.models.ModelDeposit;
import com.jaguarlabs.pabs.models.ModelPayment;
import com.jaguarlabs.pabs.models.ModelPeriod;
import com.jaguarlabs.pabs.net.NetHelper;
import com.jaguarlabs.pabs.net.NetService;
import com.jaguarlabs.pabs.net.VolleySingleton;
import com.jaguarlabs.pabs.networkingUtility.Ping;
import com.pd.chocobar.ChocoBar;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionListener;

/**
 * Class used as parent of all activities in the app.
 * Class with common methods that provide resources and
 * that can be used whenever needed.
 */
public class ExtendedActivity extends BaseSpiceActivity {
	public static long userInteractionTime = 0;
	public static boolean synched = false;
	public static final int DURATION_REQUEST_VOLLEY_IN_MILLIS = 90000;
	private static final long TIME_SESION = ((1000 * 60) * 20);// cambia el 5 por
															// el 20 y yap
															// (:

	/*
	 this flag is intended to be used as a check for debug-only functions.
	 set to false when not debugging anymore
	 */
	public TextView tv_efectivo_deposito;
	public TextView tvInternet;
	public ListView lv_cierre;
	private boolean debug = true;
	public SharedPreferences efectivoPreference;
	private Editor efectivoEditor;
	public boolean isConnected = false;

	public int numeroDeVecesQueSeVeraElAvisoDeInternet = 2;

	private List<ModelPayment> offlinePayments;

	/*
	public static boolean periodOfWeekEnded;

	static {
		PreferencesToShowWallet preferencesToShowWallet = new PreferencesToShowWallet(ApplicationResourcesProvider.getContext());
		periodOfWeekEnded = preferencesToShowWallet.lastSynchronizationToResetContractsPerWeek();
	}
	*/

	private SharedPreferences datePreference;

	public Editor getEfectivoEditor() {
		return efectivoEditor;
	}

	/**
	 * Este metodo revisa si esta disponible la conexi�n en el dispositivo,
	 * retorna true en caso de que si.
	 */
	public boolean isOnline() {
		return ((ConnectivityManager) getApplicationContext().getSystemService(
				Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;

	}

	public static boolean setInternetAsReachable = false;

	/**
	 * checks if there's an active internet connection
	 * @param context
	 */
	public void hasActiveInternetConnection(Context context, View view, final ImageView... thisIsNeededWhenLongClickingOnRefresh) {

		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		isConnected = activeNetworkInfo != null && activeNetworkInfo.isConnected();

		if (this instanceof Splash && new Preferences(ApplicationResourcesProvider.getContext()).getServerURL().equals("TEST")) {
			Toast.makeText(ApplicationResourcesProvider.getContext(), "SERVIDOR DE PRUEBAS", Toast.LENGTH_LONG).show();
		}

		if (BuildConfig.isConnectivityTestWithPingEnabled() && !setInternetAsReachable)
		{

			Util.Log.ih("normal ping");

			Ping.setOnCheckInternetReachabilityListener(new Ping.OnCheckInternetReachabilityListener() {

				@Override
				public void onCheckInternetReachability(boolean reachable) {
					isConnected = reachable;
					runOnUiThread(new Runnable() {
						boolean reachable;

						@Override
						public void run() {
							//toastSReuse((reachable ? "Si" : "No") + " hay internet");
							/*TSnackbar snackbar = TSnackbar.make(view, (reachable ? "Si" : "No") + " hay internet", TSnackbar.LENGTH_INDEFINITE);
							View snackView = snackbar.getView();
							TextView snackText = snackView.findViewById(com.jaguarlabs.pabs.activities.Login.r.id.tvInternet);
							 */





							

							//Aqui estaba el indicador del SnackBar que dijiste










							/*ChocoBar.builder().setBackgroundColor(Color.parseColor(reachable ? "#4caf50" : "#ef5350"))
									.setTextSize(12)
									.setTextColor(Color.parseColor("#FFFFFF"))
									.setTextTypefaceStyle(Typeface.NORMAL)
									.setText((reachable ? "      SI" : "      NO") + " TIENES INTERNET   ")
									.setMaxLines(1)
									.centerText()
									.setIcon(reachable ? R.drawable.si_wifi : R.drawable.no_wifi)
									.setActivity(ExtendedActivity.this)
									.setDuration(ChocoBar.LENGTH_SHORT)
									.build()
									.show();*/

						}

						public Runnable setData(boolean reachable) {
							this.reachable = reachable;
							return this;
						}
					}.setData(reachable));

					stopRefreshButtonFromRotatingIfNeeded(thisIsNeededWhenLongClickingOnRefresh);
				}
			});
		}
		else if (setInternetAsReachable){
			Util.Log.ih("else if (setInternetAsReachable){");
			isConnected = true;
			//toastSReuse("Si hay internet");
			//Snackbar.make(view, "Si hay internet", Snackbar.LENGTH_SHORT).show();
			TSnackbar snackbar = TSnackbar.make(view, "Si hay internet", TSnackbar.LENGTH_SHORT);
			View snackView = snackbar.getView();
			TextView snackText = snackView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
			snackText.setTextColor(Color.WHITE);

			snackbar.show();
			stopRefreshButtonFromRotatingIfNeeded(thisIsNeededWhenLongClickingOnRefresh);
		}
		else{
			if (isConnected){
				//toastSReuse("si hay internet");
				//Snackbar.make(view, "Si hay internet", Snackbar.LENGTH_SHORT).show();
				TSnackbar snackbar = TSnackbar.make(view, "Si hay internet", TSnackbar.LENGTH_SHORT);
				View snackView = snackbar.getView();
				TextView snackText = snackView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
				snackText.setTextColor(Color.WHITE);

				snackbar.show();
			}
			else{
				//toastSReuse("No hay internet");
				//Snackbar.make(view, "No hay internet", Snackbar.LENGTH_SHORT).show();
				TSnackbar snackbar = TSnackbar.make(view, "No hay internet", TSnackbar.LENGTH_SHORT);
				View snackView = snackbar.getView();
				TextView snackText = snackView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
				snackText.setTextColor(Color.WHITE);

				snackbar.show();
			}
			stopRefreshButtonFromRotatingIfNeeded(thisIsNeededWhenLongClickingOnRefresh);
		}
	}

	/**
	 * checks if there's internet access in order to send payments to server
	 */
	public void sendPaymentsIfActiveInternetConnection(){
		if (BuildConfig.isConnectivityTestWithPingEnabled()) {
			Ping.setOnCheckInternetReachabilityListener(reachable -> {
				if (reachable) {
					Util.Log.ih("sending payments");
					//toastSReuse("Iniciando sincronización.");
					if (Clientes.getMthis() != null) {

						Util.Log.ih("cllientes.getMthis != null");
						Clientes.getMthis().registerPagos();
					} else {
						Util.Log.ih("cllientes.getMthis = null");
					}
				}
			});
		}
	}

	/**
	 * stops refresh button animation when is rotating
	 * @param thisIsNeededWhenLongClickingOnRefresh
	 */
	public void stopRefreshButtonFromRotatingIfNeeded(ImageView... thisIsNeededWhenLongClickingOnRefresh){
		if ( thisIsNeededWhenLongClickingOnRefresh.length > 0 ){
			runOnUiThread(new Runnable() {

				ImageView refreshButton;

				@Override
				public void run() {
					refreshButton.clearAnimation();
				}

				public Runnable setData(ImageView refreshButton) {
					this.refreshButton = refreshButton;
					return this;
				}
			}.setData(thisIsNeededWhenLongClickingOnRefresh[0]));
		}
	}

	/**
	 * gets total cash
	 *
	 * @return
	 * 		total cash
	 */
	public float getTotalEfectivo()
	{
		float efectivoTotal = 0;
		float cantidadcancelados = 0;
		SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
		int periodo = efectivoPreference.getInt("periodo", 0);

		String query = "SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE periodo = '"+periodo+"'";
		List<MontosTotalesDeEfectivo> listaMontos = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
		if(listaMontos.size() > 0)
		{
			//efectivoTotal = Float.valueOf(listaMontos.get(0).getCobros_mas_cancelados());
			efectivoTotal += Float.valueOf(listaMontos.get(0).getCobros_mas_cancelados()) - Float.valueOf(listaMontos.get(0).getCancelados());
		}


		/*List<Companies> companies = DatabaseAssistant.getCompanies();
		for (int i = 0; i < companies.size(); i++)
		{
			efectivoTotal += getEfectivoPreference().getFloat(companies.get(i).getName(), 0);
			efectivoTotal -= DatabaseAssistant.getTotalCanceledByCompany(companies.get(i).getIdCompany());
		}*/

		//return (float) (efectivoTotal - cantidadcancelados);
		return (float) efectivoTotal;
	}






	public static float getTotalEfectivoWhenMoreThanOnePeriodIsActive(int periodo)
	{
		float efectivoTotal = 0;
		//float cantidadcancelados = 0;

		efectivoTotal = DatabaseAssistant.getCompaniesTotalCashAmountWhenMoreThanOnePeriodIsActive(periodo);
		//cantidadcancelados = DatabaseAssistant.getTotalCanceledWhenMoreThanOnePeriodIsActive(periodo);

		return (float) (efectivoTotal );//- cantidadcancelados);
	}

	public static float getTotalEfectivoFromAllPeriods()
	{
		List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();
		float totalFromAllPeriods = 0;

		for (ModelPeriod mp: periods) {
			totalFromAllPeriods += getTotalEfectivoWhenMoreThanOnePeriodIsActive(mp.getPeriodNumber());
		}

		return totalFromAllPeriods;

		//---

	}

	/*public static float getTotalEfectivoFromAllPeriods(int periodo)
	{
		String query = "SELECT * FROM MONTOS_TOTALES_DE_EFECTIVO WHERE periodo ='"+periodo+"'";
		List<MontosTotalesDeEfectivo> listaMontos = MontosTotalesDeEfectivo.findWithQuery(MontosTotalesDeEfectivo.class, query);
		float totalFromAllPeriods = 0;
		if(listaMontos.size() > 0)
		{
			for (MontosTotalesDeEfectivo mp: listaMontos)
			{
				totalFromAllPeriods += getTotalEfectivoWhenMoreThanOnePeriodIsActive(periodo);
			}
		}
		return totalFromAllPeriods;
	}*/

	/**
	 * sharedPreferences file 'efecitvo'
	 * with info of every company
	 * @return
	 */
	public SharedPreferences getEfectivoPreference() {
		return efectivoPreference;
	}

	public Handler handlerSesion;
	public Runnable myRunnable;
	private static boolean estoyLogin = false;

	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*
        if (BuildConfig.isIsForSplunkMintMonitoring()){
            Mint.initAndStartSession(getApplicationContext(), "4af0dfc4");
        }
		*/
		//hasActiveInternetConnection(this);
		//tv_efectivo_deposito= (TextView)findViewById(R.id.tv_efectivo_deposito);

		efectivoPreference = getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
		efectivoEditor = efectivoPreference.edit();
		efectivoEditor.apply();
		Nammu.init(getApplicationContext());
        //AndroidThreeTen.init(this);
		init();
	}

	protected void init() {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		String asd = Build.BRAND;
		String asda = Build.MODEL;
		lv_cierre= (ListView) findViewById(R.id.lv_cierre);
		Util.Log.dh("SERIAL = " + Build.SERIAL );
		Util.Log.dh("FINGERPRINT = " + Build.FINGERPRINT );
		Util.Log.dh("ID = " + Build.ID );

		if (Nammu.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
			TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			String deviceid = manager.getDeviceId();

			Util.Log.dh("deviceid = " + deviceid);
		}

		DatabaseAssistant.getRadioToShowNearbyClients();

		if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			if (this instanceof Splash) {
				window.setStatusBarColor(getResources().getColor(R.color.splash_notification));
			} else if (this instanceof Login) {
				window.setStatusBarColor(getResources().getColor(R.color.base_color_notification));
			} else {
				window.setStatusBarColor(getResources().getColor(R.color.topbar_notification));
			}
		}
	}

	/**
	 * start inactivity timer
	 * timer which ends session after 20 minutes
	 */
	public void iniciarHandlerInactividad() {
		if (handlerSesion != null) {
			handlerSesion.removeCallbacks(myRunnable);
			handlerSesion = null;
		}

		if (myRunnable == null) {
			myRunnable = () -> {
				if (!isEstoyLogin()) {
					View refBloqueo = null;
					if (Bloqueo.getMthis() != null) {
						refBloqueo = Bloqueo.getMthis().findViewById(
								R.id.tv_banco);
					}

					View refMain = null;
					if (Clientes.getMthis() != null) {
						/*refMain = Clientes.getMthis().getView().findViewById(
								R.id.listView_clientes);*/
						if (Clientes.getMthis().getView() != null)
							refMain = Clientes.getMthis().getView().findViewById(
									R.id.rv_clientes);
					}

					View refCierre = null;
					if (Cierre.getMthis() != null) {
						refCierre = lv_cierre;//Cierre.getMthis().getView().findViewById(
								//R.id.lv_cierre);
					}

					View refDeposito = null;
					if (Deposito.getMthis() != null) {
						refDeposito = Deposito.getMthis().getView().findViewById(
								R.id.tv_efectivo_deposito);
					}

					if (refBloqueo != null || refMain != null
							|| refCierre != null || refDeposito != null) {
						if (refMain != null) {
							CerrarSesionCallback callTmp = (CerrarSesionCallback) Clientes
									.getMthis();
							callTmp.cerrarSesionTimeOut();
						} else if (refCierre != null) {
							CerrarSesionCallback callTmp = (CerrarSesionCallback) Cierre
									.getMthis();
							callTmp.cerrarSesionTimeOut();
						} else if (refBloqueo != null) {
							CerrarSesionCallback callTmp = (CerrarSesionCallback) Bloqueo
									.getMthis();
							callTmp.cerrarSesionTimeOut();
						} else if (refDeposito != null) {
							CerrarSesionCallback callTmp = (CerrarSesionCallback) Deposito
									.getMthis();
							callTmp.cerrarSesionTimeOut();
						}
					}
					Util.Log.ih("Cesión cerrada por inactividad");
				}
			};
		}

		if (handlerSesion == null) {
			handlerSesion = new Handler();
			handlerSesion.postDelayed(myRunnable, TIME_SESION);
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		//Toast.makeText(this, "On resume", Toast.LENGTH_SHORT).show();
		//hasActiveInternetConnection(this);
		/*if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION))
        	new Handler().postDelayed(ApplicationResourcesProvider::startLocationUpdates, 1500);*/
	}

	@Override
	protected void onPause() {
		super.onPause();
		//Toast.makeText(this, "on Pause", Toast.LENGTH_SHORT).show();
		//ApplicationResourcesProvider.stopLocationUpdates();

        /*IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = registerReceiver(null, intentFilter);

        if (batteryStatus != null) {
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);

            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            float batteryPct = level / (float)scale * 100;

            if (batteryPct < 40)
                ApplicationResourcesProvider.stopLocationUpdates();
        }*/
	}

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		synched = false;
		ExtendedActivity.userInteractionTime = System.currentTimeMillis();
		if (handlerSesion != null) {
			handlerSesion.removeCallbacks(myRunnable);
			handlerSesion = null;
		}

		if (!isEstoyLogin()) {
			View refBloqueo = null;
			if (Bloqueo.getMthis() != null) {
				refBloqueo = Bloqueo.getMthis().findViewById(R.id.tv_banco);
			}

			View refMain = null;
			if (Clientes.getMthis() != null) {
				/*refMain = Clientes.getMthis().getView().findViewById(
						R.id.listView_clientes);*/
				if (Clientes.getMthis().getView() != null)
					refMain = Clientes.getMthis().getView().findViewById(
							R.id.rv_clientes);
			}

			View refCierre = null;
			if (Cierre.getMthis() != null)
			{
				refCierre =  lv_cierre; // Cierre.getMthis().getView().findViewById(R.id.lv_cierre);
			}

			View refDeposito = null;
			if (Deposito.getMthis() != null)
			{
				refDeposito = tv_efectivo_deposito;
				//refDeposito = Deposito.getMthis().getView().findViewById(R.id.tv_efectivo_deposito); //---------------------------------------------------------------------------
			}
			if (refMain != null) {
				Clientes.getMthis().llamarTimerInactividad();
			} else if (refBloqueo != null) {
				Bloqueo.getMthis().llamarTimerInactividad();
			} else if (refCierre != null) {
				Cierre.getMthis().llamarTimerInactividad();
			} else if (refDeposito != null) {
				Deposito.getMthis().llamarTimerInactividad();
			}

		}

	}

	@Override
	public void onUserLeaveHint() {
		super.onUserLeaveHint();
		long uiDelta = (System.currentTimeMillis() - ExtendedActivity.userInteractionTime);
		if (uiDelta < 1000) {
			Log.e("PABS", "Home key pressed");
		} else {
			Log.e("PABS", "We are leaving wii");
		}
	}

	/**
	 * 
	 * Este metodo muestra un AlertDialog.Builder
	 * 
	 * @param title
	 * @param message
	 * @param isCancelable
	 */
	public void showAlertDialog(String title, String message,
			boolean isCancelable) {
		try {
			AlertDialog.Builder alert = null;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
				alert = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
			}
			else{
				alert = new AlertDialog.Builder(this);
			}
			alert.setTitle(title);
			alert.setMessage(message);
			alert.setCancelable(isCancelable);
			alert.setPositiveButton("Aceptar", (dialog, which) -> dialog.dismiss());
			alert.create().show();
		} catch (Exception e){
			Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			e.printStackTrace();
		}
	}

	/**
	 * Check if there conexion to internet
	 * 
	 * @return A boolean with answer
	 */
	public boolean getStatusConnection() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	/**
	 * formats money
	 *
	 * @param money
	 * 			money type
	 * @return
	 * 		formatted money
	 */
	public String formatMoney(double money) {
		DecimalFormatSymbols nf = new DecimalFormatSymbols();
		nf.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("#.##", nf);
		df.setMinimumFractionDigits(2);
		df.setMaximumFractionDigits(2);
		return df.format(money);
	}

	/**
	 * checks if param is of type money
	 *
	 * @param prueba
	 * 			money type
	 * @return
	 * 		true -> if param is of type money
	 * 		false -> if param is not if type money
	 */
	public boolean isMoneyType(String prueba) {
		if (prueba.contains(",")) {
			prueba = prueba.replace(",", ".");
		}
		
		
		boolean respuesta = false;
		try {
			double moneda = Double.parseDouble(prueba);
			if (moneda > 0) {
				respuesta = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return respuesta;
	}

	/**
	 * This method shows Log message in console
	 * 
	 * @param tag
	 * @param msj
	 */
	public void log(String tag, String msj) {
		if (debug)
			Log.d(tag, msj);
	}

	/**
	 * if current activity is {@link Login}
	 *
	 * @return
	 * 		true -> current activity is {@link Login}
	 * 		false -> current activity is not {@link Login}
	 */
	public static boolean isEstoyLogin() {
		return estoyLogin;
	}

	/**
	 * set if activity is {@link Login}
	 *
	 * @param estoyLogin
	 * 			true -> current activity is {@link Login}
	 * 			false -> current activity is not {@link Login}
	 */
	public static void setEstoyLogin(boolean estoyLogin) {
		ExtendedActivity.estoyLogin = estoyLogin;
	}

	/**
	 * prints formatted message in logcat
	 * of type 'info'
	 *
	 * @param stringId
	 * 			id of message to print
	 */
	public void LogIh(int stringId){
		Util.Log.ih(getString(stringId));
	}

	/**
	 * prints formatted message in logcat
	 * of type 'error'
	 *
	 * @param stringId
	 * 			id of message to print
	 */
	public void LogEh(int stringId){
		Util.Log.eh(getString(stringId));
	}

	/**
	 * prints a short message on screen
	 * using toast
	 *
	 * @param stringId
	 * 			id of message to print
	 */
	public void toastS(int stringId) {
		Util.UI.toastS(this, getString(stringId));
	}

	/**
	 * prints a long message on screen
	 * using toast
	 *
	 * @param stringId
	 * 			id of message to print
	 */
	public void toastL(int stringId) {
		Util.UI.toastL(this, getString(stringId));
	}

	/**
	 * prints a short message on screen
	 * using toast
	 *
	 * @param message
	 * 			String of message to print
	 */
	public void toastS(String message){
		runOnUiThread(() -> Util.UI.toastS(this, message));
	}

	/**
	 * prints a long message on screen
	 * using toast
	 *
	 * @param message
	 * 			String of message to print
	 */
	public void toastL(String message){
		runOnUiThread(() -> Util.UI.toastL(this, message));
	}

	/**
	 * prints a long message on screen
	 * reusing toast if possible
	 *
	 * @param message
	 * 			String of message to print
	 */
	public void toastLReuse(String message){
		Util.UI.toastLongReuse(this, message);
	}

	/**
	 * prints a short message on screen
	 * reusing toast if possible
	 *
	 * @param message
	 * 			String of message to print
	 */
	public void toastSReuse(String message) {
		Util.UI.toastShortReuse(this, message);
	}

	/**
	 * checks if bluetooth adapter is supported and enabled
	 * @return
	 */
	public boolean checkBluetoothAdapter(){

		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
				.getDefaultAdapter();

		if (mBluetoothAdapter == null) {
			toastSReuse(getString(R.string.error_message_bluetooth_unsoported));

			return false;
		}
		else if (!mBluetoothAdapter.isEnabled()) {
			toastSReuse(getString(R.string.error_message_bluetooth_unactivated));
			return false;
		}
		return true;
	}

	/**
	 * sets date
	 */
	public void setSharedPreferencesForDateWhenNoInternetAccess(){
		datePreference = getSharedPreferences("FILE_DATE", Context.MODE_PRIVATE);
		Editor editor = datePreference.edit();
		editor.putInt("DAY_OF_YEAR", Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
		editor.putInt("YEAR_", Calendar.getInstance().get(Calendar.YEAR));
		editor.apply();
	}

	/**
	 * gets date
	 *
	 * validation for when there's no internet
	 * @return
	 */
	public int[] getDateFromSharedPreference(){
		datePreference = getSharedPreferences("FILE_DATE", Context.MODE_PRIVATE);
		return new int[]{datePreference.getInt("DAY_OF_YEAR", 0),datePreference.getInt("YEAR_", 0)};
	}

	public boolean isActualMonthMaxDay() {
		Calendar calendar = Calendar.getInstance();
		if ( (calendar.get(Calendar.DAY_OF_MONTH)) == (calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) ) {
			return true;
		}
		return false;
	}

	public boolean isActualMonthMinDay() {
		Calendar calendar = Calendar.getInstance();
		if ( (calendar.get(Calendar.DAY_OF_MONTH)) == (calendar.getActualMinimum(Calendar.DAY_OF_MONTH)) ) {
			return true;
		}
		return false;
	}

	public void checkPrinterStatus(){
		TSCPrinterHelper.setOnCheckPrinterStatusErrorListener((error, errorType) -> {
            Util.Log.ih("PRINTER STATUS PRINTER STATUS PRINTER STATUS PRINTER STATUS PRINTER STATUS");
            Util.Log.ih("error = " + error);
            Util.Log.ih("errorType = " + errorType);
            runOnUiThread(() -> {
                AlertDialog.Builder alert = new AlertDialog.Builder(CierreInformativo.getMthis().getContext());
                alert.setTitle("PRINTER STATUS");
                alert.setMessage("error con la impresora");
                alert.setCancelable(true);
                alert.setPositiveButton("Aceptar", (dialog, which) -> dialog.dismiss());
                alert.create().show();
            });
        });
	}

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            return Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        else
            return Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) != 0;
    }

    private void manageRegisterDepositosOffline(JSONObject json, List<LogRegister.LogDeposito> logDepositos) {
        if (json.has("result")) {
            Log.e("result", json.toString());
            try {
                JSONObject result = json.getJSONObject("result");
                JSONArray almacenados = result.getJSONArray("almacenados");
                if (almacenados.length() > 0) {
                    syncDepositsSavedToServer(almacenados);
                }
                JSONArray repetidos = result.getJSONArray("repetidos");
                Util.Log.ih("repetidos = " + repetidos.toString());
                if (repetidos.length() > 0) {
                    syncDepositsSavedToServer(repetidos);
                }
                JSONArray noAlmacenados = result.getJSONArray("no_almacenados");
                if (noAlmacenados.length() > 0) {
                    Toast.makeText(getApplicationContext(), "Enviando depositos bancarios", Toast.LENGTH_LONG).show();
                    enviarDepositos();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void enviarDepositos() {
        if (DatabaseAssistant.isThereAnyDepositNotSynced()) {

            List<LogRegister.LogDeposito> logDepositos = null;
            JSONObject json = new JSONObject();

            try {
                //JSONArray depositos = query.mostrarTodosLosDepositosDesincronizados().getJSONArray("DEPOSITO");
                List<ModelDeposit> deposits = DatabaseAssistant.getAllDepositsNotSynced();
                if (deposits != null) {

                    json.put("no_cobrador", getEfectivoPreference().getString("no_cobrador", ""));
                    json.put("depositos", new JSONArray( new Gson().toJson( deposits ) ));
                    json.put("periodo", getEfectivoPreference().getInt("periodo", 0));
                    //Crear arraylist de depositos y registrarlos en log si el server responde
                    logDepositos = new ArrayList<>();

                    for (ModelDeposit d: deposits){
                        LogRegister.LogDeposito logDeposito;

                        logDeposito = new LogRegister.LogDeposito(
                                "" + getEfectivoPreference().getInt("periodo", 0),//Periodo
                                d.getAmount(),//Monto
                                d.getCompany(), //Empresa
                                getEfectivoPreference().getString("no_cobrador", ""),                                                                                    //Cobrador
                                d.getNumberBank(),//Banco
                                d.getReference(),//Referencia
                                true//Ya fue enviado al server
                        );
                        logDepositos.add(logDeposito);
                    }
                    Util.Log.ih("depositos json = " + json.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

			NetService service = new NetService(ConstantsPabs.urlRegisterDepositosOffline, json);
			NetHelper.getInstance().ServiceExecuter(service, this, new NetHelper.onWorkCompleteListener() {

						List<LogRegister.LogDeposito> logDepositos;

						@Override
						public void onCompletion(String result) {
							try {
								manageRegisterDepositosOffline(new JSONObject(result), logDepositos);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}

						@Override
						public void onError(Exception e) {
							// TODO Auto-generated method stub

						}

						public NetHelper.onWorkCompleteListener setData(List<LogRegister.LogDeposito> logDepositos) {
							this.logDepositos = logDepositos;
							return this;
						}
					}.setData(logDepositos)
			);
        }
    }

	public void registerPagos() {
		JSONObject json = new JSONObject();

		try {
			String noCobrador = getEfectivoPreference().getString("no_cobrador", "");

				offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();

			json.put("no_cobrador", noCobrador);
			json.put("pagos", new JSONArray(new Gson().toJson(offlinePayments)));
			json.put("periodo", getEfectivoPreference().getInt("periodo", 0));
			try {
				TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				String deviceid = null;
				if (Nammu.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
						deviceid = manager.getImei();
					else
						deviceid = manager.getDeviceId();
				}
				json.put("imei", deviceid);
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		NetHelper.getInstance().ServiceExecuter(
				new NetService(ConstantsPabs.urlregisterPagosOffline, json),
				this, new NetHelper.onWorkCompleteListener() {

					@Override
					public void onCompletion(String result) {
						try {
							manage_RegisterPagosOffiline(new JSONObject(result));
						} catch (JSONException e) {
							Toast.makeText(
									ApplicationResourcesProvider.getContext(),
									"Problema con la conexión, intenta de nuevo más tarde.",
									Toast.LENGTH_SHORT).show();
							e.printStackTrace();
						}
					}

					@Override
					public void onError(Exception e) {
						Log.e(null, "RegisterPagos");
						//mostrarMensajeError(ConstantsPabs.listaPagosCobrador);
					}
				});
	}

	public void manage_RegisterPagosOffiline(JSONObject json) {
		Log.e("result", json.toString());

		if (json.has("result")) {
			try {
				JSONArray arrayPagosFallidos = json.getJSONArray("result");
				if (arrayPagosFallidos.length() > 0) {
					AlertDialog.Builder alert = new AlertDialog.Builder(this);
					alert.setMessage("No se pudieron sincronizar los cobros hechos en modo offline.");
					alert.setPositiveButton("Reintentar", (dialog, which) -> registerPagos());
					alert.setNegativeButton("Regresar", (dialog, which) -> {
						getFragmentManager().popBackStack();
					});
					alert.create().show();
				} else {
					sincronizarPagosRegistrados(json);

					if (isConnected) {
						enviarDepositos();
					}
					else {
						try {
							AlertDialog.Builder alertInternet = new AlertDialog.Builder(this);
							alertInternet.setMessage(getResources().getString(
									R.string.error_message_network));
							alertInternet.setPositiveButton("Salir", (dialog, which) -> {
								//finish();
								getFragmentManager().popBackStack();
							});
							alertInternet.create().show();
						} catch (Exception e){
							e.printStackTrace();
							Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_network, Toast.LENGTH_LONG);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
						}
					}

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			Util.Log.eh("ERROR: manage_RegisterPagosOffiline");

		}
	}

	public void sincronizarPagosRegistrados(JSONObject json) {
		try {

			//for (ModelPayment p: offlinePayments){
			DatabaseAssistant.updatePaymentsAsSynced(json);
			//}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


    private void syncDepositsSavedToServer(JSONArray almacenados) {
        try {
            //SqlQueryAssistant assistant = new SqlQueryAssistant(this);
            for (int i = 0; i < almacenados.length(); i++) {
                JSONObject deposito = almacenados.getJSONObject(i);
                //assistant.updateDepositsSync(deposito.getString("fecha"));
                DatabaseAssistant.updateDepositsAsSynced(deposito.getString("fecha"));
            }
            //assistant = null;
            //arrayCobrosOffline = new JSONArray();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	@Override
	public void onMultiWindowModeChanged(boolean isInMultiWindowMode) {
		super.onMultiWindowModeChanged(isInMultiWindowMode);

		if (isInMultiWindowMode)
			Toast.makeText(this, "Es posible que la aplicación no funcione correctamente en el modo de pantalla dividida", Toast.LENGTH_LONG).show();
	}

	public static String getCompanyNameFormatted( String companyname ) {
		/*switch ( BuildConfig.getTargetBranch() ) {
			case IRAPUATO:
				if ( companyname.equalsIgnoreCase("COOPERATIVA") ) {
					return "NARANJA";
				} else if ( companyname.equalsIgnoreCase("PROGRAMA") ) {
					return "AZUL";
				}
				break;
			default:
				return companyname.toUpperCase();
		}*/
		return companyname;
	}

	public static String getCompanyNameFormatted( String companyname, boolean toUpperCase ) {
		/*switch ( BuildConfig.getTargetBranch() ) {
			case IRAPUATO:
				if ( companyname.equalsIgnoreCase("COOPERATIVA") ) {
					return toUpperCase ? "NARANJA" : "Naranja";
				} else if ( companyname.equalsIgnoreCase("PROGRAMA") ) {
					return toUpperCase ? "AZUL" : "Azul";
				}
				break;
			default:
				return toUpperCase ? companyname.toUpperCase() : companyname;
		}*/
		return toUpperCase ? companyname.toUpperCase() : companyname;
	}

	/*
	static TSCPrinter tscActivity;
	static long dateAux = new Date().getTime();
	static Timer timerToCancelPrinting;

	public static TSCPrinter getOpenPort() throws Exception{
		if (tscActivity == null) {

			Util.Log.ih("NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL" +
					"NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL" +
					"NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL");

			if (Looper.myLooper() == null){
				Looper.prepare();
			}

			tscActivity = new TSCPrinter();
		}
		else {
			Util.Log.ih("NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL" +
					"NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL," +
					"NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL,NOT NULL");

			return tscActivity;
		}

		timerToCancelPrinting = new Timer();
		timerToCancelPrinting.schedule(new TimerTask() {

			int attempt;
			TSCPrinter tscDll;

			@Override
			public void run() {
				if (checkTime()) {
					Util.Log.ih("Closing port");
					try {
						Util.Log.ih("CLOSING PORT OPENED -----------");
						tscDll.closeport();
					} catch (Exception e) {
						e.printStackTrace();
						timerToCancelPrinting.cancel();
					}
				}
				Util.Log.ih("Hora, attempt: " + attempt);
			}

			public TimerTask setData(TSCPrinter tscDll) {
				this.tscDll = tscDll;
				return this;
			}
		}.setData(tscActivity), (1000 * 60));

		tscActivity.openport(DatabaseAssistant.getPrinterMacAddress());
		timerToCancelPrinting.cancel();
		Util.Log.ih("PORT OPENED -----------------------------");

		return tscActivity;
	}

	public static void closePort(){
		tscActivity = null;
	}
	*/

	/**
	 * checks if more than 59 minutes have passed
	 * @return
	 *          true -> difference is more than limit
	 *          false -> difference is under limit
	 */
	/*
	public static boolean checkTime() {
		long newTime = new Date().getTime();
		long differences = Math.abs(dateAux - newTime);
		return (differences > (1000 * 59));
	}
	*/
}
