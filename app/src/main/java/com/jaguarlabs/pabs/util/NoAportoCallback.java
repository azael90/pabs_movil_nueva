package com.jaguarlabs.pabs.util;

public interface NoAportoCallback {

	/**
	 * Processes the visit chose.
	 * the most important option here is the first one, which is
	 * 'typeStatus = 2'
	 * because does a couple of validations with bluetooth and
	 * shows up another dialog with info that will be printed
	 * on the ticket.
	 * if option chose is another than first, it will only call
	 * realizarNoAportacion() method and will save it into database
	 *
	 * @param position
	 * 			visit chose
	 */
	public void posicionSeleccionada(int position);
	
}
