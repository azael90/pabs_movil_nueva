package com.jaguarlabs.pabs.util;

import android.content.Intent;

/**
 * Created by Emmanuel Rodriguez on 28/12/2017.
 */

public interface OnFragmentResult {
    void onFragmentResult(int requestCode, int resultCode, Intent data);
}
