package com.jaguarlabs.pabs.util;

import android.widget.Toast;

import com.jaguarlabs.pabs.activities.Clientes;
import com.jaguarlabs.pabs.activities.Login;
import com.jaguarlabs.pabs.components.Preferences;

/**
 * This class holds constants, mostly urls for web services.
 * Can be used whenever needed within the app
 */
@SuppressWarnings("SpellCheckingInspection")
public class ConstantsPabs {

	public final static  String IP = getServerIP();
	public final static  String baseURL = getBaseURL();
	public final static  String baseURLTEST = getBaseURLTest();


	public static final String consultaConfiguracion ="http://35.167.149.196/ecobrotest/consultaConfiguracion.php";
	public static final int iniciarSesion = 0;
	public static final int enviarLocalizacion = 1;
	public static final int listClients = 2;
	public static final int allBanks = 3;

	public static final int START_PRINTING = 1000;
	public static final int FINISHED_PRINTING = 1001;
	public static final int START_REQUEST = 2000;
	public static final int FINISHED_REQUEST = 2001;

	public static boolean finishPrinting = false;

	private static String getServerIP() {
		Preferences preferences = new Preferences(ApplicationResourcesProvider.getContext());
		Util.Log.ih("IP = " + preferences.getServerIP());
		return preferences.getServerIP();
	}

	private static String getBaseURLTest(){

		switch (BuildConfig.getTargetBranch())

		{
			case NAYARIT:case MEXICALI:case MORELIA: case TORREON:
				return "http://54.70.207.245/ecobrotest";
			case QUERETARO:
				return "http://54.203.201.17/ecobrotest";
			case IRAPUATO:
				return "http://54.203.201.17/ecobroirapuato";

			case SALAMANCA:
				return "http://54.203.201.17/ecobrosalamanca";

			case LEON:
				return "http://54.203.201.17/ecobroleon";

			case CELAYA:
				return "http://54.203.201.17/ecobrocelaya";

			case TAMPICO:
				return "http://54.203.201.17/ecobrotampico";
			default:
				return "http://35.167.149.196/ecobrotest";
		}

	}

	private static String getBaseURL()
	{
		String baseURL = "http://" + IP;
		Util.Log.ih("baseURL: " + baseURL);

		switch (BuildConfig.getTargetBranch())
		{
			case GUADALAJARA:
				//Preferences preferences = new Preferences(ApplicationResourcesProvider.getContext());
				//if (preferences.getServerURL().equals("TEST"));
					//return baseURL + "/ecobroSAP_TEST";
				//} else {
					return baseURL + "/ecobro";
				//}
			case TOLUCA:
				return baseURL + "/ecobrotol";
			case PUEBLA:
				return baseURL + "/ecobropue";
			case NAYARIT:
				return baseURL + "/ecobronay";
			case MERIDA:
				return baseURL + "/ecobromid";
			case SALTILLO:
				return baseURL + "/ecobrosalt";
			case CANCUN:
				return baseURL + "/ecobrocun";
			case QUERETARO:
				return baseURL + "/ecobroqueretaro";
			case IRAPUATO:
				return baseURL + "/ecobroirapuato";
			case LEON:
				return baseURL + "/ecobroleon";
			case SALAMANCA:
				return baseURL + "/ecobrosalamanca";
			case CELAYA:
				return baseURL + "/ecobrocelaya";
			case CUERNAVACA:
				return baseURL + "/ecobrocue";
			case MEXICALI:
				return baseURL + "/ecobromxl";
			case TORREON:
				return baseURL + "/ecobrotrc";
			case MORELIA:
				return baseURL + "/ecobromor";
			case TAMPICO:
				return baseURL + "/ecobrotam";
			default:
				//no default behavior
		}
		return baseURL + "/ecobro";//Guadalajara
	}

	public static final String urlCheckUpdate = baseURL + "/controlusuarios/getUpdateStatus/";
	public static final String urlUpdateApp = baseURL + "/application/update/";
	public static final String urlCheckVersion = baseURL + "/application/update/version.txt";
	public static final String URL_SAVE_LOCATIONS_10_MINUTES = baseURL + "/controlubicaciones/saveLocation";
	public static final String URL_SAVE_AIRPLANEMODE = baseURL + "/controlusuarios/addDebtCollectorEvent";




	public static final String urlLoginCobrador = baseURL
			+ "/controlusuarios/loginCobrador";
	public static final String urlSaveUbicacion = baseURL
			+ "/controlubicaciones/saveUbicacion";
	public static final String urlListClient = baseURL + "/controlcartera/getListClient";
	public static final String urlAllBanks = baseURL
			+ "/controlpagos/getAllBanks";
	public static final String urlSaveGeoJSONLocations = baseURL
			+ "/controlubicaciones/saveLocationsToFile";

	public static final String urlAuthToken = baseURL
			+ "/controlusuarios/getAuthToken";

	// main
	public static final int registerPagosOfflineMain = 4;
	public static final String urlregisterPagosOfflineMain = baseURL
			+ "/controlpagos/registerPagos";

	public static final int registrarNotificacionesLeidas = 5;
	public static final String urlRegistrarNotificacionesLeidas = baseURL
			+ "/controlnotificaciones/updateNotification";

	public static final int getNumNotificaciones = 6;
	public static final String urlGetNumNotificaciones = baseURL
			+ "/controlnotificaciones/getNumNotification";

	public static final String getLocalidadesAndColonias = baseURL + "/controllocalidades/getCatalogos";
	// Pantalla bloqueo
	public static final int unlockDeposito = 7;
	public static final int getStatusDeposito = 8;
	public static final int changeStatusDeposito = 9;
	public static final int changeStatusRazon = 10;

	public static final String urlGetStatusDeposito = baseURL
			+ "/controldepositos/getStatus";
	public static final String urlUnlockDepositoViejo = baseURL
			+ "/controldepositos/registerDeposito";
	public static final String urlUnlockDeposito = baseURL
			+ "/controldepositos/registerDepositoAutoProbado";
	public static final String urlDepositoAutomatico = baseURL
			+ "/controldepositos/registerDepositoAutomatico";
	public static final String urlAprobarDepositoAutomatico = baseURL
			+ "/controldepositos/automaticApproveDeposito";
	public static final String urlAutomaticClosePeriod = baseURL
			+ "/controldepositos/automaticClosePeriod";
	public static final String urlGetFechaCierreAutomatico = baseURL
			+ "/cobradores/getFechaCierreAutomatico";
	public static final String urlGetFechaCierreByCobrador = baseURL
			+ "/cobradores/getFechaCierreByCobrador";
	public static final String urlChangeStatusDeposito = baseURL
			+ "/controldepositos/changeStatus";
	public static final String urlChangeStatusRazon = baseURL
			+ "/controldesbloqueos/changeStatus";

	public static final int unlockRazon = 11;
	public static final int getStatusRazon = 12;
	public static final String urlUnlockRazon = baseURL
			+ "/controldesbloqueos/insertUnlock";
	public static final String urlGetStatusRazon = baseURL
			+ "/controldesbloqueos/getStatus";

	// Detalle cliente
	public static final int cobroACliente = 13;
	public static final String urlSaveCobro = baseURL
			+ "/controlpagos/registerPago";
	public static final String urlImpresion = baseURL
			+ "/controlpagos/printPago";
	public static final int registroImpresion = 14;
	// Pantalla notificaciones
	public static final int getNotificaciones = 15;
	public static final String urlNotificactiones = baseURL
			+ "/controlnotificaciones/getListNotification";

	public static final String urlgetMotivosDeCancelacion = baseURL
			+ "/controlusuarios/getCancelationReasons";

	// Pantalla deposito
	public static final int depositoVoluntario = 16;
	public static final int getStatusDepVoluntario = 17;
	public static final int changeStatusVoluntario = 18;
	public static final int allBanksDepositoVol = 19;

	// Pantalla Buscar clientes
	public static final int buscarClientes = 20;
	public static final String urlBuscarClientes = baseURL
			+ "/controlcartera/searchClient";

	// Pago fuera de cartera
	public static final int cobroFueraCartera = 21;
	public static final String urlCobroFueraCartera = baseURL
			+ "/controlpagos/registerPago";
	public static final int registroImpresionFuera = 22;

	// Cierre cartera
	public static final int registerPagosOffiline = 23;
	public static final String urlregisterPagosOffline = baseURL
			+ "/controlpagos/registerPagos";

	public static final int listaPagosCobrador = 24;
	public static final String urlListaPagosCobrador = baseURL
			+ "/controlpagos/cobrosPeriodo";

	public static final int cancelarPago = 25;
	public static final String urlCancelarPago = baseURL
			+ "/controlpagos/cancelPago";

	public static final int cierreCobrador = 26;
	public static final String urlCierreCobrador = baseURL
			+ "/controlefectivo/cierreCobrador";

	// Enviar ubicaciones offline
	public static final int saveUbicacionesOffline = 27;
	public static final String urlSaveUbicacionesOffline = baseURL
			+ "/controlubicaciones/saveUbicacionOffline";

	// Obtener Depositos
	public static final int saveListaDepositos = 28;
	public static final String urlGetListaDepositos = baseURL
			+ "/controldepositos/getTotalByCobrador";
	public static final String urlGetListaDepositosPorPeriodo = baseURL
			+ "/controldepositos/depositosPeriodo";
	// Obtener Saldo Inicial
	public static final int obtenerSaldoInicial = 29;
	// public static final String urlObtenerSaldo =
	// baseURL+"/controlefectivo/search";
	public static final String urlObtenerSaldo = baseURL
			+ "/controlefectivo/searchinner";

	public static final int registerPagosOffilineDepositos = 30;
	public static final int registerPagosOffilineBloqueo = 31;

	public static final int registerPagosOfflineLogin = 32;

	public static final int saveUbicacionesOfflineLogin = 33;

	public static final String urlCheckUnlockToday = baseURL
			+ "/controldesbloqueos/checkUnlockToday";
	public static final int CheckUnlockToday = 34;

	public static final String urlGetSaldoNodepositado = baseURL
			+ "/controlpagos/getSaldoNoDepositado";
	public static final int getSaldoNoDepositado = 35;
	public static final int getSaldoNoDepositadoFromBloqueo = 36;
	public static final int registerDepositosOffline = 37;
	public static final String urlRegisterDepositosOffline = baseURL
			+ "/controldepositos/registerDepositosOffline";
	public static final String urlGetBalanceEmpresaPorCobradorYPeriodo = baseURL
			+ "/controlusuarios/getBalanceEmpresasPorCobradorYPeriodo";

	public static final String urlGetCurrentMonto = baseURL
			+ "/controlefectivo/getCurrentMonto";


	public static final String URL_CARGA_ACTUALIZACIONES= baseURL + "/controlcartera/updateAddressEmailAndCellPhoneNumber";
	//ublic static final String URL_CARGA_ACTUALIZACIONES= "http://35.167.149.196/ecobro/controlcartera/updateAddressEmailAndCellPhoneNumber";


    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }
}
