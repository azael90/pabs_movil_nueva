package com.jaguarlabs.pabs.util;

public interface UserInterationListener {
    void onUserInteraction();
}
