package com.jaguarlabs.pabs.util;

import android.os.Environment;

import org.json.JSONException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class will be in charge of registering or writing the stacktrace sent, to a '.txt' file
 * so that it'll be easier to know what is happening when something goes wrong.
 *
 * @author Jordan Alvarez
 * @version 11/06/15
 */
public class StackTraceHandler {

    private String fecha;
    private String path;
    private String localPath;
    private String fileName;

    /**
     * inits resources
     *
     * @param methodName
     *          this is the name of the method where the exception was caught.
     */
    private void initHanlder(String methodName){
        fecha = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());

        path = Environment.getExternalStorageDirectory() + "/PABS_LOGS/StackTrace/";
        File dir = new File(path);

        if (!dir.exists()){
            dir.mkdirs();
        }

        localPath = Environment.getExternalStorageDirectory() + "/PABS_LOGS/StackTrace/";
        fileName = methodName + "-StackTrace " + fecha + ".txt";
    }

    /**
     * sets variables and writes to '.txt' file
     * when JSONException is thrown
     *
     * @param methodName
     *          this is the name of the method where the exception was caught.
     * @param error
     *          this is the stacktrace of the error that the exception threw
     */
    public void uncaughtException( String methodName, JSONException error){

        initHanlder(methodName);

        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        error.printStackTrace(printWriter);;
        String stackTrace = result.toString();
        printWriter.close();

        writeToFile( stackTrace );
    }

    /**
     * sets variables and writes to '.txt' file
     * when Exception is thrown
     *
     * @param methodName
     *          this is the name of the method where the exception was caught.
     * @param error
     *          this is the stacktrace of the error that the exception threw
     */
    public void uncaughtException( String methodName, Exception error ){

        initHanlder(methodName);

        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        error.printStackTrace(printWriter);;
        String stackTrace = result.toString();
        printWriter.close();

        writeToFile( stackTrace  );

    }

    /**
     * write to '.txt' file exception thrown
     *
     * @param stackTrace
     *              error thrown
     */
    private void writeToFile( String stackTrace ){
        try {

            BufferedWriter bos = new BufferedWriter(new FileWriter(localPath + fileName));
            bos.write( stackTrace );
            bos.flush();
            bos.close();

        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
