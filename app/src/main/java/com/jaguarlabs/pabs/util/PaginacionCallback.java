package com.jaguarlabs.pabs.util;

public interface PaginacionCallback {

	/**
	 * downloads next page of notifications
	 *
	 * @param lastPosition
	 */
	public void descargarPaginaSiguiente(int lastPosition);
}
