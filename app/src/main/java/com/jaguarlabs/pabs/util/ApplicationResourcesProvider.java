package com.jaguarlabs.pabs.util;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.jaguarlabs.pabs.database.AirPlaneRegister;
import com.jaguarlabs.pabs.database.Collectors;
import com.jaguarlabs.pabs.database.LocationNow;
//import com.jaguarlabs.pabs.location.FusedLocationProvider;
import com.jaguarlabs.pabs.location.LocationProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;

public class ApplicationResourcesProvider extends com.orm.SugarApp {
    private static Context sContext;
    private static LocationProvider locationProvider;
    private static long minutosTimerUbicaciones=0;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();

        /*************** AQUI ************/
        locationProvider = new LocationProvider(this);

        comenzar();
    }
    public static Context getContext(){
        return sContext;
    }

    public static LocationProvider getLocationProvider(){
        return locationProvider;
    }

    public static void setLastKnownLocation(double latitude, double longitude){
        locationProvider.setLastKnownLocation(latitude, longitude);
    }

    public static void saveGeoJSONFile(String periodo, String collectorNumber) {
        locationProvider.createGeoJSONFile(periodo, collectorNumber);
    }

    public static void startLocationUpdates()
    {
        locationProvider.startLocationUpdates();
    }

    public static void stopLocationUpdates()
    {
        locationProvider.stopLocationUpdates();
    }

    public static void ubicacionesCada10Minutos()
    {
        JSONObject jsonUbicacionesGeneral = new JSONObject();
        String sintaxis="SELECT * FROM COLLECTORS";
        List<Collectors> listaPromotor = Collectors.findWithQuery(Collectors.class, sintaxis);
        if(listaPromotor.size()>0)
        {
            String codigoPromotor = listaPromotor.get(0).getNumberCollector();
            try
            {
                jsonUbicacionesGeneral.put("no_cobrador", codigoPromotor);

            }catch(JSONException ex)
            {
                ex.printStackTrace();
            }
        }
        else
        {
            Log.d("ERROR PROMOTOR-->", "NO hay registros");
        }


        String query="SELECT * FROM LOCATION_NOW where sync = '1' order by id desc limit 1";
        List<LocationNow> listaUbicaciones = LocationNow.findWithQuery(LocationNow.class, query);
        if(listaUbicaciones.size()>0)
        {
            JSONArray arrayUbicaciones = new JSONArray();
            for (int y = 0; y <= listaUbicaciones.size() - 1; y++)
            {
                JSONObject jsonDatos = new JSONObject();

                try {
                    jsonDatos.put("no_cobrador", listaUbicaciones.get(y).getNo_cobrador());
                    jsonDatos.put("tiempo", listaUbicaciones.get(y).getTiempo());
                    jsonDatos.put("latitud", listaUbicaciones.get(y).getLatitud());
                    jsonDatos.put("longitud", listaUbicaciones.get(y).getLongitud());
                    arrayUbicaciones.put(jsonDatos);
                } catch (JSONException ex) {

                    ex.printStackTrace();
                }
            }

            try {
                jsonUbicacionesGeneral.put("locationsNow", arrayUbicaciones);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            cargaDeDatosAServidor(jsonUbicacionesGeneral);
            Log.i("CARGA DE DATOS -->", "ubicaciones cada "+ minutosTimerUbicaciones / 60000 +" minutos: "+ jsonUbicacionesGeneral.toString());
        }
        else {
            Log.e("No hay datos", "JSON");
        }
    }

    public static void cargaDeDatosAServidor(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.URL_SAVE_LOCATIONS_10_MINUTES, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try {
                            String tiempo="", tiempoFail="", minutos_timer_ubicaciones="";
                            if (!response.has("error")) {
                                JSONArray jsonSuccess = response.getJSONArray("success");
                                JSONArray jsonFail = response.getJSONArray("fail");
                                if(jsonSuccess.length()>0)
                                {
                                    for(int i=0; i<=jsonSuccess.length()-1;i++)
                                    {
                                        JSONObject object = jsonSuccess.getJSONObject(i);
                                        tiempo = object.getString("tiempo");
                                        String result = object.getString("RESULTADO");

                                        String query="UPDATE LOCATION_NOW SET sync ='2' WHERE tiempo='" + tiempo +"'";
                                        LocationNow.executeQuery(query);
                                        Log.d("RESULTADO SUCCESS:--->", result);

                                        //*******Obtner aqui la llave*******
                                        minutos_timer_ubicaciones = object.getString("minutos_timer_ubicaciones");
                                        String queryUpdateMinutosTimer="UPDATE COLLECTORS SET minutos_timer_ubicaciones ='"+ minutos_timer_ubicaciones +"'";
                                        Collectors.executeQuery(queryUpdateMinutosTimer);
                                        Log.i("ACTUALIZADO:--->", " nuevo tiempo de actualización: "+ minutos_timer_ubicaciones + " minutos");
                                    }

                                    String queryDelete="DELETE FROM LOCATION_NOW WHERE sync = '2'";
                                    LocationNow.executeQuery(queryDelete);
                                }

                                if (jsonFail.length()>0)
                                {
                                    for(int y=0; y<=jsonFail.length()-1;y++)
                                    {
                                        JSONObject object = jsonFail.getJSONObject(y);
                                        tiempoFail = object.getString("tiempo");
                                        String result = object.getString("RESULTADO");
                                        Log.d("RESULTADO FAIL--->", result);
                                    }
                                }
                                else
                                {
                                    Log.d("IMPRESION --->", "NO HAY ERROR =D");
                                }

                                Log.i("Success :D -->", new Gson().toJson(response));
                            }
                            else
                            {
                                String error = response.getString("error");
                                Log.d("Error:-->", error);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }

    public void comenzar()
    {

        List<Collectors> lista = Collectors.findWithQuery(Collectors.class, "SELECT * FROM COLLECTORS");
        if (lista.size() > 0)
        {
            if(lista.get(0).getMinutosTimerUbicaciones()==null)
            {
                Util.Log.d("IMPRESION --> : ubicacion null");
            }
            else
            {
                String tiempoSinConvertir = lista.get(0).getMinutosTimerUbicaciones();
                double tiempoConvertido = Double.parseDouble(tiempoSinConvertir) * 60000;
                minutosTimerUbicaciones =  (int) tiempoConvertido;
            }
        }
        else
            Util.Log.d("IMPRESION --> : NO SE OBTUVIERON REGISTROS");


        Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            public void run()
            {
                ubicacionesCada10Minutos();
                AirPlaneEventRecollector();
                comenzar();
            }
        //}, 5000);
        }, minutosTimerUbicaciones);
    }


    public static void AirPlaneEventRecollector()
    {
        JSONObject jsosAirPlane = new JSONObject();
        String sintaxis="SELECT * FROM COLLECTORS";
        List<Collectors> listaPromotor = Collectors.findWithQuery(Collectors.class, sintaxis);
        if(listaPromotor.size()>0)
        {
            String codigoPromotor = listaPromotor.get(0).getNumberCollector();
            try
            {
                jsosAirPlane.put("no_cobrador", codigoPromotor);

            }catch(JSONException ex)
            {
                ex.printStackTrace();
            }
        }
        else
        {
            Log.d("ERROR PROMOTOR-->", "NO hay registros");
        }


        String query="SELECT * FROM AIR_PLANE_REGISTER where sync = '0'";
        List<AirPlaneRegister> lista = AirPlaneRegister.findWithQuery(AirPlaneRegister.class, query);
        if(lista.size()>0)
        {
            JSONArray arrayAirPlaneActivated = new JSONArray();
            for (int y = 0; y <= lista.size() - 1; y++)
            {
                JSONObject jsonDatos = new JSONObject();

                try {
                    //SWjsonDatos.put("no_cobrador", lista.get(y).getNo_cobrador());
                    jsonDatos.put("fecha", lista.get(y).getFecha());
                    jsonDatos.put("isAirPlaneActive", lista.get(y).getActivado());
                    arrayAirPlaneActivated.put(jsonDatos);
                } catch (JSONException ex) {

                    ex.printStackTrace();
                }
            }

            try {
                jsosAirPlane.put("airPlaneRecords", arrayAirPlaneActivated);
                Log.v("JSONAIRPLANE", jsosAirPlane.toString());
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            loadRecordsAirPlaneMode(jsosAirPlane);
        }
        else {
            Log.e("No hay datos", "JSON");
        }
    }

    public static void loadRecordsAirPlaneMode(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.URL_SAVE_AIRPLANEMODE, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    String fecha="";
                    if (!response.has("error")) {
                        JSONArray jsonSuccess = response.getJSONArray("airPlaneRecords");
                        if(jsonSuccess.length()>0)
                        {
                            for(int i=0; i<=jsonSuccess.length()-1;i++)
                            {
                                JSONObject object = jsonSuccess.getJSONObject(i);
                                fecha = object.getString("fecha");

                                String query="UPDATE AIR_PLANE_REGISTER SET sync ='1' WHERE fecha='" + fecha +"'";
                                AirPlaneRegister.executeQuery(query);
                            }

                            String queryDelete="DELETE FROM AIR_PLANE_REGISTER WHERE sync = '1'";
                            AirPlaneRegister.executeQuery(queryDelete);
                        }

                        Log.i("Success :D -->", new Gson().toJson(response));
                    }
                    else
                    {
                        String error = response.getString("error");
                        Log.d("Error:-->", error);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        VolleySingleton.getIntanciaVolley(sContext).addToRequestQueue(postRequest);
    }




}
