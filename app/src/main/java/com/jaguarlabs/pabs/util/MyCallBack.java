package com.jaguarlabs.pabs.util;

public interface MyCallBack {

	/**
	 * shows a dialog confirming that selected
	 * ticket would be canceled
	 *
	 * @param position
	 * 			position of selected ticket to
	 * 			be canceled
	 */
	public void onClickCancel(int position);
}
