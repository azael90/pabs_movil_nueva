package com.jaguarlabs.pabs.util;

/**
 * This class shares constants used whithin application
 */
public class SharedConstants{

	/**
	 * companies' constants
	 */
	public static class Empresa{
		public static final String id_empresa = "id";
		public static final String nombre_empresa = "nombre";
		public static final String razon_social = "razon_social";
		public static final String rfc = "rfc";
		public static final String direccion_empresa = "direccion";
		public static final String telefono_empresa = "telefono";
	}

	/**
	 * companies' constants
	 */
	public static class Empresa_Cobrador{
		public static final String id_empresa = "id";
		public static final String ult_folio = "folio";
		public static final String serie_empresa = "serie";
	}

	/**
	 * Constants to be used when calling
	 * {@link com.example.tscdll.TSCActivity#setup(int, int, int, int, int, int, int)}
	 */
	public static class Printer{
		public static final int width = 70;
		public static final int height = 130;
		public static final int speed = 2;
		public static final int density = 15;
		public static final int sensorType = 0;
		public static final int gapBlackMarkVerticalDistance = 0;
		public static  final int gapBlackMarkShiftDistance = 0;
	}
}