package com.jaguarlabs.pabs.util;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

/**
 * This class provides classes with methods to simply make
 * some code more clear.
 *
 * @version 27/05/15
 * @author Jordan Alvarez
 */
public class Util {

    /**
     * provides debug output with or without format
     */
    public static class Log{

        public static final String APP_NAME = "PABS";
        private static final String TAG_CHARACTERS = "***************************";

        /**
         * formatted info output
         *
         * @param message
         *            message to be printed in console
         */
        public static void ih(String message){
            if (BuildConfig.DEBUG) {
                i(TAG_CHARACTERS);
                i(message);
                i(TAG_CHARACTERS);
            }
        }

        /**
         * no formatted info output
         *
         * @param message
         *            message to be printed in console
         */
        public static void i(String message){
            if (BuildConfig.DEBUG) {
                if (message != null)
                    android.util.Log.i(APP_NAME, message);
            }
        }

        /**
         * formatted error output
         *
         * @param message
         *            message to be printed in console
         */
        public static void eh(String message){
            if (BuildConfig.DEBUG) {
                e(TAG_CHARACTERS);
                e(message);
                e(TAG_CHARACTERS);
            }
        }

        /**
         * no formatted error output
         *
         * @param message
         *            message to be printed in console
         */
        public static void e(String message){
            if (BuildConfig.DEBUG) {
                android.util.Log.e(APP_NAME, message);
            }
        }

        /**
         * formatted warning output
         *
         * @param message
         *            message to be printed in console
         */
        public static void wh(String message){
            if (BuildConfig.DEBUG) {
                w(TAG_CHARACTERS);
                w(message);
                w(TAG_CHARACTERS);
            }
        }

        /**
         * no formatted warning output
         *
         * @param message
         *            message to be printed in console
         */
        public static void w(String message){
            if (BuildConfig.DEBUG) {
                android.util.Log.e(APP_NAME, message);
            }
        }

        /**
         * formatted debug output
         *
         * @param message
         *            message to be printed in console
         */
        public static void dh(String message){
            if (BuildConfig.DEBUG) {
                d(TAG_CHARACTERS);
                d(message);
                d(TAG_CHARACTERS);
            }
        }

        /**
         * no formatted error output
         *
         * @param message
         *            message to be printed in console
         */
        public static void d(String message){
            if (BuildConfig.DEBUG) {
                android.util.Log.d(APP_NAME, message);
            }
        }

        /**
         * formatted verbose output
         *
         * @param message
         *            message to be printed in console
         */
        public static void vh(String message){
            if (BuildConfig.DEBUG) {
                d(TAG_CHARACTERS);
                d(message);
                d(TAG_CHARACTERS);
            }
        }

        /**
         * no formatted verbose output
         *
         * @param message
         *            message to be printed in console
         */
        public static void v(String message){
            if (BuildConfig.DEBUG) {
                android.util.Log.v(APP_NAME, message);
            }
        }

    }

    /**
     * methods in here will be seen by user
     */
    public static class UI{

        private static Toast toast;

        /**
         * shows a toast message for a short period of time
         *
         * @param context
         *              the context to use
         * @param message
         *              message to be shown
         */
        public static void toastS(Context context, String message){
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }

        /**
         * shows a toast message for a short period of time
         * reusing a toast that is currently being shown
         *
         * @param context
         *              the context to use
         * @param message
         *              message to be shown
         */
        public static void toastShortReuse(Context context, String message){
            if (toast == null || toast.getView().getWindowVisibility() != View.VISIBLE){
                toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
                toast.show();
            }
            else {
                toast.setText(message);
                toast.show();
            }
        }

        /**
         * shows a toast message for a long period of time
         * reusing a toast that is currently being shown
         *
         * @param context
         *              the context to use
         * @param message
         *              message to be shown
         */
        public static void toastLongReuse(Context context, String message){
            if (toast == null || toast.getView().getWindowVisibility() != View.VISIBLE){
                toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
                toast.show();
            }
            else {
                toast.setText(message);
                toast.show();
            }
        }

        /**
         * shows a toast message for a long period of time
         *
         * @param context
         *              the context to use
         * @param message
         *              message to be shown
         */
        public static void toastL(Context context, String message){
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            /*
            Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            */
        }
    }

}
