package com.jaguarlabs.pabs.util;

import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Jordan Alvarez on 13/07/15.
 * Use this class for debugging purposes. This is a simple sollution that allows you to write
 * the stacktrace into sd card of Uncaught exceptions, them exceptions related with system crash.
 *
 * An instance of using this could be following code declared in a context (e.g. the main Activity)
 *
 * if ( !(Thread.getDefaultUncaughtExceptionHandler() instanceof CustomExceptionHandler) ){
 * Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler());
 * }
 *
 *
 * @version 13/07/2015
 * @author Jordan Alvarez
 */
public class CustomExceptionHandler implements Thread.UncaughtExceptionHandler {

    private Thread.UncaughtExceptionHandler defaultUEH;

    private String localPath;

    private String fecha;

    /**
     * CustomExceptionHandler contructor that initializes variables
     */
    public CustomExceptionHandler(){

        fecha = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
        localPath = Environment.getExternalStorageDirectory() + "/PABS_LOGS/UnCaughtExceptionStackTrace/";
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {

        //instantiates variables
        Calendar cal = Calendar.getInstance();
        //Calendar.MONTH begins from 0
        String month = "" + (Integer.parseInt(""+cal.get(Calendar.MONTH)) + 1);

        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        ex.printStackTrace(printWriter);
        String stacktrace = result.toString();
        printWriter.close();
        String filename = "UnCaughtException "
                + fecha
                + ".txt";

        //check if path exists already
        File dir = new File(localPath);
        if (!dir.exists()){
            dir.mkdirs();
        }


        if (localPath != null) {
            writeToFile(stacktrace, filename);
        }

        // re-throw critical exception further to the os (important)
        defaultUEH.uncaughtException(thread, ex);
    }

    /**
     * write to '.txt' file
     *
     * @param stackTrace
     *          stactrace of error thrown
     * @param fileName
     *          file name to save file with
     */
    private void writeToFile( String stackTrace, String fileName){
        try {

            BufferedWriter bos = new BufferedWriter(new FileWriter(localPath + fileName));
            bos.write( stackTrace );
            bos.flush();
            bos.close();

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
