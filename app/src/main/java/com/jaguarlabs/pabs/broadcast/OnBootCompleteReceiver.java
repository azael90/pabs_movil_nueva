package com.jaguarlabs.pabs.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.jaguarlabs.pabs.activities.Splash;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.Periods;
import com.jaguarlabs.pabs.models.ModelPeriod;
import com.jaguarlabs.pabs.net.VolleySingleton;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.ConstantsPabs;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Emmanuel Rodriguez on 23/11/2017.
 */

public class OnBootCompleteReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = OnBootCompleteReceiver.class.toString();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getAction() != null) {
        }
    }
}
