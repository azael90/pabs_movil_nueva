package com.jaguarlabs.pabs.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.Util;

@SuppressWarnings("SpellCheckingInspection")
public class ReceiveBroadcastTimeChanged extends BroadcastReceiver {

    private static final double deviceDateTimeDifference = 3.6e+7;//6 horas

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i("DATE RECEIVER", "OK");

        Toast toast = Toast.makeText(context, "Por favor revisa la fecha del dispositivo asdasdasdasdas.", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        //Util.Log.ih("date have changeddate have changeddate have changeddate have changeddate have changeddate have changed");

        /*
        if (checkDate())
        {

            Util.Log.ih("Restarting Application!");

            Toast toasts = Toast.makeText(context, "Por favor revisa la fecha del dispositivo.", Toast.LENGTH_SHORT);
            toasts.setGravity(Gravity.CENTER, 0, 0);
            toasts.show();

            //android.os.Process.killProcess(android.os.Process.myPid());
        }
        */
    }

    /**
     * Checks if date-time changes
     *
     * @return true: the difference is more than 21 minutes
     *        false: otherwise
     */
    private boolean checkDate() {
        double currentTimeMillis = System.currentTimeMillis();
        double differences = Math.abs(ExtendedActivity.userInteractionTime - currentTimeMillis);
        return (differences > deviceDateTimeDifference);
    }
}
