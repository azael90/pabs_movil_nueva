package com.jaguarlabs.pabs.broadcast;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.jaguarlabs.pabs.activities.ClienteDetalle;
import com.jaguarlabs.pabs.activities.MainActivity;
import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinterHelper;

/**
 * Created by Emmanuel Rodriguez on 18/01/2018.
 */

public class BluetoothConnectionReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

        if (action != null) {

            switch (action)
            {
                case BluetoothDevice.ACTION_ACL_CONNECTED:
                    Toast.makeText(context, "Impresora conectada", Toast.LENGTH_SHORT).show();
                    Log.d("BluetoothReceiver", "CONNECTED: " + device.getAddress());
                    BluetoothPrinter.printerConnected = true;
                    if (MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES) && ClienteDetalle.printerConnecting.getVisibility() == View.VISIBLE)
                        ClienteDetalle.printerConnecting.setVisibility(View.GONE);
                    break;
                case BluetoothDevice.ACTION_ACL_DISCONNECTED:
                    BluetoothPrinter.printerConnected = false;
                    Toast.makeText(context, "Impresora desconectada", Toast.LENGTH_SHORT).show();
                    Log.d("BluetoothReceiver", "DISCONNECTED: " + device.getAddress());
                    break;
                case BluetoothDevice.ACTION_BOND_STATE_CHANGED:
                    final int extra = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);

                    switch (extra)
                    {
                        case BluetoothDevice.BOND_BONDED:
                            Log.d("BluetoothReceiver", "BONDEND");
                            BluetoothAdapter.getDefaultAdapter().disable();
                            new Handler().postDelayed(() -> BluetoothAdapter.getDefaultAdapter().enable(), 1500);
                            break;
                        case BluetoothDevice.BOND_NONE:
                            Log.d("BluetoothReceiver", "NONE");
                            break;
                        case BluetoothDevice.BOND_BONDING:
                            Log.d("BluetoothReceiver", "BONDING");
                            break;
                    }
                    break;
                case BluetoothDevice.ACTION_PAIRING_REQUEST:
                    Log.d("BluetoothReceiver", "PAIRING REQUEST");
                    break;
                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                    switch (state)
                    {
                        case BluetoothAdapter.STATE_OFF:
                            Log.d("BluetoothReceiver", "OFF");
                            if (BluetoothPrinter.printerConnected)
                            {
                                BluetoothPrinter.getInstance().disconnect();
                            }
                            BluetoothPrinter.printerConnected = false;
                            break;
                        case BluetoothAdapter.STATE_TURNING_OFF:
                            Log.d("BluetoothReceiver", "TURNING OFF");
                            break;
                        case BluetoothAdapter.STATE_ON:
                            Log.d("BluetoothReceiver", "ON");
                            break;
                        case BluetoothAdapter.STATE_TURNING_ON:
                            Log.d("BluetoothReceiver", "TURNING ON");
                            break;
                    }
                    break;
            }
        }
    }
}
