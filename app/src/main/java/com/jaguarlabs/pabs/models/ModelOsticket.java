package com.jaguarlabs.pabs.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jordan Alvarez on 4/21/2017.
 */

@SuppressWarnings("SpellCheckingInspection")
public class ModelOsticket {

    @SerializedName("topic")
    String topic;
    @SerializedName("mensaje")
    String message;
    @SerializedName("dateMilliseconds")
    long dateMilliseconds;

    public ModelOsticket(){}

    public ModelOsticket( String topic, String message, long dateMilliseconds ){
        this.topic = topic;
        this.message = message;
        this.dateMilliseconds = dateMilliseconds;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDateMilliseconds() {
        return dateMilliseconds;
    }

    public void setDateMilliseconds(long dateMilliseconds) {
        this.dateMilliseconds = dateMilliseconds;
    }
}
