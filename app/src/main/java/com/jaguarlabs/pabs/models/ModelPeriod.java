package com.jaguarlabs.pabs.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jordan Alvarez on 3/9/2017.
 */

public class ModelPeriod {

    @SerializedName("no_periodo")
    int periodNumber;

    public ModelPeriod(){}

    public ModelPeriod( int periodNumber ){
        this.periodNumber = periodNumber;
    }

    public int getPeriodNumber() {
        return periodNumber;
    }

    public void setPeriodNumber(int periodNumber) {
        this.periodNumber = periodNumber;
    }
}
