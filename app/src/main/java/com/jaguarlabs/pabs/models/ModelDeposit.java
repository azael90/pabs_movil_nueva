package com.jaguarlabs.pabs.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jordan on 03/03/16.
 */
public class ModelDeposit {

    @SerializedName("monto")
    String amount;
    @SerializedName("referencia")
    String reference;
    @SerializedName("no_banco")
    String numberBank;
    @SerializedName("empresa")
    String company;
    @SerializedName("fecha")
    String date;
    @SerializedName("periodo")
    int periodo;
    @SerializedName("fechaDeposito")
    String depositDate;

    public ModelDeposit(){}

    public ModelDeposit(String amount, String reference, String numberBank, String company, String date, String depositDate, int periodo){
        this.amount = amount;
        this.reference = reference;
        this.numberBank = numberBank;
        this.company = company;
        this.date = date;
        this.depositDate = depositDate;
        this.periodo = periodo;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getNumberBank() {
        return numberBank;
    }

    public void setNumberBank(String numberBank) {
        this.numberBank = numberBank;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public String getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(String depositDate) {
        this.depositDate = depositDate;
    }
}
