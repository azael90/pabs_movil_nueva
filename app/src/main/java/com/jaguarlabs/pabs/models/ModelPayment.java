package com.jaguarlabs.pabs.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jordan on 03/03/16.
 */
public class ModelPayment {

    @SerializedName("empresa")
    String company;
    @SerializedName("latitud")
    String latitude;
    @SerializedName("longitud")
    String longitude;
    @SerializedName("status")
    String status;
    @SerializedName("no_cobrador")
    String idCollector;
    @SerializedName("monto")
    String amount;
    @SerializedName("no_cliente")
    String idClient;
    @SerializedName("fecha")
    String date;
    @SerializedName("serie")
    String serie;
    @SerializedName("folio")
    String folio;
    @SerializedName("copias")
    String copies;
    @SerializedName("tipo_bd")
    String typeBD;
    @SerializedName("periodo")
    int periodo;
    @SerializedName("referencia")
    String paymentReference;
    @SerializedName("tipo_cobro")
    String tipo_cobro;

    public ModelPayment(){}

    public ModelPayment(String company, String latitude, String longitude,
                        String status, String idCollector, String amount,
                        String idClient, String date, String serie, String folio, String copies, String typeBD, int periodo){
        this.company = company;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
        this.idCollector = idCollector;
        this.amount = amount;
        this.idClient = idClient;
        this.date = date;
        this.serie = serie;
        this.folio = folio;
        this.copies = copies;
        this.typeBD = typeBD;
        this.periodo = periodo;
    }

    public ModelPayment(String company, String latitude, String longitude, String status, String idCollector,
                        String amount, String idClient, String date, String serie, String folio,
                        String copies, String typeBD, int periodo, String paymentReference, String tipo_cobro){
        this.company = company;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
        this.idCollector = idCollector;
        this.amount = amount;
        this.idClient = idClient;
        this.date = date;
        this.serie = serie;
        this.folio = folio;
        this.copies = copies;
        this.typeBD = typeBD;
        this.periodo = periodo;
        this.paymentReference = paymentReference;
        this.tipo_cobro = tipo_cobro;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdCollector() {
        return idCollector;
    }

    public void setIdCollector(String idCollector) {
        this.idCollector = idCollector;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCopies() {
        return copies;
    }

    public void setCopies(String copies) {
        this.copies = copies;
    }

    public String getTypeBD() {
        return typeBD;
    }

    public void setTypeBD(String typeBD) {
        this.typeBD = typeBD;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public String getTipo_cobro() {
        return tipo_cobro;
    }

    public void setTipo_cobro(String tipo_cobro) {
        this.tipo_cobro = tipo_cobro;
    }
}
