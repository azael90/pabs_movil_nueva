package com.jaguarlabs.pabs.models;

public class ModelCobrosRealizadosPorPeriodo
{
    String no_contrato="", fecha ="", monto="";


    public ModelCobrosRealizadosPorPeriodo(String no_contrato, String fecha, String monto) {
        this.no_contrato = no_contrato;
        this.fecha = fecha;
        this.monto = monto;
    }

    public String getNo_contrato() {
        return no_contrato;
    }

    public void setNo_contrato(String no_contrato) {
        this.no_contrato = no_contrato;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }
}

