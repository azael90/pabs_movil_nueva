package com.jaguarlabs.pabs.models;

import com.google.gson.annotations.SerializedName;
import com.jaguarlabs.pabs.util.Util;

import java.util.ArrayList;

/**
 * Created by jordan on 20/09/16.
 */
public class ModelLocations {

    @SerializedName("geoJson")
    private ArrayList<GeoLocations> geoLocations;

    public ArrayList<GeoLocations> getGeoLocations() {
        return geoLocations;
    }

    public void setGeoLocations(ArrayList<GeoLocations> geoLocations) {
        this.geoLocations = geoLocations;
    }

    public static class GeoLocations {

        @SerializedName("latitud")
        private Double latitude;
        @SerializedName("longitud")
        private Double longitude;
        @SerializedName("fecha")
        private String date;

        public GeoLocations() {}

        public Double getLatitude() {
            return latitude;
        }

        public GeoLocations setLatitude(Double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Double getLongitude() {
            return longitude;
        }

        public GeoLocations setLongitude(Double longitude) {
            this.longitude = longitude;
            return this;
        }

        public String getDate() {
            return date;
        }

        public GeoLocations setDate(String date) {
            this.date = date;
            return this;
        }
    }

}
