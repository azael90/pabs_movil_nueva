package com.jaguarlabs.pabs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * deprecated in 3.0
 */
public class ClientesFueraCarteraAdapter extends BaseAdapter {

	private Context context;
	private JSONArray arrayClientes;

	public ClientesFueraCarteraAdapter(Context context, JSONArray arrayClientes) {
		this.context = context;
		this.arrayClientes = arrayClientes;
	}
	
	@Override
	public int getCount() {
		return arrayClientes.length();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		CheckedTextView view = (CheckedTextView)convertView;
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			view = (CheckedTextView)inflater.inflate(android.R.layout.simple_list_item_single_choice, null);
			view.setTextColor(context.getResources().getColor(android.R.color.black));
		}
		
		try {
			JSONObject jsonTmp = arrayClientes.getJSONObject(position);
			if (jsonTmp.has("name")) {
				view.setText(jsonTmp.getString("name"));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
				
		view.setText("posicion"+position);
		return view;
	}

}
