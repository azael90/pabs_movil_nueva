package com.jaguarlabs.pabs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.rest.models.ModelSearchForClientResponse;
import com.jaguarlabs.pabs.util.ExtendedActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Show all clients found. 'Aportacion' screen.
 *
 * This adapter is used to populate and provide access to the
 * data items with the {@link android.widget.ListView}
 * used to show all clients found.
 *
 * To see this {@link android.widget.ListView} you have to look
 * for a client when trying to do a payment 'fuera de cartera' and
 * all results found are shown with this adapter.
 *
 * For further information about overridden methods
 * @see {@link android.widget.Adapter}
 */
public class BuscarClientesAdapter extends BaseAdapter {

	//private Context context;

	//clients' found information
	private JSONArray array;

	private Context context;
	private List<ModelSearchForClientResponse.ContractsFound> contractsFound;

	private LayoutInflater inflater;
	
	public BuscarClientesAdapter(Context context, JSONArray array) {
		this.context = context;
		this.array = array;
	}

	public BuscarClientesAdapter(Context context, List<ModelSearchForClientResponse.ContractsFound> contractsFound){
		this.context = context;
		this.contractsFound = contractsFound;

		this.inflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		//return array.length();
		return contractsFound.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {

		//make a view for each client found
		View view = convertView;
		ViewHolder viewHolder;
		if (view == null){
			view = inflater.inflate(R.layout.row_buscar_cliente, null);

			viewHolder = new ViewHolder();

			viewHolder.client = (TextView) view.findViewById(R.id.tv_cliente_row);
			viewHolder.contractNumber = (TextView) view.findViewById(R.id.tv_no_contrato_row);
			viewHolder.address = (TextView) view.findViewById(R.id.tv_domicilio_row);
			viewHolder.status = (TextView) view.findViewById(R.id.tv_status);
			viewHolder.company = (TextView) view.findViewById(R.id.tv_empresa_row);
			viewHolder.tvFechaReactiva = (TextView) view.findViewById(R.id.tvFechaReactiva);


			view.setTag(viewHolder);
		}
		else {
			viewHolder = (ViewHolder) view.getTag();
		}

		ModelSearchForClientResponse.ContractsFound clientFound = contractsFound.get(position);

		//set client name
		viewHolder.client.setText(
				clientFound.getName() + " " +
				clientFound.getFirstLastName() + " " +
				clientFound.getSecondLastName());

		//set contract number
		viewHolder.contractNumber.setText(clientFound.getSerie()+clientFound.getContractNumber());

		viewHolder.address.setText(
				clientFound.getStreetToPay() + " " +
				clientFound.getNumberExt() + " " +
				clientFound.getNeighborhood() + " " +
				clientFound.getLocality() + " " +
				clientFound.getBetweenStreets());

		//set status type
		String contractStatus = clientFound.getContractStatus();
		if ( contractStatus!=null && !contractStatus.equals("") ) {
			viewHolder.status.setText( contractStatus );
		} else {
			String statusType = clientFound.getStatus();
			String estatus = "";
			switch (statusType) {
				case "1":
					estatus = "Activo";
					break;
				//case "2":
				//	estatus = "Suspendido temporal";
				//	break;
				case "5":
					estatus = "Suspendido temporal";
					break;
				case "6":
					estatus = "Suspendido por cancelar";
					break;
				case "15":
					estatus = "Verificacion Temp";
					break;
				case "16":
					estatus = "Verificacion SC";
					break;
				default:
					//nothing here...
			}

			viewHolder.status.setText(estatus);
		}

		//set compane name
		String companyID = clientFound.getIdBaseGroup();
		String companyName = ExtendedActivity.getCompanyNameFormatted( DatabaseAssistant.getSingleCompany(companyID).getName() );

		viewHolder.company.setText(companyName);
		viewHolder.tvFechaReactiva.setText(clientFound.getFecha_reactiva());

		/*
		if (v == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			v = inflater.inflate(R.layout.row_buscar_cliente, null);
		}

		//populate each view's textviews with info of client found
		try {
			JSONObject json = array.getJSONObject(position);

			//set client name
			TextView tvCliente = (TextView)v.findViewById(R.id.tv_cliente_row);
			tvCliente.setText(json.getString("nombre") +" "+ 
					json.getString("apellido_pat") +" "+ 
					json.getString("apellido_mat"));

			//set contract
			TextView tvContrato = (TextView)v.findViewById(R.id.tv_no_contrato_row);
			tvContrato.setText(json.getString("serie")+json.getString("no_contrato"));

			//set address
			TextView tvDomicilio = (TextView)v.findViewById(R.id.tv_domicilio_row);
			tvDomicilio.setText(json.getString("calle_cobro") +" "+
					json.getString("no_ext_cobro") +" "+
					json.getString("colonia") +" "+
					json.getString("localidad") +" "+
					json.getString("entre_calles"));

			//set status
			int tipoEstatus = json.getInt("estatus");
			String estatus = "";
			if (tipoEstatus == 1) {
				estatus = "Activo";
			} else if (tipoEstatus == 5) {
				estatus = "Suspendido temporal";
			} else if (tipoEstatus == 6) {
				estatus = "Suspendido por cancelar";
			}else if ( tipoEstatus == 2){
				estatus = "Cancelado";
			}
			TextView tvStatus = (TextView)v.findViewById(R.id.tv_status);
			tvStatus.setText(estatus);

			//set company name
			TextView tvEmpresa = (TextView)v.findViewById(R.id.tv_empresa_row);
			String tipoEmpresa = json.getString("id_grupo_base");
			
			//SqlQueryAssistant query = new SqlQueryAssistant(context);
			//String nombreEmpresa = query.mostrarEmpresa(tipoEmpresa).getString(Empresa.nombre_empresa);
			String nombreEmpresa = DatabaseAssistant.getSingleCompany(tipoEmpresa).getName();
			
			tvEmpresa.setText(nombreEmpresa);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		*/
		
		return view;
	}

	public class ViewHolder {
		private TextView client;
		private TextView contractNumber;
		private TextView address;
		private TextView status;
		private TextView company;
		private TextView tvFechaReactiva;
	}

}
