package com.jaguarlabs.pabs.adapter;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.database.Notifications;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emmanuel Rodriguez on 23/07/2018.
 */
public class NotificationsRecyclerAdapter extends PagedListAdapter<Notifications, NotificationsRecyclerAdapter.NotificationsViewHolder>{

    private static final DiffUtil.ItemCallback<Notifications> itemCallback = new DiffUtil.ItemCallback<Notifications>() {
        @Override
        public boolean areItemsTheSame(Notifications oldItem, Notifications newItem) {
            return oldItem.getIdNotification().equals(newItem.getIdNotification());
        }

        @Override
        public boolean areContentsTheSame(Notifications oldItem, Notifications newItem) {
            return oldItem.getMessage().equals(newItem.getMessage());
        }
    };

    private List<Notifications> notifications;

    private ItemClickListener itemClickListener;

    private int colorNoLeidos;
    private int colorLeidos;

    private Context context;

    public class NotificationsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private TextView txtDescripcion, txtFecha, txtTipoNotificacion;
        private LinearLayout linear;
        private ItemClickListener listener;

        private NotificationsViewHolder(View itemView, ItemClickListener listener)
        {
            super(itemView);

            linear = itemView.findViewById(R.id.llayout);
            txtDescripcion = itemView.findViewById(R.id.tv_descripcion);
            txtFecha = itemView.findViewById(R.id.tv_fecha);
            txtTipoNotificacion = itemView.findViewById(R.id.tv_tipo_notificacion);

            this.listener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Notifications notification = getItem(getAdapterPosition());

            if (notification == null)
                return;

            listener.onItemClick(notification, getAdapterPosition());
        }
    }

    public NotificationsRecyclerAdapter(Context context, ItemClickListener itemClickListener)
    {

        super(itemCallback);

        this.itemClickListener = itemClickListener;

        notifications = new ArrayList<>();

        this.colorNoLeidos = context.getResources().getColor(R.color.list_no_leidos);
        this.colorLeidos = context.getResources().getColor(R.color.list_leidos);

        this.context = context;
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationsViewHolder holder, int position) {
        Notifications notification = getItem(position);

        if (notification != null) {
            int numCobrador;
			/*
			try {
				numCobrador = json.getInt("no_cobrador");
				Log.e("posicion: "+position, "numCobrador"+numCobrador+" texto: "+json.getString("notificacion"));
			} catch (JSONException e2) {
				e2.printStackTrace();
			}
			*/
            numCobrador = Integer.parseInt( notification.getNumberCollector() );

            //general notification
            if (numCobrador == -1) {
                holder.linear.setBackgroundColor(colorNoLeidos);
                holder.txtDescripcion.setText("General");
                //tvNotificationType.setText("General");
                holder.txtTipoNotificacion.setText("");
            }
            //personal notification
            else {
                holder.txtTipoNotificacion.setText("Personal");
                //tvNotificationType.setText("Personal");
                int tipoRenglon = 1;
                //try {
                //set read or not read
                tipoRenglon = notification.getStatus();
                //tipoRenglon = json.getInt("estatus");
                if (tipoRenglon == 0) {
                    holder.linear.setBackgroundColor(colorNoLeidos);
                } else if (tipoRenglon == 1){
                    holder.linear.setBackgroundColor(colorLeidos);
                    holder.txtDescripcion.setTextColor(ContextCompat.getColor(context, R.color.black));
                    holder.txtFecha.setTextColor(ContextCompat.getColor(context, R.color.black));
                    holder.txtTipoNotificacion.setTextColor(ContextCompat.getColor(context, R.color.black));
                }
                //} catch (JSONException e1) {
                //	e1.printStackTrace();
                //}
            }

            //set notification message
			/*
			try {
				tvDescription.setText(json.getString("notificacion"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			*/
            //set notification message
            holder.txtDescripcion.setText(notification.getMessage());

            //set notification date
			/*
			try {
				tvDate.setText(json.getString("fecha_creacion"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			*/
            //set notification date
            holder.txtFecha.setText(notification.getCreationDate());

        }
        //load more notifications button
        else {
            Log.e("nulo", ""+position);
            /*LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.row_notificaciones_loading, null);
            final View tvMensaje = view.findViewById(R.id.tv_mensaje_loading);
            final View progress = view.findViewById(R.id.progressBar1);
            view.setOnClickListener(v -> {
                tvMensaje.setVisibility(View.GONE);
                progress.setVisibility(View.VISIBLE);
                callback.descargarPaginaSiguiente(position-1);
            });*/
        }
    }

    @NonNull
    @Override
    public NotificationsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notificaciones, parent, false);

        return new NotificationsViewHolder(itemView, itemClickListener);
    }

    @Nullable
    @Override
    protected Notifications getItem(int position) {
        return notifications.get(position);
    }

    public interface ItemClickListener
    {
        void onItemClick(Notifications notification, int position);
    }
}
