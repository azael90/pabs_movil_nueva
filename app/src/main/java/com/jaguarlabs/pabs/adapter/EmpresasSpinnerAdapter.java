package com.jaguarlabs.pabs.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.util.ExtendedActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

/**
 * Show companies to deposit to.
 *
 * This adapter is used to populate and provide access to the
 * data items with the {@link android.widget.ListView}
 * used to show all companies to deposito to.
 *
 * To see this {@link android.widget.ListView} you have to be on
 * the deposit screen and click on the 'Favor de seleccionar una empresa'
 * TextField,
 * you'll see all company names; info managed by this adapter.
 *
 * For further information about overridden methods
 * @see {@link android.widget.Adapter}
 * @see {@link SpinnerAdapter}
 */
public class EmpresasSpinnerAdapter implements SpinnerAdapter{

	//companies' names
	JSONArray elements;

	List<Companies> companiesList;

	Context context;

	public EmpresasSpinnerAdapter(List<Companies> companiesList, Context context) throws JSONException {
		super();
		//this.elements = elements;
		this.companiesList = companiesList;
		this.context = context;
	}

	@Override
	public int getCount() {
		return companiesList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		//try {
			//return company id
			return Long.parseLong(companiesList.get(position).getIdCompany());
		//} catch (JSONException e) {
		//7	return 0;
		//}
	}

	/**
	 * get company name by given position
	 *
	 * @param position
	 * 		 	ListView position
	 * @return
	 * 			company name
	 */
	public String getEmpresaName(int position){
		//try{
			return companiesList.get(position).getName();
		//}catch (JSONException e){
		//7	return "00";
		//7}
	}

	/**
	 * get company id by given position
	 *
	 * @param position
	 * 			ListView position
	 * @return
	 * 			company id
	 */
	public String getEmpresaId(int position){
		//try{
			return companiesList.get(position).getIdCompany();
		//}catch (JSONException e){
		//	return "00";
		//}
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		//make a view for each company name
		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			v = inflater.inflate(R.layout.row_spinner_banks, null);

			//set TextView properties
			TextView tv = (TextView) v;
			tv.setTextSize(15f);
			//tv.setTextColor(Color.BLACK);
		}
		
		TextView tv = (TextView) v;

		//set comoany name
		//try {
		//	tv.setText(elements.getJSONObject(position).getString(Empresa.nombre_empresa));
		//} catch (JSONException e) {
		//	e.printStackTrace();
		//}
		tv.setText( ExtendedActivity.getCompanyNameFormatted( companiesList.get(position).getName() ) );
		
		return v;
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {

		//make a view
		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
  		}

		//set company name and text color
		//try {
			((TextView) convertView).setText( ExtendedActivity.getCompanyNameFormatted( companiesList.get(position).getName(), false ) );
			((TextView)convertView).setTextColor(Color.BLACK);
		//} catch (JSONException e) {
		//	e.printStackTrace();
		//}

		return convertView;
	}
	
}