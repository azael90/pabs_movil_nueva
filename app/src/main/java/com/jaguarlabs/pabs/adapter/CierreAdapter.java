package com.jaguarlabs.pabs.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.Suspensionitem;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.MyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

/**
 * Show all payment made. 'Cierre' and 'CierreInformativo' screens.
 *
 * This adapter is used to populate and provide access to the
 * data items with the {@link android.widget.ListView}
 * used to show all payments and visits made.
 *
 * To see this {@link android.widget.ListView} you only have to go
 * either to check your 'Cierre Informativo' or after doing a
 * deposit to office. all payments and visits information is
 * shown by this adapter.
 *
 * For further information about overridden methods
 * @see {@link android.widget.Adapter}
 */
public class CierreAdapter extends BaseAdapter {

	private Context context;

	//payments and visits made
	private JSONArray array;

	private MyCallBack callback;
	private boolean cancelable;

	private LayoutInflater inflater;

	public CierreAdapter(Context context, MyCallBack callback, JSONArray array, boolean cancelable) {
		this.context = context;
		this.callback = callback;
		this.array = array;
		this.cancelable = cancelable;

		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return array.length();
	}

	@Override
	public Object getItem(int position) {
	    try {
            return array.getJSONObject(position);
        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }

        return null;
	}

	@Override
	public long getItemId(int position) {
		return position;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View view = convertView;
		ViewHolder viewHolder;
		ArrayList<String> esquemas;
		if (view == null) {
			view = inflater.inflate(R.layout.row_cierre_dia, null);
			viewHolder = new ViewHolder();
			viewHolder.ibCancel = (ImageButton) view.findViewById(R.id.btn_cancelar_ticket);
			viewHolder.tvCompany = (TextView) view.findViewById(R.id.tv_empresa_cierre);
			viewHolder.tvContractNumber = (TextView) view.findViewById(R.id.tv_no_contrato_cierre);
			viewHolder.tvClient = (TextView) view.findViewById(R.id.tv_cliente_cierre);
			viewHolder.tvAmount = (TextView) view.findViewById(R.id.tv_aportacion_cierre);
			viewHolder.tvFolio = (TextView) view.findViewById(R.id.tv_folio_cierre);
			viewHolder.tvCopies = (TextView) view.findViewById(R.id.tv_copias);
			viewHolder.tvCanceled = (TextView) view.findViewById(R.id.tv_cancelado);
			viewHolder.tvFechaCobro = (TextView) view.findViewById(R.id.tvFechaCobro);
			view.setTag(viewHolder);

		}
		else {
			viewHolder = (ViewHolder) view.getTag();
		}

		viewHolder.ibCancel.setTag(position);
		viewHolder.ibCancel.setOnClickListener(v -> callback.onClickCancel(position));

		try {
			JSONObject jsonContrato = array.getJSONObject(position);
			String indexEmpresa = jsonContrato.getString("empresa");
			String nombreEmpresa = ExtendedActivity.getCompanyNameFormatted( DatabaseAssistant.getSingleCompany(indexEmpresa).getName() );
			viewHolder.tvCompany.setText(nombreEmpresa);
			viewHolder.tvContractNumber.setText(jsonContrato.getString("no_cliente"));
			if (jsonContrato.has("nombre")) {
				if (!jsonContrato.getString("nombre").equals("null")) {
					viewHolder.tvClient.setText(jsonContrato.getString("nombre"));
				} else {
					viewHolder.tvClient.setText("Sin Datos");
				}
			} else {
				viewHolder.tvClient.setText("Sin Datos");
			}


			viewHolder.tvFechaCobro.setText(jsonContrato.getString("tiempo"));


			viewHolder.tvAmount.setText("$" + formatMoney(Double.parseDouble(jsonContrato.getString("monto"))));
			viewHolder.tvFolio.setText(jsonContrato.getString("folio"));
			viewHolder.tvCopies.setText(jsonContrato.getString("copias"));
			int statusTicket = jsonContrato.getInt("status");
			int tipoCobro = jsonContrato.getInt("tipo_cobro");
			viewHolder.tvCanceled.setTextColor(context.getResources().getColor(R.color.color_wallet_holo_blue_light));

			/*
			esquemas = new ArrayList<String>();
			esquemas.add("Selecciona una opción");
			List<Suspensionitem> lista = Suspensionitem.listAll(Suspensionitem.class);
			if(lista.size()>0) {
				for (int i = 0; i < lista.size(); i++) {
					try {
						esquemas.add(lista.get(i).getDescripcion().toUpperCase());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			else {
				Snackbar.make(viewHolder.spMotivos, "Registros menores a 0", Snackbar.LENGTH_LONG).show();
			}

			if(statusTicket!=7)
			{
				viewHolder.spMotivos.setVisibility(View.VISIBLE);
				ArrayAdapter<CharSequence> adaptador = new ArrayAdapter(context, android.R.layout.simple_spinner_item, esquemas);
				viewHolder.spMotivos.setAdapter(adaptador);
			}else
				viewHolder.spMotivos.setVisibility(View.GONE); */



			String statusString = "";
			if (statusTicket == 1)
			{
				statusString = "";
				view.findViewById(R.id.ll_cancelado).setVisibility(
						View.VISIBLE);
				viewHolder.ibCancel.setVisibility(View.VISIBLE);

				if (tipoCobro == 2) {
					statusString = "MANUAL";
					viewHolder.tvCanceled.setTextColor(context.getResources().getColor(R.color.red));
					viewHolder.ibCancel.setVisibility(View.GONE);
				}
				else if (tipoCobro == 3) {
					statusString = "FACTURA";
					viewHolder.tvCanceled.setTextColor(context.getResources().getColor(R.color.red));
					viewHolder.ibCancel.setVisibility(View.GONE);
				}
				else if (tipoCobro == 4)
				{
					statusString = "CANCELADO (NO ENTREGADO)";
					viewHolder.tvCanceled.setTextColor(context.getResources().getColor(R.color.red));
					viewHolder.ibCancel.setVisibility(View.GONE);
				}

				if (!cancelable) {
					viewHolder.ibCancel.setVisibility(View.GONE);
				}
			} else if ( statusTicket >= 2 && statusTicket < 7 ) {
				statusString = "NO APORTÓ";
				viewHolder.tvCanceled.setTextColor(context.getResources().getColor(R.color.red));

				viewHolder.ibCancel.setVisibility(View.GONE);
				view.findViewById(R.id.ll_cancelado).setVisibility(
						View.VISIBLE);
			} else if (statusTicket == 7) {
				statusString = "CANCELADO";
				//Toast.makeText(context, "CANCELADO UNO", Toast.LENGTH_SHORT).show();
				viewHolder.tvCanceled.setTextColor(context.getResources().getColor(R.color.red));
				viewHolder.ibCancel.setVisibility(View.GONE);
				view.findViewById(R.id.ll_cancelado).setVisibility(View.VISIBLE);

			} else if ( statusTicket == 8 ){
				statusString = "NO EXISTE CLIENTE";

				viewHolder.tvCanceled.setTextColor(context.getResources().getColor(R.color.red));

				viewHolder.ibCancel.setVisibility(View.GONE);
				view.findViewById(R.id.ll_cancelado).setVisibility(
						View.VISIBLE);
			} else if ( statusTicket == 9 ){
				statusString = "NO EXISTE DIRECCION";

				viewHolder.tvCanceled.setTextColor(context.getResources().getColor(R.color.red));

				viewHolder.ibCancel.setVisibility(View.GONE);
				view.findViewById(R.id.ll_cancelado).setVisibility(
						View.VISIBLE);
			} else if ( statusTicket == 10 ){
				statusString = "SUPERVISADO";

				viewHolder.tvCanceled.setTextColor(context.getResources().getColor(R.color.red));

				viewHolder.ibCancel.setVisibility(View.GONE);
				view.findViewById(R.id.ll_cancelado).setVisibility(
						View.VISIBLE);
			}

			viewHolder.tvCanceled.setText(statusString);


		} catch (JSONException e) {
			e.printStackTrace();
		}

		return view;
	}

	/**
	 * formats money
	 *
	 * @param money
	 * 			money type
	 * @return
	 * 		formatted money
	 */
	private String formatMoney(double money) {
		DecimalFormatSymbols nf = new DecimalFormatSymbols();
		nf.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("#.##", nf);
		df.setMinimumFractionDigits(2);
		df.setMaximumFractionDigits(2);
		return df.format(money);
	}

	private class ViewHolder {
		private ImageButton ibCancel;
		private TextView tvCompany;
		private TextView tvContractNumber;
		private TextView tvClient;
		private TextView tvAmount;
		private TextView tvFolio;
		private TextView tvCopies;
		private TextView tvCanceled;
		private TextView tvFechaCobro;

	}

	public void updateStatus(int position, String status) throws JSONException
    {
        ((JSONObject) getItem(position)).put("status", status);
        notifyDataSetChanged();
    }

}
