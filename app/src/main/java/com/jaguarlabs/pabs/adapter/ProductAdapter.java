package com.jaguarlabs.pabs.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.components.Preferences;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.LogRegister;
import com.jaguarlabs.pabs.util.Util;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder>
{

    private Context mCtx;
    private List<Product> productList;
    Dialog myDialog;
    public AlertDialog.Builder alertDialog;
    private String nuevaIP="";
    private ExtendedActivity extendedActivity;


    public ProductAdapter(Context mCtx, List<Product> productList)
    {
        this.mCtx = mCtx;
        this.productList = productList;
    }
    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.patron_config, null);

        ProductViewHolder holder = new ProductViewHolder(view);
        myDialog= new Dialog(mCtx);

        alertDialog = new AlertDialog.Builder(mCtx);
        alertDialog.setTitle("Cambio de IP");
        alertDialog.setMessage("Nueva IP");

        //Typeface MThin = Typeface.createFromAsset(mCtx.getAssets(), "fonts/SFProDisplay-Thin.ttf");
        //holder.tvNombreNegocio.setTypeface(MRegular);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position)
    {
        final Product product= productList.get(position);
        holder.tvID.setText(product.getID());
        holder.tvMensaje.setText(product.getMensaje());

        holder.CardMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                switch (holder.tvMensaje.getText().toString())
                {
                    case "Añadir nuevo periodo":
                        //addNewPeriod%
                        Toast.makeText(mCtx, "Añadir nuevo periodo", Toast.LENGTH_SHORT).show();
                        break;

                    case "Cambiar Branch":
                        //Cambiar el branch que lo pida por Spinner
                        break;

                    case "Cambiar IP":
                        //changeServerIP%


                        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                        builder.setTitle("Nueva IP");
                        final EditText input = new EditText(mCtx);
                        input.setInputType(InputType.TYPE_CLASS_TEXT);
                        builder.setView(input);
                        builder.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                nuevaIP=input.getText().toString();
                                changeIP(nuevaIP);
                            }
                        });
                        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();
                        break;

                    case "Cambiar periodo":
                        //changePeriod%
                        break;

                    case "Cerrar periodo":
                        //closePeriod%
                        break;

                    case "Comando a impresora":
                        //printerCommand%
                        break;

                    case "Ejecutar query":
                        //executeQuery%
                        break;

                    case "Eliminar todos los pagos":
                        //deleteAllPayments%
                        break;

                    case "Enviar pagos por txt":
                        //sendPaymentstxt%
                        break;

                    case "Enviar todos los pagos":
                        //resendAllPayments%
                        break;

                    case "Exportar base":
                        //exportDB%
                        break;

                    case "Importar base":
                        //importDB%
                        break;

                    case "Reset BD":
                        SharedPreferences preferences = mCtx.getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
                        //resetDB% Login.class
                        LogRegister.registrarCheat("resetDB%");
                        try
                        {
                            DatabaseAssistant.resetDatabase( ApplicationResourcesProvider.getContext() );
                            mCtx.getSharedPreferences("folios", 0).edit().clear().commit();
                            preferences.edit().clear().commit();
                            Toast.makeText(mCtx, "Reseteando base de datos", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case "Semaforo I/O":
                        //resetSemaforo% Login.class
                        break;

                    case "Verificar internet":
                        //internet%
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        CardView CardMensaje;
        TextView tvID, tvMensaje;

        public ProductViewHolder(View itemView)
        {
            super(itemView);
            tvID= itemView.findViewById(R.id.tvID);
            tvMensaje=itemView.findViewById(R.id.tvMensaje);
            CardMensaje=itemView.findViewById(R.id.CardMensaje);
        }
    }

    private void changeIP(String ip)
    {
        try
        {
            LogRegister.registrarCheat("changeServerIP%");
            Preferences preferencesServerIP = new Preferences(ApplicationResourcesProvider.getContext());
            String oldServerIP = preferencesServerIP.getServerIP();
            preferencesServerIP.changeServerIP(ip);

            Toast.makeText(mCtx, "La IP a cambiado a: " + ip + " \n IP anterior: " + oldServerIP, Toast.LENGTH_SHORT).show();
        } catch (StringIndexOutOfBoundsException e)
        {
            e.printStackTrace();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
