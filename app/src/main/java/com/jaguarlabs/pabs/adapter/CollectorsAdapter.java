package com.jaguarlabs.pabs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.database.Collector;
import com.jaguarlabs.pabs.database.Collectors;
import com.jaguarlabs.pabs.database.DatabaseAssistant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jordan on 25/08/16.
 */
public class CollectorsAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<Collector> collectors;
    private LayoutInflater layoutInflater;

    private OnCollectorClickListener listener;

    private int colorDefaultStrong;
    private int colorDefaultLight;

    public CollectorsAdapter(Context context, List<Collector> collectors){
        this.context = context;
        this.collectors = collectors;
        this.layoutInflater = LayoutInflater.from(context);

        this.colorDefaultStrong = context.getResources().getColor(R.color.liststrong);
        this.colorDefaultLight = context.getResources().getColor(R.color.listlight);
    }

    @Override
    public int getCount() {
        return collectors.size();
    }

    @Override
    public Object getItem(int position) {
        return collectors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder viewHolder;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.row_collector, null);

            viewHolder = new ViewHolder();

            viewHolder.code = (TextView) view.findViewById(R.id.tvCode);
            viewHolder.name = (TextView) view.findViewById(R.id.tvName);
            viewHolder.linearLayout = (LinearLayout) view.findViewById(R.id.linearLayoutCollector);
            viewHolder.collectorNumber = "";

            view.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.code.setText( collectors.get(position).getCode() );
        viewHolder.name.setText( collectors.get(position).getName() );
        viewHolder.collectorNumber = collectors.get(position).getCollectorNumber();

        viewHolder.linearLayout.setBackgroundColor(position % 2 == 1 ? colorDefaultStrong : colorDefaultLight);

        viewHolder.linearLayout.setOnClickListener(v -> listener.onCollectorClick(collectors.get(position)));

        return view;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<Collector> collectors = new ArrayList<>();

                collectors = DatabaseAssistant.getFilteredCollectors(constraint.toString());

                results.count = collectors.size();
                results.values = collectors;

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                collectors = (List<Collector>) results.values;
                notifyDataSetChanged();
            }
        };

        return filter;
    }

    public void setOnCollectorClickListener(OnCollectorClickListener listener){
        this.listener = listener;
    }

    public interface OnCollectorClickListener{
        public void onCollectorClick(Collector collector);
    }

    private class ViewHolder{
        private String collectorNumber;
        private TextView name;
        private TextView code;
        private LinearLayout linearLayout;
    }
}
