package com.jaguarlabs.pabs.adapter;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.components.PreferencesToShowWallet;
import com.jaguarlabs.pabs.database.Clients;
import com.jaguarlabs.pabs.database.SyncedWallet;
import com.jaguarlabs.pabs.models.ModelClient;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.Util;

import org.json.JSONArray;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Show all clients. 'Clientes' screens.
 *
 * This adapter is used to populate and provide access to the
 * data items with the {@link android.widget.ListView}
 * used to show all clients in the main screen (Clientes); less
 * than 500.
 *
 * This {@link android.widget.ListView} is shown on the main screen.
 * All clients are shown by this adapter.
 *
 * For further information about overridden methods
 * @see {@link android.widget.Adapter}
 */
public class ClientesAdapter<CLASS_RESPONSE> extends BaseAdapter{
	private Context context;

	//clients' information
	private JSONArray array;

	/*
	private List<Clients> clientsList;
	private List<SyncedWallet> clientListSynced;
	*/
	private List<CLASS_RESPONSE> clientsList;
	private List<CLASS_RESPONSE> clientListSynced;

	private LayoutInflater inflater;

	private int colorDefaultStrong;
	private int colorDefaultLight;
	private int colorPendingContract;
	private int colorPendingContractStrong;
	private int colorPendingLevel2Contract;
	private int colorPendingLevel2ContractStrong;
	private int colorTodayPayment;
	private int colorTodayPaymentStrong;
	private int colorWithoutAnalysisContract;
	private int colorWithoutAnalysisContractStrong;
	private int colorWithoutAnalysisSuspendedContract;
	private int colorWithoutAnalysisSuspendedContractStrong;
	private int colorFirstPaymentDate;

	private DecimalFormatSymbols nf;
	private DecimalFormat df;	
	private Location myLocation;

	private String listType;

	public static final String FILTERED_CLIENTS = "filtered_clients";
	public static final String NEARBY_CLIENTS = "nearby_clients";
	public static final String WALLET_CLIENTS = "wallet_clients";
	public static final String DAY_WALLET_CLIENTS = "day_wallet_clients";

	public ClientesAdapter(Context context, List<CLASS_RESPONSE> clientsList, String listType){
		nf = new DecimalFormatSymbols();
		df = new DecimalFormat("#.##",nf);

		if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS))){
			this.clientListSynced = clientsList;
		}
		else {
			this.clientsList = clientsList;
		}

		this.context = context;

		this.listType = listType;

		this.colorDefaultStrong = context.getResources().getColor(R.color.liststrong);
		this.colorDefaultLight = context.getResources().getColor(R.color.listlight);
		this.colorPendingContract = context.getResources().getColor(R.color.color_visit_done);
		this.colorPendingContractStrong = context.getResources().getColor(R.color.color_visit_done_strong);;
		this.colorPendingLevel2Contract = context.getResources().getColor(R.color.color_payment_done);
		this.colorPendingLevel2ContractStrong = context.getResources().getColor(R.color.color_payment_done_strong);
		this.colorTodayPayment = context.getResources().getColor(R.color.color_today_payment);
		this.colorTodayPaymentStrong = context.getResources().getColor(R.color.color_today_payment_strong);
		this.colorWithoutAnalysisContract = context.getResources().getColor(R.color.color_without_analysis_contract);
		this.colorWithoutAnalysisContractStrong = context.getResources().getColor(R.color.color_without_analysis_contract_strong);
		this.colorWithoutAnalysisSuspendedContract = context.getResources().getColor(R.color.color_without_analysis_contract_suspended);
		this.colorWithoutAnalysisSuspendedContractStrong = context.getResources().getColor(R.color.color_without_analysis_contract_suspended_strong);
		this.colorFirstPaymentDate = context.getResources().getColor(R.color.color_first_payment_date);



		inflater = LayoutInflater.from(context);
	}

	public ClientesAdapter(Context context, List<CLASS_RESPONSE> clientsList, Location myLocation, String listType){
		this(context, clientsList, listType);
		this.myLocation = myLocation;
	}

	@Override
	public int getCount() {
		/*
		if (array == null){
			return 0;
		}
		if (array.length()>500){
			return 500;
		}else{
			return array.length();
		}
		*/
		if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS))){
			if (clientListSynced == null) {
				return 0;
			}
			//return clientListSynced.size() > 500 ? 500 : clientListSynced.size();
			return clientListSynced.size();
		}
		else {
			if (clientsList == null) {
				return 0;
			}
			return clientsList.size() > 500 ? 500 : clientsList.size();
		}
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		//Util.Log.ih("position = " + position);

		//make a view for each client
		View view = convertView;
		ViewHolder viewHolder;
		if (view == null) {
			//LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.row_cliente, parent, false);

			viewHolder = new ViewHolder();

			viewHolder.numberContract = (TextView) view.findViewById(R.id.textViewID);
			viewHolder.name = (TextView) view.findViewById(R.id.textViewNombre);
			viewHolder.amount = (TextView) view.findViewById(R.id.tv_monto);
			;
			viewHolder.paymentOption = (TextView) view.findViewById(R.id.textViewSem);
			;
			viewHolder.distance = (TextView) view.findViewById(R.id.tv_distancia);
			;
			viewHolder.linearLayoutDistance = (LinearLayout) view.findViewById(R.id.distancia_layout);
			viewHolder.linearLayout = (LinearLayout) view.findViewById(R.id.ly_renglon);

			viewHolder.firstPaymentDate = (TextView) view.findViewById(R.id.tvFirstPaymentDate);
			viewHolder.ivBackgroundFPD = (ImageView) view.findViewById(R.id.ivBackgroundFirstPaymentDate);

			view.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) view.getTag();
		}

		/*
		TextView id = (TextView) view.findViewById(R.id.textViewID);
		TextView nom = (TextView) view.findViewById(R.id.textViewNombre);
		TextView monto = (TextView) view.findViewById(R.id.tv_monto);
		TextView sem = (TextView) view.findViewById(R.id.textViewSem);
		TextView distance = (TextView) view.findViewById(R.id.tv_distancia);
		LinearLayout linear = (LinearLayout) view.findViewById(R.id.ly_renglon);
		*/

		//set view's color
		/*
		if (position % 2 == 1) {
			viewHolder.linearLayout.setBackgroundColor(colorDefaultStrong);
		} else {
			viewHolder.linearLayout.setBackgroundColor(colorDefaultLight);
		}
		*/

		//get client
		/*
		JSONObject json = null;
		try {
			json = array.getJSONObject(position);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		*/

		//get client
		Clients client = new Clients();
		SyncedWallet clientFromSyncedWallet = new SyncedWallet();
		ModelClient clientNearbyFromSyncedWallet = new ModelClient();

		if (BuildConfig.isWalletSynchronizationEnabled() && listType.equals(DAY_WALLET_CLIENTS)){
			clientFromSyncedWallet = (SyncedWallet)clientListSynced.get(position);
			//clientFromSyncedWallet = new SyncedWallet((SyncedWallet)clientListSynced.get(position));
		}
		else if (BuildConfig.isWalletSynchronizationEnabled() && listType.equals(NEARBY_CLIENTS)){
			clientNearbyFromSyncedWallet = (ModelClient)clientListSynced.get(position);

			clientFromSyncedWallet.setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);
			clientFromSyncedWallet.setClient(clientNearbyFromSyncedWallet);
		}
		else {
			client = (Clients)clientsList.get(position);
		}

		//Util.Log.ih("clientFromSyncedWallet 1 = " + clientFromSyncedWallet.getClient().getName());

		if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS))){

			/*
			PreferencesToShowWallet preferencesToShowWallet = new PreferencesToShowWallet(ApplicationResourcesProvider.getContext());
			long dayCounter = preferencesToShowWallet.getDayCounter();
			Calendar calendar = Calendar.getInstance();
			*/

			//TODO: BEFORE
			/*
			if (clientFromSyncedWallet.getVisitCounter() < 2) {
				if (!clientFromSyncedWallet.isPaymentMade()){
					if (clientFromSyncedWallet.getDayCounter() != dayCounter) {
						if ((dayCounter - clientFromSyncedWallet.getDayCounter()) == 1) {
							viewHolder.linearLayout.setBackgroundColor(colorPendingContract);
						} else {
							viewHolder.linearLayout.setBackgroundColor(colorPendingLevel2Contract);
						}
					} else if (clientFromSyncedWallet.isShowColor()) {
						viewHolder.linearLayout.setBackgroundColor(colorTodayPayment);
					}
				}
			} else if (clientFromSyncedWallet.isShowColor()) {
				viewHolder.linearLayout.setBackgroundColor(colorTodayPayment);
			}
			*/
			//Util.Log.ih("clientFromSyncedWallet 2 = " + clientFromSyncedWallet);

			//hide first payment date mask
			viewHolder.firstPaymentDate.setVisibility(View.GONE);
			viewHolder.firstPaymentDate.setTextColor(Color.BLACK);
			viewHolder.ivBackgroundFPD.setVisibility(View.GONE);

			viewHolder.numberContract.setTextColor(Color.WHITE);
			viewHolder.name.setTextColor(Color.WHITE);
			viewHolder.amount.setTextColor(Color.WHITE);
			viewHolder.paymentOption.setTextColor(Color.WHITE);

			//TODO: AFTER
			//set view's color
			switch (clientFromSyncedWallet.getColor()){
				case SyncedWallet.COLOR_DEFAULT_CONTRACT:
					if (position % 2 == 1) {
						viewHolder.linearLayout.setBackgroundColor(colorDefaultStrong);
					} else {
						viewHolder.linearLayout.setBackgroundColor(colorDefaultLight);
					}
					break;
				case SyncedWallet.COLOR_PENDING_CONTRACT:
					if (position % 2 == 1) {
						viewHolder.linearLayout.setBackgroundColor(colorPendingContractStrong);
					} else {
						viewHolder.linearLayout.setBackgroundColor(colorPendingContract);
					}
					break;
				case SyncedWallet.COLOR_PENDING_LEVEL_2_CONTRACT:
					if (position % 2 == 1) {
						viewHolder.linearLayout.setBackgroundColor(colorPendingLevel2ContractStrong);
					} else {
						viewHolder.linearLayout.setBackgroundColor(colorPendingLevel2Contract);
					}
					break;
				case SyncedWallet.COLOR_TODAY_CONTRACT:
					if (position % 2 == 1) {
						viewHolder.linearLayout.setBackgroundColor(colorTodayPaymentStrong);
					} else {
						viewHolder.linearLayout.setBackgroundColor(colorTodayPayment);
					}
					break;
				case SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT:

					/**
					 * FPA
					 * First Payment Date
					 * shows FPS if needed
					 */
					/*
					try {
						//get milliseconds of first payment date
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
						formatter.setLenient(false);

						String firstPaymentDateString = clientFromSyncedWallet.getClient().getFirstPaymentDate();

						Date firstPaymentDateDateFormat = formatter.parse(firstPaymentDateString);
						long firstPaymentDateMilliseconds = firstPaymentDateDateFormat.getTime();

						//get millisecond of today date
						long date = new Date().getTime();

						long difference = firstPaymentDateMilliseconds - date;
						boolean deleteFPA = difference < 0;


						Util.Log.ih("difference = " + difference);

						//check if first payment date is greater or equal to today's date
						//if so, show first payment date mask
						//if (firstPaymentDateMilliseconds >= date) -> greater validation (not equal)

						//when deleteFPA is true, means that FPA hasn't come yet
						//so show FPA mask without checking if a payment was made or if
						//contract has a last payment date
						if (!deleteFPA) {
							Util.Log.ih("adding date... deleteFPA -> false");
							viewHolder.firstPaymentDate.setText(firstPaymentDateString);
							viewHolder.ivBackgroundFPD.setVisibility(View.VISIBLE);
							viewHolder.firstPaymentDate.setVisibility(View.VISIBLE);
						} else {
							long maxDifference = -difference;

							Util.Log.ih("maxDifference = " + maxDifference);

							if ( !clientFromSyncedWallet.isPaymentMade() && clientFromSyncedWallet.getClient().getLastPaymentDate().equals("0000-00-00")) {
								//if (maxDifference < 90000000) {
								Util.Log.ih("adding date...");
								viewHolder.firstPaymentDate.setText(firstPaymentDateString);
								viewHolder.firstPaymentDate.setTextColor(colorFirstPaymentDate);
								viewHolder.ivBackgroundFPD.setVisibility(View.VISIBLE);
								viewHolder.firstPaymentDate.setVisibility(View.VISIBLE);
							} else {
								Util.Log.ih("do not adding date...");
							}
						}
					} catch (ParseException e){
						e.printStackTrace();
					} catch (Exception e){
						e.printStackTrace();
					}
					*/


					/**
					 * TODO: delete this part of the code
					 * -----------------------------------------------------------------------------
					 */
					/*
					//if a contract without payday analysis is paid, set it as blue, gray otherwise
					if (clientFromSyncedWallet.getColorWithoutAnalysisContractAux().equals(SyncedWallet.COLOR_DEFAULT_CONTRACT)){
						if (position % 2 == 1) {
							viewHolder.linearLayout.setBackgroundColor(colorDefaultStrong);
						} else {
							viewHolder.linearLayout.setBackgroundColor(colorDefaultLight);
						}
					}//-----------------------------------------------------------------------------
					*/
					//else {
						/*
						if (!clientFromSyncedWallet.getStatus().equals("1")){
							viewHolder.numberContract.setTextColor(Color.BLACK);
							viewHolder.name.setTextColor(Color.BLACK);
							viewHolder.amount.setTextColor(Color.BLACK);
							viewHolder.paymentOption.setTextColor(Color.BLACK);
							;
							viewHolder.paymentOption = (TextView) view.findViewById(R.id.textViewSem);
							if (position % 2 == 1) {
								viewHolder.linearLayout.setBackgroundColor(colorWithoutAnalysisSuspendedContractStrong);
							} else {
								viewHolder.linearLayout.setBackgroundColor(colorWithoutAnalysisSuspendedContract);
							}
						} else {
							if (position % 2 == 1) {
								viewHolder.linearLayout.setBackgroundColor(colorWithoutAnalysisContractStrong);
							} else {
								viewHolder.linearLayout.setBackgroundColor(colorWithoutAnalysisContract);
							}
						}

						if (position % 2 == 1) {
							viewHolder.linearLayout.setBackgroundColor(colorWithoutAnalysisContractStrong);
						} else {
							viewHolder.linearLayout.setBackgroundColor(colorWithoutAnalysisContract);
						}
						*/
					//}
					//Util.Log.ih("COLOR_WITHOUT_ANALYSIS_CONTRACT");
					//Util.Log.ih("name = " + clientFromSyncedWallet.getClient().getName());
					if ( clientFromSyncedWallet.isFPA() && !clientFromSyncedWallet.isFPAPending() ){
						//Util.Log.ih("add black FPA");
						//Util.Log.ih("clientFromSyncedWallet.isFPA() = " + clientFromSyncedWallet.isFPA());
						//Util.Log.ih("!clientFromSyncedWallet.isFPAPending() = " + !clientFromSyncedWallet.isFPAPending());
						//add FPA
						viewHolder.firstPaymentDate.setText( clientFromSyncedWallet.getClient().getFirstPaymentDate() );
						viewHolder.ivBackgroundFPD.setVisibility(View.VISIBLE);
						viewHolder.firstPaymentDate.setVisibility(View.VISIBLE);
					} else if ( clientFromSyncedWallet.isFPAPending() ){
						//add pending FPA
						//Util.Log.ih("add red FPA");
						//Util.Log.ih("clientFromSyncedWallet.isFPAPending() = " + clientFromSyncedWallet.isFPAPending());
						viewHolder.firstPaymentDate.setText( clientFromSyncedWallet.getClient().getFirstPaymentDate() );
						viewHolder.firstPaymentDate.setTextColor(colorFirstPaymentDate);
						viewHolder.ivBackgroundFPD.setVisibility(View.VISIBLE);
						viewHolder.firstPaymentDate.setVisibility(View.VISIBLE);
					}

					if (position % 2 == 1) {
						viewHolder.linearLayout.setBackgroundColor(colorWithoutAnalysisContractStrong);
					} else {
						viewHolder.linearLayout.setBackgroundColor(colorWithoutAnalysisContract);
					}

					break;
				case SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT:
					viewHolder.numberContract.setTextColor(Color.BLACK);
					viewHolder.name.setTextColor(Color.BLACK);
					viewHolder.amount.setTextColor(Color.BLACK);
					viewHolder.paymentOption.setTextColor(Color.BLACK);
					;
					viewHolder.paymentOption = (TextView) view.findViewById(R.id.textViewSem);
					if (position % 2 == 1) {
						viewHolder.linearLayout.setBackgroundColor(colorWithoutAnalysisSuspendedContractStrong);
					} else {
						viewHolder.linearLayout.setBackgroundColor(colorWithoutAnalysisSuspendedContract);
					}
			}

			//Util.Log.ih("clientFromSyncedWallet 3 = " + clientFromSyncedWallet);
		}
		else {
			//set view's color
			if (position % 2 == 1) {
				viewHolder.linearLayout.setBackgroundColor(colorDefaultStrong);
			} else {
				viewHolder.linearLayout.setBackgroundColor(colorDefaultLight);
			}
		}


		//Util.Log.ih("clientFromSyncedWallet 4 = " + clientFromSyncedWallet);


		//if wallet synchronization is enabled
		//shows a different color for that view when a visit
		//for this client was made
		/*
		try {
			if (json.getInt("visit") == 1) {
				linear.setBackgroundColor(colorPendingContract);
			}
			else{
				linear.setBackgroundColor(colorPendingLevel2Contract);
			}
		} catch (JSONException e){
			//e.printStackTrace();
		}
		*/

		//set id
		//compound by serie + folio
		/*
		try {
			//id.setText(json.getString("serie")+"-"+json.getString("no_contrato"));
			id.setText(json.getString("no_contrato"));
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		*/
//		Util.Log.ih("clientFromSyncedWallet 5 = " + clientFromSyncedWallet.getClient().getSerie());

		if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS))){

			//Util.Log.ih("clientFromSyncedWallet 6 = " + clientFromSyncedWallet.getClient().getName());

			//set id
			viewHolder.numberContract.setText(clientFromSyncedWallet.getClient().getSerie() + clientFromSyncedWallet.getClient().getNumberContract());
			//set client name
			viewHolder.name.setText(clientFromSyncedWallet.getClient().getName() + " " + clientFromSyncedWallet.getClient().getFirstLastName() + " " + clientFromSyncedWallet.getClient().getSecondLastName());
			//set amount that client pays
			viewHolder.amount.setText("$" + formatMoney(Double.parseDouble(clientFromSyncedWallet.getClient().getActualPayment())));
			//set distance to client when needed
			if (myLocation != null) {
				viewHolder.distance.setText("" + formatMoney(Double.parseDouble(clientFromSyncedWallet.getClient().getDistance())) + "m");
			} else {
				//view.findViewById(R.id.distancia_layout).setVisibility(View.GONE);
				viewHolder.linearLayoutDistance.setVisibility(View.GONE);
				viewHolder.amount.setGravity(Gravity.CENTER);
				viewHolder.paymentOption.setGravity(Gravity.CENTER);
			}
			//set payment option
			//- weekly
			//- fortnightly
			//- monthly
			String paymentOption = "";
			switch (Integer.parseInt(clientFromSyncedWallet.getClient().getPaymentOption())) {
				case 1:
					paymentOption = "Sem";
					break;
				case 2:
					paymentOption = "Quin";
					break;
				case 3:
					paymentOption = "Men";
					break;
				default:
					break;
			}
			viewHolder.paymentOption.setText(paymentOption);
		}
		else {
			//set id
			viewHolder.numberContract.setText(client.getSerie() + client.getNumberContract());
			//set client name
			viewHolder.name.setText(client.getName() + " " + client.getFirstLastName() + " " + client.getSecondLastName());
			//set amount that client pays
			viewHolder.amount.setText("$" + formatMoney(Double.parseDouble(client.getActualPayment())));
			//set distance to client when needed
			if (myLocation != null) {
				viewHolder.distance.setText("" + formatMoney(Double.parseDouble(client.getDistance())) + "m");
			} else {
				//view.findViewById(R.id.distancia_layout).setVisibility(View.GONE);
				viewHolder.linearLayoutDistance.setVisibility(View.GONE);
				viewHolder.amount.setGravity(Gravity.CENTER);
				viewHolder.paymentOption.setGravity(Gravity.CENTER);
			}
			//set payment option
			//- weekly
			//- fortnightly
			//- monthly
			String paymentOption = "";
			switch (Integer.parseInt(client.getPaymentOption())) {
				case 1:
					paymentOption = "Sem";
					break;
				case 2:
					paymentOption = "Quin";
					break;
				case 3:
					paymentOption = "Men";
					break;
				default:
					break;
			}
			viewHolder.paymentOption.setText(paymentOption);
		}

		return view;
	}

	/**
	 * formats money
	 *
	 * @param money
	 * 			money type
	 * @return
	 * 		formatted money
	 */
	private String formatMoney(double money) {
		nf.setDecimalSeparator('.');
		df = new DecimalFormat("#.##",nf);
		df.setMinimumFractionDigits(2);
		df.setMaximumFractionDigits(2);
		return df.format(money);
	}

	private class ViewHolder{
		private TextView numberContract;
		private TextView name;
		private TextView amount;
		private TextView paymentOption;
		private TextView distance;
		private LinearLayout linearLayoutDistance;
		private LinearLayout linearLayout;
		private TextView firstPaymentDate;
		private ImageView ivBackgroundFPD;
		private boolean showFPA;
		private boolean showFPAPending;
	}
}
