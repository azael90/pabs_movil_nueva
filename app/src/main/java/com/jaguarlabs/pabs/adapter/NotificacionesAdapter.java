package com.jaguarlabs.pabs.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.database.Notifications;
import com.jaguarlabs.pabs.util.PaginacionCallback;

import org.json.JSONArray;

import java.util.List;

//import com.apphance.android.Log;

/**
 * Show notifications.
 *
 * This adapter is used to populate and provide access to the
 * data items with the {@link android.widget.ListView}
 * used to show all notifications.
 *
 * In order to see this {@link android.widget.ListView}, you have
 * to go to Notifications screen. All notifications info is access
 * by this adapter.
 *
 * For further information about overridden methods
 * @see {@link android.widget.Adapter}
 */
@SuppressWarnings("SpellCheckingInspection")
@SuppressLint("CutPasteId")
public class NotificacionesAdapter extends BaseAdapter {

	private Context context;

	//notifications info
	private JSONArray arrayNotifications;

	private List<Notifications> notificationsList;

	private LayoutInflater inflater;

	private int colorNoLeidos;
	private int colorLeidos;

	private PaginacionCallback callback;

	public NotificacionesAdapter(Context context, List<Notifications> notificationsList, PaginacionCallback callback, boolean descargarMas) {
		this.context = context;
		this.notificationsList = notificationsList;
		if (descargarMas) {
			//this.arrayNotifications.put(null);
			this.notificationsList.add(null);
		}
		this.callback = callback;
		this.colorNoLeidos = context.getResources().getColor(R.color.list_no_leidos);
		this.colorLeidos = context.getResources().getColor(R.color.list_leidos);

		inflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		//return arrayNotifications.length();
		return notificationsList.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		//make a view for each notification
		View view = convertView;
		if (view == null) {
			inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.row_notificaciones, null);
		}

		View viewTmp = view.findViewById(R.id.progressBar1);
		if (viewTmp != null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.row_notificaciones, null);
		}

		TextView tvDescripcion = (TextView)view.findViewById(R.id.tv_descripcion);
		TextView tvTipoNotificacion = (TextView)view.findViewById(R.id.tv_tipo_notificacion);
		TextView tvFecha = (TextView)view.findViewById(R.id.tv_fecha);
		ImageView imgTipo = (ImageView) view.findViewById(R.id.imgTipo);

		Notifications notification = notificationsList.get(position);

		if (notification != null) {
			int numCobrador = -1;
			numCobrador = Integer.parseInt( notification.getNumberCollector() );

			//general notification
			if (numCobrador == -1) {
				LinearLayout linear = (LinearLayout)view.findViewById(R.id.llayout);
				//linear.setBackgroundColor(colorNoLeidos);
				tvDescripcion.setText("General");
                tvTipoNotificacion.setText("");
                tvTipoNotificacion.setVisibility(View.GONE);
				imgTipo.setImageResource(R.drawable.motocicleta);
			}


			//personal notification
			else {
				imgTipo.setImageResource(R.drawable.personal);
				tvTipoNotificacion.setText("Personal");
				tvTipoNotificacion.setVisibility(View.VISIBLE);
				int tipoRenglon = 1;
				//try {
					//set read or not read
					tipoRenglon = notification.getStatus();
					//tipoRenglon = json.getInt("estatus");
					LinearLayout linear = (LinearLayout)view.findViewById(R.id.llayout);

					/*if (tipoRenglon == 0) {
						linear.setBackgroundColor(colorNoLeidos);
					} else if (tipoRenglon == 1){
						linear.setBackgroundColor(colorLeidos);
						tvDescripcion.setTextColor(ContextCompat.getColor(context, R.color.black));
						tvFecha.setTextColor(ContextCompat.getColor(context, R.color.black));
						tvTipoNotificacion.setTextColor(ContextCompat.getColor(context, R.color.black));
					}*/
			}

			tvDescripcion.setText(notification.getMessage());
			tvFecha.setText(notification.getCreationDate());
			
		}
		//load more notifications button
		else {
			Log.e("nulo", ""+position);
			LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(R.layout.row_notificaciones_loading, null);
			final View tvMensaje = view.findViewById(R.id.tv_mensaje_loading);
			final View progress = view.findViewById(R.id.progressBar1);
			view.setOnClickListener(v -> {
				tvMensaje.setVisibility(View.GONE);
				progress.setVisibility(View.VISIBLE);
				callback.descargarPaginaSiguiente(position-1);
			});
			
		}
		
		return view;
	}
}
