package com.jaguarlabs.pabs.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.util.BuildConfig;

import java.util.ArrayList;

/**
 * Show all banks that can be deposited to. 'Deposito' screen.
 *
 * This adapter is used to populate and provide access to the
 * data items with the {@link android.widget.ListView}
 * used to show all banks that can be deposited to.
 *
 * To see this {@link android.widget.ListView} you have to be on
 * the deposit screen and click on the 'banco' TextField,
 * you'll see all bank names; info managed by this adapter.
 *
 * For further information about overridden methods
 * @see {@link android.widget.Adapter}
 */
public class BanksArrayAdapter extends BaseAdapter {

	//banks' names array
	private ArrayList<String> array;

	private Context context;
	
	public BanksArrayAdapter(Context context, ArrayList<String> array) {
		this.array = array;
		this.context = context;
	}
	
	@Override
	public int getCount() {
		return array.size();
	}

	@Override
	public Object getItem(int arg0) {
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View convertView, ViewGroup arg2) {

		//arg0 -> position
		//make a view for each bank name
		View v = convertView;

		if (v == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			v = inflater.inflate(R.layout.row_spinner_banks, null);
			TextView tv = (TextView) v;
			tv.setTextColor(Color.BLACK);

			switch (BuildConfig.getTargetBranch()) {
				case GUADALAJARA:
					switch (arg0) {
						case 0:
						case 2:
						case 4:
						case 6:
						case 8:
						case 10:
							//unavailable banks
							tv.setBackgroundColor(Color.GRAY);
							break;
						default:
							//available banks
							tv.setBackgroundColor(Color.WHITE);
					}
					break;
				default:
					tv.setBackgroundColor(Color.WHITE);
			}
		}

		//set TextView's text as a bank name
		TextView tv = (TextView) v;
		tv.setText(array.get(arg0));
		
		return v;
	}

}
