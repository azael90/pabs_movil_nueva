package com.jaguarlabs.pabs.adapter;

/**
 * Created by AzaelJimenez on 29/06/2019.
 */
public class Product
{
    private String Mensaje;
    private String ID;

    public Product(String ID, String Mensaje) {
        this.ID = ID;
        this.Mensaje = Mensaje;

    }

    public String getID() {
        return ID;
    }

    public String getMensaje() {
        return Mensaje;
    }

}
