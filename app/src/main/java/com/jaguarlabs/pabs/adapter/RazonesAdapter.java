package com.jaguarlabs.pabs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import com.jaguarlabs.pabs.R;

/**
 * Show reason about why the collector is making a visit.
 *
 * This adapter is used to populate and provide access to the
 * data items with the {@link android.widget.ListView}
 * used to show reason about why the collector is making
 * a visit. Listed below:
 * - No se encontró al cliente
 * - No tenía dinero
 * - Acordó plazo con promotor
 * - Difirió el pago
 * - Cambió de domicilio
 *
 * To see this {@link android.widget.ListView} you have to be on
 * 'Clliente Detalle' screen and select 'No aportó' button,
 * which will list 5 reasons previously mentioned.
 *
 * For further information about overridden methods
 * @see {@link android.widget.Adapter}
 */
public class RazonesAdapter extends BaseAdapter {

	private Context context;

	//listed reasons
	private String[] razones;

	public RazonesAdapter(Context context, String[] razones) {
		this.context = context;
		this.razones = razones;
	}
	
	@Override
	public int getCount() {
		return razones.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		//make a view
		CheckedTextView view = (CheckedTextView)convertView;
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			view = (CheckedTextView)inflater.inflate(android.R.layout.simple_list_item_single_choice, null);
			view.setCheckMarkDrawable(context.getResources().getDrawable(R.drawable.selector_checked_circle));
			view.setPadding(7, 10, 7, 10);
			view.setTextColor(context.getResources().getColor(R.color.base_color));
		}

		//set reasons
		view.setText(razones[position]);
		return view;
	}

}
