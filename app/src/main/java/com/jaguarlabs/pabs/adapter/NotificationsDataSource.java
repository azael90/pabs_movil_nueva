package com.jaguarlabs.pabs.adapter;

import android.arch.paging.ItemKeyedDataSource;
import android.content.Context;
import android.support.annotation.NonNull;

import com.jaguarlabs.pabs.database.Notifications;
import com.jaguarlabs.pabs.net.VolleySingleton;

/**
 * Created by Emmanuel Rodriguez on 23/07/2018.
 */
public class NotificationsDataSource extends ItemKeyedDataSource<String, Notifications> {

    private Context context;
    private int no_cobrador, pagina;

    public NotificationsDataSource(Context context, int no_cobrador, int pagina)
    {
        this.context = context;
        this.no_cobrador = no_cobrador;
        this.pagina = pagina;
    }

    @Override
    public void loadBefore(@NonNull LoadParams<String> params, @NonNull LoadCallback<Notifications> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<String> params, @NonNull LoadCallback<Notifications> callback) {

    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<String> params, @NonNull LoadInitialCallback<Notifications> callback) {
    }

    @NonNull
    @Override
    public String getKey(@NonNull Notifications item) {
        return item.getIdNotification();
    }
}
