package com.jaguarlabs.pabs.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.models.ModelCobrosRealizadosPorPeriodo;

import java.util.List;

public class AdapterCobrosRealizadosEnElPeriodo extends RecyclerView.Adapter<AdapterCobrosRealizadosEnElPeriodo.ProductViewHolder>
{

    private Context mCtx;
    private List<ModelCobrosRealizadosPorPeriodo> productList;


    public AdapterCobrosRealizadosEnElPeriodo(Context mCtx, List<ModelCobrosRealizadosPorPeriodo> productList)
    {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view =inflater.inflate(R.layout.layout_lista_de_cobros_realizados, null );
        ProductViewHolder holder = new ProductViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position)
    {
        final ModelCobrosRealizadosPorPeriodo product= productList.get(position);
        holder.tvFolio.setText(product.getNo_contrato());
        holder.tvMonto.setText("$"+product.getMonto()+".00");
        holder.tvFecha.setText("Fecha: "+product.getFecha());
    }

    @Override
    public int getItemCount()
    {
        if(productList != null)
        {
            return productList.size();
        }
        else
        {
            return 0;
        }

    }

    class ProductViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvMonto, tvFolio, tvFecha;

        public ProductViewHolder(View itemView)
        {
            super(itemView);
            tvMonto=itemView.findViewById(R.id.tvMonto);
            tvFolio=itemView.findViewById(R.id.tvFolio);
            tvFecha=itemView.findViewById(R.id.tvFecha);
        }
    }
}
