package com.jaguarlabs.pabs.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jaguarlabs.pabs.R;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emmanuel Rodriguez on 16/07/2018.
 */
public class PreviousPeriodsAdapter extends RecyclerView.Adapter<PreviousPeriodsAdapter.PreviousPeriodsViewHolder> {


    private ItemClickListener itemClickListenrer;
    private List<Integer> periodos;

    private Context context;

    public class PreviousPeriodsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView txtPeriodo;
        private ItemClickListener itemListener;

        private PreviousPeriodsViewHolder(View itemView, ItemClickListener listener)
        {
            super(itemView);
            txtPeriodo = itemView.findViewById(R.id.txtPeriodo);

            this.itemListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.onItemClick(getItem(getAdapterPosition()), getAdapterPosition());
        }
    }

    public PreviousPeriodsAdapter(Context context, List<Integer> data, ItemClickListener itemClickListener)
    {
        this.itemClickListenrer = itemClickListener;
        this.periodos = new ArrayList<>();

        this.periodos.addAll(data);

        this.context = context;
    }

    @NonNull
    @Override
    public PreviousPeriodsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.previos_periods_row, parent, false);

        return new PreviousPeriodsViewHolder(itemView, itemClickListenrer);
    }

    @Override
    public void onBindViewHolder(@NonNull PreviousPeriodsViewHolder holder, int position) {
        holder.txtPeriodo.setText(context.getResources().getString(R.string.previous_periods, getItem(position)));
    }

    protected Integer getItem(int position) {
        return periodos.get(position);
    }

    @Override
    public int getItemCount() {
        return periodos.size();
    }

    public interface ItemClickListener
    {
        void onItemClick(int periodo, int position);
    }
}
