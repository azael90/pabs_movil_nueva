package com.jaguarlabs.pabs.adapter;

import android.arch.paging.DataSource;
import android.content.Context;

import com.jaguarlabs.pabs.database.Notifications;

/**
 * Created by Emmanuel Rodriguez on 23/07/2018.
 */
public class NotificationsDataSourceFactory extends DataSource.Factory<String, Notifications> {

    private Context context;
    private int no_cobrador, pagina;

    public NotificationsDataSourceFactory(Context context, int no_cobrador, int pagina)
    {
        this.context = context;
        this.no_cobrador = no_cobrador;
        this.pagina = pagina;
    }

    @Override
    public DataSource<String, Notifications> create() {
        return new NotificationsDataSource(context, no_cobrador, pagina);
    }
}
