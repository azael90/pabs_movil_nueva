package com.jaguarlabs.pabs.adapter;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.database.Clients;
import com.jaguarlabs.pabs.database.SyncedWallet;
import com.jaguarlabs.pabs.models.ModelClient;
import com.jaguarlabs.pabs.util.BuildConfig;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emmanuel Rodriguez on 06/07/2018.
 */
public class ClientesRecyclerAdapter<CLASS_RESPONSE> extends RecyclerView.Adapter<ClientesRecyclerAdapter.ClientesViewHolder> implements Filterable {

    private List<CLASS_RESPONSE> clientsList;
    private List<CLASS_RESPONSE> clientListSynced;
    private List<CLASS_RESPONSE> clientListFiltered;

    private int colorDefaultStrong;
    private int colorDefaultLight;
    private int colorPendingContract;
    private int colorPendingContractStrong;
    private int colorPendingLevel2Contract;
    private int colorPendingLevel2ContractStrong;
    private int colorTodayPayment;
    private int colorTodayPaymentStrong;
    private int colorWithoutAnalysisContract;
    private int colorWithoutAnalysisContractStrong;
    private int colorWithoutAnalysisSuspendedContract;
    private int colorWithoutAnalysisSuspendedContractStrong;
    private int colorFirstPaymentDate;
    private int colorFirstPaymentDateBlack;

    private String listType;

    private DecimalFormatSymbols nf;
    private DecimalFormat df;
    private Location myLocation;

    public static final String FILTERED_CLIENTS = "filtered_clients";
    public static final String NEARBY_CLIENTS = "nearby_clients";
    public static final String WALLET_CLIENTS = "wallet_clients";
    public static final String DAY_WALLET_CLIENTS = "day_wallet_clients";

    private ItemClickListener itemListener;

    public class ClientesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView txtContrato, txtCliente, txtMonto, txtPago;
        private ItemClickListener itemListener;
        private TextView firstPaymentDate;
        private ImageView ivBackgroundFPD;

        public ClientesViewHolder(View itemView, ItemClickListener listener)
        {
            super(itemView);
            txtContrato = itemView.findViewById(R.id.txtContrato);
            txtCliente = itemView.findViewById(R.id.txtCliente);
            txtMonto = itemView.findViewById(R.id.txtMonto);
            txtPago = itemView.findViewById(R.id.txtPago);
            firstPaymentDate = itemView.findViewById(R.id.tvFirstPaymentDate2);
            ivBackgroundFPD = itemView.findViewById(R.id.ivBackgroundFirstPaymentDate2);

            this.itemListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS))) {
                SyncedWallet item = (SyncedWallet) getItem(getAdapterPosition());
                this.itemListener.onItemClick(item, getAdapterPosition());
            }
            else
            {
                Clients item = (Clients) getItem(getAdapterPosition());
                this.itemListener.onItemClick(item, getAdapterPosition());
            }
        }
    }

    public ClientesRecyclerAdapter(Context context, List<CLASS_RESPONSE> data, String listType, ItemClickListener listener)
    {

        this.itemListener = listener;
        nf = new DecimalFormatSymbols();
        df = new DecimalFormat("#.##",nf);

        if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS))){
            this.clientListSynced = data;
            //this.clientListFiltered = data;
        }
        else {
            this.clientsList = data;
            //this.clientListFiltered = data;
        }

        this.clientListFiltered = new ArrayList<>();

        if (data
                != null)
            this.clientListFiltered.addAll(data);

        this.listType = listType;

        this.colorDefaultStrong = context.getResources().getColor(R.color.liststrong);
        this.colorDefaultLight = context.getResources().getColor(R.color.listlight);
        this.colorPendingContract = context.getResources().getColor(R.color.color_visit_done);
        this.colorPendingContractStrong = context.getResources().getColor(R.color.color_visit_done_strong);;
        this.colorPendingLevel2Contract = context.getResources().getColor(R.color.color_payment_done);
        this.colorPendingLevel2ContractStrong = context.getResources().getColor(R.color.color_payment_done_strong);
        this.colorTodayPayment = context.getResources().getColor(R.color.color_today_payment);
        this.colorTodayPaymentStrong = context.getResources().getColor(R.color.color_today_payment_strong);
        this.colorWithoutAnalysisContract = context.getResources().getColor(R.color.color_without_analysis_contract);
        this.colorWithoutAnalysisContractStrong = context.getResources().getColor(R.color.color_without_analysis_contract_strong);
        this.colorWithoutAnalysisSuspendedContract = context.getResources().getColor(R.color.color_without_analysis_contract_suspended);
        this.colorWithoutAnalysisSuspendedContractStrong = context.getResources().getColor(R.color.color_without_analysis_contract_suspended_strong);
        this.colorFirstPaymentDate = context.getResources().getColor(R.color.color_first_payment_date);
        this.colorFirstPaymentDateBlack = context.getResources().getColor(R.color.black);
    }

    public ClientesRecyclerAdapter(Context context, List<CLASS_RESPONSE> clientsList, Location myLocation, String listType, ItemClickListener listener)
    {
        this(context, clientsList, listType, listener);
        this.myLocation = myLocation;
    }

    @NonNull
    @Override
    public ClientesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.client_list_row, parent, false);

        return new ClientesViewHolder(itemView, itemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientesRecyclerAdapter.ClientesViewHolder holder, int position) {
        Clients client = new Clients();
        SyncedWallet clientFromSyncedWallet = new SyncedWallet();
        ModelClient clientNearbyFromSyncedWallet;

        if (BuildConfig.isWalletSynchronizationEnabled() && listType.equals(DAY_WALLET_CLIENTS)){
            clientFromSyncedWallet = (SyncedWallet)getItem(position);
            //clientFromSyncedWallet = new SyncedWallet((SyncedWallet)clientListSynced.get(position));
        }
        else if (BuildConfig.isWalletSynchronizationEnabled() && listType.equals(NEARBY_CLIENTS)){
            clientFromSyncedWallet = ((SyncedWallet)getItem(position));

            clientFromSyncedWallet.setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);
            //)clientFromSyncedWallet.setClient(clientNearbyFromSyncedWallet);
        }
        else {
            client = (Clients)getItem(position);
        }

        if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS))){

            //hide first payment date mask

            holder.txtContrato.setTextColor(Color.WHITE);
            holder.txtCliente.setTextColor(Color.WHITE);
            holder.txtMonto.setTextColor(Color.WHITE);
            holder.txtPago.setTextColor(Color.WHITE);

            //TODO: AFTER
            //set view's color
            switch (clientFromSyncedWallet.getColor()){
                case SyncedWallet.COLOR_DEFAULT_CONTRACT:
                    if (position % 2 == 1) {
                        holder.itemView.setBackgroundColor(colorDefaultStrong);
                    } else {
                        holder.itemView.setBackgroundColor(colorDefaultLight);
                    }
                    break;
                case SyncedWallet.COLOR_PENDING_CONTRACT:
                    if (position % 2 == 1) {
                        holder.itemView.setBackgroundColor(colorPendingContractStrong);
                    } else {
                        holder.itemView.setBackgroundColor(colorPendingContract);
                    }
                    break;
                case SyncedWallet.COLOR_PENDING_LEVEL_2_CONTRACT:
                    if (position % 2 == 1) {
                        holder.itemView.setBackgroundColor(colorPendingLevel2ContractStrong);
                    } else {
                        holder.itemView.setBackgroundColor(colorPendingLevel2Contract);
                    }
                    break;
                case SyncedWallet.COLOR_TODAY_CONTRACT:
                    if (position % 2 == 1) {
                        holder.itemView.setBackgroundColor(colorTodayPaymentStrong);
                    } else {
                        holder.itemView.setBackgroundColor(colorTodayPayment);
                    }
                    break;
                case SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT:
                    if ( clientFromSyncedWallet.isFPA() && !clientFromSyncedWallet.isFPAPending() ){
                        //Util.Log.ih("add black FPA");
                        //Util.Log.ih("clientFromSyncedWallet.isFPA() = " + clientFromSyncedWallet.isFPA());
                        //Util.Log.ih("!clientFromSyncedWallet.isFPAPending() = " + !clientFromSyncedWallet.isFPAPending());
                        //add FPA
                        holder.firstPaymentDate.setText( clientFromSyncedWallet.getClient().getFirstPaymentDate() );
                        holder.firstPaymentDate.setTextColor(colorFirstPaymentDateBlack);
                        //holder.ivBackgroundFPD.setVisibility(View.VISIBLE);
                        holder.firstPaymentDate.setVisibility(View.VISIBLE);
                    } else if ( clientFromSyncedWallet.isFPAPending() ){
                        //add pending FPA
                        //Util.Log.ih("add red FPA");
                        //Util.Log.ih("clientFromSyncedWallet.isFPAPending() = " + clientFromSyncedWallet.isFPAPending());
                        holder.firstPaymentDate.setText( clientFromSyncedWallet.getClient().getFirstPaymentDate() );
                        holder.firstPaymentDate.setTextColor(colorFirstPaymentDate);
                        //holder.ivBackgroundFPD.setVisibility(View.VISIBLE);
                        holder.firstPaymentDate.setVisibility(View.VISIBLE);
                    }

                    if (position % 2 == 1) {
                        holder.itemView.setBackgroundColor(colorWithoutAnalysisContractStrong);
                    } else {
                        holder.itemView.setBackgroundColor(colorWithoutAnalysisContract);
                    }

                    break;
                case SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT:
                    holder.txtContrato.setTextColor(Color.BLACK);
                    holder.txtCliente.setTextColor(Color.BLACK);
                    holder.txtMonto.setTextColor(Color.BLACK);
                    holder.txtPago.setTextColor(Color.BLACK);
                    if (position % 2 == 1) {
                        holder.itemView.setBackgroundColor(colorWithoutAnalysisSuspendedContractStrong);
                    } else {
                        holder.itemView.setBackgroundColor(colorWithoutAnalysisSuspendedContract);
                    }
            }

            //Util.Log.ih("clientFromSyncedWallet 3 = " + clientFromSyncedWallet);

        }
        else {
            //set view's color
            if (position % 2 == 1) {
                holder.itemView.setBackgroundColor(colorDefaultStrong);
            } else {
                holder.itemView.setBackgroundColor(colorDefaultLight);
            }
        }

        if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS))){

            //Util.Log.ih("clientFromSyncedWallet 6 = " + clientFromSyncedWallet.getClient().getName());

            //set id
            holder.txtContrato.setText(clientFromSyncedWallet.getClient().getSerie() + clientFromSyncedWallet.getClient().getNumberContract());
            //set client name
            holder.txtCliente.setText(clientFromSyncedWallet.getClient().getName() + " " + clientFromSyncedWallet.getClient().getFirstLastName() + " " + clientFromSyncedWallet.getClient().getSecondLastName());
            //set amount that client pays
            holder.txtMonto.setText("$" + formatMoney(Double.parseDouble(clientFromSyncedWallet.getClient().getActualPayment())));
            //set distance to client when needed
            if (myLocation != null) {
                //viewHolder.distance.setText("" + formatMoney(Double.parseDouble(clientFromSyncedWallet.getClient().getDistance())) + "m");
            } else {
                //view.findViewById(R.id.distancia_layout).setVisibility(View.GONE);
                /*viewHolder.linearLayoutDistance.setVisibility(View.GONE);
                viewHolder.amount.setGravity(Gravity.CENTER);
                viewHolder.paymentOption.setGravity(Gravity.CENTER);*/
            }
            //set payment option
            //- weekly
            //- fortnightly
            //- monthly
            String paymentOption = "";
            switch (Integer.parseInt(clientFromSyncedWallet.getClient().getPaymentOption())) {
                case 1:
                    paymentOption = "Sem";
                    break;
                case 2:
                    paymentOption = "Quin";
                    break;
                case 3:
                    paymentOption = "Men";
                    break;
                default:
                    break;
            }
            holder.txtPago.setText(paymentOption);
        }
        else {
            //set id
            holder.txtContrato.setText(client.getSerie() + client.getNumberContract());
            //set client name
            holder.txtCliente.setText(client.getName() + " " + client.getFirstLastName() + " " + client.getSecondLastName());
            //set amount that client pays
            holder.txtMonto.setText("$" + formatMoney(Double.parseDouble(client.getActualPayment())));
            //set distance to client when needed
            if (myLocation != null) {
                //viewHolder.distance.setText("" + formatMoney(Double.parseDouble(client.getDistance())) + "m");
            } else {
                //view.findViewById(R.id.distancia_layout).setVisibility(View.GONE);
                /*viewHolder.linearLayoutDistance.setVisibility(View.GONE);
                viewHolder.amount.setGravity(Gravity.CENTER);
                viewHolder.paymentOption.setGravity(Gravity.CENTER);*/
            }
            //set payment option
            //- weekly
            //- fortnightly
            //- monthly
            String paymentOption = "";
            switch (Integer.parseInt(client.getPaymentOption())) {
                case 1:
                    paymentOption = "Sem";
                    break;
                case 2:
                    paymentOption = "Quin";
                    break;
                case 3:
                    paymentOption = "Men";
                    break;
                default:
                    break;
            }
            holder.txtPago.setText(paymentOption);
        }
    }

    public void addItem(CLASS_RESPONSE item)
    {
        clientListFiltered.add(item);
        notifyItemInserted(clientListFiltered.size());
    }

    public void addItem(int position, CLASS_RESPONSE item)
    {
        clientListFiltered.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position)
    {
        clientListFiltered.remove(position);
        notifyItemRemoved(position);
    }

    public CLASS_RESPONSE getItem(int position)
    {
        if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS)))
            return clientListFiltered.get(position);
        else
            return clientListFiltered.get(position);
    }

    public List<CLASS_RESPONSE> getItemList()
    {
        return clientListFiltered;
    }

    public void setItemList(List<CLASS_RESPONSE> data)
    {
        clientListFiltered = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        /*if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS))){
            if (clientListFiltered == null) {
                return 0;
            }
            //return clientListSynced.size() > 500 ? 500 : clientListSynced.size();
            return clientListFiltered.size();
        }
        else {
            if (clientListFiltered == null) {
                return 0;
            }
            return clientListFiltered.size() > 500 ? 500 : clientListFiltered.size();
        }*/

        return clientListFiltered.size() > 500 ? 500 : clientListFiltered.size();
    }

    @Override
    public long getItemId(int position) {
        if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS))){
            if (clientListFiltered == null) {
                return 0;
            }
            //return clientListSynced.size() > 500 ? 500 : clientListSynced.size();
            SyncedWallet client = (SyncedWallet)clientListFiltered.get(position);
            return Long.parseLong(client.getContractID());
        }
        else {
            if (clientListFiltered == null) {
                return 0;
            }
            Clients client = (Clients)clientListFiltered.get(position);
            return Long.parseLong(client.getIdContract());
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                List<CLASS_RESPONSE> filteredList = new ArrayList<>();
                if (charString.isEmpty())
                {
                    if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS)))
                        clientListFiltered = clientListSynced;
                    else
                        clientListFiltered = clientsList;

                    notifyDataSetChanged();
                }
                else
                {
                    if (BuildConfig.isWalletSynchronizationEnabled() && (listType.equals(DAY_WALLET_CLIENTS) || listType.equals(NEARBY_CLIENTS)))
                    {
                        for (CLASS_RESPONSE row : clientListSynced)
                        {
                            SyncedWallet client = (SyncedWallet) row;

                            if (client.getClient().getNumberContract().toLowerCase().contains(charString.toLowerCase()) || client.getClient().getName().toLowerCase().contains(charString.toLowerCase())
                                    || client.getClient().getStreetToPay().toLowerCase().contains(charString.toLowerCase()))
                                filteredList.add(row);
                        }
                    }
                    else
                    {
                        for (CLASS_RESPONSE row : clientsList)
                        {
                            Clients client = (Clients) row;

                            if (client.getNumberContract().toLowerCase().contains(charString.toLowerCase()) || client.getName().toLowerCase().contains(charString.toLowerCase())
                                    || client.getStreetToPay().toLowerCase().contains(charString.toLowerCase()))
                                filteredList.add(row);
                        }
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                clientListFiltered = (ArrayList<CLASS_RESPONSE>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    private String formatMoney(double money) {
        nf.setDecimalSeparator('.');
        df = new DecimalFormat("#.##", nf);
        df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);
        return df.format(money);
    }

    public interface ItemClickListener{
        void onItemClick(Clients item, int position);
        void onItemClick(SyncedWallet item, int position);
    }
}
