package com.jaguarlabs.pabs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.rest.models.ModelNoVisitadosResponse;
import com.jaguarlabs.pabs.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Jordan Alvarez on 12/06/2019.
 */
public class NoVisitadosAdapter extends BaseAdapter {

    private Context context;

    private JSONArray noVisitadosArray;
    List<ModelNoVisitadosResponse.ContractsNotVisited> contractsNotVisited;

    private LayoutInflater inflater;

    public NoVisitadosAdapter( Context context, JSONArray noVisitadosArray ) {
        this.context = context;
        this.noVisitadosArray = noVisitadosArray;

        this.inflater = LayoutInflater.from( context );
    }

    public NoVisitadosAdapter( Context context, List<ModelNoVisitadosResponse.ContractsNotVisited> contractsNotVisited ) {
        this.context = context;
        this.contractsNotVisited = contractsNotVisited;

        this.inflater = LayoutInflater.from( context );
    }

    @Override
    public int getCount() {
        return contractsNotVisited.size();
    }


    @Override
    public Object getItem(int position) {
        return contractsNotVisited.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;

    }

    @Override
    public View getView( final int position, View convertView, ViewGroup parent ) {

        //make a view for each payment or visit
        View view = convertView;
        ViewHolder viewHolder;
        if (view == null) {

            view = inflater.inflate(R.layout.row_no_visitados, null);

            viewHolder = new ViewHolder();

            viewHolder.tvCompany = (TextView) view.findViewById(R.id.tv_company);
            viewHolder.tvContractNumber = (TextView) view.findViewById(R.id.tv_contractNumber);
            viewHolder.tvclientName = (TextView) view.findViewById(R.id.tv_clientName);
            viewHolder.tvPaymentAmount = (TextView) view.findViewById(R.id.tv_paymentAmount);
            viewHolder.tvPaymentForm = (TextView) view.findViewById(R.id.tv_paymentForm);
            viewHolder.tvfirstPaymentDate = (TextView) view.findViewById(R.id.tv_firstPaymentDate);
            viewHolder.tvcontractAddress = (TextView) view.findViewById(R.id.tv_contractAddress);
            viewHolder.ll_paymentForm = (LinearLayout) view.findViewById(R.id.ll_paymentForm);
            viewHolder.tvFechaUltimoAbono=(TextView) view.findViewById(R.id.tvFechaUltimoAbono);

            view.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) view.getTag();
        }

        //populate each view's textviews with info of payments and visits made
        try {
            ModelNoVisitadosResponse.ContractsNotVisited singleContractNotVisited = contractsNotVisited.get(position);

            //set company name
            String paymentForm = singleContractNotVisited.getPaymentForm();
            //String nombreEmpresa = DatabaseAssistant.getSingleCompany(indexEmpresa).getName();

            viewHolder.tvCompany.setText(paymentForm);

            //set contract number
            viewHolder.tvContractNumber.setText(singleContractNotVisited.getContractNumber());

            //set client name
            if (!singleContractNotVisited.getClientName().equals("null")) {
                viewHolder.tvclientName.setText(singleContractNotVisited.getClientName());
            } else {
                viewHolder.tvclientName.setText("Sin Datos");
            }

            //set amount paid
            viewHolder.tvPaymentAmount.setText("$" + singleContractNotVisited.getPaymentAmount());

            //set payment form
            viewHolder.tvPaymentForm.setText(singleContractNotVisited.getPaymentForm());
            viewHolder.ll_paymentForm.setVisibility(View.GONE);

            //set first payment date
            viewHolder.tvfirstPaymentDate.setText(singleContractNotVisited.getFirstPaymentDate());

            //set address
            viewHolder.tvcontractAddress.setText(singleContractNotVisited.getAddress());

            viewHolder.tvFechaUltimoAbono.setText(singleContractNotVisited.getFecha_ultimo_abono());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private class ViewHolder {
        private TextView tvCompany;
        private TextView tvContractNumber;
        private TextView tvclientName;
        private TextView tvPaymentAmount;
        private TextView tvPaymentForm;
        private TextView tvfirstPaymentDate;
        private TextView tvcontractAddress;
        private TextView tvFechaUltimoAbono;
        private LinearLayout ll_paymentForm;
        private FrameLayout fl_lv;
    }

}
