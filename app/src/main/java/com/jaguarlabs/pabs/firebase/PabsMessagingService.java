package com.jaguarlabs.pabs.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.activities.Notificaciones;

import java.util.Map;

/**
 * Created by Emmanuel Rodriguez on 25/10/2017.
 */

public class PabsMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        showNotification(remoteMessage.getData());
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d("NEW_TOKEN", s);
    }

    private void showNotification(Map<String, String> data)
    {
        if (data.get("type").equals("notification")) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "Notificacion");

            builder.setContentTitle(data.get("title"));
            builder.setContentText(data.get("body"));
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(data.get("body")));
            builder.setSmallIcon(R.drawable.white_notification_icon);
            builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.icono_hdpi));
            builder.setVibrate(new long[]{500, 500, 0, 0, 500, 500});
            builder.setPriority(NotificationCompat.PRIORITY_MAX);
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

            if (notificationManager != null)
                notificationManager.notify(Integer.parseInt(data.get("notificationId")), builder.build());
        }
        else if (data.get("type").equals("action"))
        {
            if (!data.get("action").equals(""))
            {
                switch (data.get("action"))
                {
                    case "launch":
                        Intent activity = getPackageManager().getLaunchIntentForPackage("com.jaguarlabs.pabs");

                        startActivity(activity);
                        break;
                    case "deposit":
                        /*Intent intent = new Intent("depositResult");
                        sendBroadcast(intent);*/
                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "Notificacion");

                        builder.setContentTitle("Respuesta deposito");
                        builder.setContentText("Deposito correcto");
                        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(data.get("body")));
                        builder.setSmallIcon(R.drawable.white_notification_icon);
                        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.icono_hdpi));
                        builder.setVibrate(new long[]{0, 500, 200, 500});
                        builder.setPriority(NotificationCompat.PRIORITY_MAX);
                        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

                        if (notificationManager != null)
                            notificationManager.notify(Integer.parseInt(data.get("notificationId")), builder.build());
                        break;
                }
            }
        }
    }
}
