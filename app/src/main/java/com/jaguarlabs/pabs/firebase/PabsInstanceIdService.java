package com.jaguarlabs.pabs.firebase;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Emmanuel Rodriguez on 25/10/2017.
 */

public class PabsInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();

        saveToken(token);
    }

    private void saveToken(String token)
    {
        SharedPreferences preferences = getSharedPreferences("firebase", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Log.d("firebase token", token);

        editor.putString("token", token);
        editor.apply();
    }
}
