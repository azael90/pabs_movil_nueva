package com.jaguarlabs.pabs.location;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.jaguarlabs.pabs.database.Collectors;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.models.ModelLocations;
import com.jaguarlabs.pabs.util.LogRegister;
import com.jaguarlabs.pabs.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import pl.tajchert.nammu.Nammu;

/**
 * Class in charge of every Location based on GPS operation within app
 * @author Azael Jimenez
 * @version 18/12/2019
 */
public class LocationProvider implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private final String TAG = "LOCATIONPABS";
    private Context context;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation = null;
    private Location lastKnownLocation = null;
    private ArrayList<ModelLocations.GeoLocations> locations;
    private SimpleDateFormat dateFormatter;
    private Date date;

    private boolean requestLocationUpdates;

    public LocationProvider(Context context){
        this.context = context;

        requestLocationUpdates = false;

        mGoogleApiClient = new GoogleApiClient.Builder( this.context )
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        connectGoogleApiClient();

        locations = new ArrayList<>();
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    /**
     * tries to connect GoogleApiClient
     * if GoogleApiClient is ready to go
     */
    private void connectGoogleApiClient(){
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.context);
        if (resultCode == ConnectionResult.SUCCESS){
            mGoogleApiClient.connect();
        }
        else {
            Util.UI.toastL(context, "GooglePlayServices error: " + resultCode + "por favor actualiza GooglePlayServices");
        }
    }

    /**
     * retrieves last known location
     * @return
     *      Location instance
     */
    public Location getGPSPosition(){
        return mCurrentLocation;
    }

    public Location getLastKnownLocation(){
        return lastKnownLocation;
    }

    public void setLastKnownLocation(double latitude, double longitude){

        mCurrentLocation.setLatitude(latitude);
        mCurrentLocation.setLongitude(longitude);
        lastKnownLocation.setLatitude(latitude);
        lastKnownLocation.setLongitude(longitude);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "connected");
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(20000);
        mLocationRequest.setFastestInterval(15000);
    }

    public void swapRequestToNoPowerPriority(){
        mLocationRequest.setPriority(LocationRequest.PRIORITY_NO_POWER);
    }

    public void swapRequestToHighPriority(){
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "GoogleApiClient connection has been suspend");
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        if ( lastKnownLocation == null ){
            lastKnownLocation = location;
        }

        Log.i(TAG, "Ubicación obtenida: " + location);

        ModelLocations.GeoLocations geo = new ModelLocations.GeoLocations()
                .setLatitude(location.getLatitude())
                .setLongitude(location.getLongitude())
                .setDate(dateFormatter.format(date = new Date()));
        locations.add(geo);

        List<Collectors> collector = Collectors.listAll(Collectors.class);
        if (collector.size() > 0)
        {
            Random rnd = new Random();

            DatabaseAssistant.insertarLocationsNow(
                    collector.get(0).getNumberCollector(),
                    dateFormatter.format(date = new Date()),
                    String.valueOf(mCurrentLocation.getLatitude()),
                    String.valueOf(mCurrentLocation.getLongitude()));
        }
        else
        {
            Log.d("FECHA CAPTURADA: ", "No se capturo la localización con la fecha: "+ dateFormatter.format(date = new Date()));
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "GoogleApiClient connection has failed");
    }

    public void createGeoJSONFile(String periodo, String collectorNumber) {
        LogRegister.createGeoJSONDataFile(locations, periodo, collectorNumber);
        locations.clear();
    }

    public void startLocationUpdates()
    {
        if (mGoogleApiClient.isConnected() && Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION) && !requestLocationUpdates) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            requestLocationUpdates = true;
        }
    }

    public void stopLocationUpdates()
    {
        if (requestLocationUpdates) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            requestLocationUpdates = false;
        }
    }
}
