package com.jaguarlabs.pabs.location;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.jaguarlabs.pabs.database.Collectors;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.models.ModelLocations;
import com.jaguarlabs.pabs.util.LogRegister;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import pl.tajchert.nammu.Nammu;


public class FusedLocationProvider
{

    private static final int UPDATE_INTERVAL = 20000;
    private static final int FASTEST_UPDATE_INTERVAL = 15000;
    private static final String TAG = "PABS";
    private FusedLocationProviderClient mFusedLocationProvider;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation = null;
    private boolean mRequestLocationUpdates;
    private ArrayList<ModelLocations.GeoLocations> locations;
    private LocalDateTime date;

    public FusedLocationProvider(Context context)
    {
        mRequestLocationUpdates = false;
        locations = new ArrayList<>();
        mFusedLocationProvider = LocationServices.getFusedLocationProviderClient(context);
        mSettingsClient = LocationServices.getSettingsClient(context);
        createLocationRequest();
        createLocationCallback();
        buildLocationSettingsRequest();
    }

    private void createLocationRequest()
    {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void createLocationCallback()
    {
        mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Log.i(TAG, "Ubicación obtenida: " + locationResult.getLastLocation());
                mCurrentLocation = locationResult.getLastLocation();
                date = LocalDateTime.now();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String fechaComoCadena = sdf.format(new Date());

                ModelLocations.GeoLocations geo = new ModelLocations.GeoLocations()
                        .setLatitude(locationResult.getLastLocation().getLatitude())
                        .setLongitude(locationResult.getLastLocation().getLongitude())
                        .setDate(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").print(date));
                locations.add(geo);

                List<Collectors> collector = Collectors.listAll(Collectors.class);
                if (collector.size() > 0)
                {
                    Random rnd = new Random();

                    DatabaseAssistant.insertarLocationsNow(
                            collector.get(0).getNumberCollector(),
                            fechaComoCadena,
                            String.valueOf(mCurrentLocation.getLatitude()),
                            String.valueOf(mCurrentLocation.getLongitude()));
                }
                else
                {
                    Log.d("FECHA CAPTURADA: ", fechaComoCadena);
                }
            }
        };
    }

    private void buildLocationSettingsRequest()
    {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    public void startLocationUpdates()
    {
        if (mRequestLocationUpdates)
            return;

        mSettingsClient.checkLocationSettings(mLocationSettingsRequest).addOnSuccessListener(locationSettingsResponse -> {
                    Log.i(TAG, "Obteniendo ubicaciones");
                    if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        mFusedLocationProvider.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        mRequestLocationUpdates = true;
                    }
                })
                .addOnFailureListener(e -> mRequestLocationUpdates = false);
    }

    public void stopLocationUpdates()
    {
        if (!mRequestLocationUpdates)
        {
            return;
        }
        mFusedLocationProvider.removeLocationUpdates(mLocationCallback).addOnCompleteListener(task -> {
                    Log.i(TAG, "Detenido la obtencion de ubicaciones");
                    mRequestLocationUpdates = false;
                });
    }

    public Location getGPSPosition()
    {
        return mCurrentLocation;
    }

    public Location getLastKnownLocation()
    {
        return mCurrentLocation;
    }

    public void setLastKnownLocation(double latitude, double longitude){

        mCurrentLocation.setLatitude(latitude);
        mCurrentLocation.setLongitude(longitude);
    }

    public void createGeoJSONFile(String periodo, String collectorNumber) {
        LogRegister.createGeoJSONDataFile(locations, periodo, collectorNumber);
        locations.clear();
    }

}
