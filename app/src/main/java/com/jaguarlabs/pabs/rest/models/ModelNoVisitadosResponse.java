package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jordan Alvarez on 24/06/2019.
 */
public class ModelNoVisitadosResponse {

    @SerializedName("result")
    private ArrayList<ContractsNotVisited> result;
    @SerializedName("error")
    private String error;

    //Agregar las llaves del JSON para recuperarlos
    @SerializedName("startDateW")
    private String startDateW;
    @SerializedName("endDateW")
    private String endDateW;
    @SerializedName("startDateF")
    private String startDateF;
    @SerializedName("endDateF")
    private String endDateF;
    @SerializedName("startDateM")
    private String startDateM;
    @SerializedName("endDateM")
    private String endDateM;



    public ArrayList<ContractsNotVisited> getResult() {
        return result;
    }

    public void setResult(ArrayList<ContractsNotVisited> result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public class ContractsNotVisited {

        @SerializedName("frec")
        private String paymentForm;
        @SerializedName("Contrato")
        private String contractNumber;
        @SerializedName("fecha_primer_abono")
        private String firstPaymentDate;
        @SerializedName("Cliente")
        private String clientName;
        @SerializedName("Domicilio")
        private String address;
        @SerializedName("monto_abono")
        private String paymentAmount;
        @SerializedName("fecha_ultimo_abono")
        private String fecha_ultimo_abono;


        /**
         * .........................................
         * .          GETTERS AND SETTERS          .
         * .........................................
         */

        public String getPaymentForm() {
            return paymentForm;
        }

        public void setPaymentForm(String paymentForm) {
            this.paymentForm = paymentForm;
        }

        public String getContractNumber() {
            return contractNumber;
        }

        public void setContractNumber(String contractNumber) {
            this.contractNumber = contractNumber;
        }

        public String getFirstPaymentDate() {
            return firstPaymentDate;
        }

        public void setFirstPaymentDate(String firstPaymentDate) {
            this.firstPaymentDate = firstPaymentDate;
        }

        public String getClientName() {
            return clientName;
        }

        public void setClientName(String clientName) {
            this.clientName = clientName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPaymentAmount() {
            return paymentAmount;
        }

        public void setPaymentAmount(String paymentAmount) {
            this.paymentAmount = paymentAmount;
        }

        public String getFecha_ultimo_abono() {
            return fecha_ultimo_abono;
        }

        public void setFecha_ultimo_abono(String fecha_ultimo_abono) {
            this.fecha_ultimo_abono = fecha_ultimo_abono;
        }

    }


    public String getStartDateW() {
        return startDateW;
    }

    public void setStartDateW(String startDateW) {
        this.startDateW = startDateW;
    }

    public String getEndDateW() {
        return endDateW;
    }

    public void setEndDateW(String endDateW) {
        this.endDateW = endDateW;
    }

    public String getStartDateF() {
        return startDateF;
    }

    public void setStartDateF(String startDateF) {
        this.startDateF = startDateF;
    }

    public String getEndDateF() {
        return endDateF;
    }

    public void setEndDateF(String endDateF) {
        this.endDateF = endDateF;
    }

    public String getStartDateM() {
        return startDateM;
    }

    public void setStartDateM(String startDateM) {
        this.startDateM = startDateM;
    }

    public String getEndDateM() {
        return endDateM;
    }

    public void setEndDateM(String endDateM) {
        this.endDateM = endDateM;
    }


}
