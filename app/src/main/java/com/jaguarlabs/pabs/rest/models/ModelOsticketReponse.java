package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jordan Alvarez on 4/21/2017.
 */

public class ModelOsticketReponse {

    @SerializedName("resultado")
    private List<OsticketTicketsAdded> result;
    @SerializedName("error")
    private String error;

    /**
     * .........................................
     * .          GETTERS AND SETTERS          .
     * .........................................
     */

    public List<OsticketTicketsAdded> getResult() {
        return result;
    }

    public void setResult( List<OsticketTicketsAdded> result ) {
        this.result = result;
    }


    public class OsticketTicketsAdded {

        @SerializedName("topic")
        String topic;
        @SerializedName("mensaje")
        String message;
        @SerializedName("date_milliseconds")
        long dateMilliseconds;

        /**
         * .........................................
         * .          GETTERS AND SETTERS          .
         * .........................................
         */

        public String getTopic() {
            return topic;
        }

        public void setTopic(String topic) {
            this.topic = topic;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public long getDateMilliseconds() {
            return dateMilliseconds;
        }

        public void setDateMilliseconds(long dateMilliseconds) {
            this.dateMilliseconds = dateMilliseconds;
        }
    }
}
