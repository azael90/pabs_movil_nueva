package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

/**
 * Model response POJO class for cancelling a payment.
 *
 * @author Jordan Alvarez
 * @version 22/06/15
 */
public class ModelCancelPaymentResponse {

    @SerializedName("result")
    private Result result;
    @SerializedName("efectivo")
    private int efectivo;
    @SerializedName("error")
    private String error;

    /**
     * .........................................
     * .          GETTERS AND SETTERS          .
     * .........................................
     */

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public int getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(int efectivo) {
        this.efectivo = efectivo;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    /**
     * Inner class that will work as a json object
     */
    public class Result{

        @SerializedName("no_cobro")
        private String paymentNumber;
        @SerializedName("no_contrato")
        private String contractNumber;
        @SerializedName("no_cobrador")
        private String collectorNumber;
        @SerializedName("no_ubicacion")
        private String ubicationNumber;
        @SerializedName("folio")
        private String folio;
        @SerializedName("copias")
        private String copies;
        @SerializedName("monto")
        private String amount;
        @SerializedName("status")
        private int status;
        @SerializedName("empresa")
        private String company;
        @SerializedName("exportado")
        private String migrated;

        /**
         * .........................................
         * .          GETTERS AND SETTERS          .
         * .........................................
         */

        public String getPaymentNumber() {
            return paymentNumber;
        }

        public void setPaymentNumber(String paymentNumber) {
            this.paymentNumber = paymentNumber;
        }

        public String getContractNumber() {
            return contractNumber;
        }

        public void setContractNumber(String contractNumber) {
            this.contractNumber = contractNumber;
        }

        public String getCollectorNumber() {
            return collectorNumber;
        }

        public void setCollectorNumber(String collectorNumber) {
            this.collectorNumber = collectorNumber;
        }

        public String getUbicationNumber() {
            return ubicationNumber;
        }

        public void setUbicationNumber(String ubicationNumber) {
            this.ubicationNumber = ubicationNumber;
        }

        public String getFolio() {
            return folio;
        }

        public void setFolio(String folio) {
            this.folio = folio;
        }

        public String getCopies() {
            return copies;
        }

        public void setCopies(String copies) {
            this.copies = copies;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getMigrated() {
            return migrated;
        }

        public void setMigrated(String migrated) {
            this.migrated = migrated;
        }
    }
}
