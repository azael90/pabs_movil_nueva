package com.jaguarlabs.pabs.rest.webService;

import com.jaguarlabs.pabs.rest.models.ModelNoVisitadosRequest;
import com.jaguarlabs.pabs.rest.models.ModelNoVisitadosResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Jordan Alvarez on 24/06/2019.
 */
public interface ContractsNotVisitedService {

    @POST("/controlnovisitados/getContractsNotVisited")
    public ModelNoVisitadosResponse requestSearchForClient(@Body ModelNoVisitadosRequest body);

}
