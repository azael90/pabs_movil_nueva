package com.jaguarlabs.pabs.rest.models;

import org.json.JSONObject;

/**
 * Created by jordan on 16/12/15.
 */
public class ModelPrintPaymentRequest {

    private JSONObject jsonInfo;
    private JSONObject datosCobrador;
    private String new_folio;
    private String empresaString;
    private String datoempresa1;
    private String datoempresa2;
    private String rfc;
    private String[] direccion;
    private String datoempresa6;
    private String datoempresa7;
    private int impreso;
    private String mensaje;
    private boolean imprimio;
    private String total;
    private String saldo;
    private String mensajepercapita;
    private String tituloPercapita;




    public JSONObject getJsonInfo() {
        return jsonInfo;
    }

    public void setJsonInfo(JSONObject jsonInfo) {
        this.jsonInfo = jsonInfo;
    }

    public JSONObject getDatosCobrador() {
        return datosCobrador;
    }

    public void setDatosCobrador(JSONObject datosCobrador) {
        this.datosCobrador = datosCobrador;
    }

    public String getNew_folio() {
        return new_folio;
    }

    public void setNew_folio(String new_folio) {
        this.new_folio = new_folio;
    }

    public String getEmpresaString() {
        return empresaString;
    }

    public void setEmpresaString(String empresaString) {
        this.empresaString = empresaString;
    }

    public String getDatoempresa1() {
        return datoempresa1;
    }

    public void setDatoempresa1(String datoempresa1) {
        this.datoempresa1 = datoempresa1;
    }

    public String getDatoempresa2() {
        return datoempresa2;
    }

    public void setDatoempresa2(String datoempresa2) {
        this.datoempresa2 = datoempresa2;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String[] getDireccion() {
        return direccion;
    }

    public void setDireccion(String[] direccion) {
        this.direccion = direccion;
    }

    public String getDatoempresa6() {
        return datoempresa6;
    }

    public void setDatoempresa6(String datoempresa6) {
        this.datoempresa6 = datoempresa6;
    }

    public String getDatoempresa7() {
        return datoempresa7;
    }

    public void setDatoempresa7(String datoempresa7) {
        this.datoempresa7 = datoempresa7;
    }

    public int getImpreso() {
        return impreso;
    }

    public void setImpreso(int impreso) {
        this.impreso = impreso;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public boolean isImprimio() {
        return imprimio;
    }

    public void setImprimio(boolean imprimio) {
        this.imprimio = imprimio;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }


    public String getMensajepercapita() {
        return mensajepercapita;
    }

    public void setMensajepercapita(String mensajepercapita) {
        this.mensajepercapita = mensajepercapita;
    }

    public String getTituloPercapita() {
        return tituloPercapita;
    }

    public void setTituloPercapita(String tituloPercapita) {
        this.tituloPercapita = tituloPercapita;
    }
}
