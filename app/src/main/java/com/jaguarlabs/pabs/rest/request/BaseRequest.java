package com.jaguarlabs.pabs.rest.request;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.octo.android.robospice.retry.DefaultRetryPolicy;
import com.octo.android.robospice.retry.RetryPolicy;

//TODO: DELETE THIS CLASS

/**
 * Created by cbriseno on 15/04/15.
 */
public abstract class BaseRequest<T, R> extends RetrofitSpiceRequest<T, R> {

    public BaseRequest(Class clazz, Class retrofitedInterfaceClass) {
        super(clazz, retrofitedInterfaceClass);
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy(0,0,1);
    }
}
