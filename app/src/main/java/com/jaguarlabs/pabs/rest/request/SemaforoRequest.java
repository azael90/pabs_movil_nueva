package com.jaguarlabs.pabs.rest.request;

import com.jaguarlabs.pabs.rest.models.ModelSemaforoRequest;
import com.jaguarlabs.pabs.rest.models.ModelSemaforoResponse;
import com.jaguarlabs.pabs.rest.webService.SemaforoService;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by jordan on 7/07/16.
 */
public class SemaforoRequest extends RetrofitSpiceRequest<ModelSemaforoResponse, SemaforoService> {

    ModelSemaforoRequest body;

    public SemaforoRequest(ModelSemaforoRequest body){
        super(ModelSemaforoResponse.class, SemaforoService.class);

        this.body = body;
    }

    @Override
    public ModelSemaforoResponse loadDataFromNetwork() throws Exception {
        Util.Log.ih("on loadDataFromNetwork");
        publishProgress(ConstantsPabs.START_PRINTING);
        return getService().requestSemaforo(body);
    }
}
