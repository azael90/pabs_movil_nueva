package com.jaguarlabs.pabs.rest.request.offline;

import android.bluetooth.BluetoothAdapter;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.util.Log;

import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinterHelper;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.rest.models.ModelPrintPaymentRequest;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.SharedConstants;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.SpiceRequest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

/**
 * Print visit ticket request class that runs in a different thread than UI thread.
 * Gets done in background.
 * it's used to print a visit ticket using an android service
 *
 * @author Jordan Alvarez
 * @version 12/12/15
 */
@SuppressWarnings("SpellCheckingInspection")
public class PrintVisitTicketRequest extends SpiceRequest<Boolean> {

    //private TSCActivity TscDll;
    private BluetoothPrinter TscDll;

    private Timer timerToCancelPrinting;

    private String collectorName;
    private String telefono;
    private String clientName;
    private String clientLastName;
    private String clientContractNumber;
    private String idCompany;
    private String businessNameFirstLine;
    private String businessNameSecondLine;
    private String clientSerie;
    private String no_cobrador;

    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat hourFormatter;
    private Date date;
    private Long dateAux;
   // private ModelPrintPaymentRequest modelPrintPaymentRequest;

    public PrintVisitTicketRequest(String collectorName, String clientName, String clientLastName, String clientContractNumber, String clientSerie, String idCompany, String businessNameFirstLine, String businessNameSecondLine, String telefono, String no_cobrador) {
        super(Boolean.class);

        Util.Log.ih("INSTANTIATING");

        //TscDll = new TSCActivity();

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        hourFormatter = new SimpleDateFormat("HH:mm");
        date = new Date();

        this.collectorName = collectorName;
        this.telefono = telefono;
        this.clientName = clientName;
        this.clientLastName = clientLastName;
        this.clientContractNumber = clientContractNumber;
        this.clientSerie = clientSerie;
        this.idCompany = idCompany;
        this.businessNameFirstLine = businessNameFirstLine;
        this.businessNameSecondLine = businessNameSecondLine;
        this.no_cobrador=no_cobrador;
        Util.Log.ih("FINISH INSTANTIATING");
    }

    @Override
    public Boolean loadDataFromNetwork() throws Exception {
        Util.Log.ih("in loadDataFromNetwork()");
        publishProgress(ConstantsPabs.START_PRINTING);

        //return true;
        return printVisitTicket();
    }

    /**
     * prints visit ticket
     *
     * @return
     * 		true -> succeed
     * 		false -> failed
     */
    private boolean printVisitTicket(){

        Util.Log.ih("in printVisitTicket()");
        /*
        try {
            Looper.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        */

        int cont = 0;

        while (cont < 5) {
            try {

                TscDll = BluetoothPrinter.getInstance();

                if (!BluetoothPrinter.printerConnected)
                    TscDll.connect();
                    //TscDll = new TSCPrinter();
                    //TscDll.openport(DatabaseAssistant.getPrinterMacAddress());
                /*
                dateAux = new Date().getTime();

                //timer that closes printer port if got stuck
                //trying to print
                //has one minute of limit before closing port
                //closing port causes NullPointerException on openPort()
                //so it to print tries again
                timerToCancelPrinting = new Timer();
                timerToCancelPrinting.schedule(new TimerTask() {

                    String mac;
                    int attempt;
                    TSCActivity tscDll;

                    @Override
                    public void run() {
                        if (checkTime()) {
                            Util.Log.ih("Closing port");
                            try {
                                tscDll.closeport();
                            } catch (Exception e){
                                e.printStackTrace();
                                timerToCancelPrinting.cancel();
                            }
                        }
                        Util.Log.ih("attempt: " + attempt);
                    }

                    public TimerTask setData(int attempt, TSCActivity tscDll) {
                        this.attempt = attempt;
                        this.tscDll = tscDll;
                        return this;
                    }
                }.setData(cont, TscDll), (1000 * 60));

                TscDll.openport(DatabaseAssistant.getPrinterMacAddress());
                */
                    Util.Log.ih("despues de openPort");


                    //Util.Log.ih("printKILL...");
                    //TscDll.flushOutStream();
                    //TscDll.printKILL();
                    //Util.Log.ih("printKILL DONE");

                    //timerToCancelPrinting.cancel();

                    TscDll.setup(SharedConstants.Printer.width,
                            165,
                            SharedConstants.Printer.speed,
                            SharedConstants.Printer.density,
                            SharedConstants.Printer.sensorType,
                            SharedConstants.Printer.gapBlackMarkVerticalDistance,
                            SharedConstants.Printer.gapBlackMarkShiftDistance);

                    //Log.i("PRINT STATUS", TscDll.status());

                    TscDll.clearbuffer();

                    TscDll.sendcommand("GAP 0,0\n");
                    TscDll.sendcommand("CLS\n");
                    TscDll.sendcommand("CODEPAGE UTF-8\n");
                    TscDll.sendcommand("SET TEAR ON\n");
                    TscDll.sendcommand("SET HEAD ON\n");
                    TscDll.sendcommand("SET REPRINT OFF\n");
                    TscDll.sendcommand("SET COUNTER @1 1\n");

                    for (int conter = 0; conter < 500; conter = conter + 30) {
                        TscDll.printerfont(conter, 0, "3", 0, 1, 1, "-");
                    }

                    String companynameMask = ExtendedActivity.getCompanyNameFormatted( businessNameFirstLine );

                    TscDll.printerfont(400, 45, "3", 0, 1, 1, dateFormatter.format(date));
                    TscDll.printerfont(300, 45, "3", 0, 1, 1, hourFormatter.format(date));
                    TscDll.printerfont(10, 115, "2", 0, 1, 1, companynameMask);
                    TscDll.printerfont(10, 160, "2", 0, 1, 1, businessNameSecondLine);

                /* La impresión de la dirección de la empresa soporta 3 lineas exactamente
                 * Se debe dividir la dirección en 3 lineas y cada linea debe de tener un máximo de 35 caracteres
                 * de lo contrario la información no se verá plasmada en el ticket
                 */
                    switch (com.jaguarlabs.pabs.util.BuildConfig.getTargetBranch()) {
                        case GUADALAJARA:
                            if (!idCompany.equals("05")) {
                                printDireccion("Av. Federalismo Norte 1485 Piso 1",
                                        "Oficina 3 Colonia San Miguel de",
                                        "Mezquitan CP 44260 Guadalajara");
                            } else {
                                printDireccion("WASHINGTON No. 404",
                                        "COL. LA AURORA",
                                        "C.P.44460, Guadalajara, Jalisco");
                            }
                            break;
                            /*
                            Av. Hidalgo No. 3402 , entre la calle Violeta y la calle Rosa en la Colonia Flores Tampico , Tamaulipas.
                            En el edificio Planta alta .
                            Entrada por Av. Hidalgo.
                            */
                        case TAMPICO:
                            if (idCompany.equals("03") || idCompany.equals("01")) {
                                printDireccion("AV. HIDALGO NO. 3402",
                                        "ENTRE CALLE VIOLETA Y CALLE ROSA,",
                                        "COLONIA FLORES TAMPICO, TAMAULIPAS");
                            }
                            break;

                        case TOLUCA:
                            //Las empresas con id 01 y 03 comparten la misma dirección
                            //Si se necesitara agregar más empresas entonces esta condición if debe soportar
                            //los idenfiticadores de esas nuevas emprsas para elegir qué dirección imprimir

                            if (idCompany.equals("03") || idCompany.equals("01")) {
                                printDireccion("PASEO TOLLOCAN 101",
                                        "COL. SANTA MARIA DE LAS ROSAS",
                                        "CP. 50140, TOLUCA");
                            }
                            break;
                        case QUERETARO:
                            //Las empresas con id 01 y 03 comparten la misma dirección
                            //Si se necesitara agregar más empresas entonces esta condición if debe soportar
                            //los idenfiticadores de esas nuevas emprsas para elegir qué dirección imprimir

                            if (idCompany.equals("03") || idCompany.equals("01")) {
                                printDireccion("2DA PRIVADA DE 20 DE NOVIEMBRE No. 15",
                                        "2DO PISO. COL. SAN FRANCISQUITO",
                                        "C.P. 76058, QUERÉTARO QUERÉTARO");
                            }
                            break;

                        case IRAPUATO:
                            if (idCompany.equals("03") || idCompany.equals("01")) { //"BLVD. DIAZ ORDAZ # 1370, M2 INTERIOR 2, C.P. 36660. JARDINES DE IRAPUATO, IRAPUATO, GUANAJUATO, MÉXICO",
                                printDireccion("BLVD. DIAZ ORDAZ # 1370, M2 INTERIOR 2",
                                        ", C.P. 36660. JARDINES DE IRAPUATO, ",
                                        ", IRAPUATO, GUANAJUATO, MÉXICO");
                            }
                            break;

                        case LEON:
                            if (idCompany.equals("03") || idCompany.equals("01")) { //"BLVD. DIAZ ORDAZ # 1370, M2 INTERIOR 2, C.P. 36660. JARDINES DE IRAPUATO, IRAPUATO, GUANAJUATO, MÉXICO",
                                printDireccion("NICARAGUA #917; LOCAL 12; 2DO NIVEL",
                                        ", VILLA RESIDENCIAL ARBIDE, ",
                                        ", C.P. 37366, LEON, GUANAJUATO, MÉXICO");
                            }
                            break;

                        case SALAMANCA:
                            if (idCompany.equals("03") || idCompany.equals("01")) { //"BLVD. DIAZ ORDAZ # 1370, M2 INTERIOR 2, C.P. 36660. JARDINES DE IRAPUATO, IRAPUATO, GUANAJUATO, MÉXICO",
                                printDireccion("JUAN ALDAMA # 1008; 2DO PISO, ",
                                        "C.P. 36860, CENTRO, SALAMANCA, ",
                                        "GUANAJUATO, MÉXICO");
                            }
                            break;

                        case CELAYA:
                            if (idCompany.equals("03") || idCompany.equals("01")) { //"PROLONGACIÓN HIDALGO #705-A, C.P. 38040, CENTRO, CELAYA, GUANAJUATO, MÉXICO,
                                printDireccion("PROLONGACIÓN HIDALGO #705-A, ",
                                        "C.P. 38040, CENTRO, CELAYA, ",
                                        "GUANAJUATO, MÉXICO");
                            }
                            break;

                        case PUEBLA:
                            if (idCompany.equals("03") || idCompany.equals("01")) {
                                printDireccion("FRANCISCO I. MADERO No. 428",
                                        "COL. SAN BALTAZAR CAMPECHE",
                                        "C.P. 72550, PUEBLA");
                            } else {
                                printDireccion("FRANCISCO I. MADERO No. 428",
                                        "COL. SAN BALTAZAR CAMPECHE",
                                        "C.P. 72550, PUEBLA");
                            }
                            break;
                        case CUERNAVACA:
                            if (idCompany.equals("03") || idCompany.equals("01")) {
                                printDireccion("FRANCISCO I. MADERO No. 719",
                                        "COL. MIRAVAL C.P. 62270. DELEGACIÓN",
                                        "BENITO JUÁREZ, CUERNAVACA, MÉXICO");
                            } else {
                                printDireccion("FRANCISCO I. MADERO No. 719",
                                        "COL. MIRAVAL C.P. 62270. DELEGACIÓN",
                                        "BENITO JUÁREZ, CUERNAVACA, MÉXICO");
                            }
                            break;
                        case NAYARIT:
                            if (idCompany.equals("04")) {
                                printDireccion("AV. MEXICO N° 393 NTE.",
                                        "CALLE NIÑOS HÉROES Nº 82",
                                        "JAVIER MINA Nº 69 ORIENTE",
                                        "COL. CENTRO",
                                        "CP. 63000, TEPIC, NAYARIT");
                            } else {
                                printDireccion("AV. MEXICO N° 393 NTE.",
                                        "COL. CENTRO",
                                        "CP. 63000, TEPIC, NAYARIT");
                            }
                            break;
                        case MERIDA:
                            if (idCompany.equals("01")) {
                                printDireccion("Calle 33 No. 503C",
                                        "Col. Alcala Martin",
                                        "CP 97000 Merida Yucatan, México.");
                            } else {
                                printDireccion("AV. MEXICO N° 393 NTE.",
                                        "COL. CENTRO",
                                        "CP. 63000, TEPIC, NAYARIT");
                            }
                            break;
                        case SALTILLO:
                            if (idCompany.equals("01")) {
                                printDireccion("BOULEVARD NAZARIO ORTIZ",
                                        "GARZA NO 1265 COL ALPES",
                                        "CP 25270  SALTILLO COAHUILA");
                            } else {
                                printDireccion("BOULEVARD NAZARIO ORTIZ",
                                        "GARZA NO 1265 COL ALPES",
                                        "CP 25270  SALTILLO COAHUILA");
                            }
                            break;
                        case CANCUN:
                            if (idCompany.equals("01")) {
                                printDireccion("CALLE CEDRO MZ. 41",
                                        "LOTE 1-08 SM. 311",
                                        "CANCÚN, QUINTANA ROO");
                            } else {
                                printDireccion("CALLE CEDRO MZ. 41",
                                        "LOTE 1-08 SM. 311",
                                        "CANCÚN, QUINTANA ROO");
                            }
                            break;
                        case MEXICALI:
                            printDireccion("Blvd Anahuac #800 Int.A",
                                        "Col. Esperanza C.P. 21140",
                                        "Mexicali, B.C., MÉXICO",
                                        "",
                                        "");
                            break;



                        case TORREON:
                            printDireccion("CLZ.MATIAS ROMAN RIOS #425",
                                    ", L-17, C.P 27000,PLAZA",
                                    "PABELLON HIDALGO,TORREON COAHUILA, MEXICO",
                                    "",
                                    "");
                            break;


                        case MORELIA:
                            if (idCompany.equals("03")) {
                                printDireccion("BELLA AURORA No. 600",
                                        "FRACC. JARDINES DE LA ASUNCION",
                                        "C.P. 58192 MORELIA, MORELIA");
                            } else {
                                printDireccion("AV. GUADALUPE VICTORIA",
                                        "No. 364 COL. CENTRO",
                                        "C.P. 58000 MORELIA, MORELIA");
                            }
                            break;
                    }

                    int y;

                    if ( BuildConfig.getTargetBranch() == BuildConfig.Branches.NAYARIT && idCompany.equals("04") || BuildConfig.getTargetBranch() == BuildConfig.Branches.SALTILLO && idCompany.equals("01") || BuildConfig.getTargetBranch() == BuildConfig.Branches.CANCUN && idCompany.equals("01") || BuildConfig.getTargetBranch() == BuildConfig.Branches.QUERETARO || BuildConfig.getTargetBranch()== BuildConfig.Branches.IRAPUATO
                            || BuildConfig.getTargetBranch()== BuildConfig.Branches.SALAMANCA || BuildConfig.getTargetBranch()== BuildConfig.Branches.CELAYA
                            || BuildConfig.getTargetBranch()== BuildConfig.Branches.LEON) {
                        y = 430;
                    } else if ( BuildConfig.getTargetBranch() == BuildConfig.Branches.MEXICALI || BuildConfig.getTargetBranch() == BuildConfig.Branches.MORELIA || BuildConfig.getTargetBranch() == BuildConfig.Branches.TORREON) {
                        y = 378;
                    } else {
                        y = 325;
                    }

                    //int y = BuildConfig.getTargetBranch() == BuildConfig.Branches.MEXICALI && idCompany.equals("04") || BuildConfig.getTargetBranch() == BuildConfig.Branches.NAYARIT && idCompany.equals("04") || BuildConfig.getTargetBranch() == BuildConfig.Branches.SALTILLO && idCompany.equals("01") || BuildConfig.getTargetBranch() == BuildConfig.Branches.CANCUN && idCompany.equals("01") ? 430 : 325;

                    TscDll.printerfont(200, y, "3", 0, 1, 1, "Notificación");
                    y += 45;

                    TscDll.printerfont(10, y, "3", 0, 1, 1, clientName + " " + clientLastName);
                    //Log.i("PRINT STATUS", TscDll.status());
                    y += 45;

                    TscDll.printerfont(10, y, "3", 0, 1, 1, "CONTRATO:" + clientSerie + clientContractNumber);
                    y += 45;

                    TscDll.printerfont(10, y, "3", 0, 1, 1, "Estimado Afiliado:");
                    y += 45;

                    TscDll.printerfont(10, y, "2", 0, 1, 1, "Estuve en su domicilio para recoger su");
                    y += 45;

                    TscDll.printerfont(10, y, "2", 0, 1, 1, "aportación; como no se encontró, pasaré");
                    y += 45;

                    TscDll.printerfont(10, y, "2", 0, 1, 1, "de nueva cuenta el día ______, por lo ");
                    y += 45;

                    TscDll.printerfont(10, y, "2", 0, 1, 1, "que le ruego esperar o en su caso dejar ");
                    y += 45;

                    TscDll.printerfont(10, y, "2", 0, 1, 1, "el importe de su aportación.");
                    y += 45;

                    TscDll.printerfont(10, y, "2", 0, 1, 1, "Para cualquier aclaración o recado le");
                    y += 45;

                    TscDll.printerfont(10, y, "2", 0, 1, 1, "suplicamos comunicarse con nosotros a ");
                    y += 45;

                    //GUADALAJARA
                    if (BuildConfig.getTargetBranch() == BuildConfig.Branches.GUADALAJARA) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "los siguientes teléfonos: 36-15-43-30 ");
                        y += 45;

                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y 36-15-02-17. En horarios de ");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "lunes a viernes de 09:00 a 14:00");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y de 16:00 a 19:00 hrs, y sábados");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "de 09:00 a 14:00 hrs.");
                        y += 45;
                    }
                    /*
                            Av. Hidalgo No. 3402 , entre la calle Violeta y la calle Rosa en la Colonia Flores Tampico , Tamaulipas.
                            En el edificio Planta alta .
                            Entrada por Av. Hidalgo.
                            */
                    else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.TAMPICO) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 833-569-4184");
                        //833-569-4184 Y 833-569-4185
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y 833-569-4185");
                        y += 45;
                    }

                    else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.TOLUCA) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 212-78-19,");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "212-53-79 y 212-51-08.");
                        y += 45;
                    } else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.QUERETARO ) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 4422134884,");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y 4422427739.");
                        y += 45;
                    }

                    else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.IRAPUATO ) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 4626253705,");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y 4626248002.");
                        y += 45;
                    }

                    else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.LEON ) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 4777186035");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y 4777795781");
                        y += 45;
                    }

                    else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.SALAMANCA ) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 4646482086");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y 4646416958.");
                        y += 45;
                    }

                    else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.CELAYA ) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 4616093529");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y 4616144232");
                        y += 45;
                    }

                    else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.PUEBLA) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 2430967");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y 2374560.");
                        y += 45;
                    } else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.CUERNAVACA) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 7773119299");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, " y 7771704870.");
                        y += 45;
                    } else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.NAYARIT) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 2121080");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y 2169102.");
                        y += 45;
                    } else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.MERIDA) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 9201269");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y 9201268.");
                        y += 45;
                    } else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.SALTILLO) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: (844)4159299");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y (844)4148836.");
                        y += 45;
                    } else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.CANCUN) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: ");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "(998) 251.6530/31");
                        y += 45;
                    } else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.MEXICALI) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 561-2592");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, " Y 556-8660.");
                        y += 45;
                    } else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.MORELIA) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 443 1761069");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, "y 3163897");
                        y += 45;
                    }


                    else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.TORREON) {
                        TscDll.printerfont(10, y, "2", 0, 1, 1,
                                "los siguientes teléfonos: 8712852043");
                        y += 45;
                        TscDll.printerfont(10, y, "2", 0, 1, 1, " Y 8712852060");
                        y += 45;
                    }



                TscDll.printerfont(10, y, "2", 0, 1, 1, "En caso de pasar a la oficina favor ");

                    y += 45;
                    TscDll.printerfont(10, y, "2", 0, 1, 1, "de presentar este documento.");

                    y += 50;
                    TscDll.printerfont(150, y, "3", 0, 1, 1, "Respetuosamente");

                if (BuildConfig.getTargetBranch() == BuildConfig.Branches.QUERETARO || BuildConfig.getTargetBranch()== BuildConfig.Branches.IRAPUATO
                        || BuildConfig.getTargetBranch()==BuildConfig.Branches.SALAMANCA || BuildConfig.getTargetBranch()==BuildConfig.Branches.CELAYA
                        || BuildConfig.getTargetBranch()==BuildConfig.Branches.LEON)
                {
                    y += 55;
                    TscDll.printerfont(0, y, "3", 0, 1, 1, no_cobrador);
                }
                else
                {
                    y += 55;
                    TscDll.printerfont(0, y, "3", 0, 1, 1, getCollectorName(collectorName));
                }

                    if (!telefono.equals("") && !telefono.equals("null"))
                    {
                        y += 55;
                        TscDll.printerfont(0, y, "2", 0, 1, 1, "Celular: " + formatCellPhoneNumber(telefono));
                    }

                    y += 55;
                    for (int conter = 0; conter < 500; conter = conter + 30) {
                        TscDll.printerfont(conter, y, "3", 0, 1, 1, "-");
                    }

                    //Log.i("printer status", TscDll.status());

                    TscDll.printlabel(1, 1);


                    //Log.i("PRINT STATUS", TscDll.status());

                    //TscDll.closeport();

                TscDll.setup(SharedConstants.Printer.width,
                        50,
                        SharedConstants.Printer.speed,
                        SharedConstants.Printer.density,
                        SharedConstants.Printer.sensorType,
                        SharedConstants.Printer.gapBlackMarkVerticalDistance,
                        SharedConstants.Printer.gapBlackMarkShiftDistance);

                    publishProgress(ConstantsPabs.FINISHED_PRINTING);

                    ConstantsPabs.finishPrinting = true;
                    return true;

            } catch (Exception e) {

                enableBluetooth();
                Util.Log.ih("Intento = " + cont);

                try {
                    TscDll.disconnect();
                } catch (Exception e1){
                    e1.printStackTrace();
                }

                e.printStackTrace();
                cont++;
            }
        }

        publishProgress(ConstantsPabs.FINISHED_PRINTING);

        ConstantsPabs.finishPrinting = false;

        return false;
    }

    /**
     * prints address on the ticket
     * this address is divided so that it fits on the ticket
     * @param line1 first line
     * @param line2 second line
     * @param line3 third line
     */
    private void printDireccion(String line1, String line2, String line3) throws Exception{
        TscDll.printerfont(10, 205, "2", 0, 1, 1, line1);
        TscDll.printerfont(10, 250, "2", 0, 1, 1, line2);
        TscDll.printerfont(10, 295, "2", 0, 1, 1, line3);
    }

    /**
     * prints address on the ticket
     * this address is divided so that it fits on the ticket
     * @param line1 first line
     * @param line2 second line
     * @param line3 third line
     * @param line4 fourth line
     * @param line5 fifth line
     */
    private void printDireccion(String line1, String line2, String line3, String line4, String line5) throws Exception{
        TscDll.printerfont(10, 205, "2", 0, 1, 1, line1);
        TscDll.printerfont(10, 250, "2", 0, 1, 1, line2);
        TscDll.printerfont(10, 295, "2", 0, 1, 1, line3);
        TscDll.printerfont(10, 340, "2", 0, 1, 1, line4);
        TscDll.printerfont(10, 385, "2", 0, 1, 1, line5);
    }

    /**
     * checks if more than 59 minutes have passed
     * @return
     *          true -> difference is more than limit
     *          false -> difference is under limit
     */
    public boolean checkTime() {
        long newTime = new Date().getTime();
        long differences = Math.abs(dateAux - newTime);
        return (differences > (1000 * 59));
    }

    /**
     * enables bluetooth
     */
    public void enableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Util.Log.ih("enabling bluetooth");
            mBluetoothAdapter.enable();
        }
    }

    /**
     * retrieves collector name
     * @param collectorID
     * @return
     */
    public String getCollectorName(String collectorID){
        switch (collectorID){
            case "COBRANZA OFICINA":
                return "JUAN MANUEL DUEÑAS";
            case "PROBENSO 1":
                return "FERNANDO ESPINOZA";
            case "PROBENSO 2":
                return "RAUL GOMEZ PIMENTEL";
            case "PROBENSO 4":
                return "HECTOR CERVANTES RODRIGUEZ";
            case "PROBENSO 5":
                return "CRISTIAN ACEVES";
            case "PROBENSO 6":
                return "FRANCISCO MARQUEZ MARQUEZ";
            case "PROBENSO 7":
                return "OMAR BRISEÑO PIÑON";
            case "PROBENSO 8":
                return "ARGENIS RAMIREZ CARDENAS";
            case "PROBENSO 9":
                return "JAVIER ESPINOZA MOYA";
        }
        return collectorName;
    }

    /**
     * retrieves collector tel
     * @param collectorID
     * @return
     */
    public String getCollectorTel(String collectorID){
        switch (collectorID){
            case "COBRANZA OFICINA":
                return "TEL: 3636-1935 Y 3165-7579";
            case "PROBENSO 1":
                return "TEL: 33-14-41-79-37";
            case "PROBENSO 2":
                return "33-16-90-71-71";
            case "PROBENSO 4":
                return "33-10-20-12-84";
            case "PROBENSO 5":
                return "33-14-53-25-62";
            case "PROBENSO 6":
                return "33-14-50-78-32";
            case "PROBENSO 7":
                return "33-11-86-50-82";
            case "PROBENSO 8":
                return "33-31-42-63-52";
            case "PROBENSO 9":
                return "33-13-12-84-90";
        }
        return "";
    }

    private String formatCellPhoneNumber(String number)
    {
        String formattedNumber;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            formattedNumber = PhoneNumberUtils.formatNumber(number, "MX");
        }
        else {
            formattedNumber = PhoneNumberUtils.formatNumber(number);
        }

        return formattedNumber;
    }
}
