package com.jaguarlabs.pabs.rest.webService;

import com.jaguarlabs.pabs.rest.models.ModelOsticketReponse;
import com.jaguarlabs.pabs.rest.models.ModelOsticketRequest;
import com.jaguarlabs.pabs.rest.models.ModelSemaforoRequest;
import com.jaguarlabs.pabs.rest.models.ModelSemaforoResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Jordan Alvarez on 4/21/2017.
 */

public interface OsticketService {

    @POST("/controlcartera/addNewOsticketRequest")
    public ModelOsticketReponse requestOsticket(@Body ModelOsticketRequest body);

}
