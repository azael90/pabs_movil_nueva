package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

/**
 * Model request POJO class for cancelling a payment.
 *
 * @author Jordan Alvarez
 * @version 22/06/15
 */
public class ModelCancelPaymentRequest{

    @SerializedName("no_cobro")
    private int paymentNumber;
    @SerializedName("fecha")
    private String date;
    @SerializedName("periodo")
    private int periodo;
    @SerializedName("id_motivo_de_cancelacion")
    private String id_motivo_de_cancelacion;

    /**
     * .........................................
     * .          GETTERS AND SETTERS          .
     * .........................................
     */

    public int getPaymentNumber() {
        return paymentNumber;
    }

    public ModelCancelPaymentRequest setPaymentNumber(int paymentNumber) {
        this.paymentNumber = paymentNumber;
        return this;
    }

    public String getDate() {
        return date;
    }

    public ModelCancelPaymentRequest setDate(String date) {
        this.date = date;
        return this;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public String getMotivoCancelacion() {
        return id_motivo_de_cancelacion;
    }

    public void setMotivoCancelacion(String motivoCancelacion) {
        this.id_motivo_de_cancelacion = motivoCancelacion;
    }
}
