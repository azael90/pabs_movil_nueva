package com.jaguarlabs.pabs.rest.request;

import com.jaguarlabs.pabs.rest.models.ModelCellPhoneNumbersRequest;
import com.jaguarlabs.pabs.rest.models.ModelCellPhoneNumbersResponse;
import com.jaguarlabs.pabs.rest.webService.CellPhoneNumberService;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by Jordan Alvarez on 03/04/2019.
 */
public class CellPhoneNumberRequest extends RetrofitSpiceRequest<ModelCellPhoneNumbersResponse, CellPhoneNumberService> {

    ModelCellPhoneNumbersRequest body;

    public CellPhoneNumberRequest( ModelCellPhoneNumbersRequest modelCellPhoneNumbersRequest ){
        super(ModelCellPhoneNumbersResponse.class, CellPhoneNumberService.class);

        this.body = modelCellPhoneNumbersRequest;
    }

    @Override
    public ModelCellPhoneNumbersResponse loadDataFromNetwork() throws Exception {
        Util.Log.ih("on loadDataFromNetwork");
        return getService().requestCellPhoneNumber(body);
    }
}
