package com.jaguarlabs.pabs.rest.models;

/**
 * Created by jordan on 16/12/15.
 */
public class ModelPrintMoreThan400PaymentsCierreResponse {

    public boolean result;
    public boolean finishPrinting;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public boolean isFinishPrinting() {
        return finishPrinting;
    }

    public void setFinishPrinting(boolean finishPrinting) {
        this.finishPrinting = finishPrinting;
    }
}
