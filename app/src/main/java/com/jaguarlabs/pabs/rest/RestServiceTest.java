package com.jaguarlabs.pabs.rest;

import android.app.Application;
import android.util.Log;

import com.jaguarlabs.pabs.rest.models.ModelCancelPaymentResponse;
import com.jaguarlabs.pabs.rest.models.ModelSemaforoResponse;
import com.jaguarlabs.pabs.rest.persisters.GsonObjectPersister;
import com.jaguarlabs.pabs.rest.webService.CancelPaymentService;
import com.jaguarlabs.pabs.rest.webService.SemaforoService;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by jordan on 4/08/16.
 */
public class RestServiceTest extends RetrofitGsonSpiceService {

    @Override
    public void onCreate(){
        super.onCreate();
        //addRetrofitInterface(CancelPaymentService.class);
        addRetrofitInterface(SemaforoService.class);
    }


    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        RestAdapter.Builder builder = super.createRestAdapterBuilder();

        builder.setLogLevel(RestAdapter.LogLevel.FULL);
        builder.setLog(message -> Log.i("Rest", message));
//        builder.setClient(new UrlConnectionClient(){
//            @Override
//            protected HttpURLConnection openConnection(Request request) throws IOException {
//                HttpURLConnection connection = super.openConnection(request);
//
//                connection.setConnectTimeout(Util.BuildConfig.REST_TIME_OUT*1000);
//                connection.setReadTimeout(Util.BuildConfig.REST_TIME_OUT*1000);
//
//                return connection;
//            }
//        });
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(90, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(90, TimeUnit.SECONDS);
        okHttpClient.setRetryOnConnectionFailure(true);
        okHttpClient.setWriteTimeout(90, TimeUnit.SECONDS);

        builder.setClient(new OkClient(okHttpClient));

        return builder;
    }

    @Override
    protected String getServerUrl() {
        return ConstantsPabs.baseURLTEST;
    }

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager manager = new CacheManager();

        //GsonObjectPersister<ModelCancelPaymentResponse> persisterCancelTicket = new GsonObjectPersister<ModelCancelPaymentResponse>(application, ModelCancelPaymentResponse.class);
        GsonObjectPersister<ModelSemaforoResponse> persisterSendContractsFromSemaforo = new GsonObjectPersister<ModelSemaforoResponse>(application, ModelSemaforoResponse.class);

        //manager.addPersister(persisterCancelTicket);
        manager.addPersister(persisterSendContractsFromSemaforo);

        return manager;
    }

//    @Override
//    public int getThreadCount() {
//        return 10;
//    }
}
