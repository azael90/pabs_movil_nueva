package com.jaguarlabs.pabs.rest.request;

import com.jaguarlabs.pabs.rest.models.ModelSearchForClientRequest;
import com.jaguarlabs.pabs.rest.models.ModelSearchForClientResponse;
import com.jaguarlabs.pabs.rest.webService.SearchForClientService;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by jordan on 2/09/16.
 */
public class SearchForClientRequest extends RetrofitSpiceRequest<ModelSearchForClientResponse, SearchForClientService> {

    ModelSearchForClientRequest body;

    public SearchForClientRequest(ModelSearchForClientRequest body){
        super(ModelSearchForClientResponse.class, SearchForClientService.class);

        this.body = body;
    }




    @Override
    public ModelSearchForClientResponse loadDataFromNetwork() throws Exception {
        Util.Log.ih("on loadDataFromNetwork");
        publishProgress(ConstantsPabs.START_REQUEST);
        ModelSearchForClientResponse modelSearchForClientResponse = getService().requestSearchForClient(body);
        publishProgress(ConstantsPabs.FINISHED_REQUEST);
        return modelSearchForClientResponse;

    }
}
