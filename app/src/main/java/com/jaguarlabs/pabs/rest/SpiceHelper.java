package com.jaguarlabs.pabs.rest;

import com.jaguarlabs.pabs.rest.request.offline.PrintVisitTicketRequest;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.CacheLoadingException;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.octo.android.robospice.request.listener.RequestProgressListener;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * helper class to manage android service
 * Created by cbriseno on 13/04/15.
 */
public abstract class SpiceHelper<CLASS_RESPONSE> {

    private SpiceManager spiceManager;
    private String cacheKey;
    private boolean isRequestPending;
    private Class responseClass;
    private long durationInMillis;

    public SpiceHelper(SpiceManager spiceManager, String cacheKey, Class responseClass, long durationInMillis){
        this.cacheKey = cacheKey;
        this.spiceManager = spiceManager;
        this.responseClass = responseClass;
        isRequestPending = false;
        this.durationInMillis = durationInMillis;
    }

    private void clearCacheRequest(){
        try {
            Future<?> future = spiceManager.removeAllDataFromCache();
            if (future != null){
                future.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void getCacheAndPrint() {
        try {
            Future<List<Object>> future = spiceManager.getAllCacheKeys(responseClass);
            if (future.get().size() > 0) {
                Util.Log.ih("getAllCacheKeys = " + future.get().size());
                for (int i = 0; i < future.get().size(); i++) {
                    Util.Log.ih("CacheKeys = " + future.get().get(i));
                }
            }

            Future<List<Object>> futureA = spiceManager.getAllDataFromCache(responseClass);
            if (futureA.get().size() > 0) {
                Util.Log.ih("getAllDataFromCache = " + futureA.get().size());
                for (int j = 0; j < futureA.get().size(); j++) {
                    Util.Log.ih("DataFromCache = " + futureA.get().get(j));
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (CacheLoadingException e) {
            e.printStackTrace();
        }
    }

    protected void onFail(SpiceException exception){
        clearCacheRequest();
        isRequestPending = false;
    }

    protected void onSuccess(CLASS_RESPONSE response){
        //getCacheAndPrint();
        clearCacheRequest();
        isRequestPending = false;
    }

    protected void onProgressUpdate(RequestProgress progress){
    }

    public void executeRequest(SpiceRequest<CLASS_RESPONSE> request){
        isRequestPending = true;
        spiceManager.execute(request, cacheKey, durationInMillis, new SpiceRequestListener());
    }

    public void executePendingRequest(){
        isRequestPending = true;
        spiceManager.addListenerIfPending(responseClass, cacheKey, new SpicePendingRequestListener());
    }

    public boolean isRequestPending() {
        return isRequestPending;
    }

    private class SpiceRequestListener implements RequestListener<CLASS_RESPONSE>, RequestProgressListener{

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            onFail(spiceException);
        }

        @Override
        public void onRequestSuccess(CLASS_RESPONSE response) {
            onSuccess(response);
        }

        @Override
        public void onRequestProgressUpdate(RequestProgress progress) {
            onProgressUpdate(progress);
        }
    }

    private class SpicePendingRequestListener implements PendingRequestListener<CLASS_RESPONSE>{

        @Override
        public void onRequestNotFound() {
            if (isRequestPending){
                spiceManager.getFromCache(responseClass, cacheKey, durationInMillis, new SpiceRequestListener());
            }
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            onFail(spiceException);
        }

        @Override
        public void onRequestSuccess(CLASS_RESPONSE response) {
            onSuccess(response);
        }
    }

}