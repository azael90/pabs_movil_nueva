package com.jaguarlabs.pabs.rest.webService;

import com.jaguarlabs.pabs.rest.models.ModelGetCollectorsRequest;
import com.jaguarlabs.pabs.rest.models.ModelGetCollectorsResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by jordan on 26/08/16.
 */
public interface GetCollectorsService {

    //@POST("controlcartera/getCollectorsBySupervisor")
    @POST("/controlcartera/getListCollectors")
    public ModelGetCollectorsResponse requestGetCollectors(@Body ModelGetCollectorsRequest body);

}
