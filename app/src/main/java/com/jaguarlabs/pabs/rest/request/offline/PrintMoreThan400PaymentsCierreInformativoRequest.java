package com.jaguarlabs.pabs.rest.request.offline;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.jaguarlabs.pabs.activities.CierreInformativo;
import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinterHelper;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.rest.models.ModelPrintMoreThan400PaymentsCierreResponse;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.SharedConstants;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.SpiceRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

/**
 * Request offline service for sending information to printer.
 * prints 'cierre informativo' ticket
 *
 * @author Jordan Alvarez
 * @version 16/12/15
 */
public class PrintMoreThan400PaymentsCierreInformativoRequest extends SpiceRequest<ModelPrintMoreThan400PaymentsCierreResponse> {

    private Context context;
    private SharedPreferences efectivoPreference;

    private Timer timerToStopPrinting;
    private Date date = new Date();
    private long dateAux;

    private ModelPrintMoreThan400PaymentsCierreResponse response;

    //private TSCActivity TscDll;
    private BluetoothPrinter TscDll;

    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat hourFormatter;

    public PrintMoreThan400PaymentsCierreInformativoRequest(){
        super(ModelPrintMoreThan400PaymentsCierreResponse.class);

        Util.Log.ih("INSTANTIATING");

        //this.TscDll = new TSCActivity();
        this.response = new ModelPrintMoreThan400PaymentsCierreResponse();
        this.context = ApplicationResourcesProvider.getContext();

        this.dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        this.hourFormatter = new SimpleDateFormat("HH:mm");

        Util.Log.ih("FINISH INSTANTIATING");
    }


    @Override
    public ModelPrintMoreThan400PaymentsCierreResponse loadDataFromNetwork() throws Exception {
        Util.Log.ih("in loadDataFromNetwork()");

        publishProgress(ConstantsPabs.START_PRINTING);

        return printPayments();
    }

    /**
     * method that sends payment info to printer
     * prints 'cierre de periodo' ticket
     *
     * @return PrintMoreThan400PaymentsInformativoResponse instance
     */
    public ModelPrintMoreThan400PaymentsCierreResponse printPayments()
    {
        int attempts = 0;
        boolean finishIterationAndPrint = false;

        //loop where tries to send info to printer
        while(attempts < 100)
        {
            try {
                if(CierreInformativo.getMthis().arrayCierreCut == null)
                {
                    CierreInformativo.getMthis().arrayCierreCut = new JSONArray();

                    response.result = false;

                    return response;
                }

                int size = 0;
                if( (CierreInformativo.getMthis().arrayCierreArranged.length() - CierreInformativo.getMthis().printCounterToSetSize) > 400 ){
                    size = 400;
                }
                else{
                    size = CierreInformativo.getMthis().arrayCierreArranged.length() - CierreInformativo.getMthis().printCounterToSetSize;
                }
                //size = size * 4 + 245;
                size = size * 4 + 182;

                TscDll = BluetoothPrinter.getInstance();

                if (!BluetoothPrinter.printerConnected)
                    TscDll.connect();

                //dateAux = new Date().getTime();

                //timer that closes printer port if got stuck
                //trying to print
                //has one minute of limit before closing port
                //closing port causes NullPointerException on openPort()
                //so it tries again to print
                /*
                timerToStopPrinting = new Timer();
                timerToStopPrinting.schedule(new TimerTask() {

                    String mac;
                    int attempt;
                    TSCActivity tscDll;

                    @Override
                    public void run() {
                        if (checkTime()) {
                            Util.Log.ih("Closing port");
                            //disableBluetooth();
                            try {
                                tscDll.closeport();
                            } catch (Exception e){
                                e.printStackTrace();
                                timerToStopPrinting.cancel();
                            }
                        }
                        Util.Log.ih("attempt: " + attempt);
                    }

                    public TimerTask setData(int attempt, TSCActivity tscDll) {
                        this.attempt = attempt;
                        this.tscDll = tscDll;
                        return this;
                    }
                }.setData(attempts, TscDll), (1000 * 60));
                */

                //TscDll.openport(CierreInformativo.getMthis().mac);
                Util.Log.ih("despues de openPort");
                //timerToStopPrinting.cancel();

                TscDll.setup(SharedConstants.Printer.width,
                        size,
                        2,
                        SharedConstants.Printer.density,
                        SharedConstants.Printer.sensorType,
                        SharedConstants.Printer.gapBlackMarkVerticalDistance,
                        SharedConstants.Printer.gapBlackMarkShiftDistance);

                CierreInformativo.getMthis().companiesValues = DatabaseAssistant.getCompanies();

                TscDll.clearbuffer();

                //Log.i("PRINT STATUS", TscDll.status());

                TscDll.sendcommand("GAP 0,0\n");
                TscDll.sendcommand("CLS\n");
                TscDll.sendcommand("CODEPAGE UTF-8\n");
                TscDll.sendcommand("SET TEAR ON\n");
                TscDll.sendcommand("SET HEAD ON\n");
                TscDll.sendcommand("SET REPRINT OFF\n");
                TscDll.sendcommand("SET COUNTER @1 1\n");

                //totalesValues = new JSONObject();

                //everything that is sent to printer
                //has to be with coordenates
                if( CierreInformativo.getMthis().printCounterToSetSize != 0 ){
                    TscDll.printerfont(50, 10, "4", 0, 1, 1, "CONTINUACION...");
                }
                else{
                    TscDll.printerfont(50, 10, "4", 0, 1, 1, "CIERRE INFORMATIVO");
                }

                CierreInformativo.getMthis().xValue = 70;

                finishIterationAndPrint = false;

                //for para interar entre las diferentes empresas
                for ( ; CierreInformativo.getMthis().organizationCounter < CierreInformativo.getMthis().companiesValues.size() && !finishIterationAndPrint; ){

                    // !finishIterationAndPrint
                    if( CierreInformativo.getMthis().restartMonto ){
                        CierreInformativo.getMthis().montoValue = 0;
                    }
                    //JSONObject empresa = CierreInformativo.getMthis().empresasValues.getJSONObject(CierreInformativo.getMthis().organizationCounter);
                    Companies company = CierreInformativo.getMthis().companiesValues.get(CierreInformativo.getMthis().organizationCounter);

                    //Log.e("empresa", empresa.toString());

                    TscDll.printerfont( 400, CierreInformativo.getMthis().xValue + 10, "2", 0, 1, 1, dateFormatter.format(date) );
                    TscDll.printerfont( 480, CierreInformativo.getMthis().xValue + 70, "2", 0, 1, 1, hourFormatter.format(date) );
                    TscDll.printerfont( 10, CierreInformativo.getMthis().xValue + 10, "2", 0, 1, 1, CierreInformativo.getMthis().coCobrador );
                    TscDll.printerfont( 10, CierreInformativo.getMthis().xValue + 40, "2", 0, 1, 1, CierreInformativo.getMthis().nombre );
                    TscDll.printerfont( 10, CierreInformativo.getMthis().xValue + 70, "2", 0, 1, 1, "Periodo " + (getEfectivoPreference().getInt("periodo", 0)) );

                    CierreInformativo.getMthis().xValue += 150;
                    CierreInformativo.getMthis().ticketsValue = 0;

                    String companynameMask = ExtendedActivity.getCompanyNameFormatted( company.getName() );

                    TscDll.printerfont( 10, CierreInformativo.getMthis().xValue, "3", 0, 1, 1, companynameMask );

                    CierreInformativo.getMthis().ticketPosValue = CierreInformativo.getMthis().xValue;
                    CierreInformativo.getMthis().xValue += 30;

                    TscDll.printerfont( 10, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Contrato" );
                    TscDll.printerfont( 160, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Folio" );
                    TscDll.printerfont( 310, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Importe" );
                    //TscDll.printerfont( 440, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Copias" );

                    CierreInformativo.getMthis().xValue += 30;

                    //use empresa.getString("id") instead
                    //String empresaId = getEmpresaId(CierreInformativo.getMthis().organizationCounter);
                    String empresaId = company.getIdCompany();

                    CierreInformativo.getMthis().restartMonto = true;

                    //for para iterar entre los diferentes pagos pertenecientes a cada empresa
                    for (  ; CierreInformativo.getMthis().printCounter < CierreInformativo.getMthis().arrayCierreCut.length() &&
                            CierreInformativo.getMthis().arrayCierreCut.getJSONObject(CierreInformativo.getMthis().printCounter).getString("empresa").equals(empresaId) &&
                            !finishIterationAndPrint ;  ) {

                        JSONObject paymentFromArrayCierre = CierreInformativo.getMthis().arrayCierreCut.getJSONObject(CierreInformativo.getMthis().printCounter);

                        if (paymentFromArrayCierre.getDouble("monto") != 0) {
                            CierreInformativo.getMthis().ticketsValue++;

                            TscDll.printerfont( 10, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, paymentFromArrayCierre.getString("no_cliente") );
                            TscDll.printerfont( 160, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, paymentFromArrayCierre.getString("folio") );
                            //TscDll.printerfont( 440, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, paymentFromArrayCierre.getString("copias") );
                            if ( paymentFromArrayCierre.getInt("status") == 7 ) {
                                TscDll.printerfont( 310, CierreInformativo.getMthis().xValue - 10, "4", 0, 1, 1, "Cancelado" );
                            } else {
                                TscDll.printerfont( 310, CierreInformativo.getMthis().xValue, "2", 0, 1, 1,
                                        "$" + formatMoney(Double.
                                                parseDouble(paymentFromArrayCierre.
                                                        getString("monto"))) );
                                if (paymentFromArrayCierre.has("tipo_cobro") && paymentFromArrayCierre.getInt("tipo_cobro") == 2)
                                    TscDll.printerfont(430, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Manual");
                                else if (paymentFromArrayCierre.has("tipo_cobro") && paymentFromArrayCierre.getInt("tipo_cobro") == 3)
                                    TscDll.printerfont(430, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Factura");
                                else if (paymentFromArrayCierre.has("tipo_cobro") && paymentFromArrayCierre.getInt("tipo_cobro") == 4)
                                    TscDll.printerfont(430, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Cancel NE");

                            }

                        }
                        else {
                            CierreInformativo.getMthis().ticketsValue++;
                            TscDll.printerfont(10, CierreInformativo.getMthis().xValue, "2", 0, 1, 1,
                                    paymentFromArrayCierre.getString("no_cliente"));
                            //if (paymentFromArrayCierre.getInt("status") == 2) {
                            //    TscDll.printerfont(200, CierreInformativo.getMthis().xValue, "2", 0, 1, 1,
                            //            "NO APORTÓ");
                            if (paymentFromArrayCierre.getInt("status") == 8) {
                                TscDll.printerfont(160, CierreInformativo.getMthis().xValue, "2", 0, 1, 1,
                                        "NO EXISTE CLIENTE");
                            } else if (paymentFromArrayCierre.getInt("status") == 9) {
                                TscDll.printerfont(160, CierreInformativo.getMthis().xValue, "2", 0, 1, 1,
                                        "NO EXISTE DIRECCION");
                            } else if (paymentFromArrayCierre.getInt("status") == 10) {
                                TscDll.printerfont(160, CierreInformativo.getMthis().xValue, "2", 0, 1, 1,
                                        "SUPERVISADO");
                            } else {
                                TscDll.printerfont(160, CierreInformativo.getMthis().xValue, "2", 0, 1, 1,
                                        "NO APORTÓ");
                            }


                        }

                        CierreInformativo.getMthis().xValue = CierreInformativo.getMthis().xValue + 30;

                        if (paymentFromArrayCierre.getInt("status") == 1) {
                            CierreInformativo.getMthis().montoValue += paymentFromArrayCierre.getDouble("monto");
                        }

                        CierreInformativo.getMthis().printCounterToSetSize++;
                        CierreInformativo.getMthis().printCounter++;
                        CierreInformativo.getMthis().restartMonto = true;

                        if( CierreInformativo.getMthis().printCounter == CierreInformativo.getMthis().limitToPrint  ){
                            //CierreInformativo.getMthis().limitToPrint+=400;
                            CierreInformativo.getMthis().printCounter = 0;
                            finishIterationAndPrint = true;
                            CierreInformativo.getMthis().restartMonto = false;

                            TscDll.printerfont( 310, CierreInformativo.getMthis().ticketPosValue, "2", 0, 1, 1, "Tickets " + CierreInformativo.getMthis().ticketsValue );
                        }
                    }

                    if( !finishIterationAndPrint ){
                        TscDll.printerfont( 310, CierreInformativo.getMthis().ticketPosValue, "2", 0, 1, 1, "Tickets " + CierreInformativo.getMthis().ticketsValue );
                        TscDll.printerfont( 185, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Subtotal $" + formatMoney(Double.parseDouble("" + CierreInformativo.getMthis().montoValue)) );

                        CierreInformativo.getMthis().xValue = CierreInformativo.getMthis().xValue + 70;

                        CierreInformativo.getMthis().totalesValues.put( company.getName(), CierreInformativo.getMthis().montoValue );

                        CierreInformativo.getMthis().organizationCounter++;
                    }


                }

                Util.Log.ih("finishIterationAndPrint = " + finishIterationAndPrint);

                if( finishIterationAndPrint ){


                    //Util.Log.ih("PRINT STATUS -> antes de imprimir = " + TscDll.status());

                    TscDll.printlabel(1, 1);

                    //Util.Log.ih("PRINT STATUS -> despues de imprimir = " + TscDll.status());

                    //TscDll.closeport();

                    response.result = true;
                    response.finishPrinting = false;

                    publishProgress(ConstantsPabs.FINISHED_PRINTING);

                    return response;
                }
                else{
                    return finishPrintingTicket();
                }
                //return true;

            } catch (Exception e) {

                Util.Log.ih("Intento = " + attempts);
                enableBluetooth();

                try {
                    TscDll.disconnect();
                } catch (Exception e1){
                    e1.printStackTrace();
                }

                e.printStackTrace();
                attempts++;
            }


        }

        response.result = false;

        publishProgress(ConstantsPabs.FINISHED_PRINTING);

        return response;
    }

    /**
     * disables bluetooth
     */
    public void disableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
                .getDefaultAdapter();
        mBluetoothAdapter.disable();
    }

    /**
     * enables bluetooth
     */
    public void enableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Util.Log.ih("enabling bluetooth");
            mBluetoothAdapter.enable();
        }
    }

    /**
     * checks if more than 59 minutes have passed
     * @return
     *          true -> difference is more than limit
     *          false -> difference is under limit
     */
    public boolean checkTime() {
        long newTime = new Date().getTime();
        long differences = Math.abs(dateAux - newTime);
        return (differences > (1000 * 59));
    }

    /**
     * this method gets called to finish printing the ticket
     * this part of the ticket has total amount of companies
     * and deposits.
     *
     * @return PrintMoreThan400PaymentsInformativoResponse instance
     */
    public ModelPrintMoreThan400PaymentsCierreResponse finishPrintingTicket()
    {

        ModelPrintMoreThan400PaymentsCierreResponse response = new ModelPrintMoreThan400PaymentsCierreResponse();
        boolean workDone = false;
        int attempts = 0;

        while(!workDone && attempts < 5) {
            try {
                if (BuildConfig.isWalletSynchronizationEnabled()) {

                    String[] contractsNotVisited = DatabaseAssistant.getContractsNotVisited();

                    CierreInformativo.getMthis().xValue += 50;
                    TscDll.printerfont(50, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "NÚMERO DE CONTRATOS NO VISITADOS");
                    CierreInformativo.getMthis().xValue += 50;
                    TscDll.printerfont(10, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Semanales");
                    TscDll.printerfont(350, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, contractsNotVisited[0]);
                    CierreInformativo.getMthis().xValue += 30;
                    TscDll.printerfont(10, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Quincenales");
                    TscDll.printerfont(350, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, contractsNotVisited[1]);
                    CierreInformativo.getMthis().xValue += 30;
                    TscDll.printerfont(10, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Mensuales");
                    TscDll.printerfont(350, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, contractsNotVisited[2]);
                }


                CierreInformativo.getMthis().xValue += 50;

                TscDll.printerfont(10, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Efectivo Inicial");
                TscDll.printerfont(350, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "$" + formatMoney(Double.parseDouble("" + CierreInformativo.getMthis().efectivoInicial)));

                CierreInformativo.getMthis().xValue += 50;

                TscDll.printerfont(350, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "TOTALES");

                CierreInformativo.getMthis().xValue += 30;

                TscDll.printerfont(10, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "DEPOSITOS");

                TscDll.printerfont(350, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "$" + formatMoney(Double.parseDouble("" + CierreInformativo.getMthis().deposito)));

                CierreInformativo.getMthis().xValue += 30;

                TscDll.printerfont(10, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "EFECTIVO INICIAL");
                TscDll.printerfont(350, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "$" + formatMoney(Double.parseDouble("" + CierreInformativo.getMthis().efectivoInicial)));

                CierreInformativo.getMthis().xValue += 30;

                TscDll.printerfont(10, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "EFECTIVO FINAL");

                float totalempresas = 0;

                for (int i = 0; i < CierreInformativo.getMthis().companiesValues.size(); i++) {
                    //JSONObject empresa = CierreInformativo.getMthis().empresasValues.getJSONObject(i);
                    Companies company = CierreInformativo.getMthis().companiesValues.get(i);
                    String companynameMask = ExtendedActivity.getCompanyNameFormatted( company.getName() );
                    totalempresas += CierreInformativo.getMthis().totalesValues.getDouble(companynameMask);
                }
                double sumaTotal = CierreInformativo.getMthis().deposito + (totalempresas + CierreInformativo.getMthis().efectivoInicial - CierreInformativo.getMthis().deposito);

                TscDll.printerfont(350, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "$" + formatMoney(sumaTotal - CierreInformativo.getMthis().deposito));

                CierreInformativo.getMthis().xValue += 30;

                TscDll.printerfont(250, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "TOTAL: $" + formatMoney(Double.parseDouble("" + sumaTotal)));

                CierreInformativo.getMthis().xValue += 30;

                TscDll.printerfont(150, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "TOTALES POR EMPRESA");

                CierreInformativo.getMthis().xValue += 30;

                //Log.i("PRINT STATUS", TscDll.status());

                float totalfinal = 0;

                for (int i = 0; i < CierreInformativo.getMthis().companiesValues.size(); i++) {
                    //JSONObject empresa = CierreInformativo.getMthis().empresasValues.getJSONObject(i);
                    Companies company = CierreInformativo.getMthis().companiesValues.get(i);
                    String companynameMask = ExtendedActivity.getCompanyNameFormatted( company.getName() );

                    TscDll.printerfont(10, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, companynameMask);
                    TscDll.printerfont(350, CierreInformativo.getMthis().xValue, "2", 0, 1, 1,
                            "$" + formatMoney(Double.parseDouble(""
                                    + CierreInformativo.getMthis().totalesValues.getDouble(company.getName()))));

                    totalfinal += CierreInformativo.getMthis().totalesValues.getDouble(company.getName());
                    CierreInformativo.getMthis().xValue += 30;
                }

                TscDll.printerfont(150, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "TOTAL DEL DIA: $" + formatMoney(Double.parseDouble("" + totalfinal)));

                CierreInformativo.getMthis().xValue += 30;

                TscDll.printerfont(150, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "DEPOSITOS POR EMPRESA");

                CierreInformativo.getMthis().xValue += 30;

                for (int i = 0; i < CierreInformativo.getMthis().companiesValues.size(); i++) {
                    //JSONObject empresa = CierreInformativo.getMthis().empresasValues.getJSONObject(i);
                    Companies company = CierreInformativo.getMthis().companiesValues.get(i);
                    String companynameMask = ExtendedActivity.getCompanyNameFormatted( company.getName() );

                    TscDll.printerfont(10, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, companynameMask);
                    TscDll.printerfont(350, CierreInformativo.getMthis().xValue, "2", 0, 1, 1,
                            "$" + formatMoney(Double.parseDouble(""
                                    + DatabaseAssistant.getTotalDepositsByCompany(company.getIdCompany()))));
                    CierreInformativo.getMthis().xValue += 30;

                }

                //Log.i("PRINT STATUS -> antes de imprimir", TscDll.status());

                TscDll.printlabel(1, 1);

                //Log.i("PRINT STATUS -> despues de imprimir", TscDll.status());

                //TscDll.closeport();

                response.result = true;
                response.finishPrinting = true;
                workDone = false;

                publishProgress(ConstantsPabs.FINISHED_PRINTING);

                return response;
            } catch (Exception e) {

                Util.Log.ih("Intento = " + attempts);
                //enableBluetooth();

                try {
                    TscDll.disconnect();
                } catch (Exception e1){
                    e1.printStackTrace();
                }

                e.printStackTrace();
                attempts++;
            }
        }

        response.result = false;

        publishProgress(ConstantsPabs.FINISHED_PRINTING);

        return response;
    }

    /**
     * formats money
     *
     * @param money
     * 			money type
     * @return
     * 		formatted money
     */
    public String formatMoney(double money) {
        DecimalFormatSymbols nf = new DecimalFormatSymbols();
        nf.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", nf);
        df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);
        return df.format(money);
    }

    /**
     * sharedPreferences file 'efecitvo'
     * with info of every company
     * @return SharedPreferences
     */
    public SharedPreferences getEfectivoPreference(){
        efectivoPreference = context.getSharedPreferences("efecitvo", Context.MODE_PRIVATE);

        return efectivoPreference;
    }

}
