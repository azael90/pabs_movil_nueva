package com.jaguarlabs.pabs.rest.request.offline;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.jaguarlabs.pabs.activities.Cierre;
import com.jaguarlabs.pabs.activities.CierreInformativo;
import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinterHelper;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.rest.models.ModelPrintMoreThan400PaymentsCierreResponse;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.SharedConstants;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.SpiceRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

/**
 * Request offline service for sending information to printer.
 * prints 'cierre de periodo' ticket
 *
 * @author Jordan Alvarez
 * @version 16/12/15
 */
public class PrintMoreThan400PaymentsCierrePeriodoRequest extends SpiceRequest<ModelPrintMoreThan400PaymentsCierreResponse> {

    //private TSCActivity TscDll;
    private BluetoothPrinter TscDll;

    private SharedPreferences efectivoPreference;

    private long dateAux;
    private Timer timerToStopPrinting;

    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat hourFormatter;
    private Date date = new Date();

    private Context context;

    public PrintMoreThan400PaymentsCierrePeriodoRequest(){
        super(ModelPrintMoreThan400PaymentsCierreResponse.class);

        Util.Log.ih("INSTANTIATING");

        //this.TscDll = new TSCActivity();

        this.dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        this.hourFormatter = new SimpleDateFormat("HH:mm");
        this.date = new Date();

        this.context = ApplicationResourcesProvider.getContext();

        Util.Log.ih("FINISH INSTANTIATING");
    }


    @Override
    public ModelPrintMoreThan400PaymentsCierreResponse loadDataFromNetwork() throws Exception {
        Util.Log.ih("in loadDataFromNetwork()");

        publishProgress(ConstantsPabs.START_PRINTING);

        return printPayments();
    }

    /**
     * method that sends payment info to printer
     * prints 'cierre de periodo' ticket
     *
     * @return PrintMoreThan400PaymentsCierreResponse instance
     */
    public ModelPrintMoreThan400PaymentsCierreResponse printPayments()
    {
        ModelPrintMoreThan400PaymentsCierreResponse response = new ModelPrintMoreThan400PaymentsCierreResponse();

        int attempts = 0;
        boolean finishIterationAndPrint = false;

        //loop where tries to send info to printer
        while(attempts < 100)
        {
            try {
                if(Cierre.getMthis().arrayCierreCut == null)
                {
                    Cierre.getMthis().arrayCierreCut = new JSONArray();

                    response.result = false;

                    return response;
                }

                int size = 0;
                if( (Cierre.getMthis().arrayCierreArranged.length() - Cierre.getMthis().printCounterToSetSize) > 400 ){
                    size = 400;
                }
                else{
                    size = Cierre.getMthis().arrayCierreArranged.length() - Cierre.getMthis().printCounterToSetSize;
                }
                size = size * 4 + 180;

                TscDll = BluetoothPrinter.getInstance();

                if (!BluetoothPrinter.printerConnected)
                    TscDll.connect();

                Util.Log.ih("despues de openPort");
                //timerToStopPrinting.cancel();

                TscDll.setup(SharedConstants.Printer.width,
                        size,
                        2,
                        SharedConstants.Printer.density,
                        SharedConstants.Printer.sensorType,
                        SharedConstants.Printer.gapBlackMarkVerticalDistance,
                        SharedConstants.Printer.gapBlackMarkShiftDistance);

                Cierre.getMthis().companiesValues = DatabaseAssistant.getCompanies();

                TscDll.clearbuffer();

                //Log.i("PRINT STATUS", TscDll.status());

                TscDll.sendcommand("GAP 0,0\n");
                TscDll.sendcommand("CLS\n");
                TscDll.sendcommand("CODEPAGE UTF-8\n");
                TscDll.sendcommand("SET TEAR ON\n");
                TscDll.sendcommand("SET HEAD ON\n");
                TscDll.sendcommand("SET REPRINT OFF\n");
                TscDll.sendcommand("SET COUNTER @1 1\n");

                //totalesValues = new JSONObject();

                //everything that is sent to printer
                //has to be with coordenates
                if( Cierre.getMthis().printCounterToSetSize != 0 ){
                    TscDll.printerfont(10, 10, "3", 0, 1, 1, "CONTINUACION...");
                }
                else{
                    TscDll.printerfont(10, 10, "3", 0, 1, 1, "CIERRE DEL PERIODO");
                }

                Cierre.getMthis().xValue = 70;

                finishIterationAndPrint = false;

                //for para interar entre las diferentes empresas
                for ( ; Cierre.getMthis().organizationCounter < Cierre.getMthis().companiesValues.size() && !finishIterationAndPrint; ){

                    // !finishIterationAndPrint
                    if( Cierre.getMthis().restartMonto ){
                        Cierre.getMthis().montoValue = 0;
                    }
                    //JSONObject empresa = Cierre.getMthis().empresasValues.getJSONObject(Cierre.getMthis().organizationCounter);
                    Companies company = Cierre.getMthis().companiesValues.get(Cierre.getMthis().organizationCounter);


                    //Log.e("empresa", empresa.toString());

                    TscDll.printerfont( 400, Cierre.getMthis().xValue + 10, "2", 0, 1, 1, dateFormatter.format(date) );
                    TscDll.printerfont( 480, Cierre.getMthis().xValue + 70, "2", 0, 1, 1, dateFormatter.format(date) );
                    TscDll.printerfont( 10, Cierre.getMthis().xValue + 10, "2", 0, 1, 1, Cierre.getMthis().coCobrador );
                    TscDll.printerfont( 10, Cierre.getMthis().xValue + 40, "2", 0, 1, 1, Cierre.getMthis().nombre );
                    TscDll.printerfont( 10, Cierre.getMthis().xValue + 70, "2", 0, 1, 1, "Periodo " + (getEfectivoPreference().getInt("periodo", 0) - 1) );

                    Cierre.getMthis().xValue += 150;
                    Cierre.getMthis().ticketsValue = 0;

                    String companyNameMask = "";
                    switch ( BuildConfig.getTargetBranch() ) {
                        case IRAPUATO:case SALAMANCA: case CELAYA: case LEON:
                            if ( company.getName().equalsIgnoreCase("COOPERATIVA") ) {
                                companyNameMask = "NARANJA";
                            } else if ( company.getName().equalsIgnoreCase("PROGRAMA") ) {
                                companyNameMask = "AZUL";
                            }
                            break;
                        default:
                            companyNameMask = company.getName().toUpperCase();
                            break;
                    }

                    TscDll.printerfont(10, Cierre.getMthis().xValue, "3", 0, 1, 1, companyNameMask);

                    Cierre.getMthis().ticketPosValue = Cierre.getMthis().xValue;
                    Cierre.getMthis().xValue += 30;

                    TscDll.printerfont( 10, Cierre.getMthis().xValue, "2", 0, 1, 1, "Contrato" );
                    TscDll.printerfont( 160, Cierre.getMthis().xValue, "2", 0, 1, 1, "Folio" );
                    TscDll.printerfont( 310, Cierre.getMthis().xValue, "2", 0, 1, 1, "Importe" );
                    //TscDll.printerfont( 440, Cierre.getMthis().xValue, "2", 0, 1, 1, "Copias" );

                    Cierre.getMthis().xValue += 30;

                    //use empresa.getString("id") instead
                    //String empresaId = getEmpresaId(Cierre.getMthis().organizationCounter);
                    String empresaId = company.getIdCompany();

                    //for para iterar entre los diferentes pagos pertenecientes a cada empresa
                    for (  ; Cierre.getMthis().printCounter < Cierre.getMthis().arrayCierreCut.length() && Cierre.getMthis().arrayCierreCut.getJSONObject(Cierre.getMthis().printCounter).getString("empresa").equals(empresaId) && !finishIterationAndPrint ;  ) {

                        JSONObject paymentFromArrayCierre = Cierre.getMthis().arrayCierreCut.getJSONObject(Cierre.getMthis().printCounter);

                        if (paymentFromArrayCierre.getDouble("monto") != 0) {
                            Cierre.getMthis().ticketsValue++;

                            TscDll.printerfont( 10, Cierre.getMthis().xValue, "2", 0, 1, 1, paymentFromArrayCierre.getString("no_cliente") );
                            TscDll.printerfont( 160, Cierre.getMthis().xValue, "2", 0, 1, 1, paymentFromArrayCierre.getString("folio") );
                            //TscDll.printerfont( 440, Cierre.getMthis().xValue, "2", 0, 1, 1, paymentFromArrayCierre.getString("copias") );
                            if ( paymentFromArrayCierre.getInt("status") == 7 ) {
                                TscDll.printerfont( 310, Cierre.getMthis().xValue - 10, "4", 0, 1, 1, "Cancelado" );
                            } else {
                                TscDll.printerfont( 310, Cierre.getMthis().xValue, "2", 0, 1, 1,
                                        "$" + formatMoney(Double.
                                                parseDouble(paymentFromArrayCierre.
                                                        getString("monto"))) );

                                if (paymentFromArrayCierre.has("tipo_cobro") && paymentFromArrayCierre.getInt("tipo_cobro") == 2)
                                    TscDll.printerfont(430, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Manual");
                                else if (paymentFromArrayCierre.has("tipo_cobro") && paymentFromArrayCierre.getInt("tipo_cobro") == 3)
                                    TscDll.printerfont(430, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Factura");
                                else if (paymentFromArrayCierre.has("tipo_cobro") && paymentFromArrayCierre.getInt("tipo_cobro") == 4)
                                    TscDll.printerfont(430, CierreInformativo.getMthis().xValue, "2", 0, 1, 1, "Cancel NE");
                            }

                        }
                        else {
                            Cierre.getMthis().ticketsValue++;
                            TscDll.printerfont(10, Cierre.getMthis().xValue, "2", 0, 1, 1,
                                    paymentFromArrayCierre.getString("no_cliente"));
                            //if (paymentFromArrayCierre.getInt("status") == 2) {
                            //    TscDll.printerfont(200, Cierre.getMthis().xValue, "2", 0, 1, 1,
                            //            "NO APORTÓ");
                            if (paymentFromArrayCierre.getInt("status") == 8) {
                                TscDll.printerfont(160, Cierre.getMthis().xValue, "2", 0, 1, 1,
                                        "NO EXISTE CLIENTE");
                            } else if (paymentFromArrayCierre.getInt("status") == 9) {
                                TscDll.printerfont(160, Cierre.getMthis().xValue, "2", 0, 1, 1,
                                        "NO EXISTE CLIENTE");
                            } else if (paymentFromArrayCierre.getInt("status") == 10) {
                                TscDll.printerfont(160, Cierre.getMthis().xValue, "2", 0, 1, 1,
                                        "SUPERVISADO");
                            } else {
                                TscDll.printerfont(160, Cierre.getMthis().xValue, "2", 0, 1, 1,
                                        "NO APORTÓ");
                            }


                        }

                        Cierre.getMthis().xValue = Cierre.getMthis().xValue + 30;

                        if (paymentFromArrayCierre.getInt("status") == 1) {
                            Cierre.getMthis().montoValue += paymentFromArrayCierre.getDouble("monto");
                        }

                        Cierre.getMthis().printCounterToSetSize++;
                        Cierre.getMthis().printCounter++;
                        Cierre.getMthis().restartMonto = true;

                        if( Cierre.getMthis().printCounter == Cierre.getMthis().limitToPrint  ){
                            //Cierre.getMthis().limitToPrint+=400;
                            Cierre.getMthis().printCounter = 0;
                            finishIterationAndPrint = true;
                            Cierre.getMthis().restartMonto = false;

                            TscDll.printerfont( 310, Cierre.getMthis().ticketPosValue, "2", 0, 1, 1, "Tickets " + Cierre.getMthis().ticketsValue );

                        }
                    }

                    if( !finishIterationAndPrint ){
                        TscDll.printerfont( 310, Cierre.getMthis().ticketPosValue, "2", 0, 1, 1, "Tickets " + Cierre.getMthis().ticketsValue );
                        TscDll.printerfont( 185, Cierre.getMthis().xValue, "2", 0, 1, 1, "Subtotal $" + formatMoney(Double.parseDouble("" + Cierre.getMthis().montoValue)) );

                        Cierre.getMthis().xValue = Cierre.getMthis().xValue + 70;

                        Cierre.getMthis().totalesValues.put( company.getName(), Cierre.getMthis().montoValue );

                        Cierre.getMthis().organizationCounter++;
                    }


                }

                Util.Log.ih("finishIterationAndPrint = " + finishIterationAndPrint);

                if( finishIterationAndPrint ){


                    //Util.Log.ih("PRINT STATUS -> antes de imprimir = " + TscDll.status());

                    TscDll.printlabel(1, 1);

                    //Util.Log.ih("PRINT STATUS -> despues de imprimir = " + TscDll.status());

                    //TscDll.closeport();

                    response.result = true;
                    response.finishPrinting = false;

                    publishProgress(ConstantsPabs.FINISHED_PRINTING);

                    return response;
                }
                else{
                    return finishPrintingTicket();
                }
            } catch (Exception e) {

                enableBluetooth();

                try {
                    TscDll.disconnect();
                } catch (Exception e1){
                    e1.printStackTrace();
                }

                e.printStackTrace();
                Util.Log.ih("Intento = " + attempts);
                attempts++;
            }

        }

        response.result = false;

        publishProgress(ConstantsPabs.FINISHED_PRINTING);

        return response;
    }

    /**
     * disables bluetooth
     */
    public void disableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
                .getDefaultAdapter();
        mBluetoothAdapter.disable();
    }

    /**
     * enables bluetooth
     */
    public void enableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Util.Log.ih("enabling bluetooth");
            mBluetoothAdapter.enable();
        }
    }

    /**
     * checks if more than 59 minutes have passed
     * @return
     *          true -> difference is more than limit
     *          false -> difference is under limit
     */
    public boolean checkDate() {
        long newTime = new Date().getTime();
        long differences = Math.abs(dateAux - newTime);
        return (differences > (1000 * 59));
    }

    /**
     * this method gets called to finish printing the ticket
     * this part of the ticket has total amount of companies
     * and deposits.
     *
     * @return PrintMoreThan400PaymentsCierreResponse instance
     */
    public ModelPrintMoreThan400PaymentsCierreResponse finishPrintingTicket()
    {

        ModelPrintMoreThan400PaymentsCierreResponse response = new ModelPrintMoreThan400PaymentsCierreResponse();
        boolean workDone = false;
        int attempts = 0;

        //loop where tries to send info to printer
        while(!workDone && attempts < 5) {
            try {

                if (BuildConfig.isWalletSynchronizationEnabled()) {

                    String[] contractsNotVisited = DatabaseAssistant.getContractsNotVisited();

                    Cierre.getMthis().xValue += 50;
                    TscDll.printerfont(50, Cierre.getMthis().xValue, "2", 0, 1, 1, "NÚMERO DE CONTRATOS NO VISITADOS");
                    Cierre.getMthis().xValue += 50;
                    TscDll.printerfont(10, Cierre.getMthis().xValue, "2", 0, 1, 1, "Semanales");
                    TscDll.printerfont(350, Cierre.getMthis().xValue, "2", 0, 1, 1, contractsNotVisited[0]);
                    Cierre.getMthis().xValue += 30;
                    TscDll.printerfont(10, Cierre.getMthis().xValue, "2", 0, 1, 1, "Quincenales");
                    TscDll.printerfont(350, Cierre.getMthis().xValue, "2", 0, 1, 1, contractsNotVisited[1]);
                    Cierre.getMthis().xValue += 30;
                    TscDll.printerfont(10, Cierre.getMthis().xValue, "2", 0, 1, 1, "Mensuales");
                    TscDll.printerfont(350, Cierre.getMthis().xValue, "2", 0, 1, 1, contractsNotVisited[2]);
                }



                Cierre.getMthis().xValue += 50;

                TscDll.printerfont(10, Cierre.getMthis().xValue, "2", 0, 1, 1, "Efectivo Inicial");
                TscDll.printerfont(350, Cierre.getMthis().xValue, "2", 0, 1, 1, "$" + formatMoney(Double.parseDouble("" + Cierre.getMthis().efectivoInicial)));

                Cierre.getMthis().xValue += 50;

                TscDll.printerfont(350, Cierre.getMthis().xValue, "2", 0, 1, 1, "TOTALES");

                Cierre.getMthis().xValue += 30;

                TscDll.printerfont(10, Cierre.getMthis().xValue, "2", 0, 1, 1, "DEPOSITOS");

                TscDll.printerfont(350, Cierre.getMthis().xValue, "2", 0, 1, 1, "$" + formatMoney(Double.parseDouble("" + Cierre.getMthis().deposito)));

                Cierre.getMthis().xValue += 30;

                TscDll.printerfont(10, Cierre.getMthis().xValue, "2", 0, 1, 1, "EFECTIVO INICIAL");
                TscDll.printerfont(350, Cierre.getMthis().xValue, "2", 0, 1, 1, "$" + formatMoney(Double.parseDouble("" + Cierre.getMthis().efectivoInicial)));

                Cierre.getMthis().xValue += 30;

                TscDll.printerfont(10, Cierre.getMthis().xValue, "2", 0, 1, 1, "EFECTIVO FINAL");

                float totalempresas = 0;

                for (int i = 0; i < Cierre.getMthis().companiesValues.size(); i++) {
                    //JSONObject empresa = Cierre.getMthis().empresasValues.getJSONObject(i);
                    Companies company = Cierre.getMthis().companiesValues.get(i);
                    totalempresas += Cierre.getMthis().totalesValues.getDouble(company.getName());
                }
                double sumaTotal = Cierre.getMthis().deposito + (totalempresas + Cierre.getMthis().efectivoInicial - Cierre.getMthis().deposito);

                TscDll.printerfont(350, Cierre.getMthis().xValue, "2", 0, 1, 1, "$" + formatMoney(sumaTotal - Cierre.getMthis().deposito));

                Cierre.getMthis().xValue += 30;

                TscDll.printerfont(250, Cierre.getMthis().xValue, "2", 0, 1, 1, "TOTAL: $" + formatMoney(Double.parseDouble("" + sumaTotal)));

                Cierre.getMthis().xValue += 30;

                TscDll.printerfont(150, Cierre.getMthis().xValue, "2", 0, 1, 1, "TOTALES POR EMPRESA");

                Cierre.getMthis().xValue += 30;

                //Log.i("PRINT STATUS", TscDll.status());

                float totalfinal = 0;

                for (int i = 0; i < Cierre.getMthis().companiesValues.size(); i++) {
                    //JSONObject empresa = Cierre.getMthis().empresasValues.getJSONObject(i);
                    Companies company = Cierre.getMthis().companiesValues.get(i);

                    TscDll.printerfont(10, Cierre.getMthis().xValue, "2", 0, 1, 1, company.getName());
                    TscDll.printerfont(350, Cierre.getMthis().xValue, "2", 0, 1, 1,
                            "$" + formatMoney(Double.parseDouble(""
                                    + Cierre.getMthis().totalesValues.getDouble(company.getName()))));

                    totalfinal += Cierre.getMthis().totalesValues.getDouble(company.getName());
                    Cierre.getMthis().xValue += 30;
                }

                TscDll.printerfont(150, Cierre.getMthis().xValue, "2", 0, 1, 1, "TOTAL DEL DIA: $" + formatMoney(Double.parseDouble("" + totalfinal)));

                Cierre.getMthis().xValue += 30;

                TscDll.printerfont(150, Cierre.getMthis().xValue, "2", 0, 1, 1, "DEPOSITOS POR EMPRESA");

                Cierre.getMthis().xValue += 30;

                for (int i = 0; i < Cierre.getMthis().companiesValues.size(); i++) {
                    Companies company = Cierre.getMthis().companiesValues.get(i);

                    TscDll.printerfont(10, Cierre.getMthis().xValue, "2", 0, 1, 1, company.getName());
                    TscDll.printerfont(350, Cierre.getMthis().xValue, "2", 0, 1, 1,
                            "$" + formatMoney(Double.parseDouble(""
                                    + DatabaseAssistant.getTotalDepositsByCompany(company.getIdCompany()))));
                    Cierre.getMthis().xValue += 30;

                }

                //Log.i("PRINT STATUS -> antes de imprimir", TscDll.status());

                TscDll.printlabel(1, 1);

                //Log.i("PRINT STATUS -> despues de imprimir", TscDll.status());

                //TscDll.closeport();

                response.result = true;
                response.finishPrinting = true;
                workDone = false;

                publishProgress(ConstantsPabs.FINISHED_PRINTING);

                return response;
            } catch (Exception e) {

                try {
                    TscDll.disconnect();
                } catch (Exception e1){
                    e1.printStackTrace();
                }

                e.printStackTrace();
                attempts++;
            }
        }

        response.result = false;

        publishProgress(ConstantsPabs.FINISHED_PRINTING);

        return response;
    }

    /**
     * formats money
     *
     * @param money
     * 			money type
     * @return
     * 		formatted money
     */
    public String formatMoney(double money) {
        DecimalFormatSymbols nf = new DecimalFormatSymbols();
        nf.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", nf);
        df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);
        return df.format(money);
    }

    /**
     * sharedPreferences file 'efecitvo'
     * with info of every company
     * @return SharedPreferences
     */
    public SharedPreferences getEfectivoPreference(){
        efectivoPreference = context.getSharedPreferences("efecitvo", Context.MODE_PRIVATE);

        return efectivoPreference;
    }
}
