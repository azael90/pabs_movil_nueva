package com.jaguarlabs.pabs.rest;

import android.app.Application;
import android.util.Log;

import com.google.gson.Gson;
import com.jaguarlabs.pabs.activities.Deposito;
import com.jaguarlabs.pabs.rest.models.ModelCancelPaymentResponse;
import com.jaguarlabs.pabs.rest.models.ModelCellPhoneNumbersResponse;
import com.jaguarlabs.pabs.rest.models.ModelGetCollectorsResponse;
import com.jaguarlabs.pabs.rest.models.ModelNoVisitadosResponse;
import com.jaguarlabs.pabs.rest.models.ModelOsticketReponse;
import com.jaguarlabs.pabs.rest.models.ModelSearchForClientResponse;
import com.jaguarlabs.pabs.rest.models.ModelSemaforoResponse;
import com.jaguarlabs.pabs.rest.persisters.GsonObjectPersister;
import com.jaguarlabs.pabs.rest.webService.CancelPaymentService;
import com.jaguarlabs.pabs.rest.webService.CellPhoneNumberService;
import com.jaguarlabs.pabs.rest.webService.ContractsNotVisitedService;
import com.jaguarlabs.pabs.rest.webService.GetCollectorsService;
import com.jaguarlabs.pabs.rest.webService.OsticketService;
import com.jaguarlabs.pabs.rest.webService.SearchForClientService;
import com.jaguarlabs.pabs.rest.webService.SemaforoService;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by irving on 5/03/15.
 */

public class RestService extends RetrofitGsonSpiceService {

    @Override
    public void onCreate(){
        super.onCreate();
        addRetrofitInterface(CancelPaymentService.class);
        addRetrofitInterface(SemaforoService.class);
        addRetrofitInterface(GetCollectorsService.class);
        addRetrofitInterface(SearchForClientService.class);
        addRetrofitInterface(OsticketService.class);
        addRetrofitInterface(CellPhoneNumberService.class);
        addRetrofitInterface(ContractsNotVisitedService.class);
    }


    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        RestAdapter.Builder builder = super.createRestAdapterBuilder();

        builder.setLogLevel(RestAdapter.LogLevel.FULL);
        builder.setLog(message -> Log.i("Rest", message));
//        builder.setClient(new UrlConnectionClient(){
//            @Override
//            protected HttpURLConnection openConnection(Request request) throws IOException {
//                HttpURLConnection connection = super.openConnection(request);
//
//                connection.setConnectTimeout(Util.BuildConfig.REST_TIME_OUT*1000);
//                connection.setReadTimeout(Util.BuildConfig.REST_TIME_OUT*1000);
//
//                return connection;
//            }
//        });
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(90,TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(90, TimeUnit.SECONDS);
        okHttpClient.setRetryOnConnectionFailure(true);
        okHttpClient.setWriteTimeout(90, TimeUnit.SECONDS);

        builder.setClient(new OkClient(okHttpClient));
        return builder;
    }

    @Override
    protected String getServerUrl() {
        return ConstantsPabs.baseURL;
    }

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager manager = new CacheManager();

        GsonObjectPersister<ModelCancelPaymentResponse> persisterCancelTicket = new GsonObjectPersister<ModelCancelPaymentResponse>(application, ModelCancelPaymentResponse.class);
        GsonObjectPersister<ModelSemaforoResponse> persisterSendContractsFromSemaforo = new GsonObjectPersister<ModelSemaforoResponse>(application, ModelSemaforoResponse.class);
        GsonObjectPersister<ModelGetCollectorsResponse> persisterGetCollectors = new GsonObjectPersister<ModelGetCollectorsResponse>(application, ModelGetCollectorsResponse.class);
        GsonObjectPersister<ModelSearchForClientResponse> persisterSearchForClient = new GsonObjectPersister<ModelSearchForClientResponse>(application, ModelSearchForClientResponse.class);
        GsonObjectPersister<ModelOsticketReponse> persisterSendOstickets = new GsonObjectPersister<ModelOsticketReponse>(application, ModelOsticketReponse.class);
        GsonObjectPersister<ModelCellPhoneNumbersResponse> persisterSendCellPhoneNumbers = new GsonObjectPersister<ModelCellPhoneNumbersResponse>(application, ModelCellPhoneNumbersResponse.class);
        GsonObjectPersister<ModelNoVisitadosResponse> persisterContractsNotVisited = new GsonObjectPersister<ModelNoVisitadosResponse>(application, ModelNoVisitadosResponse.class);

        manager.addPersister(persisterCancelTicket);
        manager.addPersister(persisterSendContractsFromSemaforo);
        manager.addPersister(persisterGetCollectors);
        manager.addPersister(persisterSearchForClient);
        manager.addPersister(persisterSendOstickets);
        manager.addPersister(persisterSendCellPhoneNumbers);
        manager.addPersister(persisterContractsNotVisited);

        return manager;
    }



//    @Override
//    public int getThreadCount() {
//        return 10;
//    }
}
