package com.jaguarlabs.pabs.rest.request;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.MontosTotalesDeEfectivo;
import com.jaguarlabs.pabs.rest.models.ModelCancelPaymentRequest;
import com.jaguarlabs.pabs.rest.models.ModelCancelPaymentResponse;
import com.jaguarlabs.pabs.rest.webService.CancelPaymentService;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.LogRegister;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Iterator;

/**
 * Cancel payment request class that runs in a different thread than UI thread.
 * Gets done in background.
 * it's used to cancel a payment using an android service
 *
 * @author Jordan Alvarez
 * @version 23/06/15
 */
public class CancelPaymentRequest extends RetrofitSpiceRequest<ModelCancelPaymentResponse, CancelPaymentService> {
    private ModelCancelPaymentRequest cancelPayment;
    private LogRegister.LogCancelacion cancelacion;
    private SharedPreferences canceladoPreference;
    private SharedPreferences efectivoPreference;
    private SharedPreferences.Editor efectivoEditor;

    private String cancelAmount;
    private String cancelCompany;
    private String cancelCustomerNumber;
    private String cancelDate;
    private String id_motivo_de_cancelacion;

    public CancelPaymentRequest(int paymentNumber, String fecha, LogRegister.LogCancelacion cancelacion, String cancelAmount, String cancelCompany, String cancelCustomerNumber, String cancelDate, int periodo, String id_motivo_de_cancelacion)
    {
        super(ModelCancelPaymentResponse.class, CancelPaymentService.class);
        this.cancelacion = cancelacion;
        this.cancelAmount = cancelAmount;
        this.cancelCompany = cancelCompany;
        this.cancelCustomerNumber = cancelCustomerNumber;
        this.cancelDate = cancelDate;
        this.id_motivo_de_cancelacion = id_motivo_de_cancelacion;


        canceladoPreference   = ApplicationResourcesProvider.getContext().getSharedPreferences("cancelado", Context.MODE_PRIVATE);
        cancelPayment = new ModelCancelPaymentRequest();
        cancelPayment.setPaymentNumber(paymentNumber);
        cancelPayment.setDate(fecha);
        cancelPayment.setPeriodo(periodo);
        cancelPayment.setMotivoCancelacion(id_motivo_de_cancelacion);

        Util.Log.ih("INSTANTIATING");
    }

    @Override
    public ModelCancelPaymentResponse loadDataFromNetwork() throws Exception {
        Util.Log.ih("START REQUEST");
        ModelCancelPaymentResponse response = getService().requestCancelPayment(cancelPayment);
        Util.Log.ih("FINISH REQUEST");
        //implement custom behaviour
        Gson gson = new Gson();

        Util.Log.ih("START MANAGE_CANCELARPAGO");
        canceladoPreference.edit().clear().apply();
        manage_CancelarPago(new JSONObject(gson.toJson(response)), cancelacion, id_motivo_de_cancelacion);

        Util.Log.ih("FINISH MANAGE_CANCELARPAGO");
        return response;
    }

    /**
     * cancelling payment over the internet was done.
     * Now update amount of company that belongs to cancelled payment
     * @param json
     * @param cancelacion
     */
    public void manage_CancelarPago(JSONObject json, LogRegister.LogCancelacion cancelacion, String id_motivo_de_cancelacion)
    {
        Util.Log.ih("json cancel = " + json.toString());

        if (json.has("result"))
        {
            Util.Log.ih("JSON HAS RESULT");
            try {
                //float montoTotal=0;


                //----- starts updating cash -----
                Util.Log.ih("START UPDATING CASH");
                JSONObject result = json.getJSONObject("result");


                float monto = (float) result.getDouble("monto");
                //String empresa = result.getString("empresa");
                Companies company = DatabaseAssistant.getSingleCompany(result.getString("empresa"));

                Log.e("monto a descontar", "" + monto);
                //montoTotal = getEfectivoPreference().getFloat(company.getName(), 0) - monto;
                monto = getEfectivoPreference().getFloat(company.getName(), 0) - monto;
                //monto = //70 - 1



                /*DecimalFormat df = new DecimalFormat();
                df.setGroupingUsed(false);
                df.setMaximumFractionDigits(2);
                monto = Float.parseFloat(df.format(monto));
                Log.e("monto nuevo",""+ monto);*/

                //register cancelled payent in '.txt' file
                cancelacion.fueEnviadoAlServidor = true;
                LogRegister.registrarCancelacion(cancelacion);

                DatabaseAssistant.insertCanceledPayment(cancelAmount, cancelCompany, cancelCustomerNumber, cancelDate, getEfectivoPreference().getInt("periodo", 0), id_motivo_de_cancelacion);


                String queryActualizacion3="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET cobrosMenosDepositos = cobrosMenosDepositos - "+ Float.valueOf(cancelAmount) +" WHERE periodo='"+ getEfectivoPreference().getInt("periodo", 0)+"' and empresa='"+ cancelCompany +"'";
                MontosTotalesDeEfectivo.executeQuery(queryActualizacion3);



                Util.Log.ih("FINISH UPDATING CASH");
                //----- finish updating cash -----



            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {

            try {
                Toast.makeText(ApplicationResourcesProvider.getContext(), json.getString("error"), Toast.LENGTH_LONG).show();
            } catch ( JSONException e ){
                e.printStackTrace();
            } catch ( Exception e ) {
                e.printStackTrace();
            }
        }
    }

    /**
     * sharedPreferences file 'efecitvo'
     * with info of every company
     * @return SharedPreferences
     */
    public SharedPreferences getEfectivoPreference() {
        efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
        return efectivoPreference;
    }

    /**
     * sharedPreferences file 'efecitvo' editor
     * with info of every company
     * @return SharedPreferences.Editor
     */
    public SharedPreferences.Editor getEfectivoEditor() {
        efectivoEditor = getEfectivoPreference().edit();
        return efectivoEditor;
    }


}
