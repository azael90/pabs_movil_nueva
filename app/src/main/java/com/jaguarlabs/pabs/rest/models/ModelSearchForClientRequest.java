package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jordan on 2/09/16.
 */
public class ModelSearchForClientRequest {

    @SerializedName("nombre")
    private String name;
    @SerializedName("contrato")
    private String contract;

    public ModelSearchForClientRequest(String name, String contract){
        this.name = name;
        this.contract = contract;
    }

    /**
     * .........................................
     * .          GETTERS AND SETTERS          .
     * .........................................
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }
}
