package com.jaguarlabs.pabs.rest.webService;

import com.jaguarlabs.pabs.rest.models.ModelCellPhoneNumbersRequest;
import com.jaguarlabs.pabs.rest.models.ModelCellPhoneNumbersResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Jordan Alvarez on 03/04/2019.
 */
public interface CellPhoneNumberService {

    @POST("/controlcartera/addNewCellPhoneNumber")
    public ModelCellPhoneNumbersResponse requestCellPhoneNumber(@Body ModelCellPhoneNumbersRequest body);
}
