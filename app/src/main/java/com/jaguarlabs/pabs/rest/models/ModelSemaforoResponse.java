package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jordan on 4/08/16.
 */
public class ModelSemaforoResponse {

    @SerializedName("resultado")
    String result;
    @SerializedName("error")
    String error;

    /**
     * .........................................
     * .          GETTERS AND SETTERS          .
     * .........................................
     */

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
