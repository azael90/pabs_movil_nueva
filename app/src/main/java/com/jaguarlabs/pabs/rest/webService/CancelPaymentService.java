package com.jaguarlabs.pabs.rest.webService;

import com.jaguarlabs.pabs.rest.models.ModelCancelPaymentRequest;
import com.jaguarlabs.pabs.rest.models.ModelCancelPaymentResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Jordan Alberto on
 *
 * @author Jordan Alvarez
 * @version 23/06/15
 */
public interface CancelPaymentService {

    /**
     * mehod in charge of performing the network call
     * to the next url
     * "http://50.62.56.182/ecobro/controlpagos/cancelPago"
     * using an instance of {@link ModelCancelPaymentRequest}
     * as body
     *
     * @param body
     *          ModelCancelPaymentRequest instance
     * @return
     *         server response as a json string
     */
    @POST("/controlpagos/cancelPago")
    public ModelCancelPaymentResponse requestCancelPayment(@Body ModelCancelPaymentRequest body);
}
