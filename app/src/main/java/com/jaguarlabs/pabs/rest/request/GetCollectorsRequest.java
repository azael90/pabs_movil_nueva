package com.jaguarlabs.pabs.rest.request;

import com.jaguarlabs.pabs.rest.models.ModelGetCollectorsRequest;
import com.jaguarlabs.pabs.rest.models.ModelGetCollectorsResponse;
import com.jaguarlabs.pabs.rest.webService.GetCollectorsService;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by jordan on 26/08/16.
 */
public class GetCollectorsRequest extends RetrofitSpiceRequest<ModelGetCollectorsResponse, GetCollectorsService> {

    ModelGetCollectorsRequest body;

    public GetCollectorsRequest(ModelGetCollectorsRequest body){
        super(ModelGetCollectorsResponse.class, GetCollectorsService.class);

        this.body = body;

        Util.Log.ih("body = " + body.getCollectorNumber());
    }

    @Override
    public ModelGetCollectorsResponse loadDataFromNetwork() throws Exception {
        Util.Log.ih("on loadDataFromNetwork");
        //publishProgress(ConstantsPabs.START_REQUEST);
        return getService().requestGetCollectors(body);
    }
}
