package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jordan on 26/08/16.
 */
public class ModelGetCollectorsResponse {

    @SerializedName("result")
    private List<SingleCollector> collectors;

    public List<SingleCollector> getCollectors() {
        return collectors;
    }

    public void setCollectors(List<SingleCollector> collectors) {
        this.collectors = collectors;
    }

    public class SingleCollector{
        @SerializedName("nombre")
        private String name;
        @SerializedName("codigo")
        private String code;
        @SerializedName("no_cobrador")
        private String collectorNumber;

        /**
         * .........................................
         * .          GETTERS AND SETTERS          .
         * .........................................
         */

        public String getName() {
            return name;
        }

        public void setName(String nombre) {
            this.name = nombre;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCollectorNumber() {
            return collectorNumber;
        }

        public void setCollectorNumber(String collectorNumber) {
            this.collectorNumber = collectorNumber;
        }
    }

}
