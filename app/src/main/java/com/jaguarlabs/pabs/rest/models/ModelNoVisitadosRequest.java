package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jordan Alvarez on 24/06/2019.
 */
public class ModelNoVisitadosRequest {

    @SerializedName("no_cobrador")
    private String debtCollectorNumber;
    @SerializedName("forma_pago")
    private String paymentForm;

    public ModelNoVisitadosRequest(String debtCollectorNumber, String paymentForm) {
        this.debtCollectorNumber = debtCollectorNumber;
        this.paymentForm = paymentForm;
    }

    /**
     * .........................................
     * .          GETTERS AND SETTERS          .
     * .........................................
     */

    public String getDebtCollectorNumber() {
        return debtCollectorNumber;
    }

    public void setDebtCollectorNumber(String debtCollectorNumber) {
        this.debtCollectorNumber = debtCollectorNumber;
    }

    public String getPaymentForm() {
        return paymentForm;
    }

    public void setPaymentForm(String paymentForm) {
        this.paymentForm = paymentForm;
    }
}
