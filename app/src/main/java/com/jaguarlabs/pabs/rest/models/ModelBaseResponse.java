package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

//TODO: DELETE THIS CLASS

/**
 * ModelBaseResponse to inherit from
 * Deprecated in 3.o
 */
public class ModelBaseResponse {

	@SerializedName("codigo")
	private int code;
	@SerializedName("mensaje")
	private String message;
	
	public int getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
