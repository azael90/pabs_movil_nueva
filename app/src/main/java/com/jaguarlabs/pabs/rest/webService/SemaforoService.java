package com.jaguarlabs.pabs.rest.webService;

import com.jaguarlabs.pabs.rest.models.ModelSemaforoRequest;
import com.jaguarlabs.pabs.rest.models.ModelSemaforoResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by jordan on 7/07/16.
 */
public interface SemaforoService {

    @POST("/controlcartera/insertContractsFromSemaforo")
    public ModelSemaforoResponse requestSemaforo(@Body ModelSemaforoRequest body);


}
