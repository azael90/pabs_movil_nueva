package com.jaguarlabs.pabs.rest.request.offline;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.widget.Toast;

import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.components.Preferences;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.rest.models.ModelPrintPaymentRequest;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.SharedConstants;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.SpiceRequest;

import org.json.JSONException;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

public class PrintTemporalSuspensionNotice extends SpiceRequest<Boolean>
{
    private BluetoothPrinter tscDll;
    private Timer timerToStopPrinting;
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat dateHora;
    private Date date;
    private Date dateX;
    private long dateAux;
    private String nombreCobrador="", saldox="", fechaUltimpAbono="", abonado="";
    private String nombreCompleto="", serialAndContract="", motivoSuspencion="", fechaReanudacion="", ticketSuspencionTEXTO="";


    public PrintTemporalSuspensionNotice(String nombreCompleto, String serialAndContract, String motivoSuspencion, String fechaReanudacion, String ticketSuspencionTEXTO,
                                         String nombreCobrador, String saldox, String fechaUltimpAbono, String abonado)
    {
        super(Boolean.class);
        Util.Log.ih("INSTANTIATING");
        this.dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        this.dateHora = new SimpleDateFormat("HH:mm");
        this.date = new Date();
        this.dateX = new Date();
        this.nombreCobrador=nombreCobrador;
        this.saldox=saldox;
        this.fechaUltimpAbono= fechaUltimpAbono;
        this.abonado=abonado;
        this.nombreCompleto=nombreCompleto;
        this.serialAndContract=serialAndContract;
        this.motivoSuspencion=motivoSuspencion;
        this.fechaReanudacion=fechaReanudacion;
        this.ticketSuspencionTEXTO= ticketSuspencionTEXTO;

        Util.Log.ih("FINISH INSTANTIATING");
    }

    @Override
    public Boolean loadDataFromNetwork() throws Exception {
        Util.Log.ih("in loadDataFromNetwork()");
        publishProgress(ConstantsPabs.START_PRINTING);
        return printNotice();
    }

    private boolean printNotice()
    {

        Util.Log.ih("in printAccountStatusTicket()");

        int counter = 0;
        while (counter < 5) {

            try
            {
                int x = 0;
                tscDll = BluetoothPrinter.getInstance();

                if (!BluetoothPrinter.printerConnected)
                    tscDll.connect();

                int size = 175;
                size = size + ((int)(motivoSuspencion.length() / 34) * 5);
                //System.out.println(size);

                Util.Log.ih("despues de openPort");

                tscDll.setup(SharedConstants.Printer.width,
                        size,
                        SharedConstants.Printer.speed,
                        SharedConstants.Printer.density,
                        SharedConstants.Printer.sensorType,
                        SharedConstants.Printer.gapBlackMarkVerticalDistance,
                        SharedConstants.Printer.gapBlackMarkShiftDistance);

                tscDll.clearbuffer();

                tscDll.sendcommand("GAP 0,0\n");
                tscDll.sendcommand("CLS\n");
                tscDll.sendcommand("CODEPAGE UTF-8\n");
                tscDll.sendcommand("SET TEAR ON\n");
                tscDll.sendcommand("SET HEAD ON\n");
                tscDll.sendcommand("SET REPRINT OFF\n");
                tscDll.sendcommand("SET COUNTER @1 1\n");

                //ALL CODE WRITE HERE-----------------------------------------------------------------------------------------------------------

                 for (int conter = 0; conter < 600; conter = conter + 30) {
                    tscDll.printerfont(conter, x, "3", 0, 1, 1, "-");
                }
                x = 30;
                tscDll.printerfont(310, x, "2", 0, 1, 1,   dateHora.format(date).toString() + " " +dateFormat.format(date).toString());
                //scDll.printerfont(400, x + 45, "2", 0, 1, 1, "HORA: " +);
                x += 100;

                String [] mString = new String[((int)ticketSuspencionTEXTO.length() / 38)+1];
                for(int i=0, k=37; i<=(int)ticketSuspencionTEXTO.length() / 38; i++)
                {

                    if( ticketSuspencionTEXTO.substring(k-37,k >= ticketSuspencionTEXTO.length() ? ticketSuspencionTEXTO.length() : k+6 ).charAt(0)=='<')
                    {
                        System.out.println(k);
                        mString[i]= ticketSuspencionTEXTO.substring(k-37,k > ticketSuspencionTEXTO.length() ? ticketSuspencionTEXTO.length() : k+6);
                        k+=43;
                    }
                    else
                    {
                        System.out.println(k);
                        mString[i] = ticketSuspencionTEXTO.substring(k - 37, k > ticketSuspencionTEXTO.length() ? ticketSuspencionTEXTO.length() : k);
                        k+=37;
                    }
                }
                System.out.println(mString);


                String fontSize="2";
                //int caracteresPorRenglon=0, caracteres=0;
                String cadena="";

                for(int p=0; p<=mString.length-1;p++)
                {
                    if (mString[p].charAt(0) == '<' && mString[p].charAt(1)=='4')
                    {
                        fontSize = Character.toString(mString[p].charAt(1));
                        cadena=mString[p].substring(3, mString[0].lastIndexOf("<")).replace("+", "");
                        int longitudTituloPercapita = cadena.length();
                        int caracteresRestantes = 24 - longitudTituloPercapita;
                        int divisionDeCaracteresRestantes = (int) caracteresRestantes / 2;
                        String asteriscosIzquierda = "", asteriscoDerecho = "";

                        for (int i = 0; i <= divisionDeCaracteresRestantes; i++)
                        {
                            asteriscosIzquierda = asteriscosIzquierda + " ";
                            asteriscoDerecho = asteriscoDerecho + " ";
                        }
                        System.out.println(asteriscosIzquierda + cadena + asteriscoDerecho);
                        tscDll.printerfont(10, x, fontSize, 0, 1, 1, asteriscosIzquierda + cadena + asteriscoDerecho);
                        x += 45;

                    }

                    else if (mString[p].charAt(0) == '<' && mString[p].charAt(1)=='3')
                    {
                        fontSize = Character.toString(mString[p].charAt(1));
                        cadena=mString[p].substring(3, mString[0].lastIndexOf("<")).replace("+", "");
                        int longitudTituloPercapita = cadena.length();
                        int caracteresRestantes = 35 - longitudTituloPercapita;
                        int divisionDeCaracteresRestantes = (int) caracteresRestantes / 2;
                        String asteriscosIzquierda = "", asteriscoDerecho = "";

                        for (int i = 0; i <= divisionDeCaracteresRestantes; i++)
                        {
                            asteriscosIzquierda = asteriscosIzquierda + " ";
                            asteriscoDerecho = asteriscoDerecho + " ";
                        }
                        System.out.println(asteriscosIzquierda + cadena + asteriscoDerecho);
                        tscDll.printerfont(10, x, fontSize, 0, 1, 1, asteriscosIzquierda + cadena + asteriscoDerecho);
                        x += 45;

                    }

                    else
                    {
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                        try {
                            dateX = formatter.parse(fechaReanudacion);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        System.out.println(fechaReanudacion+"    "+ dateX);

                        fontSize = "2";
                        if(mString[p].contains("#nombreCompleto"))
                        {
                            String uno="", dos="";
                            int caracteresRestantes=0;
                            cadena= mString[p].replace("#nombreCompleto", "").replace("$", "").replace(" ", "");
                            caracteresRestantes = 37 - cadena.length();
                            for(int i=0; i<=caracteresRestantes-1;i++)
                            {
                                if(i==nombreCompleto.length())
                                {
                                    break;
                                }
                                else
                                    uno=uno + nombreCompleto.charAt(i);
                            }

                            int longitudNombre= nombreCompleto.length();
                            int comienzoCiclo= uno.length();

                            for(int y=comienzoCiclo; y<=longitudNombre-1;y++)
                            {
                                dos=dos + nombreCompleto.charAt(y);
                            }

                            System.out.println(uno + "   "+ dos);

                            if(uno.length()>0)
                            {

                                tscDll.printerfont(10, x, "0", 0, 12, 9, cadena);
                                tscDll.printerfont(cadena.length()+120, x, fontSize, 0, 1, 1,  uno );
                                x += 45;
                            }
                            if(dos.length()>0)
                            {
                                tscDll.printerfont(10, x, fontSize, 0, 1, 1, dos);
                                x += 45;
                            }

                        }
                        else if(mString[p].contains("#contrato"))
                        {
                            String contrato= mString[p].replace("#contrato", "").replace("$", "").replace(" ", "");
                            tscDll.printerfont(10, x, "0", 0, 12, 9, contrato);
                            //tscDll.printerfont(10, x, fontSize, 0, 1, 1, mString[p].replace("#contrato", serialAndContract).replace("$", " "));
                            tscDll.printerfont(contrato.length()+170, x, fontSize, 0, 1, 1, serialAndContract);
                            x += 45;
                        }
                        else if(mString[p].contains("#motivo"))
                        {
                            String uno="", dos="";
                            String cadenaFinal="";
                            String cadena1= mString[p].replace("#motivo", "").replace("$", "").replace(" ", "");
                            int caracteresRestantes1 = 37 - cadena1.length();
                            System.out.println(cadena1);

                            cadenaFinal = cadena1 +" "+ motivoSuspencion;



                            System.out.println(cadenaFinal);
                            tscDll.printerfont(10, x, "0", 0, 12, 9, cadena1);
                            for(int i=0, k=37; i<=(int)cadenaFinal.length() / 37; i++, k+=37)
                            {
                                if(i!=0)
                                {
                                    tscDll.printerfont(10, x, fontSize, 0, 1, 1, cadenaFinal.substring(k - 37, k > cadenaFinal.length() ? cadenaFinal.length() : k).replace("Motivo:", ""));
                                    x += 45;
                                }
                                else
                                {
                                    tscDll.printerfont(cadena1.length()+ 105, x, fontSize, 0, 1, 1, cadenaFinal.substring(k - 37, k > cadenaFinal.length() ? cadenaFinal.length() : k).replace("Motivo:", ""));
                                    x += 45;
                                }

                            }

                            System.out.println("Exit");

                        }
                        else if(mString[p].contains("#fechar"))
                        {
                            String fechar= mString[p].replace("#fechar", "").replace("$", " ");
                            tscDll.printerfont(10, x, "0", 0, 12, 9, fechar);
                            tscDll.printerfont(fechar.length()+280, x, fontSize, 0, 1, 1, formatter.format(dateX));
                            x += 45;
                        }
                        else if(mString[p].contains("#nombreCobrador"))
                        {
                            int longitudNombreCobrador = nombreCobrador.length();
                            int caracteresRestantes = 37 - longitudNombreCobrador;
                            int divisionDeCaracteresRestantes = (int) caracteresRestantes / 2;
                            String asteriscosIzquierda = "", asteriscoDerecho = "";

                            for (int i = 0; i <= divisionDeCaracteresRestantes; i++)
                            {
                                asteriscosIzquierda = asteriscosIzquierda + " ";
                                asteriscoDerecho = asteriscoDerecho + " ";
                            }
                            System.out.println(asteriscosIzquierda + nombreCobrador + asteriscoDerecho);
                            tscDll.printerfont(10, x, fontSize, 0, 1, 1, asteriscosIzquierda + nombreCobrador.toUpperCase() + asteriscoDerecho);
                            x += 10;
                        }
                        else if(mString[p].contains("#saldo"))
                        {
                            String saldillo= mString[p].replace("#saldo", "").replace("$", "").replace(" ", "");
                            tscDll.printerfont(10, x, "0", 0, 12, 9, saldillo);
                            tscDll.printerfont(saldillo.length()+140, x, fontSize, 0, 1, 1, saldox);
                            x += 45;
                        }
                        else if(mString[p].contains("#abonado"))
                        {
                            String abonadillo= mString[p].replace("#abonado", "").replace("$", "").replace(" ", "");
                            tscDll.printerfont(10, x, "0", 0, 12, 9, abonadillo);
                            tscDll.printerfont(abonadillo.length()+140, x, fontSize, 0, 1, 1, abonado);
                            x += 45;
                        }
                        else if(mString[p].contains("#ultimoAbono"))
                        {
                            String ultimoAbono= mString[p].replace("#ultimoAbono", "").replace("$", "").replace(" ", "");
                            tscDll.printerfont(10, x, "0", 0, 12, 9, ultimoAbono);
                            tscDll.printerfont(ultimoAbono.length()+140, x, fontSize, 0, 1, 1, fechaUltimpAbono);
                            x += 45;
                        }
                        else
                        {
                            tscDll.printerfont(10, x, fontSize, 0, 1, 1, mString[p]);
                            x += 45;
                        }

                    }

                }

                for (int conter = 0; conter < 600; conter = conter + 30)
                {
                    tscDll.printerfont(conter, x, "3", 0, 1, 1, "-");
                }


                tscDll.printlabel(1, 1);
                publishProgress(ConstantsPabs.FINISHED_PRINTING);
                return true;
            } catch(Exception e) {

                Util.Log.ih("intento = " + counter);
                e.printStackTrace();
                //enableBluetooth();

                try {
                    tscDll.disconnect();
                    //tscDll.closeport();
                } catch (Exception e1){
                    e1.printStackTrace();
                }

                counter++;
                e.printStackTrace();
            }
        }
        publishProgress(ConstantsPabs.FINISHED_PRINTING);
        return false;
    }

    public boolean checkTime() {
        long newTime = new Date().getTime();
        long differences = Math.abs(dateAux - newTime);
        return (differences > (1000 * 59));
    }

    /**
     * formats money
     *
     * @param money
     * 			money type
     * @return
     * 		formatted money
     */
    public String formatMoney(double money) {
        DecimalFormatSymbols nf = new DecimalFormatSymbols();
        nf.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", nf);
        df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);
        return df.format(money);
    }

    private String formatCellPhoneNumber(String number)
    {
        String formattedNumber;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            formattedNumber = PhoneNumberUtils.formatNumber(number, "MX");
        }
        else {
            formattedNumber = PhoneNumberUtils.formatNumber(number);
        }

        return formattedNumber;
    }
    /**
     * enables bluetooth
     */
    public void enableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Util.Log.ih("enabling bluetooth");
            mBluetoothAdapter.enable();
        }
    }


    public String validarVariables(String variable)
    {


        return "2";
    }

}
