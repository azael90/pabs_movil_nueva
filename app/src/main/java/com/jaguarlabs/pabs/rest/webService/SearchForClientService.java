package com.jaguarlabs.pabs.rest.webService;

import com.jaguarlabs.pabs.rest.models.ModelSearchForClientRequest;
import com.jaguarlabs.pabs.rest.models.ModelSearchForClientResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by jordan on 2/09/16.
 */
public interface SearchForClientService {

    @POST("/controlcartera/searchClient")
    public ModelSearchForClientResponse requestSearchForClient(@Body ModelSearchForClientRequest body);

}
