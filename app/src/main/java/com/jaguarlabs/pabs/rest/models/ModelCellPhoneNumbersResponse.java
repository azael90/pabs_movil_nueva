package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jordan Alvarez on 03/04/2019.
 */
public class ModelCellPhoneNumbersResponse {

    @SerializedName("resultado")
    private List<ModelCellPhoneNumbersResponse.CellPhoneNumberAdded> result;
    @SerializedName("error")
    private String error;

    /**
     * .........................................
     * .          GETTERS AND SETTERS          .
     * .........................................
     */

    public List<ModelCellPhoneNumbersResponse.CellPhoneNumberAdded> getResult() {
        return result;
    }

    public void setResult( List<ModelCellPhoneNumbersResponse.CellPhoneNumberAdded> result ) {
        this.result = result;
    }


    public class CellPhoneNumberAdded {

        String cellPhoneNumberID;

        /**
         * .........................................
         * .          GETTERS AND SETTERS          .
         * .........................................
         */

        public String getCellPhoneNumberID() {
            return cellPhoneNumberID;
        }

        public void setCellPhoneNumberID(String cellPhoneNumberID) {
            this.cellPhoneNumberID = cellPhoneNumberID;
        }
    }

}
