package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jordan Alvarez on 4/21/2017.
 */

public class ModelOsticketRequest {

    @SerializedName("osticket_tickets")
    private ArrayList<OsticketTickets> osticketTickets;
    @SerializedName("no_cobrador")
    int collectorNumber;

    public ArrayList<OsticketTickets> getOsticketTickets() {
        return osticketTickets;
    }

    public void setOsticketTickets(ArrayList<OsticketTickets> osticketTickets) {
        this.osticketTickets = osticketTickets;
    }

    public int getCollectorNumber() {
        return collectorNumber;
    }

    public void setCollectorNumber(int collectorNumber) {
        this.collectorNumber = collectorNumber;
    }

    public static class OsticketTickets {

        @SerializedName("topic")
        String topic;
        @SerializedName("mensaje")
        String message;
        @SerializedName("date_milliseconds")
        long dateMilliseconds;
        @SerializedName("latitude")
        String latitude;
        @SerializedName("longitude")
        String longitude;

        public String getTopic() {
            return topic;
        }

        public void setTopic(String topic) {
            this.topic = topic;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public long getDateMilliseconds() {
            return dateMilliseconds;
        }

        public void setDateMilliseconds(long dateMilliseconds) {
            this.dateMilliseconds = dateMilliseconds;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }
    }
}
