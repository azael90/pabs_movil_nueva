package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jordan on 26/08/16.
 */
public class ModelGetCollectorsRequest {

    @SerializedName("no_cobrador")
    private int collectorNumber;
    @SerializedName("tipo_cobrador")
    private String collectorType;

    public ModelGetCollectorsRequest(int collectorNumber, String collectorType){
        this.collectorNumber = collectorNumber;
        this.collectorType = collectorType;
    }

    /**
     * .........................................
     * .          GETTERS AND SETTERS          .
     * .........................................
     */

    public int getCollectorNumber() {
        return collectorNumber;
    }

    public void setCollectorNumber(int collectorNumber) {
        this.collectorNumber = collectorNumber;
    }

    public String getCollectorType() {
        return collectorType;
    }

    public void setCollectorType(String collectorType) {
        this.collectorType = collectorType;
    }
}
