package com.jaguarlabs.pabs.rest.request.offline;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.util.Log;

import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.database.Notice;
import com.jaguarlabs.pabs.rest.models.ModelNoVisitadosResponse;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.SharedConstants;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.SpiceRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

public class PrintContratosNoVisitadosTicketRequest  extends SpiceRequest<Boolean>
{
    private BluetoothPrinter tscDll;
    private Timer timerToStopPrinting;
    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat hourFormatter;
    private Date date;
    private Date dateX;
    private long dateAux;

    private String fechaInicialSemanal="", fechaFinalSemanal="", fechaInicialQuincenal="", fechaFinalQuincenal="", fechaInicialMensual="", fechaFinalMensual="";

    private Context context;
    private List<ModelNoVisitadosResponse.ContractsNotVisited> noVisitadosArray;
    private String codigoCobrador="", nombreCobrador="", noCobrador="";



    public PrintContratosNoVisitadosTicketRequest(
            List<ModelNoVisitadosResponse.ContractsNotVisited> noVisitadosArray,
            String codigoCobrador,
            String nombreCobrador,
            String noCobrador,
            String fechaInicialSemanal,
            String fechaFinalSemanal,
            String fechaInicialQuincenal,
            String fechaFinalQuincenal,
            String fechaInicialMensual,
            String fechaFinalMensual)
    {
        super(Boolean.class);
        Util.Log.ih("INSTANTIATING");
        this.dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        this.hourFormatter = new SimpleDateFormat("HH:mm");
        this.date = new Date();
        this.dateX = new Date();

        this.noVisitadosArray=noVisitadosArray;
        this.codigoCobrador=codigoCobrador;
        this.nombreCobrador=nombreCobrador;
        this.noCobrador=noCobrador;

        this.fechaInicialSemanal=fechaInicialSemanal;
        this.fechaFinalSemanal=fechaFinalSemanal;
        this.fechaInicialQuincenal=fechaInicialQuincenal;
        this.fechaFinalQuincenal=fechaFinalQuincenal;
        this.fechaInicialMensual=fechaInicialMensual;
        this.fechaFinalMensual=fechaFinalMensual;

        Util.Log.ih("FINISH INSTANTIATING");
    }

    @Override
    public Boolean loadDataFromNetwork() throws Exception {
        Util.Log.ih("in loadDataFromNetwork()");
        publishProgress(ConstantsPabs.START_PRINTING);
        return printNotice();
    }


    private boolean printNotice()
    {

        Util.Log.ih("in printAccountStatusTicket()");

        int counter = 0;
        while (counter < 5) {

            try
            {
                int x = 0;
                tscDll = BluetoothPrinter.getInstance();

                if (!BluetoothPrinter.printerConnected)
                    tscDll.connect();

                //int size = 160;
                int mensuales=0, semanales=0, quincenales=0, totales=0;

               // noVisitadosArray.clear();
                int size = 100;
                size = size + ((int)(noVisitadosArray.size()+2) * 4);
                /*switch (BuildConfig.getTargetBranch()){
                    case GUADALAJARA:
                        size = size * 4 + 260;
                        break;
                    default:
                        size = size * 4 + 247;
                }*/
                //size = size + ((int)(motivoSuspencion.length() / 34) * 5);
                //System.out.println(size);

                Util.Log.ih("despues de openPort");

                tscDll.setup(SharedConstants.Printer.width,
                        size,
                        SharedConstants.Printer.speed,
                        SharedConstants.Printer.density,
                        SharedConstants.Printer.sensorType,
                        SharedConstants.Printer.gapBlackMarkVerticalDistance,
                        SharedConstants.Printer.gapBlackMarkShiftDistance);

                tscDll.clearbuffer();

                tscDll.sendcommand("GAP 0,0\n");
                tscDll.sendcommand("CLS\n");
                tscDll.sendcommand("CODEPAGE UTF-8\n");
                tscDll.sendcommand("SET TEAR ON\n");
                tscDll.sendcommand("SET HEAD ON\n");
                tscDll.sendcommand("SET REPRINT OFF\n");
                tscDll.sendcommand("SET COUNTER @1 1\n");

                //ALL CODE WRITE HERE-----------------------------------------------------------------------------------------------------------

                x = 30;
                tscDll.printerfont(310, x, "2", 0, 1, 1,   dateFormatter.format(date).toString() + " " +hourFormatter.format(date).toString());
                x += 100;
                tscDll.printerfont(17, x, "4", 0, 1, 1, "CONTRATOS NO VISITADOS");
                x+=65;
                tscDll.printerfont(10, x + 10, "2", 0, 1, 1, codigoCobrador);
                tscDll.printerfont(130, x + 10, "2", 0, 1, 1, nombreCobrador);
                x += 130;

                for (int i = 0; i < noVisitadosArray.size(); i++)
                {
                    if(noVisitadosArray.get(i).getPaymentForm().equals("Mensual"))
                    {
                        mensuales++;
                    }
                    else  if(noVisitadosArray.get(i).getPaymentForm().equals("Semanal"))
                    {
                        semanales++;
                    }
                    else
                        quincenales++;

                }




                tscDll.printerfont(10, x, "3", 0, 1, 1, "SEMANALES");
                tscDll.printerfont(310, x, "2", 0, 1, 1, "Registros "+ semanales);
                x += 30;
                tscDll.printerfont(10, x, "2", 0, 1, 1, "DESDE: "+ fechaInicialSemanal.replace("-", "/") + "    HASTA: "+  fechaFinalSemanal.replace("-", "/"));
                x+=30;
                if(semanales<=0)
                {
                    Log.d("IMPRESION -->", "SEMANALES EN 0");
                }
                else
                {
                    tscDll.printerfont(10, x, "2", 0, 1, 1, "Contrato");
                    tscDll.printerfont(160, x, "2", 0, 1, 1, "Fecha último abono");
                    tscDll.printerfont(440, x, "2", 0, 1, 1, "Importe");
                    x += 35;
                }

                for (int i = 0; i < noVisitadosArray.size(); i++)
                {
                    String fecha="";
                    if(noVisitadosArray.get(i).getPaymentForm().equals("Semanal"))
                    {
                        //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-dd-MM");
                        /*SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
                        try
                        {
                            fecha = dateFormatter.format(formatter.parse(noVisitadosArray.get(i).getFirstPaymentDate()));
                            System.out.println(fecha);
                        }
                        catch (ParseException e)
                        {
                            e.printStackTrace();
                        }*/

                        tscDll.printerfont(10, x, "2", 0, 1, 1, noVisitadosArray.get(i).getContractNumber());
                        tscDll.printerfont(220, x, "2", 0, 1, 1, noVisitadosArray.get(i).getFecha_ultimo_abono().replace("-", "/") );
                        tscDll.printerfont(440, x, "2", 0, 1, 1, "$"+formatMoney(Double.parseDouble("" + noVisitadosArray.get(i).getPaymentAmount())));
                        x += 30;
                    }
                    System.out.println(noVisitadosArray.get(i).getContractNumber());
                }





                x += 70;
                tscDll.printerfont(10, x, "3", 0, 1, 1, "QUINCENALES");
                tscDll.printerfont(310, x, "2", 0, 1, 1, "Registros "+ quincenales);
                x += 30;
                tscDll.printerfont(10, x, "2", 0, 1, 1, "DESDE: "+ fechaInicialQuincenal.replace("-", "/") + "    HASTA: "+  fechaFinalQuincenal.replace("-", "/"));
                x+=30;
                if(quincenales<=0)
                {
                    Log.d("IMPRESION -->", "QUINCENALES EN 0");
                }
                else
                {
                    tscDll.printerfont(10, x, "2", 0, 1, 1, "Contrato");
                    tscDll.printerfont(160, x, "2", 0, 1, 1, "Fecha último abono");
                    tscDll.printerfont(440, x, "2", 0, 1, 1, "Importe");
                    x += 35;
                }

                for (int i = 0; i < noVisitadosArray.size(); i++)
                {
                    String fecha="";
                    if(noVisitadosArray.get(i).getPaymentForm().equals("Quincenal"))
                    {

                        tscDll.printerfont(10, x, "2", 0, 1, 1, noVisitadosArray.get(i).getContractNumber());
                        tscDll.printerfont(220, x, "2", 0, 1, 1, noVisitadosArray.get(i).getFecha_ultimo_abono().replace("-", "/") );
                        //tscDll.printerfont(410, x, "2", 0, 1, 1, "$"+noVisitadosArray.get(i).getPaymentAmount());
                        tscDll.printerfont(440, x, "2", 0, 1, 1, "$"+formatMoney(Double.parseDouble("" + noVisitadosArray.get(i).getPaymentAmount())));
                        x += 30;
                    }
                    System.out.println(noVisitadosArray.get(i).getContractNumber());
                }
                x+= 70;

                tscDll.printerfont(10, x, "3", 0, 1, 1, "MENSUALES");
                tscDll.printerfont(310, x, "2", 0, 1, 1, "Registros "+ mensuales);
                x += 30;
                tscDll.printerfont(10, x, "2", 0, 1, 1, "DESDE: "+ fechaInicialMensual.replace("-", "/") + "    HASTA: "+  fechaFinalMensual.replace("-", "/"));
                x+=30;
                if(mensuales<=0)
                {
                    Log.d("IMPRESION -->", "MENSUALES EN 0");
                }
                else
                {
                    tscDll.printerfont(10, x, "2", 0, 1, 1, "Contrato");
                    tscDll.printerfont(160, x, "2", 0, 1, 1, "Fecha último abono");
                    tscDll.printerfont(440, x, "2", 0, 1, 1, "Importe");
                    x += 35;
                }

                for (int i = 0; i < noVisitadosArray.size(); i++)
                {
                    String fecha="";
                    if(noVisitadosArray.get(i).getPaymentForm().equals("Mensual"))
                    {
                        tscDll.printerfont(10, x, "2", 0, 1, 1, noVisitadosArray.get(i).getContractNumber());
                        tscDll.printerfont(220, x, "2", 0, 1, 1, noVisitadosArray.get(i).getFecha_ultimo_abono().replace("-", "/") );
                        //tscDll.printerfont(410, x, "2", 0, 1, 1, "$"+noVisitadosArray.get(i).getPaymentAmount());
                        tscDll.printerfont(440, x, "2", 0, 1, 1, "$"+formatMoney(Double.parseDouble("" + noVisitadosArray.get(i).getPaymentAmount())));
                        x += 30;
                    }
                    System.out.println(noVisitadosArray.get(i).getContractNumber());
                }



                tscDll.printlabel(1, 1);
                publishProgress(ConstantsPabs.FINISHED_PRINTING);
                return true;
            } catch(Exception e) {

                Util.Log.ih("intento = " + counter);
                e.printStackTrace();
                //enableBluetooth();

                try {
                    tscDll.disconnect();
                    //tscDll.closeport();
                } catch (Exception e1){
                    e1.printStackTrace();
                }

                counter++;
                e.printStackTrace();
            }
        }
        publishProgress(ConstantsPabs.FINISHED_PRINTING);
        return false;
    }

    public boolean checkTime() {
        long newTime = new Date().getTime();
        long differences = Math.abs(dateAux - newTime);
        return (differences > (1000 * 59));
    }

    /**
     * formats money
     *
     * @param money
     * 			money type
     * @return
     * 		formatted money
     */
    public String formatMoney(double money) {
        DecimalFormatSymbols nf = new DecimalFormatSymbols();
        nf.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", nf);
        df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);
        return df.format(money);
    }

    private String formatCellPhoneNumber(String number)
    {
        String formattedNumber;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            formattedNumber = PhoneNumberUtils.formatNumber(number, "MX");
        }
        else {
            formattedNumber = PhoneNumberUtils.formatNumber(number);
        }

        return formattedNumber;
    }
    /**
     * enables bluetooth
     */
    public void enableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Util.Log.ih("enabling bluetooth");
            mBluetoothAdapter.enable();
        }
    }


    public String validarVariables(String variable)
    {


        return "2";
    }


}
