package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Model request POJO class for sending contracts from semaforo.
 *
 * @author Jordan Alvarez
 * @version 07/07/16
 */
@SuppressWarnings("SpellCheckingInspection")
public class ModelSemaforoRequest {

    @SerializedName("semaforo_contracts")
    private ArrayList<semaforoContract> semaforoContracts;
    @SerializedName("no_cobrador")
    int collectorNumber;
    @SerializedName("periodo")
    int periodo;

    /**
     * .........................................
     * .          GETTERS AND SETTERS          .
     * .........................................
     */

    public ArrayList<semaforoContract> getSemaforoContracts(){
        return semaforoContracts;
    }

    public ModelSemaforoRequest setSemaforoContracts(ArrayList<semaforoContract> semaforoContracts){
        this.semaforoContracts = semaforoContracts;
        return this;
    }

    public int getCollectorNumber() {
        return collectorNumber;
    }

    public ModelSemaforoRequest setCollectorNumber(int collectorNumber) {
        this.collectorNumber = collectorNumber;
        return this;
    }

    public int getPeriodo() {
        return periodo;
    }

    public ModelSemaforoRequest setPeriodo(int periodo) {
        this.periodo = periodo;
        return this;
    }

    public static class semaforoContract{

        @SerializedName("no_contrato")
        String contractNumber;
        @SerializedName("id_contrato")
        int contractID;
        @SerializedName("status")
        int statusSemaforo;

        /**
         * .........................................
         * .          GETTERS AND SETTERS          .
         * .........................................
         */

        public String getContractNumber() {
            return contractNumber;
        }

        public semaforoContract setContractNumber(String contractNumber) {
            this.contractNumber = contractNumber;
            return this;
        }

        public int getContractID() {
            return contractID;
        }

        public semaforoContract setContractID(int contractID) {
            this.contractID = contractID;
            return this;
        }



        public int getStatusSemaforo() {
            return statusSemaforo;
        }

        public semaforoContract setStatusSemaforo(int statusSemaforo) {
            this.statusSemaforo = statusSemaforo;
            return this;
        }
    }
}
