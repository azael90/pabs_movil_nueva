package com.jaguarlabs.pabs.rest.request;

import com.jaguarlabs.pabs.rest.models.ModelNoVisitadosRequest;
import com.jaguarlabs.pabs.rest.models.ModelNoVisitadosResponse;
import com.jaguarlabs.pabs.rest.webService.ContractsNotVisitedService;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by Jordan Alvarez on 24/06/2019.
 */
public class ContractsNotVisitedRequest extends RetrofitSpiceRequest<ModelNoVisitadosResponse, ContractsNotVisitedService> {

    ModelNoVisitadosRequest body;

    public ContractsNotVisitedRequest(ModelNoVisitadosRequest body){
        super(ModelNoVisitadosResponse.class, ContractsNotVisitedService.class);

        this.body = body;
    }




    @Override
    public ModelNoVisitadosResponse loadDataFromNetwork() throws Exception {
        Util.Log.ih("on loadDataFromNetwork");
        publishProgress(ConstantsPabs.START_REQUEST);
        ModelNoVisitadosResponse modelNoVisitadosResponse = getService().requestSearchForClient(body);
        publishProgress(ConstantsPabs.FINISHED_REQUEST);
        return modelNoVisitadosResponse;

    }
}