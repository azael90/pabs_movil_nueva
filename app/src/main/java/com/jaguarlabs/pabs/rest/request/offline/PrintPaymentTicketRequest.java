package com.jaguarlabs.pabs.rest.request.offline;

import android.bluetooth.BluetoothAdapter;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.util.Log;

import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinterHelper;
import com.jaguarlabs.pabs.components.Preferences;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.rest.models.ModelPrintPaymentRequest;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.LogRegister;
import com.jaguarlabs.pabs.util.SharedConstants;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.SpiceRequest;

import org.json.JSONException;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

/**
 * Created by jordan on 15/12/15.
 */
public class PrintPaymentTicketRequest extends SpiceRequest<Boolean> {

    //private TSCActivity tscDll;
    private BluetoothPrinter tscDll;

    private Timer timerToStopPrinting;

    private SimpleDateFormat dateFormat;
    private SimpleDateFormat dateHora;
    private Date date;
    private long dateAux;

    private ModelPrintPaymentRequest modelPrintPaymentRequest;

    public PrintPaymentTicketRequest(ModelPrintPaymentRequest modelPrintPaymentRequest)
    {
        super(Boolean.class);
        Util.Log.ih("INSTANTIATING");

        //this.tscDll = new TSCActivity();
        this.dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        this.dateHora = new SimpleDateFormat("HH:mm");
        this.date = new Date();

        this.modelPrintPaymentRequest = modelPrintPaymentRequest;
        Util.Log.ih("FINISH INSTANTIATING");
    }

    @Override
    public Boolean loadDataFromNetwork() throws Exception
    {
        Util.Log.ih("in loadDataFromNetwork()");

        publishProgress(ConstantsPabs.START_PRINTING);

        //set payment sync
        //if method does not reach this line
        //when "Clientes" screen shows up
        //a dialog will appear telling that
        //it was not possible to print ticket
        //asking to select same contract and
        //try to print ticket again
        //setPaymentSync();
        //return true;
        return printPaymentTicket();
    }

    public boolean printPaymentTicket()
    {
        Util.Log.ih("in printPaymentTicket()");

        int counter = 0;

        String seriecobrador = "";


        try {
            switch (com.jaguarlabs.pabs.util.BuildConfig.getTargetBranch()) {
                case NAYARIT:case MEXICALI:case MORELIA: case TORREON:
                    if (modelPrintPaymentRequest.getJsonInfo().getString("id_grupo_base").equals("01")) {
                        seriecobrador = modelPrintPaymentRequest.getDatosCobrador()
                                .getString("serie_programa");
                    } else if (modelPrintPaymentRequest.getJsonInfo().getString("id_grupo_base").equals("03")) {
                        seriecobrador = modelPrintPaymentRequest.getDatosCobrador()
                                .getString("serie_malba");
                    } else if (modelPrintPaymentRequest.getJsonInfo().getString("id_grupo_base").equals("04")) {
                        seriecobrador = modelPrintPaymentRequest.getDatosCobrador()
                                .getString("serie_empresa4");
                    } else {
                        seriecobrador = "";
                    }
                    break;
                default:
                    seriecobrador = modelPrintPaymentRequest.getDatosCobrador().getString(
                            DatabaseAssistant.getSingleCompany(modelPrintPaymentRequest.getJsonInfo().getString("id_grupo_base"))
                                    .getName());
            }

        } catch (JSONException e) {
            e.printStackTrace();
            seriecobrador = "";
            LogRegister.registrarSemaforo("Registrado: " + e.toString());
        }



        //loop where tries to send info to printer
        while (counter < 5) {

            //TSCActivity TscDll = new TSCActivity();

            Log.d("PrinterCounter", Integer.toString(counter));

            try {
                int x = 0;
                tscDll = BluetoothPrinter.getInstance();

                if (!BluetoothPrinter.printerConnected)
                    tscDll.connect();

                //modelPrintPaymentRequest.setMensaje("Esta herramienta es útil para sus tweets en Twitter, y también para una multitud de otras aplicaciones.");
                //modelPrintPaymentRequest.setMensaje("En PABS estamos para servirle...");
                int size = 165;

                if(BuildConfig.getTargetBranch()==BuildConfig.Branches.GUADALAJARA)
                {
                    size= size + ((int)(modelPrintPaymentRequest.getMensajepercapita().length() / 34) * 5);
                    System.out.println(size);
                }
                else
                {
                    size=160;
                }

                //size = 50;

                tscDll.setup(SharedConstants.Printer.width,
                        size,
                        SharedConstants.Printer.speed,
                        SharedConstants.Printer.density,
                        SharedConstants.Printer.sensorType,
                        SharedConstants.Printer.gapBlackMarkVerticalDistance,
                        SharedConstants.Printer.gapBlackMarkShiftDistance);

                tscDll.clearbuffer();
                //tscDll.flushOutStream();
                tscDll.sendcommand("GAP 0,0\n");
                tscDll.sendcommand("CLS\n");
                tscDll.sendcommand("CODEPAGE UTF-8\n");
                tscDll.sendcommand("SET TEAR ON\n");
                tscDll.sendcommand("SET HEAD ON\n");
                tscDll.sendcommand("SET REPRINT OFF\n");
                tscDll.sendcommand("SET COUNTER @1 1\n");

                //Util.Log.ih("Despues de OpenPort2");

                //everything that is sent to printer
                //has to be done with coordenates
                for (int conter = 0; conter < 600; conter = conter + 30) {
                    tscDll.printerfont(conter, x, "3", 0, 1, 1, "-");
                }
                x = 30;
                tscDll.printerfont(10, x + 45, "2", 0, 1, 1, "FOLIO: " + seriecobrador + modelPrintPaymentRequest.getNew_folio());
                tscDll.printerfont(400, x, "2", 0, 1, 1, dateFormat.format(date).toString());
                tscDll.printerfont(400, x + 45, "2", 0, 1, 1, "HORA: " + dateHora.format(date).toString());

                x += 100;
                String companynameMask = ExtendedActivity.getCompanyNameFormatted( modelPrintPaymentRequest.getEmpresaString() );
                tscDll.printerfont(200, x, "3", 0, 1, 1, companynameMask);//Empresa

                x += 55;
                tscDll.printerfont(100, x, "2", 0, 1, 1, modelPrintPaymentRequest.getDatoempresa1());

                x += 45;
                tscDll.printerfont(100, x, "2", 0, 1, 1, modelPrintPaymentRequest.getDatoempresa2());

                if (BuildConfig.getTargetBranch() != BuildConfig.Branches.SALTILLO && BuildConfig.getTargetBranch() != BuildConfig.Branches.CANCUN && BuildConfig.getTargetBranch() != BuildConfig.Branches.QUERETARO
                        && BuildConfig.getTargetBranch() != BuildConfig.Branches.IRAPUATO && BuildConfig.getTargetBranch() != BuildConfig.Branches.SALAMANCA && BuildConfig.getTargetBranch() != BuildConfig.Branches.CELAYA
                        && BuildConfig.getTargetBranch() != BuildConfig.Branches.LEON) { //Si es difernete de Saltillo y cancun imprime el RFC

                    x += 45;
                    tscDll.printerfont(100, x, "2", 0, 1, 1, modelPrintPaymentRequest.getRfc());
                }

                x += 45;
				/*
				Nayarit has 7 lines of address
				Other states/cities have 5 lines
				Otherwise the ticket will have blanks
				 */
                int length = 5;
                for (int i = 0; i < length; i++) {
                    tscDll.printerfont(100, x, "2", 0, 1, 1, modelPrintPaymentRequest.getDireccion()[i]);
                    x += 45;
                }

               if (BuildConfig.getTargetBranch() != BuildConfig.Branches.SALTILLO && BuildConfig.getTargetBranch() != BuildConfig.Branches.NAYARIT && BuildConfig.getTargetBranch() != BuildConfig.Branches.MEXICALI && BuildConfig.getTargetBranch() != BuildConfig.Branches.MORELIA && BuildConfig.getTargetBranch() != BuildConfig.Branches.TORREON) {
                    tscDll.printerfont(100, x, "2", 0, 1, 1, modelPrintPaymentRequest.getDatoempresa6());
                    x += 45;
                } else if (BuildConfig.getTargetBranch() != BuildConfig.Branches.CANCUN && BuildConfig.getTargetBranch() != BuildConfig.Branches.NAYARIT && BuildConfig.getTargetBranch() != BuildConfig.Branches.MEXICALI && BuildConfig.getTargetBranch() != BuildConfig.Branches.MORELIA  && BuildConfig.getTargetBranch() != BuildConfig.Branches.TORREON) {
                    tscDll.printerfont(100, x, "2", 0, 1, 1, modelPrintPaymentRequest.getDatoempresa6());
                    x += 45;
                } else if (BuildConfig.getTargetBranch() != BuildConfig.Branches.NAYARIT && BuildConfig.getTargetBranch() != BuildConfig.Branches.MEXICALI && BuildConfig.getTargetBranch() != BuildConfig.Branches.MORELIA && BuildConfig.getTargetBranch() != BuildConfig.Branches.TORREON) {
                    tscDll.printerfont(100, x, "2", 0, 1, 1, modelPrintPaymentRequest.getDatoempresa6());
                    x += 45;
                }
                else {
                    tscDll.printerfont(100, x, "2", 0, 1, 1, modelPrintPaymentRequest.getDatoempresa6());
                    x += 45;
                }

                if (modelPrintPaymentRequest.getImpreso() == 1) {
                    tscDll.printerfont(50, 280, "5", 90, 1, 1, " - COPIA -");
                }
                x += 45;

                tscDll.printerfont(10, x, "2", 0, 1, 1, "El saldo de su contrato al día ");
                x += 45;

                //formats amount paid
                double saldo = Double.parseDouble(modelPrintPaymentRequest.getSaldo()) - Double.parseDouble(modelPrintPaymentRequest.getTotal());

                String formattedDecimal = "";
                try {
                    DecimalFormat digitsFormatter = new DecimalFormat("#,###,###");
                    formattedDecimal = digitsFormatter.format(Double.parseDouble(modelPrintPaymentRequest.getSaldo()));
                } catch (NumberFormatException e) {
                    formattedDecimal = "----";
                    LogRegister.registrarSemaforo("Registrado: " + e.toString());
                }

                tscDll.printerfont(10, x, "2", 0, 1, 1, dateFormat.format(date) + " a las 6 am, es de   ");
                tscDll.printerfont(410, x, "3", 0, 1, 1, "$" + formattedDecimal);
                x += 45;

                if (saldo >= 0) {
                    tscDll.printerfont(10, x, "2", 0, 1, 1, "menos el monto de hoy (" + "$"
                            + formatMoney(Double.parseDouble(modelPrintPaymentRequest.getTotal())) + "): ");
                    x += 45;

                    //formats amount paid
                    String formattedDecimalTotal = "";
                    try {
                        DecimalFormat digitsFormatter = new DecimalFormat("#,###,###");
                        formattedDecimalTotal = digitsFormatter.format(saldo);
                    } catch (NumberFormatException e) {
                        formattedDecimalTotal = "----";
                    }
                    tscDll.printerfont(205, x, "3", 0, 1, 1, "$" + formattedDecimalTotal);
                    x += 60;
                }

                String nombre = modelPrintPaymentRequest.getJsonInfo().getString("nombre");
                String apellido = modelPrintPaymentRequest.getJsonInfo().getString("apellido_pat")
                        + " "
                        + modelPrintPaymentRequest.getJsonInfo().getString("apellido_mat");

                if ((nombre + apellido).length() < 33) {
                    tscDll.printerfont(10, x, "2", 0, 1, 1, "Cliente: " + nombre + " " + apellido);
                    x += 60;
                } else {
                    tscDll.printerfont(10, x, "2", 0, 1, 1, "Cliente: " + nombre);
                    x += 30;
                    tscDll.printerfont(10, x, "2", 0, 1, 1, apellido);
                    x += 30;
                }

                tscDll.printerfont(10, x, "2", 0, 1, 1, "Número de contrato: "
                        + modelPrintPaymentRequest.getJsonInfo().getString("serie")
                        + modelPrintPaymentRequest.getJsonInfo().getString("no_contrato"));

                x += 45;
                tscDll.printerfont(150, x, "2", 0, 1, 1, "Monto de Aportación:");

                x += 35;
                tscDll.printerfont(200, x, "4", 0, 1, 1, "$"
                        + formatMoney(Double.parseDouble(modelPrintPaymentRequest.getTotal())));

                x += 60;
                if (BuildConfig.getTargetBranch() == BuildConfig.Branches.QUERETARO || BuildConfig.getTargetBranch() == BuildConfig.Branches.IRAPUATO ||
                        BuildConfig.getTargetBranch() == BuildConfig.Branches.SALAMANCA || BuildConfig.getTargetBranch() == BuildConfig.Branches.CELAYA || BuildConfig.getTargetBranch() == BuildConfig.Branches.LEON)
                {
                    tscDll.printerfont(10, x, "2", 0, 1, 1, "Atendió: " + modelPrintPaymentRequest.getDatosCobrador().getString("no_cobrador"));
                }
                else
                {
                    if (modelPrintPaymentRequest.getDatosCobrador().getString("nombre").length() > 35)
                    {
                        tscDll.printerfont(10, x, "2", 0, 1, 1, "Atendió: ");
                        x += 30;
                        tscDll.printerfont(10, x, "2", 0, 1, 1, modelPrintPaymentRequest.getDatosCobrador().getString("nombre"));
                    }
                    else
                    {
                        tscDll.printerfont(10, x, "2", 0, 1, 1, "Atendió: " + modelPrintPaymentRequest.getDatosCobrador().getString("nombre"));
                    }
                }

                if (modelPrintPaymentRequest.getDatosCobrador().has("telefono") && !modelPrintPaymentRequest.getDatosCobrador().getString("telefono").equals("") && !modelPrintPaymentRequest.getDatosCobrador().getString("telefono").equals("null"))
                {
                    x += 60;
                    tscDll.printerfont(10, x, "2", 0, 1, 1, "Celular: " + formatCellPhoneNumber(modelPrintPaymentRequest.getDatosCobrador().getString("telefono")));
                }


                String [] mStringMensaje = new String[((int)modelPrintPaymentRequest.getMensaje().length() / 38)+1];

                for(int i=0, k=37; i<=(int)modelPrintPaymentRequest.getMensaje().length() / 38; i++, k+=37)
                {
                        mStringMensaje[i] = modelPrintPaymentRequest.getMensaje().substring(k - 37, k > modelPrintPaymentRequest.getMensaje().length() ? modelPrintPaymentRequest.getMensaje().length() : k);
                }

                int y = x + 60;
                x += 45;
                for(int p=0; p<=mStringMensaje.length-1;p++)
                {
                    tscDll.printerfont(10, x, "2", 0, 1, 1, mStringMensaje[p]);
                    x += 25;
                }


                if(BuildConfig.getTargetBranch()==BuildConfig.Branches.GUADALAJARA)
                {
                    if(modelPrintPaymentRequest.getMensajepercapita().length()<=0)
                    {
                        Log.d("IMPRESION -->", "SIN MENSAJE");
                    }
                    else
                    {
                        if(modelPrintPaymentRequest.getTituloPercapita().length() <=0)
                        {
                            Log.d("IMPRESION -->", "SIN MENSAJE");
                        }
                        else if (modelPrintPaymentRequest.getTituloPercapita().length() >=1 && modelPrintPaymentRequest.getTituloPercapita().length() < 34)
                        {
                            int longitudTituloPercapita = modelPrintPaymentRequest.getTituloPercapita().length();
                            int caracteresRestantes = 34 - longitudTituloPercapita;
                            int divisionDeCaracteresRestantes = (int) caracteresRestantes / 2;
                            String asteriscosIzquierda = "", asteriscoDerecho = "";

                            for (int i = 0; i <= divisionDeCaracteresRestantes; i++)
                            {
                                asteriscosIzquierda = asteriscosIzquierda + "*";
                                asteriscoDerecho = asteriscoDerecho + "*";
                            }

                            x += 45;
                            tscDll.printerfont(10, x, "3", 0, 1, 1, asteriscosIzquierda + modelPrintPaymentRequest.getTituloPercapita() + asteriscoDerecho);
                        } else {
                            x += 75;
                            tscDll.printerfont(10, x, "3", 0, 1, 1, modelPrintPaymentRequest.getTituloPercapita());
                        }


                       /* if (modelPrintPaymentRequest.getMensajepercapita().length() > 0 && modelPrintPaymentRequest.getMensajepercapita().length() <=34)
                        {
                            x += 35;
                            tscDll.printerfont(10, x, "3", 0, 1, 1,modelPrintPaymentRequest.getMensajepercapita());
                            x += 35;
                            tscDll.printerfont(10, x, "3", 0, 1, 1, "**************************************");
                        }
                        else if (modelPrintPaymentRequest.getMensajepercapita().length() > 34)
                        {*/
                            String [] mString = new String[((int)modelPrintPaymentRequest.getMensajepercapita().length() / 35)+1];
                            for(int i=0, k=34; i<=(int)modelPrintPaymentRequest.getMensajepercapita().length() / 35; i++, k+=34)
                            {
                                mString[i] = modelPrintPaymentRequest.getMensajepercapita().substring(k - 34, k > modelPrintPaymentRequest.getMensajepercapita().length() ? modelPrintPaymentRequest.getMensajepercapita().length() : k);
                            }
                            x+=45;
                            for(int p=0; p<=mString.length-1;p++)
                            {
                                tscDll.printerfont(10, x, "3", 0, 1, 1, mString[p]);
                                x += 35;
                            }
                            x += 35;
                            tscDll.printerfont(10, x, "3", 0, 1, 1, "**************************************");
                        //}
                    }
                }
                else {
                    y += 60;
                    for (int conter = 0; conter < 600; conter = conter + 30) {
                        tscDll.printerfont(conter, y + 30, "3", 0, 1, 1, "");
                    }
                }

                y += 35;
                for (int conter = 0; conter < 600; conter = conter + 30) {
                    tscDll.printerfont(conter, y + 30, "3", 0, 1, 1, "-");
                }
               // x += 35;
                //tscDll.printerfont(10, x, "3", 0, 1, 1, "---------------------------------------------------");


                modelPrintPaymentRequest.setImprimio(true);
                tscDll.printlabel(1, 1);

                //set payment sync
                //if method does not reach this line
                //when "Clientes" screen shows up
                //a dialog will appear telling that
                //it was not possible to print ticket
                //asking to select same contract and
                //try to print ticket again
                setPaymentSync();

                //TODO: UNCOMMENT WHEN COPIES ENABLED

                if (modelPrintPaymentRequest.getImpreso() == 1) {
                    DatabaseAssistant.setNumberOfCopies(modelPrintPaymentRequest.getNew_folio());
                }

                modelPrintPaymentRequest.setImpreso(modelPrintPaymentRequest.getImpreso() + 1);

                //response.result = true;
                tscDll.setup(SharedConstants.Printer.width,
                        50,
                        SharedConstants.Printer.speed,
                        SharedConstants.Printer.density,
                        SharedConstants.Printer.sensorType,
                        SharedConstants.Printer.gapBlackMarkVerticalDistance,
                        SharedConstants.Printer.gapBlackMarkShiftDistance);

                publishProgress(ConstantsPabs.FINISHED_PRINTING);

                ConstantsPabs.finishPrinting = true;

                return true;
                //}

            } catch (Exception e) {

                    enableBluetooth();

                try {
                    tscDll.disconnect();
                } catch (Exception e1){
                    e1.printStackTrace();
                    LogRegister.registrarSemaforo("Registrado: " + e.toString());
                }

                Util.Log.ih("Intento = " + counter);
                counter++;
                e.printStackTrace();
                LogRegister.registrarSemaforo("Registrado: " + e.toString());
            }
        }
        publishProgress(ConstantsPabs.FINISHED_PRINTING);

        ConstantsPabs.finishPrinting = false;

        return false;
    }

    /**
     * checks if more than 59 minutes have passed
     * @return
     *          true -> difference is more than limit
     *          false -> difference is under limit
     */
    public boolean checkTime() {
        long newTime = new Date().getTime();
        long differences = Math.abs(dateAux - newTime);
        return (differences > (1000 * 59));
    }

    /**
     * formats money
     *
     * @param money
     * 			money type
     * @return
     * 		formatted money
     */
    public String formatMoney(double money) {
        DecimalFormatSymbols nf = new DecimalFormatSymbols();
        nf.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", nf);
        df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);
        return df.format(money);
    }

    private String formatCellPhoneNumber(String number)
    {
        String formattedNumber;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            formattedNumber = PhoneNumberUtils.formatNumber(number, "MX");
        }
        else {
            formattedNumber = PhoneNumberUtils.formatNumber(number);
        }

        return formattedNumber;
    }

    /**
     * set payment sync
     * if method does not reach this line
     * when "Clientes" screen shows up
     * a dialog will appear telling that
     * it was not possible to print ticket
     * asking to select same contract and
     * try to print ticket again
     */
    public void setPaymentSync(){
        Preferences preferencesPendingTciet = new Preferences(ApplicationResourcesProvider.getContext());
        preferencesPendingTciet.setPaymentSync();
    }

    /**
     * enables bluetooth
     */
    public void enableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Util.Log.ih("enabling bluetooth");
            mBluetoothAdapter.enable();
        }
    }
}
