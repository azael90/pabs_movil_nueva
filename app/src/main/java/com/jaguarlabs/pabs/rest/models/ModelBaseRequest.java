package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

//TODO: DELETE THIS CLASS

/**
 * ModelBaseRequest class to inherit from
 * Deprecated in 3.0
 */
public abstract class ModelBaseRequest {
	
	@SerializedName("solicitud")
	private String request;
	
	protected ModelBaseRequest(){
		String requestId = getRequestId();
		
		if (requestId == null || requestId.isEmpty()){
			throw new RuntimeException();
		}
		
		request = getRequestId();
	}
	
	public String getRequest() {
		return request;
	}
	
	protected abstract String getRequestId();
	
}
