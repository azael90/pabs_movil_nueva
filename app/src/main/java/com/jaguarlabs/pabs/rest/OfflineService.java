package com.jaguarlabs.pabs.rest;

import android.app.Application;
import android.content.Context;

import com.jaguarlabs.pabs.rest.models.ModelPrintMoreThan400PaymentsCierreResponse;
import com.jaguarlabs.pabs.rest.persisters.GsonObjectPersister;
import com.octo.android.robospice.SpiceService;
import com.octo.android.robospice.networkstate.NetworkStateChecker;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;

/**
 * Class which is used to run a process in background
 * usin SpiceService
 *
 * Created by irving on 5/03/15.
 */

public class OfflineService extends SpiceService {

    @Override
    public CacheManager createCacheManager( Application application ) throws CacheCreationException {
        CacheManager manager = new CacheManager();

        GsonObjectPersister<Boolean> printTicket = new GsonObjectPersister<Boolean>(application, Boolean.class);
        GsonObjectPersister<ModelPrintMoreThan400PaymentsCierreResponse> persisterPrintMoreThan400PaymentsCierreInformativo = new GsonObjectPersister<ModelPrintMoreThan400PaymentsCierreResponse>(application, ModelPrintMoreThan400PaymentsCierreResponse.class);

        manager.addPersister(printTicket);
        manager.addPersister(persisterPrintMoreThan400PaymentsCierreInformativo);

        return manager;
    }

    @Override
    protected NetworkStateChecker getNetworkStateChecker() {
        return new NetworkStateChecker() {

            @Override
            public boolean isNetworkAvailable( Context context ) {
                return true;
            }

            @Override
            public void checkPermissions( Context context ) {
                // do nothing
            }
        };
    }

}
