package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jordan Alvarez on 03/04/2019.
 */
public class ModelCellPhoneNumbersRequest {

    private ArrayList<ModelCellPhoneNumbersRequest.ContractsCellPhoneNumbers> cellPhoneNumbers;
    @SerializedName("no_cobrador")
    int collectorNumber;

    public ArrayList<ModelCellPhoneNumbersRequest.ContractsCellPhoneNumbers> getCellPhoneNumbers() {
        return cellPhoneNumbers;
    }

    public void setCellPhoneNumbers(ArrayList<ModelCellPhoneNumbersRequest.ContractsCellPhoneNumbers> cellPhoneNumbers) {
        this.cellPhoneNumbers = cellPhoneNumbers;
    }

    public int getCollectorNumber() {
        return collectorNumber;
    }

    public void setCollectorNumber(int collectorNumber) {
        this.collectorNumber = collectorNumber;
    }

    public static class ContractsCellPhoneNumbers {

        long cellPhoneNumberID;
        long contractID;
        String cellPhoneNumber;
        String contractSerie;
        String contractNumber;

        public long getContractID() {
            return contractID;
        }

        public void setContractID(long contractID) {
            this.contractID = contractID;
        }

        public long getCellPhoneNumberID() {
            return cellPhoneNumberID;
        }

        public void setCellPhoneNumberID(long cellPhoneNumberID) {
            this.cellPhoneNumberID = cellPhoneNumberID;
        }

        public String getCellPhoneNumber() {
            return cellPhoneNumber;
        }

        public void setCellPhoneNumber(String cellPhoneNumber) {
            this.cellPhoneNumber = cellPhoneNumber;
        }

        public String getContractSerie() {
            return contractSerie;
        }

        public void setContractSerie(String contractSerie) {
            this.contractSerie = contractSerie;
        }

        public String getContractNumber() {
            return contractNumber;
        }

        public void setContractNumber(String contractNumber) {
            this.contractNumber = contractNumber;
        }
    }
}
