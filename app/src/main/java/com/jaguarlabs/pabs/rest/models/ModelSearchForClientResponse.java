package com.jaguarlabs.pabs.rest.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by jordan on 2/09/16.
 */
public class ModelSearchForClientResponse {

    @SerializedName("result")
    private ArrayList<ContractsFound> result;
    @SerializedName("error")
    private String error;

    public ArrayList<ContractsFound> getResult() {
        return result;
    }

    public void setResult(ArrayList<ContractsFound> result) {
        this.result = result;
    }

    public class ContractsFound {

        @SerializedName("id_contrato")
        private String contractID;
        @SerializedName("serie")
        private String serie;
        @SerializedName("no_contrato")
        private String contractNumber;
        @SerializedName("nombre")
        private String name;
        @SerializedName("apellido_pat")
        private String firstLastName;
        @SerializedName("apellido_mat")
        private String secondLastName;
        @SerializedName("calle_cobro")
        private String streetToPay;
        @SerializedName("no_ext_cobro")
        private String numberExt;
        @SerializedName("colonia")
        private String neighborhood;
        @SerializedName("localidad")
        private String locality;
        @SerializedName("entre_calles")
        private String betweenStreets;
        @SerializedName("forma_pago_actual")
        private String paymentOption;
        @SerializedName("monto_pago_actual")
        private String actualPayment;
        @SerializedName("no_cobrador")
        private String collectorNumber;
        @SerializedName("tipo_bd")
        private String typeBD;
        @SerializedName("estatus")
        private String status;
        @SerializedName("ultimo_abono")
        private String lastPaymentDate;
        @SerializedName("monto_atrasado")
        private String overdueBalance;
        @SerializedName("fecha_primer_abono")
        private String firstPaymentDate;
        @SerializedName("id_grupo_base")
        private String idBaseGroup;
        @SerializedName("Abonado")
        private String payment;
        @SerializedName("Saldo")
        private String balance;
        @SerializedName("contrato_estatus")
        private String contractStatus;
        @SerializedName("fecha_reactiva")
        private String fecha_reactiva;

        /**
         * .........................................
         * .          GETTERS AND SETTERS          .
         * .........................................
         */

        public String getContractID() {
            return contractID;
        }

        public void setContractID(String contractID) {
            this.contractID = contractID;
        }

        public String getSerie() {
            return serie;
        }

        public void setSerie(String serie) {
            this.serie = serie;
        }

        public String getContractNumber() {
            return contractNumber;
        }

        public void setContractNumber(String contractNumber) {
            this.contractNumber = contractNumber;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFirstLastName() {
            return firstLastName;
        }

        public void setFirstLastName(String firstLastName) {
            this.firstLastName = firstLastName;
        }

        public String getSecondLastName() {
            return secondLastName;
        }

        public void setSecondLastName(String secondLastName) {
            this.secondLastName = secondLastName;
        }

        public String getStreetToPay() {
            return streetToPay;
        }

        public void setStreetToPay(String streetToPay) {
            this.streetToPay = streetToPay;
        }

        public String getNumberExt() {
            return numberExt;
        }

        public void setNumberExt(String numberExt) {
            this.numberExt = numberExt;
        }

        public String getNeighborhood() {
            return neighborhood;
        }

        public void setNeighborhood(String neighborhood) {
            this.neighborhood = neighborhood;
        }

        public String getLocality() {
            return locality;
        }

        public void setLocality(String locality) {
            this.locality = locality;
        }

        public String getBetweenStreets() {
            return betweenStreets;
        }

        public void setBetweenStreets(String betweenStreets) {
            this.betweenStreets = betweenStreets;
        }

        public String getPaymentOption() {
            return paymentOption;
        }

        public void setPaymentOption(String paymentOption) {
            this.paymentOption = paymentOption;
        }

        public String getActualPayment() {
            return actualPayment;
        }

        public void setActualPayment(String actualPayment) {
            this.actualPayment = actualPayment;
        }

        public String getCollectorNumber() {
            return collectorNumber;
        }

        public void setCollectorNumber(String collectorNumber) {
            this.collectorNumber = collectorNumber;
        }

        public String getTypeBD() {
            return typeBD;
        }

        public void setTypeBD(String typeBD) {
            this.typeBD = typeBD;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getLastPaymentDate() {
            return lastPaymentDate;
        }

        public void setLastPaymentDate(String lastPaymentDate) {
            this.lastPaymentDate = lastPaymentDate;
        }

        public String getOverdueBalance() {
            return overdueBalance;
        }

        public void setOverdueBalance(String overdueBalance) {
            this.overdueBalance = overdueBalance;
        }

        public String getFirstPaymentDate() {
            return firstPaymentDate;
        }

        public void setFirstPaymentDate(String firstPaymentDate) {
            this.firstPaymentDate = firstPaymentDate;
        }

        public String getIdBaseGroup() {
            return idBaseGroup;
        }

        public void setIdBaseGroup(String idBaseGroup) {
            this.idBaseGroup = idBaseGroup;
        }

        public String getPayment() {
            return payment;
        }

        public void setPayment(String payment) {
            this.payment = payment;
        }

        public String getBalance() {
            return balance;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }

        public String getContractStatus() {
            return contractStatus;
        }

        public void setContractStatus(String contractStatus) {
            this.contractStatus = contractStatus;
        }

        public String getFecha_reactiva() {
            return fecha_reactiva;
        }

        public void setFecha_reactiva(String fecha_reactiva) {
            this.fecha_reactiva = fecha_reactiva;
        }
    }
}
