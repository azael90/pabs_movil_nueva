package com.jaguarlabs.pabs.rest.request.offline;

import android.bluetooth.BluetoothAdapter;
import android.util.Log;

import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinterHelper;
import com.jaguarlabs.pabs.database.Collectors;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.Novisitados;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.SharedConstants;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.SpiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;

/**
 * Print cierre informativo or Periodo request class that runs in a different thread than UI thread.
 * Gets done in background.
 * it's used to print a cierre either 'informativo' or 'de periodo' using an android service
 *
 * @author Jordan Alvarez
 * @version 15/12/15
 */
public class PrintCierreInformativoOPeriodoTicketRequest extends SpiceRequest<Boolean> {

    //private TSCActivity tscDll;
    private BluetoothPrinter tscDll;

    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat hourFormatter;
    private Date date;
    private long dateAux;

    private Timer timerToCancelPrinting;

    private boolean informativo;

    private JSONArray arrayCierre;

    private String collectorId;
    private String collectorName;
    private int initialCash;
    private int period;
    private double deposit;

    public PrintCierreInformativoOPeriodoTicketRequest(boolean informativo, JSONArray arrayCierre, String collectorId, String collectorName, int initialCash, int period, double deposit) {
        super(Boolean.class);

        Util.Log.ih("INSTANTIATING");

        //tscDll = new TSCActivity();

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        hourFormatter = new SimpleDateFormat("HH:mm");
        date = new Date();

        this.informativo = informativo;

        this.arrayCierre = arrayCierre;

        this.collectorId = collectorId;
        this.collectorName = collectorName;
        this.initialCash = initialCash;
        this.period = period;
        this.deposit = deposit;

        Util.Log.ih("FINISH INSTANTIATING");
    }

    @Override
    public Boolean loadDataFromNetwork() throws Exception {
        Util.Log.ih("in loadDataFromNetwork()");
        publishProgress(ConstantsPabs.START_PRINTING);

        return printCierreInformativo();
    }

    /**
     * prints ticket with all payments
     * and visits
     *
     * @return
     * 		true -> succeed
     * 		false -> failed
     */
    private boolean printCierreInformativo(){

        Util.Log.ih("in printCierreInformativo()");

        try {
            Util.Log.ih("0 = " + arrayCierre.getJSONObject(0).getString("tiempo"));//last payment
            Util.Log.ih("max - 1 = " + arrayCierre.getJSONObject( arrayCierre.length() - 1 ).getString("tiempo") );//first payment
        } catch (Exception e) {
            e.printStackTrace();
        }

        int counter = 0;
        while (counter < 5) {

            try {
                if (arrayCierre == null) {
                    arrayCierre = new JSONArray();
                }
                int size = arrayCierre.length();
                //size = size * 4 + 247;

                switch (BuildConfig.getTargetBranch()){
                    case GUADALAJARA: case TAMPICO:
                        size = size * 4 + 260;
                        break;
                    case TOLUCA:
                        size = size * 4 + 182;
                        break;
                    case QUERETARO:
                        size = size * 4 + 182;
                        break;
                    case IRAPUATO:case SALAMANCA: case CELAYA: case LEON:
                        size = size * 4 + 182;
                        break;
                    case PUEBLA:case CUERNAVACA:
                        size = size * 4 + 260;
                        break;
                    case MERIDA:
                        size = size * 4 + 150;
                        break;
                    case SALTILLO:
                        size = size * 4 + 150;
                        break;
                    case CANCUN:
                        size = size * 4 + 150;
                        break;
                    default:
                        size = size * 4 + 247;
                }

                tscDll = BluetoothPrinter.getInstance();

                if (!BluetoothPrinter.printerConnected)
                    tscDll.connect();
                //tscDll = new TSCPrinter();
                //tscDll.openport(DatabaseAssistant.getPrinterMacAddress());

                //dateAux = new Date().getTime();

                //timer that closes printer port if got stuck
                //trying to print
                //has one minute of limit before closing port
                //closing port causes NullPointerException on openPort()
                //so it tries again to print
                /*
                timerToCancelPrinting = new Timer();
                timerToCancelPrinting.schedule(new TimerTask() {

                    int attempt;
                    TSCActivity tscDll;

                    @Override
                    public void run() {
                        if (checkTime()) {
                            Util.Log.ih("Closing port");
                            try {
                                tscDll.closeport();
                            } catch (Exception e){
                                e.printStackTrace();
                                timerToCancelPrinting.cancel();
                            }
                        }
                        Util.Log.ih("Hora, attempt: " + attempt);
                    }

                    public TimerTask setData(int attempt, TSCActivity tscDll) {
                        this.attempt = attempt;
                        this.tscDll = tscDll;
                        return this;
                    }
                }.setData(counter, tscDll), (1000 * 60));
                */

                //tscDll.openport(DatabaseAssistant.getPrinterMacAddress());
                Util.Log.ih("despues de openPort");
                //timerToCancelPrinting.cancel();
                tscDll.setup(SharedConstants.Printer.width,
                        size,
                        SharedConstants.Printer.speed,
                        SharedConstants.Printer.density,
                        SharedConstants.Printer.sensorType,
                        SharedConstants.Printer.gapBlackMarkVerticalDistance,
                        SharedConstants.Printer.gapBlackMarkShiftDistance);

                List<Companies> companies = DatabaseAssistant.getCompanies();

                tscDll.clearbuffer();
                //Util.Log.ih("PRINT STATUS - " + tscDll.status());

                tscDll.sendcommand("GAP 0,0\n");
                tscDll.sendcommand("CLS\n");
                tscDll.sendcommand("CODEPAGE UTF-8\n");
                tscDll.sendcommand("SET TEAR ON\n");
                tscDll.sendcommand("SET HEAD ON\n");
                tscDll.sendcommand("SET REPRINT OFF\n");
                tscDll.sendcommand("SET COUNTER @1 1\n");

                JSONObject totales = new JSONObject();

                if (informativo) {
                    tscDll.printerfont(50, 80, "4", 0, 1, 1, "CIERRE INFORMATIVO");
                } else {
                    tscDll.printerfont(50, 80, "4", 0, 1, 1, "CIERRE DEL PERIODO");
                }
                int x = 140;

                //tscDll.printerfont(10, x, "2", 0, 1, 1, "Del: " + arrayCierre.getJSONObject( 0 ).getString("tiempo"));
                //x += 30;
                //tscDll.printerfont(10, x, "2", 0, 1, 1, "Hasta: " + arrayCierre.getJSONObject( arrayCierre.length() - 1 ).getString("tiempo") );

                //x+= 70;

                int companiesSize;
                switch (BuildConfig.getTargetBranch()) {
                    case GUADALAJARA: case TAMPICO:
                        companiesSize = companies.size();
                        break;
                    case TOLUCA:
                        companiesSize = 4;
                        break;
                    case QUERETARO:
                        companiesSize = 2;
                        break;
                    case IRAPUATO:case SALAMANCA:case CELAYA: case LEON:
                        companiesSize = 2;
                        break;
                    case PUEBLA:case CUERNAVACA:
                        companiesSize = 4;
                        break;
                    case MERIDA:
                        companiesSize = 4;
                        break;
                    case SALTILLO:
                        companiesSize = 4;
                        break;
                    case CANCUN:
                        companiesSize = 4;
                        break;
                    case NAYARIT:
                        companiesSize = 3;
                        break;
                    case MEXICALI: case TORREON:
                        companiesSize = 3;
                        break;
                    case MORELIA:
                        companiesSize = 3;
                        break;
                    default:
                        companiesSize = 4;
                }


                for (int contE = 0; contE < companiesSize; contE++) {//for (int contE = 0; contE < companies.sise(); contE++) {
                    double monto = 0;

                    Companies company = companies.get(contE);
                    Log.e("empresa", company.toString());
                    tscDll.printerfont(400, x + 10, "2", 0, 1, 1, dateFormatter.format(date));
                    tscDll.printerfont(480, x + 70, "2", 0, 1, 1, hourFormatter.format(date));
                    tscDll.printerfont(10, x + 10, "2", 0, 1, 1, collectorId);
                    tscDll.printerfont(10, x + 40, "2", 0, 1, 1, collectorName);
                    tscDll.printerfont(10, x + 70, "2", 0, 1, 1, "Periodo " + period);

                    x += 150;
                    int tickets = 0;
                    String companynameMask = ExtendedActivity.getCompanyNameFormatted( company.getName() );
                    tscDll.printerfont(10, x, "3", 0, 1, 1, companynameMask);

                    int ticketpos = x;
                    x += 30;
                    tscDll.printerfont(10, x, "2", 0, 1, 1, "Contrato");
                    tscDll.printerfont(150, x, "2", 0, 1, 1, "Folio");
                    tscDll.printerfont(350, x, "2", 0, 1, 1, "Importe");
                    //tscDll.printerfont(480, x, "2", 0, 1, 1, "Copias");

                    x += 30;
                    for (int cont = 0; cont < arrayCierre.length(); cont++) {
                        JSONObject json = arrayCierre.getJSONObject(cont);
                        Log.e(json.getString("empresa"), company.getIdCompany());

                        if (json.getString("empresa").equals(company.getIdCompany())) { // Programa

                            Util.Log.ih("contract = " + json.getString("no_cliente"));

                            //Log.e("json", json.toString());
                            if (json.getDouble("monto") != 0) {
                                Util.Log.ih("printing amount...");
                                tickets++;

                                tscDll.printerfont(10, x, "2", 0, 1, 1, json.getString("no_cliente"));
                                tscDll.printerfont(150, x, "2", 0, 1, 1, json.getString("folio"));
                                //tscDll.printerfont(480, x, "2", 0, 1, 1, json.getString("copias"));
                                //Util.Log.ih("json" + json.toString());
                                if (json.has("tipo_cobro") && json.getInt("tipo_cobro") == 2) {
                                    tscDll.printerfont(430, x -10, "4", 0, 1, 1, "Manual");
                                }

                                if (json.getInt("status") == 7) {

                                    tscDll.printerfont(240, x, "2", 0, 1, 1, "$" + formatMoney(Double.parseDouble(json.getString("monto"))));
                                    tscDll.printerfont(350, x - 10, "4", 0, 1, 1,  "Cancelado");

                                } else {
                                    tscDll.printerfont(
                                            350,
                                            x,
                                            "2",
                                            0,
                                            1,
                                            1,
                                            "$"
                                                    + formatMoney(Double.parseDouble(json
                                                    .getString("monto"))));

                                    if (json.has("tipo_cobro") && json.getInt("tipo_cobro") == 3)
                                        tscDll.printerfont(430, x, "2", 0, 1, 1, "Factura");
                                    else if (json.has("tipo_cobro") && json.getInt("tipo_cobro") == 4)
                                        tscDll.printerfont(430, x, "2", 0, 1, 1, "Cancel NE");
                                }
                                Util.Log.ih("finish printing amount...");
                            } else {
                                Util.Log.ih("not printing ampunt");
                                tickets++;
                                tscDll.printerfont(10, x, "2", 0, 1, 1, json.getString("no_cliente"));

                                if (json.getInt("status") == 8) {
                                    tscDll.printerfont(160, x, "2", 0, 1, 1, "NO EXISTE CLIENTE");
                                } else if (json.getInt("status") == 9) {
                                    tscDll.printerfont(160, x, "2", 0, 1, 1, "NO EXISTE DIRECCION");
                                } else if (json.getInt("status") == 10) {
                                    tscDll.printerfont(160, x, "2", 0, 1, 1, "SUPERVISADO");
                                } else {
                                    tscDll.printerfont(160, x, "2", 0, 1, 1, "NO APORTÓ");
                                }
                                Util.Log.ih("finish NOT printing ampunt");
                            }

                            x = x + 30;
                            if (json.getInt("status") == 1) {
                                monto += json.getDouble("monto");
                            }
                        }
                    }
                    tscDll.printerfont(350, ticketpos, "2", 0, 1, 1, "Tickets " + tickets);
                    tscDll.printerfont(185, x, "2", 0, 1, 1, "Subtotal $" + formatMoney(Double.parseDouble("" + monto)));

                    x = x + 70;
                    totales.put(company.getName(), monto);
                }

                if (BuildConfig.isWalletSynchronizationEnabled())
                {

                    String[] contractsNotVisited = DatabaseAssistant.getContractsNotVisited();

                    String sqlSintax="SELECT * FROM NOVISITADOS";
                    List<Novisitados> lista = Novisitados.findWithQuery(Novisitados.class, sqlSintax);
                    if (lista.size() > 0)
                    {
                        //lista.get(0).getMensajeaviso();
                        x += 50;
                        tscDll.printerfont(50, x, "2", 0, 1, 1, "NÚMERO DE CONTRATOS NO VISITADOS");
                        x += 30;
                        tscDll.printerfont(25, x, "2", 0, 1, 1, "FECHA DE ACT: "+ lista.get(0).getFecha());
                        x += 50;
                        tscDll.printerfont(10, x, "2", 0, 1, 1, "Semanales");
                        tscDll.printerfont(350, x, "2", 0, 1, 1, lista.get(0).getSemanales());
                        x += 30;
                        tscDll.printerfont(10, x, "2", 0, 1, 1, "Quincenales");
                        tscDll.printerfont(350, x, "2", 0, 1, 1, lista.get(0).getQuincenales());
                        x += 30;
                        tscDll.printerfont(10, x, "2", 0, 1, 1, "Mensuales");
                        tscDll.printerfont(350, x, "2", 0, 1, 1, lista.get(0).getMensuales());
                        x+=50;


                    }


                }

                /*x += 50;
                tscDll.printerfont(10, x, "2", 0, 1, 1, "Efectivo Inicial");
                tscDll.printerfont(350, x, "2", 0, 1, 1, "$" + formatMoney(Double.parseDouble("" + initialCash)));*/

                x += 50;
                tscDll.printerfont(350, x, "2", 0, 1, 1, "TOTALES");

                x += 30;
                tscDll.printerfont(10, x, "2", 0, 1, 1, "DEPOSITOS");
                tscDll.printerfont(350, x, "2", 0, 1, 1, "$" + formatMoney(Double.parseDouble("" + deposit)));

                /*x += 30;
                tscDll.printerfont(10, x, "2", 0, 1, 1, "EFECTIVO INICIAL");
                tscDll.printerfont(350, x, "2", 0, 1, 1, "$" + formatMoney(Double.parseDouble("" + initialCash)));*/

                x += 30;
                tscDll.printerfont(10, x, "2", 0, 1, 1, "EFECTIVO");





                float totalempresas = 0;
                for (int i = 0; i < companiesSize; i++)
                {
                    Companies company = companies.get(i);
                    totalempresas += totales.getDouble(company.getName());
                }
                double sumaTotal = deposit + (totalempresas + initialCash - deposit);

                tscDll.printerfont(350, x, "2", 0, 1, 1, "$" +formatMoney(sumaTotal- deposit));// formatMoney(sumaTotal - deposit));






                x += 30;
                tscDll.printerfont(250, x, "2", 0, 1, 1, "TOTAL: $" + formatMoney(Double.parseDouble("" + sumaTotal)));

                x += 30;
                tscDll.printerfont(150, x, "2", 0, 1, 1, "TOTALES POR EMPRESA");

                x += 30;
                //Util.Log.ih("PRINT STATUS - " + tscDll.status());
                float totalfinal = 0;
                for (int i = 0; i < companiesSize; i++) {//for (int i = 0; i < companies.size(); i++) {
                    Companies company = companies.get(i);

                    String companynameMask = ExtendedActivity.getCompanyNameFormatted( company.getName() );

                    tscDll.printerfont(10, x, "2", 0, 1, 1, companynameMask);
                    tscDll.printerfont(
                            350,
                            x,
                            "2",
                            0,
                            1,
                            1,
                            "$"
                                    + formatMoney(Double.parseDouble(""
                                    + totales.getDouble(company.getName()))));
                    totalfinal += totales.getDouble(company.getName());
                    x += 30;
                }

                tscDll.printerfont(150, x, "2", 0, 1, 1, "TOTAL DEL DIA: $" + formatMoney(Double.parseDouble("" + totalfinal)));
                x += 30;

                tscDll.printerfont(150, x, "2", 0, 1, 1, "DEPOSITOS POR EMPRESA");
                x += 30;

                for (int i = 0; i < companiesSize; i++)
                {
                    Companies company = companies.get(i);
                    String nombreCompania = ExtendedActivity.getCompanyNameFormatted(company.getName());

                    tscDll.printerfont(10, x, "2", 0, 1, 1, nombreCompania);

                    tscDll.printerfont(350, x, "2", 0, 1, 1, "$"
                            + formatMoney(Double.parseDouble(""
                            + DatabaseAssistant.getTotalDepositosPorPeriodoAndCompany(period, company.getIdCompany()))));
                    x += 30;

                }
                tscDll.printlabel(1, 1);

                //Util.Log.ih("PRINT STATUS - " + tscDll.status());

                //tscDll.closeport();

                tscDll.setup(SharedConstants.Printer.width,
                        50,
                        SharedConstants.Printer.speed,
                        SharedConstants.Printer.density,
                        SharedConstants.Printer.sensorType,
                        SharedConstants.Printer.gapBlackMarkVerticalDistance,
                        SharedConstants.Printer.gapBlackMarkShiftDistance);

                publishProgress(ConstantsPabs.FINISHED_PRINTING);

                return true;
            } catch(Exception e) {

                Util.Log.ih("intento = " + counter);
                //enableBluetooth();

                try {
                    tscDll.disconnect();
                } catch (Exception e1){
                    e1.printStackTrace();
                }

                counter++;
                e.printStackTrace();
            }
        }

        Util.Log.ih("return false -> before publishProgress - finish_printing");

        publishProgress(ConstantsPabs.FINISHED_PRINTING);

        return false;
    }

    /**
     * checks if more than 59 minutes have passed
     * @return
     *          true -> difference is more than limit
     *          false -> difference is under limit
     */
    public boolean checkTime() {
        long newTime = new Date().getTime();
        long differences = Math.abs(dateAux - newTime);
        return (differences > (1000 * 59));
    }

    /**
     * formats money
     *
     * @param money
     * 			money type
     * @return
     * 		formatted money
     */
    public String formatMoney(double money) {
        DecimalFormatSymbols nf = new DecimalFormatSymbols();
        nf.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", nf);
        df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);
        return df.format(money);
    }

    /**
     * enables bluetooth
     */
    public void enableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Util.Log.ih("enabling bluetooth");
            mBluetoothAdapter.enable();
        }
    }
}