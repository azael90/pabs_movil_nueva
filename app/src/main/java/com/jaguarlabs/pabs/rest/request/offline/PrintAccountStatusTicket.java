package com.jaguarlabs.pabs.rest.request.offline;

import android.bluetooth.BluetoothAdapter;
import android.util.Log;

import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinterHelper;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.LogRegister;
import com.jaguarlabs.pabs.util.SharedConstants;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.SpiceRequest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

/**
 * Print Account Status Ticket request class that runs in a different thread than UI thread.
 * Gets done in background.
 * it's used to print a account status ticket using an android service
 *
 * @author Jordan Alvarez
 * @version 15/12/15
 */
public class PrintAccountStatusTicket extends SpiceRequest<Boolean> {

    //private TSCActivity tscDll;
    private BluetoothPrinter tscDll;

    private Timer timerToCancelPrinting;

    private String name;
    private String collectorName;
    private String clientName;
    private String clientLastName;
    private String clientContractNumber;
    private String clientSerie;
    private String clientBalance;

    SimpleDateFormat dateFormatter;
    Date date;
    private Long dateAux;

    public PrintAccountStatusTicket(String collectorName, String clientName, String clientLastName, String clientContractNumber, String clientSerie, String clientBalance) {
        super(Boolean.class);

        Util.Log.ih("INSTANTIATING");

        //tscDll = new TSCActivity();
        //tscDll = ExtendedActivity.getOpenPort();

        dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        date = new Date();

        this.collectorName = collectorName;
        this.clientName = clientName;
        this.clientLastName = clientLastName;
        this.clientContractNumber = clientContractNumber;
        this.clientSerie = clientSerie;
        this.clientBalance = clientBalance;

        if (clientLastName.startsWith(" "))
            this.name = clientName + clientLastName;
        else
            this.name = clientName + " " + clientLastName;

        Util.Log.ih("FINISH INSTANTIATING");
    }

    @Override
    public Boolean loadDataFromNetwork() throws Exception {
        Util.Log.ih("in loadDataFromNetwork()");
        publishProgress(ConstantsPabs.START_PRINTING);

        return printAccountStatusTicket();
    }

    private boolean printAccountStatusTicket()
    {

        Util.Log.ih("in printAccountStatusTicket()");

        int counter = 0;
        while (counter < 5) {

            try {

                tscDll = BluetoothPrinter.getInstance();

                if (!BluetoothPrinter.printerConnected)
                    tscDll.connect();
                    //tscDll = new TSCPrinter();
                    //tscDll.openport(DatabaseAssistant.getPrinterMacAddress());

                    //dateAux = new Date().getTime();

                    //timer that closes printer port if got stuck
                    //trying to print
                    //has one minute of limit before closing port
                    //closing port causes NullPointerException on openPort()
                    //so it tries again to print
                /*
                timerToCancelPrinting = new Timer();
                timerToCancelPrinting.schedule(new TimerTask() {

                    int attempt;
                    TSCActivity tscDll;

                    @Override
                    public void run() {
                        if (checkTime()) {
                            Util.Log.ih("Closing port");
                            try {
                                tscDll.closeport();
                            } catch (Exception e) {
                                e.printStackTrace();
                                timerToCancelPrinting.cancel();
                            }
                        }
                        Util.Log.ih("Hora, attempt: " + attempt);
                    }

                    public TimerTask setData(int attempt, TSCActivity tscDll) {
                        this.attempt = attempt;
                        this.tscDll = tscDll;
                        return this;
                    }
                }.setData(counter, tscDll), (1000 * 60));
                */

                    //tscDll.openport(DatabaseAssistant.getPrinterMacAddress());
                    Util.Log.ih("despues de openPort");

                    //Util.Log.ih("printKILL...");
                    //TscDll.flushOutStream();
                    //tscDll.printKILL();
                    //Util.Log.ih("printKILL DONE");
                    //timerToCancelPrinting.cancel();

                    tscDll.setup(SharedConstants.Printer.width,
                            50,
                            SharedConstants.Printer.speed,
                            SharedConstants.Printer.density,
                            SharedConstants.Printer.sensorType,
                            SharedConstants.Printer.gapBlackMarkVerticalDistance,
                            SharedConstants.Printer.gapBlackMarkShiftDistance);

                    tscDll.clearbuffer();

                    tscDll.sendcommand("GAP 0,0\n");
                    tscDll.sendcommand("CLS\n");
                    tscDll.sendcommand("CODEPAGE UTF-8\n");
                    tscDll.sendcommand("SET TEAR ON\n");
                    tscDll.sendcommand("SET HEAD ON\n");
                    tscDll.sendcommand("SET REPRINT OFF\n");
                    tscDll.sendcommand("SET COUNTER @1 1\n");

                    for (int conter = 0; conter < 600; conter = conter + 30) {
                        tscDll.printerfont(conter, 10, "3", 0, 1, 1, "-");
                    }
                    tscDll.printerfont(10, 55, "3", 0, 1, 1, clientSerie + clientContractNumber);

                    tscDll.printerfont(10, 100, "3", 0, 1, 1, name);

                    //Log.i("PRINT STATUS", tscDll.status());

                    tscDll.printerfont(10, 145, "3", 0, 1, 1, "El saldo de su contrato al día ");
                    tscDll.printerfont(10, 190, "3", 0, 1, 1, dateFormatter.format(date) + " a las 6 am, es de $" + clientBalance);
                    tscDll.printerfont(180, 240, "3", 0, 1, 1, "Atentamente");
                    tscDll.printerfont(25, 285, "2", 0, 1, 1, getCollectorName(collectorName));
                    tscDll.printerfont(25, 330, "2", 0, 1, 1, getCollectorTel(collectorName));

                    //Log.i("PRINT STATUS", tscDll.status());
                    tscDll.printlabel(1, 1);

                    //Log.i("PRINT STATUS", tscDll.status());
                    //tscDll.closeport();

                    publishProgress(ConstantsPabs.FINISHED_PRINTING);
                    return true;
            } catch(Exception e) {

                Util.Log.ih("intento = " + counter);
                e.printStackTrace();
                //LogRegister.registrarSemaforo("Registrado: " + e.toString());
                //enableBluetooth();

                try {
                    tscDll.disconnect();
                    //tscDll.closeport();
                } catch (Exception e1){
                    e1.printStackTrace();
                }

                counter++;
                e.printStackTrace();
               // LogRegister.registrarSemaforo("Registrado: " + e.toString());
            }
        }
        publishProgress(ConstantsPabs.FINISHED_PRINTING);
        return false;
    }

    /**
     * checks if more than 59 minutes have passed
     * @return
     *          true -> difference is more than limit
     *          false -> difference is under limit
     */
    public boolean checkTime() {
        long newTime = new Date().getTime();
        long differences = Math.abs(dateAux - newTime);
        return (differences > (1000 * 59));
    }

    /**
     * enables bluetooth
     */
    public void enableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Util.Log.ih("enabling bluetooth");
            mBluetoothAdapter.enable();
        }
    }

    /**
     * retrieves collector name
     * @param collectorID
     * @return
     */
    public String getCollectorName(String collectorID){
        switch (collectorID){
            case "COBRANZA OFICINA":
                return "JUAN MANUEL DUEÑAS";
            case "PROBENSO 1":
                return "FERNANDO ESPINOZA";
            case "PROBENSO 2":
                return "RAUL GOMEZ PIMENTEL";
            case "PROBENSO 4":
                return "HECTOR CERVANTES RODRIGUEZ";
            case "PROBENSO 5":
                return "CRISTIAN ACEVES";
            case "PROBENSO 6":
                return "FRANCISCO MARQUEZ MARQUEZ";
            case "PROBENSO 7":
                return "OMAR BRISEÑO PIÑON";
            case "PROBENSO 8":
                return "ARGENIS RAMIREZ CARDENAS";
            case "PROBENSO 9":
                return "JAVIER ESPINOZA MOYA";
        }
        return collectorName;
    }

    /**
     * retrieves collector tel
     * @param collectorID
     * @return
     */
    public String getCollectorTel(String collectorID){
        switch (collectorID){
            case "COBRANZA OFICINA":
                return "TEL: 3636-1935 Y 3165-7579";
            case "PROBENSO 1":
                return "TEL: 33-14-41-79-37";
            case "PROBENSO 2":
                return "33-16-90-71-71";
            case "PROBENSO 4":
                return "33-10-20-12-84";
            case "PROBENSO 5":
                return "33-14-53-25-62";
            case "PROBENSO 6":
                return "33-14-50-78-32";
            case "PROBENSO 7":
                return "33-11-86-50-82";
            case "PROBENSO 8":
                return "33-31-42-63-52";
            case "PROBENSO 9":
                return "33-13-12-84-90";
        }
        return "";
    }
}
