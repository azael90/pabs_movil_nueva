package com.jaguarlabs.pabs.rest.request;

import com.jaguarlabs.pabs.rest.models.ModelOsticketReponse;
import com.jaguarlabs.pabs.rest.models.ModelOsticketRequest;
import com.jaguarlabs.pabs.rest.webService.OsticketService;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by Jordan Alvarez on 4/21/2017.
 */

public class OsticketRequest extends RetrofitSpiceRequest<ModelOsticketReponse, OsticketService> {

    ModelOsticketRequest body;

    public OsticketRequest( ModelOsticketRequest modelOsticketRequest ) {
        super(ModelOsticketReponse.class, OsticketService.class);

        this.body = modelOsticketRequest;
    }

    @Override
    public ModelOsticketReponse loadDataFromNetwork() throws Exception {
        Util.Log.ih("on loadDataFromNetwork");
        return getService().requestOsticket(body);
    }
}
