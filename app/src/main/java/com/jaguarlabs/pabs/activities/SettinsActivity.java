package com.jaguarlabs.pabs.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.jaguarlabs.pabs.R;

/**
 * Created by jordan on 10/09/16.
 */
public class SettinsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
