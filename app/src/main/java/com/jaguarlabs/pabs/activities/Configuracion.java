package com.jaguarlabs.pabs.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Location;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.Product;
import com.jaguarlabs.pabs.adapter.ProductAdapter;
import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.components.PreferenceSettings;
import com.jaguarlabs.pabs.components.Preferences;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.ExportImportDB;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.LogRegister;
import com.jaguarlabs.pabs.util.SharedConstants;
import com.jaguarlabs.pabs.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Configuracion extends AppCompatActivity
{

    public String opcionSeleccionada="";
    RecyclerView recyclerView;
    ProductAdapter adapter;
    List<Product> productList;
    private GridLayoutManager gridLayoutManager;
    ProgressDialog pDialog;
    private ImageView btBack;
    private AlertDialog.Builder dialogo;
    private Switch btSemaforo, btInternet;
    private Spinner spBranch;
    private Button btOkBranch;
    private String arregloBranches[] = {"GUADALAJARA", "TOLUCA", "PUEBLA", "NAYARIT", "MERIDA", "SALTILLO", "CANCUN", "QUERETARO", "CUERNAVACA", "MEXICALI"};
    public Clientes clientes;
    ExtendedActivity extended;

    private EditText etCambiarPeriodo, etCambiarIP, etNuevoPeriodo, etCerrarPeriodo, etComandoImpresora;
    private Button btOkPeriodo, btOKIP, btExportar, btImportar, btBorrar, btOkNuevoPeriodo, btOKCerrarPeriodo, btTxt, btEnviarPagos, btEliminarPagos, btComandoImpresora;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);
        btBack=(ImageView)findViewById(R.id.btBack);
        btSemaforo=(Switch)findViewById(R.id.btSemaforo);
        btInternet=(Switch)findViewById(R.id.btInternet);
        spBranch=(Spinner)findViewById(R.id.spBranch);
        btOkBranch=(Button)findViewById(R.id.btOKBranch);
        etCambiarPeriodo=(EditText)findViewById(R.id.etCambiarPeriodo);
        btOkPeriodo=(Button)findViewById(R.id.btOKPeriodo);
        etCambiarPeriodo=(EditText)findViewById(R.id.etCambiarPeriodo);
        btOKIP=(Button)findViewById(R.id.btOKIP);
        btExportar=(Button)findViewById(R.id.btExportar);
        btImportar=(Button)findViewById(R.id.btImportar);
        btBorrar=(Button)findViewById(R.id.btBorrar);
        btOkNuevoPeriodo=(Button)findViewById(R.id.btOkNuevoPeriodo);
        etNuevoPeriodo=(EditText)findViewById(R.id.etNuevoPeriodo);
        btOKCerrarPeriodo=(Button)findViewById(R.id.btOKCerrarPeriodo);
        etCerrarPeriodo=(EditText)findViewById(R.id.etCerrarPeriodo);
        btTxt=(Button)findViewById(R.id.btPagoTXT);
        btEnviarPagos=(Button)findViewById(R.id.btEnviarPagos);
        btEliminarPagos=(Button)findViewById(R.id.btEliminarPagos);
        etComandoImpresora=(EditText)findViewById(R.id.etComandoImpresora);
        btComandoImpresora=(Button)findViewById(R.id.btOKComando);
        clientes=new Clientes();
        extended = new ExtendedActivity();


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arregloBranches);
        spBranch.setAdapter(adapter);//agregamos las branches que tenemos para poder visualizarlas.
        spBranch.setDropDownVerticalOffset(40);

        PreferenceSettings objeto = new PreferenceSettings(ApplicationResourcesProvider.getContext());
        if(objeto.isColorContractsActive())
            btSemaforo.setChecked(true);
        else
            btSemaforo.setChecked(false);


        if(extended.setInternetAsReachable) {
            btInternet.setChecked(true);
        }
        else
            btInternet.setChecked(false);

            //Hacer el escuchador para el Spinner
        /*spBranch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                opcionSeleccionada= spBranch.getItemAtPosition(position).toString();
                //opcionSeleccionada = spBranch.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //Nada
            }
        });*/

        /*btOkBranch.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Guardar en Preferencias el Branch
                switch (opcionSeleccionada)
                {
                    case "GUADALAJARA":
                        BuildConfig.targetBranch = BuildConfig.Branches.GUADALAJARA;
                        //Preferences.savePreferenceBranch(Configuracion.this, BuildConfig.Branches.GUADALAJARA , Preferences.KEY_BRANCH);
                        break;
                    case "TOLUCA":
                        BuildConfig.targetBranch = BuildConfig.Branches.TOLUCA;
                        break;
                    case "PUEBLA":
                        BuildConfig.targetBranch = BuildConfig.Branches.PUEBLA;
                        break;
                    case "NAYARIT":
                        BuildConfig.targetBranch = BuildConfig.Branches.NAYARIT;
                        break;
                    case "MERIDA":
                        BuildConfig.targetBranch = BuildConfig.Branches.MERIDA;
                        break;
                    case "SALTILLO":
                        BuildConfig.targetBranch = BuildConfig.Branches.SALTILLO;
                        break;
                    case "CANCUN":
                        BuildConfig.targetBranch = BuildConfig.Branches.CANCUN;
                        break;
                    case "QUERETARO":
                        BuildConfig.targetBranch = BuildConfig.Branches.QUERETARO;
                        break;
                    case "CUERNAVACA":
                        BuildConfig.targetBranch = BuildConfig.Branches.CUERNAVACA;
                        break;
                    case "MEXICALI":
                        BuildConfig.targetBranch = BuildConfig.Branches.MEXICALI;
                        break;
                }
                Toast.makeText(getApplicationContext(), "BRANCH MODIFICADO", Toast.LENGTH_SHORT).show();
            }
        });*/
        //----------------------------------------------------------------------------------------------

        btOkPeriodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(etCambiarPeriodo.getText().toString().equals("") || etCambiarPeriodo.getText().toString().isEmpty())
                {
                    Toast.makeText(getApplicationContext(), "Porfavor Ingresa el periodo a cambiar", Toast.LENGTH_LONG).show();
                }
                else
                {
                    ExtendedActivity objeto = new ExtendedActivity();
                    Clientes clientes = new Clientes();
                    try {
                        //String period = etCambiarPeriodo.toString().substring(etCambiarPeriodo.toString().indexOf("%") + 1, etCambiarPeriodo.toString().lastIndexOf("%")
                        String period = "changePeriod%"+ etCambiarPeriodo.getText().toString();
                        //);
                        Util.Log.ih("periodo obtenido = " + period);
                        String previoudPeriod = Integer.toString(objeto.getEfectivoPreference().getInt("periodo", 0));
                        LogRegister.registrarCheat(etCambiarPeriodo.toString() + " Periodo anterior: " + previoudPeriod);
                        objeto.getEfectivoEditor().putInt("periodo", Integer.parseInt(period)).apply();
                        DatabaseAssistant.eraseDatabaseAfterChangingPeriod();
                        Toast.makeText(getApplicationContext(), "El periodo ha cambiado a: " + period + "; periodo anterior: " + previoudPeriod, Toast.LENGTH_LONG).show();
                        clientes.printCierrePerido = !clientes.printCierrePerido;
                        Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                        DatabaseAssistant.insertOsticket("Cheat", etCambiarPeriodo.toString(), myLocation);
                        //clientes.executeSendOsticket();
                    } catch (StringIndexOutOfBoundsException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        //--------------------------------------------------------------------------------------------------

        btOKIP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Clientes clientes = new Clientes();
                try
                {
                    String IP = etCambiarIP.getText().toString().substring(
                            etCambiarIP.getText().toString().indexOf("%") + 1,
                            etCambiarIP.getText().toString().lastIndexOf("%")
                    );
                    LogRegister.registrarCheat("changeServerIP%");
                    Util.Log.ih("IP obtenida = " + IP);

                    Preferences preferencesServerIP = new Preferences(ApplicationResourcesProvider.getContext());
                    String oldServerIP = preferencesServerIP.getServerIP();

                    preferencesServerIP.changeServerIP(IP);
                    Toast.makeText(getApplicationContext(), "La IP a cambiado a: " + IP + "\nIP anterior: " + oldServerIP, Toast.LENGTH_SHORT).show();
                    //mainActivity.toastL("La IP a cambiado a: " + IP + "\nIP anterior: " + oldServerIP);
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", etCambiarIP.getText().toString(), myLocation);
                //clientes.executeSendOsticket();
            }
        });


        //----------------------------------------------------------------------------------------------------


       /* productList= new ArrayList<>();
        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        gridLayoutManager= new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);*/

        if(checkInternetConnection())
        {
            //cargar();
        }else
        {
            /*dialogo=new AlertDialog.Builder(this);
            dialogo.setTitle("¡No tienes conexión a internet!");
            dialogo.setCancelable(false);
            dialogo.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            dialogo.show();*/
        }

        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btSemaforo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if (isChecked)
                {
                    LogRegister.registrarCheat("turnColors%ON");
                    PreferenceSettings preferenceSettings = new PreferenceSettings(ApplicationResourcesProvider.getContext());
                    preferenceSettings.turnColorContracts(true);
                    Toast.makeText(Configuracion.this, "Semáforo activado", Toast.LENGTH_LONG).show();
                }
                else
                {
                    LogRegister.registrarCheat("turnColors%OFF");
                    PreferenceSettings preferenceSettings = new PreferenceSettings(ApplicationResourcesProvider.getContext());
                    preferenceSettings.turnColorContracts(false);
                    Toast.makeText(Configuracion.this, "Semáforo desactivado", Toast.LENGTH_LONG).show();
                }
            }
        });

        btInternet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if (isChecked)
                {
                    ExtendedActivity activity = new ExtendedActivity();
                    LogRegister.registrarCheat("admin%internet");
                    activity.setInternetAsReachable = !activity.setInternetAsReachable;
                    Toast.makeText(getApplicationContext(), "Internet " + (activity.setInternetAsReachable ? "habilitado" : "deshabilitado")  + " manualmente", Toast.LENGTH_SHORT).show();
                    //activity.toastL("Internet " + (activity.setInternetAsReachable ? "habilitado" : "deshabilitado")  + " manualmente");
                }
                else
                {
                    ExtendedActivity activity = new ExtendedActivity();
                    LogRegister.registrarCheat("admin%internet");
                    activity.setInternetAsReachable = !activity.setInternetAsReachable;
                    Toast.makeText(getApplicationContext(), "Internet " + (activity.setInternetAsReachable ? "habilitado" : "deshabilitado")  + " manualmente", Toast.LENGTH_SHORT).show();
                    //activity.toastL("Internet " + (activity.setInternetAsReachable ? "habilitado" : "deshabilitado")  + " manualmente");
                }
            }
        });

        btImportar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Importar
                LogRegister.registrarCheat("importDB%");
                ExportImportDB exportImportDB = new ExportImportDB();
                try {
                    if (exportImportDB.importDB()) {
                        Toast.makeText(getApplicationContext(), "Base de datos importada correctamente", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Ocurrio un error al importar la base de datos", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "importDB%", myLocation);
                //clientes.executeSendOsticket();
            }
        });

        btExportar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                //Exportar
                ExportImportDB exportImportDB = new ExportImportDB();
                LogRegister.registrarCheat("exportDB%");
                try {
                    if (exportImportDB.exportDB()) {
                        Toast.makeText(getApplicationContext(), "Base de datos exportada correctamente", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Ocurrio un error al exportar la base de datos", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "exportDB%", myLocation);
                //clientes.executeSendOsticket();
                clientes.bandera=true;
            }
        });

        btBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                //Borrar
                LogRegister.registrarCheat("resetDB%");
                try {
                    DatabaseAssistant.resetDatabase( ApplicationResourcesProvider.getContext() );
                    getSharedPreferences("folios", 0).edit().clear().commit();
                    //ApplicationResourcesProvider.getContext().getSharedPreferences("folios", 0).edit().clear().commit();
                    //toastL("reseteando base de datos");
                    Toast.makeText(Configuracion.this, "Reseteando Base de datos", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btOkNuevoPeriodo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Añadir nuevo Periodo
                try {
                    //String period="";
                    String period = etNuevoPeriodo.getText().toString().substring(etNuevoPeriodo.getText().toString().indexOf("%") + 1, etNuevoPeriodo.getText().toString().lastIndexOf("%"));
                    LogRegister.registrarCheat("addNewPeriod%");
                    Util.Log.ih("periodo obtenido = " + period);
                    DatabaseAssistant.insertNewPeriodIfNeeded(Integer.parseInt(period));
                    Toast.makeText(getApplicationContext(), DatabaseAssistant.checkLastThreePeriodsStatus()+ period, Toast.LENGTH_SHORT).show();
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "addNewPeriod%", myLocation);
                //clientes.executeSendOsticket();
            }
        });


        btOKCerrarPeriodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                try {
                    //String period="";
                    String period = etCerrarPeriodo.getText().toString().substring(etCerrarPeriodo.getText().toString().indexOf("%") + 1, etCerrarPeriodo.getText().toString().lastIndexOf("%"));
                    LogRegister.registrarCheat("closePeriod%");
                    Util.Log.ih("periodo obtenido = " + period);
                    DatabaseAssistant.closePeriod(period);
                    Toast.makeText(getApplicationContext(), DatabaseAssistant.checkLastThreePeriodsStatus(), Toast.LENGTH_SHORT).show();
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "closePeriod%", myLocation);
                //clientes.executeSendOsticket();
            }
        });

        btTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogRegister.registrarCheat("sendPaymentstxt%");
                clientes.registerPagostxt();
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "sendPaymentstxt%", myLocation);
                //clientes.executeSendOsticket();
            }
        });

        btEnviarPagos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                clientes.metodo("resendAllPayments%");
               /* ExtendedActivity activity = new ExtendedActivity();
                try {
                    LogRegister.registrarCheat("resendAllPayments%");
                    String response = DatabaseAssistant.updateAllPaymentAsNotSyncedToSendAgain();
                    //mainActivity.toastL(response ? "Se modificó los recibos. Ingresa al cierre informativo." : "No se encontró recibos");

                    runOnUiThread(() ->
                    {
                        try
                        {
                            activity.showAlertDialog("Folios", response, true);
                        } catch (Exception ex) {
                            Toast toast = Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    });

                   clientes.printCierrePerido = !clientes.printCierrePerido;
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "resendAllPayments%", myLocation);
                //clientes.executeSendOsticket();*/
            }
        });

        btEliminarPagos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                extended.toastL("Eliminando todos los cobros");
                /*LogRegister.registrarCheat("deleteAllPayments%");
                DatabaseAssistant.deleteAllPayments();
                //mainActivity.toastL("Eliminando todos los cobros");
                Toast.makeText(getApplicationContext(), "Eliminando todos los cobros", Toast.LENGTH_SHORT).show();
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "deleteAllPayments%", myLocation);
                //clientes.executeSendOsticket();*/
            }
        });

        btComandoImpresora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                try {
                    String command="";
                    //String command = arg0.toString().substring(arg0.toString().indexOf("%") + 1, arg0.toString().lastIndexOf("%"));
                    String tspl = "";
                    String prefix = "";
                    int counter = 0;

                    switch (command.toUpperCase()) {
                        case "SERIAL":
                            tspl = "_SERIAL$";
                            prefix = "Serial No: ";
                            break;
                        case "MODEL":
                            tspl = "_MODEL$";
                            prefix = "Model: ";
                            break;
                        case "VERSION":
                            tspl = "_VERSION$";
                            prefix = "Version: ";
                            break;
                        case "ADDRESS":
                            tspl = "GETSETTING$(\"CONFIG\", \"BT\", \"MAC ADDRESS\")";
                            prefix = "BT MAC Address: ";
                            break;
                        case "CHECKSUM":
                            tspl = "GETSETTING$(\"SYSTEM\", \"INFORMATION\", \"CHECKSUM\")";
                            prefix = "Checksum: ";
                            break;
                        case "DPI":
                            tspl = "GETSETTING$(\"SYSTEM\", \"INFORMATION\", \"DPI\")";
                            prefix = "DPI: ";
                            break;
                        case "RESET":
                            tspl = (char) 27 + "!R";
                            break;
                        case "FEED":
                            tspl = (char) 27 + "!F";
                            break;
                        case "PAUSE":
                            tspl = (char) 27 + "!P";
                            break;
                        case "CONTINUE":
                            tspl = (char) 27 + "!O";
                            break;
                    }

                    while (counter < 5) {
                        Log.d("SerialCounter", Integer.toString(counter));

                        try {

                            BluetoothPrinter.getInstance().connect();

                            BluetoothPrinter.getInstance().setup(
                                    SharedConstants.Printer.width,
                                    40,
                                    SharedConstants.Printer.speed,
                                    SharedConstants.Printer.density,
                                    SharedConstants.Printer.sensorType,
                                    SharedConstants.Printer.gapBlackMarkVerticalDistance,
                                    SharedConstants.Printer.gapBlackMarkShiftDistance
                            );

                            String serial = BluetoothPrinter.getInstance().sendcommand_getString("OUT \"\";" + tspl).split("\r\n")[0];

                            if (!serial.equals(" ")) {
                                BluetoothPrinter.getInstance().clearbuffer();
                                BluetoothPrinter.getInstance().printerfont(100, 150, "3", 0, 0, 0, prefix + serial);
                                BluetoothPrinter.getInstance().printlabel(1, 1);
                                Toast.makeText(getApplicationContext(), prefix + serial, Toast.LENGTH_SHORT).show();
                            }
                            break;
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            counter++;
                        }
                    }

                    if (counter == 5) {
                        Log.d("SerialCounter", "No se pudo conectar con la impresora");
                        //mainActivity.toastS("No se pudo conectar con la impresora");
                        Toast.makeText(getApplicationContext(), "No se pudo conectar con la impresora", Toast.LENGTH_SHORT).show();
                    }
                } catch (StringIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "printerCommand%", myLocation);
                //clientes.executeSendOsticket();
            }
        });

    }

    /*private void cargar()
    {
        pDialog=new ProgressDialog(Configuracion.this);
        pDialog.setMessage("Cargando...");
        pDialog.setCanceledOnTouchOutside(true);
        pDialog.show();

        ConstantsPabs objeto = new ConstantsPabs();
        StringRequest stringRequest = new StringRequest(Request.Method.GET,   objeto.consultaConfiguracion, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                pDialog.hide();
                try
                {
                    JSONArray products = new JSONArray(response);
                    for(int i=0; i<products.length(); i++)
                    {
                        JSONObject productObject= products.getJSONObject(i);
                        String ID= productObject.getString("id");
                        String Mensaje=productObject.getString("ticket_mensaje");

                        if(ID.equals("1") && Mensaje.equals("En PABS estamos para servirle..."))
                        {

                        }
                        else
                        {
                            Product product = new Product(ID, Mensaje);
                            productList.add(product);
                        }

                        pDialog.hide();
                    }
                    adapter= new ProductAdapter(Configuracion.this, productList);
                    recyclerView.setAdapter(adapter);

                } catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Snackbar.make(recyclerView, error.getMessage(), Snackbar.LENGTH_SHORT).show();
            }
        });
        Volley.newRequestQueue(this).add(stringRequest);
    }*/
    private boolean checkInternetConnection()
    {
        ConnectivityManager con = (ConnectivityManager) Configuracion.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo= con.getActiveNetworkInfo();
        if(networkInfo!=null && networkInfo.isConnected())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void validarBranch()
    {

    }
}