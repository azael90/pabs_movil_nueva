package com.jaguarlabs.pabs.activities;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.NotificacionesAdapter;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.Notifications;
import com.jaguarlabs.pabs.net.NetHelper;
import com.jaguarlabs.pabs.net.NetHelper.onWorkCompleteListener;
import com.jaguarlabs.pabs.net.NetService;
import com.jaguarlabs.pabs.net.VolleySingleton;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.PaginacionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to show all notification either general
 * or personal on the screen.
 * From here you can select a personal notification, so that notifications
 * counter decreases.
 * All notification are colored blue, but read personal notifications
 * which are colored gray.
 */
@SuppressWarnings("SpellCheckingInspection")
public class Notificaciones extends ExtendedActivity implements
		OnItemClickListener, PaginacionCallback {

	//region Variables

	//region Primitives

	private int pagina;

	private boolean descargarMas;

	private int lastPosition;

	//endregion

	//region Objects

	//region Statics

	private static Notificaciones mthis = null;

	//endregion

	private String idCobrador;

	private List<Notifications> notificationsList;

	private ListView listView;

	private RelativeLayout notificationLayout;

	//endregion

	//endregion

	//region Lifecycle

	@Override
	public void onResume() {
		super.onResume();
		mthis = this;
	}

	//endregion

	//region Methods

	//region Public

	/**
	 * manages clicks from screen of resources as
	 *
	 * - imageViewBack
	 * 		finishes activity
	 *
	 * @param v
	 * 		view of resources
	 */
	public void onClickFromXML(View v) {
		switch (v.getId()) {
			case R.id.imageViewBack:
				mthis = null;
				finish();
				break;
			default:
				break;
		}
	}

	//endregion

	//region Private

	/**
	 * requests all notifications of server
	 *
	 * web Servive
	 * 'http://50.62.56.182/ecobro/controlnotificaciones/getListNotification'
	 *
	 * JSONObject param
	 * {
	 *     "no_cobrador": "",
	 *     "pag":
	 * }
	 */
	private void getNotificaciones(){
		JSONObject json = new JSONObject();
		try {
			json.put("no_cobrador", idCobrador);
			json.put("pag", "" + pagina);

		} catch (Exception e) {
			e.printStackTrace();
		}

		/*RequestResult requestResult = new RequestResult(this);

		requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
			@Override
			public void onRequestResult(NetworkResponse response) {
				try
				{
					manage_GetNotifications(new JSONObject(VolleyTickle.parseResponse(response)));
				}
				catch (JSONException ex)
				{
					ex.printStackTrace();
				}
			}

			@Override
			public void onRequestError(NetworkResponse error) {

			}
		});

		requestResult.executeRequest(ConstantsPabs.urlNotificactiones, Request.Method.POST, json);*/

		/*NetService service = new NetService(ConstantsPabs.urlNotificactiones, json);
		NetHelper.getInstance().ServiceExecuter(service, this, new onWorkCompleteListener() {

			@Override
			public void onError(Exception e) {

			}

			@Override
			public void onCompletion(String result) {
				try {
					manage_GetNotifications(new JSONObject(result));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});*/

		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlNotificactiones, json, this::manage_GetNotifications, Throwable::printStackTrace);
		request.setShouldCache(false);
		request.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(request);

	}

	/**
	 * fills ListView with notifications
	 */
	private void llenarLista() {
		notificationsList = DatabaseAssistant.getNotifications();

		if ( notificationsList == null ){
			notificationsList = new ArrayList<>();
		}

		NotificacionesAdapter adapter = new NotificacionesAdapter(ApplicationResourcesProvider.getContext(),
				notificationsList, this, descargarMas);

		listView = (ListView) findViewById(R.id.lv_notification);
		listView.setDivider(getResources().getDrawable(
				R.drawable.divider_list_notificaciones));
		listView.setAdapter(adapter);
		listView.setSelection(lastPosition);
		listView.setOnItemClickListener(this);
	}

	/**
	 * manages response of server
	 *
	 * @param json
	 * 			server response
	 */
	private void manage_GetNotifications(JSONObject json) {
		if (json.has("result")) {
			if (pagina == 1) {
				DatabaseAssistant.deleteNotifications();
			}
			pagina++;
			saveListNotifications(json);
			llenarLista();
			if (json.has("notificaciones")) {
				try {
					int notificaciones = json.getInt("notificaciones");
					if (Clientes.getMthis() != null) {
						Clientes.getMthis().setNumeroNotificaciones(notificaciones);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		} else {
			mostrarMensajeError(ConstantsPabs.getNotificaciones);
		}
	}

	/**
	 * shows error message
	 *
	 * @param caso
	 * 			constant when failed
	 */
	private void mostrarMensajeError(final int caso) {
		try {
			AlertDialog.Builder alert = getAlertDialogBuilder();
			alert.setMessage(getResources().getString(R.string.error_message_network));
			alert.setPositiveButton("Salir", (dialog, which) -> {
				mthis = null;
				finish();
			});
			alert.create();
			alert.setCancelable(false);
			alert.show();
		} catch (Exception e){
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), "Por favor vuelve a intentarlo", Toast.LENGTH_LONG).show();
			mthis = null;
			finish();
		}
	}

	/**
	 * saves notificcations into database
	 *
	 * @param json
	 * 			JSONObject response from server
	 */
	private void saveListNotifications(JSONObject json) {
		try {
			JSONArray arrayNotifications = json.getJSONArray("result");

			if (arrayNotifications.length() > 0) {

				for (int i = 0; i < arrayNotifications.length(); i++) {

					JSONObject jsonTmp = arrayNotifications.getJSONObject(i);
					String fechaCreacion = jsonTmp.getString("fecha_creacion");
					String id = jsonTmp.getString("id");
					String notification = jsonTmp.getString("notificacion");
					String noCobrador = jsonTmp.getString("no_cobrador");
					int estatus = jsonTmp.getInt("estatus");

					DatabaseAssistant.insertNotification(fechaCreacion, id, notification,
							noCobrador, estatus);
				}
			} else {
				/*Toast.makeText(ApplicationResourcesProvider.getContext(), "Son todas las notificaciones",
						Toast.LENGTH_SHORT).show();*/
				Snackbar.make(notificationLayout, "Son todas las notificaciones", Snackbar.LENGTH_SHORT).show();
				descargarMas = false;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	//endregion

	//region Protected

	@Override
	protected void init() {
		super.init();
		setContentView(R.layout.activity_notificaciones);

		notificationLayout = findViewById(R.id.notificationLayout);
		mthis = this;
		setEstoyLogin(false);
		descargarMas = true;
		lastPosition = 0;
		Bundle bundle = getIntent().getExtras();
		pagina = 1;
		if (bundle != null) {
			if (bundle.containsKey("id_cobrador")) {
				idCobrador = bundle.getString("id_cobrador");
			}
		}

		if (getStatusConnection()) {
			//Toast.makeText(getApplicationContext(), "Descargando notificaciones...", Toast.LENGTH_SHORT).show();
			Snackbar.make(notificationLayout, "Descargando notificaciones...", Snackbar.LENGTH_SHORT).show();
			getNotificaciones();
		} else {
			Toast.makeText(ApplicationResourcesProvider.getContext(),
					getString(R.string.info_message_network_offline),
					Toast.LENGTH_LONG).show();
			llenarLista();
		}

	}

	//endregion

	//region Interfaces

	//region DescargarPaginaSiguiente

	@Override
	public void descargarPaginaSiguiente(int lastPosition) {
		if (getStatusConnection()) {
			this.lastPosition = listView.getFirstVisiblePosition() + 1;
			getNotificaciones();
		} else {
			Toast.makeText(ApplicationResourcesProvider.getContext(), getString(R.string.error_message_network),
					Toast.LENGTH_LONG).show();
		}
	}

	//endregion

	//region OnItemClick

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		int numCobrador = -1;

		numCobrador = Integer.parseInt( notificationsList.get(position).getNumberCollector() );

		if (numCobrador != -1) {

			//change color of notification
			RelativeLayout renglon = (RelativeLayout) view;
			LinearLayout vistaBackground = (LinearLayout) renglon
					.findViewById(R.id.llayout);
			TextView tvDescripcion = (TextView)view.findViewById(R.id.tv_descripcion);
			TextView tvTipoNotificacion = (TextView)view.findViewById(R.id.tv_tipo_notificacion);
			TextView tvFecha = (TextView)view.findViewById(R.id.tv_fecha);

			vistaBackground.setBackgroundColor(getResources().getColor(
					R.color.list_leidos));

			tvDescripcion.setTextColor(ContextCompat.getColor(this, R.color.black));
			tvFecha.setTextColor(ContextCompat.getColor(this, R.color.black));
			tvTipoNotificacion.setTextColor(ContextCompat.getColor(this, R.color.black));

			int idLeida;
			idLeida = Integer.parseInt( notificationsList.get(position).getIdNotification() );
			DatabaseAssistant.updateNotificationAsViewed(idLeida);

			/**
			 * Removes notification from notification drawer
			 */
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			mNotificationManager.cancel(idLeida);
			if (Clientes.getMthis() != null){
				if (Clientes.notifications > 0){
					Clientes.notifications--;
				}
			}

			DatabaseAssistant.insertViewedNotification(idLeida);

			int estatus;

			estatus = notificationsList.get(position).getStatus();
			if (estatus == 0) {
				notificationsList.get(position).setStatus(1);
				if (Clientes.getMthis() != null) {
					Clientes.getMthis().actualizarNotificaciones();
				}
			}
		}
	}

	//endregion

	//region OnKeyDown

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			mthis = null;
		}
		return super.onKeyDown(keyCode, event);
	}

	//endregion

	//region OnUserInteraction

	@Override
	public void onUserInteraction() {
		View refMain = null;
		if (Clientes.getMthis() != null && Clientes.getMthis().getView() != null) {
			//refMain = Clientes.getMthis().getView().findViewById(R.id.listView_clientes);
			refMain = Clientes.getMthis().getView().findViewById(R.id.rv_clientes);
		}

		if (refMain != null) {
			try {
				if (Clientes.getMthis() != null) {
					Clientes.getMthis().llamarTimerInactividad();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	//endregion

	//endregion

	//region Getters

	/**
	 * Gets context from this class.
	 * In some cases you cannot just call 'this', because another context is envolved
	 * or you want a reference to an object of this class from another class,
	 * then this context is helpful.
	 *
	 * NOTE
	 * Not recommended, it may cause NullPointerException though
	 *
	 * @return context from Cierre
	 */
	public static Notificaciones getMthis() {
		return mthis;
	}

	/**
	 * returns an instance of AlertDialogBuilder
	 * @return instance of AlertDialogBuilder
	 */
	private AlertDialog.Builder getAlertDialogBuilder(){
		return new AlertDialog.Builder(this);
	}

	//endregion

	//region Setters

	/**
	 * Sets 'mthis' global variable as Cierre's context
	 * @param mthis Cierre's context
	 */
	public void setMthis(Notificaciones mthis) {
		Notificaciones.mthis = mthis;
	}

	//endregion

	//endregion

}
