package com.jaguarlabs.pabs.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.tscdll.TSCActivity;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinterHelper;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.Payments;
import com.jaguarlabs.pabs.database.Periods;
import com.jaguarlabs.pabs.models.ModelPayment;
import com.jaguarlabs.pabs.models.ModelPeriod;
import com.jaguarlabs.pabs.net.VolleySingleton;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.UserInterationListener;
import com.jaguarlabs.pabs.util.Util;
import com.tingyik90.snackprogressbar.SnackProgressBarManager;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends ExtendedActivity {

    public int bandera = 0;
    private UserInterationListener userInteractionListener;
    public static final String TAG_CLIENTES = "clientes";
    public static final String TAG_CIERRE_INFORMATIVO = "cierre informativo";
    public static final String TAG_CLIENTE_DETALLES = "cliente detalles";
    public static final String TAG_APORTACION = "aportacion";
    public static final String TAG_DEPOSITO = "deposito";
    public static final String TAG_CIERRE = "cierre";
    public static final String TAG_PREVIOUS_PERIODS = "periodos previos";
    public static final String TAG_CONTRATOS_NO_VISITADOS = "contratos no visitados";
    public static String CURRENT_TAG = TAG_CLIENTES;
    private static int automaticClosedPeriod = -1;
    private static boolean isAutomaticDepositMade = false;
    public boolean goBack = true;
    public static boolean shouldCloseBluetoothPort = false;
    private BluetoothPrinter printer;

    private BroadcastReceiver bluetoothReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if (action != null) {

                switch (action) {
                    case BluetoothDevice.ACTION_ACL_CONNECTED:
                        Snackbar.make(mainLayout, "Impresora Conectada", Snackbar.LENGTH_SHORT).show();
                        Log.d("BluetoothReceiver", "CONNECTED: " + device.getAddress());
                        BluetoothPrinter.printerConnected = true;
                        if (MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES) && ClienteDetalle.printerConnecting.getVisibility() == View.VISIBLE)
                            ClienteDetalle.printerConnecting.setVisibility(View.GONE);
                        break;
                    case BluetoothDevice.ACTION_ACL_DISCONNECTED:
                        BluetoothPrinter.printerConnected = false;
                        Log.d("BluetoothReceiver", "DISCONNECTED: " + device.getAddress());
                        break;
                    case BluetoothDevice.ACTION_BOND_STATE_CHANGED:
                        final int extra = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);

                        switch (extra) {
                            case BluetoothDevice.BOND_BONDED:
                                Log.d("BluetoothReceiver", "BONDEND");
                                BluetoothAdapter.getDefaultAdapter().disable();
                                new Handler().postDelayed(() -> BluetoothAdapter.getDefaultAdapter().enable(), 1500);
                                break;
                            case BluetoothDevice.BOND_NONE:
                                Log.d("BluetoothReceiver", "NONE");
                                break;
                            case BluetoothDevice.BOND_BONDING:
                                Log.d("BluetoothReceiver", "BONDING");
                                break;
                        }
                        break;
                    case BluetoothDevice.ACTION_PAIRING_REQUEST:
                        Log.d("BluetoothReceiver", "PAIRING REQUEST");
                        break;
                    case BluetoothAdapter.ACTION_STATE_CHANGED:
                        final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                        switch (state) {
                            case BluetoothAdapter.STATE_OFF:
                                Log.d("BluetoothReceiver", "OFF");
                                if (BluetoothPrinter.printerConnected) {
                                    BluetoothPrinter.getInstance().disconnect();
                                }
                                BluetoothPrinter.printerConnected = false;
                                break;
                            case BluetoothAdapter.STATE_TURNING_OFF:
                                Log.d("BluetoothReceiver", "TURNING OFF");
                                break;
                            case BluetoothAdapter.STATE_ON:
                                Log.d("BluetoothReceiver", "ON");
                                break;
                            case BluetoothAdapter.STATE_TURNING_ON:
                                Log.d("BluetoothReceiver", "TURNING ON");
                                break;
                        }
                        break;
                }
            }
        }
    };

    private IntentFilter filter;
    private AlertDialog closePeriodDialog;
    private SharedPreferences login;
    private String printerSerial;
    public CoordinatorLayout mainLayout;
    public SnackProgressBarManager progressBarManager;
    private boolean showMakeDepositDialog = false;
    private android.support.v7.app.AlertDialog depositDialog;
    private Timer timerFechaCierre;
    private String fechaCierre;
    public TextView tvInternet;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainLayout = findViewById(R.id.mainLayout);
        tvInternet = (TextView) findViewById(R.id.tvInternet);
        checkInternetConnection();
        progressBarManager = new SnackProgressBarManager(mainLayout);

        if (savedInstanceState == null)
        {
            Bundle args = getIntent().getExtras();
            if (args != null && args.containsKey("action"))
            {

                if (args.getString("action", "").equals("cierre")) {
                    Cierre cierre = new Cierre();
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.root_layout, cierre, TAG_CIERRE)
                            .commitAllowingStateLoss();
                    return;
                }
            }

            Runnable runnable = () ->
            {
                Clientes clientes = Clientes.newInstance();
                clientes.setArguments(args);
                getSupportFragmentManager().beginTransaction().add(R.id.root_layout, clientes, TAG_CLIENTES).commitAllowingStateLoss();
            };

            new Handler().post(runnable);
        }

        filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        filter.addAction(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
        printer = BluetoothPrinter.getInstance();
        login = getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);

        depositDialog = new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle("¡AVISO!")
                .setMessage("No te olvides de verificar que tu teléfono haya quedado en cero")
                .setPositiveButton("Aceptar", (dialog1, which) -> dialog1.dismiss())
                .setCancelable(false)
                .create();

        /*EstimoteCloudCredentials cloudCredentials = new EstimoteCloudCredentials("oscar-vaca-gmail-com-s-you-nva","9d0d5f4b3d017b7525ddbcf8ec0da25d");

        ProximityObserver proximityObserver = new ProximityObserverBuilder(this, cloudCredentials)
                .withOnErrorAction(throwable -> null)
                .withBalancedPowerMode()
                .build();*/

        /*ProximityZone cobranzaZone = proximityObserver.zoneBuilder().forAttachmentKeyAndValue("venue", "cobranza")
                .inNearRange()
                .withOnEnterAction(proximityAttachment -> {
                    Snackbar.make(mainLayout, "Entraste en el rango del Beacon", Snackbar.LENGTH_LONG).show();
                    Log.d("ProximityBeacon", "Entraste en el rango del Beacon");
                    showMakeDepositDialog = true;
                    showWarningDialog();
                    return null;
                })
                .withOnExitAction(proximityAttachment -> {
                    Snackbar.make(mainLayout, "Saliste del rango del Beacon", Snackbar.LENGTH_LONG).show();
                    Log.d("ProximityBeacon", "Saliste del rango del Beacon");
                    showMakeDepositDialog = false;
                    return null;
                })
                .create();*/

        /*ProximityZone cobranzaZone = proximityObserver.zoneBuilder().forTag("cobranza")
                .inNearRange()
                .withOnEnterAction(proximityContext -> {
                    Snackbar.make(mainLayout, "Entraste en el rango del Beacon", Snackbar.LENGTH_LONG).show();
                    Log.d("ProximityBeacon", "Entraste en el rango del Beacon");
                    showMakeDepositDialog = true;
                    showWarningDialog();
                    return null;
                })
                .withOnExitAction(proximityContext -> {
                    Snackbar.make(mainLayout, "Saliste del rango del Beacon", Snackbar.LENGTH_LONG).show();
                    Log.d("ProximityBeacon", "Saliste del rango del Beacon");
                    showMakeDepositDialog = false;
                    return null;
                })
                .create();*/

        /*RequirementsWizardFactory.createEstimoteRequirementsWizard().fulfillRequirements(this, () -> {
            observerHandler = proximityObserver.addProximityZone(cobranzaZone).start();
            return null;
        }, requirements -> null, throwable -> null);*/

        if (BuildConfig.getTargetBranch() == BuildConfig.Branches.GUADALAJARA)
        {
            try {
                JSONObject datosCobrador = new JSONObject(getIntent().getStringExtra("datos_cobrador"));
                JSONObject data = new JSONObject();

                data.put("no_cobrador", datosCobrador.getString("no_cobrador"));

                timerFechaCierre = new Timer();

                timerFechaCierre.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlGetFechaCierreByCobrador, data, response -> {
                            if (!response.has("error")) {
                                try {
                                    String fecha_cierre = "";
                                    String fecha_cobrador = response.getString("fecha_cobrador");
                                    if (response.has("fecha_cierre")) {
                                        fecha_cierre = response.getString("fecha_cierre");
                                    }

                                    if (fecha_cierre.equals("")) {
                                        fechaCierre = fecha_cobrador;
                                    } else {
                                        if (fecha_cobrador.equals("")) {
                                            fechaCierre = fecha_cierre;
                                        } else {
                                            DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                                            LocalDateTime fechaCierreCobrador = LocalDateTime.parse(fecha_cobrador, dateTimeFormatter);
                                            LocalDateTime fechaCierreAutomatico = LocalDateTime.parse(fecha_cierre, dateTimeFormatter);

                                            if (fechaCierreCobrador.isAfter(fechaCierreAutomatico))
                                                fechaCierre = fecha_cobrador;
                                            else
                                                fechaCierre = fecha_cierre;
                                        }
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                    fechaCierre = "";
                                }
                            }
                            Log.d("timerFechaCierre", "Response from server: " + response.toString());
                        }, error -> {
                        });
                        request.setShouldCache(false);

                        request.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        VolleySingleton.getInstance(MainActivity.this).addToRequestQueue(request);

                        Log.d("timerFechaCierre", "Timer scheduled");
                    }
                }, 1000, (1000 * 30) * 30);
            } catch (JSONException ex) {
                ex.printStackTrace();
                fechaCierre = "";
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(bluetoothReceiver, filter);
        ApplicationResourcesProvider.startLocationUpdates();

        if (BuildConfig.getTargetBranch() == BuildConfig.Branches.GUADALAJARA)
            checkIfShouldClosePeriod(this, fechaCierre);
        if (showMakeDepositDialog)
        {
            showWarningDialog();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if ((BuildConfig.getTargetBranch() == BuildConfig.Branches.GUADALAJARA ||
                BuildConfig.getTargetBranch() == BuildConfig.Branches.PUEBLA) && Build.VERSION.SDK_INT < Build.VERSION_CODES.N
        ) {
            Util.Log.ih("disconnecting printer");
            printer.disconnect();
        } else {
            Util.Log.ih("BuildConfig.getTargetBranch(): " + BuildConfig.getTargetBranch());
            Util.Log.ih("Build.VERSION.SDK_INT: " + Build.VERSION.SDK_INT);
            Util.Log.ih("Build.VERSION.SDK_INT < Build.VERSION_CODES.N: " + (Build.VERSION.SDK_INT < Build.VERSION_CODES.N));
        }

        ApplicationResourcesProvider.stopLocationUpdates();

        unregisterReceiver(bluetoothReceiver);

        if (closePeriodDialog != null && closePeriodDialog.isShowing())
        {
            closePeriodDialog.dismiss();

            closePeriodDialog = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        //observerHandler.stop();

        super.onDestroy();

        //unregisterReceiver(bluetoothReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed()
    {

        int fragmentStack = getSupportFragmentManager().getBackStackEntryCount();

        if (fragmentStack > 0)
        {

            switch (CURRENT_TAG)
            {
                case TAG_CIERRE_INFORMATIVO:
                    CierreInformativo cierreInformativo = (CierreInformativo) getSupportFragmentManager().findFragmentByTag(TAG_CIERRE_INFORMATIVO);
                    if (!cierreInformativo.goBack)
                        return;
                    break;



                case TAG_CLIENTE_DETALLES:
                    ClienteDetalle clienteDetalle = (ClienteDetalle) getSupportFragmentManager().findFragmentByTag(TAG_CLIENTE_DETALLES);
                    try
                    {
                        if (!clienteDetalle.goBack)
                        {
                            final FrameLayout flLoading = (FrameLayout) Objects.requireNonNull(clienteDetalle.getView()).findViewById(R.id.flLoading);
                            if(flLoading.getVisibility()==View.VISIBLE)
                            {
                                clienteDetalle.dismissMyCustomDialog();
                            }
                            else
                                Log.d("NADA BRO", "NADA VATO");

                            return;
                        }
                        else
                        {
                            Log.d("gO back", "NADA VATO");
                        }
                        break;
                    }catch (Exception ex)
                    {
                        Log.i("goBack", "Ocurrio un error en goBack de cliente detalle a clientes");
                    }


                /**-----------------------------------------------------------------------------------------------------------------------
                 *Se añadio el case de TAG_DEPOSITO dentro de switch donde se valida el boton back y el boton iv_back
                 * una vez que el proceso de envio de solicitud se envia
                 *
                 * verificar con una bandera
                 * Azael Jimenez
                 */
                case TAG_DEPOSITO:

                    if(bandera==0)//Desactivar el back
                    {
                        //Toast.makeText(this, "bandera: "+bandera, Toast.LENGTH_SHORT).show();
                        if (getFragmentManager().getBackStackEntryCount() > 1)
                            getFragmentManager().popBackStack();
                        else
                        {
                            Deposito deposito = (Deposito) getSupportFragmentManager().findFragmentByTag(TAG_DEPOSITO);
                            if (deposito.goBack)
                            {
                                // deposito.back.setEnabled(false);
                                Toast.makeText(this, "Espera a la solicitud", Toast.LENGTH_SHORT).show();

                                return;
                            }
                        }
                        break;
                    }
                    else if (bandera==1) //Activar el back
                    {
                        //Toast.makeText(this, "Activar el Back, bandera: "+bandera, Toast.LENGTH_LONG).show();
                    }

                /**-----------------------------------------------------------------------------------------------------------------------
                 *Fin
                 */
            }

            super.onBackPressed();
            return;
        }

        if (CURRENT_TAG.equals(TAG_CIERRE))
            return;

        moveTaskToBack(true);
    }







    private void automaticClosePeriod(LocalDateTime startDate, LocalDateTime closeDate) {

        Log.d("PeriodStartDate", "Mes: " + startDate.getMonthOfYear());
        Log.d("PeriodCloseDate", "Mes: " + closeDate.getMonthOfYear());

        if (startDate.getMonthOfYear() <= closeDate.getMonthOfYear())
        {
            registerPagos();
            int periodo = getEfectivoPreference().getInt("periodo", 0);
            String noTemp = getEfectivoPreference().getString("no_cobrador", "");
            getEfectivoEditor().clear().apply();
            getEfectivoEditor().putString("no_cobrador", noTemp).apply();

            try
            {
                getEfectivoEditor().putInt("periodo", periodo).apply();
            } catch (Exception e) {
                e.printStackTrace();
            }

            int periodoAux = getEfectivoPreference().getInt("periodo", 0);
            DatabaseAssistant.insertNewPeriodIfNeeded(periodoAux + 1);
            getEfectivoEditor().putInt("periodo", (periodoAux + 1)).apply();
            Util.Log.ih("DEPOSITO periodo cambio a = " + getEfectivoPreference().getInt("periodo", 0));
            automaticClosedPeriod = periodoAux;
        }
    }

    public void checkIfShouldClosePeriod(Context context, String fechaCierre)
    {
        if (canParseDate(fechaCierre)) {
            LocalDateTime today = LocalDateTime.now();
            LocalDateTime closeDate = LocalDateTime.parse(fechaCierre, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));

            if (today.isAfter(closeDate))
            {
                List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();

                if (periods.size() > 0)
                {
                    boolean lastPeriodClosed = DatabaseAssistant.checkThatPeriodWasClosedOnLastDayOfMonth(periods.get(periods.size() - 1).getPeriodNumber());

                    if (!lastPeriodClosed)
                    {
                        Periods period = DatabaseAssistant.getOnePeriodOfAllActive(periods.size());

                        if (period != null) {
                            LocalDateTime periodStartDate = LocalDateTime.parse(period.getStartDate(), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));

                            if ((periodStartDate.isAfter(closeDate) || periodStartDate.equals(closeDate)) && periods.size() == 1)
                                return;

                            if (automaticClosedPeriod != period.getPeriodNumber() && periods.size() == 1)
                                automaticClosePeriod(periodStartDate, closeDate);

                            if (automaticClosedPeriod != -1) {

                                if (!isAutomaticDepositMade)
                                {
                                    try {
                                        JSONObject datosCobrador = new JSONObject(getIntent().getStringExtra("datos_cobrador"));
                                        JSONObject data = new JSONObject();

                                        data.put("no_cobrador", datosCobrador.getString("no_cobrador"));
                                        data.put("periodo", automaticClosedPeriod);

                                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlAutomaticClosePeriod, data, response -> {
                                            try {
                                                if (response.has("error")) {
                                                    Snackbar.make(mainLayout, response.getString("error"), Snackbar.LENGTH_SHORT).show();
                                                } else if (response.has("result") && response.has("id_deposito")) {
                                                    Log.d("IdDepositoAutomático", response.getString("id_deposito"));
                                                    isAutomaticDepositMade = true;
                                                }
                                            } catch (JSONException ex) {
                                                ex.printStackTrace();
                                            }
                                        }, error -> {
                                            Snackbar.make(mainLayout, error.getMessage(), Snackbar.LENGTH_SHORT).show();
                                        });
                                        request.setShouldCache(false);
                                        request.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                        VolleySingleton.getInstance(this).addToRequestQueue(request);
                                    }
                                    catch (JSONException ex)
                                    {
                                        ex.printStackTrace();
                                    }
                                }

                                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                                builder.setTitle("Cierre de periodo pendiente");
                                builder.setMessage("Tienes un cierre de periodo pendiente. Se hará un cierre automatico de periodo");
                                builder.setCancelable(false);
                                builder.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                                });
                                builder.setNegativeButton("Descartar", (dialog, which) -> {
                                });

                                closePeriodDialog = builder.show();

                                closePeriodDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
                                    //Cerrar periodo
                                    final LinearLayout linearLayout = new LinearLayout(this);
                                    final TextInputLayout textInputLayout = new TextInputLayout(this);
                                    final EditText password = new EditText(this);
                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    LinearLayout.LayoutParams inputParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    layoutParams.setMargins(30, 0, 30, 0);

                                    password.setSingleLine(true);
                                    password.setLayoutParams(layoutParams);
                                    password.setHint("Contraseña");
                                    password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                                    textInputLayout.setLayoutParams(inputParams);
                                    textInputLayout.setPasswordVisibilityToggleEnabled(true);

                                    textInputLayout.addView(password);

                                    linearLayout.addView(textInputLayout);

                                    AlertDialog.Builder secureDialog = new AlertDialog.Builder(this);
                                    secureDialog.setTitle("Cierre automático");
                                    secureDialog.setMessage("Ingresa la contraseña para cerrar el periodo");
                                    secureDialog.setView(linearLayout);
                                    secureDialog.setPositiveButton("Aceptar", (dialog, which) -> {
                                    });
                                    secureDialog.setNegativeButton("Cancelar", (dialog, which) -> {
                                    });

                                    AlertDialog dialog = secureDialog.show();

                                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v1 -> {

                                        if (password.getText().toString().equals("password")) {
                                            Toast.makeText(context, "Password Correcto", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            closePeriodDialog.dismiss();

                                            //depositoAutomatico(automaticClosedPeriod);

                                            DatabaseAssistant.finishPeriod(automaticClosedPeriod);
                                            Cierre cierre = new Cierre();
                                            getSupportFragmentManager().beginTransaction()
                                                    .replace(R.id.root_layout, cierre, TAG_CIERRE)
                                                    .commitAllowingStateLoss();
                                        } else
                                            Toast.makeText(context, "Password Incorrecto", Toast.LENGTH_SHORT).show();
                                    });

                                    //depositoAutomatico(automaticClosedPeriod);
                                });
                            }
                        }
                    }
                }
            }
        }
    }

    private void depositoAutomatico(int periodo)
    {
        try
        {
            JSONObject deposito = new JSONObject();
            deposito.put("periodo", periodo);
            deposito.put("empresa", "00");
            deposito.put("no_banco", "11");
            deposito.put("referencia", "automatico");
            deposito.put("timestamp", LocalDateTime.now().toDate().getTime());
            deposito.put("no_cobrador", getEfectivoPreference().getString("no_cobrador", ""));
            deposito.put("monto", getTotalEfectivo());

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlDepositoAutomatico, deposito, response -> {
                if (response.has("result"))
                {
                    Toast.makeText(this, "Deposito guardado", Toast.LENGTH_SHORT).show();
                    try {
                        aprobarDepositoAutomatico(response.getInt("result"));
                    }
                    catch (JSONException ex)
                    {
                        ex.printStackTrace();
                    }
                }
                else if (response.has("error"))
                {
                    try {
                        Toast.makeText(this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                    catch (JSONException ex)
                    {
                        ex.printStackTrace();
                    }
                }
            }, error -> {
                Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    private void aprobarDepositoAutomatico(int deposito)
    {
        try
        {
            JSONObject data = new JSONObject();

            data.put("deposito", deposito);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlAprobarDepositoAutomatico, data, response -> {
                if (response.has("result")) {
                    try {
                        Toast.makeText(this, "Deposito aceptado con el monto: $" + response.getString("result"), Toast.LENGTH_SHORT).show();
                        DatabaseAssistant.finishPeriod(automaticClosedPeriod);
                        Cierre cierre = new Cierre();
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.root_layout, cierre, TAG_CIERRE)
                                .commitAllowingStateLoss();
                    }
                    catch (JSONException ex)
                    {
                        ex.printStackTrace();
                    }
                }
                else if (response.has("error"))
                {
                    try {
                        Toast.makeText(this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                    catch (JSONException ex)
                    {
                        ex.printStackTrace();
                    }
                }
            }, error -> {
                Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
            });
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    private boolean canParseDate(String date)
    {
        try
        {
            java.time.format.DateTimeFormatter formatter = null;
            String formatDateTime = null;
            java.time.LocalDateTime now = null;
            LocalDateTime.parse(date, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));

            //Get current date time


            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                now = java.time.LocalDateTime.now();
                formatter = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                formatDateTime = now.format(formatter);
            }
            return true;
        }
        catch (NullPointerException ex)
        {
            ex.printStackTrace();
            return false;
        }
    }

    private void setPrinterSerial()
    {
        printerSerial = "";
        new Handler().postDelayed(() -> {
            try {
                printerSerial = BluetoothPrinter.getInstance().sendcommand_getString("OUT \"\";_SERIAL$").split("\r\n")[0];
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }

            Log.d("Serial", printerSerial);
        }, 1000);
    }

    private void showWarningDialog()
    {
        List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();

        if (periods.size() > 0) {

            ModelPeriod period = periods.get(periods.size() - 1);

            List<ModelPayment> payments = DatabaseAssistant.getAllPayments();

            if (payments.size() > 0) {

                for (ModelPayment payment : payments) {

                    if (payment.getStatus().equals("1") && payment.getPeriodo() == period.getPeriodNumber()) {

                        depositDialog.show();

                        Button dismissButton = depositDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE);
                        dismissButton.setVisibility(View.INVISIBLE);

                        new Handler().postDelayed(() -> dismissButton.setVisibility(View.VISIBLE), 2500);

                        break;
                    }
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (userInteractionListener != null)
            userInteractionListener.onUserInteraction();
        checkInternetConnection();

    }

    public void setUserInteractionListener(UserInterationListener userInteractionListener) {
        this.userInteractionListener = userInteractionListener;
    }

    public boolean checkInternetConnection()
    {
        ConnectivityManager con = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = con.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
        {
            tvInternet.setVisibility(View.VISIBLE);
            tvInternet.setText("Si hay Internet");
            tvInternet.setBackgroundColor(Color.parseColor("#255d00"));
            tvInternet.setCompoundDrawablesWithIntrinsicBounds(R.drawable.si_wifi, 0, 0, 0);
            return true;
        }
        else
        {
            tvInternet.setVisibility(View.VISIBLE);
            tvInternet.setText("No hay Internet");
            tvInternet.setCompoundDrawablesWithIntrinsicBounds(R.drawable.no_wifi, 0, 0, 0);
            tvInternet.setBackgroundColor(Color.RED);
            return false;
        }
    }

}
