package com.jaguarlabs.pabs.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.splunk.mint.Mint;

/**
 * Created by juliolemuslara on 3/26/15.
 */
public class MintActivity extends AppCompatActivity {

    /**********************************************************************************************
     *
     *                                         Splunk MINT SDK for Android
     *
     **********************************************************************************************/
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (com.jaguarlabs.pabs.util.BuildConfig.isIsForSplunkMintMonitoring()){
            Mint.disableNetworkMonitoring();
            Mint.initAndStartSession(this, "f604b706");
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        Mint.startSession(this);
        Mint.setFlushOnlyOverWiFi(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        Mint.closeSession(this);
    }

}
