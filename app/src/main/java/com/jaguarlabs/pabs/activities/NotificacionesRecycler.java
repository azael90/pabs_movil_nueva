package com.jaguarlabs.pabs.activities;

import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.NotificationsRecyclerAdapter;
import com.jaguarlabs.pabs.database.Notifications;

import java.util.List;

public class NotificacionesRecycler extends AppCompatActivity implements NotificationsRecyclerAdapter.ItemClickListener{

    private int pagina;

    private boolean descargarMas;

    private int lastPosition;

    //endregion

    //region Objects

    //region Statics

    private static Notificaciones mthis = null;

    //endregion

    private String idCobrador;

    private List<Notifications> notificationsList;

    private RecyclerView recyclerView;

    private RelativeLayout notificationLayout;

    private NotificationsRecyclerAdapter adapter;

    private LiveData<PagedList<Notifications>> notificaciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificaciones_recycler);

        recyclerView = findViewById(R.id.rv_notification);
        notificationLayout = findViewById(R.id.notificationLayout);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setHasFixedSize(true);

        adapter = new NotificationsRecyclerAdapter(this, this);

    }

    public void onClickFromXML(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                mthis = null;
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(Notifications notification, int position) {

    }
}
