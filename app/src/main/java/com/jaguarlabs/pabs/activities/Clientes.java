package com.jaguarlabs.pabs.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import com.google.gson.Gson;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.ClientesAdapter;
import com.jaguarlabs.pabs.adapter.ClientesRecyclerAdapter;
import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.components.PreferenceSettings;
import com.jaguarlabs.pabs.components.Preferences;
import com.jaguarlabs.pabs.database.Clients;
import com.jaguarlabs.pabs.database.Collector;
import com.jaguarlabs.pabs.database.Collectors;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.CompanyAmounts;
import com.jaguarlabs.pabs.database.Configuraciones;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.Actualizados;
import com.jaguarlabs.pabs.database.ExportImportDB;
import com.jaguarlabs.pabs.database.Localidad_Colonia;
import com.jaguarlabs.pabs.database.LocationNow;
import com.jaguarlabs.pabs.database.Locations;
import com.jaguarlabs.pabs.database.MontosTotalesDeEfectivo;
import com.jaguarlabs.pabs.database.Periods;
import com.jaguarlabs.pabs.database.SyncedWallet;
import com.jaguarlabs.pabs.models.ModelClient;
import com.jaguarlabs.pabs.models.ModelDeposit;
import com.jaguarlabs.pabs.models.ModelPayment;
import com.jaguarlabs.pabs.models.ModelPeriod;
import com.jaguarlabs.pabs.net.NetHelper;
import com.jaguarlabs.pabs.net.NetHelper.onWorkCompleteListener;
import com.jaguarlabs.pabs.net.NetService;
import com.jaguarlabs.pabs.net.VolleySingleton;

import com.jaguarlabs.pabs.rest.SpiceHelper;
import com.jaguarlabs.pabs.rest.models.ModelCellPhoneNumbersRequest;
import com.jaguarlabs.pabs.rest.models.ModelCellPhoneNumbersResponse;
import com.jaguarlabs.pabs.rest.models.ModelOsticketReponse;
import com.jaguarlabs.pabs.rest.models.ModelOsticketRequest;
import com.jaguarlabs.pabs.rest.models.ModelPrintPaymentRequest;
import com.jaguarlabs.pabs.rest.request.CellPhoneNumberRequest;
import com.jaguarlabs.pabs.rest.request.OsticketRequest;
import com.jaguarlabs.pabs.util.*;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.orm.SugarRecord;
import com.splunk.mint.Mint;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


import pl.tajchert.nammu.Nammu;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * This is the main activity from where everything flows.
 *
 * When starts, loads lots of resources and info
 * so depending on the phone it may take a whlie to load
 * when logging for the first time.
 */
@SuppressWarnings({"FieldCanBeLocal", "SpellCheckingInspection"})
@SuppressLint({ "SimpleDateFormat", "Wakelock" })

public class Clientes extends Fragment implements OnItemClickListener,
        CerrarSesionCallback, DatabaseAssistant.OnGetClientsFromSyncedWalletListener,
        AdapterView.OnItemSelectedListener, OnFragmentResult, ClientesRecyclerAdapter.ItemClickListener, UserInterationListener {

    //region Variables
    //region Primitives
    //SkeletonScreen skeletonScreen;
    public boolean bandera = false;
    ProgressDialog progress;
    Dialog myDialog, myDialog2;
    public ExtendedActivity extended;
    private static final long deviceDateTimeDifference = 1260000;//21 minutes
    private static final boolean writable = true;
    private static final int MAX_EFECTIVO_ALERTA = 500;

    public static long mTime = new Date().getTime();
    public static int notifications = 0;
    String phone = "", fecha_sistema_servidor = "";
    private long newTime;

    private boolean mandarABloqueo;
    private String robado;
    private AlertDialog.Builder dialogo;
    private int position;

    private int cont = 0;

    public static boolean targetURLTEST = false;

    private String getCierreOfDebtCollectorCode;

    //false -> 'Cierre Informativo'
    //true -> 'Cierre de Periodo'
    public boolean printCierrePerido = false;

    private boolean listViewStateGotten = false;

    private boolean firstLoad = false;

    private String osticketTopicSelected;
    private int osticketTopicSelectedPosition;
    private String osticketMessage;
    private ModelPrintPaymentRequest modelPrintPaymentRequest;
    String userLoader = "";
    //endregion

    //region Objects

    private static Clientes mthis = null;

    private static final String TAG = Clientes.class.getName();

    //private ClientesAdapter adapter;
    Dialog dialogo_de_bloqueo;
    private ClientesRecyclerAdapter adapter;
    private Preferences preferencesPendingTicket;
    private AlertDialog pendingTicketAlert;
    private String fechaUltimaActualizacion = "", fechaReactiva = "";
    private ArrayList<JSONObject> arrayCercanos;
    private JSONArray arrayClientes;

    private JSONObject datosCobrador;

    private List<Clients> clientsList;
    private List<Clients> filteredClients;
    private List<ModelClient> nearbyClientsFromSyncedWallet;
    private List<Clients> nearbyClients;
    private List<Locations> offlineLocations;
    private List<ModelPayment> offlinePayments;
    private List<SyncedWallet> clientListFromSyncedWallet;
    private List<SyncedWallet> clientListFromSyncedWalletAll;
    //private List<SyncedWallet> clientListFromSyncedWalletWithoutAnalysis;
    //private List<SyncedWallet> clientListFromSyncedWalletForAdapter;

    private PowerManager pm;
    private WakeLock wl;

    private Timer timer;
    private Timer timerNotificaciones;
    private Timer timerSendPaymentsEachTenMinutes;
    private Timer timerUpdate;

    private Random rand = new Random();

    private ClientesAdapter adapterSyncedWalletClients;
    //private ListView lvClientes;
    private RecyclerView lvClientes;

    private Parcelable listViewState;

    private DatabaseAssistant.OnGetClientsFromSyncedWalletListener onGetClientsFromSyncedWalletListener;

    private Dialog dialog;

    private Spinner spinnerOsticketTopics;
    private TextView tvAceptar;
    private TextView tvCancelar;
    private EditText osticketMessageEdiText;
    private EditText etFiltrado;
    private String nDias = "";
    private MainActivity mainActivity;
    private SpiceManager spiceManager;
    private SpiceHelper<ModelOsticketReponse> spiceSendOsticket;
    private SpiceHelper<ModelCellPhoneNumbersResponse> spiceSendContractsCellPhoneNumbers;

    private boolean updatePostponed = false;
    private LottieAnimationView lottieNF;
    private String fechaCierre;
    private TextView tvError, tvEfectivoAcumulado;
    private Animation izquierda_derecha, derecha_izquierda;
    private ImageView btAirplaneMode;
    private LocalDateTime date;
    boolean isRegisterAirPlaneMode= false;

    //public static IntentFilter s_intentFilter;
    //public static final String APP_KEY = "e36efec7a5ee2a81ec3b2d5fe1ad6105f0dfca75";

    //endregion

    //endregion

    //region Lifecycle

    /*
    if (BuildConfig.getTargetBranch() == BuildConfig.Branches.QUERETARO || BuildConfig.getTargetBranch() == BuildConfig.Branches.IRAPUATO
                                                || BuildConfig.getTargetBranch() == BuildConfig.Branches.SALAMANCA || BuildConfig.getTargetBranch() == BuildConfig.Branches.CELAYA
                                                || BuildConfig.getTargetBranch() == BuildConfig.Branches.LEON) {
                                            showPaymentReferenceDialog();
                                        } else {
     */

    public static Clientes newInstance() {
        return new Clientes();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_clientes, container, false);
        mainActivity = (MainActivity) getActivity();
        ((MainActivity) Objects.requireNonNull(getActivity())).setUserInteractionListener(this::onUserInteraction);
        lottieNF = (LottieAnimationView) view.findViewById(R.id.lottieNF);
        tvError = (TextView) view.findViewById(R.id.tvError);
        btAirplaneMode= view.findViewById(R.id.btAirplaneMode);
        tvEfectivoAcumulado = (TextView) view.findViewById(R.id.tv_total_efec);
        spiceManager = mainActivity.getSpiceManager();
        etFiltrado = view.findViewById(R.id.et_filtrado);
        myDialog = new Dialog(mainActivity);
        myDialog2 = new Dialog(mainActivity);
        dialogo_de_bloqueo = new Dialog(mainActivity);


        return view;
    }


    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

    void airPlaneCheck()
    {
        if(isAirplaneModeOn(mainActivity))
        {
            Toast.makeText(mainActivity, "NO PUDES TRABAJAR CON MODO AVIÓN ACTIVADO", Toast.LENGTH_LONG).show();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String fecha = sdf.format(new Date());
            try
            {
                if(!isRegisterAirPlaneMode){
                    DatabaseAssistant.insertAirPlaneActivated(datosCobrador.getString("no_cobrador"), fecha, "1");
                    isRegisterAirPlaneMode = true;
                }
            }catch (JSONException e)
            {
                Log.e("JSONEXCEPTION", e.getMessage());
            }

            cerrarSesion();
        }
    }

    boolean isAirPlaneModeActive()
    {
        if(isAirplaneModeOn(mainActivity))
            return true;
        else
            return  false;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        spiceSendOsticket = new SpiceHelper<ModelOsticketReponse>(spiceManager, "sendOsticket", ModelOsticketReponse.class, DurationInMillis.ONE_HOUR) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
            }

            @Override
            protected void onSuccess(ModelOsticketReponse result) {
                try {
                    if (result != null) {
                        super.onSuccess(result);
                        if (result.getResult() != null) {
                            DatabaseAssistant.updateOsticketAsSent(result.getResult());
                        } else {
                            //fail();
                        }
                    } else {
                        super.onSuccess(result);
                        //DatabaseAssistant.updateOsticketAsSent();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                super.onProgressUpdate(progress);
            }
        };

        spiceSendContractsCellPhoneNumbers = new SpiceHelper<ModelCellPhoneNumbersResponse>(spiceManager, "contractsCellPhoneNumbers", ModelCellPhoneNumbersResponse.class, DurationInMillis.ONE_HOUR) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
            }

            @Override
            protected void onSuccess(ModelCellPhoneNumbersResponse result) {
                try {
                    if (result != null) {
                        super.onSuccess(result);
                        if (result.getResult() != null) {
                            DatabaseAssistant.updateCellPhoneNumberAsSent(result.getResult());
                        } else {
                            //fail();
                        }
                    } else {
                        super.onSuccess(result);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                super.onProgressUpdate(progress);
            }
        };

        //init
        mthis = this;
        Deposito.depositosIsActive = false;
        mainActivity.isConnected = getArguments().getBoolean("isConnected", false);
        mainActivity.hasActiveInternetConnection(getContext(), mainActivity.mainLayout);
        Util.Log.ih("periodo: " + mainActivity.getEfectivoPreference().getInt("periodo", 0));

        mthis = this;
        ExtendedActivity.setEstoyLogin(false);
        mandarABloqueo = false;
        //gpsProvider = new GPSProvider(this);

        timerSendPaymentsEachTenMinutes = null;
        enviarCobrosDiezMinutos();
        pm = (PowerManager) getContext().getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "tag");
        wl.acquire();
        robado = "0";
        //iniciarHandlerInactividad();
        llamarTimerInactividad();
        //alert = new Builder(this);
        //arrayFiltrados = null;
        filteredClients = null;
        timer = null;

		/*
		if ( DatabaseAssistant.isThereMoreThanOnePeriodActive() ) {
			List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();
			getEfectivoEditor().putInt("periodo", periods.get( periods.size()-1 ).getPeriodNumber()).apply();
		}
		*/

        ImageView ivTexto = (ImageView) view.findViewById(R.id.iv_efectivo_texto);
        Animation animAlpha = AnimationUtils.loadAnimation(getContext(), R.anim.cash_animation);
        ivTexto.startAnimation(animAlpha);


        lvClientes = view.findViewById(R.id.rv_clientes);
        RecyclerView.LayoutManager layoutManager = new Clientes.CustomLinearLayoutManager(getContext());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        layoutManager.setItemPrefetchEnabled(true);


        lvClientes.setLayoutManager(layoutManager);
        lvClientes.setHasFixedSize(true);
        lvClientes.addItemDecoration(itemDecoration);
        lvClientes.setItemViewCacheSize(500);

        try {
            Mint.setUserIdentifier(loadUser());
        } catch (Exception e) {
            e.printStackTrace();
        }


        Bundle bundle = getArguments();

        /**
         * Instantiates datosCobrador variable giving it
         * company name and its serie
         *
         * NOTE*
         * 		ADDS COMPANIES
         *
         * NOTE**
         * 		USELESS CODE - REMOVE
         */
        if (bundle != null) {
            if (bundle.containsKey("datos_cobrador")) {
                try {
                    datosCobrador = new JSONObject(bundle.getString("datos_cobrador"));

                    DatabaseAssistant.setCobrador(datosCobrador.getString("no_cobrador"));

                    switch (BuildConfig.getTargetBranch()) {
                        case SALTILLO:
                        case CANCUN:
                            datosCobrador.put("Programa", datosCobrador.getString("serie_programa"));
                            datosCobrador.put("PBJ", datosCobrador.getString("serie_malba"));
                            datosCobrador.put("Cooperativa", datosCobrador.getString("serie_programa"));
                            datosCobrador.put("PABS LATINO", datosCobrador.getString("serie_empresa5"));
                            break;
                        default:
                            datosCobrador.put("Programa", datosCobrador.getString("serie_programa"));
                            datosCobrador.put("PBJ", datosCobrador.getString("serie_malba"));
                            datosCobrador.put("Cooperativa", datosCobrador.getString("serie_empresa4"));
                            datosCobrador.put("PABS LATINO", datosCobrador.getString("serie_empresa5"));
                            break;

                    }
                    mainActivity.getEfectivoEditor().putString("no_cobrador", datosCobrador.getString("no_cobrador")).apply();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    try {
                        datosCobrador = new JSONObject(bundle.getString("datos_cobrador"));

                        datosCobrador.put("Programa", datosCobrador.getString("serie_programa"));
                        datosCobrador.put("PBJ", datosCobrador.getString("serie_malba"));
                        datosCobrador.put("Cooperativa", datosCobrador.getString("serie_empresa4"));
                        datosCobrador.put("PABS LATINO", datosCobrador.getString("serie_empresa5"));
                        mainActivity.getEfectivoEditor().putString("no_cobrador", datosCobrador.getString("no_cobrador")).apply();

                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }

                if (getArguments().containsKey("query")) {
                    try {
                        String query = getArguments().getString("query", "");

                        Log.d("ServerQuery", query);

                        if (!query.equals("") && !query.equals("null")) {
                            SugarRecord.executeQuery(query);

                            getArguments().remove("query");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }


        /**
         * Timer to get notifications, starts
         */
        timerNotificaciones = null;
        timerNotificaciones = new Timer();
        timerNotificaciones.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mainActivity.isConnected) {
                    Util.Log.ih("getting personal notifications");
                    getNumNotificaciones();
                }
            }
        }, 1000, 5000);//1000, (1000 * 30) * 30);



        SharedPreferences preferences = mainActivity.getSharedPreferences("PABS_Main", Context.MODE_PRIVATE);
        if (preferences.contains("numero_notificaciones")) {
            TextView tvICon = (TextView) view.findViewById(R.id.tv_icon_notificaciones);
            tvICon.setText(preferences.getString("numero_notificaciones", "" + 0));
        }

        try {
            putInfo();
        } catch (JSONException e1) {
            e1.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setListeners();

        /**
         * Search Clients field
         *
         * looks for given text on the databasse
         */


        if (BuildConfig.getTargetBranch() == BuildConfig.Branches.QUERETARO || BuildConfig.getTargetBranch() == BuildConfig.Branches.IRAPUATO
                || BuildConfig.getTargetBranch() == BuildConfig.Branches.SALAMANCA || BuildConfig.getTargetBranch() == BuildConfig.Branches.CELAYA
                || BuildConfig.getTargetBranch() == BuildConfig.Branches.LEON) {
            Log.e("PLAZAS EXTERNAS", "Esta plaza no contine la actualizacion de cartera.");
        } else {
            Log.e("PLAZA INTERNA", "Esta plaza si lanza la actualizacion de cartera.");
            String fechaCollectors = "", fechaActual = "";

            List<Collectors> collectors = Collectors.findWithQuery(Collectors.class, "SELECT * FROM COLLECTORS;");
            if (collectors.size() > 0) {
                fechaCollectors = collectors.get(0).getFecha_sistema_servidor();
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd");
                fechaActual = mdformat.format(calendar.getTime());

                if (fechaCollectors != null && fechaCollectors.equals(fechaActual)) {
                    //Fechas iguales
                    tvError.setVisibility(View.GONE);
                    lottieNF.setVisibility(View.INVISIBLE);
                } else {
                    tvError.setVisibility(View.VISIBLE);
                    lottieNF.setAnimation("act.json");
                    lottieNF.loop(true);
                    lottieNF.playAnimation();
                }
            }
        }


        //lottieNF = (LottieAnimationView) view.findViewById(R.id.lottieNF);
        if (Preferences.obtenerPreferenceBoolean(mainActivity, Preferences.KEY_CARTERA_ACTUALIZADA)) {
            Log.d("IMPRESION -- >", "NO MOSTRAR ANIMACION");
        } else {
            //izquierda_derecha = AnimationUtils.loadAnimation(getActivity(), R.anim.izquierda_derecha);
            //tvError.setVisibility(View.VISIBLE);
            //tvError.setAnimation(izquierda_derecha);
            //lottieNF.setAnimation("act.json");
            //lottieNF.loop(true);
            //lottieNF.playAnimation();
        }


        etFiltrado.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //lanzar un CustomDialog para pedir la contraseña de ingreso
                Vibrator vibra = (Vibrator) mainActivity.getSystemService(VIBRATOR_SERVICE);
                vibra.vibrate(0700);
                pedirContraseña(v);
                return false;
            }
        });


        etFiltrado.addTextChangedListener(new TextWatcher() {
            //private Timer timerToSearch = new Timer();
            //private final long DELAY = 10;
            Thread thread;

            @Override
            public void afterTextChanged(Editable arg0) {

                if (thread != null && thread.isAlive())
                    thread.interrupt();

                thread = new Thread(() -> {

                    try {
                        if (etFiltrado.length() > 0) {

                            // TODO
                            if (nearbyClients != null || nearbyClientsFromSyncedWallet != null) {// si el filtro de cercanos esta
                                Log.d("nearbyClients", "!= NULL");
                                final ImageButton cercanos = (ImageButton) view.findViewById(R.id.btn_buscar_cercanos);
                                mainActivity.runOnUiThread(cercanos::performClick);
                            } else {
                                if (!listViewStateGotten) {
                                    mainActivity.runOnUiThread(() -> {
                                        listViewStateGotten = !listViewStateGotten;
                                    });
                                }
                                Log.d("nearbyClients", "NULL");
                            }

                            filteredClients = null;

                            {
                                /**
                                 * ==============================
                                 * |           CHEATS			|
                                 * ==============================
                                 * This is for debugging purposes
                                 */

                                /**
                                 * changes period
                                 */
                                if (arg0.length() > 0 && arg0.toString().contains("changePeriod%")) {
                                    try {
                                        String period = arg0.toString().substring(
                                                arg0.toString().indexOf("%") + 1,
                                                arg0.toString().lastIndexOf("%")
                                        );
                                        Util.Log.ih("periodo obtenido = " + period);
                                        String previoudPeriod = Integer.toString(mainActivity.getEfectivoPreference().getInt("periodo", 0));
                                        LogRegister.registrarCheat(arg0.toString() + " Periodo anterior: " + previoudPeriod);
                                        mainActivity.getEfectivoEditor().putInt("periodo", Integer.parseInt(period)).apply();
                                        DatabaseAssistant.eraseDatabaseAfterChangingPeriod();
                                        mainActivity.toastL("El periodo ha cambiado a: " + period + "; periodo anterior: " + previoudPeriod);
                                        printCierrePerido = !printCierrePerido;
                                        Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                        DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                        executeSendOsticket();
                                    } catch (StringIndexOutOfBoundsException e) {
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                /**
                                 * exports database to device memory
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("exportDB%")) {
                                    ExportImportDB exportImportDB = new ExportImportDB();
                                    LogRegister.registrarCheat(arg0.toString());
                                    try {
                                        if (exportImportDB.exportDB()) {
                                            mainActivity.toastL("Base de datos exportada correctamente");
                                        } else {
                                            mainActivity.toastL("Hubo un error al exportar la base de datos.");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * exports database to device memory
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("exportDBHidden%")) {
                                    ExportImportDB exportImportDB = new ExportImportDB();
                                    LogRegister.registrarCheat(arg0.toString());
                                    try {
                                        if (exportImportDB.exportDBHidden()) {
                                            mainActivity.toastL("Base de datos exportada correctamente");
                                        } else {
                                            mainActivity.toastL("Hubo un error al exportar la base de datos.");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * exports database to device memory
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("files%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    File sdCardRoot = Environment.getExternalStorageDirectory();
                                    File yourDir = new File(sdCardRoot, "/PABS_LOGS/.TestDataBase Backup/");
                                    for (File f : yourDir.listFiles()) {
                                        if (f.isFile()) {
                                            String name = f.getName();
                                            Util.Log.ih("name = " + name);
                                            Util.Log.ih("lastModified = " + new Date(f.lastModified()));
                                            // Do your stuff
                                        } else {
                                            String name = f.getName();
                                            Util.Log.ih("f name = " + name);
                                            Util.Log.ih("lastModified = " + new Date(f.lastModified()));
                                        }
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * imports databasse from device memory to app
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("importDB%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    ExportImportDB exportImportDB = new ExportImportDB();
                                    try {
                                        if (exportImportDB.importDB()) {
                                            mainActivity.toastL("Base de datos importada correctamente");
                                        } else {
                                            mainActivity.toastL("Hubo un error al importar la base de datos.");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * imports databasse from device memory to app
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("importDBHidden%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    ExportImportDB exportImportDB = new ExportImportDB();
                                    try {
                                        if (exportImportDB.importDBHidden()) {
                                            mainActivity.toastL("Base de datos importada correctamente");
                                        } else {
                                            mainActivity.toastL("Hubo un error al importar la base de datos.");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * change target URL to ecobrotest
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("changeURL%TEST%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    targetURLTEST = true;
                                    Preferences p = new Preferences(ApplicationResourcesProvider.getContext());
                                    p.saveURLTEST();
                                    mainActivity.toastL("Has cambiado a servidor de TEST. Por favor reinicia la app");
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * change target URL to ecobro
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("changeURL%PROD%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    targetURLTEST = false;
                                    Preferences p = new Preferences(ApplicationResourcesProvider.getContext());
                                    p.clearURL();
                                    mainActivity.toastL("Has cambiado a servidor de PRODUCCION. Por favor reinicia la app");
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * delete all payments
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("deleteAllPayments%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    DatabaseAssistant.deleteAllPayments();
                                    mainActivity.toastL("Eliminando todos los cobros");
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * set contract as paid
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("setContractAsPaid%")) {
                                    try {
                                        String contractNumber = arg0.toString().substring(
                                                arg0.toString().indexOf("%") + 1,
                                                arg0.toString().lastIndexOf("%")
                                        );
                                        LogRegister.registrarCheat(arg0.toString());
                                        Util.Log.ih("contrato obtenido = " + contractNumber);
                                        String contractID = DatabaseAssistant.getContractID(contractNumber);
                                        if (contractID != null) {
                                            DatabaseAssistant.updatePaymentMade(contractID);
                                            mainActivity.toastL("El contrato " + contractNumber + " se ha seteado como pagado en bd");
                                        } else {
                                            mainActivity.toastL("El contrato " + contractNumber + " no existe");
                                        }
                                    } catch (StringIndexOutOfBoundsException e) {
                                        //nothing here
                                    } catch (Exception e) {
                                        //nothing here
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * set contract as visited
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("setContractAsVisited%")) {
                                    try {
                                        String contractNumber = arg0.toString().substring(
                                                arg0.toString().indexOf("%") + 1,
                                                arg0.toString().lastIndexOf("%")
                                        );
                                        LogRegister.registrarCheat(arg0.toString());
                                        Util.Log.ih("contracto obtenido = " + contractNumber);
                                        String contractID = DatabaseAssistant.getContractID(contractNumber);
                                        if (contractID != null) {
                                            DatabaseAssistant.updateVisitMade(contractID);
                                            mainActivity.toastL("El contrato " + contractNumber + " se ha seteado como visitado en bd");
                                        } else {
                                            mainActivity.toastL("El contrato " + contractNumber + " no existe");
                                        }
                                    } catch (StringIndexOutOfBoundsException e) {
                                        //nothing here
                                    } catch (Exception e) {
                                        //nothing here
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * get contracts mark up as not visited
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("getContractsNotVisited%")) {
                                    try {
                                        mainActivity.runOnUiThread(() -> {
                                            try {
                                                //dismissMyCustomDialog();
                                                mainActivity.showAlertDialog("Contratos",
                                                        DatabaseAssistant.getContractsNotVisitedFullInfo(), true);
                                            } catch (Exception ex) {
                                                Toast toast = Toast.makeText(getContext(), DatabaseAssistant.getContractsNotVisitedFullInfo(), Toast.LENGTH_LONG);
                                                toast.setGravity(Gravity.CENTER, 0, 0);
                                                toast.show();
                                            }
                                        });
                                    } catch (StringIndexOutOfBoundsException e) {
                                        //nothing here
                                    } catch (Exception e) {
                                        //nothing here
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * do not show yellow and red contracts
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("deleteYellowsAndReds%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    DatabaseAssistant.resetContractsNotVisitedFromYesterdayAndBeforeYesterday();
                                    mainActivity.toastL("Modificando contratos, espera...");
                                    updateListView(true);
                                    mainActivity.toastL("Contratos modificados");
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * resends specific payment
                                 */
                                if (arg0.length() > 0 && arg0.toString().contains("resendPayment%")) {
                                    try {
                                        String folio = arg0.toString().substring(
                                                arg0.toString().indexOf("%") + 1,
                                                arg0.toString().indexOf(",")
                                        );
                                        String serie = arg0.toString().substring(
                                                arg0.toString().indexOf(",") + 1,
                                                arg0.toString().lastIndexOf("%")
                                        );
                                        LogRegister.registrarCheat(arg0.toString());
                                        Util.Log.ih("folio obtenido = " + folio);
                                        Util.Log.ih("serie obtenida = " + serie);
                                        boolean response = DatabaseAssistant.updatePaymentAsNotSyncedToSendAgain(serie, folio);
                                        mainActivity.toastL(response ? "Se modificó el recibo. Ingresa al cierre informativo." : "No se encontró el recibo");
                                        printCierrePerido = !printCierrePerido;
                                    } catch (StringIndexOutOfBoundsException e) {
                                        //nothing here
                                    } catch (Exception e) {
                                        //nothing here
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * resends all payment
                                 */
                                if (arg0.length() > 0 && arg0.toString().contains("resendAllPayments%")) {
                                    try {
                                        LogRegister.registrarCheat(arg0.toString());
                                        String response = DatabaseAssistant.updateAllPaymentAsNotSyncedToSendAgain();
                                        //mainActivity.toastL(response ? "Se modificó los recibos. Ingresa al cierre informativo." : "No se encontró recibos");
                                        mainActivity.runOnUiThread(() -> {
                                            try {
                                                //dismissMyCustomDialog();
                                                mainActivity.showAlertDialog("Folios",
                                                        response, true);
                                            } catch (Exception ex) {
                                                Toast toast = Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG);
                                                toast.setGravity(Gravity.CENTER, 0, 0);
                                                toast.show();
                                            }
                                        });

                                        printCierrePerido = !printCierrePerido;
                                    } catch (StringIndexOutOfBoundsException e) {
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * Execute raw query
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("executeQuery%")) {
                                    try {

                                        String query = arg0.toString().replace("$", "*").replace("_", "'");

                                        query = query.substring(query.indexOf("%") + 1, query.lastIndexOf("%"));
                                        LogRegister.registrarCheat(arg0.toString());
                                        SugarRecord.executeQuery(query);
                                        mainActivity.toastL("Query ejecutado: " + query);
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * create 500 payments
                                 */

                                else if (arg0.length() > 0 && arg0.toString().contains("createPayments%")) {
                                    try {
                                        int numberOfContracts = Integer.parseInt(arg0.toString().substring(arg0.toString().indexOf("%") + 1,
                                                arg0.toString().lastIndexOf("%")));
                                        LogRegister.registrarCheat(arg0.toString());
                                        mainActivity.toastL("Creando pagos...");

                                        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        SharedPreferences folios = mainActivity.getSharedPreferences("folios", Context.MODE_PRIVATE);

                                        int folio = Integer.parseInt(folios.getString("Cooperativa", "0"));
                                        //int folio = 1000;
                                        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                                        LocalDateTime date = LocalDateTime.now();
                                        for (int i = 0; i < numberOfContracts; i++) {
                                            folio++;

                                            DatabaseAssistant.insertPayment(
                                                    "04",
                                                    "20.6826024",
                                                    "-103.3821053",
                                                    "1",
                                                    datosCobrador.getString("no_cobrador"),
                                                    "1",
                                                    "04257",
                                                    formatter.print(date),
                                                    "1CJ",
                                                    Integer.toString(folio),
                                                    datosCobrador.getString("serie_empresa4"),
                                                    "1",
                                                    mainActivity.getEfectivoPreference().getInt("periodo", 0),
                                                    "0"
                                            );

                                            date = date.plusMinutes(1);
                                        }

                                            /*folio = Integer.parseInt(folios.getString("Programa", "0"));
                                            for (int i = 0; i < numberOfContracts; i++)
                                            {
                                                folio++;

                                                DatabaseAssistant.insertPayment(
                                                        "01",
                                                        "20.6826024",
                                                        "-103.3821053",
                                                        "1",
                                                        datosCobrador.getString("no_cobrador"),
                                                        "1",
                                                        "04257",
                                                        dateFormat.format(new Date()),
                                                        "1CJ",
                                                        Integer.toString(folio),
                                                        datosCobrador.getString("serie_programa"),
                                                        "1",
                                                        mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                                );
                                            }

                                            folio = Integer.parseInt(folios.getString("PBJ", "0"));
                                            for (int i = 0; i < numberOfContracts; i++)
                                            {
                                                folio++;

                                                DatabaseAssistant.insertPayment(
                                                        "03",
                                                        "20.6826024",
                                                        "-103.3821053",
                                                        "1",
                                                        datosCobrador.getString("no_cobrador"),
                                                        "1",
                                                        "04257",
                                                        dateFormat.format(new Date()),
                                                        "1CJ",
                                                        Integer.toString(folio),
                                                        datosCobrador.getString("serie_malba"),
                                                        "1",
                                                        mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                                );
                                            }

                                            folio = Integer.parseInt(folios.getString("PABS LATINO", "0"));
                                            for (int i = 0; i < numberOfContracts; i++)
                                            {
                                                folio++;

                                                DatabaseAssistant.insertPayment(
                                                        "05",
                                                        "20.6826024",
                                                        "-103.3821053",
                                                        "1",
                                                        datosCobrador.getString("no_cobrador"),
                                                        "1",
                                                        "04257",
                                                        dateFormat.format(new Date()),
                                                        "1CJ",
                                                        Integer.toString(folio),
                                                        datosCobrador.getString("serie_empresa5"),
                                                        "1",
                                                        mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                                );
                                            }*/

                                        mainActivity.toastL("Pagos creados");
                                    } catch (Exception ex) {

                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                    /*else if (arg0.length() > 0 && arg0.toString().contains("createPayments%")) {
                                        LogRegister.registrarCheat(arg0.toString());
                                        mainActivity.toastL("Creando pagos...");

                                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                        int folio4 = 3333;
                                        for (int i = 0; i < 1; i++) {
                                            folio4 += 1;
                                            String folioString = "" + folio4;
                                            DatabaseAssistant.insertPayment(
                                                    "04",
                                                    "20.6826024",
                                                    "-103.3821053",
                                                    "1",
                                                    "27561",
                                                    "1",
                                                    "042753",
                                                    dateFormat.format(new Date()),
                                                    "1CJ",
                                                    folioString,
                                                    "E",
                                                    "1",
                                                    mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                            );
                                        }

                                        int folio3 = 1111;
                                        for (int i = 0; i < 1; i++) {
                                            folio3 += 1;
                                            String folioString = "" + folio3;
                                            DatabaseAssistant.insertPayment(
                                                    "03",
                                                    "20.6826024",
                                                    "-103.3821053",
                                                    "1",
                                                    "27561",
                                                    "1",
                                                    "042753",
                                                    dateFormat.format(new Date()),
                                                    "1CJ",
                                                    folioString,
                                                    "E",
                                                    "1",
                                                    mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                            );
                                        }

                                        int folio2 = 9999;
                                        for (int i = 0; i < 1; i++) {
                                            folio2 += 1;
                                            String folioString = "" + folio2;
                                            DatabaseAssistant.insertPayment(
                                                    "02",
                                                    "20.6826024",
                                                    "-103.3821053",
                                                    "1",
                                                    "27561",
                                                    "1",
                                                    "042753",
                                                    dateFormat.format(new Date()),
                                                    "1CJ",
                                                    folioString,
                                                    "E",
                                                    "1",
                                                    mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                            );
                                        }

                                        mainActivity.toastL("Pagos creados");
                                    }*/
                                /**
                                 * sets internet connection as reachable
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("internet%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    ExtendedActivity.setInternetAsReachable = !ExtendedActivity.setInternetAsReachable;
                                    mainActivity.toastL("Internet " + (ExtendedActivity.setInternetAsReachable ? "siempre activo" : ""));
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * runs method to change visited or paid status
                                 * (show contracts that have to pay today)
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("changeVisitedOrPaidStatus%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    Calendar calendar = Calendar.getInstance();
                                    int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                                    int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                                    int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);

                                    mainActivity.toastL("Modificando contratos, espera...");
                                    DatabaseAssistant.changeVisitedOrPaidStatusFromContractsManual(dayOfWeek, dayOfMonth, dayOfYear);
                                    //toastL("Modificando contratos, espera...");
                                    updateListView(true);
                                    mainActivity.toastL("Contratos modificados");
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * updates list view
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("updateWallet%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    mainActivity.toastS("Actualizando cartera...");
                                    updateListView(true);
                                    mainActivity.toastS("Listo");
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * create GeoJson file
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("geoJson%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    mainActivity.toastS("Creando archivo...");
                                    try {
                                        //ApplicationResourcesProvider.saveGeoJSONFile(datosCobrador.getString("periodo"), datosCobrador.getString("no_cobrador"));
                                        ApplicationResourcesProvider.saveGeoJSONFile("" + mainActivity.getEfectivoPreference().getInt("periodo", 0), datosCobrador.getString("no_cobrador"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    mainActivity.toastS("Listo");
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * create GeoJson file
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("resetContracts%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    DatabaseAssistant.resetContractsPerWeek();
                                    mainActivity.toastS("contratos reiniciados");
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * send payments
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("sendPaymentstxt%")) {
                                    LogRegister.registrarCheat(arg0.toString());
                                    registerPagostxt();
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * get 'cierre informativo' of another debt colletor
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("getCierre%")) {
                                    try {
                                        getCierreOfDebtCollectorCode = arg0.toString().substring(
                                                arg0.toString().indexOf("%") + 1,
                                                arg0.toString().lastIndexOf("%"));
                                        LogRegister.registrarCheat(arg0.toString());
                                        Util.Log.ih("codigo obtenido = " + getCierreOfDebtCollectorCode);
                                        mainActivity.toastL("Ingresa al cierre informativo para obtener el cierre del cobrador con el código: " + getCierreOfDebtCollectorCode);
                                    } catch (StringIndexOutOfBoundsException e) {
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * add new period to db
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("addNewPeriod%")) {
                                    try {
                                        String period = arg0.toString().substring(arg0.toString().indexOf("%") + 1, arg0.toString().lastIndexOf("%"));

                                        LogRegister.registrarCheat(arg0.toString());
                                        Util.Log.ih("periodo obtenido = " + period);
                                        DatabaseAssistant.insertNewPeriodIfNeeded(Integer.parseInt(period));
                                        String arreglo[] = {"01", "03", "04", "05"};
                                        for (int i = 0; i < arreglo.length; i++) {
                                            DatabaseAssistant.insertarMontosTotalesPorEmpresaYPeriodo(arreglo[i], "0", "0", "0", "0", "0", "0", "" + period);
                                        }
                                        mainActivity.toastL(DatabaseAssistant.checkLastThreePeriodsStatus());
                                        //DatabaseAssistant.updateEndDatePeriodButKeepActive(60);
                                        //toastL("Nuevo periodo insertado");
                                    } catch (StringIndexOutOfBoundsException e) {
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * close active period
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("closePeriod%")) {
                                    try {
                                        String period = arg0.toString().substring(
                                                arg0.toString().indexOf("%") + 1,
                                                arg0.toString().lastIndexOf("%")
                                        );
                                        LogRegister.registrarCheat(arg0.toString());
                                        Util.Log.ih("periodo obtenido = " + period);
                                        DatabaseAssistant.closePeriod(period);
                                        mainActivity.toastL(DatabaseAssistant.checkLastThreePeriodsStatus());
                                        //DatabaseAssistant.updateEndDatePeriodButKeepActive(60);
                                        //toastL("Nuevo periodo insertado");
                                    } catch (StringIndexOutOfBoundsException e) {
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 * change server ip manually
                                 */
                                else if (arg0.length() > 0 && arg0.toString().contains("changeServerIP%")) {

                                    try {
                                        String IP = arg0.toString().substring(
                                                arg0.toString().indexOf("%") + 1,
                                                arg0.toString().lastIndexOf("%")
                                        );
                                        LogRegister.registrarCheat(arg0.toString());
                                        Util.Log.ih("IP obtenida = " + IP);

                                        Preferences preferencesServerIP = new Preferences(ApplicationResourcesProvider.getContext());
                                        String oldServerIP = preferencesServerIP.getServerIP();

                                        preferencesServerIP.changeServerIP(IP);
                                        mainActivity.toastL("La IP a cambiado a: " + IP + "\nIP anterior: " + oldServerIP);
                                    } catch (StringIndexOutOfBoundsException e) {
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                } else if (arg0.length() > 0 && arg0.toString().contains("printerCommand%")) {
                                    try {

                                        String command = arg0.toString().substring(arg0.toString().indexOf("%") + 1,
                                                arg0.toString().lastIndexOf("%"));

                                        String tspl = "";
                                        String prefix = "";
                                        int counter = 0;

                                        switch (command.toUpperCase()) {
                                            case "SERIAL":
                                                tspl = "_SERIAL$";
                                                prefix = "Serial No: ";
                                                break;
                                            case "MODEL":
                                                tspl = "_MODEL$";
                                                prefix = "Model: ";
                                                break;
                                            case "VERSION":
                                                tspl = "_VERSION$";
                                                prefix = "Version: ";
                                                break;
                                            case "ADDRESS":
                                                tspl = "GETSETTING$(\"CONFIG\", \"BT\", \"MAC ADDRESS\")";
                                                prefix = "BT MAC Address: ";
                                                break;
                                            case "CHECKSUM":
                                                tspl = "GETSETTING$(\"SYSTEM\", \"INFORMATION\", \"CHECKSUM\")";
                                                prefix = "Checksum: ";
                                                break;
                                            case "DPI":
                                                tspl = "GETSETTING$(\"SYSTEM\", \"INFORMATION\", \"DPI\")";
                                                prefix = "DPI: ";
                                                break;
                                            case "RESET":
                                                tspl = (char) 27 + "!R";
                                                break;
                                            case "FEED":
                                                tspl = (char) 27 + "!F";
                                                break;
                                            case "PAUSE":
                                                tspl = (char) 27 + "!P";
                                                break;
                                            case "CONTINUE":
                                                tspl = (char) 27 + "!O";
                                                break;
                                        }

                                        while (counter < 5) {
                                            Log.d("SerialCounter", Integer.toString(counter));

                                            try {

                                                BluetoothPrinter.getInstance().connect();

                                                BluetoothPrinter.getInstance().setup(
                                                        SharedConstants.Printer.width,
                                                        40,
                                                        SharedConstants.Printer.speed,
                                                        SharedConstants.Printer.density,
                                                        SharedConstants.Printer.sensorType,
                                                        SharedConstants.Printer.gapBlackMarkVerticalDistance,
                                                        SharedConstants.Printer.gapBlackMarkShiftDistance
                                                );

                                                //String serial = BluetoothPrinter.getInstance().sendcommand_getString("OUT \"\";_SERIAL$").split("\r\n")[0];
                                                String serial = BluetoothPrinter.getInstance().sendcommand_getString("OUT \"\";" + tspl).split("\r\n")[0];

                                                if (!serial.equals(" ")) {
                                                    BluetoothPrinter.getInstance().clearbuffer();

                                                    BluetoothPrinter.getInstance().printerfont(100, 150, "3", 0, 0, 0, prefix + serial);

                                                    BluetoothPrinter.getInstance().printlabel(1, 1);

                                                    mainActivity.toastS(prefix + serial);
                                                }

                                                break;
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                                counter++;
                                            }
                                        }

                                        if (counter == 5) {
                                            Log.d("SerialCounter", "No se pudo conectar con la impresora");
                                            mainActivity.toastS("No se pudo conectar con la impresora");
                                        }
                                    } catch (StringIndexOutOfBoundsException ex) {
                                        ex.printStackTrace();
                                    }
                                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                                    DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                                    executeSendOsticket();
                                }
                                /**
                                 *
                                 */
                                /**
                                 * Here is where looks for given text on database
                                 */
                                else {

                                    /*
                                    arrayFiltrados = query.mostrarClientesFiltrados(
                                            etFiltrado.getText().toString()).getJSONArray(
                                            "clientes");
                                    */
                                    /*filteredClients = new ArrayList<>();

                                    for (Clients c: clientsList){
                                        if ( (c.getName() + " " + c.getFirstLastName() + " " + c.getSecondLastName()).contains(arg0.toString().toUpperCase()) ){
                                            filteredClients.add(c);
                                        } else if ( (c.getSerie() + c.getNumberContract()).contains(arg0.toString().toUpperCase()) ){
                                            filteredClients.add(c);
                                        } else if ( (c.getStreetToPay() + " " + c.getNumberExt() + " " + c.getNeighborhood() + " " + c.getBetweenStreets() + " " + c.getLocality()).contains(arg0.toString().toUpperCase()) ){
                                            filteredClients.add(c);
                                        }
                                    }
                                    */
                                    try {
                                        filteredClients = DatabaseAssistant.getFilteredClients(arg0.toString());
                                    } catch (SQLiteException e) {
                                        e.printStackTrace();
                                        filteredClients = null;
                                    }
                                }
                            }

                            if (filteredClients == null) {
                                filteredClients = new ArrayList<>();
                            }
                            if (filteredClients.size() > -1) {
                                //ClientesAdapter adapter = getClientesAdapterFilteredClients();
                                adapter = getClientesAdapterFilteredClients();

                                //runOnUiThread(new Runnable() {
                                //	@Override
                                //	public void run() {
                                mainActivity.runOnUiThread(() -> {
                                    //lvClientes = (ListView) view.findViewById(R.id.listView_clientes);
                                    if (lvClientes != null) {
                                        lvClientes.setAdapter(adapter);
                                            /*lvClientes.setDivider(getContext().getResources().getDrawable(
                                                    R.drawable.divider_list_clientes));
                                            lvClientes.setOnItemClickListener(mthis);*/
                                    }
                                });
                                //	}
                                //});
                                        /*
                                        lvClientes = (ListView) findViewById(R.id.listView_clientes);
                                        lvClientes.setAdapter(adapter);
                                        lvClientes.setDivider(getResources().getDrawable(
                                                R.drawable.divider_list_clientes));
                                        lvClientes.setOnItemClickListener(mthis);
                                        */
                            } else {
                                etFiltrado.setText("");
                                try {
                                    mainActivity.toastS("Sin resultados para la busqueda, intenta de nuevo con otra opción");
                                } catch (Exception e) {
                                    mainActivity.runOnUiThread(() -> {
                                        Toast toast = Toast.makeText(getContext(), "Sin resultados para la busqueda, intenta de nuevo con otra opción", Toast.LENGTH_LONG);
                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                        toast.show();
                                    });
                                    e.printStackTrace();
                                }
                            }

                            /**
                             * Erases given text when touching on cancel icon
                             * and shows accounts within wallet
                             */

                            //runOnUiThread(new Runnable() {
                            //	@Override
                            //	public void run() {
                            mainActivity.runOnUiThread(() -> {
                                final ImageButton btnCancelarFiltrado = (ImageButton) view.findViewById(R.id.ib_cancelar_filtro);
                                btnCancelarFiltrado.setVisibility(View.VISIBLE);
                                btnCancelarFiltrado
                                        .setOnClickListener(view -> {
                                        /*
                                            arrayFiltrados = null;
                                            SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());

                                            if (BuildConfig.isWalletSynchronizationEnabled()) {
                                                JSONObject myWallet;
                                                try{
                                                    myWallet = query.getTodaysContracts();
                                                    if (myWallet.has("MyTodaysWallet")){
                                                        arrayClientes = myWallet.getJSONArray("MyTodaysWallet");
                                                    }
                                                    else{
                                                        arrayClientes = new JSONArray();
                                                    }
                                                } catch (JSONException e){
                                                    e.printStackTrace();
                                                }
                                            }
                                            */
                                            /*
                                            if (query.db != null){
                                                query.db.close();
                                            }
                                            query = null;
                                            */

                                            filteredClients = null;

                                            //ClientesAdapter adapter = getAdapterSyncedWalletClients();
                                            if (BuildConfig.isWalletSynchronizationEnabled()) {
                                                adapter = getAdapterSyncedWalletClients();
                                            } else {
                                                adapter = getClientesAdapterClientsList();
                                            }

                                            mainActivity.runOnUiThread(() -> {
                                                //lvClientes = (ListView) view.findViewById(R.id.listView_clientes);
                                                if (lvClientes != null) {
                                                    lvClientes.setAdapter(adapter);
                                                    if (listViewState != null) {
                                                        //lvClientes.onRestoreInstanceState(listViewState);
                                                    }
                                                        /*lvClientes
                                                                .setDivider(getResources()
                                                                        .getDrawable(
                                                                                R.drawable.divider_list_clientes));
                                                        lvClientes.setOnItemClickListener(mthis);*/
                                                }
                                                btnCancelarFiltrado
                                                        .setVisibility(View.GONE);
                                                etFiltrado.setText("");

                                                listViewStateGotten = false;
                                            });
                                        });
                                                    /*
                                                    lvClientes = (ListView) findViewById(R.id.listView_clientes);
                                                    lvClientes.setAdapter(adapter);
                                                    if (listViewState != null){
                                                        lvClientes.onRestoreInstanceState(listViewState);
                                                    }
                                                    lvClientes
                                                            .setDivider(getResources()
                                                                    .getDrawable(
                                                                            R.drawable.divider_list_clientes));
                                                    lvClientes.setOnItemClickListener(mthis);
                                                    btnCancelarFiltrado
                                                            .setVisibility(View.GONE);
                                                    etFiltrado.setText("");

                                                    listViewStateGotten = false;
                                                    */
                            });
                            //	}
                            //});


                        } else if (etFiltrado.length() == 0) {
                            //arrayFiltrados = null;
                            filteredClients = null;
                            //ClientesAdapter adapter = getAdapterSyncedWalletClients();
                            if (BuildConfig.isWalletSynchronizationEnabled()) {
                                adapter = getAdapterSyncedWalletClients();
                            } else {
                                adapter = getClientesAdapterClientsList();
                            }

                            //runOnUiThread(new Runnable() {
                            //	@Override
                            //	public void run() {
                            mainActivity.runOnUiThread(() -> {
                                //lvClientes = (ListView) view.findViewById(R.id.listView_clientes);
                                if (lvClientes != null) {
                                    lvClientes.setAdapter(adapter);
                                    if (listViewState != null) {
                                        //lvClientes.onRestoreInstanceState(listViewState);
                                    }
                                        /*lvClientes.setDivider(getContext().getResources().getDrawable(
                                                R.drawable.divider_list_clientes));
                                        lvClientes.setOnItemClickListener(mthis);*/
                                }
                                ImageButton btnCancelarFiltrado = (ImageButton) view.findViewById(R.id.ib_cancelar_filtro);
                                btnCancelarFiltrado.setVisibility(View.GONE);

                                listViewStateGotten = false;
                            });
                            //	}
                            //});
                                    /*
                                    lvClientes = (ListView) findViewById(R.id.listView_clientes);
                                    lvClientes.setAdapter(adapter);
                                    if (listViewState != null){
                                        lvClientes.onRestoreInstanceState(listViewState);
                                    }
                                    lvClientes.setDivider(getResources().getDrawable(
                                            R.drawable.divider_list_clientes));
                                    lvClientes.setOnItemClickListener(mthis);
                                    ImageButton btnCancelarFiltrado = (ImageButton) findViewById(R.id.ib_cancelar_filtro);
                                    btnCancelarFiltrado.setVisibility(View.GONE);

                                    listViewStateGotten = false;
                                    */

                        } else {

                            //runOnUiThread(new Runnable() {
                            //	@Override
                            //	public void run() {
                            final ImageButton btnCancelarFiltrado = (ImageButton) view.findViewById(R.id.ib_cancelar_filtro);
                            btnCancelarFiltrado.setVisibility(View.GONE);
                            //	}
                            //});
                                    /*
                                    final ImageButton btnCancelarFiltrado = (ImageButton) findViewById(R.id.ib_cancelar_filtro);
                                    btnCancelarFiltrado.setVisibility(View.GONE);
                                    */
                            //ClientesAdapter adapter = getAdapterSyncedWalletClients();
                            if (BuildConfig.isWalletSynchronizationEnabled()) {
                                adapter = getAdapterSyncedWalletClients();
                            } else {
                                adapter = getClientesAdapterClientsList();
                            }

                            //runOnUiThread(new Runnable() {
                            //	@Override
                            //	public void run() {
                            mainActivity.runOnUiThread(() -> {
                                //lvClientes = (ListView) view.findViewById(R.id.listView_clientes);
                                if (lvClientes != null) {
                                    lvClientes.setAdapter(adapter);
                                    if (listViewState != null) {
                                        //lvClientes.onRestoreInstanceState(listViewState);
                                    }
                                        /*lvClientes.setDivider(getResources().getDrawable(
                                                R.drawable.divider_list_clientes));
                                        lvClientes.setOnItemClickListener(mthis);*/
                                }
                            });
                            //	}
                            //});
                                    /*
                                    lvClientes = (ListView) mthis.findViewById(R.id.listView_clientes);
                                    lvClientes.setAdapter(adapter);
                                    if (listViewState != null){
                                        lvClientes.onRestoreInstanceState(listViewState);
                                    }
                                    lvClientes.setDivider(getResources().getDrawable(
                                            R.drawable.divider_list_clientes));
                                    lvClientes.setOnItemClickListener(mthis);
                                    */
                        }

                        //}
                        //});


                        //}

                        //public TimerTask setData(Editable arg0) {
                        //	this.arg0 = arg0;
                        //	return this;
                        //}
                        //}.setData(arg0), DELAY);

                    } catch (Exception e) {

                        //Util.Log.ih("inside first try catch");

                        e.printStackTrace();


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //Cambiar controles
                                etFiltrado.setText("");
                            }
                        });

                        //mainActivity.toastS("Sin resultados para la busqueda, intenta de nuevo con otra opción");

                        filteredClients = null;
                        if (BuildConfig.isWalletSynchronizationEnabled()) {
                            adapter = getAdapterSyncedWalletClients();
                        } else {
                            adapter = getClientesAdapterClientsList();
                        }

                        mainActivity.runOnUiThread(() -> {

                            if (lvClientes != null) {
                                lvClientes.setAdapter(adapter);
                                if (listViewState != null) {
                                    //lvClientes.onRestoreInstanceState(listViewState);
                                }

                            }
                            ImageButton btnCancelarFiltrado = (ImageButton) view.findViewById(R.id.ib_cancelar_filtro);
                            btnCancelarFiltrado.setVisibility(View.GONE);

                            listViewStateGotten = false;
                        });
                    }
                });

                thread.start();

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

        });

        if (mainActivity.isConnected && !DatabaseAssistant.isThereAnyClient())
        {
            try {
                showMyCustomDialog();
            } catch (Exception e) {
                Toast toast = Toast.makeText(getContext(), R.string.info_message_network_offline, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                e.printStackTrace();
            }
            getListClients();
        } else if (getArguments().getBoolean("isSupervisorOrSubstitute", false)
                && mainActivity.isConnected
                && !DatabaseAssistant.clientsFromSameCollector(getArguments().getString("clientsCollectorNumber"))) {
            try {
                showMyCustomDialog();
            } catch (Exception e) {
                Toast toast = Toast.makeText(getContext(), R.string.info_message_network_offline, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                e.printStackTrace();
            }
            getListClients();
        }
        else {
            Log.e("isconnected", "" + mainActivity.isConnected);

            if (!mainActivity.isConnected) {
                try {
                    mainActivity.showAlertDialog("", getResources().getString(R.string.info_message_network_offline), true);
                } catch (Exception e) {
                    Toast toast = Toast.makeText(getContext(), R.string.info_message_network_offline, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    e.printStackTrace();
                }
            }

            try {
                showMyCustomDialog();
                Snackbar.make(mainActivity.mainLayout, "Cargando...", Snackbar.LENGTH_SHORT).show();

                Runnable runnable = () ->
                {
                    if (BuildConfig.isWalletSynchronizationEnabled()) {
                        clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
                        clientListFromSyncedWalletAll = DatabaseAssistant.getAllClientsFromSyncedWallet();

                        mainActivity.runOnUiThread(() ->
                        {
                            adapter = getAdapterSyncedWalletClients();

                            if (lvClientes != null) {
                                lvClientes.setAdapter(adapter);
                            }
                            dismissMyCustomDialog();
                        });
                    } else {
                        clientsList = DatabaseAssistant.getAllClients();
                        mainActivity.runOnUiThread(() -> {
                            adapter = getClientesAdapterClientsList();
                            if (lvClientes != null) {
                                lvClientes.setAdapter(adapter);
                            }
                            dismissMyCustomDialog();
                        });
                    }
                };
                Thread thread = new Thread(runnable);

                thread.start();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        /**
         * Updates cash
         * this if statement is true after doing 'cierre de periodo'
         *
         * Web Service
         * http://50.62.56.182/ecobro/controlusuarios/getBalanceEmpresasPorCobradorYPeriodo'
         * JSONObject param
         * {
         * 		"periodo": ,
         * 		"no_cobrador": ""
         * }
         *
         */
        Log.e("va a sincronizar?", "" + getArguments().getBoolean("periodo", false));

        if (getArguments().getBoolean("periodo", false)) {
            Log.e("sincroniza,", "efectivo");
            Snackbar.make(mainActivity.mainLayout, "Sincronizando efectivo con el servidor... espera un momento", Snackbar.LENGTH_SHORT).show();
            //Toast.makeText(mainActivity, "Entro a WS", Toast.LENGTH_LONG).show();
            JSONObject params = new JSONObject();

            try {
                if (DatabaseAssistant.isThereMoreThanOnePeriodActive()) {
                    params.put("periodo", new JSONArray(new Gson().toJson(DatabaseAssistant.getAllPeriodsActive())));
                } else {
                    params.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
                }
                params.put("no_cobrador", mainActivity.getEfectivoPreference().getString("no_cobrador", ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            requestBalanceEmpresaPorCobradorVolley(params);


            /*NetService service = new NetService(ConstantsPabs.urlGetBalanceEmpresaPorCobradorYPeriodo, params);
            NetHelper helper = NetHelper.getInstance();
            helper.ServiceExecuter(service, getContext(), new onWorkCompleteListener()
            {

                @Override
                public void onError(Exception e) {
                    Log.e("balanceEmpresas", "Error al obtener balance por empresas");
                }

                @Override
                public void onCompletion(String result)
                {
                    //NEW PROCESS
                    JSONArray empresas;
                    JSONObject jsonResult;
                    try {
                        jsonResult = new JSONObject(result);

                        for(Iterator<String> iter = jsonResult.keys(); iter.hasNext(); ) {
                            String key = iter.next();
                            empresas = jsonResult.getJSONArray(key);
                            Util.Log.ih("empresas = " + empresas);

                            SharedPreferences.Editor efectivoEditor = mainActivity.getEfectivoEditor();

                            try {
                                for (int i = 0; i < empresas.length(); i++) {
                                    JSONObject empresa;
                                    try {
                                        empresa = empresas.getJSONObject(i);

                                        String nombre = DatabaseAssistant.getSingleCompany(empresa.getString("empresa")).getName();

                                        float cantidad = (float) empresa.getDouble("cobros");
                                        cantidad = cantidad - (float) empresa.getDouble("depositos");

                                        efectivoEditor.putFloat(nombre, cantidad).apply();
                                        //Util.Log.ih("commit boolean = " + commitB);
                                    } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                onError(e);
                            }
                        }
                    } catch (JSONException e) {
                        //nothing here...
                    }

                    Snackbar.make(mainActivity.mainLayout, "Efectivo sincronizado", Snackbar.LENGTH_SHORT).show();
                    try {
                        putInfo();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });*/
        }


        if(checkInternetConnection())
        {
            try {
                createJsonForLocalidades(datosCobrador.getString("no_cobrador"));
            }catch (JSONException e)
            {
                Log.e("PARAMETROSJSON", e.getMessage());
            }
        }
        else
            //Toast.makeText(mainActivity, "No hay internet para parametrosJson", Toast.LENGTH_SHORT).show();
                    
        
        enviarDepositos();

        if (com.jaguarlabs.pabs.util.BuildConfig.isAutoPrintingEnable()) {
            Handler handler = new Handler();
            handler.postDelayed(this::randomListClick, 10000);
        }

        preferencesPendingTicket = new Preferences(getContext());


        Util.Log.ih("datosCobrador = " + datosCobrador != null ? datosCobrador.toString() : "null");

        if (datosCobrador.has("fecha_cierre")) {
            try {
                fechaCierre = datosCobrador.getString("fecha_cierre");
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        } else {
            fechaCierre = "2018-01-31 10:52:47";
        }


    }

    public void insertSerialPrinter()
    {
        List<Collectors> listaCollectors = Collectors.findWithQuery(Collectors.class, "SELECT * FROM COLLECTORS");
        if (listaCollectors.size() > 0)
        {
            Configuraciones.deleteAll(Configuraciones.class);
            DatabaseAssistant.insertarConfiguracion(getPrinterSerial(), listaCollectors.get(0).getMacAddress());

        } else
            Log.i("IMPRESION-->", "No se obtuvieron registros de Collectors");
    }

    private void requestBalanceEmpresaPorCobradorVolley(JSONObject jsonParams)
    {
        //progress = ProgressDialog.show(mainActivity, "Iniciando sesión", "Por favor espera...", true);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlGetBalanceEmpresaPorCobradorYPeriodo, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                manage_RegisterPagosOffiline(response);
                JSONArray jsonDatosPorEmpresa;
                try {
                    for(Iterator<String> iter = response.keys(); iter.hasNext(); )
                    {
                        String key = iter.next();
                        jsonDatosPorEmpresa = response.getJSONArray(key);
                        SharedPreferences.Editor efectivoEditor = mainActivity.getEfectivoEditor();



                        try
                        {
                            for (int i = 0; i < jsonDatosPorEmpresa.length(); i++)
                            {
                                JSONObject objetoEmpresa;
                                try
                                {
                                    objetoEmpresa = jsonDatosPorEmpresa.getJSONObject(i);
                                    String empresaRespuesta= objetoEmpresa.getString("empresa");
                                    float cobrosRespuesta = (float) objetoEmpresa.getDouble("cobros");
                                    float depositosRespuesta = (float) objetoEmpresa.getDouble("depositos");
                                    float cobrosCanceladosRespuesta = (float) objetoEmpresa.getDouble("cobros_cancelados");
                                    float cobrosSumandoCanceladosRespuesta = (float) objetoEmpresa.getDouble("cobros_sumando_cancelados");
                                    float totalCobrosMenosDepositosRespuesta = (float) objetoEmpresa.getDouble("total_cobros_menos_depositos");
                                    float cobrosMenosDepositosRespuesta = (float) objetoEmpresa.getDouble("cobros_menos_depositos");

                                    String nombre_de_la_empresa_registrada_en_base_de_datos = DatabaseAssistant.getSingleCompany(empresaRespuesta).getName();
                                    if(DatabaseAssistant.hayRegistrosConElPeriodoActual(mainActivity.getEfectivoPreference().getInt("periodo", 0), empresaRespuesta))
                                    {
                                        String queryActualizacion="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET cobros ='"+cobrosRespuesta+"', depositos='"+depositosRespuesta
                                                +"', cancelados='"+cobrosCanceladosRespuesta+"', cobrosMasCancelados='"+cobrosSumandoCanceladosRespuesta+"', totalCobrosMenosDepositos='"+totalCobrosMenosDepositosRespuesta
                                                +"', cobrosMenosDepositos='"+cobrosMenosDepositosRespuesta+"' WHERE periodo='"+mainActivity.getEfectivoPreference().getInt("periodo", 0)+"' and empresa='"+empresaRespuesta+"'";
                                        MontosTotalesDeEfectivo.executeQuery(queryActualizacion);
                                    }
                                    else
                                    {
                                        DatabaseAssistant.insertarMontosTotalesPorEmpresaYPeriodo(
                                                ""+ empresaRespuesta,
                                                ""+ cobrosRespuesta,
                                                ""+depositosRespuesta,
                                                ""+cobrosCanceladosRespuesta,
                                                ""+ cobrosSumandoCanceladosRespuesta,
                                                ""+totalCobrosMenosDepositosRespuesta,
                                                ""+ cobrosMenosDepositosRespuesta,
                                                ""+ mainActivity.getEfectivoPreference().getInt("periodo", 0));
                                    }

                                    /*efectivoEditor.putFloat("cobros "+nombre_de_la_empresa_registrada_en_base_de_datos, cobrosRespuesta).apply();
                                    efectivoEditor.putFloat("depositos" +nombre_de_la_empresa_registrada_en_base_de_datos, depositosRespuesta).apply();
                                    efectivoEditor.putFloat("cancelados " +nombre_de_la_empresa_registrada_en_base_de_datos, cobrosCanceladosRespuesta).apply();
                                    efectivoEditor.putFloat("cobrosMasCancelados "+nombre_de_la_empresa_registrada_en_base_de_datos, cobrosSumandoCanceladosRespuesta).apply();
                                    efectivoEditor.putFloat("cobrosMenosCanceladosMenosDepositos "+nombre_de_la_empresa_registrada_en_base_de_datos, totalCobrosMenosDepositosRespuesta).apply();
                                    efectivoEditor.putFloat("cobrosMenosDepositos "+nombre_de_la_empresa_registrada_en_base_de_datos, cobrosMenosDepositosRespuesta).apply();*/
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.i("Error -->", "requestBalanceEmpresaPorCobradorVolley: "+ e.toString());
                                }
                            }

                        } catch (NullPointerException e) {
                            e.printStackTrace();

                        }
                    }
                } catch (JSONException e) {
                    //nothing here...
                    Log.i("Error -->", "requestBalanceEmpresaPorCobradorVolley: "+ e.toString());
                }

                Snackbar.make(mainActivity.mainLayout, "Efectivo sincronizado", Snackbar.LENGTH_SHORT).show();
                try {
                    putInfo();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                Log.i("Request balance-->", new Gson().toJson(response));
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("balanceEmpresas", "Error al obtener balance por empresas");
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }

    /**
     * private void requestBalanceEmpresaPorCobradorVolley(JSONObject jsonParams)
     *     {
     *         JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlGetBalanceEmpresaPorCobradorYPeriodo, jsonParams, new Response.Listener<JSONObject>() {
     *             @Override
     *             public void onResponse(JSONObject response)
     *             {
     *                 manage_RegisterPagosOffiline(response);
     *
     *                 //NEW PROCESS
     *                 JSONArray empresas;
     *                 JSONObject jsonResult;
     *                 try {
     *
     *                     //jsonResult = new JSONObject(result);
     *
     *                     for(Iterator<String> iter = response.keys(); iter.hasNext(); ) {
     *                         String key = iter.next();
     *                         empresas = response.getJSONArray(key);
     *                         Util.Log.ih("empresas = " + empresas);
     *
     *                         SharedPreferences.Editor efectivoEditor = mainActivity.getEfectivoEditor();
     *
     *                         try {
     *                             for (int i = 0; i < empresas.length(); i++) {
     *                                 JSONObject empresa;
     *                                 try {
     *                                     empresa = empresas.getJSONObject(i);
     *
     *                                     String nombre = DatabaseAssistant.getSingleCompany(empresa.getString("empresa")).getName();
     *
     *                                     float cantidad = (float) empresa.getDouble("cobros");
     *                                     cantidad = cantidad - (float) empresa.getDouble("depositos");
     *                                     efectivoEditor.putFloat(nombre, cantidad).apply();
     *                                 } catch (JSONException e) {
     *                                     e.printStackTrace();
     *                                     Log.i("Error -->", "requestBalanceEmpresaPorCobradorVolley: "+ e.toString());
     *                                 }
     *                             }
     *
     *                         } catch (NullPointerException e) {
     *                             e.printStackTrace();
     *
     *                         }
     *                     }
     *                 } catch (JSONException e) {
     *                     //nothing here...
     *                     Log.i("Error -->", "requestBalanceEmpresaPorCobradorVolley: "+ e.toString());
     *                 }
     *
     *                 Snackbar.make(mainActivity.mainLayout, "Efectivo sincronizado", Snackbar.LENGTH_SHORT).show();
     *                 try {
     *                     putInfo();
     *                 } catch (JSONException e) {
     *                     e.printStackTrace();
     *                 }
     *
     *
     *                 Log.i("Request LOGIN-->", new Gson().toJson(response));
     *             }
     *         },
     *                 new Response.ErrorListener() {
     *                     @Override
     *                     public void onErrorResponse(VolleyError error) {
     *                         error.printStackTrace();
     *                         Log.e("balanceEmpresas", "Error al obtener balance por empresas");
     *                     }
     *                 }) {
     *
     *         };
     *         Log.d("POST REQUEST-->", postRequest.toString());
     *         com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
     *     }
     */



    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (timerNotificaciones != null) {
            timerNotificaciones.cancel();
            timerNotificaciones = null;
        }
        if (timerSendPaymentsEachTenMinutes != null){
            //Util.Log.ih("onDestroy() -> timer = " + timerSendPaymentsEachTenMinutes);
            timerSendPaymentsEachTenMinutes.cancel();
            timerSendPaymentsEachTenMinutes = null;
            //Util.Log.ih("onDestroy()_ -> timer = " + timerSendPaymentsEachTenMinutes);
        }
        wl.release();

        /**
         * Broadcast
         * This broadcast gets call when time or timezone changes
         */

        //registerReceiver(m_timeChangedReceiver, s_intentFilter);

        //unregisterReceiver(m_timeChangedReceiver);
        Preferences.savePreferenceBoolean(mainActivity, false, Preferences.KEY_CARTERA_ACTUALIZADA);
        mthis = null;

    }

    @Override
    public void onResume()
    {
        super.onResume();

        MainActivity.CURRENT_TAG = MainActivity.TAG_CIERRE_INFORMATIVO;

        if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION) && Nammu.checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION))
            ApplicationResourcesProvider.startLocationUpdates();

        Deposito.depositosIsActive = false;
        //enviarCancelado();
        //hasActiveInternetConnection(this);
        mthis = this;
        calcularTotal();
        try {
            putInfo();
        } catch (JSONException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        final LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        final boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);


        mainActivity.getEfectivoEditor().putInt("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0)).commit();

        /**
         * when the activity calls onResume, checks if GPS is enabled, if not, shows a dialog
         * telling to enable it until it's enabled
         */
        if (!isGPSEnabled) {
            final Builder dialog = getBuilder();
            try {
                dialog.setTitle("Advertencia:");
                dialog.setMessage("Enciende el GPS para continuar.");
                dialog.setPositiveButton("Aceptar", (dialog1, which) ->
                {
                    boolean isGPSEnabled2 = locationManager
                            .isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (!isGPSEnabled2) {
                        dialog.create().show();
                    }
                });
                dialog.create().show();

            } catch (Exception e) {
                Toast toast = Toast.makeText(getContext(), "Advertencia: Enciende el GPS para continuar.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                e.printStackTrace();
            }
        }

        // enviando los notificaciones leidas
        try {
            String viewedNotifications = DatabaseAssistant.getViewedNotifications();
            if (viewedNotifications.length() > 0) {
                if (mainActivity.isConnected) {
                    registarNotificacionesLeidas();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        checkPendingTicket();
        checkPendingPeriod();
        checkForUpdate();
    }

    @Override
    public void onHiddenChanged(boolean hidden)
    {
        super.onHiddenChanged(hidden);
        //Toast.makeText(mainActivity, "onHidden: " + hidden, Toast.LENGTH_SHORT).show();

        if (!hidden)
        {
            MainActivity.CURRENT_TAG = MainActivity.TAG_CLIENTES;

            if (etFiltrado.length() > 0)
            {
                filteredClients = DatabaseAssistant.getFilteredClients(etFiltrado.getText().toString());
                adapter = getClientesAdapterFilteredClients();
                lvClientes.setAdapter(adapter);
            }
            try {
                putInfo();
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    //endregion

    //region Methods

    //region Public

    private void checkForUpdate()
    {
        JSONObject data = new JSONObject();

        try {

            data.put("no_cobrador", datosCobrador.getString("no_cobrador"));

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlCheckUpdate, data, response -> {
                if (!response.has("error")) {
                    try {
                        int checkUpdate = response.getInt("actualizar");

                        Log.d("UpdateCheck", "Debe actualizar: " + (checkUpdate == 1 ? "Si" : "No"));

                        if (checkUpdate == 1 && !updatePostponed)
                            checkVersion();
                        else if (checkUpdate == 2)
                            forceUpdate();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, error -> {
                if (timerUpdate != null)
                    timerUpdate.cancel();

                timerUpdate = new Timer();

                timerUpdate.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        updatePostponed = false;
                    }
                }, (1000 * 60) * 60);

                updatePostponed = true;

                error.printStackTrace();
            });

            request.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(mainActivity.getApplicationContext()).addToRequestQueue(request);
        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    private void checkVersion()
    {
        StringRequest request = new StringRequest(Request.Method.GET, ConstantsPabs.urlCheckVersion, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                try {
                    PackageInfo info = mainActivity.getPackageManager().getPackageInfo(mainActivity.getPackageName(), 0);
                    String version = info.versionName;

                    int comparison = version.compareTo(response);

                    Log.d("UpdateComparison", "Comparison: " + comparison);

                    if (!version.equals(response) && comparison < 0)
                    {
                        if(getActivity() != null)
                        {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                            builder.setTitle("Actualización");
                            builder.setMessage("Hay una nueva versión de la aplicación lista para descargar e instalar. \n\nVersión actual: " + version + "\n\nVersión nueva: " + response);
                            builder.setCancelable(false);
                            builder.setPositiveButton("Actualizar", (dialog1, which) -> {
                                String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
                                String fileName = "pabs.apk";
                                final Uri uri = Uri.parse("file://" + destination + fileName);

                                showMyCustomDialog();

                                LogRegister.registrarActualizacion("Aceptada");

                                File file = new File(destination, fileName);

                                if (file.exists())
                                    file.delete();

                                DownloadManager.Request downloadRequest = new DownloadManager.Request(Uri.parse(ConstantsPabs.urlUpdateApp + response + ".apk"));
                                //downloadRequest.setDescription("Descargando PABS");
                                downloadRequest.setTitle("Actualización PABS");

                                downloadRequest.setDestinationUri(uri);
                                downloadRequest.allowScanningByMediaScanner();
                                downloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                                final DownloadManager manager = (DownloadManager) mainActivity.getSystemService(Context.DOWNLOAD_SERVICE);
                                if (manager != null) {
                                    final long downloadId = manager.enqueue(downloadRequest);

                                    BroadcastReceiver onComplete = new BroadcastReceiver() {
                                        @Override
                                        public void onReceive(Context context, Intent intent) {
                                            if (file.exists()) {
                                                Uri apkUri = Uri.fromFile(file);
                                                Intent update = new Intent();
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                    apkUri = FileProvider.getUriForFile(getActivity(), com.jaguarlabs.pabs.BuildConfig.APPLICATION_ID + ".provider", file);
                                                    update.setAction(Intent.ACTION_INSTALL_PACKAGE);
                                                    update.setDataAndType(apkUri, "application/vnd.android.package-archive");
                                                    update.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                                    startActivity(update);
                                                } else {
                                                    update.setAction(Intent.ACTION_VIEW);
                                                    update.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    update.setDataAndType(apkUri, "application/vnd.android.package-archive");
                                                    startActivity(update);
                                                }

                                                Log.i("ApkUpdate", manager.getMimeTypeForDownloadedFile(downloadId));

                                                mainActivity.unregisterReceiver(this);
                                                mainActivity.finish();
                                            } else {
                                                dismissMyCustomDialog();
                                                timerUpdate = new Timer();

                                                manager.remove(downloadId);

                                                timerUpdate.schedule(new TimerTask() {
                                                    @Override
                                                    public void run() {
                                                        updatePostponed = false;
                                                    }
                                                }, (1000 * 60) * 60);
                                                updatePostponed = true;

                                                mainActivity.toastL("No se encontro el archivo de actualización");
                                            }
                                        }
                                    };

                                    mainActivity.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                                }
                            });
                            builder.setNegativeButton("Posponer 1 hora", (dialog1, which) -> {
                                timerUpdate = new Timer();

                                timerUpdate.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        updatePostponed = false;
                                        Log.i("UpdatePostponed", "false");
                                    }
                                }, (1000 * 60) * 60);

                                updatePostponed = true;

                                Log.i("UpdatePostponed", "true");
                                LogRegister.registrarActualizacion("Pospuesta");
                            });

                            builder.show();
                        }
                    }
                } catch (PackageManager.NameNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        }, error -> {
            error.printStackTrace();
        });
        request.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(mainActivity.getApplicationContext()).addToRequestQueue(request);
    }

    private void forceUpdate()
    {
        StringRequest request = new StringRequest(Request.Method.GET, ConstantsPabs.urlCheckVersion, response -> {
            String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
            String fileName = "pabs.apk";
            final Uri uri = Uri.parse("file://" + destination + fileName);

            showMyCustomDialog();

            File file = new File(destination, fileName);

            if (file.exists())
                file.delete();

            DownloadManager.Request downloadRequest = new DownloadManager.Request(Uri.parse(ConstantsPabs.urlUpdateApp + response + ".apk"));
            //downloadRequest.setDescription("Descargando PABS");
            downloadRequest.setTitle("Actualización PABS");

            downloadRequest.setDestinationUri(uri);
            downloadRequest.allowScanningByMediaScanner();
            downloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

            final DownloadManager manager = (DownloadManager) mainActivity.getSystemService(Context.DOWNLOAD_SERVICE);
            if (manager != null) {
                final long downloadId = manager.enqueue(downloadRequest);

                BroadcastReceiver onComplete = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (file.exists()) {
                            Uri apkUri = Uri.fromFile(file);
                            Intent update = new Intent();
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                apkUri = FileProvider.getUriForFile(getActivity(), com.jaguarlabs.pabs.BuildConfig.APPLICATION_ID + ".provider", file);
                                update.setAction(Intent.ACTION_INSTALL_PACKAGE);
                                update.setDataAndType(apkUri, "application/vnd.android.package-archive");
                                update.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                startActivity(update);
                            } else {
                                update.setAction(Intent.ACTION_VIEW);
                                update.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                update.setDataAndType(apkUri, "application/vnd.android.package-archive");
                                startActivity(update);
                            }

                            Log.i("ApkUpdate", manager.getMimeTypeForDownloadedFile(downloadId));

                            mainActivity.unregisterReceiver(this);
                            mainActivity.finish();
                        } else {
                            dismissMyCustomDialog();
                            timerUpdate = new Timer();

                            manager.remove(downloadId);

                            timerUpdate.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    updatePostponed = false;
                                }
                            }, (1000 * 60) * 60);
                            updatePostponed = true;

                            mainActivity.toastL("No se encontro el archivo de actualización");
                        }
                    }
                };

                mainActivity.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
            }
        }, error -> {

        });
        request.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(mainActivity.getApplicationContext()).addToRequestQueue(request);
    }

    /**
     * Sets the number of notifications on the notifications counter
     * and updates number of notification on the SharedPreferences file.
     * if it's connected to internet, will call registarNotificacionesLeidas()
     * to try to register read notification to server.
     */
    public void actualizarNotificaciones() {
        TextView tvICon = (TextView) getView().findViewById(R.id.tv_icon_notificaciones);
        int notificacionesActuales = Integer.parseInt(tvICon.getText()
                .toString());
        notificacionesActuales = notificacionesActuales - 1;
        tvICon.setText("" + notificacionesActuales);
        Editor editor = getContext().getSharedPreferences("PABS_Main",
                Context.MODE_PRIVATE).edit();
        editor.putString("numero_notificaciones", "" + notificacionesActuales);
        editor.apply();
        if (mainActivity.isConnected) {
            registarNotificacionesLeidas();
        }
    }

    /**
     * update collector's cash
     * @param efectivo
     */
    public void agregarEfectivoDatosCobrador(String efectivo) {
        if (efectivo.length() > 0) {
            try {
                datosCobrador.put("efectivo", efectivo);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        modificarInfoToLoginAndSplash();
    }

    /**
     * start inactivity timer
     * timer which ends session after 20 minutes
     */
    public void llamarTimerInactividad() {
        //iniciarHandlerInactividad();
    }

    /**
     * sends to server all payments made that are not sync yet
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controlpagos/registerPagos'
     * JSONObject param
     * {
     *     "no_cobrador": "",
     *     "pagos": {}
     *     "periodo":
     * }
     */
    public void registerPagos() {

        Util.Log.ih("register pagos");
        JSONObject json = new JSONObject();

        try {
            //if (arrayCobrosOffline == null) {
            //	arrayCobrosOffline = new SqlQueryAssistant(ApplicationResourcesProvider.getContext())
            //			.obtenerCobrosDesincronizados().getJSONArray("cobros");
            //}
            try {
                TelephonyManager manager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
                String deviceid = null;
                if (Nammu.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        deviceid = manager.getImei();
                    else
                        deviceid = manager.getDeviceId();
                }
                json.put("imei", deviceid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //if (offlinePayments == null){
                offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();
            //}
            json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
            //json.put("pagos", arrayCobrosOffline);
            json.put("pagos", new JSONArray( new Gson().toJson( offlinePayments ) ));
            json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));

            Log.e("cobros", new Gson().toJson(offlinePayments));
        } catch (Exception e) {
            e.printStackTrace();
        }

		requestRegiterPagosVolley(json);

        /*NetService service = new NetService(ConstantsPabs.urlregisterPagosOfflineMain, json);
        NetHelper helper = NetHelper.getInstance();
        helper.ServiceExecuter(service, getContext(), new onWorkCompleteListener()
        {

            @Override
            public void onCompletion(String result) {
                try {
                    manage_RegisterPagosOffiline(new JSONObject(result));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Exception e) {
                Log.e("regPagosOffline", "Error al registrar pagos offline");
            }
        });*/
    }

    private void requestRegiterPagosVolley(JSONObject jsonParams) //Azael--
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlregisterPagosOfflineMain, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                manage_RegisterPagosOffiline(response);
                Log.i("REGISTER_PAGOS -->", new Gson().toJson(response));
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("regPagosOffline", "Error al registrar pagos offline");
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }

    public void registerPagostxt() {

        Util.Log.ih("register pagos");
        JSONObject json = new JSONObject();

        try {

            try {
                TelephonyManager manager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
                String deviceid = null;
                if (Nammu.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        deviceid = manager.getImei();
                    else
                        deviceid = manager.getDeviceId();
                }
                json.put("imei", deviceid);
            } catch (Exception e) {
                e.printStackTrace();
            }

            offlinePayments = LogRegister.getPaymentsFromTxt(datosCobrador.getString("no_cobrador"));
            json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
            json.put("pagos", new JSONArray( new Gson().toJson( offlinePayments ) ));
            json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));

            Log.e("cobros", offlinePayments.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        requestRegisterPagosOfflineVolley(json);

        /*NetService service = new NetService(
                ConstantsPabs.urlregisterPagosOfflineMain, json);
        NetHelper helper = NetHelper.getInstance();
        helper.ServiceExecuter(service, getContext(), new onWorkCompleteListener() {

            @Override
            public void onCompletion(String result) {
                try {
                    manage_RegisterPagosOffiline(new JSONObject(result));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Exception e) {
                Log.e("regPagosOffline", "Error al registrar pagos offline");
            }
        });*/
    }

    private void requestRegisterPagosOfflineVolley(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlregisterPagosOfflineMain, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                manage_RegisterPagosOffiline(response);
                Log.i("Request LOGIN-->", new Gson().toJson(response));
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("regPagosOffline", "Error al registrar pagos offline");
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }

    /**
     * finish refresh button animation
     * @param refreshButon
     */
    public void finishRefreshButtonAnimation(ImageView refreshButon){
        refreshButon.clearAnimation();
    }

    /**
     * Set number of notifications
     * @param notificaciones
     */
    public void setNumeroNotificaciones(int notificaciones) {
        TextView tvICon = (TextView) getView().findViewById(R.id.tv_icon_notificaciones);
        tvICon.setText(String.format(Locale.getDefault(), "%d", notificaciones));
    }

    //endregion

    //region Private

    public void executeSendOsticket(){
        try {
            Util.Log.ih("trying to send tickets...");

            ModelOsticketRequest modelOsticketRequest = new ModelOsticketRequest();
           /* if (bandera)
            {
                //Preferencias
                SharedPreferences preferences = ApplicationResourcesProvider.getContext().getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
                userLoader = preferences.getString("user_login", "");
                modelOsticketRequest.setCollectorNumber(Integer.parseInt(userLoader));
            }else*/
            modelOsticketRequest.setCollectorNumber( Integer.parseInt( datosCobrador.getString("no_cobrador") ) );

            modelOsticketRequest.setOsticketTickets( DatabaseAssistant.getAllOsticketsPendingToSend() );

            if ( modelOsticketRequest.getOsticketTickets().size() > 0 ) {
                OsticketRequest osticketRequest = new OsticketRequest(modelOsticketRequest);
                spiceSendOsticket.executeRequest(osticketRequest);
            } else {
                Util.Log.ih("No hay tickets para enviar a osticket");
            }
        } catch (JSONException e){
            Util.Log.eh("Error: No se pudo obtener el numero de cobrador");
        } catch (NullPointerException e){
            e.printStackTrace();
            Util.Log.eh("Error: No se enviaron los tickets");
        }

    }

    private void executeSendCellPhoneNumber(){
        try {
            Util.Log.ih("trying to send cell phone numbers...");

            ModelCellPhoneNumbersRequest modelCellPhoneNumbersRequest = new ModelCellPhoneNumbersRequest();
            modelCellPhoneNumbersRequest.setCollectorNumber( Integer.parseInt( datosCobrador.getString("no_cobrador") ) );
            modelCellPhoneNumbersRequest.setCellPhoneNumbers( DatabaseAssistant.getAllCellPhoneNumbersPendingToSend() );

            if ( modelCellPhoneNumbersRequest.getCellPhoneNumbers().size() > 0 ) {
                CellPhoneNumberRequest cellPhoneNumberRequest = new CellPhoneNumberRequest(modelCellPhoneNumbersRequest);
                spiceSendContractsCellPhoneNumbers.executeRequest(cellPhoneNumberRequest);
            } else {
                Util.Log.ih("No hay numeros de telefonos celulares para enviar");
            }
        } catch (JSONException e){
            Util.Log.eh("Error: No se pudo obtener el numero de cobrador");
        } catch (NullPointerException e){
            e.printStackTrace();
            Util.Log.eh("Error: No se enviaron los numeros de telefonos celulares");
        }
    }

    private void updateClientList(int position, boolean isVisit){
        SyncedWallet sw = clientListFromSyncedWallet.get(position);

        if (isVisit){
            if ( sw.isFPAPending() || (!sw.isFPA() && !sw.isFPAPending()) && !sw.getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT) ) {
                sw.setVisitCounter(sw.getVisitCounter() + 1);
                if (sw.getVisitCounter() >= 2) {

                    Util.Log.ih("aquiaquiaquiaquiaquiaquiaquiaquiaquiaquiaquiaquiaqui");

                    sw.setShowColor(false);
                    sw.setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);

                    clientListFromSyncedWallet.remove(position);

                    for (int i = clientListFromSyncedWallet.size() - 1; i >= 0; i--) {
                        if (!clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT) &&
                                !clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT)) {
                            clientListFromSyncedWallet.add(i + 1, sw);
                            break;
                        }
                    }
                } else {
                    if (sw.getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT)) {
                        //sw.setShowColor(false);
                        //if (sw.isFPAPending() || (!sw.isFPA() && !sw.isFPAPending())) {
                        sw.setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                        //sw.setVisitCounter(sw.getVisitCounter() + 1);
                        sw.setShowColor(true);
                        clientListFromSyncedWallet.remove(position);
                        for (int i = clientListFromSyncedWallet.size() - 1; i >= 0; i--) {
                            if (clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_TODAY_CONTRACT) ||
                                    clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_PENDING_CONTRACT) ||
                                    clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_PENDING_LEVEL_2_CONTRACT)) {
                                clientListFromSyncedWallet.add(i + 1, sw);
                                break;
                            } else if (i == 0) {
                                clientListFromSyncedWallet.add(i, sw);
                                break;
                            }
                        }
                        //} else {
                        //sw.setVisitCounter(sw.getVisitCounter() - 1);
                        //}
                        //} else if (sw.getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT)) {
                        //sw.setVisitCounter(sw.getVisitCounter() - 1);
                        //}
                    }
                }
            }
        }
        else {
            sw.setShowColor(false);
            sw.setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);
            sw.setFPA(false);
            sw.setFPAPending(false);

            clientListFromSyncedWallet.remove(position);

            for (int i = clientListFromSyncedWallet.size() - 1; i >= 0; i--) {
                if ( !clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT) &&
                        !clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT) ) {
                    clientListFromSyncedWallet.add(i + 1, sw);
                    break;
                }
            }
        }
    }

    private void updateListView(Boolean... reloadClientsFromSyncedWallet){
        //reload clients if needed
        if (reloadClientsFromSyncedWallet.length > 0){
            new Thread(() -> clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet()).start();
            //clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
        }

        mainActivity.runOnUiThread(() ->{
            adapter = getAdapterSyncedWalletClients();
            //lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);
            if (lvClientes != null) {
                lvClientes.setAdapter(adapter);
                if (listViewState != null) {
                    Util.Log.ih("trying to restore ListView state...");
                    //lvClientes.onRestoreInstanceState(listViewState);
                }
                /*lvClientes.setDivider(getResources().getDrawable(R.drawable.divider_list_clientes));
                lvClientes.setOnItemClickListener(Clientes.this);*/
            }
        });


    }

    @Override
    public void onUserInteraction() {
            airPlaneCheck();
    }

    /**
     * CustomComparator class which is used to compare two objects to determine their
     * ordering with respect to each other.
     */
    private class CustomComparatorToSortByDistanceFromSyncedWallet implements Comparator<ModelClient>{

        /**
         * Compares the two specified objects to determine their relative ordering.
         * The ordering implied by the return value of this method for all possible
         * pairs of (lhs, rhs) should form an equivalence relation.
         * @param lhs an Object
         * @param rhs an Object to compare with lhs
         * @return an integer < 0 if lhs is less than rhs, 0 if they are equal, and > 0 if lhs is greater than rhs.
         */
        @Override
        public int compare(ModelClient lhs, ModelClient rhs) {
            Double firstDistance = 0.0;
            firstDistance = Double.parseDouble( lhs.getDistance() );

            Double secondDistance = 0.0;
            secondDistance = Double.parseDouble( rhs.getDistance() );

            int comparisonResult = -1;
            if (firstDistance > secondDistance) {
                comparisonResult = 1;
            } else if (firstDistance.equals(secondDistance)) {
                comparisonResult = 0;
            }
            return comparisonResult;
        }
    }

    /**
     * CustomComparator class which is used to compare two objects to determine their
     * ordering with respect to each other.
     */
    private class CustomComparatorToSortByDistance implements Comparator<Clients>{

        /**
         * Compares the two specified objects to determine their relative ordering.
         * The ordering implied by the return value of this method for all possible
         * pairs of (lhs, rhs) should form an equivalence relation.
         * @param lhs an Object
         * @param rhs an Object to compare with lhs
         * @return an integer < 0 if lhs is less than rhs, 0 if they are equal, and > 0 if lhs is greater than rhs.
         */
        @Override
        public int compare(Clients lhs, Clients rhs) {
            Double firstDistance = 0.0;
            firstDistance = Double.parseDouble( lhs.getDistance() );

            Double secondDistance = 0.0;
            secondDistance = Double.parseDouble( rhs.getDistance() );

            int comparisonResult = -1;
            if (firstDistance > secondDistance) {
                comparisonResult = 1;
            }
            return comparisonResult;
        }
    }

    /**
     * Sets all deposits stored in arrayCobrosOffline variable which were just sent
     * to server as sync (sync <- 1) so that does not resend payments made
     * causing a lot of data usage.
     */
    private void sincronizarPagosRegistrados(JSONObject json) {
        try {
            //for (ModelPayment p: offlinePayments){
                DatabaseAssistant.updatePaymentsAsSynced(json);
            //}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets all deposits that were just sent
     * to server as sync (sync <- 1) so that does not resend deposits made
     * causing data usage.
     */
    private void syncDepositsSavedToServer(JSONArray almacenados) {
        try {
            for (int i = 0; i < almacenados.length(); i++) {
                JSONObject deposito = almacenados.getJSONObject(i);
                DatabaseAssistant.updateDepositsAsSynced(deposito.getString("fecha"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method updates ListView of accounts (accounts within wallet)
     * getting new info from database
     */
    private void actualizarListView() {
        //runOnUiThread(new Runnable() {

        //@Override
        //public void run() {



        if (BuildConfig.isWalletSynchronizationEnabled()) {

            clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
            clientListFromSyncedWalletAll = DatabaseAssistant.getAllClientsFromSyncedWallet();
            //clientsList = DatabaseAssistant.getAllClients();
            //clientListFromSyncedWalletWithoutAnalysis = DatabaseAssistant.getClientsFromSyncedWalletWithoutAnalysis();
        } else {
            clientsList = DatabaseAssistant.getAllClients();
        }



				/*
				if (BuildConfig.isWalletSynchronizationEnabled()) {
					//List<SyncedWallet> syncedClientsFromWallet = DatabaseAssistant.getClientsFromSyncedWallet();
					Util.Log.ih("size: " + clientListFromSyncedWallet.size());
					for (int i = 0; i < clientListFromSyncedWallet.size(); i++) {
						Util.Log.ih("wallet client: " + clientListFromSyncedWallet.get(i).getClient().getName());
					}
				}
				*/
        //for (int i = 0; i < clientListFromSyncedWalletWithoutAnalysis.size(); i++) {
        //	Util.Log.ih("wallet client W/A: " + clientListFromSyncedWalletWithoutAnalysis.get(i).getClient().getName());
        //}
        //clientListFromSyncedWalletForAdapter = clientListFromSyncedWallet;
        //clientListFromSyncedWalletForAdapter.add(clientListFromSyncedWalletWithoutAnalysis.get(0));

        //Util.Log.ih("clientListFromSyncedWallet = " + clientListFromSyncedWallet.size());
        //Util.Log.ih("clientListFromSyncedWalletForAdapter = " + clientListFromSyncedWalletForAdapter.size());

        //ClientesAdapter adapter;

        mainActivity.runOnUiThread(() -> {
            if (BuildConfig.isWalletSynchronizationEnabled()) {
                //TODO: update ClientesAdapter
                adapter = getAdapterSyncedWalletClients();
            } else {
                adapter = getClientesAdapterClientsList();
            }
            //lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);

            if (lvClientes != null) {
                lvClientes.setAdapter(adapter);
                /*lvClientes.setDivider(getResources().getDrawable(
                        R.drawable.divider_list_clientes));
                lvClientes.setOnItemClickListener(mthis);*/
            }

            dismissMyCustomDialog();

            if (BuildConfig.isWalletSynchronizationEnabled()) {
                if (clientListFromSyncedWallet.size() == 500) {
                    mainActivity.toastLReuse("El número total de clientes es bastante alta, se ha limitado la vista a 500, si un cliente asignado no se ve en la lista, puedes encontrarlo utilizando la barra de búsqueda.");
                } else {
                    //mainActivity.toastSReuse("Lista de clientes actualizada.");
                    Snackbar.make(mainActivity.mainLayout, "Lista de clientes actualizada", Snackbar.LENGTH_SHORT).show();
                    Preferences.savePreferenceBoolean(mainActivity, true, Preferences.KEY_CARTERA_ACTUALIZADA);
                    lottieNF.setVisibility(View.INVISIBLE);
                    //derecha_izquierda = AnimationUtils.loadAnimation(getActivity(), R.anim.derecha_izquierda);
                    //tvError.setAnimation(derecha_izquierda);
                    tvError.setVisibility(View.GONE);
                    lottieNF.setVisibility(View.INVISIBLE);
                }
            } else {
                if (clientsList.size() == 500) {
                    mainActivity.toastLReuse("El número total de clientes es bastante alta, se ha limitado la vista a 500, si un cliente asignado no se ve en la lista, puedes encontrarlo utilizando la barra de búsqueda.");
                } else {
                    //mainActivity.toastSReuse("Lista de clientes actualizada.");
                    Snackbar.make(mainActivity.mainLayout, "Lista de clientes actualizada", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
		/*
				if (BuildConfig.isWalletSynchronizationEnabled()) {
					//TODO: update ClientesAdapter
					adapter = getAdapterSyncedWalletClients();
				} else {
					adapter = getClientesAdapterClientsList();
				}
				lvClientes = (ListView) findViewById(R.id.listView_clientes);
				lvClientes.setAdapter(adapter);
				lvClientes.setDivider(getResources().getDrawable(
						R.drawable.divider_list_clientes));
				lvClientes.setOnItemClickListener(mthis);

				dismissMyCustomDialog();

				if (BuildConfig.isWalletSynchronizationEnabled()) {
					if (clientListFromSyncedWallet.size() == 500) {
						toastLReuse("El número total de clientes es bastante alta, se ha limitado la vista a 500, si un cliente asignado no se ve en la lista, puedes encontrarlo utilizando la barra de búsqueda.");
					} else {
						toastSReuse("Lista de clientes actualizada.");
					}
				} else {
					if (clientsList.size() == 500) {
						toastLReuse("El número total de clientes es bastante alta, se ha limitado la vista a 500, si un cliente asignado no se ve en la lista, puedes encontrarlo utilizando la barra de búsqueda.");
					} else {
						toastSReuse("Lista de clientes actualizada.");
					}
				}

		*/
        //}
        //});

    }

    /**
     * Deletes all data from database recolected by app and closes session.
     */
    private void borrarDatos() {
        DatabaseAssistant.eraseDatabaseToBlock();
        cerrarSesionTimeOut();
    }

    /**
     * sets collector's cash
     */
    private void calcularTotal() {
		/*
		//SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
		try {
			SharedPreferences preferencesLogin = getSharedPreferences(
					"PABS_Login", Context.MODE_PRIVATE);
			double efetivoInicial = Double.parseDouble(preferencesLogin
					.getString("efectivo_inicial", "0.0"));

			//String saldoPrograma = formatMoney(query.getTotalPrograma());
			//String saldoMalba = formatMoney(query.getTotalMalba());

			//double totalDepositos = query.getTotalDepositos();
			double totalDepositos = DatabaseAssistant.getTotalDeposits();
			//double totalCancMalba = query.getTotalCanceladosMalba();
			//double totalCancPrograma = query.getTotalCanceladosPrograma();
			//JSONArray empresas = query.mostrarEmpresas();
			List<Companies> companies = DatabaseAssistant.getCompanies();
			//String efectivoEmpresas = new String();
			//String cancelados = new String();
			double cantidadempresas = 0;
			double cantidadcancelados = 0;
			for (int cont = 0; cont < companies.size(); cont++) {
				//JSONObject empresa = empresas.getJSONObject(cont);
				Companies company = companies.get(cont);
				//cantidadempresas += query.getTotalEmpresa(empresa
				//		.getString(Empresa.id_empresa));
				cantidadempresas += DatabaseAssistant.getTotalByCompany(company.getIdCompany());
				//cantidadcancelados += query.getTotalCanceladosEmpresa(empresa
				//		.getString(Empresa.id_empresa));
				cantidadcancelados += DatabaseAssistant.getTotalCanceledByCompany(company.getIdCompany());

                /*
				efectivoEmpresas += "+"
						+ empresa.getString(Empresa.nombre_empresa)
						+ ": $"
						+ formatMoney(query.getTotalEmpresa(empresa
								.getString(Empresa.id_empresa))) + "\n";
				cancelados += "-Cancelados "
						+ empresa.getString(Empresa.nombre_empresa).charAt(0)
						+ " : $"
						+ query.getTotalCanceladosEmpresa(empresa
								.getString(Empresa.id_empresa)) + "\n";
				*/
			/*}
			double totalMomento = efetivoInicial + cantidadempresas
					- cantidadcancelados - totalDepositos;
			datosCobrador.put("efectivo", totalMomento);

			//query = null;

		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
    }


    /**
     * Closes session by cancelling all timers (to make sure nothing is running
     * in the background) and finishing current activity
     * going back to login screen.
     */
    private void cerrarSesion() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (timerNotificaciones != null) {
            timerNotificaciones.cancel();
            timerNotificaciones = null;
        }
        if (timerSendPaymentsEachTenMinutes != null){

            timerSendPaymentsEachTenMinutes.cancel();
            timerSendPaymentsEachTenMinutes = null;
        }

        modificarInfoToLoginAndSplash();

        View refLogin = null;
        if (Login.getMthis() != null) {
            refLogin = Login.getMthis().findViewById(R.id.editTextUser);
        }

        if (refLogin != null) {
            Login.getMthis().finish();
        }

        Intent intent = new Intent(getContext(), Login.class);
        if (datosCobrador != null) {
            intent.putExtra("datos_cobrador", datosCobrador.toString());
        }
        if (robado.equals("1")) {
            intent.putExtra("robado", true);
        }
        startActivity(intent);

        Preferences.savePreferenceBoolean(mainActivity, false, Preferences.KEY_CARTERA_ACTUALIZADA);
        Util.Log.ih("SESION TERMINADA CORRECTAMENTE");

        mthis = null;
        mainActivity.finish();
    }

    /**
     * Blocks app.
     * This mehtod gets called when cash is greater than permitted,
     * preventing the app from letting the collector collect more than his limit.
     *
     * finishes all activities loaded by reference 'mthis' on each activity if they weren't finished yet
     * and cancels all timers (to make sure nothing is running in the background) and finishing current activity
     * going back to login screen.
     */
    private void cerrarTodoYMandarBloqueo() {
        Intent intent = new Intent(getContext(), Bloqueo.class);
        calcularTotal();
        intent.putExtra("datos_cobrador", datosCobrador.toString());
        intent.putExtra("iniciar_time_sesion", true);
        intent.putExtra("isConnected", true);
        startActivity(intent);
        if (mainActivity.handlerSesion != null) {
            mainActivity.handlerSesion.removeCallbacks(mainActivity.myRunnable);
            mainActivity.handlerSesion = null;
        }

        View refBuscarClientes = null;
        if (BuscarCliente.getMthis() != null) {
            refBuscarClientes = BuscarCliente.getMthis().findViewById(
                    R.id.lv_buscar_clientes);
        }

        View refAportacion = null;
        if (Aportacion.getMthis() != null) {
            refAportacion = Aportacion.getMthis().getView().findViewById(
                    R.id.et_nombre_apellidos);
        }

        View refCierre = null;
        if (Cierre.getMthis() != null) {
            refCierre = Cierre.getMthis().getView().findViewById(R.id.lv_cierre);
        }

        View refDeposito = null;
        if (Deposito.getMthis() != null) {
            refDeposito = Deposito.getMthis().getView().findViewById(R.id.tv_banco);
        }

        View refDetalleCliente = null;
        if (ClienteDetalle.getMthis() != null) {
            refDetalleCliente = ClienteDetalle.getMthis().getView().findViewById(
                    R.id.et_cantidad_aportacion);
        }

        View refNotificaciones = null;
        if (Notificaciones.getMthis() != null) {
            refNotificaciones = Notificaciones.getMthis().findViewById(
                    R.id.lv_notification);
        }

        if (refBuscarClientes != null) {
            BuscarCliente.getMthis().finish();
        } else if (refAportacion != null) {
            Aportacion.getMthis().getActivity().finish();
        }

        if (refCierre != null) {
            Cierre.getMthis().getActivity().finish();
        }

        if (refDeposito != null) {
            Deposito.getMthis().getActivity().finish();
        }

        if (refDetalleCliente != null) {
            ClienteDetalle.getMthis().getActivity().finish();
        }

        if (refNotificaciones != null) {
            Notificaciones.getMthis().finish();
        }

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (timerNotificaciones != null) {
            timerNotificaciones.cancel();
            timerNotificaciones = null;
        }
        if (timerSendPaymentsEachTenMinutes != null){
            timerSendPaymentsEachTenMinutes.cancel();
            timerSendPaymentsEachTenMinutes = null;
        }
        mthis = null;
        mainActivity.finish();
    }

    /*
	 * Este metodo enviar cobros offline cada 10 minutos
	 */
    private void enviarCobrosDiezMinutos()
    {
        timerSendPaymentsEachTenMinutes = new Timer();
        timerSendPaymentsEachTenMinutes.schedule(new TimerTask() {

            @Override
            public void run() {
                mainActivity.runOnUiThread(() -> {
                    Util.Log.ih("trying to send payments...");
                    generadorJSON();
                    if (!Deposito.depositosIsActive) {
                        ExtendedActivity.userInteractionTime = System.currentTimeMillis();
                        offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();

                        if (offlinePayments != null && offlinePayments.size() > 0)
                        {
                            mainActivity.sendPaymentsIfActiveInternetConnection();
                        } else {
                            Util.Log.ih("no hay cobros pendientes...");
                            enviarDepositos();
                        }
                    }
                    executeSendOsticket();
                    executeSendCellPhoneNumber();
                });
            }
        }, (1000 * 60), (1000 * 60) * 10);//}, (1000 * 60) * 10 , (1000 * 60) * 10 );

    }

    /**
     * Sends to server all deposits made using Web Service
     *
     * 'http://50.62.56.182/ecobro/controldepositos/registerDepositosOffline'
     * JSONObject param
     * {
     *     "no_cobrador": "";
     *     "depositos": {},
     *     "periodo": ,
     * }
     */
    private void enviarDepositos() {
        //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
        //if (query.hayDepositos()) {
        if (DatabaseAssistant.isThereAnyDepositNotSynced()) {

            List<LogRegister.LogDeposito> logDepositos = null;
            JSONObject json = new JSONObject();

            try {
                //JSONArray depositos = query.mostrarTodosLosDepositosDesincronizados().getJSONArray("DEPOSITO");
                List<ModelDeposit> deposits = DatabaseAssistant.getAllDepositsNotSynced();
                if (deposits != null) {

                    json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
                    json.put("depositos", new JSONArray( new Gson().toJson( deposits ) ));
                    json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
                    //Crear arraylist de depositos y registrarlos en log si el server responde
                    logDepositos = new ArrayList<>();
					/*
					for (int i = 0; i < depositos.length(); i++) {
						LogRegister.LogDeposito logDeposito;
						JSONObject jDeposito = depositos.getJSONObject(i);

						logDeposito = new LogRegister.LogDeposito(
								"" + getEfectivoPreference().getInt("periodo", 0),                                                //Periodo
								jDeposito.getString("monto"),                                    //Monto
								jDeposito.getString("empresa"), //Empresa
								getEfectivoPreference().getString("no_cobrador", ""),                                                                                    //Cobrador
								jDeposito.getString("no_banco"),                                                                    //Banco
								jDeposito.getString("referencia"),                                    //Referencia
								true                                                                                            //Ya fue enviado al server
						);
						logDepositos.add(logDeposito);
					}
					*/
                    for (ModelDeposit d: deposits){
                        LogRegister.LogDeposito logDeposito;

                        logDeposito = new LogRegister.LogDeposito(
                                "" + mainActivity.getEfectivoPreference().getInt("periodo", 0),//Periodo
                                d.getAmount(),//Monto
                                d.getCompany(), //Empresa
                                mainActivity.getEfectivoPreference().getString("no_cobrador", ""),                                                                                    //Cobrador
                                d.getNumberBank(),//Banco
                                d.getReference(),//Referencia
                                true//Ya fue enviado al server
                        );
                        logDepositos.add(logDeposito);
                    }
                    Util.Log.ih("depositos json = " + json.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            /*RequestResult requestResult = new RequestResult(getContext());

            requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {

                List<LogRegister.LogDeposito> logDepositos;

                @Override
                public void onRequestResult(NetworkResponse response) {
                    try
                    {
                        manageRegisterDepositosOffline(new JSONObject(VolleyTickle.parseResponse(response)), logDepositos);
                    }
                    catch (JSONException ex)
                    {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onRequestError(NetworkResponse error) {
                    Log.e("EnviarDepositos", "Error al enviar depositos");
                }

                private RequestResult.ResultListener setData(List<LogRegister.LogDeposito> logDepositos)
                {
                    this.logDepositos = logDepositos;
                    return this;
                }
            }.setData(logDepositos));

            requestResult.executeRequest(ConstantsPabs.urlRegisterDepositosOffline, Request.Method.POST, json);*/
            requestRegisterDepositosOffline(json);

            /*NetService service = new NetService(ConstantsPabs.urlRegisterDepositosOffline, json);
            NetHelper.getInstance().ServiceExecuter(service, getContext(),
                    new onWorkCompleteListener()
                    {

                        List<LogRegister.LogDeposito> logDepositos;

                        @Override
                        public void onCompletion(String result) {
                            try {
                                manageRegisterDepositosOffline(new JSONObject(result), logDepositos);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e("EnviarDepositos", "Error al enviar depositos");

                        }

                        public onWorkCompleteListener setData(List<LogRegister.LogDeposito> logDepositos){
                            this.logDepositos = logDepositos;
                            return this;
                        }
                    }.setData(logDepositos));*/

        }
        //query = null;
    }

    private void requestRegisterDepositosOffline(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlRegisterDepositosOffline, jsonParams, new Response.Listener<JSONObject>()
        {
            List<LogRegister.LogDeposito> logDepositos;
            @Override
            public void onResponse(JSONObject response)
            {
                manageRegisterDepositosOffline(response, logDepositos);
                Log.i("Request LOGIN-->", new Gson().toJson(response));
            }


        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }

    /**
     * Sends to server all locations saved 'till now
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controlubicaciones/saveUbicacion'
     *
     * @param params JSONObject with locations and cash of every company
     */
    private void enviarUbicacion(JSONObject params)
    {

        NetService service = new NetService(ConstantsPabs.urlSaveGeoJSONLocations, params);
        NetHelper.getInstance().ServiceExecuter(service, getContext(),
                new onWorkCompleteListener()
                {
                    @Override
                    public void onCompletion(String result) {
                        try {
                            manage_SaveUbicacion(new JSONObject(result));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("EnviarUbicacion", "Error al enviar ubicaciones");
                    }
                });
    }

    /**
     * Gets distance from current location to client location
     * @param myLocation current location
     * @param latitude client's  latitude
     * @param longitude client's longitude
     * @return distance to client from current location
     */
    private double getDistance(Location myLocation, double latitude, double longitude) {
        Location locationTmp = new Location("other");
        locationTmp.setLatitude(latitude);
        locationTmp.setLongitude(longitude);

        return myLocation.distanceTo(locationTmp);
    }

    private boolean goBack = true;

    /**
     * shows framelayout as progress dialog
     */
    private void showMyCustomDialog(){

        goBack = false;

        if (getView() != null) {
            final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
            flLoading.setVisibility(View.VISIBLE);
        }

        //final Animation qwe = AnimationUtils.loadAnimation(this, R.animator.rotate_around_center_point);
        //final ImageView ivLoadingIcon = (ImageView) ClienteDetalle.this.findViewById(R.id.ivLoading);
        //ivLoadingIcon.startAnimation(qwe);
    }

    /**
     * hides framelayout used as progress dialog
     */
    private void dismissMyCustomDialog(){

        goBack = true;

        if (getView() != null) {
            final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
            flLoading.setVisibility(View.GONE);
        }

        //final ImageView ivLoadingIcon = (ImageView) ClienteDetalle.this.findViewById(R.id.ivLoading);
        //ivLoadingIcon.clearAnimation();
    }

    /**
     * Gets list of clients assigned to collector using a
     * Web Serrvice 'http://50.62.56.182/ecobro/controlcartera/getListClient'
     *
     * JSONObject param
     * {
     *     "no_cobrador": "";
     * }
     */
    private void getListClients() {
        SharedPreferences preferencesSplash = getContext().getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
        String clientsCollectorNumber = preferencesSplash.getString("clientsCollectorNumber", null);

        JSONObject json = new JSONObject();
        try {
            String idCobrador = "";
            try {
                if ( clientsCollectorNumber == null ) {
                    Util.Log.ih("if ( !getIntent().getBooleanExtra(\"isSupervisor\", false) ) {");
                    idCobrador = datosCobrador.getString("no_cobrador");
                } else {
                    Util.Log.ih("} else {");
                    idCobrador = clientsCollectorNumber;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            json.put("no_cobrador", idCobrador);
        } catch (Exception e) {
            e.printStackTrace();
        }

        requestGetListClients(json);
    }


    private void requestGetListClients(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlListClient, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    manage_ListClients(response);
                    Log.i("Request CLIENTES-->", new Gson().toJson(response));
                }catch (Exception e)
                {
                    e.printStackTrace();
                    mainActivity.runOnUiThread(() -> dismissMyCustomDialog());
                    if(progress!=null)
                        progress.dismiss();
                }
            }
        },
            new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    error.printStackTrace();
                    mainActivity.runOnUiThread(() -> dismissMyCustomDialog());

                    if (BuildConfig.isWalletSynchronizationEnabled()){
                        if (clientListFromSyncedWallet == null) {
                            clientListFromSyncedWallet = new ArrayList<>();
                        }
                    }
                    else {
                        if (clientsList == null) {
                            clientsList = new ArrayList<>();
                        }
                    }

                    if (BuildConfig.isWalletSynchronizationEnabled()){
                        adapter = getAdapterSyncedWalletClients();
                    }
                    else {
                        adapter = getClientesAdapterClientsList();
                    }

                    lvClientes.setAdapter(adapter);

                    Toast toast = Toast.makeText(getContext(), "Hubo un problema de conexión con internet. Por favor inténtalo de nuevo.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    if(progress!=null)
                        progress.dismiss();

                    Log.i("Error ListClient", error.toString());
                }
            }) {};
        postRequest.setRetryPolicy(new DefaultRetryPolicy(90000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Gets number of new personal notifications
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controlnotificaciones/getNumNotification'
     * JSONObject param
     * {
     *     "no_cobrador": ""
     * }
     */
    private void getNumNotificaciones() {
        JSONObject json = new JSONObject();
        try {
            String idCobrador = "";
            try {
                idCobrador = datosCobrador.getString("no_cobrador");
            } catch (Exception e) {
                e.printStackTrace();
            }
            json.put("no_cobrador", idCobrador);
        } catch (Exception e) {
            e.printStackTrace();
        }

        requestGetNumNotificaciones(json);
    }

    private void requestGetNumNotificaciones(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlGetNumNotificaciones, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                manage_GetNumNotificaciones(response);
                Log.i("ROBADO CLIENTES-->", new Gson().toJson(response));
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("getNumNotificaciones", "Error al obtener numero de notificaciones");
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }

    /**
     * Checks if date-time changes
     *
     * @return true: the difference is more than 21 minutes
     * @return false: otherwise
     */
    private boolean checkDate() {
        newTime = new Date().getTime();
        long differences = Math.abs(mTime - newTime);
        return (differences > deviceDateTimeDifference);
    }

    /**
     * Loads user name of collector.
     * This is focused to Splunk Mint, in order to report error by user name
     *
     * @return collector's user name
     */
    private String loadUser() {
        return getContext().getSharedPreferences("PABS_Login", Context.MODE_PRIVATE).getString("user_login", "no hay datos");
    }

    /**
     * Gets and saves deposit into database
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controldepositos/depositosPeriodo'
     *
     * JSONObject param
     * {
     *     "periodo": ,
     *     "no_cobrador":
     * }
     */
    private void saveDepositos() {
        JSONObject params = new JSONObject();

        try {
            params.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
            params.put("no_cobrador",
                    mainActivity.getEfectivoPreference().getString("no_cobrador", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetService service = new NetService(ConstantsPabs.urlGetListaDepositosPorPeriodo, params);
        NetHelper helper = NetHelper.getInstance();
        helper.ServiceExecuter(service, getContext(), new onWorkCompleteListener() {

            @Override
            public void onError(Exception e) {
                // TODO Auto-generated method stub

            }

            // {"result":[{"no_deposito":"32","monto":"55","no_cobrador":"4438","fecha":"2013-06-26 11:14:42","no_banco":"5","status":"2","referencia":"xd","empresa":null},{"no_deposito":"31","monto":"9885","no_cobrador":"4438","fecha":"2013-06-26 10:56:45","no_banco":"4","status":"3","referencia":"fff","empresa":null}]}
            @Override
            public void onCompletion(String result) {
                try {
                    if (new JSONObject(result).has("error")) {
						/*
						SqlQueryAssistant query = new SqlQueryAssistant(
								ApplicationResourcesProvider.getContext());
						try {
							query.deleteDepositos();
						} catch (JSONException e1) {
							e1.printStackTrace();
						}*/

                        DatabaseAssistant.deleteAllDeposits();

                    } else {
                        JSONArray results = new JSONObject(result).getJSONArray("result");
                        DatabaseAssistant.deleteAllDeposits();
                        for (int q = 0; q < results.length(); q++) {
                            JSONObject deposito = results.getJSONObject(q);
                            if (deposito.getString("status").equals("1"))
                                DatabaseAssistant.insertDeposit(
                                        deposito.getString("monto"),
                                        deposito.getString("fecha"),
                                        deposito.getString("referencia"),
                                        deposito.getString("no_banco"),
                                        deposito.getString("status"),
                                        deposito.getString("empresa"),
                                        mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                );
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Manages response of Web Service
     *
     * if there's new notifications, will change number of
     * notifications on the notifications counter and
     * also will show them on the notifications bar and drawer
     *
     * @param json JSONObject with reponse of Web Service
     */
    private void manage_GetNumNotificaciones(JSONObject json) {
        Log.e("json notificatiosn", json.toString());
        if (json.has("result")) {
            try {
                Log.e("JSON", "Json: " + json.toString());
                robado = json.getString("robado");
                if (robado.equals("999")) {
                    borrarDatos();
                }
                else if(robado.equals("1"))
                {
                    try {
                        SharedPreferences preferences = mainActivity.getSharedPreferences("Login", Context.MODE_PRIVATE);
                        Editor editor = preferences.edit();
                        editor.putBoolean("robado", true);
                        editor.apply();
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    cerrarSesion();
                }
                else {

                    try {
                        String numNotifications = json.getString("result");
                        if (getView() != null) {
                            ((TextView) getView().findViewById(R.id.tv_icon_notificaciones)).setText(numNotifications);
                            Editor editor = getContext().getSharedPreferences("PABS_Main", Context.MODE_PRIVATE).edit();
                            editor.putString("numero_notificaciones", "" + numNotifications);
                            editor.apply();
                            int notificationsCounter = Integer.parseInt(numNotifications);

                            if (notificationsCounter > 0) {
                                notifications = notificationsCounter;
                                getNotificationAndShow(notificationsCounter);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * Shows notifications on notifications drawer
     * @param notificationMessage notifications message
     * @param notificationId notification id
     */
    private void showNotification(String notificationMessage, String notificationId)
    {
        long[] vibrate = {500, 500, 0, 0, 500, 500};
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String channelId = "pabsNotification";
        Intent notificationIntent = new Intent(mainActivity.getApplicationContext(), Notificaciones.class);
        try {
            notificationIntent.putExtra("id_cobrador", datosCobrador.getString("no_cobrador"));
        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);


        Vibrator v = (Vibrator) mainActivity.getSystemService(VIBRATOR_SERVICE);
        v.vibrate(2000);
        Intent intent = new Intent(mainActivity, Notificaciones.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //PendingIntent pe = PendingIntent.getActivity(MyService.this , 0, intent, PendingIntent.FLAG_ONE_SHOT);
        PendingIntent pe = PendingIntent.getActivity(mainActivity.getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri sonido = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        NotificationCompat.Builder noti= new NotificationCompat.Builder(mainActivity, channelId)
                .setSmallIcon(R.drawable.icono_hdpi)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(notificationMessage)
                .setVibrate(vibrate)
                .setSound(alarmSound)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationMessage))
                .setContentIntent(pe)
                .setAutoCancel(true);
        NotificationManager notifi= (NotificationManager) mainActivity.getSystemService(Context.NOTIFICATION_SERVICE);
        notifi.notify(0, noti.build());



      //  PendingIntent intent = PendingIntent.getActivity(mainActivity.getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //verNotificacion("PABS", notificationMessage);
        /*NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mainActivity, channelId  )
                .setSmallIcon(getIcon());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            mBuilder.setLargeIcon(getLargeIcon()).setColor(getResources().getColor(R.color.base_color));
        }
        mBuilder.setWhen(System.currentTimeMillis())
                .setContentTitle(getString(R.string.app_name))
                .setContentText(notificationMessage)
                .setVibrate(vibrate)
                .setSound(alarmSound)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationMessage))
                .setContentIntent(intent)
                .setAutoCancel(true);

        NotificationManager mNotificationManager =
                (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);

            if (mNotificationManager != null)
                mNotificationManager.createNotificationChannel(channel);
        }
        else
        {
            mBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
        }

        if (mNotificationManager != null)
            mNotificationManager.notify(Integer.parseInt(notificationId), mBuilder.build());*/

        Util.Log.ih("notificacion creada");
    }


    private void showSummaryNotifications(JSONArray notifications)
    {
        try
        {
            long[] vibrate = {500, 500, 0, 0, 500, 500};
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            String channelId = "pabsNotification";
            Intent notificationIntent = new Intent(mainActivity.getApplicationContext(), Notificaciones.class);

            notificationIntent.putExtra("id_cobrador", datosCobrador.getString("no_cobrador"));

            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent intent = PendingIntent.getActivity(mainActivity.getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mainActivity, channelId  )
                    .setSmallIcon(getIcon());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                mBuilder.setLargeIcon(getLargeIcon()).setColor(getResources().getColor(R.color.base_color));
            }

            mBuilder.setWhen(System.currentTimeMillis())
                    .setContentTitle(getString(R.string.app_name))
                    .setVibrate(vibrate)
                    .setSound(alarmSound)
                    .setContentIntent(intent)
                    .setAutoCancel(true);

            int numNotifications = 0;
            NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle("PABS");
            LocalDateTime now = LocalDateTime.now();
            JSONObject notification;

            for (int i = 0; i < notifications.length(); i++)
            {
                notification = notifications.getJSONObject(i);

                if (notification.getString("estatus").equals("0")) {
                    LocalDateTime date = LocalDateTime.parse(notification.getString("fecha_creacion"), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));

                    if (numNotifications + 1 < 7) {
                        if (notification.getString("no_cobrador").equals("-1")) {
                            if (date.getMonthOfYear() == now.getMonthOfYear() && date.getYear() == now.getYear())
                                messagingStyle.addMessage(notification.getString("notificacion"), date.toDate().getTime(), "");
                            else
                                continue;
                        }
                        else
                            messagingStyle.addMessage(notification.getString("notificacion"), date.toDate().getTime(), "Personal");
                    }

                    numNotifications++;
                }
            }

            messagingStyle.setConversationTitle(numNotifications + " notificaciones no leídas");

            mBuilder.setStyle(messagingStyle);

            NotificationManager mNotificationManager =
                    (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);

                if (mNotificationManager != null)
                    mNotificationManager.createNotificationChannel(channel);
            }
            else
            {
                mBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
            }

            //Util.Log.ih("mId = " + mId);

            if (mNotificationManager != null)
                mNotificationManager.notify(999, mBuilder.build());
        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * retrieves an icon to use with {@link Notification.Builder#setSmallIcon(int)}
     * according to SDK
     * @return
     * 		drawable with respective icon
     */
    private int getIcon(){
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
            return R.drawable.white_notification_icon;
        }
        else{
            return R.drawable.icono_hdpi;
        }
    }

    /**
     * retrieves an icon to use with {@link Notification.Builder#setLargeIcon(Bitmap)}
     * according to SDK
     * @return
     * 		drawable with respective icon
     */
    private Bitmap getLargeIcon()
    {
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP )
        {
            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.icono_hdpi);
            return getCircleBitmap(largeIcon);
        }
        else{
            return null;
        }
    }

    /**
     * This method will create a circle shaped bitmap
     * @param bitMap
     * 			bitmap object
     * @return
     * 			bitmap object customized
     */
    private Bitmap getCircleBitmap(Bitmap bitMap){
        final Bitmap output = Bitmap.createBitmap(bitMap.getWidth(),
                bitMap.getHeight(), Bitmap.Config.ARGB_8888);

        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitMap.getWidth(), bitMap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitMap, rect, rect, paint);

        bitMap.recycle();

        return output;
    }

    /**
     * downloads notifications and get new ones
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controlnotificaciones/getListNotification'
     *
     * JSONObject param
     * {
     *     "no_cobrador": "",
     *     "pag": ""
     * }
     * @param notificationsCounter
     */
    private void getNotificationAndShow(int notificationsCounter){
        JSONObject json = new JSONObject();
        try {
            json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
            json.put("pag", "" + 1);

        } catch (Exception e) {
            e.printStackTrace();
        }
        requestGetNotificaciones(json);
    }

    private void requestGetNotificaciones(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlNotificactiones, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                manage_GetNumNotificaciones(response);
                Log.i("Request LOGIN-->", new Gson().toJson(response));
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("getNumNotificaciones", "Error al obtener numero de notificaciones");
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }

    /**
     * Manages response of Web Service
     * gets message and id of new notifications and calls showNotification() method
     *
     * @param json web service's response
     * @param notificationsCounter number of new notifications
     */
    private void manage_GetNotifications(JSONObject json, int notificationsCounter)
    {
        if (json.has("result")) {
            try {
                JSONArray arrayNotifications = json.getJSONArray("result");
                if (arrayNotifications.length() > 0) {
                    /*for ( int i = 0; i < notificationsCounter; i++ ){
                        JSONObject jsonTmp = arrayNotifications.getJSONObject(i);
                        String notificationMessage = jsonTmp.getString("notificacion");
                        String notificationId = jsonTmp.getString("id");

                        showNotification(notificationMessage, notificationId);
                    }*/
                    if (arrayNotifications.length() > 1)
                        showSummaryNotifications(arrayNotifications);
                    else
                    {
                        JSONObject jsonTmp = arrayNotifications.getJSONObject(0);
                        String notificationMessage = jsonTmp.getString("notificacion");
                        String notificationId = jsonTmp.getString("id");

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                            //startMyOwnForeground();
                            showNotification(notificationMessage, notificationId);
                        else
                            Log.d("Version: ", "Version not support");
                            //startForeground(1, new Notification());
                    }
                }
            } catch (JSONException e){
                e.printStackTrace();
            }
        } else {
            mostrarMensajeError(ConstantsPabs.getNotificaciones);
        }
    }


    /**
     * Este metodo procesa el response y obtiene la respuesta del servidor
     *
     * @param json
     *            JSON que contiene el saldo del dia anterior
     */

    //-----------------------------------------------------------------------------------------------BUSCAR ESTO------------------------------------------------
    private void manage_ListClients(JSONObject json)
    {
        Log.e("descarga", "clientes");
        if (json.has("result")) {

            //it updates date from device when logging in without internet access
            // but after updating contracts,
            // which mean that  the device has now internet access
            //setSharedPreferencesForDateWhenNoInternetAccess();
            saveListClients(json);
        } else if (json.has("error"))
        {
            dismissMyCustomDialog();
            if(progress!=null)
                progress.dismiss();
            String error = null;
            try {
                error = json.getString("error");

                if (error.equals("No hay Clientes disponibles") || error.equals("No se obtuvieron depositos")) {
                    if (BuildConfig.isWalletSynchronizationEnabled()){
                        if (clientListFromSyncedWallet == null) {
                            clientListFromSyncedWallet = new ArrayList<>();
                        }
                    }
                    else {
                        if (clientsList == null) {
                            clientsList = new ArrayList<>();
                        }
                    }
                    try {
                        mainActivity.showAlertDialog("", error, true);
                    } catch (Exception e){
                        Toast toast = Toast.makeText(getContext(), "Error: " + error, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        e.printStackTrace();
                    }
                    //ClientesAdapter adapter;
                    if (BuildConfig.isWalletSynchronizationEnabled()){
                        adapter = getAdapterSyncedWalletClients();
                    }
                    else {
                        adapter = getClientesAdapterClientsList();
                    }
                    //lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);
                    if (lvClientes != null) {
                        lvClientes.setAdapter(adapter);
                        /*lvClientes.setDivider(getResources().getDrawable(
                                R.drawable.divider_list_clientes));
                        lvClientes.setOnItemClickListener(mthis);*/
                    }
                    if(progress!=null)
                        progress.dismiss();
                } else {
                    mostrarMensajeError(ConstantsPabs.listClients);
                    if(progress!=null)
                        progress.dismiss();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                if(progress!=null)
                    progress.dismiss();
            }

        } else {
            dismissMyCustomDialog();
            mostrarMensajeError(ConstantsPabs.listClients);
            if(progress!=null)
                progress.dismiss();
        }

    }

    //--------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Manages response of web service
     * if failed to send payments, it will try to send them again,
     * if not, will set them as sync
     * @param json JSONObject with response of web service
     */
    private void manage_RegisterPagosOffiline(JSONObject json) {
        Log.e("result", json.toString());
        ExtendedActivity.synched = true;

        if (json.has("result"))
        {
            try {
                JSONArray arrayPagosFallidos = json.getJSONArray("result");
                if (arrayPagosFallidos.length() > 0) {
                    //mainActivity.runOnUiThread(this::registerPagos );
                    Log.i("Array Fallidos", "se encontraron pagos fallidos" + json.toString());

                } else {
                    sincronizarPagosRegistrados(json);
                    enviarDepositos();

                    Util.Log.ih("payments registered");
                    if (mandarABloqueo) {
                        mandarABloqueo = false;
                        cerrarTodoYMandarBloqueo();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Manages response of web service
     * @param json JSONObject with response of web service
     */
    private void manage_RegistrarNotificacionesLeidas(JSONObject json) {
        Log.e("result", json.toString());
        if (json.has("result")) {
            try {
                JSONArray arrayNotificacionesFallidos = json
                        .getJSONArray("result");
                if (arrayNotificacionesFallidos.length() > 0) {
                    TextView tvICon = (TextView) getView().findViewById(R.id.tv_icon_notificaciones);
                    int notificacionesActuales = Integer.parseInt(tvICon
                            .getText().toString());
                    notificacionesActuales = notificacionesActuales
                            + arrayNotificacionesFallidos.length();
                    tvICon.setText("" + notificacionesActuales);

                    Editor editor = getContext().getSharedPreferences(
                            "PABS_Main", Context.MODE_PRIVATE).edit();
                    editor.putString("numero_notificaciones", ""
                            + notificacionesActuales);
                    editor.apply();
                    mainActivity.runOnUiThread(this::registarNotificacionesLeidas);

                } else {
                    Editor editor = getContext().getSharedPreferences(
                            "PABS_Main", Context.MODE_PRIVATE).edit();
                    editor.putString("numero_notificaciones", "" + 0);
                    editor.apply();
                    //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
                    //query.deleteNotificacionesLeidas();
                    //query = null;
                    DatabaseAssistant.deleteViewedNotifications();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Manages response of web service
     * @param json JSONObject with response of web service
     */
    private void manage_SaveUbicacion(JSONObject json)
    {
        //TODO: BEFORE
		/*
		if (json.has("result")) {
			try {
				String numNotifications = json.getString("notifications");
				((TextView) findViewById(R.id.tv_icon_notificaciones))
						.setText(numNotifications);
				Editor editor = getSharedPreferences(
						"PABS_Main", Context.MODE_PRIVATE).edit();
				editor.putString("numero_notificaciones", "" + numNotifications);
				editor.apply();
				toastSReuse("Ubicación almacenada a servidor.");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		*/
        //TODO: AFTER
        if (json.has("result")) {
            //try {
            Util.Log.ih("deleting geoJSON file");
            //LogRegister.deleteGeoJsonFile(datosCobrador.getString("periodo"));
            LogRegister.deleteGeoJsonFile( "" + mainActivity.getEfectivoPreference().getInt("periodo", 0) );
            //} catch (JSONException e) {
            //	e.printStackTrace();
            //}
        }
    }

    /**
     * Manages response of web service
     * @param json JSONObject with response of web service
     */
    private void manage_SaveUbicacionesOffline(JSONObject json) {
        if (json.has("result")) {
            try {
                JSONArray arrayUbicacionesFallidas = json
                        .getJSONArray("result");
                if (arrayUbicacionesFallidas.length() > 0) {
                    mainActivity.runOnUiThread(this::saveUbicaciones);

                } else {
                    //mainActivity.toastSReuse("Ubicaciones enviadas correctamente.");
                    Snackbar.make(mainActivity.mainLayout, "Ubicaciones enviadas correctamente", Snackbar.LENGTH_SHORT).show();
                    //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
                    //query.deleteUbicaciones();
                    //query = null;
                    DatabaseAssistant.deleteLocations();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Manages response of web service
     * if failed to send deposits, it will try to send them again,
     * @param json JSONObject with response of web service
     * @param logDepositos List<LogRegister.LogDeposito> instance. deprecated in 3.0
     */
    private void manageRegisterDepositosOffline(JSONObject json, List<LogRegister.LogDeposito> logDepositos) {
        if (json.has("result")) {
            Log.e("result", json.toString());
            try {
                JSONObject result = json.getJSONObject("result");
                JSONArray almacenados = result.getJSONArray("almacenados");
                if (almacenados.length() > 0) {
                    syncDepositsSavedToServer(almacenados);
                }
                JSONArray repetidos = result.getJSONArray("repetidos");
                Util.Log.ih("repetidos = " + repetidos.toString());
                if (repetidos.length() > 0) {
                    syncDepositsSavedToServer(repetidos);
                }
                JSONArray noAlmacenados = result.getJSONArray("no_almacenados");
                if (noAlmacenados.length() > 0) {
                    //mainActivity.toastSReuse("Enviando depositos bancarios.");
                    Snackbar.make(mainActivity.mainLayout, "Enviando depositos bancarios", Snackbar.LENGTH_SHORT).show();
                    enviarDepositos();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * gets and sets info to log in
     */
    private void modificarInfoToLoginAndSplash() {
        SharedPreferences preferencesSplash = getContext().getSharedPreferences(
                "PABS_SPLASH", Context.MODE_PRIVATE);
        Editor editorSplash = preferencesSplash.edit();
        try {
            editorSplash.putString("no_cobrador",
                    datosCobrador.getString("no_cobrador"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        editorSplash.putString("datos_cobrador", datosCobrador.toString());
        editorSplash.apply();
        SharedPreferences preferencesLogin = getContext().getSharedPreferences("PABS_Login",
                Context.MODE_PRIVATE);
        Editor editorLogin = preferencesLogin.edit();
        try {
            editorLogin.putString("no_cobrador",
                    datosCobrador.getString("no_cobrador"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        editorLogin.putString("datos_cobrador", datosCobrador.toString());
        editorLogin.apply();
    }

    /**
     * shows error message
     * @param caso constant when failed
     */
    private void mostrarMensajeError(final int caso) {
        try {
            mainActivity.hasActiveInternetConnection(ApplicationResourcesProvider.getContext(), mainActivity.mainLayout);
            Builder alert = getBuilder();
            alert.setMessage("Hubo un error de comunicación con el servidor, verifique sus ajustes de red o intente más tarde.");
            alert.setPositiveButton("Aceptar",(dialogInterface, which) -> {});
			/*
			alert.setNegativeButton("Reintentar",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if (isConnected) {

							} else {
								mostrarMensajeError(caso);
							}
						}
					});
			*/
            alert.create();
            alert.setCancelable(false);
            alert.show();
        } catch (Exception e){
            //nothing...
            e.printStackTrace();
        }
    }

    /**
     * gets and shows cash at the moment when pressing cash amount on the screen
     *
     * company's amount -> total from company
     * company's canceled amount -> total canceled from company
     * company's deposited amount -> total deposited from company
     *
     * (company's amount) - (company's canceled amount) - (company's deposited amount) = cash at the moment
     */
    private void mostrarSaldoAlMomento() {
        double totalDepositos = DatabaseAssistant.getTotalDeposits();
        List<Companies> companies = DatabaseAssistant.getCompanies();
        String efectivoEmpresas = new String();
        String cancelados = new String();

        int size;
        switch (BuildConfig.getTargetBranch()){
            case GUADALAJARA: case TAMPICO:
                size = companies.size();
                break;
            case TOLUCA:
                size = 4;
                break;
            case QUERETARO:case IRAPUATO:case SALAMANCA:case CELAYA: case LEON:
                size = 2;
                break;
            case PUEBLA:case CUERNAVACA:
                size = 4;
                break;
            case MERIDA:
                size = 4;
                break;
            case SALTILLO:
                size = 4;
                break;
            case CANCUN:
                size = 4;
                break;
            case MEXICALI: case TORREON:
                size = 3;
                break;
            case MORELIA:
                size = 3;
                break;
            default:
                size = 4;
        }

        if ( DatabaseAssistant.isThereMoreThanOnePeriodActive() )
        {
            // HAY MAS DE UN PERIODO ACTIVO ------------------------------

            List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();
            for (ModelPeriod mp: periods)
            {
                efectivoEmpresas = "";
                cancelados = "";
                Util.Log.ih("adding period..." + mp.getPeriodNumber());
                mainActivity.getEfectivoEditor().putInt("periodo", mp.getPeriodNumber()).apply();

                for (int cont = 0; cont < size; cont++) //4 ITERACIONES PARA GDL
                {
                    Companies company = companies.get(cont);
                    float depositos = (float) DatabaseAssistant.getTotalDepositsByCompany(company.getIdCompany());

                    //efectivoEmpresas += "+"+ ExtendedActivity.getCompanyNameFormatted( company.getName(), false ) + ": $" + mainActivity.formatMoney(DatabaseAssistant.getTotalCompanyWhenMoreThanOnePeriodIsActive(company.getName(), mp.getPeriodNumber()) + depositos) + "\n";
                   // efectivoEmpresas += "+"+ ExtendedActivity.getCompanyNameFormatted( company.getName(), false ) + ": $" +
                     //       mainActivity.formatMoney(DatabaseAssistant.getTotalCompanyWhenMoreThanOnePeriodIsActive(company.getIdCompany(), mp.getPeriodNumber()) + depositos) + "\n";

                    efectivoEmpresas += "+"
                            + ExtendedActivity.getCompanyNameFormatted( company.getName(), false )
                            + ": $"
                            //+ mainActivity.formatMoney(mainActivity.getEfectivoPreference().getFloat(company.getName(), 0) + depositos) + "\n";
                            + mainActivity.formatMoney(DatabaseAssistant.montosCobrosMasCancelados(company.getIdCompany())) + "\n"; //Pooner tambien que periodo se requiere

                    cancelados += "-Cancelados "
                            + ExtendedActivity.getCompanyNameFormatted( company.getName(), true ).charAt(0)
                            + " : $"
                            + DatabaseAssistant.getTotalCanceledByCompany(company.getIdCompany())
                            + "\n";
                }



                try {
                    /*mainActivity.showAlertDialog("Efectivo al momento - " + mp.getPeriodNumber(),
                            efectivoEmpresas
                                    + "-Depositos: $" + totalDepositos + "\n" + cancelados
                                    + "=Total: $" + (ExtendedActivity.getTotalEfectivoWhenMoreThanOnePeriodIsActive(mp.getPeriodNumber())), true);*/

                    mainActivity.showAlertDialog("Efectivo al momento - " + mp.getPeriodNumber(),
                            efectivoEmpresas
                                    + "-Depositos: $" + totalDepositos + "\n" + cancelados
                                    + "=Total: $" + (DatabaseAssistant.getTotalEfectivoAcumuladoPorPeriodo(mp.getPeriodNumber())), true);

                } catch (Exception e) {
                    Toast toast = Toast.makeText(getContext(), "Error: Vuelve a intentarlo", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    e.printStackTrace();
                }
            }
        }
        else {

            // SOLO HAY UN PERIODO ACTIVO

            for (int cont = 0; cont < size; cont++)
            {
                Companies company = companies.get(cont);

                float depositos = (float) DatabaseAssistant.getTotalDepositsByCompany(company.getIdCompany());

                efectivoEmpresas += "+"
                        + ExtendedActivity.getCompanyNameFormatted( company.getName(), false )
                        + ": $"
                        //+ mainActivity.formatMoney(mainActivity.getEfectivoPreference().getFloat(company.getName(), 0) + depositos) + "\n";
                        + mainActivity.formatMoney(DatabaseAssistant.montosCobrosMasCancelados(company.getIdCompany())) + "\n";

                //(cobros + cancelados)



                cancelados += "-Cancelados "
                        + ExtendedActivity.getCompanyNameFormatted( company.getName(), false ).charAt(0)
                        + " : $"
                        + DatabaseAssistant.getTotalCanceledByCompany(company.getIdCompany()) + "\n";
            }

            try {
                mainActivity.showAlertDialog("Efectivo al momento - " + mainActivity.getEfectivoPreference().getInt("periodo", 0),
                        efectivoEmpresas
                                + "-Depositos: $" + totalDepositos + "\n" + cancelados
                                + "=Total: $" + (DatabaseAssistant.getTotalEfectivoAcumulado()), true);
            } catch (Exception e) {
                Toast toast = Toast.makeText(getContext(), "Error: Vuelve a intentarlo", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                e.printStackTrace();
            }
        }
    }

    /**
     * This is for debugging purposes
     */
    private void randomListClick(){
        if (cont < 10){
            cont++;
            int min = 0;
            //int max = arrayClientes.length();
            int max = clientListFromSyncedWallet.size();
            int position = rand.nextInt((max - min) + 1) + min;
            JSONObject jsonTmp = null;
            Gson gson = new Gson();
            try {
                if (filteredClients != null) {
                    jsonTmp = new JSONObject( gson.toJson( filteredClients.get( position ) ) );
                } else if (arrayCercanos != null) {
                    jsonTmp = arrayCercanos.get(position);
                } else {
                    //jsonTmp = arrayClientes.getJSONObject(position);
                    jsonTmp = new JSONObject(gson.toJson( clientListFromSyncedWallet.get(position) ));
                }
                Log.d("Cliente", jsonTmp.toString(1));

                ClienteDetalle clienteDetalle = new ClienteDetalle();

                Bundle args = new Bundle();
                //Intent intent = new Intent(getContext(), ClienteDetalle.class);
                args.putString("datos", jsonTmp.toString());
                args.putString("datoscobrador", datosCobrador.toString());
                args.putBoolean("isConnected", mainActivity.isConnected);
                args.putString("callback", this.toString());
                args.putInt("requestCode", 1);
                //startActivityForResult(intent, 1);
                clienteDetalle.setArguments(args);
                clienteDetalle.setFragmentResult(this);
                getFragmentManager().beginTransaction()
                        .add(R.id.root_layout, clienteDetalle, MainActivity.TAG_CLIENTE_DETALLES)
                        .hide(this)
                        .addToBackStack("cliente_detalle")
                        .commitAllowingStateLoss();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * updates total cash and max cash on the screen
     * @throws JSONException
     */
    private void putInfo() throws JSONException
    {
        try
        {

            if (DatabaseAssistant.isThereMoreThanOnePeriodActive())
            {
                //String textoFormado = "$" + (ExtendedActivity.getTotalEfectivoFromAllPeriods(mainActivity.getEfectivoPreference().getInt("periodo", 0))) + "/$" + mainActivity.formatMoney(Double.parseDouble(datosCobrador.getString("max_efectivo")));
                String textoFormado = "$" + (ExtendedActivity.getTotalEfectivoFromAllPeriods()) + "/$" + mainActivity.formatMoney(Double.parseDouble(datosCobrador.getString("max_efectivo")));
                tvEfectivoAcumulado.setText(textoFormado);
            }
            else
                {

                    if (mainActivity.isConnected)
                    {
                        //String textoFormado="$" + (mainActivity.getTotalEfectivo()) + "/$" + mainActivity.formatMoney(Double.parseDouble(datosCobrador.getString("max_efectivo")));
                        String textoFormado="$" + (mainActivity.formatMoney(DatabaseAssistant.getTotalEfectivoAcumulado())) + "/$" + mainActivity.formatMoney(Double.parseDouble(datosCobrador.getString("max_efectivo")));
                        tvEfectivoAcumulado.setText(textoFormado);
                    }
                    else
                        {
                        //TextView txtEfectivxo = getView().findViewById(R.id.tv_total_efec);

                        try
                        {
                            String textoFormado="$" + (mainActivity.formatMoney(DatabaseAssistant.getTotalEfectivoAcumulado())) + "/$" + mainActivity.formatMoney(Double.parseDouble(datosCobrador.getString("max_efectivo")));
                            //String textoFormado = "$" + (mainActivity.getTotalEfectivo()) + "/$" + mainActivity.formatMoney(Double.parseDouble(datosCobrador.getString("max_efectivo")));
                            tvEfectivoAcumulado.setText(textoFormado);
                        }
                        catch (JSONException ex)
                        {
                            ex.printStackTrace();
                            tvEfectivoAcumulado.setText("$-/-");
                        }
                    }
                }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
    }



    /**
     * sends to server read notifications
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controlnotificaciones/updateNotification'
     * JSONObject param
     * {
     *     "notificaciones": {}
     * }
     */
    private void registarNotificacionesLeidas() {

        try {
            //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
            String viewedNotifications = DatabaseAssistant.getViewedNotifications();
            //JSONObject jsonTmp = query.mostrarNotificacionesLeidas();
            //query = null;
            JSONArray jsonArrayNotificaciones = null;
            if (viewedNotifications.length() > 0) {
                jsonArrayNotificaciones = new JSONArray( viewedNotifications );
            } else {
                jsonArrayNotificaciones = new JSONArray();
            }
            JSONObject json = new JSONObject();
            json.put("notificaciones", jsonArrayNotificaciones);

            requestRegisterNotificacionesNoLeidas(json);

            /*NetService service = new NetService(ConstantsPabs.urlRegistrarNotificacionesLeidas, json);
            NetHelper.getInstance().ServiceExecuter(service, getContext(),
                    new onWorkCompleteListener() {

                        @Override
                        public void onCompletion(String result) {
                            try {
                                manage_RegistrarNotificacionesLeidas(new JSONObject(
                                        result));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e("regNotificaciones", "Error al registrar notificaciones leidas");
                        }
                    });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestRegisterNotificacionesNoLeidas(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlRegistrarNotificacionesLeidas, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                manage_RegistrarNotificacionesLeidas(response);
                Log.i("Request LOGIN-->", new Gson().toJson(response));
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("regNotificaciones", "Error al registrar notificaciones leidas");
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }


    /**
     * saves list of clients into database
     * @param jsonobject
     */

    //-----------------------------------------------------------------------------BUSCAR ESTO------------------------------------------------------------
    private void saveListClients(final JSONObject jsonobject)
    {

        //mainActivity.toastSReuse("Guardando clientes... Espere un momento.");
        Snackbar.make(mainActivity.mainLayout, "Guardando clientes... Espere un momento", Snackbar.LENGTH_SHORT).show();
        Runnable run = () -> {
            DatabaseAssistant.deleteClients();
            try {
                Log.d("ALMACENANDO CLIENTES", ":)");
                JSONArray arrayClientes = jsonobject.getJSONArray("result");

                if (jsonobject.has("result_topics"))
                {

                    Util.Log.ih("jsonobject does have result_topics");

                    DatabaseAssistant.deleteAllOsticketTopics();

                    JSONArray arrayOsticketTopics = jsonobject.getJSONArray("result_topics");
                    for (int j = 0; j < arrayOsticketTopics.length(); j++) {
                        JSONObject topic = arrayOsticketTopics.getJSONObject(j);

                        DatabaseAssistant.insertOsticketTopics(
                                topic.getString("osticket_topic")
                        );
                    }
                } else {
                    Util.Log.ih("jsonobject does not have (\"result_topics\")");
                }

                //*****************Verificamos si tenemos la fecha del sistema para verificar la cartera actualizada
                if(jsonobject.has("fecha_sistema"))
                {
                    fecha_sistema_servidor = jsonobject.getString("fecha_sistema");
                     Collectors.executeQuery("UPDATE COLLECTORS SET fechaSistemaServidor = '" + fecha_sistema_servidor + "'");
                    Log.d("COLLECTORS:--->", "Actualizados: " + new Gson().toJson(Collector.listAll(Collectors.class)));
                }
                else
                {
                    fecha_sistema_servidor="1900-01-01";
                    Collectors.executeQuery("UPDATE COLLECTORS SET fechaSistemaServidor = '" + fecha_sistema_servidor + "'");
                    Log.d("COLLECTORS:--->", "Sin Actualizar: " + new Gson().toJson(Collector.listAll(Collectors.class)));
                }
                //{-----------------------------------------------------------------------------------------


                for (int cont = 0; cont < arrayClientes.length(); cont++)
                {

                    JSONObject json = arrayClientes.getJSONObject(cont);
                    String localidad = json.getString("localidad");
                    String no_contrato = json.getString("no_contrato");
                    String no_ext_cobro = json.getString("no_ext_cobro");
                    String apellido_pat = json.getString("apellido_pat");
                    String monto_pago_actual = json.getString("monto_pago_actual");
                    String colonia = json.getString("colonia");
                    String calle_cobro = json.getString("calle_cobro");
                    String nombre = json.getString("nombre");
                    String serie = json.getString("serie");
                    String forma_pago_actual = json.getString("forma_pago_actual");
                    String apellido_mat = json.getString("apellido_mat");
                    String id_contrato = json.getString("id_contrato");
                    String no_cobrador = json.getString("no_cobrador");
                    String abono = json.getString("Abonado");
                    String saldo = json.getString("Saldo");
                    String entre_calles = json.getString("entre_calles");
                    String id_grupo_base = json.getString("id_grupo_base");
                    String tipo_bd = json.getString("tipo_bd");
                    String latitude = json.getString("latitud");
                    String longitude = json.getString("longitud");
                    String estatus = json.getString("estatus");
                    String fecha_ultimo_abono = json.getString("tiempo");
                    String comments = "";
                    String fecha_primer_abono = json.getString("fecha_primer_abono");

                    if(json.has("phone"))
                    {
                        phone = json.getString("phone");
                    }else
                        phone ="sin teléfono";



                    if(json.has("fecha_sistema"))
                    {
                        fecha_sistema_servidor = json.getString("fecha_sistema");
                    }else
                        fecha_sistema_servidor ="1900-01-01";



                    if(json.has("fecha_ultima_actualizacion"))
                    {
                        fechaUltimaActualizacion= json.getString("fecha_ultima_actualizacion");
                    }
                    else
                        fechaUltimaActualizacion="";


                    if(json.has("fecha_reactiva"))
                    {
                        fechaReactiva= json.getString("fecha_reactiva");
                    }
                    else
                        fechaReactiva="Sin fecha reactiva";




                    String contractStatus = "";
                    if(json.has("contrato_estatus"))
                    {
                        contractStatus = json.getString("contrato_estatus");
                    }

                    String email="";
                    if(json.has("correo"))
                    {
                        email=json.getString("correo");
                    }
                    else
                    {
                        email="";
                    }


                    String numeroInterior="";
                    if(json.has("numero_interior"))
                    {
                        numeroInterior=json.getString("numero_interior");
                    }
                    else
                    {
                        numeroInterior="";
                    }



                    if (json.has("monto_atrasado")) {
                        String saldo_atrasado = json.getString("monto_atrasado");

                        DatabaseAssistant.insertClient(id_contrato, nombre, apellido_pat, apellido_mat,
                                localidad, no_contrato, no_cobrador, calle_cobro, no_ext_cobro,
                                entre_calles, colonia, abono, saldo, monto_pago_actual, serie,
                                forma_pago_actual, id_grupo_base, tipo_bd, latitude, longitude,
                                estatus, fecha_ultimo_abono, saldo_atrasado, comments, fecha_primer_abono,
                                phone, contractStatus, fechaUltimaActualizacion, email, numeroInterior, fechaReactiva);
                    } else {

                        DatabaseAssistant.insertClient(id_contrato, nombre, apellido_pat, apellido_mat,
                                localidad, no_contrato, no_cobrador, calle_cobro, no_ext_cobro,
                                entre_calles, colonia, abono, saldo, monto_pago_actual, serie,
                                forma_pago_actual, id_grupo_base, tipo_bd, latitude, longitude,
                                estatus, fecha_ultimo_abono, comments, fecha_primer_abono, phone,
                                contractStatus, fechaUltimaActualizacion, email, numeroInterior, fechaReactiva);
                    }
                }

                boolean updatePaydays = false;
                //inserts paydays into each client
                if (BuildConfig.isWalletSynchronizationEnabled()) {
                    Util.Log.ih("SEMAFORO ACTIVADO");
                    if (jsonobject.has("result_paydays")) {

                        updatePaydays = true;

                        Util.Log.ih("jsonobject does have (\"result_paydays\")");
                        JSONArray arrayPaydays = jsonobject.getJSONArray("result_paydays");
                        for (int j = 0; j < arrayPaydays.length(); j++) {
                            JSONObject contract = arrayPaydays.getJSONObject(j);
                            DatabaseAssistant.insertPaydaysIntoClients(
                                    contract.getString("id_contrato"),
                                    contract.getString("forma_pago_actual"),
                                    contract.getString("semanal"),
                                    contract.getString("quincenal1"),
                                    contract.getString("quincenal2"),
                                    contract.getString("mensual")
                            );
                        }
                    } else {
                        Util.Log.ih("jsonobject does not have (\"result_paydays\")");
                    }
                } else {
                    Util.Log.ih("SEMAFORO DEASCTIVADO");
                }

                if (BuildConfig.isWalletSynchronizationEnabled()) {
                    Util.Log.ih("UPDATING SYNCED_WALLET TABLE...");

                    DatabaseAssistant.updateClientsInfoFromSyncedWallet(updatePaydays);
                    DatabaseAssistant.deleteContractsNotAssigned();
                    DatabaseAssistant.insertNewClientsIntoSyncedWallet();

                    Util.Log.ih("FINISH UPDATING SYNCED_WALLET TABLE...");
                } else {
                    Util.Log.ih("SEMAFORO DEASCTIVADO");
                }

                firstLoad = true;
					/*
					if (BuildConfig.isWalletSynchronizationEnabled()) {
						DatabaseAssistant.getClientsFromSyncedWalletThread();
					} else {
						DatabaseAssistant.getAllClients();
					}
					*/

                if (jsonobject.has("result_banks")) {

                    DatabaseAssistant.deleteBanks();

                    Util.Log.ih("jsonobject does have (\"bancos\")");
                    JSONArray arrayBanks = jsonobject.getJSONArray("result_banks");
                    for (int j = 0; j < arrayBanks.length(); j++) {
                        JSONObject json = arrayBanks.getJSONObject(j);
                        String bankID = json.getString("no_banco");
                        String name = json.getString("nombre");

                        DatabaseAssistant.insertBanks(bankID, name);
                    }
                } else {
                    Util.Log.ih("jsonobject does not have (\"bancos\")");
                }


                actualizarListView();

            } catch (Exception e) {
                e.printStackTrace();

                Log.d("Clientes Error", e.getMessage());

                mainActivity.runOnUiThread(() -> {
                    try {
                        dismissMyCustomDialog();
                        mainActivity.showAlertDialog("Error", "No se pudieron obtener los clientes. Por favor presiona el botón de actualizar (flecha circular en la parte superior)", false);
                    } catch (Exception ex) {
                        Toast toast = Toast.makeText(getContext(), "Error: No se pudieron obtener los clientes. Por favor presiona el botón de actualizar (flecha circular en la parte superior)", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                });
            }
            if(progress!=null)
                progress.dismiss();
        };
        Thread thread = new Thread(run);
        thread.start();

    }

    //_-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * sends to server all locations saved plus cash of every company
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controlubicaciones/saveUbicacionOffline'
     * JSONObject param
     * {
     *     "no_cobrador": "",
     *     "ubicaciones": {},
     *     "efectivo": ,
     *     "efectivoMalba": ,
     *     "efectivo_empresa4": ,
     *     "efectivo_empresa5": ,
     *     "cancelados_empresa4": ,
     *     "cancelados_empresa5": ,
     *     "efectivo_depositos": ,
     *     "efectivo_inicial": ,
     *     "cancelados_malba": ,
     *     "cancelados_programa":
     * }
     *
     * NOTE*
     * 		ADDS COMPANIES
     */
    private void saveUbicaciones() {
        JSONObject json = new JSONObject();

        try {
            //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());

            SharedPreferences preferencesLogin = getContext().getSharedPreferences(
                    "PABS_Login", Context.MODE_PRIVATE);

            json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
            json.put("ubicaciones", new JSONArray( new Gson().toJson( offlineLocations ) ));
            json.put("efectivo", "" + DatabaseAssistant.getTotalByCompany("01"));
            json.put("efectivoMalba", "" + DatabaseAssistant.getTotalByCompany("03"));
            json.put("efectivo_empresa4", DatabaseAssistant.getTotalByCompany("04"));
            json.put("efectivo_empresa5", DatabaseAssistant.getTotalByCompany("05"));
            json.put("cancelados_empresa4",
                    DatabaseAssistant.getTotalCanceledByCompany("04"));
            json.put("cancelados_empresa5",
                    DatabaseAssistant.getTotalCanceledByCompany("05"));

            json.put("efectivo_depositos", DatabaseAssistant.getTotalDeposits());
            json.put("efectivo_inicial",
                    preferencesLogin.getString("efectivo_inicial", "0.0"));
            json.put("cancelados_malba", DatabaseAssistant.getTotalCanceledByCompany("03"));
            json.put("cancelados_programa", DatabaseAssistant.getTotalCanceledByCompany("01"));

            //query = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
        requestSaveUbicacionesOffline(json);
        /*NetService service = new NetService(ConstantsPabs.urlSaveUbicacionesOffline, json);
        NetHelper.getInstance().ServiceExecuter(service, getContext(), new onWorkCompleteListener() {

                    @Override
                    public void onCompletion(String result) {
                        try {
                            manage_SaveUbicacionesOffline(new JSONObject(result));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("saveUbicacion", "Error al guardar ubicacion");
                    }
        });*/

    }
    private void requestSaveUbicacionesOffline(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlSaveUbicacionesOffline, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                manage_SaveUbicacionesOffline(response);
                Log.i("Request LOGIN-->", new Gson().toJson(response));
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("saveUbicacion", "Error al guardar ubicacion");
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }


    /**
     * instantiates timer to send locations to server
     * starting 1 second after instantiated and
     * with a delay of 10 minutes to run again
     */
    private void sendLocation()
    {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                mainActivity.runOnUiThread(() -> {
                    Util.Log.ih("trying to send locations...");
                    Util.Log.ih("5 minutes of inactivity");
                    if (getContext() != null && !Deposito.depositosIsActive)
                    {
                        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                        boolean isGPSEnabled;
                        if (locationManager != null)
                        {
                            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                        }
                        else
                        {
                            isGPSEnabled = false;
                        }


                        if (isGPSEnabled) {

                            //TODO: NEW PROCESS
                            try {
                                String periodo = "" + mainActivity.getEfectivoPreference().getInt("periodo", 0);
                                String collectorNumber = datosCobrador.getString("no_cobrador");

                                ApplicationResourcesProvider.saveGeoJSONFile(periodo, collectorNumber);
                                //ApplicationResourcesProvider.ubicacionesCada10Minutos();

                                JSONObject jsonEnviar = new JSONObject();
                                String geoLocations = LogRegister.readGeoJSONDataFile(periodo);

                                if (geoLocations == null)
                                    throw new JSONException("geoLocations IS NULL");

                                jsonEnviar.put("no_cobrador", collectorNumber).put("geoLocations", geoLocations).put("periodo", periodo);

                                if (mainActivity.isConnected) {
                                        //enviarUbicacion(jsonEnviar);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Snackbar.make(mainActivity.mainLayout, "Ubicación registrada", Snackbar.LENGTH_SHORT).show();

                        }
                    }
                });

            }
        }, 0, 300000);
    }

    /**
     * sets listener of resources on screen as
     * notifications
     * nearby clients
     * deposits
     * payments 'fuera de cartera'
     * 'cierre informativo'
     * refresh
     * end session
     * cash at the moment---------------------------------------------------------------------------------------------------------------------------------------
     */
    private void setListeners() {
        final RelativeLayout notifications = (RelativeLayout) getView().findViewById(R.id.rlNotifications);

        final ImageView deposito = (ImageView) getView().findViewById(R.id.ImageViewDeposito);

        final ImageView aportefuera = (ImageView) getView().findViewById(R.id.imageViewAporteFuera);

        final ImageView refresh = (ImageView) getView().findViewById(R.id.imageViewRefresh);

        final ImageView cierre = (ImageView) getView().findViewById(R.id.imageViewCierre);

        final ImageButton cercanos = (ImageButton) getView().findViewById(R.id.btn_buscar_cercanos);

        final ImageView salir = (ImageView) getView().findViewById(R.id.imageViewSalir);

        final ImageView osticket = (ImageView) getView().findViewById(R.id.btn_ticket_support_iv);

        FrameLayout imageSaldoMomento = (FrameLayout) getView().findViewById(R.id.btn_show_saldos);

        final ImageView noVisitados = (ImageView) getView().findViewById(R.id.imageViewNoVisitados);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //notifications image view
            notifications.setOnClickListener(view -> setImageViewNotificationsTouchBehaviour());

            //deposits image view
            deposito.setOnClickListener(view -> setImageViewDepositsTouchBehaviour());

            //Fuera de cartera image view
            aportefuera.setOnClickListener(view -> setImageViewFueraDeCarteraTouchBehaviour());


            //cierre informativo image view
            cierre.setOnClickListener(view -> setImageViewCierreInformativoTouchBehaviour());

            //no visitados image view
            noVisitados.setOnClickListener(view -> setImageViewNoVisitadosTouchBehaviour());
        }
        else{
            //notifications image view
            notifications.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    notifications.setAlpha(0.5f);
                    return true;
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    notifications.setAlpha(1.0f);
                    setImageViewNotificationsTouchBehaviour();
                    return true;
                }
                return false;
            });

            //deposits image view
            deposito.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    deposito.setAlpha(0.5f);
                    return true;
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    deposito.setAlpha(1.0f);
                    setImageViewDepositsTouchBehaviour();
                    return true;
                }
                return false;
            });

            //Fuera de cartera image view
            aportefuera.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    aportefuera.setAlpha(0.5f);
                    return true;
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    aportefuera.setAlpha(1.0f);
                    setImageViewFueraDeCarteraTouchBehaviour();
                    return true;
                }
                return false;
            });


            cierre.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    cierre.setAlpha(0.5f);
                    return true;
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    cierre.setAlpha(1.0f);
                    setImageViewCierreInformativoTouchBehaviour();
                    return true;
                }
                return false;
            });

            noVisitados.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    noVisitados.setAlpha(0.5f);
                    return true;
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    noVisitados.setAlpha(1.0f);
                    setImageViewNoVisitadosTouchBehaviour();
                    return true;
                }
                return false;
            });
        }

        cercanos.setOnClickListener(view ->
        {
            Location myLocation=null;
            double latitud=0, longitud=0;
            double radio = Double.parseDouble( DatabaseAssistant.getRadioToShowNearbyClients() );

            try {

                LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                //Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                //Log.d("GPS ACTIVADO?", "" + myLocation);


                if (isGPSEnabled)
                {
                    if (ApplicationResourcesProvider.getLocationProvider().getGPSPosition() != null)
                    {
                        myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                        if (BuildConfig.isWalletSynchronizationEnabled())
                        {
                            if (clientListFromSyncedWalletAll != null && clientListFromSyncedWalletAll.size() > 0)
                            {
                                if (filteredClients != null) {
                                    filteredClients = null;
                                    getView().findViewById(R.id.ib_cancelar_filtro).setVisibility(View.GONE);
                                    ((EditText) getView().findViewById(R.id.et_filtrado)).setText("");

                                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(((EditText) getView().findViewById(R.id.et_filtrado)).getWindowToken(), 0);
                                    Log.d("filteredClients", " era != NULL");
                                } else {
                                    Log.d("filteredClients", "es NULL");
                                }

                                if (nearbyClientsFromSyncedWallet == null) {
                                    nearbyClientsFromSyncedWallet = new ArrayList<ModelClient>();
                                }
                                Drawable drawable = null;

                                //gets all clientes that within radio
                                if (nearbyClientsFromSyncedWallet.size() == 0)
                                {
                                    for (int i = 0; i < clientListFromSyncedWalletAll.size(); i++) {
                                        try {
                                            ModelClient nearbyClientTemp = clientListFromSyncedWalletAll.get(i).getClient();
                                            double latitude = Double.parseDouble(nearbyClientTemp.getLatitude());
                                            double longitude = Double.parseDouble(nearbyClientTemp.getLongitude());
                                            double distance = getDistance(myLocation, latitude, longitude);

                                            if (distance <= radio) {
                                                nearbyClientTemp.setDistance(Double.toString(distance));
                                                nearbyClientsFromSyncedWallet.add(nearbyClientTemp);
                                            }
                                        } catch (Exception e) {
                                            //e.printStackTrace();
                                        }
                                    }
                                    //shows clients on screen
                                    if (nearbyClientsFromSyncedWallet.size() > 0) {
                                        Toast.makeText(getContext(), "Búsqueda finalizada", Toast.LENGTH_SHORT).show();

                                        Collections.sort(nearbyClientsFromSyncedWallet, new CustomComparatorToSortByDistanceFromSyncedWallet());
                                        List<SyncedWallet> nearbySyncedWallet = new ArrayList<>();
                                        for (ModelClient client: nearbyClientsFromSyncedWallet)
                                        {
                                            SyncedWallet syncedClient = new SyncedWallet(client.getIdContract(), client.getPayDay(), client.getPaymentOption(), client.getFirstPayday(), client.getSecondPayday(), client, client.getStatus());
                                            syncedClient.setClient(client);
                                            nearbySyncedWallet.add(syncedClient);
                                        }

                                        adapter = new ClientesRecyclerAdapter<>(getContext(), nearbySyncedWallet, myLocation, ClientesAdapter.NEARBY_CLIENTS, this);
                                        drawable = getResources().getDrawable(
                                                R.drawable.btn_clientescer_pressed);
                                    } else {//if there's no cliente near by current location, shows wallet
                                        Util.Log.ih("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                                        Toast.makeText(getContext(), "Sin clientes a " + radio + " m de radio", Toast.LENGTH_SHORT).show();
                                        drawable = getResources().getDrawable(R.drawable.btn_clientescerc);
                                        adapter = getAdapterSyncedWalletClients();
                                        nearbyClientsFromSyncedWallet = null;
                                    }

                                } else {//when clicking again to show main list
                                    Util.Log.ih("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
                                    drawable = getResources().getDrawable(R.drawable.btn_clientescerc);
                                    adapter = getAdapterSyncedWalletClients();
                                    nearbyClientsFromSyncedWallet = null;
                                }
                                cercanos.setImageDrawable(drawable);
                                if (lvClientes != null) {
                                    lvClientes.setAdapter(adapter);
                                    if (listViewState != null) {
                                        Util.Log.ih("!= null");
                                    }
                                }
                            } else {
                                Toast.makeText(getContext(), "No cuentas con clientes a organizar", Toast.LENGTH_SHORT).show();
                            }
                        } else{
                            clientsList = DatabaseAssistant.getAllClients();

                            if (clientsList != null && clientsList.size() > 0) {
                                //means that search field has given text
                                if (filteredClients != null) {
                                    filteredClients = null;
                                    getView().findViewById(R.id.ib_cancelar_filtro).setVisibility(View.GONE);
                                    ((EditText) getView().findViewById(R.id.et_filtrado)).setText("");

                                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(((EditText) getView().findViewById(R.id.et_filtrado)).getWindowToken(), 0);

                                    Log.d("filteredClients", " era != NULL");
                                } else {
                                    Log.d("filteredClients", "es NULL");
                                }

                                if (nearbyClients == null) {
                                    nearbyClients = new ArrayList<Clients>();
                                }
                                Drawable drawable = null;

                                //gets all clientes that within radio
                                if (nearbyClients.size() == 0) {
                                    for (int i = 0; i < clientsList.size(); i++) {

                                        Util.Log.ih("count = " + i);
                                        try {

                                            Clients nearbyClientTemp = clientsList.get(i);

                                            double latitude = Double.parseDouble(nearbyClientTemp.getLatitude());
                                            double longitude = Double.parseDouble(nearbyClientTemp.getLongitude());
                                            Location locationTmp = new Location("other");
                                            locationTmp.setLatitude(20.740930);
                                            locationTmp.setLongitude(-103.322662);

                                            double distance = getDistance(locationTmp, latitude, longitude);
                                            Log.d("Nombre: " + clientsList.get(i).getName() + " " + clientsList.get(i).getFirstLastName(),
                                                    "DISTANCIA: " + distance + " lat" + nearbyClientTemp.getLatitude() + " long" + nearbyClientTemp.getLongitude());

                                            if (distance <= radio) {
                                                nearbyClientTemp.setDistance(Double.toString(distance));
                                                nearbyClients.add(nearbyClientTemp);

                                                Util.Log.ih("there's a contract near you: " + clientsList.get(i).getIdContract());
                                            }

                                            Util.Log.ih("distance = " + distance);
                                            Util.Log.ih("contract: " + clientsList.get(i).getIdContract());
                                        } catch (Exception e) {
                                        }
                                    }

                                    //shows clients on screen
                                    if (nearbyClients.size() > 0) {
                                        Toast.makeText(getContext(), "Búsqueda finalizada", Toast.LENGTH_SHORT).show();
                                        Collections.sort(nearbyClients, new CustomComparatorToSortByDistance());

                                        adapter = new ClientesRecyclerAdapter<Clients>(getContext(), nearbyClients, myLocation, ClientesAdapter.NEARBY_CLIENTS, this);
                                        drawable = getResources().getDrawable(R.drawable.btn_clientescer_pressed);
                                    } else {
                                        Toast.makeText(getContext(), "Sin clientes a " + radio + " m de radio", Toast.LENGTH_SHORT).show();
                                        drawable = getResources().getDrawable(R.drawable.btn_clientescerc);
                                        Util.Log.ih("filteredClients.size = " + clientsList.size());
                                        adapter = getClientesAdapterClientsList();
                                        nearbyClients = null;
                                    }

                                } else { //if there's no cliente near by current location, shows wallet
                                    drawable = getResources().getDrawable(R.drawable.btn_clientescerc);
                                    adapter = getClientesAdapterClientsList();
                                    nearbyClients = null;
                                }
                                cercanos.setImageDrawable(drawable);

                                if (lvClientes != null) {
                                    lvClientes.setAdapter(adapter);
                                }

                            } else {
                                Toast.makeText(getContext(), "No cuentas con clientes a organizar", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else
                    {
                        double auxLatitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLatitude();
                        double auxLongitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLongitude();
                    }


                } else {
                    Toast.makeText(getContext(), "No se puede obtener tu ubicación", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    mainActivity.showAlertDialog("", "No se ha podido obtener el radio", true);
                } catch (Exception e1) {
                    Toast toast = Toast.makeText(getContext(), "No se ha podido obtener el radio", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });

        salir.setOnClickListener(view -> {
            Builder alert = getBuilder();
            alert.setMessage(R.string.info_message_close);
            alert.setPositiveButton(R.string.accept, (dialogInterface, which) -> cerrarSesion());
            alert.setNegativeButton(R.string.cancel, null);
            alert.show();
        });

        imageSaldoMomento.setOnClickListener(view -> mostrarSaldoAlMomento());

        refresh.setOnClickListener(view -> {
            setImageViewRefreshTouchBehaviour();
            progress = ProgressDialog.show(mainActivity, "Cargando cartera", "Por favor espera...", true);
        });

        refresh.setOnLongClickListener(view ->
        {
            Animation rotationAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_around_center_point_animation);
            refresh.startAnimation(rotationAnimation);
            mainActivity.hasActiveInternetConnection(getContext(), mainActivity.mainLayout, refresh);
            return true;
        });

        osticket.setOnClickListener(view -> showOsticketDialog());
    }

    private void showOsticketDialog()
    {

        try {

            mainActivity.runOnUiThread(() -> {
                dialog = getDialogB();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_osticket);
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            });

            if (dialog != null){
                osticketMessageEdiText = (EditText) dialog.findViewById(R.id.etOsticketMessage);
                spinnerOsticketTopics = (Spinner) dialog.findViewById(R.id.spinnerPeriods);
                String[] items = DatabaseAssistant.getAllOsticketTopics();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, items);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerOsticketTopics.setAdapter(adapter);
                spinnerOsticketTopics.setOnItemSelectedListener(Clientes.this);
                spinnerOsticketTopics.setSelection( 0 );

                tvCancelar = (TextView) dialog.findViewById(R.id.textView12);
                tvCancelar.setOnClickListener(view -> {
                    if (!mainActivity.isFinishing()) {
                        if (dialog != null && dialog.isShowing()) {
                            Util.Log.ih("dismissing progress dialog");
                            dialog.dismiss();
                            dialog = null;
                        }
                    }
                });

                tvAceptar = (TextView) dialog.findViewById(R.id.textView13);
                tvAceptar.setOnClickListener(view ->
                {
                    osticketMessage = osticketMessageEdiText.getText().toString();

                    Util.Log.ih("osticketTopicSelected = " + osticketTopicSelected);
                    Util.Log.ih("osticketTopicSelectedPosition = " + osticketTopicSelectedPosition);
                    Util.Log.ih("osticketMessage = " + osticketMessage);

                    if ( osticketMessage.length() < 5 ) {
                        Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_gotta_be_more_specific, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                    else if (!mainActivity.isFinishing()) {
                        Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                        if(myLocation!=null || myLocation.getProvider().equals("") || myLocation.getProvider() != null)
                        {
                            DatabaseAssistant.insertOsticket( osticketTopicSelected, osticketMessage, myLocation);
                        }
                        else
                        {
                            DatabaseAssistant.insertOsticket( osticketTopicSelected, osticketMessage, "0", "0");
                        }

                        executeSendOsticket();

                        if (dialog != null && dialog.isShowing()) {
                            Util.Log.ih("dismissing progress dialog");
                            dialog.dismiss();
                            dialog = null;
                        }
                        Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.info_message_osticket_sending, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            mainActivity.toastS("Hubo un error. Por favor intentalo de nuevo");
        }
    }

    /**
     * returns an instance of Dialog
     * @return instance of Dialog
     */
    private Dialog getDialogB(){
        Dialog mDialog = new Dialog(getContext());
        return mDialog;
    }

    /**
     * set notifications button behaviour
     */
    private void setImageViewNotificationsTouchBehaviour(){
        Intent intent = new Intent(getContext(), Notificaciones.class);
        try {
            intent.putExtra("id_cobrador",
                    datosCobrador.getString("no_cobrador"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        startActivity(intent);
    }

    /**
     * set deposit button behaviour
     */
    //------------------------------------Metodo que manda a llamar a Fragment deposito------------------------------------------------------------------------------------------
    private void setImageViewDepositsTouchBehaviour(){
		/*Intent intent = new Intent(getContext(), Deposito.class);
		mainActivity.isConnected = ExtendedActivity.setInternetAsReachable;
		intent.putExtra("isConnected", mainActivity.isConnected);
		intent.putExtra("datos_cobrador", datosCobrador.toString());
		startActivityForResult(intent, 2);*/

        Deposito deposito = new Deposito();
        Bundle args = new Bundle();

        args.putBoolean("isConnected", mainActivity.isConnected);
        args.putString("datos_cobrador", datosCobrador.toString());
        args.putInt("requestCode", 2);

        deposito.setArguments(args);
        deposito.setFragmentResult(this);

        getFragmentManager().beginTransaction()
                .add(R.id.root_layout, deposito, MainActivity.TAG_DEPOSITO)
                .hide(this)
                .addToBackStack("deposito")
                .commitAllowingStateLoss();

        MainActivity objeto = new MainActivity();
        objeto.bandera=1;
        //mainActivity.bandera=1;

    }

    //------------------------------------------------------------------------------------------------------------------------------

    /**
     * set 'fuera de cartera' button behaviour
     */
    private void setImageViewFueraDeCarteraTouchBehaviour(){
        //it will only start Aportacion activity if it is connected to internet
        //if (isConnected) {
        Aportacion aportacion = new Aportacion();
        Bundle args = new Bundle();

        //Intent aportacionIntent = new Intent(getContext(), Aportacion.class);
        args.putString("datosCobrador", datosCobrador.toString());
        args.putBoolean("isConnected", mainActivity.isConnected);
        args.putInt("requestCode", 3);
        //startActivityForResult(aportacionIntent, 3);
        aportacion.setArguments(args);
        aportacion.setFragmentResult(this);

        getFragmentManager().beginTransaction()
                .add(R.id.root_layout, aportacion, MainActivity.TAG_APORTACION)
                .hide(this)
                .addToBackStack("aportacion")
                .commitAllowingStateLoss();

			/*
			startActivityForResult(new Intent(
					getApplicationContext(),
					Aportacion.class).putExtra(
					"datosCobrador", datosCobrador.toString()), 3);
					*/
			/*
		} else {
			try {
				showAlertDialog(
						"",
						getString(R.string.message_trabajando_modo_offline),
						true);
			} catch (Exception e) {
				Toast toast = Toast.makeText(getApplicationContext(), R.string.message_trabajando_modo_offline, Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				e.printStackTrace();
			}
		}
		*/
    }










    /**
     * set refresh button behaviour
     */
    private void setImageViewRefreshTouchBehaviour()
    {
        try{
            showMyCustomDialog();
        } catch (Exception e){
            Toast toast = Toast.makeText(getContext(), R.string.info_message_network_offline, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            e.printStackTrace();
        }

        if (nearbyClients != null || nearbyClientsFromSyncedWallet != null) {// si el filtro de cercanos
            // esta activado, se pasa a
            // desactivar.
            Log.d("nearbyClients", "!= NULL");
            final ImageButton cercanos = (ImageButton) getView().findViewById(R.id.btn_buscar_cercanos);
            cercanos.performClick();
        } else {
            Log.d("nearbyClients", "NULL");
        }

        if (filteredClients != null) {
            filteredClients = null;
            getView().findViewById(R.id.ib_cancelar_filtro)
                    .setVisibility(View.GONE);
            ((EditText) getView().findViewById(R.id.et_filtrado))
                    .setText("");

            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(
                        ((EditText) getView().findViewById(R.id.et_filtrado))
                                .getWindowToken(), 0);
            }

            Log.d("filteredClients", " era != NULL");
        } else {
            Log.d("filteredClients", "es NULL");
        }


        DatabaseAssistant.insertNewPeriodFromScratchIfNeeded( mainActivity.getEfectivoPreference().getInt("periodo", 0) );
        if ( DatabaseAssistant.checkThatPeriodWasClosedOnLastDayOfMonth( mainActivity.getEfectivoPreference().getInt("periodo", 0) ) ) {

            List<Companies> companies = DatabaseAssistant.getCompanies();
            for (int cont = 0; cont < companies.size(); cont++) {
                Companies company = companies.get(cont);

                //DatabaseAssistant.insertCompanyAmountsByPeriodWhenMoreThanOnePeriodIsActive(company.getName(), mainActivity.getEfectivoPreference().getFloat(company.getName(), 0), mainActivity.getEfectivoPreference().getInt("periodo", 0));
                DatabaseAssistant.insertCompanyAmountsByPeriodWhenMoreThanOnePeriodIsActive(
                        company.getName(),
                        Float.parseFloat(String.valueOf(DatabaseAssistant.getTotalEfectivoAcumuladoPorEmpresa(company.getIdCompany()))),
                        mainActivity.getEfectivoPreference().getInt("periodo", 0));

                mainActivity.getEfectivoEditor().putFloat(company.getName(), 0).apply();
            }

            mainActivity.getEfectivoEditor().putInt("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0) + 1).apply();

            checkPendingPeriod();
        }



        getListClients();


    }






    public void generadorJSON()
    {
        JSONObject jsonDatosGenerales = new JSONObject();
        JSONArray arrayJSON = new JSONArray();
        JSONObject jsonDomicilio = new JSONObject();

        String aux= "SELECT * FROM ACTUALIZADOS WHERE sync <2";
        List<Actualizados> listaDatosActualizados = Actualizados.findWithQuery(Actualizados.class, aux);
        if(listaDatosActualizados.size()>0)
        {
            for(int i=0; i<listaDatosActualizados.size();i++)
            {
                try
                {
                    arrayJSON.put(i, new JSONObject(new Gson().toJson(listaDatosActualizados.get(i))));
                    //jsonDomicilio.put("actualizados", new JSONObject(new Gson().toJson(listaDatosActualizados.get(i))));
                    //arrayJSON.put(jsonDomicilio);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }
        else
        {
            Log.d("SIN DATOS --->", "NO HAY DATOS POR ACTUALZAR");
        }

        try
        {
            jsonDatosGenerales.put("general", arrayJSON);
        }catch (JSONException ex)
        {
            Log.d("SIN DATOS --->", "NO HAY DATOS POR ACTUALZAR");
        }
        String ola ="";
        cargaDeDatosAServidor(jsonDatosGenerales);

        //Util.Log.d("GENERADO------>", jsonDatosGenerales.toString());
    }



    public void cargaDeDatosAServidor(JSONObject jsonParams) //Azael--
    {

        String hola = ConstantsPabs.URL_CARGA_ACTUALIZACIONES;
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.URL_CARGA_ACTUALIZACIONES, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        try
                        {
                            String numeroContrato="", numeroContratoFail="";

                            if (!response.has("error"))
                            {

                                JSONArray jsonSuccess = response.getJSONArray("success");
                                JSONArray jsonFail = response.getJSONArray("fail");

                                if(jsonSuccess.length()>0)
                                {
                                    for(int i=0; i<=jsonSuccess.length()-1; i++)
                                    {
                                        JSONObject object = jsonSuccess.getJSONObject(i);
                                        numeroContrato = object.getString("numerocontrato");

                                        String query="UPDATE ACTUALIZADOS SET sync ='2' WHERE numerocontrato='" + numeroContrato +"'";
                                        Actualizados.executeQuery(query);
                                        String hla="";
                                    }
                                    //String hola = ConstantsPabs.URL_CARGA_ACTUALIZACIONES;
                                }

                                if (jsonFail.length()>0)
                                {

                                    for(int y=0; y<=jsonFail.length()-1;y++)
                                    {
                                        JSONObject object = jsonFail.getJSONObject(y);
                                        //JSONObject solicitud = object.getJSONObject("solicitud");
                                        numeroContratoFail = object.getString("numerocontrato");
                                        String result = object.getString("RESULTADO");
                                        Log.d("FAIL RESULT-->", result);
                                    }
                                    //String hola = ConstantsPabs.URL_CARGA_ACTUALIZACIONES;
                                }
                                else
                                {
                                        String entroAqui="";
                                }
                            }
                            else
                            {
                                String error = response.getString("error");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        Log.d("POST REQUEST-->",  new Gson().toJson(postRequest) );
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }















    /**
     * set 'cierre informativo' button behaviour
     */
    private void setImageViewCierreInformativoTouchBehaviour()
    {
        Util.Log.ih("Clientes - setInternetAsReachable = " + ExtendedActivity.setInternetAsReachable);
        CierreInformativo cierreInformativo = new CierreInformativo();
        Bundle args = new Bundle();

        try
        {
            args.putString("no_cobrador", getCierreOfDebtCollectorCode == null ? datosCobrador.getString("no_cobrador") : getCierreOfDebtCollectorCode);
            args.putString("efectivo", datosCobrador.getString("efectivo"));
            args.putString("nombre", datosCobrador.getString("nombre"));
            args.putBoolean("printCierrePerido", printCierrePerido);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        args.putInt("requestCode", 4);

        cierreInformativo.setArguments(args);
        cierreInformativo.setFragmentResult(this);

        getFragmentManager().beginTransaction()
                .add(R.id.root_layout, cierreInformativo, MainActivity.TAG_CIERRE_INFORMATIVO)
                .hide(this)
                .addToBackStack("cierre_informativo")
                .commitAllowingStateLoss();
    }

    /**
     * set 'no visitados' button behaviour
     */
    private void setImageViewNoVisitadosTouchBehaviour(){
        //if (isConnected) {

        //Toast.makeText(mainActivity, "Entro a no visitados", Toast.LENGTH_SHORT).show();

        Util.Log.ih("Clientes - setInternetAsReachable = " + ExtendedActivity.setInternetAsReachable);

        ContratosNoVisitados contratosNoVisitados = new ContratosNoVisitados();

        getFragmentManager().beginTransaction()
                .add(R.id.root_layout, contratosNoVisitados, MainActivity.TAG_CONTRATOS_NO_VISITADOS)
                .hide(this)
                .addToBackStack("contratos_no_visitados")
                .commitAllowingStateLoss();

    }

    /**
     * checks if cash is within limit cash
     * @param efectivo
     */
    private void verificarEfectivoAlerta(double efectivo) {
        try {
            double maxEfectivo = datosCobrador.getInt("max_efectivo");

            double diferencia = maxEfectivo - efectivo;
            if (diferencia <= MAX_EFECTIVO_ALERTA) {
                try {
                    mainActivity.showAlertDialog("", "Te resta $" + diferencia
                            + " para llegar al máximo de efectivo.", false);
                } catch (Exception e){
                    Toast toast = Toast.makeText(getContext(), "Te resta $" + diferencia + " para llegar al máximo de efectivo", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if there's a pending ticket to be printed
     */
    private void checkPendingTicket()
    {

        /**
         * Checks if there's a payment
         * and
         * checks if payment was made and printed (is synchronized)
         * receives false: is not synchronized
         * receives true: is synchronized
         */
        if ( !preferencesPendingTicket.isThereAPaymentAndIsSynchronized() )
        {

            /**
             * Checks if payment was made and printed (is synchronized)
             * receives false: is not synchronized
             * receives true: is synchronized
             */
            //if (!preferencesPendingTicket.isSynchronized()) {

            /**
             * Gets folio from last payment registered
             * and shows it
             */
            JSONObject lastPayment = preferencesPendingTicket.getPayment();

            try {

                if (pendingTicketAlert != null && pendingTicketAlert.isShowing())
                    pendingTicketAlert.dismiss();

                pendingTicketAlert = getAlertDialogBuilder()
                        .setTitle(R.string.info_title_pending)
                        .setMessage(getString(R.string.info_message_pending1, lastPayment.getString(getString(R.string.key_folio)), lastPayment.getString(getString(R.string.key_contract_number))))
                        .setCancelable(false)
                        .setNeutralButton(R.string.ok, null)
                        .create();

                pendingTicketAlert.show();

            } catch (JSONException e){
                e.printStackTrace();
            }
            //}
        }
    }

    private void checkPendingPeriod() {
        if ( DatabaseAssistant.isThereMoreThanOnePeriodActive() ) {
            try {

                if (pendingTicketAlert != null && pendingTicketAlert.isShowing())
                    pendingTicketAlert.dismiss();

                pendingTicketAlert = getAlertDialogBuilder()
                        .setTitle(R.string.info_title_pending_period)
                        .setMessage(
                                getString(R.string.info_message_pending_period)
                        )
                        .setCancelable(false)
                        .setNeutralButton(R.string.ok, null)
                        .create();

                pendingTicketAlert.show();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void setNull(){
        filteredClients = null;

        arrayCercanos = null;
        nearbyClientsFromSyncedWallet = null;
        nearbyClients = null;
    }

    //endregion

    //region Protected

    //endregion

    //region Getters

    /**
     * Gets context from this class.
     * In some cases you cannot just call 'this', because another context is envolved
     * or you want a reference to an object of this class from another class,
     * then this context is helpful.
     *
     * NOTE
     * Not recommended, it may cause NullPointerException though
     *
     * @return context from Clientes
     */
    public static Clientes getMthis() {
        return mthis;
    }

    /**
     * Provides an instance of ClientesAdapter
     * using filteredClients variable
     * @return ClientesAdapter's instance
     */
    /*private ClientesAdapter getClientesAdapterFilteredClients(){
        //return new ClientesAdapter(this, arrayFiltrados);
        return new ClientesAdapter<Clients>(getContext(), filteredClients, ClientesAdapter.FILTERED_CLIENTS);
    }*/

    private ClientesRecyclerAdapter getClientesAdapterFilteredClients(){
        //return new ClientesAdapter(this, arrayFiltrados);
        return new ClientesRecyclerAdapter<Clients>(getContext(), filteredClients, ClientesAdapter.FILTERED_CLIENTS, this);
    }

    /**
     * Provides an instance of ClientesAdapter
     * using clientsList variable
     * @return ClientesAdapter's instance
     */
    /*private ClientesAdapter getClientesAdapterClientsList(){
        return new ClientesAdapter<Clients>(mainActivity.getApplicationContext(), clientsList, ClientesAdapter.WALLET_CLIENTS);
    }

    private ClientesAdapter getAdapterSyncedWalletClients(){
        //clientListFromSyncedWalletForAdapter = new ArrayList<>();
        setNull();
        return new ClientesAdapter<SyncedWallet>(mainActivity.getApplicationContext(), clientListFromSyncedWallet, ClientesAdapter.DAY_WALLET_CLIENTS);
    }*/

    private ClientesRecyclerAdapter getClientesAdapterClientsList()
    {
        setNull();
        return new ClientesRecyclerAdapter<Clients>(mainActivity.getApplicationContext(), clientsList, ClientesRecyclerAdapter.WALLET_CLIENTS, this);
    }

    private ClientesRecyclerAdapter getAdapterSyncedWalletClients()
    {
        setNull();
        return new ClientesRecyclerAdapter<SyncedWallet>(mainActivity.getApplicationContext(), clientListFromSyncedWallet, ClientesRecyclerAdapter.DAY_WALLET_CLIENTS, this);
    }

    /**
     * returns an instance of Dialog
     * @return instance of Dialog
     */
    private Dialog getDialog(){
        return new Dialog(getContext());
    }

    /**
     * returns an instance of Builder
     * @return instance of Builder
     */
    private Builder getBuilder(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            return new Builder(getContext(), android.R.style.Theme_Material_Light_Dialog_Alert);
        }
        else{
            return new Builder(getContext());
        }
    }

    /**
     * returns an instance of AlertDialogBuilder
     * @return instance of AlertDialogBuilder
     */
    private AlertDialog.Builder getAlertDialogBuilder()
    {
        return new AlertDialog.Builder(getContext());
    }

    /**
     * returns an instance of ProgressDialog
     * @return instance of ProgressDialog
     */
    private ProgressDialog getProgressDialog(){
        return new ProgressDialog(getContext());
    }

    //endregion

    //region Setters

    /**
     * Sets 'mthis' global variable as Clientes's context
     * @param mthis Clientes's context
     */
    public static void setMthis(Clientes mthis) {
        Clientes.mthis = mthis;
    }

    //endregion

    //region Interfaces

    @Override
    public void onGetClientsFromSyncedWallet(List<SyncedWallet> clientListFromSyncedWallet) {
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        this.clientListFromSyncedWallet = clientListFromSyncedWallet;
        if (firstLoad) {
            this.actualizarListView();
            firstLoad = false;
        }
        updateListView();
    }

    //region CerrarSesionTimeOut

    @Override
    public void cerrarSesionTimeOut() {
        if (mainActivity.handlerSesion != null) {
            mainActivity.handlerSesion.removeCallbacks(mainActivity.myRunnable);
            mainActivity.handlerSesion = null;
        }

        View refBuscarClientes = null;
        if (BuscarCliente.getMthis() != null) {
            refBuscarClientes = BuscarCliente.getMthis().findViewById(
                    R.id.lv_buscar_clientes);
        }

        View refAportacion = null;
        if (Aportacion.getMthis() != null) {
            refAportacion = Aportacion.getMthis().getView().findViewById(
                    R.id.et_nombre_apellidos);
        }

        View refCierre = null;
        if (Cierre.getMthis() != null) {
            refCierre = Cierre.getMthis().getView().findViewById(R.id.lv_cierre);
        }

        View refDeposito = null;
        if (Deposito.getMthis() != null) {
            refDeposito = Deposito.getMthis().getView().findViewById(R.id.tv_banco);
        }

        View refDetalleCliente = null;
        if (ClienteDetalle.getMthis() != null) {
            refDetalleCliente = ClienteDetalle.getMthis().getView().findViewById(
                    R.id.et_cantidad_aportacion);
        }

        View refNotificaciones = null;
        if (Notificaciones.getMthis() != null) {
            refNotificaciones = Notificaciones.getMthis().findViewById(
                    R.id.lv_notification);
        }

        if (refBuscarClientes != null) {
            BuscarCliente.getMthis().finish();
        } else if (refAportacion != null) {
            Aportacion.getMthis().getActivity().finish();
        }

        if (refCierre != null) {
            Cierre.getMthis().getActivity().finish();
        }

        if (refDeposito != null) {
            Deposito.getMthis().getActivity().finish();
        }

        if (refDetalleCliente != null) {
            ClienteDetalle.getMthis().getActivity().finish();
        }

        if (refNotificaciones != null) {
            Notificaciones.getMthis().finish();
        }
        Util.Log.ih("SESION CERRADA");

        cerrarSesion();

    }

    //endregion

    //region OnItemSelected

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
        osticketTopicSelected = adapterView.getItemAtPosition(pos).toString();
        osticketTopicSelectedPosition = pos;
    }

    //endregion

    //region OnNothingSelected

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    //endregion

    //region OnItemClick

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
    {

        Toast.makeText(mainActivity, "Cliente presionado", Toast.LENGTH_SHORT).show();
        this.position = position;
        //this.listViewState = lvClientes.onSaveInstanceState();
        JSONObject jsonTmp = null;

        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(
                ((EditText) getView().findViewById(R.id.et_filtrado))
                        .getWindowToken(), 0);

        try {

            Gson gson = new Gson();

            if (filteredClients != null && filteredClients.size() > 0) {
                //jsonTmp = arrayFiltrados.getJSONObject(position);
                Util.Log.ih( "gsonToJson = " + new JSONObject( gson.toJson( filteredClients.get( position ) ) ) );
                jsonTmp = new JSONObject( gson.toJson( filteredClients.get( position ) ) );
            } else if (nearbyClients != null || nearbyClientsFromSyncedWallet != null) {
                //jsonTmp = arrayCercanos.get(position);
                if (BuildConfig.isWalletSynchronizationEnabled()){
                    //jsonTmp = new JSONObject( gson.toJson( nearbyClientsFromSyncedWallet.get(position) ) );
                    Clients client = DatabaseAssistant.getSingleClient(nearbyClientsFromSyncedWallet.get(position).getIdContract());
                    if (client != null) {
                        jsonTmp = new JSONObject(gson.toJson(client));
                    }
                    else {
                        throw new NullPointerException();
                    }
                }
                else {
                    jsonTmp = new JSONObject( gson.toJson( nearbyClients.get(position) ) );
                }
            } else {
                //jsonTmp = arrayClientes.getJSONObject(position);
                if (BuildConfig.isWalletSynchronizationEnabled()){
                    //jsonTmp = new JSONObject(gson.toJson(clientListFromSyncedWallet.get(position).getClient()));
                    Clients client = DatabaseAssistant.getSingleClient( clientListFromSyncedWallet.get(position).getContractID() );
                    if (client != null) {
                        jsonTmp = new JSONObject(gson.toJson(client));
                    }
                    else {
                        throw new NullPointerException();
                    }
                }
                else {
                    jsonTmp = new JSONObject(gson.toJson(clientsList.get(position)));
                }
            }
            //Log.d("Cliente", jsonTmp.toString(1));
            Util.Log.ih("jsonTmp = " + jsonTmp.toString());

            ClienteDetalle clienteDetalle = new ClienteDetalle();

            Bundle args = new Bundle();
            //Intent intent = new Intent(getContext(), ClienteDetalle.class);
            args.putString("datos", jsonTmp.toString());
            args.putString("datoscobrador", datosCobrador.toString());
            mainActivity.isConnected = false;
            args.putBoolean("isConnected", mainActivity.isConnected);
            args.putString("callback", this.toString());
            args.putInt("requestCode", 1);
            //startActivityForResult(intent, 1);
            clienteDetalle.setArguments(args);
            clienteDetalle.setFragmentResult(this);

            getFragmentManager().beginTransaction()
                    .add(R.id.root_layout, clienteDetalle, MainActivity.TAG_CLIENTE_DETALLES)
                    .hide(this)
                    .addToBackStack("cliente_detalle")
                    .commitAllowingStateLoss();

            Toast.makeText(mainActivity, "Cliente presionado", Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            mainActivity.toastS(R.string.error_message_call);
            e.printStackTrace();
        } catch (NullPointerException e){
            mainActivity.toastS(R.string.error_message_call);
            e.printStackTrace();
        }
    }

    //endregion

    //region OnActivityResult


    @Override
    public void onFragmentResult(int requestCode, int resultCode, Intent data) {
        //updates cash
        /**
         * came back from ClienteDetalle
         */
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String maxEfec = "";
                try {
                    maxEfec = datosCobrador.getString("max_efectivo");
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                boolean paymentDone = data.getBooleanExtra("paymentDone", false);


                if (paymentDone) {

                    //after making a payment
                    //move contract selected to last  position
                    //(bottom of screen)
                    //and give it base color
                    if (BuildConfig.isWalletSynchronizationEnabled()){

                        //if (filteredClients == null && (nearbyClients == null || nearbyClientsFromSyncedWallet == null)){
                        if (clientListFromSyncedWallet != null && clientListFromSyncedWallet.size() > 0){

                            if (filteredClients != null && filteredClients.size() > 0){
                                String contractIDFromFilteredClient = filteredClients.get(position).getIdContract();
                                for (int i = 0; i < clientListFromSyncedWallet.size(); i++){
                                    if (clientListFromSyncedWallet.get(i).getContractID().equals(contractIDFromFilteredClient)){
                                        updateClientList(i, false);
                                        break;
                                    }
                                }
                            } else if (nearbyClientsFromSyncedWallet != null){
                                String contractIDFromFilteredClient = nearbyClientsFromSyncedWallet.get(position).getIdContract();
                                for (int i = 0; i < clientListFromSyncedWallet.size(); i++){
                                    if (clientListFromSyncedWallet.get(i).getContractID().equals(contractIDFromFilteredClient)){
                                        updateClientList(i, false);
                                        break;
                                    }
                                }
                            } else {
                                updateClientList(position, false);
                                updateListView();
                            }
                        }
                    }


                    try {
                        // datosCobrador.put("efectivo",
                        // data.getStringExtra("nuevo_efectivo"));
                        modificarInfoToLoginAndSplash();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (mainActivity.getTotalEfectivo() > Double.parseDouble(maxEfec)) {

                        modificarInfoToLoginAndSplash();
                        cerrarTodoYMandarBloqueo();
                        // if (!isConnected) {
                        // modificarInfoToLoginAndSplash();
                        // alertaDeCobrosOffline();
                        //
                        // } else {
                        // mandarABloqueo = false;
                        // cerrarTodoYMandarBloqueo();
                        // }
                    } else {
                        try {
                            // double efectivo = Double.parseDouble(data
                            // .getStringExtra("nuevo_efectivo"));
                            calcularTotal();
                            verificarEfectivoAlerta(mainActivity.getTotalEfectivo());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    //registerPagos();
                } else {
                    //after making a visit
                    //increase visit counter
                    //and if visit counter if greater or equal to 2
                    //set not to show color
                    //and move to last position (bottom of screen)
                    //if (filteredClients == null && (nearbyClients == null || nearbyClientsFromSyncedWallet == null)) {
                    if (clientListFromSyncedWallet != null && clientListFromSyncedWallet.size() > 0) {

                        if (filteredClients != null && filteredClients.size() > 0){
                            String contractIDFromFilteredClient = filteredClients.get(position).getIdContract();
                            for (int i = 0; i < clientListFromSyncedWallet.size(); i++){
                                if (clientListFromSyncedWallet.get(i).getContractID().equals(contractIDFromFilteredClient)){
                                    updateClientList(i, true);
                                    break;
                                }
                            }
                        } else if (nearbyClientsFromSyncedWallet != null){
                            String contractIDFromFilteredClient = nearbyClientsFromSyncedWallet.get(position).getIdContract();
                            for (int i = 0; i < clientListFromSyncedWallet.size(); i++){
                                if (clientListFromSyncedWallet.get(i).getContractID().equals(contractIDFromFilteredClient)){
                                    updateClientList(i, true);
                                    break;
                                }
                            }
                        } else {
                            updateClientList(position, true);
                            updateListView();
                        }
							/*
								SyncedWallet sw = clientListFromSyncedWallet.get(position);
								sw.setVisitCounter(sw.getVisitCounter() + 1);

								if (sw.getVisitCounter() >= 2) {
									sw.setShowColor(false);
									sw.setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);

									clientListFromSyncedWallet.remove(position);

									for (int i = clientListFromSyncedWallet.size() - 1; i >= 0; i--) {
										if (!clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT)) {
											clientListFromSyncedWallet.add(i + 1, sw);
											break;
										}
									}

									//clientListFromSyncedWallet.add(sw);

									setNull();

									adapter = getAdapterSyncedWalletClients();
									lvClientes = (ListView) findViewById(R.id.listView_clientes);
									lvClientes.setAdapter(adapter);
									if (listViewState != null){
										Util.Log.ih("trying to restore ListView state...");
										lvClientes.onRestoreInstanceState(listViewState);
									}
									lvClientes.setDivider(getResources().getDrawable(R.drawable.divider_list_clientes));
									lvClientes.setOnItemClickListener(this);
								}
							*/
                        //}
                    }
                    //}
                }

                if (BuildConfig.isAutoPrintingEnable()){
                    randomListClick();
                }

                registerPagos();
            }
        }
        /**
         * came back from Deposito
         */
        else if (requestCode == 2) {

            //NEW PROCESS
            if ( DatabaseAssistant.isThereMoreThanOnePeriodActive() ) {
                try {
                    SharedPreferences.Editor efectivoEditor = mainActivity.getEfectivoEditor();

                    int lastPeriod = DatabaseAssistant.getAllPeriodsActive()
                            .get(DatabaseAssistant.getNumberOfPeriodsActive() - 1)
                            .getPeriodNumber();
                    List<CompanyAmounts> companyAmounts = DatabaseAssistant.getCompanyAmountWhenMoreThanOnePeriodIsActive(
                            lastPeriod
                    );
                    mainActivity.getEfectivoEditor().putInt(
                            "periodo",
                            lastPeriod
                    ).apply();
                    for (CompanyAmounts ca : companyAmounts) {
                        efectivoEditor.putFloat(ca.getCompanyName(), ca.getAmount()).apply();
                    }

                    Util.Log.ih("onItemSelected end");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            if (resultCode == Activity.RESULT_OK) {
                String maxEfec = "";
                try {
                    maxEfec = datosCobrador.getString("max_efectivo");
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                modificarInfoToLoginAndSplash();
                //} catch (JSONException e) {
                //	e.printStackTrace();
                //}
                if (mainActivity.getTotalEfectivo() > Double.parseDouble(maxEfec)) {
                    modificarInfoToLoginAndSplash();
                    cerrarTodoYMandarBloqueo();
                    // if (!isConnected) {
                    // modificarInfoToLoginAndSplash();
                    // alertaDeCobrosOffline();
                    // } else {
                    // mandarABloqueo = false;
                    // cerrarTodoYMandarBloqueo();
                    // }
                } else {
                    try {
                        //double efectivo = Double.parseDouble(data
                        //		.getStringExtra("nuevo_efectivo"));
                        verificarEfectivoAlerta(mainActivity.getTotalEfectivo());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        /**
         * came back from Aportacion
         */
        else if (requestCode == 3) {
            if (resultCode == Activity.RESULT_OK) {
                String maxEfec = "";
                try {
                    maxEfec = datosCobrador.getString("max_efectivo");
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                try {
                    datosCobrador.put("efectivo",
                            data.getStringExtra("nuevo_efectivo"));
                    modificarInfoToLoginAndSplash();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mainActivity.getTotalEfectivo() > Double.parseDouble(maxEfec)) {
                    modificarInfoToLoginAndSplash();
                    cerrarTodoYMandarBloqueo();
                    // if (!isConnected) {
                    // modificarInfoToLoginAndSplash();
                    // alertaDeCobrosOffline();
                    // } else {
                    // mandarABloqueo = false;
                    // cerrarTodoYMandarBloqueo();
                    // }
                } else {
                    try {
                        double efectivo = Double.parseDouble(data
                                .getStringExtra("nuevo_efectivo"));
                        verificarEfectivoAlerta(mainActivity.getTotalEfectivo());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                registerPagos();
            }
        }
        /**
         * came back from CierreInformativo
         */
        else if (requestCode == 4) {

            if ( DatabaseAssistant.isThereMoreThanOnePeriodActive() ) {
                List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();
                mainActivity.getEfectivoEditor().putInt("periodo", periods.get( periods.size()-1 ).getPeriodNumber()).apply();
            }

            Util.Log.ih("came back from CierreInformativo");
            if (resultCode == Activity.RESULT_OK) {
                cerrarSesion();
            }
            else if (resultCode == 99) {
                Util.Log.ih("RESULT_FIRST_USER");

                showMyCustomDialog();

                if (BuildConfig.isWalletSynchronizationEnabled()) {

                    Util.Log.ih("isWalletSynchronizationEnabled");

                    Thread thread = new Thread(() -> {
                        clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();

                        mainActivity.runOnUiThread(() -> {
                            //clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
                            //clientsList = DatabaseAssistant.getAllClients();
                            adapter = getAdapterSyncedWalletClients();
                            //lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);
                            if (lvClientes != null) {
                                lvClientes.setAdapter(adapter);
                                /*lvClientes.setDivider(getResources().getDrawable(R.drawable.divider_list_clientes));
                                lvClientes.setOnItemClickListener(mthis);*/
                            }

                            dismissMyCustomDialog();
                        });
                    });
                    thread.start();

					/*
					clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
					//clientsList = DatabaseAssistant.getAllClients();
					adapter = getAdapterSyncedWalletClients();
					lvClientes = (ListView) findViewById(R.id.listView_clientes);
					lvClientes.setAdapter(adapter);
					lvClientes.setDivider(getResources().getDrawable(R.drawable.divider_list_clientes));
					lvClientes.setOnItemClickListener(mthis);
					*/
                }
                else {
                    Util.Log.ih("isWalletSynchronizationEnabled - NO");
                    //clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
                    clientsList = DatabaseAssistant.getAllClients();
                    adapter = getClientesAdapterClientsList();
                    //lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);
                    if (lvClientes != null) {
                        lvClientes.setAdapter(adapter);
                        /*lvClientes.setDivider(getResources().getDrawable(R.drawable.divider_list_clientes));
                        lvClientes.setOnItemClickListener(mthis);*/
                    }

                    dismissMyCustomDialog();
                }
            }
        }

    }

    @Override
    public void onItemClick(Clients item, int position) { //onClick de la lista de clientes cuando buscas en el editText
        try {
            if(isAirPlaneModeActive())
            {


            }
            else
            {
                this.position = position;

                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(
                        ((EditText) getView().findViewById(R.id.et_filtrado))
                                .getWindowToken(), 0);

                JSONObject jsonTmp;

                Gson gson = new Gson();

                jsonTmp = new JSONObject(gson.toJson(item));

                Util.Log.ih("jsonTmp = " + jsonTmp.toString());

                ClienteDetalle clienteDetalle = new ClienteDetalle();

                Bundle args = new Bundle();
                //Intent intent = new Intent(getContext(), ClienteDetalle.class);
                args.putString("datos", jsonTmp.toString());
                args.putString("datoscobrador", datosCobrador.toString());
                mainActivity.isConnected = false;
                args.putBoolean("isConnected", mainActivity.isConnected);
                args.putString("callback", this.toString());
                args.putInt("requestCode", 1);
                //startActivityForResult(intent, 1);
                clienteDetalle.setArguments(args);
                clienteDetalle.setFragmentResult(this);

                getFragmentManager().beginTransaction()
                        .add(R.id.root_layout, clienteDetalle, MainActivity.TAG_CLIENTE_DETALLES)
                        .hide(this)
                        .addToBackStack("cliente_detalle")
                        .commitAllowingStateLoss();
            }


        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void onItemClick(SyncedWallet item, int position) { //onClick de item de lista de clientes
        try {

            if(isAirPlaneModeActive()) {

            }
            else
            {
                this.position = position;

                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(
                        ((EditText) getView().findViewById(R.id.et_filtrado))
                                .getWindowToken(), 0);

                JSONObject jsonTmp;
                Gson gson = new Gson();
                Clients client = DatabaseAssistant.getSingleClient(item.getContractID());
                jsonTmp = new JSONObject(gson.toJson(client));
                Util.Log.ih("jsonTmp = " + jsonTmp.toString());
                ClienteDetalle clienteDetalle = new ClienteDetalle();

                Bundle args = new Bundle();
                //Intent intent = new Intent(getContext(), ClienteDetalle.class);
                args.putString("datos", jsonTmp.toString());
                args.putString("datoscobrador", datosCobrador.toString());
                mainActivity.isConnected = false;
                args.putBoolean("isConnected", mainActivity.isConnected);
                args.putString("callback", this.toString());
                args.putInt("requestCode", 1);
                //startActivityForResult(intent, 1);
                clienteDetalle.setArguments(args);
                clienteDetalle.setFragmentResult(this);

                getFragmentManager().beginTransaction()
                        .add(R.id.root_layout, clienteDetalle, MainActivity.TAG_CLIENTE_DETALLES)
                        .hide(this)
                        .addToBackStack("cliente_detalle")
                        .commitAllowingStateLoss();


            }

        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    private static class CustomLinearLayoutManager extends LinearLayoutManager {

        private CustomLinearLayoutManager(Context context)
        {
            super(context);
        }

        public CustomLinearLayoutManager(Context context, int orientation, boolean reverseLayout)
        {
            super(context, orientation, reverseLayout);
        }

        public CustomLinearLayoutManager(Context context, AttributeSet attrs, int defSyleAttr, int defStyleRes)
        {
            super(context, attrs, defSyleAttr, defStyleRes);
        }

        @Override
        public boolean supportsPredictiveItemAnimations() {
            return false;
        }
    }

    //endregion

    //region OnKeyDown

	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (goBack) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				moveTaskToBack(true);
			}
			return super.onKeyDown(keyCode, event);
		}
		return false;
	}*/

    //endregion

    //endregion

    //endregion
    public void metodo(String arg0)
    {
        try {
            LogRegister.registrarCheat(arg0.toString());
            String response = DatabaseAssistant.updateAllPaymentAsNotSyncedToSendAgain();
            //mainActivity.toastL(response ? "Se modificó los recibos. Ingresa al cierre informativo." : "No se encontró recibos");
            mainActivity.runOnUiThread(() -> {
                try {
                    //dismissMyCustomDialog();
                    mainActivity.showAlertDialog("Folios",
                            response, true);
                } catch (Exception ex) {
                    Toast toast = Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            });

            printCierrePerido = !printCierrePerido;
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
        DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
        executeSendOsticket();
    }

    //lanzar el customDialog------------------------------------------------------------------------
    public void pedirContraseña(View view)
    {
        TextView tvAceptar, tvCancelar;
        EditText etPassword;
        myDialog.setContentView(R.layout.dialog_password);
        myDialog.setCancelable(false);
        tvAceptar=(TextView) myDialog.findViewById(R.id.tvAccept);
        tvCancelar=(TextView) myDialog.findViewById(R.id.tvCancel);
        etPassword =(EditText)myDialog.findViewById(R.id.etPassword);

        tvAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(view != null || myDialog !=null)
                {
                    if(etPassword.getText().toString().equals("@@@"))
                    {
                        showPopup(view);
                        myDialog.dismiss();
                    }
                    else
                    {
                        mainActivity.toastL("Contraseña incorrecta.");
                        myDialog.dismiss();
                    }
                    myDialog.dismiss();
                }


            }
        });

        tvCancelar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        try
        {
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            myDialog.show();
        }catch (Exception ex)
        {
            Log.d("EXCEPCION -- >", "Error: " + ex.toString());
        }



    }
    //----------------------------------------------------------------------------------------------

    public void showPopup(View view)
    {
        Switch btSemaforo, btInternet;
        Spinner spBranch;
        Button btOKBranch, btOKPeriodo, btOKIP, btExportar, btImportar, btBorrar, btOKNuevoPeriodo, btOKCerrarPeriodo, btPagosTXT, btEnviarPagos, btEliminarPagos, btOKComando;
        ImageView btCerrarVentana;
        EditText etCambiarPeriodo, etCambiarIP, etNuevoPeriodo, etCerrarPeriodo, etComandoImpresora;

        myDialog2.setContentView(R.layout.preview_configuracion);
        myDialog2.setCancelable(false);

        btSemaforo=(Switch)myDialog2.findViewById(R.id.btSemaforo);
        btInternet=(Switch)myDialog2.findViewById(R.id.btInternet);
        spBranch=(Spinner)myDialog2.findViewById(R.id.spBranch);
        btCerrarVentana=(ImageView)myDialog2.findViewById(R.id.btCerrar);
        btOKBranch=(Button)myDialog2.findViewById(R.id.btOKBranch);
        btOKPeriodo=(Button)myDialog2.findViewById(R.id.btOKPeriodo);
        btOKIP=(Button)myDialog2.findViewById(R.id.btOKIP);
        btExportar=(Button)myDialog2.findViewById(R.id.btExportar);
        btImportar=(Button)myDialog2.findViewById(R.id.btImportar);
        btBorrar=(Button)myDialog2.findViewById(R.id.btBorrar);
        btOKNuevoPeriodo=(Button)myDialog2.findViewById(R.id.btOkNuevoPeriodo);
        btOKCerrarPeriodo=(Button)myDialog2.findViewById(R.id.btOKCerrarPeriodo);
        btPagosTXT=(Button)myDialog2.findViewById(R.id.btPagoTXT);
        btEnviarPagos=(Button)myDialog2.findViewById(R.id.btEnviarPagos);
        btEliminarPagos=(Button)myDialog2.findViewById(R.id.btEliminarPagos);
        btOKComando=(Button)myDialog2.findViewById(R.id.btOKComando);

        etCambiarPeriodo=(EditText)myDialog2.findViewById(R.id.etCambiarPeriodo);
        etCambiarIP=(EditText)myDialog2.findViewById(R.id.eCambiarIP);
        etNuevoPeriodo=(EditText)myDialog2.findViewById(R.id.etNuevoPeriodo);
        etCerrarPeriodo=(EditText)myDialog2.findViewById(R.id.etCerrarPeriodo);
        etComandoImpresora=(EditText)myDialog2.findViewById(R.id.etComandoImpresora);

        PreferenceSettings objeto = new PreferenceSettings(ApplicationResourcesProvider.getContext());
        extended = new ExtendedActivity();
        if(objeto.isColorContractsActive())
            btSemaforo.setChecked(true);
        else
            btSemaforo.setChecked(false);


       // if(mainActivity.setInternetAsReachable) {
         //   btInternet.setChecked(true);
       // }
       //else
         //   btInternet.setChecked(false);

        if(mainActivity.setInternetAsReachable && checkInternetConnection() ||
                ExtendedActivity.setInternetAsReachable && checkInternetConnection()) {
            btInternet.setChecked(true);
        }
        else
            btInternet.setChecked(false);

       //-----------Escuchadores de botones----------------------------------------------

        btCerrarVentana.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog2.dismiss();
            }
        });

        btSemaforo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(buttonView != null)
                {
                    if (isChecked)
                    {
                        LogRegister.registrarCheat("turnColors%ON");
                        PreferenceSettings preferenceSettings = new PreferenceSettings(ApplicationResourcesProvider.getContext());
                        preferenceSettings.turnColorContracts(true);
                        //Toast.makeText(Configuracion.this, "Semáforo activado", Toast.LENGTH_LONG).show();
                        mainActivity.toastL("Semaforo activado");
                    }
                    else
                    {
                        LogRegister.registrarCheat("turnColors%OFF");
                        PreferenceSettings preferenceSettings = new PreferenceSettings(ApplicationResourcesProvider.getContext());
                        preferenceSettings.turnColorContracts(false);
                        //Toast.makeText(Configuracion.this, "Semáforo desactivado", Toast.LENGTH_LONG).show();
                        mainActivity.toastL("Semaforo desactivado");
                    }
                }
            }
        });

        btInternet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if (isChecked)
                {
                    LogRegister.registrarCheat("internet%");
                    ExtendedActivity.setInternetAsReachable = !ExtendedActivity.setInternetAsReachable;
                    mainActivity.toastL("Internet " + (ExtendedActivity.setInternetAsReachable ? "siempre activo" : ""));
                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                    DatabaseAssistant.insertOsticket("Cheat", "internet%", myLocation);
                    executeSendOsticket();
                }
                else
                {
                    LogRegister.registrarCheat("internet%");
                    ExtendedActivity.setInternetAsReachable = !ExtendedActivity.setInternetAsReachable;
                    mainActivity.toastL("Internet " + (ExtendedActivity.setInternetAsReachable ? "con ping habilitado" : ""));
                    Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                    DatabaseAssistant.insertOsticket("Cheat", "internet%", myLocation);
                    executeSendOsticket();
                }
            }
        });

        btOKPeriodo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(etCambiarPeriodo.getText().toString().equals("") || etCambiarPeriodo.getText().toString().isEmpty())
                {
                    mainActivity.toastL("Porfavor ingresa el periodo a cambiar");
                }
                else
                {
                    try
                    {
                        String period = etCambiarPeriodo.getText().toString();
                        Util.Log.ih("periodo obtenido = " + period);
                        //mainActivity.toastL("Period: "+period);
                        String previoudPeriod = Integer.toString(mainActivity.getEfectivoPreference().getInt("periodo", 0));
                        //LogRegister.registrarCheat(arg0.toString() + " Periodo anterior: " + previoudPeriod);
                        LogRegister.registrarCheat(etCambiarPeriodo.getText().toString() + " Periodo anterior: " + previoudPeriod);
                        mainActivity.getEfectivoEditor().putInt("periodo", Integer.parseInt(period)).apply();
                        DatabaseAssistant.eraseDatabaseAfterChangingPeriod();
                        mainActivity.toastL("El periodo ha cambiado a: " + period + "; periodo anterior: " + previoudPeriod);
                        printCierrePerido = !printCierrePerido;
                        Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                        //DatabaseAssistant.insertOsticket("Cheat", arg0.toString(), myLocation);
                        DatabaseAssistant.insertOsticket("Cheat", etCambiarPeriodo.getText().toString(), myLocation);
                        executeSendOsticket();
                        etCambiarPeriodo.setText("");
                    } catch (StringIndexOutOfBoundsException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        btOKIP.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try {
                    String IP = etCambiarIP.getText().toString();
                    //String IP = arg0.toString().substring(arg0.toString().indexOf("%") + 1, arg0.toString().lastIndexOf("%"));
                    LogRegister.registrarCheat("changeServerIP%");
                    Util.Log.ih("IP obtenida = " + IP);

                    Preferences preferencesServerIP = new Preferences(ApplicationResourcesProvider.getContext());
                    String oldServerIP = preferencesServerIP.getServerIP();

                    preferencesServerIP.changeServerIP(IP);
                    mainActivity.toastL("La IP a cambiado a: " + IP + "\nIP anterior: " + oldServerIP);
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "changeServerIP%", myLocation);
                executeSendOsticket();
                etCambiarIP.setText("");
            }
        });

        btExportar.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ExportImportDB exportImportDB = new ExportImportDB();
                LogRegister.registrarCheat("exportDB%");
                try {
                    if (exportImportDB.exportDB()) {
                        mainActivity.toastL("Base de datos exportada correctamente");
                    } else {
                        mainActivity.toastL("Hubo un error al exportar la base de datos.");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "exportDB%", myLocation);
                executeSendOsticket();
            }
        });

        btImportar.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                LogRegister.registrarCheat("importDB%");
                ExportImportDB exportImportDB = new ExportImportDB();
                try {
                    if (exportImportDB.importDB()) {
                        mainActivity.toastL("Base de datos importada correctamente");
                    } else {
                        mainActivity.toastL("Hubo un error al importar la base de datos.");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "importDB%", myLocation);
                executeSendOsticket();
            }
        });

        btBorrar.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Lanzar dialogo para mandarte al login y mandar el comando.
                mandarAlerta(v);
            }
        });

        btOKNuevoPeriodo.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try {
                    String period = etNuevoPeriodo.getText().toString();
                    LogRegister.registrarCheat("addNewPeriod%");
                    Util.Log.ih("periodo obtenido = " + period);
                    DatabaseAssistant.insertNewPeriodIfNeeded(Integer.parseInt(period));
                    mainActivity.toastL(DatabaseAssistant.checkLastThreePeriodsStatus());
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "addNewPeriod%", myLocation);
                executeSendOsticket();
                etNuevoPeriodo.setText("");
            }
        });

        btOKCerrarPeriodo.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try {
                    String period = etCerrarPeriodo.getText().toString();
                    //String period = arg0.toString().substring(arg0.toString().indexOf("%") + 1, arg0.toString().lastIndexOf("%"));
                    LogRegister.registrarCheat("closePeriod%");
                    Util.Log.ih("periodo obtenido = " + period);
                    DatabaseAssistant.closePeriod(period);
                    mainActivity.toastL(DatabaseAssistant.checkLastThreePeriodsStatus());
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "closePeriod%", myLocation);
                executeSendOsticket();
                etCerrarPeriodo.setText("");
            }
        });

        btPagosTXT.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                LogRegister.registrarCheat("sendPaymentstxt%");
                registerPagostxt();
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "sendPaymentstxt%", myLocation);
                executeSendOsticket();
            }
        });

        btEnviarPagos.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try {
                    LogRegister.registrarCheat("resendAllPayments%");
                    String response = DatabaseAssistant.updateAllPaymentAsNotSyncedToSendAgain();
                    mainActivity.runOnUiThread(() -> {
                        try {
                            //dismissMyCustomDialog();
                            mainActivity.showAlertDialog("Folios",
                                    response, true);
                        } catch (Exception ex) {
                            Toast toast = Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    });

                    printCierrePerido = !printCierrePerido;
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "resendAllPayments%", myLocation);
                executeSendOsticket();
            }
        });

        btEliminarPagos.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                LogRegister.registrarCheat("deleteAllPayments%");
                DatabaseAssistant.deleteAllPayments();
                mainActivity.toastL("Eliminando todos los cobros");
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "deleteAllPayments%", myLocation);
                executeSendOsticket();
            }
        });

        btOKComando.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try {

                    //String command = arg0.toString().substring(arg0.toString().indexOf("%") + 1, arg0.toString().lastIndexOf("%"));
                    String command = etComandoImpresora.getText().toString();
                    String tspl = "";
                    String prefix = "";
                    int counter = 0;

                    switch (command.toUpperCase()) {
                        case "SERIAL":
                            tspl = "_SERIAL$";
                            prefix = "Serial No: ";
                            break;
                        case "MODEL":
                            tspl = "_MODEL$";
                            prefix = "Model: ";
                            break;
                        case "VERSION":
                            tspl = "_VERSION$";
                            prefix = "Version: ";
                            break;
                        case "ADDRESS":
                            tspl = "GETSETTING$(\"CONFIG\", \"BT\", \"MAC ADDRESS\")";
                            prefix = "BT MAC Address: ";
                            break;
                        case "CHECKSUM":
                            tspl = "GETSETTING$(\"SYSTEM\", \"INFORMATION\", \"CHECKSUM\")";
                            prefix = "Checksum: ";
                            break;
                        case "DPI":
                            tspl = "GETSETTING$(\"SYSTEM\", \"INFORMATION\", \"DPI\")";
                            prefix = "DPI: ";
                            break;
                        case "RESET":
                            tspl = (char) 27 + "!R";
                            break;
                        case "FEED":
                            tspl = (char) 27 + "!F";
                            break;
                        case "PAUSE":
                            tspl = (char) 27 + "!P";
                            break;
                        case "CONTINUE":
                            tspl = (char) 27 + "!O";
                            break;
                    }
                    while (counter < 5) {
                        Log.d("SerialCounter", Integer.toString(counter));
                        try
                        {
                            BluetoothPrinter.getInstance().connect();
                            BluetoothPrinter.getInstance().setup(
                                    SharedConstants.Printer.width,
                                    40,
                                    SharedConstants.Printer.speed,
                                    SharedConstants.Printer.density,
                                    SharedConstants.Printer.sensorType,
                                    SharedConstants.Printer.gapBlackMarkVerticalDistance,
                                    SharedConstants.Printer.gapBlackMarkShiftDistance
                            );
                            String serial = BluetoothPrinter.getInstance().sendcommand_getString("OUT \"\";" + tspl).split("\r\n")[0];
                            if (!serial.equals(" ")) {
                                BluetoothPrinter.getInstance().clearbuffer();
                                BluetoothPrinter.getInstance().printerfont(100, 150, "3", 0, 0, 0, prefix + serial);
                                BluetoothPrinter.getInstance().printlabel(1, 1);
                                mainActivity.toastS(prefix + serial);
                            }
                            break;
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            counter++;
                        }
                    }
                    if (counter == 5) {
                        Log.d("SerialCounter", "No se pudo conectar con la impresora");
                        mainActivity.toastS("No se pudo conectar con la impresora");
                    }
                } catch (StringIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                DatabaseAssistant.insertOsticket("Cheat", "printerCommand%", myLocation);
                executeSendOsticket();
                etComandoImpresora.setText("");
            }
        });

        //-------------------------------------------------------------------------------
        myDialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog2.show();
    }

    private boolean checkInternetConnection()
    {
        ConnectivityManager con = (ConnectivityManager) mainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo= con.getActiveNetworkInfo();
        if(networkInfo!=null && networkInfo.isConnected())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void mandarAlerta(View view)
    {
        dialogo=new AlertDialog.Builder(mainActivity);
        dialogo.setTitle("¡Advertencia!");
        dialogo.setMessage("Si borras la base de datos, cerraremos la sesión. ¿Deseas continuar?");
        dialogo.setCancelable(false);
        dialogo.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent= new Intent(mainActivity, Login.class);
                intent.putExtra("bandera", "1");
                startActivity(intent);
                myDialog2.dismiss();
            }
        });

        dialogo.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        dialogo.show();
    }


    public void createJsonForLocalidades(String codigo)
    {
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("codigo", codigo);
            jsonParams.put("localidad_colonia_id", DatabaseAssistant.getLastLocalidadColoniaInserted());
            consumirWebServiceCatalogosLocalidades(jsonParams);
            Log.d("JSON", jsonParams.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void consumirWebServiceCatalogosLocalidades(JSONObject jsonParams)
    {
        progress = ProgressDialog.show(mainActivity, "Descargando localidades", "Por favor espera...", true);
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.getLocalidadesAndColonias, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.has("error"))
                            {
                                JSONObject json = response.getJSONObject("result");
                                JSONArray arregloDirecciones  = json.getJSONArray("direcciones");

                                if ( arregloDirecciones.length() > 0 )
                                {
                                    Localidad_Colonia.deleteAll(Localidad_Colonia.class);
                                }

                                for(int i=0; i<=arregloDirecciones.length()-1;)
                                {
                                    JSONObject jsonTmp = arregloDirecciones.getJSONObject(i);
                                    DatabaseAssistant.insertarLocalidadColonia(jsonTmp.getString("localidad"), jsonTmp.getString("colonia"), jsonTmp.getInt("ID"));
                                    i++;
                                }

                                if(progress!=null)
                                    progress.dismiss();
                            }
                            else
                            {
                                String error = response.getString("error");
                                if(progress!=null)
                                    progress.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            if(progress!=null)
                                progress.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {

        };
        //VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }


    private String getPrinterSerial() {
        String serial = "";
        int retries = 0;
        while (retries < 2) {
            try {
                BluetoothPrinter.getInstance().connect();
                serial = BluetoothPrinter.getInstance().sendcommand_getString("OUT \"\";_SERIAL$").split("\r\n")[0];

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
                    BluetoothPrinter.getInstance().disconnect();

                break;
            } catch (Exception ex) {
                ex.printStackTrace();
                retries++;
            }
        }

        return serial;
    }


}