package com.jaguarlabs.pabs.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.NoVisitadosAdapter;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.Novisitados;
import com.jaguarlabs.pabs.rest.SpiceHelper;
import com.jaguarlabs.pabs.rest.models.ModelNoVisitadosRequest;
import com.jaguarlabs.pabs.rest.models.ModelNoVisitadosResponse;
import com.jaguarlabs.pabs.rest.models.ModelPrintMoreThan400PaymentsCierreResponse;
import com.jaguarlabs.pabs.rest.request.ContractsNotVisitedRequest;
import com.jaguarlabs.pabs.rest.request.offline.PrintCierreInformativoOPeriodoTicketRequest;
import com.jaguarlabs.pabs.rest.request.offline.PrintContratosNoVisitadosTicketRequest;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.OnFragmentResult;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.listener.RequestProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by Jordan Alvarez on 12/06/2019.
 */
public class ContratosNoVisitados extends Fragment  {

    private MainActivity mainActivity;

    private static ContratosNoVisitados mthis = null;;

    private RelativeLayout noVisitadosLayout;
    private Button btn_efectuar_cierre;
    private ImageView back;
    private ListView lvContractsNotVisited;
    public static String mac;
    private SpiceHelper<Boolean> spicePrintNoVisitados;
    private SpiceManager spiceManagerOffline;
    private HashMap<String, JSONObject> noVisitadosArrayList;
    private List<ModelNoVisitadosResponse.ContractsNotVisited> noVisitadosArray;
    private PowerManager pm;
    private PowerManager.WakeLock wl;
    private FrameLayout flLoading;
    private SpiceManager spiceManager;
    private SpiceHelper<ModelNoVisitadosResponse> spiceNoVisitados;
    private String fechaInicialSemanal="", fechaFinalSemanal="", fechaInicialQuincenal="", fechaFinalQuincenal="", fechaInicialMensual="", fechaFinalMensual="";
    public String fecha_ultimo_abono="";

    private OnFragmentResult fragmentResult;

    List<ModelNoVisitadosResponse.ContractsNotVisited> contractsNotVisited;

    private String TAG = "CACHE_CONTRACTS_NOT_VISITED";

    private boolean pendingGettingContractsNotVisitedRequest;
    private boolean gettingContractsNotVisited = false;


    public static void setMthis(ContratosNoVisitados mthis) {
        ContratosNoVisitados.mthis = mthis;
    }

    @Override
    public void onStart() {
        super.onStart();

        //Toast.makeText(mainActivity, spiceNoVisitados.isRequestPending(), Toast.LENGTH_SHORT).show();

        //spiceManager.start(ApplicationResourcesProvider.getContext());
        if ( spiceNoVisitados.isRequestPending() ){
            //ki  spiceNoVisitados.executePendingRequest();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mainActivity.handlerSesion != null) {
            mainActivity.handlerSesion.removeCallbacks(mainActivity.myRunnable);
            mainActivity.handlerSesion = null;
            if (Clientes.getMthis() != null) {
                Clientes.getMthis().getActivity().finish();
            }
        }
        wl.release();
    }

    @Override
    public void onResume() {
        super.onResume();
        mthis = this;

        MainActivity.CURRENT_TAG = MainActivity.TAG_CONTRATOS_NO_VISITADOS;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_no_visitados, container, false);

        mainActivity = (MainActivity) getActivity();
        spiceManagerOffline = mainActivity.getSpiceManagerOffline();
        spiceManager = mainActivity.getSpiceManager();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        noVisitadosLayout = view.findViewById(R.id.noVisitadosLayout);
        btn_efectuar_cierre = view.findViewById(R.id.btn_efectuar_cierre);
        flLoading = (FrameLayout) view.findViewById(R.id.flLoading);

        back = view.findViewById(R.id.imageViewBack);

        back.setOnClickListener(v -> {
            mthis = null;
            getFragmentManager().popBackStack();
        });

        mainActivity.hasActiveInternetConnection(getContext(), mainActivity.mainLayout);

        /**
         * EditText to Search Folios
         * This edittext searches through ArrayCierre array using complete
         * folio (with serie, comma, dot and everything it has) or part of
         * the folio spelled.
         * If there's a coincidence, scrolls to that position.
         * Always takes the first coincidence, so if you are looking for an
         * specific folio, got to give complete folio.
         *
         * Jordan Alvarez
         */
        TextView etSearch = (TextView) view.findViewById(R.id.et_SearchContracts);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable changedText) {

                if (noVisitadosArray != null) {

                    if (changedText.length() != 0) {
                        String singleContractNotVisited;

                        for (int i = 0; i < noVisitadosArray.size(); i++) {
                            singleContractNotVisited = noVisitadosArray.get(i).getContractNumber();
                            if (singleContractNotVisited.equalsIgnoreCase( changedText.toString() )){
                                lvContractsNotVisited.setSelection(i);
                                break;
                            }
                        }
                    }
                }
            }
        });

        mthis = this;
        ExtendedActivity.setEstoyLogin(false);

        //noVisitadosArrayList = new HashMap<String,JSONObject>();

        pm = (PowerManager) getContext().getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "PABS:mywakelockapp");
        wl.acquire();

        Snackbar.make(noVisitadosLayout, R.string.info_message_getting_ready, Snackbar.LENGTH_LONG).show();

        spiceNoVisitados = new SpiceHelper<ModelNoVisitadosResponse>(spiceManager, "noVisitados", ModelNoVisitadosResponse.class, DurationInMillis.ONE_HOUR)
        {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                fail();
            }

            @Override
            protected void onSuccess(ModelNoVisitadosResponse modelNoVisitadosResponse)
            {
                try
                {
                    if (modelNoVisitadosResponse != null)
                    {
                        super.onSuccess(modelNoVisitadosResponse);
                        fillList(modelNoVisitadosResponse);

                    } /*else {
                        super.onSuccess(modelNoVisitadosResponse);
                        fail();
                    }*/
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                progressUpdate(progress);
            }
        };

        spicePrintNoVisitados = new SpiceHelper<Boolean>(spiceManagerOffline, "printNoVisitados", Boolean.class, DurationInMillis.ONE_HOUR) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                fail();
            }

            @Override
            protected void onSuccess(Boolean result) {
                try {
                    if (result != null) {
                        super.onSuccess(result);
                        if (result) {
                            Log.d("IMPRESION", "IMPRESION NORMAL");
                        } else {
                            fail();
                        }
                    } else {
                        super.onSuccess(true);
                        Log.d("IMPRESION", "IMPRESION NORMAL");
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                progressUpdate(progress);
            }
        };


        executeGetContractsNotVisited();


        btn_efectuar_cierre.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //ejecurar la instrucccion de contratos no visitados para imprimirlos en ticket, estoo esta muy similar a un cierre informativo
                if( mainActivity.checkBluetoothAdapter() )
                {
                        if (mthis != null)
                        {
                            executePrintNoVisitados();
                        }
                        else
                        {
                            Log.d("Entro", "Entro");
                        }
                }
                else
                {
                    Log.d("Entro", "Entro");
                }
            }
        });

    }

    private void executePrintNoVisitados(){

        String arregloDatosCobrador[] = DatabaseAssistant.consultarDatosDeCobrador();
        if(arregloDatosCobrador.length>0)
        {
            System.out.println(arregloDatosCobrador[3]); //no_cobrador
            System.out.println(arregloDatosCobrador[4]); //codigoCobrador
            System.out.println(arregloDatosCobrador[7]); //nombreCobrador

            if(noVisitadosArray==null || noVisitadosArray.isEmpty())
            {
                Toast.makeText(mainActivity, "Lo sentimos, trata de imprimir mas tarde", Toast.LENGTH_SHORT).show();
            }
            else
            {
                PrintContratosNoVisitadosTicketRequest printContratosNoVisitadosTicketRequest = new PrintContratosNoVisitadosTicketRequest(
                        noVisitadosArray,
                        arregloDatosCobrador[4],
                        arregloDatosCobrador[7],
                        arregloDatosCobrador[3],
                        fechaInicialSemanal,
                        fechaFinalSemanal,
                        fechaInicialQuincenal,
                        fechaFinalQuincenal,
                        fechaInicialMensual,
                        fechaFinalMensual);

                spicePrintNoVisitados.executeRequest(printContratosNoVisitadosTicketRequest);

                //mandar los parametros de las fechas tambien
            }
            //configurar los parametros para mandarlos a la clase de impresion del ticket
            //consultar los datos de la base de datos, se va a consultar los datos de collectors para obtener el no_cobrador, codigo del cobrador, nombre del cobrador
        }
        else
            Log.d("IMPRESION -- >", "NO SE RETORNARON DATOS");


    }

    private void executeGetContractsNotVisited(){

        ModelNoVisitadosRequest modelNoVisitadosRequest = new ModelNoVisitadosRequest( DatabaseAssistant.getCollector().getNumberCollector(), "-1");
        ContractsNotVisitedRequest contractsNotVisitedRequest = new ContractsNotVisitedRequest(modelNoVisitadosRequest);
        contractsNotVisitedRequest.setPriority(SpiceRequest.PRIORITY_HIGH);
        spiceNoVisitados.executeRequest(contractsNotVisitedRequest);
        //spiceManager.execute( contractsNotVisitedRequest, TAG, DurationInMillis.ONE_HOUR, new getContractsNotVisitedListener() );

    }


    private void onContractsNotVisitedFailure(){

        pendingGettingContractsNotVisitedRequest = false;
        gettingContractsNotVisited = false;

        mainActivity.toastL(R.string.error_message_call);

        if (getFragmentManager() != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CONTRATOS_NO_VISITADOS)) {
            getFragmentManager().popBackStack();
        }
    }

    /**
     * cleans cache with info of android service
     */
    private void clearCancelPaymentCache(){
        try{

            Future<?> future = spiceManager.removeAllDataFromCache();
            if ( future != null ){
                future.get();
            }

        } catch ( InterruptedException e){
            e.printStackTrace();
        } catch ( ExecutionException e ){
            e.printStackTrace();
        }
    }

    public void fillList(ModelNoVisitadosResponse result)
    {
        if (result.getResult() != null)
        {
            noVisitadosArray = result.getResult();
            System.out.println(noVisitadosArray);

            List<ModelNoVisitadosResponse.ContractsNotVisited> contractsNotVisited = result.getResult();
            this.contractsNotVisited = contractsNotVisited;

            NoVisitadosAdapter noVisitadosAdapter = new NoVisitadosAdapter(getContext(), contractsNotVisited);
            lvContractsNotVisited = (ListView) getView().findViewById(R.id.lv_noVisitados);
            lvContractsNotVisited.setAdapter(noVisitadosAdapter);

            FrameLayout frameLayout = (FrameLayout) getView().findViewById(R.id.fl_lv_noVisitados);
            frameLayout.setVisibility(View.VISIBLE);

            TextView countWeekly = (TextView) getView().findViewById(R.id.tv_w_amount);
            TextView countfortnightly = (TextView) getView().findViewById(R.id.tv_f_amount);
            TextView countMonthly = (TextView) getView().findViewById(R.id.tv_m_amount);

            long weeklyAmount = 0;
            long fortnightlyAmount = 0;
            long monthlyAmount = 0;
            for( int i = 0; i < contractsNotVisited.size(); i++ ) {
                switch ( contractsNotVisited.get(i).getPaymentForm() ) {
                    case "Semanal":
                        weeklyAmount++;
                        break;
                    case "Quincenal":
                        fortnightlyAmount++;
                        break;
                    case "Mensual":
                        monthlyAmount++;
                        break;
                    default:
                }
            }

            countWeekly.setText( "" + weeklyAmount );
            countfortnightly.setText( "" + fortnightlyAmount );
            countMonthly.setText( "" + monthlyAmount );

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
            Date date = new Date();

            String fecha = dateFormat.format(date);

            Novisitados.deleteAll(Novisitados.class);
            DatabaseAssistant.insertarNoVisitados(String.valueOf(weeklyAmount), String.valueOf(fortnightlyAmount), String.valueOf(monthlyAmount), fecha);


            fechaInicialSemanal=result.getStartDateW();
            fechaFinalSemanal= result.getEndDateW() ;
            fechaInicialQuincenal= result.getStartDateF() ;
            fechaFinalQuincenal= result.getEndDateF() ;
            fechaInicialMensual=result.getStartDateM();
            fechaFinalMensual= result.getEndDateM();


        } else if ( result.getError() != null )
        {
            try {
                AlertDialog.Builder alert = getAlertDialogBuilder();
                alert.setMessage( result.getError() );
                alert.setPositiveButton("Ok", (dialogInterface, i) -> {
                    mthis = null;
                    if (getFragmentManager() != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CONTRATOS_NO_VISITADOS)) {
                        getFragmentManager().popBackStack();
                    }
                });
                alert.create().show();
            }
            catch (NullPointerException e){
                e.printStackTrace();
                mthis = null;
                if (getFragmentManager() != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CONTRATOS_NO_VISITADOS)) {
                    getFragmentManager().popBackStack();
                }
            }
            catch (Exception e){
                e.printStackTrace();
                mthis = null;
                if (getFragmentManager() != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CONTRATOS_NO_VISITADOS)) {
                    getFragmentManager().popBackStack();
                }
            }
        }
    }

    /**
     * returns an instance of AlertDialogBuilder
     * @return instance of AlertDialogBuilder
     */
    private AlertDialog.Builder getAlertDialogBuilder()
    {
        if (getContext() != null)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                return new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                return new AlertDialog.Builder(getContext());
            }
        }

        return null;
    }

    private void fail(){
        dismissMyCustomDialog();
        mainActivity.toastL(R.string.error_message_call);
    }

    /**
     * hides framelayout used as progress dialog
     */
    private void dismissMyCustomDialog(){

        //goBack = true;
        flLoading.setVisibility(View.GONE);

        //final ImageView ivLoadingIcon = (ImageView) CierreInformativo.this.findViewById(R.id.ivLoading);
        //ivLoadingIcon.clearAnimation();
        FragmentManager fm = getFragmentManager();
        fm = null;

        if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
            fm.popBackStack();
            //Toast.makeText(mainActivity, "Entro a if", Toast.LENGTH_SHORT).show();
        } else if (fm == null && isAdded()) {
            fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
            //Toast.makeText(mainActivity, "Entro a else es NULLO", Toast.LENGTH_SHORT).show();
            fm.popBackStack();
        }
    }

    private void progressUpdate(RequestProgress progress){
        Util.Log.ih("inside progressUpdate method");
        switch ( (int) progress.getProgress() ){
            case ConstantsPabs.START_PRINTING:
                Util.Log.ih("case START_PRINTING");
                showMyCustomDialog();
                break;
            case ConstantsPabs.FINISHED_PRINTING:
                Util.Log.ih("case FINISHED_PRINTING");
                dismissMyCustomDialog();
                break;
            default:
                Util.Log.ih("default");
                //nothing
        }
    }

    /**
     * shows framelayout as progress dialog
     */
    private void showMyCustomDialog(){

        //goBack = false;

        //final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
        flLoading.setVisibility(View.VISIBLE);

        //final Animation qwe = AnimationUtils.loadAnimation(this, R.animator.rotate_around_center_point);
        //final ImageView ivLoadingIcon = (ImageView) CierreInformativo.this.findViewById(R.id.ivLoading);
        //ivLoadingIcon.startAnimation(qwe);
    }

}
