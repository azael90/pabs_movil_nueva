package com.jaguarlabs.pabs.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Looper;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.tscdll.TSCActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.CierreAdapter;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.Deposits;
import com.jaguarlabs.pabs.net.NetHelper;
import com.jaguarlabs.pabs.net.NetHelper.onWorkCompleteListener;
import com.jaguarlabs.pabs.net.NetService;
import com.jaguarlabs.pabs.rest.SpiceHelper;
import com.jaguarlabs.pabs.rest.models.ModelPrintMoreThan400PaymentsCierreResponse;
import com.jaguarlabs.pabs.rest.request.offline.PrintCierreInformativoOPeriodoTicketRequest;
import com.jaguarlabs.pabs.rest.request.offline.PrintMoreThan400PaymentsCierrePeriodoRequest;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.CerrarSesionCallback;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.MyCallBack;
import com.jaguarlabs.pabs.util.SharedConstants.Printer;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestProgress;

//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This class is shown after succeed with sending a deposit to office.
 * downloads all payments and visits made within a period.
 * It's posible to print ticket with them all and after dialog_copies it,
 * goes back to loggin screen.
 */
@SuppressWarnings({"SpellCheckingInspection", "AccessStaticViaInstance"})
@SuppressLint({ "Wakelock", "SimpleDateFormat" })
public class Cierre extends Fragment implements MyCallBack,
		CerrarSesionCallback {

	private static Cierre mthis = null;;
	private static final int TIME_TOAST = 2000;
	private ListView lvCierre;
	//This variables are used when dialog_copies payments greater than 400
	public static JSONArray arrayCierreArranged;
	public static int printCounterToSetSize = 0;
	public static int printCounter = 0;
	public static int organizationCounter = 0;
	public static int xValue;
	public static int ticketPosValue;
	//public static JSONArray empresasValues;
	public static List<Companies> companiesValues;

	public static JSONObject totalesValues;
	//public static SqlQueryAssistant queryValues;
	public static double montoValue;
	public static boolean restartMonto = true;
	public static int ticketsValue;
	public static int limitToPrint = 400;
	public static int sectionOfArray = 1;
	public static JSONArray arrayCierreCut;
	private static final String KEY_CACHE = "print_more_than_400_payments_cierre_cache";
	private ProgressDialog pd;
	private TextView et_monto_cierre;
	private long dateAux;
	//private static final UUID my_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	private Timer timer;

	private MainActivity mainActivity;
	private SpiceManager spiceManagerOffline;
	private SpiceHelper<Boolean> spicePrintCierrePeriodo;
	private SpiceHelper<ModelPrintMoreThan400PaymentsCierreResponse> spicePrintMoreThan400payments;

	private ImageView refresh;

	/**
	 * Gets context from this class.
	 * In some cases you cannot just call 'this', because another context is envolved
	 * or you want a reference to an object of this class from another class,
	 * then this context is helpful.
	 *
	 * NOTE
	 * Not recommended, it may cause NullPointerException though
	 *
	 * @return context from Cierre
	 */
	public static Cierre getMthis() {
		return mthis;
	}

	/**
	 * Sets 'mthis' global variable as Cierre's context
	 * @param mthis Cierre's context
	 */
	public static void setMthis(Cierre mthis) {
		Cierre.mthis = mthis;
	}

	private boolean dontexit = true;
	private String noCobrador;
	private String fechaOperacion;
	public static String coCobrador = "";
	public static String mac;
	public static String nombre;
	public static double deposito = 0;
	private JSONArray arrayCierre;
	public static int efectivoInicial = 0;
	private int posicionCancelacion;
	private JSONArray arrayCobrosOffline;
	TSCActivity TscDll = new TSCActivity();
	private PowerManager pm;
	private WakeLock wl;
	private boolean callSuperOnTouch = false;

	private int ticketsPrograma = 0;

	private int ticketsMalba = 0;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_cierre_, container, false);

		mainActivity = (MainActivity) getActivity();
		spiceManagerOffline = mainActivity.getSpiceManagerOffline();

		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		lvCierre = (ListView) view.findViewById(R.id.lv_cierre);
		et_monto_cierre= (TextView) view.findViewById(R.id.et_monto_cierre);

		spicePrintCierrePeriodo = new SpiceHelper<Boolean>(spiceManagerOffline, "printCierrePeriodo", Boolean.class, DurationInMillis.ONE_HOUR) {
			@Override
			protected void onFail(SpiceException exception) {
				dismissMyCustomDialog();
				mainActivity.toastL(R.string.error_message_printing);
			}

			@Override
			protected void onSuccess(Boolean result) {
				try {
					if (result != null) {
						super.onSuccess(result);
						if (result) {
							copiaImpresion();
						} else {
							dismissMyCustomDialog();
							mainActivity.toastL(R.string.error_message_printing);
						}
					} else {
						super.onSuccess(true);
						copiaImpresion();
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
			}

			@Override
			protected void onProgressUpdate(RequestProgress progress) {
				super.onProgressUpdate(progress);
				switch ((int) progress.getProgress()) {
					case ConstantsPabs.START_PRINTING:
						showMyCustomDialog();
						break;
					case ConstantsPabs.FINISHED_PRINTING:
						dismissMyCustomDialog();
						break;
					default:
						//nothing
				}
			}
		};

		spicePrintMoreThan400payments = new SpiceHelper<ModelPrintMoreThan400PaymentsCierreResponse>(spiceManagerOffline, "print_more_than_400_payments", ModelPrintMoreThan400PaymentsCierreResponse.class, DurationInMillis.ONE_HOUR) {
			@Override
			protected void onFail(SpiceException exception) {
				super.onFail(exception);
				onPrintingFailure(exception);
			}

			@Override
			protected void onSuccess(ModelPrintMoreThan400PaymentsCierreResponse result) {
				try {
					if (result != null) {
						super.onSuccess(result);
						onPrintingSuccess(result);
					} else {
						ModelPrintMoreThan400PaymentsCierreResponse m = new ModelPrintMoreThan400PaymentsCierreResponse();
						m.setResult(false);
						m.setFinishPrinting(false);
						super.onSuccess(m);
						onPrintingSuccess(m);
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
			}

			@Override
			protected void onProgressUpdate(RequestProgress progress) {
				switch ((int) progress.getProgress()) {
					case ConstantsPabs.START_PRINTING:
						showMyCustomDialog();
						break;
					case ConstantsPabs.FINISHED_PRINTING:
						dismissMyCustomDialog();
						break;
					default:
						//nothing
				}
			}
		};

		//init
		{
//		Clientes.getMthis().isConnected = true;
			mainActivity.isConnected = true;
			// Clientes.getMthis().hasActiveInternetConnection(Clientes.getMthis());

			view.findViewById(R.id.imageViewBack).setVisibility(View.GONE);
			refresh = view.findViewById(R.id.imageViewRefresh);
			refresh.setOnClickListener(v -> {
				descargarHistorial();
			});
			/*Toast.makeText(ApplicationResourcesProvider.getContext(),
					"Preparando... espera un momento.", Toast.LENGTH_LONG).show();*/
			Snackbar.make(mainActivity.mainLayout, "Preparando... espera un momento", Snackbar.LENGTH_SHORT).show();
			mthis = this;
			ExtendedActivity.setEstoyLogin(false);
			Button cierre = view.findViewById(R.id.btn_efectuar_cierre);
			cierre.setEnabled(false);
			cierre.setVisibility(View.GONE);

			cierre.setOnClickListener(v -> realizarCierre());

			mainActivity.hasActiveInternetConnection(getContext(), mainActivity.mainLayout);

			//SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
			//try {
			//query.deleteDepositos();
			DatabaseAssistant.deleteAllDeposits();
			//coCobrador = query.MostrarDatosCobrador().getString("codigo_cobrador");
			coCobrador = DatabaseAssistant.getCollector().getIdCollector();








			saveDepositos();










			//} catch (JSONException e1) {

			//write stacktrace into a .txt file
			//StackTraceHandler writeStackTrace = new StackTraceHandler();
			//writeStackTrace.uncaughtException("CIERRE.init", e1);

			//	e1.printStackTrace();
			//}


			pm = (PowerManager) getContext().getSystemService(
					Context.POWER_SERVICE);
			wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "tag");
			wl.acquire();

			Bundle bundle = getArguments();
			if (bundle != null) {
				if (bundle.containsKey("iniciar_time_sesion")) {
					boolean inicarTimer = bundle.getBoolean("iniciar_time_sesion");
					if (inicarTimer) {
						callSuperOnTouch = true;
						//iniciarHandlerInactividad();

					}
				}
				//if (bundle.containsKey("mac")) {
				//	mac = bundle.getString("mac");
				//}
				if (bundle.containsKey("isConnected")){
					mainActivity.isConnected = bundle.getBoolean("isConnected");
				}
			}
			mac = DatabaseAssistant.getPrinterMacAddress();
			//try {
			if (mainActivity.isConnected) {
				descargarHistorial();
			} else {
				AlertDialog.Builder alertInternet = new AlertDialog.Builder(getContext());
				alertInternet.setMessage(getResources().getString(
						R.string.error_message_network));
				alertInternet.setPositiveButton("Reintentar",(dialog, which) -> descargarHistorial());
				alertInternet.setNegativeButton("Salir", (dialog, which) -> {
					View refSplash = null;
					if (Splash.mthis != null) {
						refSplash = Splash.mthis
								.findViewById(R.id.linear_splash);
					}
					if (refSplash != null) {
						Splash.mthis.finish();
					}
					//finish();
					getFragmentManager().popBackStack();
				});
				alertInternet.create().show();
			}
			//} catch (JSONException e) {
			//	e.printStackTrace();
			//}

			totalesValues = new JSONObject();

		}
	}

	@Override
	public void onStart() {
		super.onStart();
		//spiceManagerOffline.start(ApplicationResourcesProvider.getContext());

		if (spicePrintCierrePeriodo.isRequestPending()){
			spicePrintCierrePeriodo.executePendingRequest();
		}
		else if (spicePrintMoreThan400payments.isRequestPending()){
			spicePrintMoreThan400payments.executePendingRequest();
		}
	}

	@Override
	public void onStop() {
		//spiceManagerOffline.shouldStop();
		super.onStop();
	}

	@Override
	public void cerrarSesionTimeOut() {
		if (mainActivity.handlerSesion != null) {
			mainActivity.handlerSesion.removeCallbacks(mainActivity.myRunnable);
			mainActivity.handlerSesion = null;

		}

		mthis = null;
		//finish();
		getFragmentManager().popBackStack();
	}

	/**
	 * shows a dialog asking for a copie
	 */
	public void copiaImpresion() {

		mthis = this;

		mainActivity.runOnUiThread(() -> {
			try {
				/**
				 * newsecondDialog.show();
				 *
				 * error:
				 * android.view.WindowManager$BadTokenException: Unable to add window --  is your activity running?
				 *
				 * possible solution:
				 *
				 * if( !isFinishing() ){...}
				 *
				 * isFinishing()
				 * Check to see whether this activity is in the process of finishing, either because you called
				 * finish() on it or someone else has requested that it finished. This is often used in onPause()
				 * to determine whether the activity is simply pausing or completely finishing.
				 *
				 * Jordan Alvarez
				 */
				final Dialog newsecondDialog = getDialog();
				newsecondDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				newsecondDialog.setContentView(R.layout.dialog_copies);
				newsecondDialog.setCancelable(false);
				newsecondDialog.show();
				TextView text = (TextView) newsecondDialog.findViewById(R.id.textView2);
				text.setText("¿Deseas imprimir una copia?");
				final TextView textc = (TextView) newsecondDialog
						.findViewById(R.id.textViewCancel);
				textc.setVisibility(View.VISIBLE);
				textc.setText("No");
				textc.setGravity(Gravity.CENTER_HORIZONTAL);
				//final SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
				textc.setOnTouchListener((v, event) -> {
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						textc.setBackgroundColor(Color.RED);
						return true;
					}
					if (event.getAction() == MotionEvent.ACTION_UP) {
						textc.setEnabled(false);

						textc.setBackgroundColor(Color.WHITE);
						vaciar();

						return true;
					}
					return false;
				});
				final TextView texta = (TextView) newsecondDialog
						.findViewById(R.id.textViewAceptar);
				texta.setText("Sí");
				texta.setOnTouchListener((v, event) -> {
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						texta.setBackgroundColor(Color.RED);
						return true;
					} else {
						texta.setEnabled(false);
						texta.setBackgroundColor(Color.WHITE);

						if (arrayCierre != null && arrayCierre.length() > 400) {
							//todo service migrated to robospice

							restartVariablesToPrint();

							sortArrayCierre();
							splitArrayCierreArrangedBy400();

							if (!mainActivity.isFinishing()) {
								if (newsecondDialog != null && newsecondDialog.isShowing()) {
									newsecondDialog.dismiss();
								}
							}

							//executePrintMoreThan400PaymentsService();
							executePrintMoreThan400Payments();
						} else {
							//AsyncPrint obj = new AsyncPrint(4, mthis, getProgressDialog());
							//obj.execute();
							if (!mainActivity.isFinishing()) {
								if (newsecondDialog != null && newsecondDialog.isShowing()) {
									newsecondDialog.dismiss();
								}
							}
							executePrintCierrePeriodo();
						}

						return true;
					}
				});
			} catch (WindowManager.BadTokenException e) {
				vaciar();
				e.printStackTrace();
			} catch (Exception e) {
				vaciar();
				e.printStackTrace();
			}
		});

	}

	/**
	 * gets 'noCobrador', 'nombre' and 'fechaOperacion' to end up
	 * calling {@link #listaPagosCobrador()} method where payments are downloaded
	 */
	private void descargarHistorial()
	{

		noCobrador = mainActivity.getEfectivoPreference().getString("no_cobrador", "");
		Log.e("cobrador", noCobrador);
		//SqlQueryAssistant query = new SqlQueryAssistant(this);
		//try {
		//nombre = query.MostrarDatosCobrador().getString("nombre");
		nombre = DatabaseAssistant.getCollector().getName();
		//} catch (JSONException e) {
		//	e.printStackTrace();
		//}


		//try {
		//	query.deleteCancelados();
		//} catch (JSONException e){
		//	e.printStackTrace();
		//}
		DatabaseAssistant.deleteAllCanceled();

		//((TextView) getView().findViewById(R.id.et_monto_cierre)).setText(mainActivity.formatMoney(mainActivity.getTotalEfectivo()));
		et_monto_cierre.setText(mainActivity.formatMoney(mainActivity.getTotalEfectivo()));
		Date date = new Date(System.currentTimeMillis());
		fechaOperacion = new SimpleDateFormat("yyyy-MM-dd").format(date);

		mainActivity.runOnUiThread(this::listaPagosCobrador);

	}

	/**
	 * disables bluetooth
	 */
	public void disableBluetooth() {

		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
				.getDefaultAdapter();
		mBluetoothAdapter.disable();
	}

	/**
	 * enables bluetooth
	 */
	public void enableBluetooth() {

		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (!mBluetoothAdapter.isEnabled()) {
			Util.Log.ih("enabling bluetooth");
			mBluetoothAdapter.enable();
		}
	}

	/**
	 * checks if more than 59 minutes have passed
	 * @return
	 *          true -> difference is more than limit
	 *          false -> difference is under limit
	 */
	public boolean checkDate() {
		long newTime = new Date().getTime();
		long differences = Math.abs(dateAux - newTime);
		return (differences > (1000 * 59));
	}

	/**
	 * gets deposit to office that was just made
	 * and inserts it into database
	 *
	 * Web Service
	 * 'http://50.62.56.182/ecobro/controldepositos/depositosPeriodo'
	 *
	 * JSONOBject param
	 * {
	 *     "periodo":, ,
	 *     "no_cobrador": ""
	 * }
	 */
	void saveDepositos()
	{
		JSONObject params = new JSONObject();

		try {
			params.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0) - 1);
			params.put("no_cobrador", mainActivity.getEfectivoPreference().getString("no_cobrador", ""));

		} catch (JSONException e) {
			e.printStackTrace();
		}

		requestSaveDepositos(params);

		/*NetService service = new NetService(ConstantsPabs.urlGetListaDepositosPorPeriodo, params);
		NetHelper helper = NetHelper.getInstance();

		helper.ServiceExecuter(service, getContext(), new onWorkCompleteListener()
		{
			@Override
			public void onError(Exception e) {
				// TODO Auto-generated method stub
				Log.d("IMPRESION -->", "ERROR");
			}

			@Override
			public void onCompletion(String result) {
				try {
					if (new JSONObject(result).has("error"))
					{
						Log.d("IMPRESION -->", "ERROR");
					}
					JSONArray results = new JSONObject(result).getJSONArray("result");

					for (int q = 0; q < results.length(); q++)
					{
						JSONObject deposito = results.getJSONObject(q);
						Log.e("deposito " + q, deposito.toString());

						if (deposito.getString("status").equals("1"))
						{
							DatabaseAssistant.insertDepositFromCierre(
									deposito.getString("monto"),
									deposito.getString("fecha"),
									deposito.getString("referencia"),
									deposito.getString("no_banco"),
									deposito.getString("status"),
									deposito.getString("empresa"),
									mainActivity.getEfectivoPreference().getInt("periodo", 0)-1);
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
					Log.i("IMPRESION -->", " Error onCompletation saveDeposits");
				}
			}
		});*/
	}

	private void requestSaveDepositos(JSONObject jsonParams)
	{
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlGetListaDepositosPorPeriodo, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				try {
					if (response.has("error"))
					{
						Log.d("IMPRESION -->", "ERROR");
					}

					JSONArray results = response.getJSONArray("result");
					JSONObject jsonDeposito = results.getJSONObject(0);
					String periodoRepuesta = jsonDeposito.getString("periodo");
					System.out.println(periodoRepuesta);

					String queryDelete = "DELETE FROM DEPOSITS WHERE periodo = '" + periodoRepuesta + "'";
					Deposits.executeQuery(queryDelete);

					System.out.println(periodoRepuesta);

					for (int q = 0; q < results.length(); q++)
					{
						JSONObject deposito = results.getJSONObject(q);
						Log.e("deposito " + q, deposito.toString());

						if (deposito.getString("status").equals("1"))
						{
							DatabaseAssistant.insertDepositFromCierre(
									deposito.getString("monto"),
									deposito.getString("fecha"),
									deposito.getString("referencia"),
									deposito.getString("no_banco"),
									deposito.getString("status"),
									deposito.getString("empresa"),
									mainActivity.getEfectivoPreference().getInt("periodo", 0)-1);
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
					Log.i("IMPRESION -->", " Error onCompletation saveDeposits");
				}
				Log.i("Request LOGIN-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						Log.d("IMPRESION -->", "ERROR");
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
	}


	/**
	 * start inactivity timer
	 * timer which ends session after 20 minutes
	 */
	public void llamarTimerInactividad() {
		//iniciarHandlerInactividad();
	}

	/**
	 * fills ListView with payments and visits made
	 * within period
	 *
	 * @param json
	 * 			Web Service response
	 */
	private void llenarLista(JSONObject json) {
		if (arrayCierre != null) {
			arrayCierre = null;
		}
		try {
			//SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
			//try {
			//query.deleteCancelados();
			DatabaseAssistant.deleteAllCanceled();
			//} catch (JSONException e) {
			//	e.printStackTrace();
			//}
			arrayCierre = json.getJSONArray("result");
			CierreAdapter adapter = new CierreAdapter(ApplicationResourcesProvider.getContext(), this, arrayCierre,
					false);

			lvCierre.setAdapter(adapter);
			/*
			lvCierre.setSelection(posicionCancelacion);
			posicionCancelacion = 0;
			ticketsPrograma = 0;
			ticketsMalba = 0;
			for (int cont = 0; cont < arrayCierre.length(); cont++) {
				JSONObject jsonTmp = arrayCierre.getJSONObject(cont);

				Log.e("incierre Programa" + jsonTmp.getInt("inCierre"),
						jsonTmp.getString("no_contrato"));
				if (jsonTmp.getInt("status") == 7
						&& jsonTmp.getInt("inCierre") == 0) {
					Log.d("ticketCancelado Programa", "" + jsonTmp.toString(1));
					String monto = jsonTmp.getString("monto");
					String empresa = jsonTmp.getString("empresa");
					String noCliente = jsonTmp.getString("no_cliente");
					query.insertarTicketCancelado(monto, empresa, noCliente);

				}
			}
			*/

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * counts how many tickets per company the collector has, and shows dialog with
	 * the info
	 *
	 * @throws JSONException
	 */
	public void contarTicketsDialogo() throws JSONException {
		//SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
		//JSONArray empresas = query.mostrarEmpresas();
		List<Companies> companies = DatabaseAssistant.getCompanies();
		String dialog = "";
		for (int x = 0; x < 1; x++) {//for (int x = 0; x < companies.size(); x++) {
			int tickets = 0;
			for (int y = 0; y < arrayCierre.length(); y++) {
				if (arrayCierre
						.getJSONObject(y)
						.getString("empresa")
						.equals(companies.get(x).getIdCompany())) {
					if (arrayCierre.getJSONObject(y).getDouble("monto") != 0
							|| arrayCierre.getJSONObject(y).getInt("status") == 2) {
						tickets++;
					}
				}
			}
			dialog += "Tickets "
					+ companies.get(x).getName() + " : " + tickets + "\n";
		}
		mainActivity.showAlertDialog("Tickets totales", dialog, true);
	}

	/**
	 * gets and processes response from Web Service
	 * if response is successful, calls {@link #llenarLista(JSONObject)}
	 * to fill ListView with all payments and visits, then sets listener
	 * for 'imageViewTicketButton' with number of tickets calling
	 * {@link #contarTicketsDialogo()}
	 *
	 * @param json Web Service response
	 */
	public void manage_GetListaPagosByCobrador(JSONObject json) {
		manageObtenerDepositos();
		if (json.has("result")) {

			llenarLista(json);

			ImageView ticketButton = (ImageView) getView().findViewById(R.id.imageViewTicketButton);
			ticketButton.setOnClickListener(v -> {
				try {
					contarTicketsDialogo();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			});

			Button cierre = (Button) getView().findViewById(R.id.btn_efectuar_cierre);
			cierre.setEnabled(true);
			cierre.setVisibility(View.VISIBLE);

		} else if (json.has("error")) {
			try {
				String motivo = json.getString("error");
				if (motivo.equals("No se obtuvieron registros")) {
					mostrarMensajeSinCoincidencias(motivo);
					Button cierre = (Button) getView().findViewById(R.id.btn_efectuar_cierre);
					cierre.setEnabled(true);
					cierre.setVisibility(View.VISIBLE);
				} else {
					mostrarMensajeError(ConstantsPabs.listaPagosCobrador);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * returns an instance of Dialog
	 * @return instance of Dialog
	 */
	private Dialog getDialog(){
		return new Dialog(getContext());
	}

	/**
	 * returns an instance of AlertDialogBuilder
	 * @return instance of AlertDialogBuilder
	 */
	private AlertDialog.Builder getAlertDialogBuilder(){
		return new AlertDialog.Builder(getContext());
	}

	/**
	 * returns an instance of ProgressDialog
	 * @return instance of ProgressDialog
	 */
	private ProgressDialog getProgressDialog(){
		return new ProgressDialog(getContext());
	}

	/**
	 * sets 'deposito' variable to total deposits
	 *
	 * @param rs
	 */
	public void manageObtenerDepositos() {

		try {
			deposito = DatabaseAssistant.getDepositoDeOficinaPorPeriodo(mainActivity.getEfectivoPreference().getInt("periodo", 0) -1);
			Log.d("IMPRESION -->", "Deposito: "+ deposito);
		} catch (Exception e) {
			deposito = 0;
			e.printStackTrace();
		}

	}

	/**
	 * shows error message
	 *
	 * @param caso
	 * 			constant when failed
	 */
	public void mostrarMensajeError(final int caso) {
		AlertDialog.Builder alert = getAlertDialogBuilder();
		alert.setMessage("Hubo un error de comunicación con el servidor, verifique sus ajustes de red o intente más tarde.");
		alert.setPositiveButton("Salir", (dialog, which) -> {
			View refMain = null;
			if (Clientes.getMthis() != null) {
				/*refMain = Clientes.getMthis().getView().findViewById(
						R.id.listView_clientes);*/
				if (Clientes.getMthis().getView() != null)
					refMain = Clientes.getMthis().getView().findViewById(
							R.id.rv_clientes);
			}

			View refLogin = null;
			if (Login.getMthis() != null) {
				refLogin = Login.getMthis().findViewById(
						R.id.editTextUser);
			}

			View refSplash = null;
			if (Splash.mthis != null) {
				refSplash = Splash.mthis
						.findViewById(R.id.linear_splash);
			}

			if (refMain != null) {
				mthis = null;
				//finish();
				getFragmentManager().popBackStack();;
			} else if (refLogin != null) {
				Bundle bundle = getArguments();
				if (bundle != null) {
					if (bundle.containsKey("iniciar_time_sesion")) {
						if (mainActivity.handlerSesion != null) {
							mainActivity.handlerSesion
									.removeCallbacks(mainActivity.myRunnable);
							mainActivity.handlerSesion = null;

						}
					}
				}

				Login.getMthis().finish();
				if (refSplash != null) {
					Splash.mthis.finish();
				}
				mthis = null;
				//finish();
				getFragmentManager().popBackStack();
			} else if (refSplash != null) {
				Bundle bundle = getArguments();
				if (bundle != null) {
					if (bundle.containsKey("iniciar_time_sesion")) {
						if (mainActivity.handlerSesion != null) {
							mainActivity.handlerSesion
									.removeCallbacks(mainActivity.myRunnable);
							mainActivity.handlerSesion = null;

						}
					}
				}
				Splash.mthis.finish();
				mthis = null;
				//finish();
				getFragmentManager().popBackStack();

			}
		});
		alert.setNegativeButton("Reintentar", (dialog, which) -> {
			if (!mainActivity.isConnected) {
				mostrarMensajeError(caso);
			}
		});
		alert.create();
		alert.setCancelable(false);
		alert.show();
	}

	/**
	 * this method gets launched when there's no payments and visits
	 * made yet
	 * shows a dialog with message from server
	 *
	 * @param motivo
	 * 			response from server that contains a message
	 */
	private void mostrarMensajeSinCoincidencias(final String motivo) {
		AlertDialog.Builder alert = getAlertDialogBuilder();
		alert.setMessage(motivo);
		alert.setPositiveButton("Aceptar", (dialog, which) -> {});
		alert.create().show();
	}

	@Override
	public void onClickCancel(final int position) {
		//nothing here...
	}

	/*@Override
	public void onBackPressed() {
		Toast.makeText(ApplicationResourcesProvider.getContext(), "Imprime el cierre para salir",
				Toast.LENGTH_SHORT).show();
	}*/

	/**
	 * does a clean in the database and sharedPreferences files
	 */
	void vaciar() {
		//SqlQueryAssistant sql = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
		/*
		sql.vaciarCobrador();
		try {
			sql.deleteCobros();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			sql.deleteDepositos();
		} catch (JSONException e) {

			//write stacktrace into a .txt file
			//StackTraceHandler writeStackTrace = new StackTraceHandler();
			//writeStackTrace.uncaughtException("APORTACION.Vaciar", e);

			e.printStackTrace();
		}

		try {
			sql.deleteCancelados();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		*/
		DatabaseAssistant.eraseDatabaseAfterDoingCierre();

		SharedPreferences preferences = getContext().getSharedPreferences("PABS_Login",
				Context.MODE_PRIVATE);
		/**
		 * user_login is needed after doing "cierre de periodo"
		 * since the same user should log in.
		 * Jordan Alvarez
		 */
		String userLogin = preferences.getString("user_login", "");
		//preferences.edit().remove("fecha_sesion").clear().putString("user_login", userLogin).apply();
		preferences.edit().putString("user_login", userLogin).apply();
		//preferences.edit().remove("fecha_sesion").clear().apply();

		SharedPreferences preferencesLogin = getContext().getSharedPreferences(
				"PABS_SPLASH", Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = preferencesLogin.edit();
		edit.remove("fecha_sesion");
		edit.remove("fecha");
		edit.remove("id_status_deposito");
		edit.clear();
		edit.apply();
		startActivity(new Intent(ApplicationResourcesProvider.getContext(), Login.class));

		Toast.makeText(ApplicationResourcesProvider.getContext(),
				getString(R.string.success_message_deposit),
				Toast.LENGTH_LONG).show();

		SharedPreferences sharedPreferences = getContext().getSharedPreferences(
				"PABS_SPLASH", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor2 = sharedPreferences
				.edit();
		editor2.remove("idStatus");
		editor2.apply();

		/**
		 * Updates periodo
		 * just in case server didn't do it
		 */
		/*
		int periodo = getEfectivoPreference().getInt("periodo", 0);
		getEfectivoEditor().putInt("periodo", (periodo + 1)).apply();
		Util.Log.ih("periodo cambio a = " + getEfectivoPreference().getInt("periodo", 0));
		*/

		mthis = null;
		//finish();
		getFragmentManager().popBackStack();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	/**
	 * manages clicks from screen of resources as
	 *
	 * - imageViewBack
	 * 		finishes activity
	 *
	 * - btn_efectuar_cierre
	 * 		calls {@link #realizarCierre()}
	 *
	 * @param v
	 * 		view of resources
	 */
	public void onClickFromXML(View v) {
		switch (v.getId()) {
			case R.id.imageViewBack:
				if (dontexit) {
					mthis = null;
					//finish();
				}
				break;
			case R.id.btn_efectuar_cierre:
			/*
			if (Clientes.getMthis() != null) {
				Clientes.getMthis().writeToFile("Se intento cerrar sesión");
			}
			*/
				realizarCierre();
				break;
			default:
				break;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (mainActivity.handlerSesion != null) {
			mainActivity.handlerSesion.removeCallbacks(mainActivity.myRunnable);
			mainActivity.handlerSesion = null;
			if (Clientes.getMthis() != null) {
				Clientes.getMthis().getActivity().finish();
			}
		}
		wl.release();
	}

	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		//when true
		//a ticket is being printed
		if (goBack) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				SharedPreferences preferencesSplash = getContext().getSharedPreferences(
						"PABS_SPLASH", Context.MODE_PRIVATE);
				if (preferencesSplash.contains("fecha")) {
					moveTaskToBack(true);
				}

			}
			return super.onKeyDown(keyCode, event);
		}
		return false;
	}*/

	@Override
	public void onResume() {
		super.onResume();
		mthis = this;

		MainActivity.CURRENT_TAG = MainActivity.TAG_CIERRE;
		/*
		if (Clientes.getMthis() != null) {
			Clientes.getMthis().hasActiveInternetConnection(Clientes.getMthis());
		}
		*/
	}

	/*@Override
	public void onUserInteraction() {
		if (callSuperOnTouch) {
			super.onUserInteraction();
		} else {
			View refMain = null;
			if (Clientes.getMthis() != null) {
				refMain = Clientes.getMthis().getView().findViewById(
						R.id.listView_clientes);
			}
			if (refMain != null) {
				try {
					Clientes.getMthis().llamarTimerInactividad();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}*/

	/**
	 * calls {@link #sortArrayCierre()} and {@link #splitArrayCierreArrangedBy400()}
	 * to after call {@link #executePrintMoreThan400Payments()} wichi is an
	 * android service if payments and visits are more than 400,
	 * else, calls {@link #executePrintCierrePeriodo()}
	 */
	private void realizarCierre() {
		//SqlQueryAssistant query = new SqlQueryAssistant(this);
		TextView etMonto = (TextView) getView().findViewById(R.id.et_monto_cierre);
		if (etMonto.getText().length() == 0)
		{
			etMonto.setText("0.00");
		}

		if (etMonto.getText().length() > 0)
		{

			//if (isConnected) {
			if(mainActivity.checkBluetoothAdapter()) {
				int cont = 0;
				boolean ban = true;
				while (cont < 5 && ban) {
					try {
						if (mac == null) {
							mac = DatabaseAssistant.getPrinterMacAddress();
						}
						if (arrayCierre != null && arrayCierre.length() > 400) {

							if (mthis != null) {

								//After
								//todo service migrated to robospice
								//Jordan Alvarez
								sortArrayCierre();
								splitArrayCierreArrangedBy400();
								executePrintMoreThan400Payments();

								ban = false;
							}
						} else {
							if (mthis != null) {
								executePrintCierrePeriodo();
								ban = false;
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						cont++;
					}
					if (cont > 4) {
						Toast.makeText(getContext(), "No se ha encontrado la impresora", Toast.LENGTH_SHORT).show();
					}
				}
			}

			//} else {
			//	mostrarMensajeError(ConstantsPabs.cierreCobrador);
			//}
		} else {
			mainActivity.showAlertDialog("",
					getResources().getString(R.string.error_message_fields),
					false);
		}

	}

	/**
	 * sorts 'arrayCierreArranged' variable by
	 * companies id, so that they'll be printed
	 * in order
	 */
	public void sortArrayCierre(){
		JSONArray temp = new JSONArray();
		JSONObject objectFromArray = new JSONObject();
		//JSONArray arrayCierreArranged = new JSONArray();
		arrayCierreArranged = new JSONArray();
		//JSONArray empresas = new JSONArray();
		//JSONObject empresa = new JSONObject();
		String empresaId = "";

		List<Companies> companies;
		Companies company;

		//SqlQueryAssistant query = new SqlQueryAssistant(this);

		temp = arrayCierre;

		//try{
		//empresas = query.mostrarEmpresas();
		companies = DatabaseAssistant.getCompanies();
		//} catch (JSONException e){
		//	e.printStackTrace();
		//}

		for( int j = 0; j < companies.size(); j++ ) {//for( int j = 0; j < companies.size(); j++ ) {

			//try{
			//empresa = empresas.getJSONObject(j);
			company = companies.get(j);
			//empresaId = empresa.getString(Empresa.id_empresa);
			empresaId = company.getIdCompany();
			//} catch (JSONException e){
			//	e.printStackTrace();
			//}

			for (int i = 0; i < temp.length(); i++) {
				try {
					objectFromArray = temp.getJSONObject(i);

					if (objectFromArray.getString("empresa").equals(empresaId)) {
						arrayCierreArranged.put(objectFromArray);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		Util.Log.ih(arrayCierreArranged.toString());
	}

	/**
	 * splits 'arrayCierreArranged' variable
	 * by 400, so that only 400 payments and
	 * visits are printed per ticket
	 */
	public void splitArrayCierreArrangedBy400(){
		arrayCierreCut = new JSONArray();

		int limitToCut = sectionOfArray * 400;
		int index = limitToCut - 400;

		for( ; index < limitToCut && index < arrayCierreArranged.length(); index++ ){
			try{
				arrayCierreCut.put(arrayCierreArranged.getJSONObject(index));

			} catch (JSONException e){
				e.printStackTrace();
			}
		}

		Util.Log.ih(arrayCierreCut.toString());
	}

	/**
	 * downloads all payment and visits made
	 *
	 * Web Service
	 * 'http://50.62.56.182/ecobro/controlpagos/cobrosPeriodo'
	 *
	 * JSONObject param
	 * {
	 *     "no_cobrador": "",
	 *     ""periodo":
	 * }
	 */
	public void listaPagosCobrador() {
		JSONObject json = new JSONObject();
		try {
			json.put("no_cobrador", noCobrador);
			json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0) - 1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*RequestResult requestResult = new RequestResult(getContext());

        requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
            @Override
            public void onRequestResult(NetworkResponse response) {
                try
                {
                    manage_GetListaPagosByCobrador(new JSONObject(VolleyTickle.parseResponse(response)));
                }
                catch (JSONException ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onRequestError(NetworkResponse error) {
                Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), "Hubo un error de comunicacion con el servidor, intentando nuevamente", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                descargarHistorial();
            }
        });

        requestResult.executeRequest(ConstantsPabs.urlListaPagosCobrador, Request.Method.POST, json);*/
		requestListaPagosCobrador(json);

		/*NetHelper.getInstance().ServiceExecuter(
				new NetService(ConstantsPabs.urlListaPagosCobrador, json),
				getContext(), new onWorkCompleteListener() {

					@Override
					public void onError(Exception e) {
						Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), "Hubo un error de comunicacion con el servidor, intentando nuevamente", Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						//finish();
						descargarHistorial();
					}

					@Override
					public void onCompletion(String result) {
						try {
							manage_GetListaPagosByCobrador(new JSONObject(
									result));
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				});*/
	}
	private void requestListaPagosCobrador(JSONObject jsonParams)
	{
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlListaPagosCobrador, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				try
				{
					manage_GetListaPagosByCobrador(response);
					Log.i("Request LOGIN-->", new Gson().toJson(response));
				}catch (Exception ex)
				{
					ex.printStackTrace();
				}

			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), "Hubo un error de comunicacion con el servidor, intentando nuevamente", Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						descargarHistorial();
						error.printStackTrace();
						Log.e("getNumNotificaciones", "Error al obtener numero de notificaciones");
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
	}

	/**
	 * starts android service to print 'cierre de periodo' in another thread
	 */

	private void executePrintCierrePeriodo(){
		PrintCierreInformativoOPeriodoTicketRequest printCierrePeriodoRequest = new PrintCierreInformativoOPeriodoTicketRequest(
				false,
				arrayCierre,
				coCobrador,
				nombre,
				efectivoInicial,
				mainActivity.getEfectivoPreference().getInt("periodo", 0) - 1,
				deposito);

		spicePrintCierrePeriodo.executeRequest(printCierrePeriodoRequest);
	}

	/**
	 * sorts made payments by contract number
	 */
	private JSONArray sortArrayCierreByContractNumber(){
		JSONArray arrayCierreSortByContractNumber = arrayCierre;

		try {
			for (int i = 0; i < arrayCierreSortByContractNumber.length() - 1; i++) {
				for (int j = 0; j < arrayCierreSortByContractNumber.length() - 1; j++) {

					JSONObject firstItem = arrayCierreSortByContractNumber.getJSONObject(j);
					JSONObject secondItem = arrayCierreSortByContractNumber.getJSONObject(j + 1);

					int firstItemContract = Integer.parseInt(firstItem.getString("no_cliente").replace("C", ""));
					int secondItemContract = Integer.parseInt(secondItem.getString("no_cliente").replace("C", ""));

					if (firstItemContract < secondItemContract) {
						arrayCierreSortByContractNumber.put(j+1, firstItem);
						arrayCierreSortByContractNumber.put(j, secondItem);
					}
				}
			}
		} catch (JSONException e){
			e.printStackTrace();
		}
		return arrayCierreSortByContractNumber;
	}

	/**
	 * starts android service to print ticket in another thread
	 */
	/*
	private void executePrintMoreThan400PaymentsService(){
		//clearPrintMoreThan400PaymentsCache();
		PrintMoreThan400PaymentsCierreRequest request = new PrintMoreThan400PaymentsCierreRequest();
		getSpiceManager().execute(request, new PrintMoreThan400PaymentsListener());
	}
	*/

	/**
	 * starts android service to print ticket in another thread
	 */
	private void executePrintMoreThan400Payments(){
		PrintMoreThan400PaymentsCierrePeriodoRequest printMoreThan400PaymentsCierrePeriodoRequest = new PrintMoreThan400PaymentsCierrePeriodoRequest();
		spicePrintMoreThan400payments.executeRequest(printMoreThan400PaymentsCierrePeriodoRequest);
	}

	/**
	 * android service
	 * an error happened
	 * show up a dialog with option to retry to print
	 *
	 * @param exception
	 * 			error thrown
	 */
	private void onPrintingFailure(SpiceException exception){
		mainActivity.LogEh(R.string.cierrePeriodo_ticket_printed_failed);

		AlertDialog.Builder tryAgainDialog = getAlertDialogBuilder()
				.setMessage(R.string.error_message_printing_try_again)
				.setPositiveButton(R.string.error_title_try_again, (dialog, which) -> {
					restartVariablesToPrint();

					sortArrayCierre();
					splitArrayCierreArrangedBy400();

					//executePrintMoreThan400PaymentsService();
					executePrintMoreThan400Payments();
				});
		tryAgainDialog.create().show();

	}

	/**
	 * this method gets called when android service for dialog_copies tickets finishes
	 * either with an error or with success
	 *
	 * @param response
	 * 			response with the result of the android service
	 *          what is needed is response.result which is a boolean
	 *          true -> successfull
	 *          false -> error
	 */
	private void onPrintingSuccess(ModelPrintMoreThan400PaymentsCierreResponse response){
		mainActivity.LogIh(R.string.printing_service_ended);

		if( response.result ){

			/*
			pd.dismiss();
			*/
			dismissMyCustomDialog();

			if( !response.finishPrinting ){

				AlertDialog.Builder tryAgainDialog = getAlertDialogBuilder()
						.setMessage(R.string.success_message_continue_printing)
						.setPositiveButton(R.string.info_message_continue, (dialog, which) -> {
							Cierre.sectionOfArray++;

							splitArrayCierreArrangedBy400();

							//executePrintMoreThan400PaymentsService();
							executePrintMoreThan400Payments();
						});
				tryAgainDialog.create().show();
			}
			else{

				copiaImpresion();
				//finish();
			}
		}
		else{

			/*
			pd.dismiss();
			*/
			dismissMyCustomDialog();

			mainActivity.LogEh(R.string.error_message_printing_try_again);

			AlertDialog.Builder tryAgainDialog = getAlertDialogBuilder()
					.setMessage(R.string.error_message_printing_try_again)
					.setPositiveButton(R.string.error_title_try_again, (dialog, which) -> {
						restartVariablesToPrint();

						sortArrayCierre();
						splitArrayCierreArrangedBy400();

						//executePrintMoreThan400PaymentsService();
						executePrintMoreThan400Payments();
					});
			tryAgainDialog.create().show();

		}
		//if (pd != null) {
		//	pd.dismiss();
		//}
	}

	/**
	 * restarts variables to print more than 400
	 * payments and visits when failed trying
	 * to print ticket
	 */
	private void restartVariablesToPrint(){
		this.printCounterToSetSize = 0;
		this.printCounter = 0;
		this.organizationCounter = 0;
		this.restartMonto = true;
		this.limitToPrint = 400;
		this.sectionOfArray = 1;
	}

	private boolean goBack = true;

	/**
	 * shows framelayout as progress dialog
	 */
	private void showMyCustomDialog(){

		goBack = false;

		final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
		flLoading.setVisibility(View.VISIBLE);

		//final Animation qwe = AnimationUtils.loadAnimation(this, R.animator.rotate_around_center_point);
		//final ImageView ivLoadingIcon = (ImageView) ClienteDetalle.this.findViewById(R.id.ivLoading);
		//ivLoadingIcon.startAnimation(qwe);
	}

	/**
	 * hides framelayout used as progress dialog
	 */
	private void dismissMyCustomDialog(){

		goBack = true;

		final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
		flLoading.setVisibility(View.GONE);

		//final ImageView ivLoadingIcon = (ImageView) ClienteDetalle.this.findViewById(R.id.ivLoading);
		//ivLoadingIcon.clearAnimation();
	}

}