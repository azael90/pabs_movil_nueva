package com.jaguarlabs.pabs.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.EmpresasSpinnerAdapter;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.Payments;
import com.jaguarlabs.pabs.dialog.BanksDialog;
import com.jaguarlabs.pabs.models.ModelPayment;
import com.jaguarlabs.pabs.net.NetHelper;
import com.jaguarlabs.pabs.net.NetHelper.onWorkCompleteListener;
import com.jaguarlabs.pabs.net.NetService;
import com.jaguarlabs.pabs.util.CerrarSesionCallback;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This activity is used for the 'Bloqueo' screen.
 * So that when the device gets blocked by some reason, this class gets called.
 */
@SuppressLint("Wakelock")
public class Bloqueo extends ExtendedActivity implements CerrarSesionCallback,
		OnItemSelectedListener {

	private static Bloqueo mthis = null;
	private JSONArray arrayBanks;
	private int bancoPosicion;
	private String idStatus;
	private int status;

	private JSONObject datosCobrador;
	private boolean tipoConfStatusRazon;
	private EmpresasSpinnerAdapter adapter;
	private boolean requesterCanceled;
	private boolean alertShowing;
	private PowerManager pm;
	private WakeLock wl;
	private JSONArray arrayCobrosOffline;
	private boolean callSuperOnTouch = false;
	private double montoDepositado;
	private JSONObject saldo;

	private List<ModelPayment> offlinePayments;
	private RelativeLayout bloqueoLayout;

	/**
	 * Sets all deposits stored in arrayCobrosOffline variable which were just sent
	 * to server as sync (sync <- 1) so that does not resend payments made
	 * causing a lot of data usage.
	 */
	public void sincronizarPagosRegistrados(JSONObject json) {
		try {
			//for (ModelPayment p: offlinePayments){
				DatabaseAssistant.updatePaymentsAsSynced(json);
			//}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * sends to server all payments made that are not sync yet
	 *
	 * Web Service
	 * 'http://50.62.56.182/ecobro/controlpagos/registerPagos'
	 * JSONObject param
	 * {
	 *     "no_cobrador": "",
	 *     "pagos": {}
	 *     "periodo":
	 * }
	 */
	public void registerPagos() {

		Util.Log.ih("registrando pagos...");
		
		JSONObject json = new JSONObject();
		
		try {
			json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
			json.put("pagos", new JSONArray( new Gson().toJson( offlinePayments ) ));
			json.put("periodo",getEfectivoPreference().getInt("periodo", 0));
			//HashMap<String, String> hm = new HashMap<String, String>();
			//hm.put("datos", arrayCobrosOffline.toString());
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*RequestResult requestResult = new RequestResult(this);

		requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
			@Override
			public void onRequestResult(NetworkResponse response) {
				try {
					manage_RegisterPagosOffiline(new JSONObject(VolleyTickle.parseResponse(response)));
				}
				catch (JSONException ex)
				{
					ex.printStackTrace();
				}
			}

			@Override
			public void onRequestError(NetworkResponse error) {
				Util.Log.ih("Error registrando pagos");
			}
		});

		requestResult.executeRequest(ConstantsPabs.urlregisterPagosOfflineMain, Request.Method.POST, json);*/
		requestRegiterPagosOfflineVolley(json);
		/*NetHelper.getInstance().ServiceExecuter(new NetService(ConstantsPabs.urlregisterPagosOfflineMain, json), this, new onWorkCompleteListener() {

							@Override
							public void onError(Exception e) {
								Util.Log.ih("Error registrando pagos");
							}

							@Override
							public void onCompletion(String result) {
								try {
									manage_RegisterPagosOffiline(new JSONObject(
											result));
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						});*/
	}

	private void requestRegiterPagosOfflineVolley(JSONObject jsonParams) //Azael--
	{
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlregisterPagosOfflineMain, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				manage_RegisterPagosOffiline(response);
				Log.i("REGISTER_PAGOS-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						Log.e("regPagosOffline", "Error al registrar pagos offline");
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
	}

	/**
	 * shows a dialog when is trying to send payments to server but
	 * internet is not available
	 */
	private void alertaDeCobrosOffline() {
		final AlertDialog.Builder alert = new AlertDialog.Builder(mthis);
		alert.setTitle("Advertencia");
		alert.setCancelable(false);
		alert.setMessage("Necesitas tener internet para continuar.");
		alert.setPositiveButton("Aceptar", (dialog, which) -> {
			//SqlQueryAssistant query = new SqlQueryAssistant(mthis);
			//try {
			//JSONObject json = query
			//		.obtenerCobrosDesincronizados();
			offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();
			if (offlinePayments != null && offlinePayments.size() > 0) {

				if (isConnected) {
					registerPagos();
				}
			} else {
				alertaDeCobrosOffline();
				enviarDepositos();
			}
			//} catch (JSONException e) {
			//	e.printStackTrace();
			//}
			dialog.dismiss();
		});
		alert.create().show();

	}

	/**
	 * Manages response of web service
	 * if failed to send payments, it will try to send them again,
	 * if not, will set them as sync
	 * @param json JSONObject with response of web service
	 */
	public void manage_RegisterPagosOffiline(JSONObject json)
	{
		Log.e("manage_RegisterPagosOff", "result = " + json.toString());

		String fecha="";
		if (json.has("result"))
		{
			try
			{
				JSONArray arrayPagosFallidos = json.getJSONArray("result");

				if (arrayPagosFallidos.length() > 0)
				{
					registerPagos();
				}
				else {
					sincronizarPagosRegistrados(json);
					setConfiguraciones();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		enviarDepositos();
	}

	@Override
	protected void init() {
		super.init();
		setContentView(R.layout.activity_bloqueo);

		bloqueoLayout = findViewById(R.id.RelativeLayout1);

		hasActiveInternetConnection(this, bloqueoLayout);

		mthis = this;
		setEstoyLogin(false);
		getApplicationContext();
		pm = (PowerManager) getApplicationContext().getSystemService(
				Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "tag");
		wl.acquire();
		arrayBanks = null;
		status = 0;
		tipoConfStatusRazon = false;
		requesterCanceled = false;
		alertShowing = false;
		findViewById(R.id.btn_desbloqueo_deposito)
				.setVisibility(View.INVISIBLE);
		findViewById(R.id.btn_desbloqueo_razon).setVisibility(View.INVISIBLE);

		//SqlQueryAssistant query = new SqlQueryAssistant(mthis);
		Spinner empresas = (Spinner) findViewById(R.id.spinnerEmpresas);
		try {
			adapter = new EmpresasSpinnerAdapter(
					DatabaseAssistant.getCompaniesForSpinner(), getApplicationContext());
			empresas.setAdapter(adapter);
			empresas.setOnItemSelectedListener(this);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		boolean vieneSplash = false;
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			if (bundle.containsKey("vengo_de_splash")) {
				vieneSplash = bundle.getBoolean("vengo_de_splash");
			}
			isConnected = bundle.getBoolean("isConnected");
		}
		if (vieneSplash) {
			Log.e("vengo de esplash", "no");
			SharedPreferences preferences = getSharedPreferences(
					"PABS_bloqueo", Context.MODE_PRIVATE);
			if (preferences.contains("datos_cobrador")) {
				Util.Log.ih("existe PABS_bloqueo sharedPreferences file ");
				try {
					datosCobrador = new JSONObject(preferences.getString(
							"datos_cobrador", ""));
					((TextView) findViewById(R.id.tv_efectivo_bloqueada))
							.setText("$ "
									+ formatMoney(getTotalEfectivo()));
					((TextView) findViewById(R.id.tv_max_efectivo_bloqueo))
							.setText("$ "
									+ formatMoney(Double
											.parseDouble(datosCobrador
													.getString("max_efectivo"))));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (preferences.contains("idStatus")) {
					idStatus = preferences.getString("idStatus", "");
				}

				if (preferences.contains("configuracion_por_razon")) {
					tipoConfStatusRazon = preferences.getBoolean(
							"configuracion_por_razon", false);
				}

				if (preferences.contains("requesterCanceled")) {
					requesterCanceled = preferences.getBoolean(
							"requesterCanceled", false);
				}

				if (requesterCanceled) {
					requesterCanceled = false;
					if (preferences.contains("monto_depositado")) {
						montoDepositado = Double.parseDouble(preferences
								.getString("monto_depositado", "" + 0.0));
						Log.e("obt-monto depositado", ""
								+ montoDepositado);
					}
					if (tipoConfStatusRazon) {
						verificarStatusRazon();
						disableTextBoxes();
					} else {
						verificarStatusDeposito();
						disableTextBoxes();
					}
				}

				JSONObject bancos;
				try {
					bancos = new JSONObject(
							"{\"result\":[{\"no_banco\":\"1\",\"nombre\":\"Bancomer\"},{\"no_banco\":\"2\",\"nombre\":\"Banamex\"},{\"no_banco\":\"3\",\"nombre\":\"IXE Banco\"},{\"no_banco\":\"4\",\"nombre\":\"Santander\"},{\"no_banco\":\"5\",\"nombre\":\"HSBC\"},{\"no_banco\":\"6\",\"nombre\":\"Scotia Bank\"},{\"no_banco\":\"7\",\"nombre\":\"Inverlat\"},{\"no_banco\":\"8\",\"nombre\":\"BanRegio\"},{\"no_banco\":\"9\",\"nombre\":\"Inbursa\"},{\"no_banco\":\"10\",\"nombre\":\"Banco del Baj\u00edo\"},{\"no_banco\":\"12\",\"nombre\":\"Banorte\"},{\"no_banco\":\"11\",\"nombre\":\"Oficina\"}]}");
					manage_AllBanks(bancos);
					setConfiguraciones();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				Util.Log.ih("NO EXISTE PABS_bloqueo sharedPreferences file ");
				//try {
					//JSONObject json = query.obtenerCobrosDesincronizados();
					offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();
					if (offlinePayments != null && offlinePayments.size() > 0) {
						if (isConnected) {
							Util.Log.ih("si esta conectado - call registerPagos()");
							registerPagos();
						} else {
							Util.Log.ih("no esta conectado");
							setConfiguraciones();
							alertaDeCobrosOffline();
						}
					} else {
						setConfiguraciones();
					}
				//} catch (JSONException e) {
				//	e.printStackTrace();
				//}
				
			}
		} else {
			if (datosCobrador == null) {
				bundle = getIntent().getExtras();
				if (bundle != null) {
					if (bundle.containsKey("datos_cobrador")) {

						try {
							datosCobrador = new JSONObject(
									bundle.getString("datos_cobrador"));
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}

			}
			//try {
				//JSONObject json = query.obtenerCobrosDesincronizados();
				offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();
				if (offlinePayments != null && offlinePayments.size() > 0) {
					if (isConnected) {
						registerPagos();
					} else {
						setConfiguraciones();
						alertaDeCobrosOffline();
					}

				} else {
					setConfiguraciones();
				}
			//} catch (JSONException e) {
			//	e.printStackTrace();
			//}
			enviarDepositos();
		}

	}

	/**
	 * Sends to server all deposits made using Web Service
	 *
	 * 'http://50.62.56.182/ecobro/controldepositos/registerDepositosOffline'
	 * JSONObject param
	 * {
	 *     "no_cobrador": "";
	 *     "depositos": {},
	 *     "periodo": ,
	 * }
	 */
	public void enviarDepositos() {
		//SqlQueryAssistant query = new SqlQueryAssistant(getApplicationContext());
		if (DatabaseAssistant.isThereAnyDepositNotSynced()) {

			JSONObject json = new JSONObject();

			try {
				json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
				json.put("depositos",
						new JSONArray( new Gson().toJson(DatabaseAssistant.getAllDepositsNotSynced())));
				json.put("periodo", getEfectivoPreference()
						.getInt("periodo", 0));

			} catch (Exception e) {
				e.printStackTrace();
			}

			/*RequestResult requestResult = new RequestResult(this);

			requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
				@Override
				public void onRequestResult(NetworkResponse response) {
					try
					{
						manageRegisterDepositosOffline(new JSONObject(VolleyTickle.parseResponse(response)));
					}
					catch (JSONException ex)
					{
						ex.printStackTrace();
					}
				}

				@Override
				public void onRequestError(NetworkResponse error) {
					enviarDepositos();
				}
			});

			requestResult.executeRequest(ConstantsPabs.urlregisterPagosOffline, Request.Method.POST, json);*/
			requestEnviarDepositosVolley(json);

			/*NetService service = new NetService(
					ConstantsPabs.urlRegisterDepositosOffline, json);
			NetHelper.getInstance().ServiceExecuter(service, this,
					new onWorkCompleteListener() {

						@Override
						public void onCompletion(String result) {
							try {
								manageRegisterDepositosOffline(new JSONObject(
										result));
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}

						@Override
						public void onError(Exception e) {
							// TODO Auto-generated method stub
							enviarDepositos();
						}
					});*/

		}
	}

	private void requestEnviarDepositosVolley(JSONObject jsonParams) //Azael--
	{
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlRegisterDepositosOffline, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				manageRegisterDepositosOffline(response);
				Log.i("Request LOGIN-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						Log.e("regPagosOffline", "Error al registrar pagos offline");
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
	}

	/**
	 * Manages response of web service
	 * if failed to send deposits, it will try to send them again,
	 * @param json JSONObject with response of web service
	 * @param logDepositos List<LogRegister.LogDeposito> instance. deprecated in 3.0
	 */
	public void manageRegisterDepositosOffline(JSONObject json) {
		if (json.has("result")) {
			JSONObject result;
			try {
				result = json.getJSONObject("result");
				JSONArray almacenados = result.getJSONArray("almacenados");
				if (almacenados.length() > 0) {
					syncDepositsSavedToServer(almacenados);
				}
				JSONArray noAlmacenados = result.getJSONArray("repetidos");
				if (noAlmacenados.length() > 0) {
					//Toast.makeText(getMthis(), "Enviando depositos bancarios", Toast.LENGTH_SHORT).show();
					//enviarDepositos();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Sets all deposits that were just sent
	 * to server as sync (sync <- 1) so that does not resend deposits made
	 * causing data usage.
	 */
	public void syncDepositsSavedToServer(JSONArray almacenados) {
		try {
			for (int i = 0; i < almacenados.length(); i++) {
				JSONObject deposito = almacenados.getJSONObject(i);
				DatabaseAssistant.updateDepositsAsSynced(deposito.getString("fecha"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * sets all info related with time, collector and banks
	 */
	private void setConfiguraciones() {
		SharedPreferences preferences = getSharedPreferences("PABS_bloqueo",
				Context.MODE_PRIVATE);
		Bundle bundle = getIntent().getExtras();

		if (bundle != null) {
			if (bundle.containsKey("iniciar_time_sesion")) {
				boolean iniciarTimerSesion = bundle
						.getBoolean("iniciar_time_sesion");
				if (iniciarTimerSesion) {
					callSuperOnTouch = true;
					iniciarHandlerInactividad();
				}
			}

			if (bundle.containsKey("datos_cobrador")) {
				try {
					datosCobrador = new JSONObject(
							bundle.getString("datos_cobrador"));

					Editor editor = preferences.edit();
					editor.putString("datos_cobrador", datosCobrador.toString());
					editor.apply();
					SharedPreferences preference = getSharedPreferences(
							"folios", Context.MODE_PRIVATE);
					String foliomalva = preference.getString("ult_folio_malva",
							datosCobrador.getString("ult_folio_malva"));
					String folioapoyo = preference.getString("ult_folio_apoyo",
							datosCobrador.getString("ult_folio_apoyo"));
					Editor edit = preference.edit();
					edit.putString("ult_folio_malva", "" + foliomalva);
					edit.putString("ult_folio_apoyo", "" + folioapoyo);
					edit.apply();
					findViewById(R.id.btn_desbloqueo_deposito).setVisibility(
							View.VISIBLE);
					findViewById(R.id.btn_desbloqueo_razon).setVisibility(
							View.VISIBLE);

					((TextView) findViewById(R.id.tv_efectivo_bloqueada))
							.setText("$ "
									+ formatMoney(getTotalEfectivo()));
					((TextView) findViewById(R.id.tv_max_efectivo_bloqueo))
							.setText("$ "
									+ formatMoney(Double
											.parseDouble(datosCobrador
													.getString("max_efectivo"))));
					if (datosCobrador.getInt("efectivo") == 0) {
						Toast.makeText(
								getApplicationContext(),
								"Es necesario la conexión a Internet para registrar el nuevo efectivo.",
								Toast.LENGTH_LONG).show();
						finish();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

				JSONObject bancos;
				try {
					bancos = new JSONObject(
							"{\"result\":[{\"no_banco\":\"1\",\"nombre\":\"Bancomer\"},{\"no_banco\":\"2\",\"nombre\":\"Banamex\"},{\"no_banco\":\"3\",\"nombre\":\"IXE Banco\"},{\"no_banco\":\"4\",\"nombre\":\"Santander\"},{\"no_banco\":\"5\",\"nombre\":\"HSBC\"},{\"no_banco\":\"6\",\"nombre\":\"Scotia Bank\"},{\"no_banco\":\"7\",\"nombre\":\"Inverlat\"},{\"no_banco\":\"8\",\"nombre\":\"BanRegio\"},{\"no_banco\":\"9\",\"nombre\":\"Inbursa\"},{\"no_banco\":\"10\",\"nombre\":\"Banco del Baj\u00edo\"},{\"no_banco\":\"12\",\"nombre\":\"Banorte\"},{\"no_banco\":\"11\",\"nombre\":\"Oficina\"}]}");
					manage_AllBanks(bancos);
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		} else if (preferences.contains("datos_cobrador")) {
			try {
				datosCobrador = new JSONObject(preferences.getString(
						"datos_cobrador", ""));
				((TextView) findViewById(R.id.tv_efectivo_bloqueada))
						.setText("$ "
								+ formatMoney(getTotalEfectivo()));
				((TextView) findViewById(R.id.tv_max_efectivo_bloqueo))
						.setText("$ "
								+ formatMoney(Double.parseDouble(datosCobrador
										.getString("max_efectivo"))));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if (preferences.contains("idStatus")) {
				idStatus = preferences.getString("idStatus", "");
			}

			if (preferences.contains("configuracion_por_razon")) {
				tipoConfStatusRazon = preferences.getBoolean(
						"configuracion_por_razon", false);
			}

			if (preferences.contains("requesterCanceled")) {
				requesterCanceled = preferences.getBoolean("requesterCanceled",
						false);
			}

			if (requesterCanceled) {
				requesterCanceled = false;
				if (tipoConfStatusRazon) {
					verificarStatusRazon();
					disableTextBoxes();
				} else {
					verificarStatusDeposito();
					disableTextBoxes();
				}
			}

			try {
				JSONObject json = new JSONObject(
						"{\"result\":[{\"no_banco\":\"1\",\"nombre\":\"Bancomer\"},{\"no_banco\":\"2\",\"nombre\":\"Banamex\"},{\"no_banco\":\"3\",\"nombre\":\"IXE Banco\"},{\"no_banco\":\"4\",\"nombre\":\"Santander\"},{\"no_banco\":\"5\",\"nombre\":\"HSBC\"},{\"no_banco\":\"6\",\"nombre\":\"Scotia Bank\"},{\"no_banco\":\"7\",\"nombre\":\"Inverlat\"},{\"no_banco\":\"8\",\"nombre\":\"BanRegio\"},{\"no_banco\":\"9\",\"nombre\":\"Inbursa\"},{\"no_banco\":\"10\",\"nombre\":\"Banco del Baj\u00edo\"},{\"no_banco\":\"11\",\"nombre\":\"Oficina\"}]}");
				manage_AllBanks(json);
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
	}

	@Override
	public void onUserInteraction() {
		if (callSuperOnTouch) {
			super.onUserInteraction();
		} else {
			View refMain = null;
			if (Clientes.getMthis() != null) {
				/*refMain = Clientes.getMthis().getView().findViewById(
						R.id.listView_clientes);*/
				if (Clientes.getMthis().getView() != null)
					refMain = Clientes.getMthis().getView().findViewById(
							R.id.rv_clientes);
			}
			if (refMain != null) {
				try {
					Clientes.getMthis().llamarTimerInactividad();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			moveTaskToBack(true);
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (handlerSesion != null) {
			handlerSesion.removeCallbacks(myRunnable);
			handlerSesion = null;

		}
		wl.release();
		mthis = null;
	}

	@Override
    public void onResume() {
		super.onResume();
		mthis = this;
	}

	/**
	 * sets 'arrayBanks' variable with all available banks,
	 * banks that are shown in spinner to chose a bank to deposit to.
	 *
	 * @param json
	 * 			json with banks and position
	 */
	public void manage_AllBanks(JSONObject json) {
		if (json.has("result")) {
			try {
				arrayBanks = json.getJSONArray("result");
			} catch (JSONException e) {
				e.printStackTrace();
				mostrarMensajeError(ConstantsPabs.allBanks);
			}
		} else {
			mostrarMensajeError(ConstantsPabs.allBanks);
		}
	}

	/**
	 * manages response from server when either bank or office is selected
	 * @param json
	 */
	public void manage_UnlockDeposito(JSONObject json) {
		Spinner empresas = (Spinner) findViewById(R.id.spinnerEmpresas);
		if (json.has("result") && getBancoPosicion() < 11) {
			try {
				double nuevoEfectivo = json.getDouble("result");

				try {
					datosCobrador.put("efectivo", json.getString("result"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				float nuevoEfectivoEmpresa = getEfectivoPreference().getFloat(
						adapter.getEmpresaName(empresas
								.getSelectedItemPosition()), 0);
				nuevoEfectivoEmpresa = nuevoEfectivoEmpresa
						- (Float.parseFloat(((EditText) findViewById(R.id.et_monto))
								.getText().toString()));
				getEfectivoEditor().putFloat(
						adapter.getEmpresaName(empresas
								.getSelectedItemPosition()),
						nuevoEfectivoEmpresa).apply();
				;
				double maxEfectivo = datosCobrador.getDouble("max_efectivo");
				if (nuevoEfectivo > maxEfectivo) {
					Toast.makeText(
							mthis,
							getString(R.string.error_message_over_cash),
							Toast.LENGTH_SHORT).show();
					enableTextBoxes();
					((TextView) findViewById(R.id.tv_banco)).setText("");
					((TextView) findViewById(R.id.et_folio)).setText("");
					((TextView) findViewById(R.id.et_monto)).setText("");
					((TextView) findViewById(R.id.et_desbloqueo_razon))
							.setText("");

					((TextView) findViewById(R.id.tv_efectivo_bloqueada))
							.setText("$ " + getTotalEfectivo());
					((TextView) findViewById(R.id.tv_max_efectivo_bloqueo))
							.setText("$ "
									+ datosCobrador.getString("max_efectivo"));
				} else {
					// AlertDialog.Builder alert = new AlertDialog.Builder(
					// mthis);
					// alert.setMessage(getResources().getString(
					// R.string.desbloqueo_aceptado));
					// alert.setPositiveButton("Aceptar",
					// new DialogInterface.OnClickListener() {
					//
					// @Override
					// public void onClick(
					// DialogInterface dialog,int which) {
					// }
					// });
					// alert.create().show();

					Toast.makeText(mthis,
							getString(R.string.success_message_block),
							Toast.LENGTH_LONG).show();

					SharedPreferences sharedPreferences = getSharedPreferences(
							"PABS_SPLASH", Context.MODE_PRIVATE);
					Editor editor2 = sharedPreferences.edit();
					editor2.remove("idStatus");
					editor2.apply();

					// Lo que estaba en un alertdialog
					Intent intent = new Intent(mthis, MainActivity.class);
					try {
						datosCobrador.put("efectivo",
								json.getString("efectivo"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
					//SqlQueryAssistant query = new SqlQueryAssistant(
					//				getApplicationContext());

					Log.e("entrando", "" + montoDepositado);
					//query.insertarMontoDeposito(String.valueOf(montoDepositado));
					DatabaseAssistant.insertDeposit(montoDepositado, getEfectivoPreference().getInt("periodo", 0));

					if (DatabaseAssistant.getPrinterMacAddress() == null) {

						intent = new Intent(getApplicationContext(),
								Login.class);

						Toast.makeText(getApplicationContext(),
								"Inicia sesión nuevamente para continuar.",
								Toast.LENGTH_SHORT).show();

						DatabaseAssistant.deleteAllDeposits();
						startActivity(intent);
						finish();
					}
					intent.putExtra("datos_cobrador", datosCobrador.toString());
					SharedPreferences preferences = getSharedPreferences(
							"PABS_bloqueo", Context.MODE_PRIVATE);
					Editor editor = preferences.edit();
					editor.remove("idStatus");
					editor.remove("datos_cobrador");
					editor.remove("monto_depositado");
					editor.apply();
					mthis = null;
					finish();
					startActivity(intent);
					// //
				}
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			//
		} else {
			if (json.has("result")) {
				try {
					idStatus = json.getString("result");
					verificarStatusDeposito();
					disableTextBoxes();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				//try {
					//SqlQueryAssistant query = new SqlQueryAssistant(mthis);
					//JSONObject jsonData = query.obtenerCobrosDesincronizados();
					offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();
					if (offlinePayments != null && offlinePayments.size() > 0) {
						if (isConnected) {
							registerPagos();
						} else {
							setConfiguraciones();
							alertaDeCobrosOffline();
						}

					} else {
						setConfiguraciones();
					}
				//} catch (JSONException e) {
				//	e.printStackTrace();
				//}
			}

		}

	}

	/**
	 * manages response from server when is trying to unlock by reason given
	 * @param json
	 */
	public void manage_UnlockRazon(JSONObject json) {
		if (json.has("result")) {
			try {
				idStatus = json.getString("result");
				tipoConfStatusRazon = true;
				verificarStatusRazon();
				disableTextBoxes();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

	}


	public void manage_GetStatusDeposito(final JSONObject json) {
		if (status != -1 && !requesterCanceled) {
			try {
				status = json.getJSONObject("result").getInt("status");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if (status == 1 || status == 2) {
				findViewById(R.id.rl_cancelar_peticion)
						.setVisibility(View.GONE);

				if (status == 1)
				{
					if (getBancoPosicion() == 11) {
						String noTemp = getEfectivoPreference().getString(
								"no_cobrador", "");
						int periodo = getEfectivoPreference().getInt("periodo", 0);

						getEfectivoEditor().clear().apply();
						getEfectivoEditor().putString("no_cobrador", noTemp)
								.apply();
						try {
//							if (json.getJSONObject("result").has("periodo")) {
								getEfectivoEditor().putInt(
										"periodo",
										periodo).apply();

//							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						
						Intent intent = new Intent(mthis, MainActivity.class);
						intent.putExtra("action", "cierre");
						startActivity(intent);
						finish();
						
					}
					try {
						double nuevoEfectivo = json.getJSONObject("result")
								.getDouble("efectivo");
						try {
							datosCobrador.put(
									"efectivo",
									json.getJSONObject("result").getString(
											"efectivo"));
						} catch (JSONException e) {
							e.printStackTrace();
						}
						double maxEfectivo = datosCobrador
								.getDouble("max_efectivo");
						if (nuevoEfectivo > maxEfectivo) {
							Toast.makeText(
									mthis,
									getString(R.string.error_message_over_cash),
									Toast.LENGTH_SHORT).show();
							enableTextBoxes();
							((TextView) findViewById(R.id.tv_banco))
									.setText("");
							((TextView) findViewById(R.id.et_folio))
									.setText("");
							((TextView) findViewById(R.id.et_monto))
									.setText("");
							((TextView) findViewById(R.id.et_desbloqueo_razon))
									.setText("");

							((TextView) findViewById(R.id.tv_efectivo_bloqueada))
									.setText("$ "
											+ getTotalEfectivo());
							((TextView) findViewById(R.id.tv_max_efectivo_bloqueo))
									.setText("$ "
											+ datosCobrador
													.getString("max_efectivo"));
						} else {
							// AlertDialog.Builder alert = new
							// AlertDialog.Builder(
							// mthis);
							// alert.setMessage(getResources().getString(
							// R.string.desbloqueo_aceptado));
							// alert.setPositiveButton("Aceptar",
							// new DialogInterface.OnClickListener() {
							//
							// @Override
							// public void onClick(
							// DialogInterface dialog,int which) {
							// }
							// });
							// alert.create().show();

							Toast.makeText(mthis,
									getString(R.string.success_message_block),
									Toast.LENGTH_LONG).show();

							SharedPreferences sharedPreferences = getSharedPreferences(
									"PABS_SPLASH", Context.MODE_PRIVATE);
							Editor editor2 = sharedPreferences
									.edit();
							editor2.remove("idStatus");
							editor2.apply();

							
							// //
						}
					} catch (JSONException e1) {
						e1.printStackTrace();
					}

				} else if (status == 2) {
					enableTextBoxes();
					SharedPreferences sharedPreferences = getSharedPreferences(
							"PABS_SPLASH", Context.MODE_PRIVATE);
					Editor editor2 = sharedPreferences.edit();
					editor2.remove("idStatus");
					editor2.apply();
					showAlertDialog("", getResources().getString(R.string.error_message_block), false);
				}

				status = -1;
				SharedPreferences preferences = getSharedPreferences(
						"PABS_bloqueo", Context.MODE_PRIVATE);
				Editor editor = preferences.edit();
				editor.remove("idStatus");
				editor.remove("datos_cobrador");
				editor.apply();

			}
		}

	}

	public boolean isConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	

	public void manage_GetStatusRazon(JSONObject json) {
		if (status != -1 && !requesterCanceled) {
			try {
				status = json.getInt("result");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if (status == 1 || status == 2) {
				enableTextBoxes();
				findViewById(R.id.rl_cancelar_peticion)
						.setVisibility(View.GONE);

				if (status == 1) {
					AlertDialog.Builder alert = new AlertDialog.Builder(mthis);
					alert.setMessage(getResources().getString(
							R.string.success_message_block));
					alert.setPositiveButton("Aceptar", (dialog, which) -> {
						//SqlQueryAssistant query = new SqlQueryAssistant(
						//		getApplicationContext());
						if (DatabaseAssistant.getPrinterMacAddress() == null) {
							Intent intent = new Intent(
									getApplicationContext(),
									Login.class);
							Toast.makeText(
									getApplicationContext(),
									"Inicia sesión nuevamente para continuar.",
									Toast.LENGTH_SHORT).show();
							//try {
							//query.deleteDepositos();
							DatabaseAssistant.deleteAllDeposits();
							//} catch (JSONException e) {
							//	e.printStackTrace();
							//}
							startActivity(intent);
							finish();
						}
						Intent intent = new Intent(mthis,
								MainActivity.class);
						intent.putExtra("datos_cobrador",
								datosCobrador.toString());
						mthis = null;
						finish();
						startActivity(intent);
					});
					alert.create().show();

					SharedPreferences sharedPreferences = getSharedPreferences(
							"PABS_SPLASH", Context.MODE_PRIVATE);
					Editor editor2 = sharedPreferences.edit();
					editor2.remove("idStatus");
					editor2.apply();

				} else if (status == 2) {//-------------------------------------------------------------------------------------------------------------------------
					showAlertDialog("", getResources().getString(R.string.error_message_block), false);
				}

				status = -1;
				SharedPreferences preferences = getSharedPreferences(
						"PABS_bloqueo", Context.MODE_PRIVATE);
				Editor editor = preferences.edit();
				editor.remove("idStatus");
				editor.remove("datos_cobrador");
				editor.remove("configuracion_por_razon");
				editor.apply();

			}
		}

	}

	

	public void manage_getSaldoNoDepositado(JSONObject json) {
		saldo = new JSONObject();
		if (json.has("result"))
			Log.e("json", json.toString());
		{
			try {
				JSONArray array = json.getJSONArray("result");
				for (int i = 0; i < array.length(); i++) {
					saldo.put(array.getJSONObject(i).getString("empresa"),
							array.getJSONObject(i).getString("total"));
				}
				// paintSaldos();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}


	public void manage_ChangeStatusDeposito(JSONObject json) {
		if (json.has("result")) {

		} else {
			mostrarMensajeError(ConstantsPabs.changeStatusRazon);
		}
	}

	

	public void manage_ChangeStatusRazon(JSONObject json) {
		if (json.has("result")) {

		} else {
			mostrarMensajeError(ConstantsPabs.changeStatusDeposito);
		}
	}

	public void onClickFromXML(View v) {
		switch (v.getId()) {
		case R.id.fl_banco:
			if (findViewById(R.id.et_folio).isEnabled()) {
				showSpinnerBancos();
			}
			break;
		case R.id.btn_desbloqueo_deposito:
			if (findViewById(R.id.et_folio).isEnabled()) {
				crearPeticionDesbloqueoDeposito();
			}
			break;
		case R.id.btn_desbloqueo_razon:
			if (findViewById(R.id.et_folio).isEnabled()) {
				crearPeticionDesbloqueoRazon();
			}
			break;
		case R.id.btn_cancelar_pet:
			cancelAnyRequest();
			break;
		default:
			break;
		}
	}

	private void showSpinnerBancos() {
		if (arrayBanks != null) {
			ArrayList<String> listaAdapter = new ArrayList<String>();
			for (int i = 0; i < arrayBanks.length(); i++) {
				JSONObject jsonTmp;
				try {
					jsonTmp = arrayBanks.getJSONObject(i);
					if (jsonTmp.has("nombre")) {
						listaAdapter.add(jsonTmp.getString("nombre"));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
			BanksDialog dialog = new BanksDialog(this, listaAdapter, 0);
			dialog.show();

		} else {
			JSONObject bancos;
			try {
				bancos = new JSONObject(
						"{\"result\":[{\"no_banco\":\"1\",\"nombre\":\"Bancomer\"},{\"no_banco\":\"2\",\"nombre\":\"Banamex\"},{\"no_banco\":\"3\",\"nombre\":\"IXE Banco\"},{\"no_banco\":\"4\",\"nombre\":\"Santander\"},{\"no_banco\":\"5\",\"nombre\":\"HSBC\"},{\"no_banco\":\"6\",\"nombre\":\"Scotia Bank\"},{\"no_banco\":\"7\",\"nombre\":\"Inverlat\"},{\"no_banco\":\"8\",\"nombre\":\"BanRegio\"},{\"no_banco\":\"9\",\"nombre\":\"Inbursa\"},{\"no_banco\":\"10\",\"nombre\":\"Banco del Baj\u00edo\"},{\"no_banco\":\"12\",\"nombre\":\"Banorte\"},{\"no_banco\":\"11\",\"nombre\":\"Oficina\"}]}");
				manage_AllBanks(bancos);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

	}

	private void unlockRazon() {
		JSONObject json = new JSONObject();

		try {
			String noCobrador = "";
			try {
				noCobrador = datosCobrador.getString("no_cobrador");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			EditText etTmp = (EditText) findViewById(R.id.et_desbloqueo_razon);
			try {
				json.put("no_cobrador", noCobrador);
				json.put("razon", etTmp.getText().toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		/*RequestResult requestResult = new RequestResult(this);

		requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
			@Override
			public void onRequestResult(NetworkResponse response) {
				try
				{
					manage_UnlockRazon(new JSONObject(VolleyTickle.parseResponse(response)));
				}
				catch (JSONException ex)
				{
					ex.printStackTrace();
				}
			}

			@Override
			public void onRequestError(NetworkResponse error) {

			}
		});

		requestResult.executeRequest(ConstantsPabs.urlUnlockRazon, Request.Method.POST, json);*/

		requestUnlockRazonVolley(json);

		/*NetHelper.getInstance().ServiceExecuter(
				new NetService(ConstantsPabs.urlUnlockRazon, json), this,
				new onWorkCompleteListener() {

					@Override
					public void onError(Exception e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onCompletion(String result) {
						try {
							manage_UnlockRazon(new JSONObject(result));
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				});*/
	}

	private void requestUnlockRazonVolley(JSONObject jsonParams) //Azael--
	{
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlUnlockRazon, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				manage_UnlockRazon(response);
				Log.i("Request LOGIN-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						Log.e("regPagosOffline", "Error al registrar pagos offline");
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
	}

	private void crearPeticionDesbloqueoRazon() {
		requesterCanceled = false;
		EditText etTmp = (EditText) findViewById(R.id.et_desbloqueo_razon);
		if (etTmp.getText().toString().length() > 0) {
			if (isConnected) {
				unlockRazon();
			} else {
				showAlertDialog("",
						getString(R.string.error_message_network), false);
			}
		} else {
			showAlertDialog("", getString(R.string.error_message_fields), true);
		}
	}

	private void getStatusRazon() {
		JSONObject json = new JSONObject();
		try {
			json.put("id", "" + idStatus);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*RequestResult requestResult = new RequestResult(this);

		requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
			@Override
			public void onRequestResult(NetworkResponse response) {
				try
				{
					manage_GetStatusRazon(new JSONObject(VolleyTickle.parseResponse(response)));
				}
				catch (JSONException ex)
				{
					ex.printStackTrace();
				}
			}

			@Override
			public void onRequestError(NetworkResponse error) {

			}
		});

		requestResult.executeRequest(ConstantsPabs.urlGetStatusRazon, Request.Method.POST, json);*/

		requestGetStatusRazonVolley(json);

		/*NetHelper.getInstance().ServiceExecuter(
				new NetService(ConstantsPabs.urlGetStatusRazon, json), this,
				new onWorkCompleteListener() {

					@Override
					public void onError(Exception e) {

					}

					@Override
					public void onCompletion(String result) {
						try {
							manage_GetStatusRazon(new JSONObject(result));
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				});*/
	}

	private void requestGetStatusRazonVolley(JSONObject jsonParams) //Azael--
	{
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlGetStatusRazon, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				manage_GetStatusRazon(response);
				Log.i("Request LOGIN-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						Log.e("regPagosOffline", "Error al registrar pagos offline");
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
	}


	private void verificarStatusRazon() {
		findViewById(R.id.rl_cancelar_peticion).setVisibility(View.VISIBLE);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		InputMethodManager inputManager = (InputMethodManager) mthis
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(this.getCurrentFocus()
				.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		status = 0;
		new Thread(() -> {
			while (status == 0) {
				if (isConnected()) {
					SharedPreferences preferences = getSharedPreferences(
							"PABS_bloqueo", Context.MODE_PRIVATE);
					Editor editor = preferences.edit();
					editor.putString("idStatus", "" + idStatus);
					editor.putBoolean("configuracion_por_razon", true);
					editor.putBoolean("requesterCanceled", true);
					editor.apply();

					SharedPreferences sharedPreferences = getSharedPreferences(
							"PABS_SPLASH", Context.MODE_PRIVATE);
					Editor editor2 = sharedPreferences
							.edit();
					editor2.putString("idStatus", idStatus);
					editor2.apply();
					runOnUiThread(this::getStatusRazon);

				} else {
					runOnUiThread(() -> Toast.makeText(mthis, getResources().getString(R.string.error_message_network), Toast.LENGTH_SHORT).show());
				}
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	private void unlockDeposito() {
		JSONObject json = new JSONObject();
		String url;
		if (bancoPosicion == 11) {
			url = ConstantsPabs.urlUnlockDepositoViejo;
		} else {
			url = ConstantsPabs.urlUnlockDeposito;

		}
		try {
			String noCobrador = "";
			try {
				noCobrador = datosCobrador.getString("no_cobrador");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			JSONObject jsonBanco = arrayBanks.getJSONObject(bancoPosicion);
			String idBanco = jsonBanco.getString("no_banco");

			try {
				if (((Spinner) findViewById(R.id.spinnerEmpresas))
						.getSelectedItemPosition() == 0) {
					json.put("empresa", "00");
				} else {
					json.put(
							"empresa",
							adapter.getEmpresaId(((Spinner) findViewById(R.id.spinnerEmpresas))
									.getSelectedItemPosition()));
				}

				json.put("no_cobrador", noCobrador);
				json.put("monto", ((EditText) findViewById(R.id.et_monto))
						.getText().toString());
				json.put("no_banco", idBanco);
				json.put("periodo", getEfectivoPreference().getInt("periodo", 0));
				json.put("referencia", ((EditText) findViewById(R.id.et_folio))
						.getText().toString());
				json.put("timestamp",new Date().getTime());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			montoDepositado = Double
					.parseDouble(((EditText) findViewById(R.id.et_monto))
							.getText().toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*RequestResult requestResult = new RequestResult(this);

		requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
			@Override
			public void onRequestResult(NetworkResponse response) {
				try
				{
					manage_UnlockDeposito(new JSONObject(VolleyTickle.parseResponse(response)));
				}
				catch (JSONException ex)
				{
					ex.printStackTrace();
				}
			}

			@Override
			public void onRequestError(NetworkResponse error) {

			}
		});

		requestResult.executeRequest(url, Request.Method.POST, json);*/

		requestUnlockDepositoVolley(json);

		/*NetHelper.getInstance().ServiceExecuter(new NetService(url, json),
				this, new onWorkCompleteListener() {

					@Override
					public void onError(Exception e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onCompletion(String result) {
						Log.i("onCompletion",result);
						try {
							manage_UnlockDeposito(new JSONObject(result));
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				});*/
	}

	private void requestUnlockDepositoVolley(JSONObject jsonParams) //Azael--
	{
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlUnlockDeposito, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				manage_UnlockDeposito(response);
				Log.i("Request LOGIN-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						Log.e("regPagosOffline", "Error al registrar pagos offline");
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
	}

	private void crearPeticionDesbloqueoDeposito() {
		requesterCanceled = false;
		if (camposDepositoLlenos()) {

			try {
				float maxEfectivo = (float) getTotalEfectivo();
				String efecCob = ((EditText) findViewById(R.id.et_monto))
						.getText().toString();
				float efectivoCobrador;
				try {
					efectivoCobrador = Float.parseFloat(efecCob);
					if (efectivoCobrador > maxEfectivo) {
						showAlertDialog("", "Ingresa un monto menor a $"
								+ getTotalEfectivo(), false);
					} else {
						if (isConnected) {
							unlockDeposito();
						} else {
							showAlertDialog(
									"",
									getString(R.string.error_message_network),
									false);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			showAlertDialog("", getString(R.string.error_message_fields), true);
		}
	}

	/**
	 * gets deposit status from server
	 */
	private void getStatusDeposito() {
		JSONObject json = new JSONObject();

		try {
			json.put("no_deposito", "" + idStatus);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*RequestResult requestResult = new RequestResult(this);

		requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
			@Override
			public void onRequestResult(NetworkResponse response) {
				try
				{
					manage_GetStatusDeposito(new JSONObject(VolleyTickle.parseResponse(response)));
				}
				catch (JSONException ex)
				{
					ex.printStackTrace();
				}
			}

			@Override
			public void onRequestError(NetworkResponse error) {

			}
		});

		requestResult.executeRequest(ConstantsPabs.urlGetStatusDeposito, Request.Method.POST, json);*/
		requestGetStatusDepositoVolley(json);
		/*NetHelper.getInstance().ServiceExecuter(
				new NetService(ConstantsPabs.urlGetStatusDeposito, json), this,
				new onWorkCompleteListener() {

					@Override
					public void onError(Exception e) {

					}

					@Override
					public void onCompletion(String result) {
						try {
							manage_GetStatusDeposito(new JSONObject(result));
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				});*/
	}

	private void requestGetStatusDepositoVolley(JSONObject jsonParams) //Azael--
	{
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlGetStatusDeposito, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				manage_GetStatusDeposito(response);
				Log.i("Request LOGIN-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						Log.e("regPagosOffline", "Error al registrar pagos offline");
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
	}

	/**
	 * checks deposits status in server
	 */
	private void verificarStatusDeposito() {
		findViewById(R.id.rl_cancelar_peticion).setVisibility(View.VISIBLE);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		InputMethodManager inputManager = (InputMethodManager) mthis
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(this.getCurrentFocus()
				.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		status = 0;
		new Thread(() -> {
			while (status == 0) {
				if (isConnected) {
					SharedPreferences preferences = getSharedPreferences(
							"PABS_bloqueo", Context.MODE_PRIVATE);
					Editor editor = preferences.edit();
					editor.putString("idStatus", "" + idStatus);
					editor.putBoolean("configuracion_por_razon", false);
					editor.putBoolean("requesterCanceled", true);
					editor.putString("monto_depositado", ""
							+ montoDepositado);
					Log.e("monto dep.almacenado", "$ "
							+ montoDepositado);

					editor.apply();

					SharedPreferences sharedPreferences = getSharedPreferences(
							"PABS_SPLASH", Context.MODE_PRIVATE);
					Editor editor2 = sharedPreferences
							.edit();
					editor2.putString("idStatus", idStatus);
					editor2.apply();

					runOnUiThread(this::getStatusDeposito);

				} else {
					runOnUiThread(() -> Toast.makeText(mthis, getResources().getString(R.string.error_message_network), Toast.LENGTH_SHORT).show());
				}
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	/**
	 * cancel any request send to server
	 * gets called when 'cancel' is pressed
	 */
	private void cancelAnyRequest() {
		requesterCanceled = true;
		status = -1;
		SharedPreferences preferences = getSharedPreferences("PABS_bloqueo",
				Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putBoolean("requesterCanceled", false);
		editor.apply();

		findViewById(R.id.rl_cancelar_peticion).setVisibility(View.GONE);
		if (arrayBanks == null) {
			JSONObject bancos;
			try {
				bancos = new JSONObject(
						"{\"result\":[{\"no_banco\":\"1\",\"nombre\":\"Bancomer\"},{\"no_banco\":\"2\",\"nombre\":\"Banamex\"},{\"no_banco\":\"3\",\"nombre\":\"IXE Banco\"},{\"no_banco\":\"4\",\"nombre\":\"Santander\"},{\"no_banco\":\"5\",\"nombre\":\"HSBC\"},{\"no_banco\":\"6\",\"nombre\":\"Scotia Bank\"},{\"no_banco\":\"7\",\"nombre\":\"Inverlat\"},{\"no_banco\":\"8\",\"nombre\":\"BanRegio\"},{\"no_banco\":\"9\",\"nombre\":\"Inbursa\"},{\"no_banco\":\"10\",\"nombre\":\"Banco del Baj\u00edo\"},{\"no_banco\":\"12\",\"nombre\":\"Banorte\"},{\"no_banco\":\"11\",\"nombre\":\"Oficina\"}]}");
				manage_AllBanks(bancos);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		int casoTask = ConstantsPabs.changeStatusDeposito;
		if (tipoConfStatusRazon) {
			casoTask = ConstantsPabs.changeStatusRazon;
		}

		enableTextBoxes();

	}

	/**
	 * checks if deposit fields are filled with info
	 * @return
	 */
	private boolean camposDepositoLlenos() {
		boolean isValido = false;
		TextView banco = (TextView) findViewById(R.id.tv_banco);
		EditText folio = (EditText) findViewById(R.id.et_folio);
		EditText monto = (EditText) findViewById(R.id.et_monto);

		if ((banco.getText().toString().length() > 0)
				&& (folio.getText().toString().length() > 0)
				&& (monto.getText().toString().length() > 0)) {
			isValido = true;
		}
		return isValido;
	}

	/**
	 * disables textboxes
	 */
	private void disableTextBoxes() {
		findViewById(R.id.et_folio).setEnabled(false);
		findViewById(R.id.et_monto).setEnabled(false);
		findViewById(R.id.et_desbloqueo_razon).setEnabled(false);
	}

	/**
	 * enables textboxes
	 */
	private void enableTextBoxes() {
		findViewById(R.id.et_folio).setEnabled(true);
		findViewById(R.id.et_monto).setEnabled(true);
		findViewById(R.id.et_desbloqueo_razon).setEnabled(true);
	}

	/**
	 * shows error message
	 * @param caso
	 */
	public void mostrarMensajeError(final int caso) {
		alertShowing = true;
		AlertDialog.Builder alert = new AlertDialog.Builder(mthis);
		alert.setMessage("Hubo un error de comunicación con el servidor, verifique sus ajustes de red o intente más tarde.");
		alert.setPositiveButton("Salir", (dialog, which) -> {
			View refLogin = null;
			if (Login.getMthis() != null) {
				refLogin = Login.getMthis().findViewById(
						R.id.editTextUser);
			}

			View refSplash = null;
			if (Splash.mthis != null) {
				refSplash = Splash.mthis
						.findViewById(R.id.linear_splash);
			}

			if (refLogin != null) {
				Login.getMthis().finish();
				if (refSplash != null) {
					Splash.mthis.finish();
				}
			} else if (refSplash != null) {
				Splash.mthis.finish();
			}

			finish();

			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				if (bundle.containsKey("iniciar_time_sesion")) {
					if (handlerSesion != null) {
						handlerSesion.removeCallbacks(myRunnable);
						handlerSesion = null;

					}
				}
			}
		});
		alert.setNegativeButton("Reintentar", (dialog, which) -> {
			alertShowing = false;
			if (!isConnected) {
				mostrarMensajeError(caso);
			}
		});
		alert.create();
		alert.setCancelable(false);
		alert.show();
	}

	/**
	 * Gets context from this class.
	 * In some cases you cannot just call 'this', because another context is envolved
	 * or you want a reference to an object of this class from another class,
	 * then this context is helpful.
	 *
	 * NOTE
	 * Not recommended, it may cause NullPointerException though
	 *
	 * @return context from Bloqueo
	 */
	public static Bloqueo getMthis() {
		return mthis;
	}

	/**
	 * Sets 'mthis' global variable as Bloqueo's context
	 * @param mthis Bloqueo's context
	 */
	public static void setMthis(Bloqueo mthis) {
		Bloqueo.mthis = mthis;
	}

	/**
	 * returns position of bank selected
	 *
	 * @return
	 * 	 	Integer with postion of bank selected
	 */
	public int getBancoPosicion() {
		return bancoPosicion;
	}

	/**
	 * if office is selected, total cash in set in amount text field and
	 * company selector disappears,
	 * else, just company selector appears.
	 *
	 * @param bancoPosicion
	 * 			bank selector position selected
	 */
	public void setBancoPosicion(int bancoPosicion) {
		this.bancoPosicion = bancoPosicion;
		EditText etMonto = (EditText) findViewById(R.id.et_monto);
		if (bancoPosicion == 11) {
			((Spinner) findViewById(R.id.spinnerEmpresas)).setSelection(0);

			findViewById(R.id.spinnerEmpresas).setVisibility(View.GONE);
			try {
				etMonto.setText("" + getTotalEfectivo());
			} catch (Exception e) {
				etMonto.setText("");

				e.printStackTrace();
			}
		} else {
			findViewById(R.id.spinnerEmpresas).setVisibility(View.VISIBLE);

			((Spinner) findViewById(R.id.spinnerEmpresas)).setSelection(0);
			etMonto.setText("");

		}
	}

	/**
	 * deprecated in 3.0
	 * @return
	 */
	public boolean isAlertShowing() {
		return alertShowing;
	}

	/**
	 * deprecated in 3.0
	 * @return
	 */
	public void setAlertShowing(boolean alertShowing) {
		this.alertShowing = alertShowing;
	}

	/**
	 * start inactivity timer
	 * timer which ends session after 20 minutes
	 */
	public void llamarTimerInactividad() {
		iniciarHandlerInactividad();
	}

	@Override
	public void cerrarSesionTimeOut() {
		if (handlerSesion != null) {
			handlerSesion.removeCallbacks(myRunnable);
			handlerSesion = null;
		}

		View refLogin = null;
		if (Login.getMthis() != null) {
			refLogin = Login.getMthis().findViewById(R.id.editTextUser);
		}

		View refSplash = null;
		if (Splash.mthis != null) {
			refSplash = Splash.mthis.findViewById(R.id.linear_splash);
		}

		if (refLogin != null) {
			Login.getMthis().finish();
			if (refSplash != null) {
				Splash.mthis.finish();
			}
		} else if (refSplash != null) {
			Splash.mthis.finish();
		}
		mthis = null;
		finish();
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		EditText montoText = (EditText) findViewById(R.id.et_monto);

		try {
			if (arg2 != 0) {
				String empresa = adapter.getEmpresaName(arg2);
				Log.e("Empresa,", empresa);
				montoText.setText(""
						+ getEfectivoPreference().getFloat(empresa, 0));
			} else {
				montoText.setText("0");
			}
		} catch (Exception e) {
			montoText.setText("0");

			e.printStackTrace();
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}
}