package com.jaguarlabs.pabs.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidadvance.topsnackbar.TSnackbar;
import com.google.gson.Gson;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.components.PreferenceSettings;
import com.jaguarlabs.pabs.components.Preferences;
import com.jaguarlabs.pabs.components.PreferencesToShowWallet;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.ExportImportDB;
import com.jaguarlabs.pabs.database.Payments;
import com.jaguarlabs.pabs.database.Suspensionitem;
import com.jaguarlabs.pabs.database.MotivosDeCancelacion;
import com.jaguarlabs.pabs.models.ModelPayment;
import com.jaguarlabs.pabs.networkingUtility.Ping;
import com.jaguarlabs.pabs.util.*;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.splunk.mint.Mint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;
import pl.tajchert.nammu.PermissionListener;

/**
 * This class is in charge of managing the access to the app.
 * To be able to log in bluetooth and GPS have to be turned on.
 * There's two ways to get in:
 * when it's the first time trying to log in, internet access is
 * needed as well as when deposit to office is made ('cierre de periodo' done),
 * doing use of a Web Service to download info (collector's data, account info,
 * companies data, etc).
 * Also without internet access but only after one time logged in with internet.
 */
@SuppressWarnings({"SpellCheckingInspection", "EmptyCatchBlock"})
@SuppressLint("SimpleDateFormat")
public class Login extends ExtendedActivity {

	//region Variables

	//region Objects

	//region Statics
	public TextView tvInternet;
	Dialog dialogo_de_bloqueo;
	Button btSolicitarDesbloqueo;
	ProgressBar progressBar;
	boolean isShowedLockedApp = false;
	private Timer timerNotificaciones;
	private String robado="";
	private static Login mthis = null;
	ProgressDialog progress;
	//endregion
	public String bandera="";
	private String user;
	private String password;
    Dialog myDialog;
    private String dias_notificacion_capturar_detalle_cliente="";
	private List<ModelPayment> offlinePayments;
	private Button buttonLogin;
	private ImageView btnConfiguracion;

	//endregion

	//region Primitives

    public  boolean isButtonClicked = false;

	private boolean isAnotherMobile = false;

    final PermissionCallback permissionCallback = new PermissionCallback()
	{
        @Override
        public void permissionGranted() {
            //ApplicationResourcesProvider.startLocationUpdates();
			if (Nammu.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE))
				getDataAndFoliosNotSavedToServerAfterErasingAppData();
        }

        @Override
        public void permissionRefused() {
        }
    };

    private RelativeLayout loginLayout;

	//endregion

	//endregion

	//region Lifecycle

	@Override
	public void onResume() {
		super.onResume();
		mthis = this;

		Nammu.permissionCompare(new PermissionListener() {
			@Override
			public void permissionsChanged(String s) {

			}

			@Override
			public void permissionsGranted(String s) {
				Log.d("Location permission", s);
			}

			@Override
			public void permissionsRemoved(String s) {
				Nammu.askForPermission(Login.this, s, permissionCallback);
			}
		});

		String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

		if (Nammu.hasPermission(this, permissions)) {
			ApplicationResourcesProvider.startLocationUpdates();
			getDataAndFoliosNotSavedToServerAfterErasingAppData();
		} else {
			Nammu.askForPermission(this, permissions, permissionCallback);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		ApplicationResourcesProvider.stopLocationUpdates();
	}

	//endregion

	//region Methods

	//region Public

	//endregion

	//region Private

    private String getPrinterSerial()
    {
        String serial = "";
        int retries = 0;
        while(retries < 2) {
			try {
				BluetoothPrinter.getInstance().connect();

				serial = BluetoothPrinter.getInstance().sendcommand_getString("OUT \"\";_SERIAL$").split("\r\n")[0];

				//Snackbar.make(loginLayout, serial, Snackbar.LENGTH_SHORT).show();

				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
					BluetoothPrinter.getInstance().disconnect();

				break;
			} catch (Exception ex) {
				ex.printStackTrace();
				retries++;
			}
		}

        return serial;
    }

	private void getDataAndFoliosNotSavedToServerAfterErasingAppData() {

		DatabaseAssistant.getRadioToShowNearbyClients();
		Util.Log.eh(" getPrinterMacAddress = " + DatabaseAssistant.getRadioToShowNearbyClients());

		Util.Log.ih("!(this instanceof Splash)");

		//TODO NEW PROCESS
		SharedPreferences appSharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		boolean IS_DATA_CLEARED =  appSharedPrefs.getBoolean("IS_DATA_CLEARED",true);
		Util.Log.eh(" IS_DATA_CLEARED = " + IS_DATA_CLEARED);
		if ( IS_DATA_CLEARED ) {




			Util.Log.eh(" importing database = " + IS_DATA_CLEARED);

			ExportImportDB exportImportDB = new ExportImportDB();
			boolean successImporting = false;
			try {
				successImporting = exportImportDB.importDBHidden();
				if ( successImporting ) {
					Util.Log.i("Base de datos importada correctamente");
				} else {
					Util.Log.i("Hubo un error al importar la base de datos.");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			if ( successImporting )
			{
				//encerrar en try-catch
				try
				{
					SharedPreferences preferencesfolios = getSharedPreferences("folios", Context.MODE_PRIVATE);
					Editor editFolio = preferencesfolios.edit();
					//editFolio.putString(company.getName(), folioCompany);
					//editFolio.commit();

					List<Payments> lastPaymentFirstCompany = Payments.findWithQuery(Payments.class, "SELECT * FROM PAYMENTS WHERE company = ? AND folio != ? ORDER BY id desc", new String[]{"01", "----"});
					if (lastPaymentFirstCompany.size() > 0) {

						String companyName = Companies.find(Companies.class, "id_company = ?", "01").get(0).getName();
						String lastFolio = lastPaymentFirstCompany.get(0).getFolio();

						editFolio.putString(companyName, lastFolio);
						editFolio.commit();
					}
					List<Payments> lastPaymentSecondCompany = Payments.findWithQuery(Payments.class, "SELECT * FROM PAYMENTS WHERE company = ? AND folio != ? ORDER BY id desc", new String[]{"03", "----"});
					if (lastPaymentSecondCompany.size() > 0) {

						String companyName = Companies.find(Companies.class, "id_company = ?", "03").get(0).getName();
						String lastFolio = lastPaymentSecondCompany.get(0).getFolio();

						editFolio.putString(companyName, lastFolio);
						editFolio.commit();
					}
					List<Payments> lastPaymentThirdCompany = Payments.findWithQuery(Payments.class, "SELECT * FROM PAYMENTS WHERE company = ? AND folio != ? ORDER BY id desc", new String[]{"04", "----"});
					if (lastPaymentThirdCompany.size() > 0) {

						String companyName = Companies.find(Companies.class, "id_company = ?", "04").get(0).getName();
						String lastFolio = lastPaymentThirdCompany.get(0).getFolio();

						editFolio.putString(companyName, lastFolio);
						editFolio.commit();
					}
					List<Payments> lastPaymentFourthCompany = Payments.findWithQuery(Payments.class, "SELECT * FROM PAYMENTS WHERE company = ? AND folio != ? ORDER BY id desc", new String[]{"05", "----"});
					if (lastPaymentFourthCompany.size() > 0) {

						String companyName = Companies.find(Companies.class, "id_company = ?", "05").get(0).getName();
						String lastFolio = lastPaymentFourthCompany.get(0).getFolio();

						editFolio.putString(companyName, lastFolio);
						editFolio.commit();
					}

					Util.Log.ih("folios actualizados");
				}
				catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				Util.Log.ih("folios NO actualizados");
			}

			com.jaguarlabs.pabs.database.Collectors collectorInfo = DatabaseAssistant.getCollector();
			String message = "Se borró la información de la app de cobranza del cobrador " + collectorInfo.getIdCollector() + " - " + collectorInfo.getName();
			Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
			if (myLocation != null)
				DatabaseAssistant.insertOsticket( "Borrado de datos", message, myLocation );
            else
            {
                final Location lastKnownLocation = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation();
                if (lastKnownLocation != null)
                    new Handler().postDelayed(() -> DatabaseAssistant.insertOsticket("Borrado de datos", message, lastKnownLocation), 3500);

            }
			//executeSendOsticket();


		} else {
			Util.Log.eh(" NOT importing database = " + IS_DATA_CLEARED);
		}
		//TODO END NEW PROCESS



		//SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		boolean IS_DATA_CLEARED2 =  appSharedPrefs.getBoolean("IS_DATA_CLEARED",true);
		if ( IS_DATA_CLEARED2 ) {
			Util.Log.ih("if ( IS_DATA_CLEARED ) {");
			SharedPreferences.Editor editor = appSharedPrefs.edit();
			editor.putBoolean("IS_DATA_CLEARED",false);
			editor.apply();
		}

	}

	/**
	 * checks that spelled user name and password are correct
	 * comparing with data stored into sharedPreferences file 'PABS_Login'
	 * @return
	 */
	private boolean camposCorrectos() {
		boolean estado = false;
		SharedPreferences preferences = getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
		String userPref = preferences.getString("user_login", "no hay user");
		String passwordPref = preferences.getString("password_login",
				"no hay pass");
		if (userPref.equals(user) && passwordPref.equals(password)) {
			estado = true;
		}

		return estado;
	}



	/**
	 * this is for debugging purposes
	 */
	private void throwAnErrorOnPurpose(){
		String a = "something";
		a = null;
		a.toString();
	}

	/**
	 * Este metodo verifica si los campos de usuario y contrase�a esta llenos
	 *
	 * @return Un booleano con la respuesta
	 */
	private boolean camposLlenos()
	{

		Util.Log.ih( DatabaseAssistant.getRadioToShowNearbyClients() );

		//Util.Log.ih("today = " + Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
		//Util.Log.ih("getDateFromSharedPreference = " + getDateFromSharedPreference());
		boolean isValido = false;
		EditText euser = (EditText) findViewById(R.id.editTextUser);
		EditText epass = (EditText) findViewById(R.id.editTextPass);
		user = euser.getText().toString().replace(" ", "");
		password = epass.getText().toString();
		if (user.length() > 0 && password.length() > 0) {
			isValido = true;
		}

		if (user.equals("admin%registerPayments")){
			LogRegister.registrarCheat(user);
			toastL("Registrando cobros...");
			RegisterPagos(false);
			return false;
		} else if (user.equals("admin%registerAllPayments")){
			LogRegister.registrarCheat(user);
			toastL("Registrando todos los cobros...");
			RegisterPagos(true);
			return false;
		} else if (user.equals("admin%internet")){
			LogRegister.registrarCheat(user);
			setInternetAsReachable = !setInternetAsReachable;
			toastL("Internet " + (setInternetAsReachable ? "habilitado" : "deshabilitado")  + " manualmente");
			return false;
		} else if (user.equals("turnColors%OFF")) {
			LogRegister.registrarCheat(user);
			PreferenceSettings preferenceSettings = new PreferenceSettings(ApplicationResourcesProvider.getContext());
			preferenceSettings.turnColorContracts(false);
			toastL("Semáforo desactivado");
			return false;
		} else if (user.equals("turnColors%ON")) {
			LogRegister.registrarCheat(user);
			PreferenceSettings preferenceSettings = new PreferenceSettings(ApplicationResourcesProvider.getContext());
			preferenceSettings.turnColorContracts(true);
			toastL("Semáforo activado");
			return false;
		} else if (user.equals("exportDB%")) {
			LogRegister.registrarCheat(user);
			ExportImportDB exportImportDB = new ExportImportDB();
			try {
				if ( exportImportDB.exportDB() ) {
					toastL("Base de datos exportada correctamente");
				} else {
					toastL("Hubo un error al exportar la base de datos.");
				}
			} catch (IOException e){
				e.printStackTrace();
			}
		} else if (user.contains("changeServerIP%")) {

			try {
				String IP = user.toString().substring(
						user.toString().indexOf("%") + 1,
						user.toString().lastIndexOf("%")
				);
				LogRegister.registrarCheat(user);
				Util.Log.ih("IP obtenida = " + IP);

				Preferences preferencesServerIP = new Preferences(ApplicationResourcesProvider.getContext());
				String oldServerIP = preferencesServerIP.getServerIP();

				preferencesServerIP.changeServerIP( IP );
				toastL("La IP a cambiado a: " + IP + "\nIP anterior: " + oldServerIP);
			} catch (StringIndexOutOfBoundsException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		} else if (user.contains("resetDB%")) {
			LogRegister.registrarCheat(user);
			try {
				DatabaseAssistant.resetDatabase( ApplicationResourcesProvider.getContext() );
				getSharedPreferences("folios", 0).edit().clear().commit();
                getEfectivoPreference().edit().clear().commit();

				//ApplicationResourcesProvider.getContext().getSharedPreferences("folios", 0).edit().clear().commit();

				toastL("reseteando base de datos");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		} else if (user.equals("debug%ON")) {
			LogRegister.registrarCheat(user);
			BuildConfig.setDEBUG( true );
			toastL("debug activado");
			return false;
		} else if (user.equals("debug%OFF")) {
			LogRegister.registrarCheat(user);
			BuildConfig.setDEBUG( false );
			toastL("debug desactivado");
			return false;
        } else if (user.equals("resetSemaforo%")) {
            LogRegister.registrarCheat(user);
            DatabaseAssistant.resetSemaforo();
            toastL("Semáforo reiniciado");
            return false;
		} else if (BuildConfig.isWalletSynchronizationEnabled()) {
			Util.Log.ih("SEMAFORO ACTIVADO");
		}

		return isValido;
	}

	/**
	 * checks that current cash is under cash limit
	 *
	 * @param datosCobrador collector data
	 * @return true if current cash is under cash limit
	 * 		   false if current cash is greater than cash limit
	 */
	private boolean canUseApp(JSONObject datosCobrador) {
		double efectivo = 0.0f;
		try {
			efectivo = datosCobrador.getDouble("efectivo");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		double maxEfectivo = 0.0f;
		try {
			maxEfectivo = datosCobrador.getDouble("max_efectivo");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return efectivo < maxEfectivo;
	}

	/**
	 * loads user to show it on user field when app is open,
	 * if user is available
	 */
	private void cargarUsuario() {
		SharedPreferences preferences = getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
		String userLoader = preferences.getString("user_login", "");
		((TextView) findViewById(R.id.editTextUser)).setText(userLoader);
	}

	/**
	 * Sometimes the device time changes, when this happens,
	 * the user ought to change this time either hour or date
	 * before logging in.
	 * So this method validates that device
	 * time is under a limit of 20 minutes difference regard of
	 * server time.
	 * Works when has internet access
	 *
	 * @param date server time
	 * @return true if device time is within difference limit
	 * @return false if device is not within difference limit
	 */
	private boolean CheckDate(String date) {
		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
				Locale.getDefault());
		Date parsed = new Date();
		try {
			parsed = inputFormat.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Log.e("parsed" + parsed.toGMTString(),
				" Actual time" + new Date().toGMTString());
		long serverTime = parsed.getTime();
		long localTime = new Date().getTime();
		long differences = Math.abs(serverTime - localTime);
		Log.e("differences", "" + differences);
		return (differences > 12000000);//1200000

	}

	/**
	 * processes to log in when there's no internet (if it's allowed)
	 * starting Clientes screen which is the main screen
	 * if there's internet calls logIn()
	 *
	 * @param inicarSesionOffline boolean telling if it's possible
	 *                            to get in without internet
	 */
	private void iniciarSesion(boolean inicarSesionOffline) {
		PreferencesToShowWallet preferencesToShowWallet = new PreferencesToShowWallet(ApplicationResourcesProvider.getContext());
		preferencesToShowWallet.setResetContractsPerWeek();
		PreferenceSettings preferenceSettings = new PreferenceSettings(ApplicationResourcesProvider.getContext());
		BuildConfig.setWalletSynchronizationEnabled(preferenceSettings.isColorContractsActive());
		String a = ConstantsPabs.urlAllBanks;

		SharedPreferences preferences = getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
		boolean isSupervisor = preferences.getBoolean("isSupervisor", false);


		Log.e("isConnected", "" + isConnected);
		if (isConnected) {
			logIn();
		} else if (inicarSesionOffline) {

			if (camposCorrectos()) {

				if(!checkDateWhenNoConnectedToInternet())
				{
					//offline mode
					logInOfflineMode();
				}
				else{
					buttonLogin.setEnabled(true);
					try {
						hasActiveInternetConnection(this, loginLayout);
						SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
						Date date = new Date();

						AlertDialog.Builder alert = getAlertDialogBuilder();
						alert.setMessage("Fecha Actual: " + formatter.format(date) + "\nPor favor verifica la fecha del dispositivo y conéctate a internet.\nDESEAS CONTINUAR?");
						alert.setPositiveButton("SI",(dialogInterface, which) -> {
							AlertDialog.Builder confirmDialog = getAlertDialogBuilder();
							confirmDialog.setMessage("Estas seguro?");
							confirmDialog.setPositiveButton("SI",(dialogInterface1, i) -> logInOfflineMode());
							confirmDialog.setNegativeButton("Cancelar",(dialogInterface1, i) -> {});
							confirmDialog.create();
							confirmDialog.setCancelable(false);
							confirmDialog.show();
						});
						alert.setNegativeButton("Cancelar",(dialogInterface, i) -> {});
						alert.create();
						alert.setCancelable(false);
						alert.show();



					} catch (Exception e){
						Toast toast = Toast.makeText(getApplicationContext(), R.string.error_message_user_pass, Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						e.printStackTrace();
					}
				}
			} else {
				buttonLogin.setEnabled(true);
				try {
					showAlertDialog("", getString(R.string.error_message_user_pass),
							false);
				} catch (Exception e){
					Toast toast = Toast.makeText(getApplicationContext(), R.string.error_message_user_pass, Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					e.printStackTrace();
				}
			}
		} else {
			buttonLogin.setEnabled(true);
			try {
				showAlertDialog("",
						getString(R.string.error_message_login),
						true);
				hasActiveInternetConnection(this, loginLayout);
			} catch (Exception e){
				Toast toast = Toast.makeText(getApplicationContext(), R.string.error_message_login, Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				e.printStackTrace();
			}
		}
	}

	private void logInOfflineMode(){
		buttonLogin.setEnabled(true);
		SharedPreferences preferences = getSharedPreferences(
				"PABS_Login", Context.MODE_PRIVATE);
		String datosString = "";

		if (preferences.contains("statusBloqueo"))
        {
            if (preferences.getInt("statusBloqueo", 0) == 1)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle("Bloqueado");
                builder.setMessage("Tu aplicación esta bloqueada. No puedes cobrar hasta mañana");
                builder.setNeutralButton("Aceptar",(dialogInterface, i) -> {});

                builder.show();
                return;
            }
        }
		if (preferences.contains("datos_cobrador")) {
			datosString = preferences.getString("datos_cobrador", "");
		}

		//Splunk Mint instruction to report an event of 'Inicio de Sesion'
		//Mint.logEvent("Inicio de Sesion - " + user + " - " + new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()), MintLogLevel.Info);

		/*
		check if it's day 1 and period hasn't been closed yet
		if so, start a new period
		 */
		DatabaseAssistant.insertNewPeriodFromScratchIfNeeded( getEfectivoPreference().getInt("periodo", 0) );
		if ( DatabaseAssistant.checkThatPeriodWasClosedOnLastDayOfMonth( getEfectivoPreference().getInt("periodo", 0) ) ) {

			List<Companies> companies = DatabaseAssistant.getCompanies();
			/*
			int size;
			switch (BuildConfig.getTargetBranch()){
				case GUADALAJARA:
					size = companies.size();
					break;
				case TOLUCA:
					size = 2;
					break;
				case PUEBLA:
					size = 2;
					break;
				case MERIDA:
					size = 1;
					break;
				case SALTILLO:
					size = 1;
					break;
				default:
					size = 4;
			}
			*/

			for (int cont = 0; cont < companies.size(); cont++) {
				Companies company = companies.get(cont);

				DatabaseAssistant.insertCompanyAmountsByPeriodWhenMoreThanOnePeriodIsActive(
						company.getName(),
						getEfectivoPreference().getFloat(company.getName(), 0),
						getEfectivoPreference().getInt("periodo", 0)
				);

				getEfectivoEditor().putFloat(company.getName(), 0).apply();
			}

			getEfectivoEditor().putInt("periodo", getEfectivoPreference().getInt("periodo", 0) + 1).apply();
		}

		Intent intent = new Intent(this, MainActivity.class);
		intent.putExtra("user_login", user);
		intent.putExtra("datos_cobrador", datosString);
		startActivity(intent);
		if(timerNotificaciones!=null)
			timerNotificaciones.cancel();
		finish();
	}

	/**
	 * loads user
	 * targeted for Splunk Mint so that if app throws an error, this
	 * error will have an id associated with collector
	 *
	 * @return collector's username
	 */
	private String loadUser() {
		return getSharedPreferences("PABS_Login", Context.MODE_PRIVATE).getString("user_login", "no hay datos");
	}

	/**
	 * When loggin has to be with internet access this method
	 * get launched
	 *
	 * Web Service
	 * 'http://50.62.56.182/ecobro/controlusuarios/loginCobrador'
	 *
	 * JSONObject param
	 * {
	 * 	"nombre":"c0003",
	 * 	"id_movil":"TA92914SHV",
	 * 	"app_version":"3.0.31",
	 * 	"android_version":"5.0.2",
	 * 	"password":"sistemas"
	 * }
	 */
	private void logIn()
	{
		JSONObject json = new JSONObject();
		try {
			json.put("nombre", user);
			json.put("id_movil", Build.SERIAL);
			PackageInfo pInfo;
			try {
				String deviceid = null;
				try {
					TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
					if (Nammu.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
					    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
					        deviceid = manager.getImei();
					    else
                            deviceid = manager.getDeviceId();
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
				pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
				String version = pInfo.versionName;
				json.put("app_version", version);
				json.put("android_version", Build.VERSION.RELEASE);
				json.put("imei", deviceid);
				json.put("serie_impresora", DatabaseAssistant.getSerialPrinter());
				json.put("android_model", android.os.Build.MODEL);

			} catch (NameNotFoundException e1) {
				e1.printStackTrace();
			}
			json.put("password", password);
		} catch (Exception e) {
			log("postException", "" + e);
		}

		requestLogin(json, loginLayout);



		/*NetService service = new NetService(ConstantsPabs.urlLoginCobrador, json);

		//toastSReuse("Iniciando...");
		Snackbar.make(loginLayout, "Iniciando...", Snackbar.LENGTH_SHORT).show();

		NetHelper.getInstance().ServiceExecuter(service, this, new NetHelper.onWorkCompleteListener()
		{
			@Override
			public void onCompletion(String result) {
				try {
					manage_CheckAuthentication(new JSONObject(result));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onError(Exception e) {
				buttonLogin.setEnabled(true);
				isConnected = false;
				Log.e("no se puede", "iniciar");
				Toast toast = Toast.makeText(getApplicationContext(), R.string.error_message_call, Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
		});*/
	}

	private void requestLogin(JSONObject jsonParams, View view)
	{
		progress = ProgressDialog.show(this, "Iniciando sesión", "Por favor espera...", true);
		//Snackbar.make(view, "Iniciando...", Snackbar.LENGTH_SHORT).show();
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlLoginCobrador, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				manage_CheckAuthentication(response);
				if(progress!=null)
					progress.dismiss();
				Log.i("Request LOGIN-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						if(progress!=null)
							progress.dismiss();
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
	}

	/**
	 * This method gets and processes response from server.
	 * Sets a lot of data to be use with collector's info.
	 * Also saves companies into database as well as saves
	 * folios of each company into sharedPreferences file,
	 * ending up with starting Clientes screen.
	 *
	 * @param json
	 *            JSONOBject with all info that the app
	 *            needs to run properly. Info of companies,
	 *            collector and device.
	 *
	 *            {
					"resultado": {
					"no_cobrador": "644",
					"serie_malba": "L.",
					"serie_programa": "L",
					"serie_empresa4": "L,",
					"serie_empresa5": "L!",
					"nombre": "JOSE CRUZ RUIZ SILVA",
					"ult_folio_malva": "14",
					"ult_folio_apoyo": "24",
					"ult_folio_empresa4": "28",
					"ult_folio_empresa5": "5",
					"efectivo": "1038",
					"max_efectivo": "500",
					"id_impresora": "00:19:0E:A0:93:CA",
					"perimetro": "0",
					"periodo": "2"
					},
					"mensaje": "En PABS estamos para servirle y queremos que tengas un buen servicio en nuestras nuevas instalaciones con calidez y calidad.",
					"fecha_servidor": "2015-10-01 18:05:14",
					"sesion": true,
					"id_movil_anterior": {
					"id_movil": "TA92914SHV"
					},
					"resultado_pagos": {
					"resultado": "No se ingresaron pagos para registrar"
					}
				  }
	 */



	private void manage_CheckAuthentication(JSONObject json)
	{
	    String mensajeTicketNotice="vacio", empresas_excluidas_de_captura_de_informacion="";
	    String timeMinutesForSaveLocations="";
		//String mensajeTicketNotice="<4>++++++++++PROGRAMA DE APOYO++++++++++</><4>+++++++++DE BENEFICIO SOCIAL+++++++++</>                                     <3>+++++++++++++GUADALAJARA+++++++++++++</>                                           Joaquin Angulo 2565, Colonia        Ladrón de Guevara, C.P. 44600          Teléfono: (33) 3415 0217                        y                              (33)3615 0207                                                 <4>+++++++++AVISO DE SUSPENSION+++++++++</><4>++++++++++TEMPORAL DE PAGOS++++++++++</>                                     Fecha actual: #fechaActual          $Cliente: #nombreCompleto            $No.Contrato: #contrato              $Motivo: #motivo                     $Fecha de reanudacion: #fechar       $                                                                          _____________________________________          firma del cliente                                                                                                                         _____________________________________          firma del cobrador                                              Por medio del presente me comprometo a reanudar mis abonos al PROGRAMA DE  APOYO DE BENEFICIO SOCIAL en el día  que queda especificado arriba y que  yo mismo indique. En caso contrario  me sujetare a lo establecido en el                contrato.              ";
		//String mensajeTicketNotice="<4>+++++++++AVISO XX SUSPENSION+++++++++</><4>++++++++++TEMPORAL DE PAGOS++++++++++</>                                     Cliente: #nombreCompleto            $No.Contrato: #contrato              $Motivo: #motivo                     $Fecha de reanudacion: #fechar       $                                                                          _____________________________________          Firma del cliente                                                                                                                         _____________________________________          Firma del cobrador                                              Por medio del presente me comprometo a reanudar mis abonos al PROGRAMA DE  APOYO DE BENEFICIO SOCIAL en el día  que queda especificado arriba y que  yo mismo indique. En caso contrario  me sujetare a lo establecido en el                contrato.                    Teléfono: (33) 3415 0217                         y                              (33) 3615 0207           ";
		Log.i("manage_CheckAuth", json.toString());

		if (!json.has("error"))
		{
			SharedPreferences preferences = getSharedPreferences("PABS_Login"+"", Context.MODE_PRIVATE);
			try {
				JSONObject resultado = json.getJSONObject("resultado");

				if (resultado.has("statusBloqueo")) {
					try {

						Editor editor = preferences.edit();
						editor.putInt("statusBloqueo", resultado.getInt("statusBloqueo"));
						editor.apply();

						if (resultado.getInt("statusBloqueo") == 1)
						{
							AlertDialog.Builder builder = new AlertDialog.Builder(this);
							builder.setTitle("Bloqueado");
							builder.setMessage("Tu aplicación esta bloqueada. No puedes cobrar hasta mañana");
							builder.setNeutralButton("Aceptar", (dialogInterface, i)  -> {});
							builder.show();
							return;
						}

					} catch (JSONException ex) {
						ex.printStackTrace();
					}
				}

				if (resultado.has("fecha_cierre"))
				{
					Editor editor = preferences.edit();
					editor.putString("fechaCierre", resultado.getString("fecha_cierre"));
					editor.apply();
				}

			}
			catch (JSONException ex)
			{
				ex.printStackTrace();
			}

			//String userPref = preferences.getString("user_login", "");

			Editor editor = preferences.edit();
			editor.putString("user_login", user);
			editor.putString("fecha_sesion", new Date().toGMTString());
			editor.apply();

			JSONObject datosCobrador;
			if (json.has("resultado"))
			{
				editor.putString("user_login", user);
				editor.putString("password_login", password);
				editor.apply();

				try {
					if(json.has("dias_notificacion_capturar_detalle_cliente"))
					{
						dias_notificacion_capturar_detalle_cliente = json.getString("dias_notificacion_capturar_detalle_cliente");
					}
					else
					{
						dias_notificacion_capturar_detalle_cliente="vacio";
					}


					if (json.has("ticket_data_notice"))
					{
						mensajeTicketNotice=json.getString("ticket_data_notice");
						DatabaseAssistant.insertarDatosTicket(mensajeTicketNotice);
					}
					else {
						DatabaseAssistant.insertarDatosTicket(mensajeTicketNotice);
						Log.d("IMPRESION -->", "NO SE ENCONTRO EL TICKET");
					}

					if (json.has("empresas_excluidas_de_captura_de_informacion"))
					{
						empresas_excluidas_de_captura_de_informacion=json.getString("empresas_excluidas_de_captura_de_informacion");
					}
					else {
						empresas_excluidas_de_captura_de_informacion="00";
						Log.d("IMPRESION -->", "NO SE ENCONTRO EL TICKET");
					}



					boolean haveDate = json.getJSONObject("resultado").has("fecha");
					String ultimoIdMovil = json.getJSONObject("id_movil_anterior").getString("id_movil");
					isAnotherMobile = !ultimoIdMovil.equals(Build.SERIAL);


					if (CheckDate(json.getString("fecha_servidor"))) {
						try {
							showAlertDialog("Error",
									"La hora y/o la fecha ha sido modificada", true);
						} catch (Exception e){
							Toast toast = Toast.makeText(getApplicationContext(), "Error: La fecha y/o hora ha sido modificada", Toast.LENGTH_LONG);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							e.printStackTrace();
						}
						return;
					}

					setSharedPreferencesForDateWhenNoInternetAccess();

					if (haveDate)
					{
						DatabaseAssistant.deleteCompanies();
						insertarEmpresas();

						JSONObject jsonCierre = json.getJSONObject("resultado");
						String fecha = jsonCierre.getString("fecha");

						String noCobrador = jsonCierre.getString("no_cobrador");
						String efectivoCobrador = json.getString("efectivo");

						SharedPreferences preferencesSplash = getSharedPreferences("PABS_SPLASH", Context.MODE_PRIVATE);
						Editor editorSplash = preferencesSplash.edit();
						editorSplash.putString("fecha_sesion", fecha);
						editorSplash.putString("no_cobrador", noCobrador);
						editorSplash.putString("efectivo", efectivoCobrador);
						editorSplash.apply();


						String mac = json.getString("mac");
						//datosCobrador = json.getJSONObject("resultado");

						DatabaseAssistant.insertCollectorInfo("", "", "", "", "",
								json.getString("nombre"), "", "", "", "", "", "", "", "", "", "", "");


						Intent intent = new Intent(this, Cierre.class);
						intent.putExtra("fecha", fecha);
						intent.putExtra("mac", mac);
						intent.putExtra("nombre", json.getString("nombre"));
						intent.putExtra("no_cobrador", noCobrador);
						intent.putExtra("iniciar_time_sesion", true);

						try {
							JSONObject datosCobTmp = new JSONObject(preferencesSplash.getString("datos_cobrador", "no hay datos"));
							intent.putExtra("efectivo", datosCobTmp.getString("efectivo"));
						} catch (JSONException e) {
							e.printStackTrace();
						}
						startActivity(intent);
						finish();
					} else {
						try {
							datosCobrador = json.getJSONObject("resultado");

                            String query = "";

                            if (datosCobrador.has("query"))
                            {
                                query = datosCobrador.getString("query");

                                datosCobrador.remove("query");
                            }


							try {
								Preferences preferencesServerIP = new Preferences(this);
								String oldServerIP = preferencesServerIP.getServerIP();
								String newServerIP = datosCobrador.getString("serverIP");
								Util.Log.ih("!oldServerIP.equals( newServerIP ) " + (!oldServerIP.equals(newServerIP)));

								if (!oldServerIP.equals(newServerIP))
								{
									Util.Log.ih("Changing web server ip: " + newServerIP);
									preferencesServerIP.changeServerIP(newServerIP);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}

							/*
							 * This validation is needed when periodo from server haven't been upgraded yet
							 *
							 * true: takes periodo from server (which was upgraded)
							 * false: takes periodo from device (not upgraded in server, upgraded in device)
							 */
							Util.Log.ih("periodo de dispositivo = " + getEfectivoPreference().getInt("periodo", 0));
							Util.Log.ih("periodo de servidor = " + datosCobrador.getInt("periodo"));

							if ( !(getEfectivoPreference().getInt("periodo", 0) > datosCobrador.getInt("periodo")))
							{
								getEfectivoEditor().putInt("periodo", datosCobrador.getInt("periodo")).apply();
								Util.Log.ih("cambio periodo...");
							}

							Intent intent = new Intent();               //----> intent para MainActivity
							intent.putExtra("user_login", user);
							intent.putExtra("datos_cobrador", datosCobrador.toString());


							if (canUseApp(datosCobrador))
							{
								editor.putString("datos_cobrador", datosCobrador.toString());
								editor.putString("fecha_sesion", "" + System.currentTimeMillis());

								if (!preferences.contains("efectivo_inicial"))
								{
									Log.e("SE SETEO EFECTIVO LOGIN", "" + datosCobrador.getString("efectivo"));
									editor.putString("efectivo_inicial", datosCobrador.getString("efectivo"));
								}

								editor.apply();
								EditText euser = (EditText) findViewById(R.id.editTextUser);
								String codigo_cobrador = euser.getText().toString();
								String noCobrador = datosCobrador.getString("no_cobrador");


								//TODO: CORRECT THIS AFTER DEBUG
								String macAddress = datosCobrador.getString("id_impresora");
								//String macAddress = "00:19:0E:A0:93:CA";


								if(datosCobrador.has("minutos_timer_ubicaciones"))
								{
									if(datosCobrador.getString("minutos_timer_ubicaciones").equals("0"))
									{
										timeMinutesForSaveLocations="5";
									}
									else
									{
										timeMinutesForSaveLocations=datosCobrador.getString("minutos_timer_ubicaciones");
									}
								}
								else
									timeMinutesForSaveLocations="5";


								if (macAddress.equals("null")) {
									Toast.makeText(
											this,
											"Este usuario no tiene registrada una MacAddress",
											Toast.LENGTH_SHORT).show();
									buttonLogin.setEnabled(true);
									return;
								}

								DatabaseAssistant.deleteCompanies();
								insertarEmpresas();
								List<Companies> companies = DatabaseAssistant.getCompanies();

								SharedPreferences preference = getSharedPreferences(
										"folios", Context.MODE_PRIVATE);

								String folioMalva = "";
								String folioApoyo = "";
								String folioCompany = "";
								String mostrar_actualizar_datos="";

								for ( int i = 0; i < companies.size(); i++ )
								{
									Companies company = companies.get(i);

									String folioEmpresaShared = preference.getString(company.getName(), "0" );

									String lastFolioCompany = "ult_folio_";
									switch (i){
										case 0:
											//Programa
											lastFolioCompany = lastFolioCompany + "apoyo";
											folioCompany = datosCobrador.getString(lastFolioCompany);
											folioApoyo = folioCompany;
											break;
										case 1:
											//PBJ
											lastFolioCompany = lastFolioCompany + "malva";
											folioCompany = datosCobrador.getString(lastFolioCompany);
											folioMalva = folioCompany;
											break;
										case 2:
											//Cooperativa
											lastFolioCompany = lastFolioCompany + "empresa4";
											folioCompany = datosCobrador.getString(lastFolioCompany);
											break;
										case 3:
											lastFolioCompany = lastFolioCompany + "empresa5";
											folioCompany = datosCobrador.getString(lastFolioCompany);
											break;
									}



									SharedPreferences preferencesfolios = getSharedPreferences("folios", Context.MODE_PRIVATE);

									if ( Integer.parseInt(folioEmpresaShared) > Integer.parseInt(folioCompany) ){
										folioCompany = folioEmpresaShared;
									}

									Util.Log.ih("updating folios");
									Util.Log.ih("company.getName()" + company.getName());
									Util.Log.ih("folioCompany" + folioCompany);
									Editor editFolio = preferencesfolios.edit();
									editFolio.putString(company.getName(), folioCompany);
									editFolio.commit();

								}

								SharedPreferences preferencesSplash = getSharedPreferences("PABS_SPLASH", Context.MODE_PRIVATE);

								datosCobrador = new JSONObject(preferencesSplash.getString("datos_cobrador", datosCobrador.toString()));

								Editor editorSplash = preferencesSplash.edit();
								editorSplash.putString("fecha_sesion", "" + System.currentTimeMillis());
								editorSplash.putString("no_cobrador", datosCobrador.getString("no_cobrador"));
								editorSplash.putString("datos_cobrador", datosCobrador.toString());
								editorSplash.apply();


								//Editar aqui las preferencias para el supervisor
								boolean isSupervisor = json.getBoolean("esSupervisor");
								boolean isSubstitute = json.getBoolean("esBanca");

								if ( isSupervisor || isSubstitute )
								{
									Util.Log.ih("if ( isSupervisor || isSubstitue ) {");
									if (isSubstitute) {
										editor.putBoolean("isSupervisorOrSubstitute", true);
									}
									editor.putBoolean("isSupervisor", true);
									editor.apply();
									intent.putExtra("collectorType", isSupervisor ? "supervisor" : "banca");
									intent.setClass(this, Collectors.class);
								}
								else
									{
										String no_cobradoParaResetearPreferencia = datosCobrador.getString("no_cobrador");
										SharedPreferences preferencesSupervisor = getContext().getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
										SharedPreferences.Editor editorPreferencesSupervisor = preferencesSupervisor.edit();
										editorPreferencesSupervisor.putString("clientsCollectorNumber", no_cobradoParaResetearPreferencia);
										editorPreferencesSupervisor.commit();

										Util.Log.ih("not supervisor nor banca");
										intent.setClass(this, MainActivity.class); //----> MainActivity
									}




								//this validation will help when on Clientes screen
								//because will load clients and cash
								//works after doing 'cierre de periodo'
								if (preferencesSplash.getBoolean("first", true)){
									editorSplash.putBoolean("first", false).apply();
									isAnotherMobile = true;
								}
								intent.putExtra("periodo", isAnotherMobile);








								String mensaje="";
								String numeroPagos="2";
								String tituloPercapita="";
								String mensajeCobradorPercapita="";


								if(json.has("mostrar_actualizar_datos"))
								{
									mostrar_actualizar_datos=json.getString("mostrar_actualizar_datos");
								}
								else
								{
									mostrar_actualizar_datos="0";
								}

								if(json.has("mensaje_percapita"))
								{
									mensaje = json.getString("mensaje_percapita");


									if(json.has("numero_pagos_mostrar_mensaje_percapita"))
									{
										numeroPagos = json.getString("numero_pagos_mostrar_mensaje_percapita");


										if(json.has("titulo_mensaje_percapita"))
										{
											tituloPercapita = json.getString("titulo_mensaje_percapita");


											if(json.has("mensaje_percapita_cobrador"))
											{
												mensajeCobradorPercapita=json.getString("mensaje_percapita_cobrador");

												DatabaseAssistant.insertCollectorInfo(codigo_cobrador,
														noCobrador, macAddress, folioMalva, folioApoyo,
														datosCobrador.getString("nombre"),
														json.getString("mensaje"),
														datosCobrador.getString("perimetro"),
														dias_notificacion_capturar_detalle_cliente,
														mensaje,
														numeroPagos,
														tituloPercapita,
														mensajeCobradorPercapita,
														timeMinutesForSaveLocations, mostrar_actualizar_datos, "", empresas_excluidas_de_captura_de_informacion);
											}

										}
									}
									else
									{
										DatabaseAssistant.insertCollectorInfo(codigo_cobrador,
												noCobrador, macAddress, folioMalva, folioApoyo,
												datosCobrador.getString("nombre"),
												json.getString("mensaje"),
												datosCobrador.getString("perimetro"),
												dias_notificacion_capturar_detalle_cliente,
												mensaje,
												"vacio",
												"vacio",
												"vacio",
												timeMinutesForSaveLocations, mostrar_actualizar_datos, "", empresas_excluidas_de_captura_de_informacion);
									}
								}
								else
								{
									DatabaseAssistant.insertCollectorInfo(codigo_cobrador,
											noCobrador, macAddress, folioMalva, folioApoyo,
											datosCobrador.getString("nombre"),
											json.getString("mensaje"),
											datosCobrador.getString("perimetro"),
											dias_notificacion_capturar_detalle_cliente,
											"vacio",
											"vacio",
											"vacio",
											"vacio",
											timeMinutesForSaveLocations, mostrar_actualizar_datos, "", empresas_excluidas_de_captura_de_informacion);
								}



								if(json.has("estatus_suspensiones"))
								{
									Suspensionitem.deleteAll(Suspensionitem.class);

									JSONArray arregloSolicitudes = json.getJSONArray("estatus_suspensiones");
									for(int i=0; i<=arregloSolicitudes.length()-1;)
									{
										JSONObject jsonTmp = arregloSolicitudes.getJSONObject(i);
										String susp_est = jsonTmp.getString("susp_est");
										Log.d("IMPRESION --> "+ i, "OBTENIDO CORRECTAMENTE");
										DatabaseAssistant.insertarMensajeSuspensiones(susp_est);
										i++;
									}
								}


								if(json.has("motivos_de_cancelacion_de_pago"))
								{
									JSONArray arregloSolicitudes = json.getJSONArray("motivos_de_cancelacion_de_pago");

									if(arregloSolicitudes.length() > 0)
									{
										MotivosDeCancelacion.deleteAll(MotivosDeCancelacion.class);
										for(int i=0; i<=arregloSolicitudes.length()-1;)
										{
											JSONObject jsonTmp = arregloSolicitudes.getJSONObject(i);
											String motivo = jsonTmp.getString("motivo_de_cancelacion");
											String id_motivo_cancelacion_de_pago = jsonTmp.getString("id_motivo_cancelacion_de_pago");

											Log.d("IMPRESION --> "+ i, "OBTENIDO CORRECTAMENTE");
											DatabaseAssistant.insertarMotivoDeCancelacionDePago(motivo, id_motivo_cancelacion_de_pago);
											i++;
										}
									}
								}

								intent.putExtra("isConnected", true);
								Util.Log.ih("iniciando collectors activity");
								Util.Log.ih("despues de finish");

								if (datosCobrador.has("fecha_inicio_periodo"))
								{
									DatabaseAssistant.insertNewPeriodFromScratchIfNeeded(datosCobrador.getString("fecha_inicio_periodo"), getEfectivoPreference().getInt("periodo", 0));
								}
								else
									DatabaseAssistant.insertNewPeriodFromScratchIfNeeded(getEfectivoPreference().getInt("periodo", 0));




								if ( DatabaseAssistant.checkThatPeriodWasClosedOnLastDayOfMonth(getEfectivoPreference().getInt("periodo", 0)))
								{
									List<Companies> companiesPeriods = DatabaseAssistant.getCompanies();
									for (int cont = 0; cont < companiesPeriods.size(); cont++)
									{
										Companies company = companiesPeriods.get(cont);
										DatabaseAssistant.insertCompanyAmountsByPeriodWhenMoreThanOnePeriodIsActive(
												company.getName(),
												getEfectivoPreference().getFloat(company.getName(), 0),
												getEfectivoPreference().getInt("periodo", 0));

										getEfectivoEditor().putFloat(company.getName(), 0).apply();
									}

									getEfectivoEditor().putInt("periodo", getEfectivoPreference().getInt("periodo", 0) + 1).apply();
								}

								intent.putExtra("query", query);
								startActivity(intent);
								if(timerNotificaciones!=null)
									timerNotificaciones.cancel();
								finish();

							} else {
								/*
								 * Blocks app going to screen 'Bloqueo'
								 */
								try {
									String mac = json.getString("mac");
									intent.putExtra("mac", mac);
								} catch (Exception e) {
									e.printStackTrace();
								}
								SharedPreferences preferencesSplash = getSharedPreferences("PABS_SPLASH", Context.MODE_PRIVATE);
								Editor editorSplash = preferencesSplash.edit();
								editorSplash.putString("datos_cobrador", datosCobrador.toString());
								editorSplash.apply();

								intent.setClass(this, Bloqueo.class);
								intent.putExtra("datos_cobrador", datosCobrador.toString());
								intent.putExtra("isConnected", isConnected);
								DatabaseAssistant.deleteCompanies();
								insertarEmpresas();
								intent.putExtra("iniciar_time_sesion", true);
								startActivity(intent);
								finish();
							}

						} catch (JSONException e) {
							e.printStackTrace();
							buttonLogin.setEnabled(true);
							try {
								showAlertDialog("","Ocurrió un problema al intentar iniciar sesión",true);
							} catch (Exception ex){
								Toast.makeText(this, R.string.error_message_user_pass, Toast.LENGTH_LONG).show();
							}
						}
					}
				} catch (JSONException e1) {
					e1.printStackTrace();
					buttonLogin.setEnabled(true);
					try {
						showAlertDialog("","Ocurrió un problema al intentar iniciar sesión",true);
					} catch (Exception ex){
						Toast.makeText(this, R.string.error_message_user_pass, Toast.LENGTH_LONG).show();
					}
				}

			} else {
				mostrarMensajeError(ConstantsPabs.iniciarSesion);
			}

		} else {
			buttonLogin.setEnabled(true);
			try {
				showAlertDialog("", mthis.getString(R.string.error_message_user_pass),
						true);
			} catch (Exception e){
				Toast.makeText(this, R.string.error_message_user_pass, Toast.LENGTH_LONG).show();
			}
		}

	}

	/**
	 * Saves companies into database according to targetBranch.
	 * Even though some companies are not needed in some cases of
	 * targetBrach, they have to be added with whatever information
	 *
	 * NOTE*
	 * 		ADDS COMPANIES
	 */
	private void insertarEmpresas(){
		switch(BuildConfig.getTargetBranch()){
			case GUADALAJARA:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC: AURA-781005-TL3",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"WASHINGTON No. 404, COL. LA AURORA C.P.44460",
						"36503695");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC : PBP 140220 TN2",
						"Cooperativa",
						"Programa de Beneficio PABS S.C. de P. de R.L. de C.V.",
						"Av. Federalismo Norte 1485 Piso 1 Oficina 3 Colonia San Miguel de Mezquitan CP 44260 Guadalajara, Jalisco, México",
						"36503695 y 38232155");
				DatabaseAssistant.insertCompany(
						"03",
						"RFC : PBJ 080818 EL5",
						"PBJ",
						"PROGRAMA DE BENEFICIO JALISCO, S.A. DE C.V.",
						"Av. Federalismo Norte 1485 Piso 1 Oficina 3 Colonia San Miguel de Mezquitan CP 44260 Guadalajara, Jalisco, México",
						"36503695 y 38232155");
				DatabaseAssistant.insertCompany(
						"01",
						"RFC : PBP 140220 TN2",
						"Programa",
						"PROGRAMA DE BENEFICIO PABS SC DE P DE RL DE CV",
						"Av. Federalismo Norte 1485 Piso 1 Oficina 3 Colonia San Miguel de Mezquitan CP 44260 Guadalajara, Jalisco, México",
						"36154330 y 36150217");
				break;

			case TAMPICO:
				DatabaseAssistant.insertCompany(
						"05",
						"",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"WASHINGTON No. 404, COL. LA AURORA C.P.44460",
						"36503695");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC : PBP 140220 TN2",
						"Cooperativa",
						"Programa de Beneficio PABS S.C. de P. de R.L. de C.V.",
						"Av. Federalismo Norte 1485 Piso 1 Oficina 3 Colonia San Miguel de Mezquitan CP 44260 Guadalajara, Jalisco, México",
						"36503695 y 38232155");
				DatabaseAssistant.insertCompany(
						"03",
						"RFC : PBJ 080818 EL5",
						"PBJ",
						"PROGRAMA DE BENEFICIO JALISCO, S.A. DE C.V.",
						"Av. Federalismo Norte 1485 Piso 1 Oficina 3 Colonia San Miguel de Mezquitan CP 44260 Guadalajara, Jalisco, México",
						"36503695 y 38232155");
				DatabaseAssistant.insertCompany(
						"01",
						"RFC : PBP 140220 TN2",
						"Programa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"Av. Hidalgo No. 3402 planta alta, entre calle Violeta y calle Rosa, colonia Flores Tampico, Tamaulipas, México",
						"833-569-4184 Y 833-569-4185");
				break;

						/*
						Av. Hidalgo No. 3402 , entre la calle Violeta y la calle Rosa en la Colonia Flores Tampico , Tamaulipas.
						En el edificio Planta alta .
						Entrada por Av. Hidalgo.
						 */
			case IRAPUATO:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC RFC RFC RFC RFC",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"WASHINGTON No. 404, COL. LA AURORA C.P.44460",
						"36503695");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC RFC RFC RFC RFC",
						"PBJ",
						"PROGRAMA DE BENEFICIO PABS TOLUCA SC DE P DE RL DE CV",
						"PASEO TOYOCAN # 101, COL. SANTA MARIA DE LAS ROSAS, TOLUCA, EDO D MEX",
						"212-78-19 Y 212-53-79");
				DatabaseAssistant.insertCompany(
						"03",
						"",
						"Cooperativa",
						"GRUPO ASISTENCIAL MEMORY SCP DE RL DE CV.",
						"BLVD. DIAZ ORDAZ # 1370, M2 INTERIOR 2, C.P. 36660. JARDINES DE IRAPUATO, IRAPUATO, GUANAJUATO, MÉXICO",
						"4626253705 y 4626248002");
				DatabaseAssistant.insertCompany(
						"01",
						"",
						"Programa",
						"GRUPO ASISTENCIAL MEMORY SCP DE RL DE CV.",
						"BLVD. DIAZ ORDAZ # 1370, M2 INTERIOR 2, C.P. 36660. JARDINES DE IRAPUATO, IRAPUATO, GUANAJUATO, MÉXICO",
						"4626253705 y 4626248002");//Cambiar aqui
				break;


			case LEON:
				DatabaseAssistant.insertCompany(
						"05",
						"",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"WASHINGTON No. 404, COL. LA AURORA C.P.44460",
						"36503695");
				DatabaseAssistant.insertCompany(
						"04",
						"",
						"PBJ",
						"PROGRAMA DE BENEFICIO PABS TOLUCA SC DE P DE RL DE CV",
						"PASEO TOYOCAN # 101, COL. SANTA MARIA DE LAS ROSAS, TOLUCA, EDO D MEX",
						"212-78-19 Y 212-53-79");
				DatabaseAssistant.insertCompany(
						"03",
						"",
						"Cooperativa",
						"GRUPO MEMORY SERVICIOS SCP DE RL DE CV.",
						"NICARAGUA #917, LOCAL 12, 2DO NIVEL, C.P. 37366, VILLA RESIDENCIAL ARBIDE, LEON, GUANAJUATO, MÉXICO",
						"4777186035 y 4777795781");
				DatabaseAssistant.insertCompany(
						"01",
						"",
						"Programa",
						"GRUPO MEMORY SERVICIOS SCP DE RL DE CV.",
						"NICARAGUA #917, LOCAL 12, 2DO NIVEL, C.P. 37366, VILLA RESIDENCIAL ARBIDE, LEON, GUANAJUATO, MÉXICO",
						"4777186035 y 4777795781");//Cambiar aqui
				break;


			case SALAMANCA:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC RFC RFC RFC RFC",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"WASHINGTON No. 404, COL. LA AURORA C.P.44460",
						"36503695");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC RFC RFC RFC RFC",
						"PBJ",
						"PROGRAMA DE BENEFICIO PABS TOLUCA SC DE P DE RL DE CV",
						"PASEO TOYOCAN # 101, COL. SANTA MARIA DE LAS ROSAS, TOLUCA, EDO D MEX",
						"212-78-19 Y 212-53-79");

				DatabaseAssistant.insertCompany(
						"03",
						"",
						"Cooperativa",
						"COOPERATIVA MEMORY SERVICIOS SCP de RL de C.V.",
						"JUAN ALDAMA #1008, C.P. 36860, CENTRO, SALAMANCA, GUANAJUATO, MÉXICO",
						"4646482086 - 4646416958");
				DatabaseAssistant.insertCompany(
						"01",
						"",
						"Programa",
						"COOPERATIVA MEMORY SERVICIOS SCP de RL de C.V.",
						"JUAN ALDAMA #1008, C.P. 36860, CENTRO, SALAMANCA, GUANAJUATO, MÉXICO",
						"4646482086 - 4646416958");//Cambiar aqui
				break;

			case CELAYA:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC RFC RFC RFC RFC",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"WASHINGTON No. 404, COL. LA AURORA C.P.44460",
						"36503695");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC RFC RFC RFC RFC",
						"PBJ",
						"PROGRAMA DE BENEFICIO PABS TOLUCA SC DE P DE RL DE CV",
						"PASEO TOYOCAN # 101, COL. SANTA MARIA DE LAS ROSAS, TOLUCA, EDO D MEX",
						"212-78-19 Y 212-53-79");

				DatabaseAssistant.insertCompany(
						"03",
						"",
						"Cooperativa",
						"GRUPO ASISTENCIAL MEMORY SCP de RL de C.V.",
						"PROLONGACIÓN HIDALGO #705-A, C.P. 38040, CENTRO, CELAYA, GUANAJUATO, MÉXICO",
						"4616093529 y 4616144232");
				DatabaseAssistant.insertCompany(
						"01",
						"",
						"Programa",
						"GRUPO ASISTENCIAL MEMORY SCP de RL de C.V.",
						"PROLONGACIÓN HIDALGO #705-A, C.P. 38040, CENTRO, CELAYA, GUANAJUATO, MÉXICO",
						"4616093529 y 4616144232");//Cambiar aqui
				break;





			case TOLUCA:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC: AURA-781005-TL3",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"WASHINGTON No. 404, COL. LA AURORA C.P.44460",
						"36503695");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC: PBP 140324 Q37",
						"PBJ",
						"PROGRAMA DE BENEFICIO PABS TOLUCA SC DE P DE RL DE CV",
						"PASEO TOYOCAN # 101, COL. SANTA MARIA DE LAS ROSAS, TOLUCA, EDO D MEX",
						"212-78-19 Y 212-53-79");
				DatabaseAssistant.insertCompany(
						"03",
						"RFC: PBP 140324 Q37",
						"Cooperativa",
						"PROGRAMA DE BENEFICIO PABS TOLUCA SC DE P DE RL DE CV",
						"PASEO TOYOCAN # 101, COL. SANTA MARIA DE LAS ROSAS, TOLUCA, EDO D MEX",
						"212-78-19 Y 212-53-79");
				DatabaseAssistant.insertCompany(
						"01",
						"RFC PBS 130731 5A9",
						"Programa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL TOLUCA SA DE CV",
						"PASEO TOYOCAN # 101, COL. SANTA MARIA DE LAS ROSAS, TOLUCA, EDO D MEX",
						"212-78-19 Y 212-53-79");

				break;
			case PUEBLA:
				DatabaseAssistant.insertCompany(
						"05",
							"",
						"PABS LATINO",
						"PROGRAMA DE BENEFICIO PABS PUEBLA S.C.P. DE R.L. DE C.V.",
						"FRANCISCO I. MADERO No. 428 COL. SAN BALTAZAR CAMPECHE C.P. 72550. PUEBLA PUEBLA, MÉXICO",
						"22224309967"); //222 2430 9967
				DatabaseAssistant.insertCompany(
						"04",
						"RFC : PBP 140220 TN2",
						"PBJ",
						"Programa de Beneficio PABS S.C. de P. de R.L. de C.V.",
						"FRANCISCO I. MADERO No. 428 COL. SAN BALTAZAR CAMPECHE C.P. 72550. PUEBLA PUEBLA, MÉXICO",
						"36503695 y 38232155");
				DatabaseAssistant.insertCompany(
						"03",
						"RFC: PBP140408QJ7",
						"Cooperativa",
						"PROGRAMA DE BENEFICIO PABS PUEBLA S.C.P. DE R.L. DE C.V.",
						"FRANCISCO I. MADERO No. 428 COL. SAN BALTAZAR CAMPECHE C.P. 72550. PUEBLA PUEBLA, MÉXICO",
						"2406040 y 2374560");
				DatabaseAssistant.insertCompany(
						"01",
						"RFC: PAB100615KS9",
						"Programa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"FRANCISCO I. MADERO No. 428 COL. SAN BALTAZAR CAMPECHE C.P. 72550. PUEBLA PUEBLA, MÉXICO",
						"2406040 y 2374560");
				break;
			case NAYARIT:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC: AURA-781005-TL3",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"WASHINGTON No. 404, COL. LA AURORA C.P.44460",
						"36503695");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC: CAHN 800216 7M9",
						"Cooperativa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"AV. MEXICO N° 393 NTE CALLE NIÑOS HEROES Nº 82 JAVIER MINA Nº69 ORIENTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO",
						"2121080 Y 2169102");
				DatabaseAssistant.insertCompany(
						"03",
						"",
						"Apoyo",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"AV. MEXICO N° 393 NTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO",
						"2121080 Y 2169102");
				DatabaseAssistant.insertCompany(
						"01",
						"",
						"Programa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"AV. MEXICO N° 393 NTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO",
						"2121080 Y 2169102");
				break;
			case MERIDA:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC: AURA-781005-TL3",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"WASHINGTON No. 404, COL. LA AURORA C.P.44460",
						"36503695");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC: CAHN 800216 7M9",
						"Programa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"AV. MEXICO N° 393 NTE CALLE NIÑOS HEROES Nº 82 JAVIER MINA Nº69 ORIENTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO",
						"2121080 Y 2169102");
				DatabaseAssistant.insertCompany(
						"03",
						"",
						"Apoyo",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"AV. MEXICO N° 393 NTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO",
						"2121080 Y 2169102");
				DatabaseAssistant.insertCompany(
						"01",
						"RFC: PBP 140220 TN2",
						"Cooperativa",
						"PROGRAMA DE BENEFICIO PABS SC DE RL DE CV",
						"Calle 33 No. 503C Col. Alcala Martin CP 97000 Merida Yucatan, México.",
						"9201269 y 9201268");
				break;
			case SALTILLO:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC: AURA-781005-TL3",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"WASHINGTON No. 404, COL. LA AURORA C.P.44460",
						"36503695");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC: CAHN 800216 7M9",
						"Programa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"AV. MEXICO N° 393 NTE CALLE NIÑOS HEROES Nº 82 JAVIER MINA Nº69 ORIENTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO",
						"2121080 Y 2169102");
				DatabaseAssistant.insertCompany(
						"03",
						"",
						"Apoyo",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"AV. MEXICO N° 393 NTE COL. CENTRO CP. 63000 TEPIC, NAYARIT, MEXICO",
						"2121080 Y 2169102");
				DatabaseAssistant.insertCompany(
						"01",
						"",
						"Cooperativa",
						"PROGRAMA DE BENEFICIO PABS SC DE RL DE CV",
						"BOULEVARD NAZARIO ORTIZ GARZA NO 1265 (ESQUINA ABASOLO) COL ALPES CP 25270 SALTILLO COAHUILA.",
						"(844)4159299 y (844)4148836");
				break;
			case CANCUN:
				DatabaseAssistant.insertCompany(
						"05",
						"",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"CALLE CEDRO MZ. 41 LOTE 1-08 SM. 311 CANCÚN, QUINTANA ROO",
						"(998) 251.6530/31");
				DatabaseAssistant.insertCompany(
						"04",
						"",
						"Programa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"CALLE CEDRO MZ. 41 LOTE 1-08 SM. 311 CANCÚN, QUINTANA ROO",
						"(998) 251.6530/31");
				DatabaseAssistant.insertCompany(
						"03",
						"",
						"Apoyo",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"CALLE CEDRO MZ. 41 LOTE 1-08 SM. 311 CANCÚN, QUINTANA ROO",
						"(998) 251.6530/31");
				DatabaseAssistant.insertCompany(
						"01",
						"",
						"Cooperativa",
						"PROGRAMA DE BENEFICIO PABS SC DE RL DE CV",
						"CALLE CEDRO MZ. 41 LOTE 1-08 SM. 311 CANCÚN, QUINTANA ROO",
						"(998) 251.6530/31");
				break;
			case CUERNAVACA:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC: AURA-781005-TL3",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"FRANCISCO I. MADERO No. 719 COL. MIRAVAL C.P. 62270. DELEGACIÓN BENITO JUÁREZ, CUERNAVACA, MÉXICO",
						"7773119299 y 7771704870");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC: PBP140408QJ7",
						"Cooperativa",
						"PROGRAMA DE BENEFICIO PABS PUEBLA S.C.P. DE R.L. DE C.V.",
						"FRANCISCO I. MADERO No. 719 COL. MIRAVAL C.P. 62270. DELEGACIÓN BENITO JUÁREZ, CUERNAVACA, MÉXICO",
						"7773119299 y 7771704870");

				DatabaseAssistant.insertCompany(
						"03",
						"RFC : PBP 140220 TN2",
						"PBJ",
						"Programa de Beneficio PABS S.C. de P. de R.L. de C.V.",
						"FRANCISCO I. MADERO No. 719 COL. MIRAVAL C.P. 62270. DELEGACIÓN BENITO JUÁREZ, CUERNAVACA, MÉXICO",
						"7773119299 y 7771704870");
				DatabaseAssistant.insertCompany(
						"01",
						"RFC: PAB100615KS9",
						"Programa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"FRANCISCO I. MADERO No. 719 COL. MIRAVAL C.P. 62270. DELEGACIÓN BENITO JUÁREZ, CUERNAVACA, MÉXICO",
						"7773119299 y 7771704870");
				break;
			case MEXICALI:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC:",
						"PABS LATINO",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"Blvd Anahuac #800 Int.A, Col. Esperanza, C.P. 21140 Mexicali B.C., MÉXICO",
						"561-2592 Y 556-8660");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC : PBP 140220 TN2",
						"Cooperativa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"Blvd Anahuac #800 Int.A, Col. Esperanza, C.P. 21140 Mexicali B.C., MÉXICO",
						"561-2592 Y 556-8660");
				DatabaseAssistant.insertCompany(
						"03",
						"RFC: PBP140408QJ7",
						"Apoyo",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"Blvd Anahuac #800 Int.A, Col. Esperanza, C.P. 21140 Mexicali B.C., MÉXICO",
						"561-2592 Y 556-8660");
				DatabaseAssistant.insertCompany(
						"01",
						"RFC:",
						"Programa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"Blvd Anahuac #800 Int.A, Col. Esperanza, C.P. 21140 Mexicali B.C., MÉXICO",
						"561-2592 Y 556-8660");
				break;


			case TORREON:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC: MEOS651002IW6",
						"PABS LATINO",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"CLZ. MATIAS ROMAN RIOS # 425, L-17, C.P 27000, PLAZA PABELLON HIDALGO, TORREON COAHUILA, MÉXICO.",
						"8712852043 Y 8712852060");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC: MEOS651002IW6",
						"Cooperativa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"CLZ. MATIAS ROMAN RIOS # 425, L-17, C.P 27000, PLAZA PABELLON HIDALGO, TORREON COAHUILA, MÉXICO.",
						"8712852043 Y 8712852060");
				DatabaseAssistant.insertCompany(
						"03",
						"RFC:MEOS651002IW6",
						"Apoyo",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"CLZ. MATIAS ROMAN RIOS # 425, L-17, C.P 27000, PLAZA PABELLON HIDALGO, TORREON COAHUILA, MÉXICO.",
						"8712852043 Y 8712852060");
				DatabaseAssistant.insertCompany(
						"01",
						"RFC: MEOS651002IW6",
						"Programa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"CLZ. MATIAS ROMAN RIOS # 425, L-17, C.P 27000, PLAZA PABELLON HIDALGO, TORREON COAHUILA, MÉXICO.",
						"8712852043 Y 8712852060");
				break;



			case QUERETARO:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC RFC RFC RFC RFC",
						"PABS LATINO",
						"LATINOAMERICANA RECINTO FUNERAL",
						"WASHINGTON No. 404, COL. LA AURORA C.P.44460",
						"36503695");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC RFC RFC RFC RFC",
						"PBJ",
						"PROGRAMA DE BENEFICIO PABS TOLUCA SC DE P DE RL DE CV",
						"PASEO TOYOCAN # 101, COL. SANTA MARIA DE LAS ROSAS, TOLUCA, EDO D MEX",
						"212-78-19 Y 212-53-79");
				DatabaseAssistant.insertCompany(
						"03",
						"",
						"Cooperativa",
						"COOPERATIVA MEMORY SERVICIOS SCP de RL de C.V.",
						"2DA PRIVADA DE 20 DE NOVIEMBRE No. 15 2DO PISO COL. SAN FRANCISQUITO, C.P. 76058. QUERÉTARO QUERÉTARO, MÉXICO",
						"4422134884 y 4422427739");
				DatabaseAssistant.insertCompany(
						"01",
						"",
						"Programa",
						"COOPERATIVA MEMORY SERVICIOS SCP de RL de C.V.",
						"2DA PRIVADA DE 20 DE NOVIEMBRE No. 15 2DO PISO COL. SAN FRANCISQUITO, C.P. 76058. QUERÉTARO QUERÉTARO, MÉXICO",
						"4422134884 y 4422427739"); //Cambiar aqui

				break;
			case MORELIA:
				DatabaseAssistant.insertCompany(
						"05",
						"RFC:",
						"PABS LATINO",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"AV. GUADALUPE VICTORIA NO. 364 COL. CENTRO C.P. 58000 MORELIA, MICHOACAN",
						"443 1761069 y 3163897");
				DatabaseAssistant.insertCompany(
						"03",
						"RFC: JAS020826JT7",
						"PBJ",
						"JARDINES DE LA ASUNCION SA DE CV",
						"BELLA AURORA No. 600 FRACC. JARDINES DE LA ASUNCION C.P. 58195 MORELIA, MICHOACAN",
						"443 176 1069");
				DatabaseAssistant.insertCompany(
						"04",
						"RFC:",
						"Cooperativa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"AV. GUADALUPE VICTORIA NO. 364 COL. CENTRO C.P. 58000 MORELIA, MICHOACAN",
						"443 1761069 y 3163897");
				DatabaseAssistant.insertCompany(
						"01",
						"RFC:",
						"Programa",
						"PROGRAMA DE APOYO DE BENEFICIO SOCIAL",
						"AV. GUADALUPE VICTORIA NO. 364 COL. CENTRO C.P. 58000 MORELIA, MICHOACAN",
						"443 1761069 y 3163897");

				break;
			default:
				Log.e("PABS", "Error when inserting companies into DB");
				throw new RuntimeException();
		}
	}

	/**
	 * Sets all payments stored in arrayCobrosOffline variable which were just sent
	 * to server as sync (sync <- 1) so that does not resend payments made
	 * causing a lot of data usage.
	 */
	public void sincronizarPagosRegistrados(JSONObject jsonObject) {
		try {
			//for (ModelPayment p: offlinePayments){
				DatabaseAssistant.updatePaymentsAsSynced(jsonObject);
			//}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Manages response of web service
	 * if failed to send payments, it will try to send them again,
	 * if not, will set them as sync
	 *
	 * @param json
	 * 			JSONObject with response of web service
	 */
	public void manage_RegisterPagosOffiline(JSONObject json) {
		if (json.has("result")) {
			try {
				JSONArray arrayPagosFallidos = json.getJSONArray("result");
				if (arrayPagosFallidos.length() > 0) {
					runOnUiThread(() -> RegisterPagos(false));

				} else {
					sincronizarPagosRegistrados(json);
					toastL(R.string.info_message_done);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			toastL(R.string.error);
		}
	}

	/**
	 * this method is launched when an error happened when trying to get
	 * information from server, but if it's allowed to log ing without
	 * internet, will actually log in, else, will show a dialog with error
	 *
	 * @param caso
	 * 			contant when failed
	 */
	private void mostrarMensajeError(final int caso) {
		if (camposCorrectos()) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.info_message_network_offline),
					Toast.LENGTH_LONG).show();
			SharedPreferences preferences = getSharedPreferences("PABS_Login",
					Context.MODE_PRIVATE);
			String datosString = "";
			if (preferences.contains("datos_cobrador")) {
				datosString = preferences.getString("datos_cobrador", "");
			}

			//Splunk Mint instruction to report an event of 'Inicio de Sesion'
			//Mint.logEvent("Inicio de Sesion - " + user + " - " + new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()), MintLogLevel.Info);

			Intent intent = new Intent(this, MainActivity.class);
			intent.putExtra("user_login", user);
			intent.putExtra("datos_cobrador", datosString);
			startActivity(intent);
			finish();
		} else {
			AlertDialog.Builder alert = getAlertDialogBuilder();
			alert.setMessage("Hubo un error de comunicación con el servidor, verifique sus ajustes de red o intente más tarde.");
			alert.setPositiveButton("Salir",(dialogInterface, i) -> {
				View refSplash = null;
				if (Splash.mthis != null) {
					refSplash = Splash.mthis
							.findViewById(R.id.linear_splash);
				}

				if (refSplash != null) {
					Splash.mthis.finish();
				}

				mthis = null;
				finish();
			});
			alert.setNegativeButton("Reintentar",(dialogInterface, i) -> {
				if(!isConnected) mostrarMensajeError(caso);
			});
			alert.create();
			alert.setCancelable(false);
			alert.show();
		}
	}

	/**
	 * opens Date & Time Setting screen, so that time can be
	 * changed
	 */
	private void openDateSettings() {
		startActivity(new Intent(Settings.ACTION_DATE_SETTINGS));
		//finish();
	}

	/**
	 * sends to server all payments made that are not sync yet
	 *
	 * Web Service
	 * 'http://50.62.56.182/ecobro/controlpagos/registerPagos'
	 * JSONObject param
	 * {
	 *     "no_cobrador": "",
	 *     "pagos": {}
	 *     "periodo":
	 * }
	 */
	private void RegisterPagos(boolean all) {
		//SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());

		//try {
		//JSONObject json = query.obtenerCobrosDesincronizados();
		if (all){
			offlinePayments = DatabaseAssistant.getAllPayments();
		} else {
			offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();
		}
		//if (json.has("cobros")) {
		if (offlinePayments.size() > 0)
		{
			//arrayCobrosOffline = json.getJSONArray("cobros");
			JSONObject data = new JSONObject();
			//if (arrayCobrosOffline.length() > 0) {
			try {
				data.put("no_cobrador", offlinePayments.get(0).getIdCollector());
				data.put("pagos", new JSONArray(new Gson().toJson(offlinePayments)));
				data.put("periodo", getEfectivoPreference().getInt("periodo", 0));
				try {
					TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
					String deviceid = null;
					if (Nammu.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
							deviceid = manager.getImei();
						else
							deviceid = manager.getDeviceId();
					}
					data.put("imei", deviceid);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			requestRegisterPagosVolley(data);

			/*NetService service = new NetService(ConstantsPabs.urlregisterPagosOfflineMain, data);
			NetHelper.getInstance().ServiceExecuter(service, this, new NetHelper.onWorkCompleteListener() {

				@Override
				public void onCompletion(String result) {
					try {
						manage_RegisterPagosOffiline(new JSONObject(result));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onError(Exception e) {

				}
			});*/

		} else {
			toastL("Sin cobros a registrar.");
		}
	}
	private void requestRegisterPagosVolley(JSONObject jsonParams)
	{
		progress = ProgressDialog.show(this, "Actualizando pagos", "Por favor espera...", true);
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlregisterPagosOfflineMain, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				manage_RegisterPagosOffiline(response);
				if(progress!=null)
					progress.dismiss();
				Log.i("Request LOGIN-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
	}

	/**
	 * Checks date when user is trying to log in without internet
	 * access. There's a range of 4 days (which is what a period
	 * normally takes)
	 * @return
	 * 		false if date is within limit
	 * 		true if date is out of limit
	 */
	private boolean checkDateWhenNoConnectedToInternet(){
		int[] dayStoredAux = getDateFromSharedPreference();
		if ( dayStoredAux[0] == 0 || dayStoredAux[1] == 0){
			setSharedPreferencesForDateWhenNoInternetAccess();
		}
		else{
			//gets days of year
			int checkLeapYear = checkLeapYear();

			Calendar calendar = Calendar.getInstance();
			int today = calendar.get(Calendar.DAY_OF_YEAR);
			int year = calendar.get(Calendar.YEAR);

			if ( checkLeapYear <= dayStoredAux[0] ){
				Util.Log.ih("checkLeapYear <= dayStoredAux[0]");
				return false;
			}
			else if ( year != dayStoredAux[1] ){
				Util.Log.ih("year != dayStoredAux[1]");
				return true;
			}
			Util.Log.ih("currentDay = " + today);
			Util.Log.ih("dayStored = " + dayStoredAux[0]);

			int dayStoredOne = dayStoredAux[0];
			int dayStoredTwo = dayStoredAux[0];

			int counter = 0;
			for (int i = 0; ; i++, counter++){
				if (today == dayStoredOne) {
					break;
				}
				if (dayStoredOne == checkLeapYear){
					dayStoredOne = 0;
				}
				dayStoredOne++;
			}


			int counterTwo = 0;
			for (int i = 0; ; i++, counterTwo++){
				if (today == dayStoredTwo) {
					break;
				}
				if (dayStoredTwo == 0){
					dayStoredTwo = checkLeapYear + 1;
				}
				dayStoredTwo--;
			}

			return counter > 2 && counterTwo > 2;

		}
		return false;
	}

	/**
	 * gets days of year
	 *
	 * @return
	 * 		366 if it's a leap year
	 * 		365 otherwise
	 */
	private int checkLeapYear(){
		int year = Calendar.getInstance().get(Calendar.YEAR);
		if (year%4==0){
			if (year%100==0){
				if (year%400==0){
					return 366;
				}
				else{
					return 365;
				}
			}
			else{
				return 366;
			}
		}
		else{
			return 365;
		}
	}

	/**
	 * shows a dialog with option to open setting
	 * @param title dialog title
	 * @param message dialog message
	 * @param isCancelable if dialog is cancelable
	 */
	private void showAlertDialogInternet(String title, String message,
										boolean isCancelable) {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle(title);
		alert.setMessage(message);
		alert.setCancelable(isCancelable);
		alert.setPositiveButton("Aceptar",(dialogInterface, i) -> dialogInterface.dismiss());
		alert.setNeutralButton("Abrir Ajustes",(dialogInterface, i) -> openDateSettings());
		alert.create().show();
	}

	/**
	 * With this method you can get city, state or country.
	 * It's useful because only one APK file has to be build and
	 * can be used with different cities without change targetBranch
	 * in BuildConfig class.
	 *
	 * @author Jordan Alvarez
	 */
	private boolean getCityStateAutomatically(){
		//GPSProvider gpsProvider = new GPSProvider(this);
		//double lat = gpsProvider.getPosicion().getLatitude();
		//double lon = gpsProvider.getPosicion().getLongitude();

		Geocoder gcd = new Geocoder(this, Locale.getDefault());

		try {
			List<Address> addresses = gcd.getFromLocation(21.232326, -104.909419, 1);
			if(addresses.size() > 0){
				Util.Log.ih(addresses.get(0).getLocality());//city
				Util.Log.ih(addresses.get(0).getAdminArea());//state
				Util.Log.ih(addresses.get(0).getCountryName());//country
				return true;
			}
			else {
				Util.Log.ih("location not detected");
				return true;
			}
		} catch (IOException e){
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		return true;
	}

	//endregion

	//region Protected

	@Override
	protected void init() {
		super.init();
		setContentView(R.layout.activity_login);
		loginLayout = findViewById(R.id.rl_padre);
		tvInternet=(TextView) findViewById(R.id.tvInternet);
		checkInternetConnection();
		dialogo_de_bloqueo = new Dialog(getContext());
		dialogo_de_bloqueo.setContentView(R.layout.aviso_bloqueo_extravio);
		dialogo_de_bloqueo.setCancelable(false);
		dialogo_de_bloqueo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));



		btSolicitarDesbloqueo = (Button) dialogo_de_bloqueo.findViewById(R.id.btSolicituar);
		progressBar=(ProgressBar) dialogo_de_bloqueo.findViewById(R.id.progressBar);

		btSolicitarDesbloqueo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				verificarInternet();
				if(verificarInternet()) {
					btSolicitarDesbloqueo.setVisibility(View.GONE);
					progressBar.setVisibility(View.VISIBLE);
					getNumNotificaciones();
				}
				else
					Toast.makeText(getApplicationContext(), "Conectate a internet", Toast.LENGTH_LONG).show();
			}
		});


		final Bundle extras = getIntent().getExtras();

		if(extras!=null)
		{
			try
			{
				bandera= extras.getString("bandera");
				if(bandera.equals("1"))
				{
					LogRegister.registrarCheat(user);
					try {
						DatabaseAssistant.resetDatabase( ApplicationResourcesProvider.getContext() );
						getSharedPreferences("folios", 0).edit().clear().commit();
						getEfectivoPreference().edit().clear().commit();
						toastL("reseteando base de datos");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}catch (Exception e	)
			{
				e.printStackTrace();
			}
		}
		else
		{
			Toast.makeText(getApplicationContext(), "No se recuperaron Extras", Toast.LENGTH_SHORT);
		}

		hasActiveInternetConnection(this, loginLayout);

		Mint.setUserIdentifier(loadUser());
		btnConfiguracion = (ImageView)findViewById(R.id.btnConfig);
        myDialog= new Dialog(this);
		mthis = this;

		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String version = pInfo.versionName;
			((TextView) findViewById(R.id.tv_version_code))
					.setText("Cobranza Móvil - Versión " + version);
		} catch (NameNotFoundException e1) {
			e1.printStackTrace();
		}
		if (isConnected) {
			//RegisterPagos();
		}
		setEstoyLogin(true);



		SharedPreferences preferences = getSharedPreferences("Login", Context.MODE_PRIVATE);
		boolean robadoPreferences = preferences.getBoolean("robado", false);
		Log.i("LOGIN ROBADO-->", " ------> " + String.valueOf(robadoPreferences));
		if(robadoPreferences) {
			robado = "1";
			Log.i("LOGIN ROBADO-->", "dialogo: " + String.valueOf(dialogo_de_bloqueo.isShowing()));
			Log.i("LOGIN ROBADO-->", "timer: " + String.valueOf(timerNotificaciones));
			Log.i("LOGIN ROBADO-->", "isShowed: " + String.valueOf(isShowedLockedApp));

			if(!dialogo_de_bloqueo.isShowing() && timerNotificaciones==null && !isShowedLockedApp) {
				dialogo_de_bloqueo.show();
				isShowedLockedApp = true;
			}
		}
		else {
			robado = "0";
			isShowedLockedApp=false;
		}


		/**
		 * Timer to get notifications, starts
		 */
		timerNotificaciones = null;
		timerNotificaciones = new Timer();
		timerNotificaciones.schedule(new TimerTask() {
			@Override
			public void run() {
				if (isConnected) {
					Util.Log.ih("getting personal notifications");
					getNumNotificaciones();
				}
			}
		}, 1000, 5000);//1000, (1000 * 30) * 30);















			cargarUsuario();
			setListeners();





























		PreferenceSettings preferenceSettings = new PreferenceSettings(ApplicationResourcesProvider.getContext());
		BuildConfig.setWalletSynchronizationEnabled(preferenceSettings.isColorContractsActive());

		if (BuildConfig.isAutoLoginEnable()){

			EditText euser = (EditText) findViewById(R.id.editTextUser);
			EditText epass = (EditText) findViewById(R.id.editTextPass);
			euser.setText("c0006");
			epass.setText("yxpc0006");
			findViewById(R.id.buttonLogin).performClick();
		}

		btnConfiguracion.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v)
            {
                //lanzar un CustomDialog para pedir la contraseña de ingreso
                Vibrator vibra = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                vibra.vibrate(0700);
				showPopup(v);
                return false;
            }
        });
		Util.Log.ih("60 segundos");
	}

	private boolean verificarInternet()
	{
		ConnectivityManager connection = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connection.getActiveNetworkInfo();
		if(networkInfo !=null && networkInfo.isConnected())
			return true;
		else
			return false;
	}


	private void getNumNotificaciones() {
		JSONObject json = new JSONObject();
		try {
			String idCobrador = "";
			try {
				SharedPreferences preferences = ApplicationResourcesProvider.getContext().getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
				//idCobrador = preferences.getString("no_cobrador", null);
				idCobrador = getEfectivoPreference().getString("no_cobrador", "");
				//idCobrador = datosCobrador.getString("no_cobrador");
			} catch (Exception e) {
				e.printStackTrace();
			}
			json.put("no_cobrador", idCobrador);
		} catch (Exception e) {
			e.printStackTrace();
		}

		requestGetNumNotificaciones(json);
	}

	private void requestGetNumNotificaciones(JSONObject jsonParams)
	{
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlGetNumNotificaciones, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				SharedPreferences preferences = getSharedPreferences("Login", Context.MODE_PRIVATE);
				boolean robado = preferences.getBoolean("robado", false);
				manage_GetNumNotificaciones(response);
				Log.i("PREFERNCES ROBADO-->", "------> " + String.valueOf(robado));
				Log.i("LOGIN ROBADO-->", new Gson().toJson(response));


			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						Log.e("getNumNotificaciones", "Error al obtener numero de notificaciones");
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(getApplicationContext()).addToRequestQueue(postRequest);
	}

	private void manage_GetNumNotificaciones(JSONObject json) {
		if (json.has("result")) {
			try {
				robado = json.getString("robado");

				if (robado.equals("0")) {
					if(dialogo_de_bloqueo!=null) {
						dialogo_de_bloqueo.dismiss();
						progressBar.setVisibility(View.GONE);
						btSolicitarDesbloqueo.setVisibility(View.VISIBLE);
						dialogo_de_bloqueo.cancel();
						//Toast.makeText(getApplicationContext(), "Desbloquado satisfactoriamente", Toast.LENGTH_LONG).show();
					}

					if(dialogo_de_bloqueo==null)
						timerNotificaciones.cancel();

					SharedPreferences preferences = getContext().getSharedPreferences("Login", Context.MODE_PRIVATE);
					Editor editor= preferences.edit();
					editor.putBoolean("robado", false);
					editor.apply();

					isShowedLockedApp= false;
				}
				else if(robado.equals("1"))
				{
					SharedPreferences preferences = getContext().getSharedPreferences("Login", Context.MODE_PRIVATE);
					Editor editor= preferences.edit();
					editor.putBoolean("robado", true);
					editor.apply();

					if(dialogo_de_bloqueo!=null && timerNotificaciones!=null && !isShowedLockedApp) {
						dialogo_de_bloqueo.show();
						isShowedLockedApp = true;
					}

					if(dialogo_de_bloqueo==null)
						timerNotificaciones.cancel();
				}

			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	}

	//endregion

	//region Listeners

	/**
	 * Este metodo setea los listeners generales para el funcionamiento de
	 * botones.
	 */
	private void setListeners() {


		ImageView ivSettings = (ImageView) findViewById(R.id.ivSettins);
		ivSettings.setOnClickListener(view -> {
            Intent intent = new Intent(ApplicationResourcesProvider.getContext(), SettinsActivity.class);
            startActivity(intent);
        });


		EditText pass = (EditText) findViewById(R.id.editTextPass);

		pass.setOnEditorActionListener((textView, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_GO)
            {
                MotionEvent motionEvent = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis()+100,MotionEvent.ACTION_UP, 0.0f, 0.0f, 0);
                buttonLogin.dispatchTouchEvent(motionEvent);
            }

            return false;
        });



		buttonLogin = (Button) findViewById(R.id.buttonLogin);

		//debugging purposes
		if (BuildConfig.isAutoLoginEnable())
		{


			buttonLogin.setOnClickListener(view ->
			{

                final LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                final boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                Button buttonLogin = (Button) findViewById(R.id.buttonLogin);
                buttonLogin.setBackgroundResource(R.drawable.btn_iniciar);


                if (!isGPSEnabled) {
                    Toast.makeText(getApplicationContext(), "Enciende el gps para continuar.", Toast.LENGTH_LONG).show();
                }

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String cadenaUno = ".";
                String cadenaDos = "";
                boolean inicarSesionOffline = true;


                try {
                    SharedPreferences preferences = getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
                    if (!preferences.getString("fecha_sesion", "").equals("")) {
                        inicarSesionOffline = true;
                    }
                } catch (Exception e) {
                    inicarSesionOffline = false;
                    e.printStackTrace();
                }


                if (camposLlenos()) {
                    boolean hayDatosCobrador = false;

                    hayDatosCobrador = DatabaseAssistant.getCompanies().size() > 0;

                    if (!hayDatosCobrador) {
                        iniciarSesion(inicarSesionOffline);
                    } else {

                        SharedPreferences preferences = getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
                        String userPref = preferences.getString("user_login", "");
                        String lastSesionString = preferences.getString("fecha_sesion", "no hay fechas");

                        if (user.equals(userPref))
                        {
                            iniciarSesion(inicarSesionOffline);
                        } else {
                            if (lastSesionString.equals("no hay fechas")) {
                                iniciarSesion(false);
                            } else if (!user.equals(userPref)) {
                                try {
                                    showAlertDialog("Error", "El usuario " + user + " no pertenece a este dispositivo", false);
                                } catch (Exception e) {
                                    Toast toast = Toast.makeText(getApplicationContext(), "Error: El usuario " + user + " no pertenece a este dispositivo", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    showAlertDialog("", "El usuario " + userPref + " no ha realizado cierre.", false);
                                } catch (Exception e) {
                                    Toast toast = Toast.makeText(getApplicationContext(), "El usuario " + userPref + " no ha realizado cierre", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                    e.printStackTrace();
                                }
                            }

                        }

                    }
                } else {
                    try {
                        showAlertDialog("", getString(R.string.error_message_fields), true);
                    } catch (Exception e) {
                        Toast toast = Toast.makeText(getApplicationContext(), R.string.error_message_fields, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        e.printStackTrace();
                    }
                }
            });



		}
		else{
			/*
			 * ButtonLogin gets disabled preventing it from being pressed more
			 * than once, so that only one session is runnig.
			 * Gets enabled when an error happens or when succeed logging in,
			 * giving the user the chance to try to log in again
			 */
			buttonLogin.setOnTouchListener((view, motionEvent) -> {

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    Button buttonLogin = (Button) findViewById(R.id.buttonLogin);
                    buttonLogin.setBackgroundResource(R.drawable.btn_iniciar_pressed);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    buttonLogin.setEnabled(false);

                    final LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                    final boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                    Button buttonLogin = (Button) findViewById(R.id.buttonLogin);
                    buttonLogin.setBackgroundResource(R.drawable.btn_iniciar);


                    if (!isGPSEnabled) {
                        buttonLogin.setEnabled(true);
                        Toast.makeText(getContext(), "Enciende el gps para continuar.", Toast.LENGTH_SHORT).show();
                        return true;
                    }

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String cadenaUno = ".";
                    String cadenaDos = "";

                    boolean inicarSesionOffline = false;
                    try {
                        SharedPreferences preferences = getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
                        if (!preferences.getString("fecha_sesion", "").equals(""))
                        {
                            inicarSesionOffline = true;
                        }
                    } catch (Exception e) {
                        inicarSesionOffline = false;
                        e.printStackTrace();
                    }

                    if (camposLlenos()) {

                        boolean hayDatosCobrador = false;
                        hayDatosCobrador = DatabaseAssistant.getCompanies().size() > 0;

                        if (!hayDatosCobrador) {
                            iniciarSesion(inicarSesionOffline);

                        } else {

                            SharedPreferences preferences = getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
                            String userPref = preferences.getString("user_login", "");

                            String lastSesionString = preferences.getString("fecha_sesion", "no hay fechas");

                            Util.Log.ih("user_login = " + userPref);
                            Util.Log.ih("fecha_sesion = " + lastSesionString);
                            Util.Log.ih("iniciarSesionOffile = " + inicarSesionOffline);

                            if (user.equals(userPref.replace(" ", "")))
                            {
                                iniciarSesion(inicarSesionOffline);
                            } else
                            	{
                                if (lastSesionString.equals("no hay fechas"))
                                {
                                    iniciarSesion(false);

                                } else if (!user.equals(userPref)) {

                                	buttonLogin.setEnabled(true);
                                    try
									{
                                        showAlertDialog("Error", "El usuario " + user + " no pertenece a este dispositivo", false);
                                    } catch (Exception e) {
                                        Toast toast = Toast.makeText(getApplicationContext(), "Error: El usuario " + user + " no pertenece a este dispositivo", Toast.LENGTH_LONG);
                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                        toast.show();
                                        e.printStackTrace();
                                    }
                                } else {
                                    buttonLogin.setEnabled(true);
                                    try {
                                        showAlertDialog("", "El usuario " + userPref + " no ha realizado cierre.", false);

                                    } catch (Exception e) {
                                        Toast toast = Toast.makeText(getApplicationContext(), "El usuario " + userPref + " no ha realizado cierre", Toast.LENGTH_LONG);
                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                        toast.show();
                                        e.printStackTrace();
                                    }
                                }

                            }

                        }
                        //} else {
                        //	buttonLogin.setEnabled(true);
                        //}

                    } else {
                        buttonLogin.setEnabled(true);
                        try {
                            showAlertDialog("",
                                    getString(R.string.error_message_fields), true);
                        } catch (Exception e) {
                            Toast toast = Toast.makeText(getApplicationContext(), R.string.error_message_fields, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            e.printStackTrace();
                        }
                    }

                }

                return false;

            });
		}
	}

	private Login getContext(){
		return this;
	}

	//endregion

	//region Interfaces

	//region OnKeyDown

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			moveTaskToBack(true);
		}
		return super.onKeyDown(keyCode, event);
	}

	//endregion

	//endregion

	//region Getters

	/**
	 * Gets context from this class.
	 * In some cases you cannot just call 'this', because another context is envolved
	 * or you want a reference to an object of this class from another class,
	 * then this context is helpful.
	 *
	 * NOTE
	 * Not recommended, it may cause NullPointerException though
	 *
	 * @return context from Login
	 */
	public static Login getMthis() {
		return mthis;
	}

	/**
	 * returns an instance of Dialog
	 * @return instance of Dialog
	 */
	private Dialog getDialog(){
		return new Dialog(this);
	}

	/**
	 * returns an instance of AlertDialogBuilder
	 * @return instance of AlertDialogBuilder
	 */
	private AlertDialog.Builder getAlertDialogBuilder(){
		AlertDialog.Builder alert = null;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			alert = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
		}
		else{
			alert = new AlertDialog.Builder(this);
		}
		return alert;
	}

	/**
	 * returns an instance of ProgressDialog
	 * @return instance of ProgressDialog
	 */
	private ProgressDialog getProgressDialog(){
		return new ProgressDialog(this);
	}

	//endregion

	//region Setters

	/**
	 * Sets 'mthis' global variable as Login's context
	 * @param mthis Login's context
	 */
	public static void setMthis(Login mthis) {
		Login.mthis = mthis;
	}

	//lanzar el customDialog------------------------------------------------------------------------
    public void showPopup(View view)
    {
        TextView tvAceptar, tvCancelar;
        EditText etPassword;
        myDialog.setContentView(R.layout.dialog_password);
        tvAceptar=(TextView) myDialog.findViewById(R.id.tvAccept);
        tvCancelar=(TextView) myDialog.findViewById(R.id.tvCancel);
        etPassword =(EditText)myDialog.findViewById(R.id.etPassword);

        tvAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                //if(etPassword.getText().toString().equals("sistemas2%"))
                //{
                	Intent intent = new Intent(mthis, Configuracion.class);
                	intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                	startActivity(intent);
                //}
                //else
                //{

                //}
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();


    }
    //----------------------------------------------------------------------------------------------

	//endregion

	//endregion


	@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	public boolean isAirplaneModeOn(Context context) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
			return Settings.System.getInt(context.getContentResolver(),
					Settings.System.AIRPLANE_MODE_ON, 0) != 0;
		} else {
			return Settings.Global.getInt(context.getContentResolver(),
					Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
		}
	}

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		checkInternetConnection();
	}

	public boolean checkInternetConnection()
	{
		ConnectivityManager con = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = con.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected())
		{
			tvInternet.setVisibility(View.VISIBLE);
			tvInternet.setText("Si hay Internet");
			tvInternet.setBackgroundColor(Color.parseColor("#255d00"));
			tvInternet.setCompoundDrawablesWithIntrinsicBounds(R.drawable.si_wifi, 0, 0, 0);
			return true;
		}
		else
		{
			tvInternet.setVisibility(View.VISIBLE);
			tvInternet.setText("No hay Internet");
			tvInternet.setCompoundDrawablesWithIntrinsicBounds(R.drawable.no_wifi, 0, 0, 0);
			tvInternet.setBackgroundColor(Color.RED);
			return false;
		}
	}


}