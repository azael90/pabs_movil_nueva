package com.jaguarlabs.pabs.activities;

import android.os.DeadObjectException;
import android.os.RemoteException;

/**
 * The core Android system has died and is going through a runtime restart. All
 * running apps will be promptly killed.
 */
public class DeadSystemException extends DeadObjectException {
    public DeadSystemException() {
        super();
    }
}