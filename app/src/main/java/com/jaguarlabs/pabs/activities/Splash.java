package com.jaguarlabs.pabs.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.widget.FrameLayout;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.database.ExportImportDB;
import com.jaguarlabs.pabs.net.VolleySingleton;
import com.jaguarlabs.pabs.util.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;
import pl.tajchert.nammu.PermissionListener;

public class Splash extends ExtendedActivity {

	//region Variables

	//region Primitives

	private static int SPLASH_TIME_OUT = 3500;

	//endregion

	//region Objects

	public static Splash mthis = null;

	private ConstraintLayout splashLayout;

	//endregion

	//endregion

	//region Lifecycle

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		splashLayout = findViewById(R.id.linear_splash);
		mthis = this;
	}

	@Override
	public void onResume() {
		super.onResume();

		GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
		int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
		if (status != ConnectionResult.SUCCESS)
		{
			if (googleApiAvailability.isUserResolvableError(status))
			{
				googleApiAvailability.getErrorDialog(this, status, 2404, dialog -> finish()).show();
			}
			else
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(this);

				builder.setTitle(googleApiAvailability.getErrorString(status));
				builder.setMessage("Ocurrió un problema con los Servicios de Google Play y la aplicación no se puede iniciar.");
				builder.setNeutralButton("Aceptar", (dialogInterface, i) -> finish());
				builder.setOnCancelListener(dialogInterface -> finish());

				builder.show();
			}
		}
		else
		{
			new Handler().postDelayed(this::startLogin, SPLASH_TIME_OUT);

			//check internet reachability since app starts
			hasActiveInternetConnection(this, splashLayout);
		}
	}

	//endregion

	//region Methods

	//region Private


	/**
	 * Starts activities either Bloqueo or Login
	 * This method is called after 2 seconds of waiting
	 */
	private void startLogin() {
		SharedPreferences preferences = getSharedPreferences("PABS_SPLASH", Context.MODE_PRIVATE);

		//deprecated in 3.0
		//TODO: remove this code - Jordan Alvarez

		if (preferences.contains("idStatus")) {
			startActivity(new Intent(mthis, Bloqueo.class)
					.putExtra("datos_cobrador", preferences.getString("datos_cobrador", ""))
					.putExtra("vengo_de_splash", true)
					.putExtra("iniciar_time_sesion", true));
			finish();
		} else {

			startActivity(new Intent(this, Login.class));
			finish();
		}
	}

	//endregion

	//endregion

}
