package com.jaguarlabs.pabs.activities;

import com.jaguarlabs.pabs.rest.OfflineService;
import com.jaguarlabs.pabs.rest.RestService;
import com.jaguarlabs.pabs.rest.RestServiceTest;
import com.octo.android.robospice.SpiceManager;

/**
 * This class is the base class of all activities of the sample project.
 * 
 * Typically, in a new project, you will have to create a base class like this one and copy the content of the
 * {@link BaseSpiceActivity} into your own class.
 * 
 * @author sni
 * 
 */
public class BaseSpiceActivity extends MintActivity {
    private SpiceManager spiceManagerOffline = new SpiceManager(OfflineService.class);
    private SpiceManager spiceManager = new SpiceManager(RestService.class);
    private SpiceManager spiceManagerTest = new SpiceManager(RestServiceTest.class);

    @Override
    public void onStart() {
        spiceManagerOffline.start( this );
        spiceManager.start( this );
        spiceManagerTest.start( this );
        super.onStart();
    }

    @Override
    public void onStop() {
        spiceManagerOffline.shouldStop();
        spiceManager.shouldStop();
        spiceManagerTest.shouldStop();
        super.onStop();
    }

    public SpiceManager getSpiceManagerOffline() {
        return spiceManagerOffline;
    }

    public SpiceManager getSpiceManager(){
        return spiceManager;
    }

    public SpiceManager getSpiceManagerTest(){
        return spiceManagerTest;
    }

}
