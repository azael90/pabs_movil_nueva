package com.jaguarlabs.pabs.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Outline;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Trace;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tscdll.TSCActivity;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.AdapterCobrosRealizadosEnElPeriodo;
import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.bluetoothPrinter.TSCPrinterHelper;
import com.jaguarlabs.pabs.components.Preferences;
import com.jaguarlabs.pabs.database.Actualizados;
import com.jaguarlabs.pabs.database.Clients;
import com.jaguarlabs.pabs.database.Collector;
import com.jaguarlabs.pabs.database.Collectors;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.Configuraciones;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.ExportImportDB;
import com.jaguarlabs.pabs.database.Localidad_Colonia;
import com.jaguarlabs.pabs.database.MontosTotalesDeEfectivo;
import com.jaguarlabs.pabs.database.Notice;
import com.jaguarlabs.pabs.database.Suspensionitem;
import com.jaguarlabs.pabs.dialog.RazonesDialog;
import com.jaguarlabs.pabs.models.ModelCobrosRealizadosPorPeriodo;
import com.jaguarlabs.pabs.models.ModelPeriod;
import com.jaguarlabs.pabs.rest.SpiceHelper;
import com.jaguarlabs.pabs.rest.models.ModelPrintPaymentRequest;
import com.jaguarlabs.pabs.rest.request.offline.PrintAccountStatusTicket;
import com.jaguarlabs.pabs.rest.request.offline.PrintPaymentTicketRequest;
import com.jaguarlabs.pabs.rest.request.offline.PrintTemporalSuspensionNotice;
import com.jaguarlabs.pabs.rest.request.offline.PrintVisitTicketRequest;
import com.jaguarlabs.pabs.util.*;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.SharedConstants.Printer;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.splunk.mint.Mint;
import com.splunk.mint.MintLogLevel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// --PABS 3--//

/**
 * Class used when collector ir trying to make either a payment or
 * a visit to a client selected on Clientes screen.
 */
@SuppressWarnings({"SpellCheckingInspection"})
@SuppressLint({"ShowToast", "SimpleDateFormat"})
public class ClienteDetalle extends Fragment implements
        NoAportoCallback {

    //Dialog dialogo_suspencion;

    public String coloniaSelecionada;
    DatePickerDialog.OnDateSetListener mDateSetListener;
    public int diia, mess, ano;
    String opcionSeleccionada = "";
    String TAG = "PABS TIME";
    private boolean pendingPrintingRequest;
    boolean imprimio = false;
    private int selected = 0;
    private static ClienteDetalle mthis = null;
    private Dialog dialog;
    String mensaje = "";
    String no_cobro = "";
    boolean showMessagePercapita = false;
    private String codigoCobrador = "";
    boolean canuse = true;
    private String mensajePercapita = "", tituloPercapitaGeneral = "";
    String nombre = "";
    String datosfiscales = "";
    String empresaString = "";
    String datoempresa1 = "";
    double montoactual = 0;
    String rfc = "";
    String nDias = "";
    String datoempresa2 = "";
    String datoempresa3 = "";
    String datoempresa4 = "";
    String[] direccion;
    String datoempresa4_5 = "";
    String apellido = " ";
    String datoempresa5 = "";
    int ImpresionCase = 0;
    String ult_folio = "";
    String new_folio = "";
    String datoempresa6 = "";
    String datoempresa7 = "";
    String no_contrato = "";
    String total = "";
    String serie = "";
    String efectivo = "";
    String empresa = "";
    String telefono = "";
    Dialog myDialog, myDialog2, myDialog3, dialogo_suspencion, dialogo_cobros_realizados;
    int impreso = 0;
    String idContrato = "";
    private JSONObject jsonDatos;
    private JSONObject result;
    private JSONObject datosCobrador;
    TextView test;
    private int typeStatus;
    private double latitud = 20.682413;
    private double longitud = -103.381570;
    public static boolean pendingCobro = false;
    private Dialog newsecondDialog;
    private ProgressDialog pd;
    //AsyncPrint asyncSaldo;
    private Dialog commentsDialog;
    private boolean pendingTicket = false;
    //public GPSProvider gpsProviderClienteDetalle;
    Preferences preferences;
    private float cashPreferenceBeforeChange = 0;
    private long dateAux;
    private Timer timer;
    //private nt copiesCounter = 0;
    private EditText etAmount;
    private String amount;
    private AlertDialog.Builder tryAgainDialog;
    private ImageView estado;
    private Button aporto;
    private Button noaporto;
    private ImageView cellphoneNumber;
    private Dialog secondDialog;
    private TextView tvAceptar;
    private TextView tvCancelar;
    private TextView cancelSavingCellphoneNumber;
    private TextView cancelSavingPaymentReference;
    private TextView imprimir;
    private TextView saveCellPhoneNumber;
    private TextView savePaymentReference;
    private int copies = 0;
    private boolean commentAdded = false;
    private String comment;
    private String paymentReferenceString = "";
    private MainActivity mainActivity;
    private SpiceManager spiceManagerOffliine;
    private SpiceHelper<Boolean> spicePrintVisitTicket;
    private SpiceHelper<Boolean> spicePrintAccountStatusTicket;
    private SpiceHelper<Boolean> spicePrintPaymentTicket;
    private SpiceHelper<Boolean> spicePrintNotice;
    private ModelPrintPaymentRequest modelPrintPaymentRequest;
    private Preferences preferencesPendingTicket;
    private OnFragmentResult fragmentResult;
    private int requestCode = -1;
    public static ProgressBar printerConnecting;
    public LinearLayout layoutGuardado;
    private android.support.v7.app.AlertDialog.Builder dialogo;
    public boolean dialogoCancelable = false;
    ImageView amountButton200, amountButton300, amountButton400, amountButton500, amountButton75, amountButton150, amountButton300New;

    public JSONArray arrarJSONGeneral = new JSONArray();


    private ImageView imgActualizarDatos;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_cliente_detalle, container, false);
        mainActivity = (MainActivity) getActivity();
        myDialog = new Dialog(getContext());
        myDialog2 = new Dialog(getContext());
        myDialog3 = new Dialog(getContext());
        dialogo_cobros_realizados = new Dialog(getContext());
        //dialogo_suspencion = new Dialog(getContext());
        spiceManagerOffliine = mainActivity.getSpiceManagerOffline();
        if (view != null) {
            amountButton200 = (ImageView) view.findViewById(R.id.imageViewOp1);
            amountButton300 = (ImageView) view.findViewById(R.id.imageViewOp2);
            amountButton400 = (ImageView) view.findViewById(R.id.imageViewOp3);
            amountButton500 = (ImageView) view.findViewById(R.id.imageViewOp4);
            amountButton75 = (ImageView) view.findViewById(R.id.imageViewOp5);
            amountButton150 = (ImageView) view.findViewById(R.id.imageViewOp6);
            amountButton300New = (ImageView) view.findViewById(R.id.imageViewOp7);
        }



        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        printerConnecting = view.findViewById(R.id.printerConnecting);
        requestCode = getArguments().getInt("requestCode", -1);
        imgActualizarDatos = (ImageView) view.findViewById(R.id.imgActualizarDatos);
        // verificarFechasde3Meses();


        spicePrintVisitTicket = new SpiceHelper<Boolean>(spiceManagerOffliine, "printVisitTicketCache", Boolean.class, 1000 * 30) {

            /**
             * if return is NULL
             * check your cache time
             *
             * @param result
             */
            @Override
            protected void onSuccess(Boolean result) {
                if (result != null) {
                    Util.Log.ih("result != null");
                    super.onSuccess(result);
                    //checkPrinterStatus();
                    if (result) {
                        ConstantsPabs.finishPrinting = false;
                        procesarNoAporto();
                    } else {
                        ConstantsPabs.finishPrinting = false;
                        fail();
                        mainActivity.toastL(R.string.error_message_printing);
                    }
                } else {
                    Util.Log.ih("result == null");
                    //if (ConstantsPabs.finishPrinting) {
                    super.onSuccess(ConstantsPabs.finishPrinting);
                    //checkPrinterStatus();
                    if (ConstantsPabs.finishPrinting) {
                        ConstantsPabs.finishPrinting = false;
                        procesarNoAporto();
                    } else {
                        ConstantsPabs.finishPrinting = false;
                        fail();
                        mainActivity.toastL(R.string.error_message_printing);
                    }
                    //}
                }
            }

            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                fail();
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                super.onProgressUpdate(progress);
                switch ((int) progress.getProgress()) {
                    case ConstantsPabs.START_PRINTING:
                        showMyCustomDialog();
                        break;
                    case ConstantsPabs.FINISHED_PRINTING:
                        dismissMyCustomDialog();
                        break;
                    default:
                        //nothing
                }
            }
        };

        spicePrintAccountStatusTicket = new SpiceHelper<Boolean>(spiceManagerOffliine, "printAccountStatusTicket", Boolean.class, DurationInMillis.ONE_HOUR) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                fail();
            }

            @Override
            protected void onSuccess(Boolean result) {
                super.onSuccess(result);
                if (!result) {
                    mainActivity.toastL(R.string.error_message_printing);
                }
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                super.onProgressUpdate(progress);
                switch ((int) progress.getProgress()) {
                    case ConstantsPabs.START_PRINTING:
                        showMyCustomDialog();
                        break;
                    case ConstantsPabs.FINISHED_PRINTING:
                        dismissMyCustomDialog();
                        break;
                    default:
                        //nothing
                }
            }
        };













       /* spicePrintNotice = new SpiceHelper<Boolean>(spiceManagerOffliine, "print_notice", Boolean.class, 1000 * 30)
        {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                onServicioImpresionFailure(exception);
            }

            @Override
            protected void onSuccess(Boolean result) {
                try {
                    if (result != null) {
                        ConstantsPabs.finishPrinting = false;
                        super.onSuccess(result);
                        Util.Log.ih("onSuccess - spicePrintNotice");

                    } else {
                        super.onSuccess(ConstantsPabs.finishPrinting);
                        Util.Log.ih("onSuccess - spicePrintNotice");
                        ConstantsPabs.finishPrinting = false;
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    ConstantsPabs.finishPrinting = false;
                    super.onSuccess(true);
                    Util.Log.ih("NullPointerException - onSuccess - spicePrintNotice");
                }
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                progressUpdate(progress);
            }
        };*/


        spicePrintPaymentTicket = new SpiceHelper<Boolean>(spiceManagerOffliine, "print_payment_ticket", Boolean.class, 1000 * 30) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                onServicioImpresionFailure(exception);
            }

            @Override
            protected void onSuccess(Boolean result) {
                try {
                    if (result != null) {
                        Util.Log.ih("result != null");

                        ConstantsPabs.finishPrinting = false;

                        super.onSuccess(result);
                        Util.Log.ih("onSuccess - spicePrintPaymentTicket");
                        onServicioImpresionSuccess(result);
                    } else {
                        Util.Log.ih("result == null");

                        super.onSuccess(ConstantsPabs.finishPrinting);
                        Util.Log.ih("onSuccess - spicePrintPaymentTicket");
                        onServicioImpresionSuccess(ConstantsPabs.finishPrinting);

                        ConstantsPabs.finishPrinting = false;
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();

                    ConstantsPabs.finishPrinting = false;

                    //setPaymentNotSync();
                    if (!preferencesPendingTicket.isThereAPaymentAndIsSynchronized()) {
                        setPaymentNotSync();
                    }

                    super.onSuccess(true);
                    Util.Log.ih("NullPointerException - onSuccess - spicePrintPaymentTicket");
                    onServicioImpresionSuccess(true);
                }
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                progressUpdate(progress);
            }
        };

        //init

        //GPSProvider instance
        if (ApplicationResourcesProvider.getLocationProvider().getGPSPosition() == null) {
            Util.Log.ih("ApplicationResourcesProvider.getLocationProvider().getGPSPosition() == null");
              /*
              gpsProviderClienteDetalle = new GPSProvider(this);
              Util.Log.ih("gpsProviderClienteDetalle = " + gpsProviderClienteDetalle.isConnected);//will return false because was just created
              */
        } else {
            Util.Log.ih("LocationProvider: "
                    + " latitude = " + ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLatitude()
                    + " longitude = " + ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLongitude());
        }

        preferences = new Preferences(getContext());

        //hasActiveInternetConnection(this);

        etAmount = (EditText) view.findViewById(R.id.et_cantidad_aportacion);

        mthis = this;
        ExtendedActivity.setEstoyLogin(false);
        pendingCobro = false;
        pendingPrintingRequest = false;
          /*
          SqlQueryAssistant query = new SqlQueryAssistant(this);
          try {
              mensaje = query.MostrarDatosCobrador().getString("mensaje");
          } catch (JSONException e1) {
              e1.printStackTrace();
          } finally {
              /*
              if (query.db != null){
                  query.db.close();
              }
              query = null;
              */
        //}
        mensaje = DatabaseAssistant.getCollector().getMessage();

        //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey("datos")) {
                try {
                    Util.Log.ih("bundle.getString(datos) = " + bundle.getString("datos"));
                    jsonDatos = new JSONObject(bundle.getString("datos"));
                    datosCobrador = new JSONObject(bundle.getString("datoscobrador"));
                    empresa = jsonDatos.getString("id_grupo_base");
                    montoactual = jsonDatos.getDouble("monto_pago_actual");
                    idContrato = jsonDatos.getString("id_contrato");

                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    if (jsonDatos == null) {
                        jsonDatos = new JSONObject();
                    }
                }
            }
            if (bundle.containsKey("isConnected")) {
                mainActivity.isConnected = bundle.getBoolean("isConnected");
            }
        }

        llenarCampos();
        setListeners();
        setListenersOpciones();


        //if(BuildConfig.getTargetBranch()== BuildConfig.Branches.GUADALAJARA)
        //{

        List<Collectors> listaCollectors = Collectors.findWithQuery(Collectors.class, "SELECT * FROM COLLECTORS");
        if (listaCollectors.size() > 0) {
            String mostrarActualizarDatos = listaCollectors.get(0).getMostrar_actualizar_datos();
            System.out.println(mostrarActualizarDatos);
            if (mostrarActualizarDatos.equals("1"))
                dialogoCancelable = true;
            else
                dialogoCancelable = false;
        }


        verificarCreditoPercapita(idContrato); //}

        if (BuildConfig.isAutoPrintingEnable()) {
            Handler handler = new Handler();
            handler.postDelayed(() -> actionAporto(null), 5000);
        }

        preferencesPendingTicket = new Preferences(getContext());


    }

    /**
     * manages progress dialog when printing
     *
     * @param progress
     */
    private void progressUpdate(RequestProgress progress) {
        switch ((int) progress.getProgress()) {
            case ConstantsPabs.START_PRINTING:
                showMyCustomDialog();
                break;
            case ConstantsPabs.FINISHED_PRINTING:
                dismissMyCustomDialog();
                break;
            default:
                //nothing
        }
    }

    /**
     * manages when printing failed
     */
    private void fail() {
        dismissMyCustomDialog();
        mainActivity.toastL(R.string.error_message_printing);
    }

    /**
     * set payment as not sync
     * when something went wrong while printing
     */
    public void setPaymentNotSync() {
        Preferences preferencesPendingTciet = new Preferences(ApplicationResourcesProvider.getContext());
        preferencesPendingTciet.setPaymentNotSync();
    }

    public boolean goBack = true;

    /**
     * shows framelayout as progress dialog
     */
    private void showMyCustomDialog() {

        goBack = false;

        if (getView() != null) {
            final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
            flLoading.setVisibility(View.VISIBLE);
        }

        //final Animation qwe = AnimationUtils.loadAnimation(this, R.animator.rotate_around_center_point);
        //final ImageView ivLoadingIcon = (ImageView) ClienteDetalle.this.findViewById(R.id.ivLoading);
        //ivLoadingIcon.startAnimation(qwe);
    }

    /**
     * hides framelayout used as progress dialog
     */
    public void dismissMyCustomDialog() {

        goBack = false;

        if (getView() != null) {
            final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
            flLoading.setVisibility(View.GONE);
        }

        //final ImageView ivLoadingIcon = (ImageView) ClienteDetalle.this.findViewById(R.id.ivLoading);
        //ivLoadingIcon.clearAnimation();
    }

    /*@Override
      public void onUserInteraction() {
          Log.d("onUserInteraction ", "Cliente Detalle");
          View refMain = null;
          if (Clientes.getMthis() != null) {
              refMain = Clientes.getMthis().getView().findViewById(R.id.listView_clientes);
          }

          if (refMain != null) {
              try {
                  Clientes.getMthis().llamarTimerInactividad();
                  // Clientes.getMthis().cerrarSesionTimeOut();
              } catch (Exception e) {
                  e.printStackTrace();
              }
          }
      }*/

    @Override
    public void onStart() {
        super.onStart();

        //spiceManagerOffliine.start(this);

        if (spicePrintPaymentTicket.isRequestPending()) {
            Util.Log.ih("::::::::spicePrintPaymentTicket.isRequestPending()");
            spicePrintPaymentTicket.executePendingRequest();
        } else if (spicePrintVisitTicket.isRequestPending()) {
            Util.Log.ih("spicePrintVisitTicket.isRequestPending()");
            spicePrintVisitTicket.executePendingRequest();
        }
    }

    @Override
    public void onStop() {
        //spiceManagerOffliine.shouldStop();

        super.onStop();

          /*
          if (!isFinishing()) {
              if (asyncSaldo != null && asyncSaldo.pd != null) {
                  asyncSaldo.pd.dismiss();
                  asyncSaldo.pd = null;
              }
          }
          */

        if (!pendingCobro) {
            mthis = null;
            //gpsProviderClienteDetalle = null;
            //finish();
            //getFragmentManager().popBackStack();
        }

        // Apphance.onStop(this);
    }

      /*@Override
      public boolean onKeyDown(int keyCode, KeyEvent event) {
          //when true
          //a ticket is being printed
          if (goBack) {
              if (keyCode == KeyEvent.KEYCODE_BACK) {
                  mthis = null;
              }
              return super.onKeyDown(keyCode, event);
          }
          return false;
      }*/

    /**
     * Gets info of given company (company related to client's account, set as id)
     * as well as gets new folio plus one, so that it will be new folio for the
     * new payment that is ongoing
     *
     * @param nempresa company of client's account
     *                 01, 03, 04 or 05
     */
    void setEmpresa(String nempresa) {
        SharedPreferences preference = getContext().getSharedPreferences("folios", Context.MODE_PRIVATE);
        //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
        //try {
        Companies company = DatabaseAssistant.getSingleCompany(nempresa);
        //JSONObject empresa = query.mostrarEmpresa(nempresa);
        Log.e("empresa", company.toString());
        empresaString = company.getName();
        rfc = company.getRfc();
        setRazon(company.getBusinessName());
        setDireccion(company.getAddress());
        setTel(company.getPhone());
        //} catch (JSONException e) {
        //  e.printStackTrace();
        //}

        try {
            ult_folio = preference.getString(company.getName(), "0");
        } catch (Exception e) {
            e.printStackTrace();
            mthis = null;
            mainActivity.toastL("Hubo un error. Por favor intentalo de nuevo");
            //finish();

            FragmentManager fm = getFragmentManager();
            fm = null;

            if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                fm.popBackStack();
                //Toast.makeText(mainActivity, "Entro a if", Toast.LENGTH_SHORT).show();
            } else if (fm == null && isAdded()) {
                fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                //Toast.makeText(mainActivity, "Entro a else es NULLO", Toast.LENGTH_SHORT).show();
                fm.popBackStack();
            }


            //getFragmentManager().popBackStack();
        }
        try {
            new_folio = "" + (Integer.parseInt(ult_folio) + 1);

            //TODO: UNCOMMENT AND MODIFY THIS WHEN THERE ARE PAYMENTS WITH UNWANTED FOLIOS -------
              /*
              if ( new_folio.equals("26985") ) {
                  new_folio = "" + 27093;
              }
              */
            //TODO: ------------------------------------------------------------------------------
        } catch (NumberFormatException e) {
            e.printStackTrace();
            mthis = null;
            mainActivity.toastL("Hubo un error. Por favor intentalo de nuevo");
            //finish();

            FragmentManager fm = getFragmentManager();
            fm = null;

            if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                fm.popBackStack();
                //Toast.makeText(mainActivity, "Entro a if", Toast.LENGTH_SHORT).show();
            } else if (fm == null && isAdded()) {
                fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                //Toast.makeText(mainActivity, "Entro a else es NULLO", Toast.LENGTH_SHORT).show();
                fm.popBackStack();
            }

            //getFragmentManager().popBackStack();
        }

        //query = null;

    }

    /**
     * sets company's tel info
     */
    public void setTel(String tel) {
        //Log.e("tel", tel);
        datoempresa5 = "INFORMES Y SERVICIOS";
        //if (tel.length() <= 12) {
        datoempresa6 = "TELS. " + tel;
        datoempresa7 = "";
              /*
          } else {
              for (int x = 7; x <= 15; x++) {
                  if (tel.charAt(x) == ' ' || x == 15) {
                      datoempresa6 = "TELS. "
                              + tel.substring(0, x + 1);
                      datoempresa7 = tel.substring(x + 1);
                      x = 15;
                  }
              }

          }
          */
    }

    /**
     * not in use
     */
    public String getFirstPhone(String phone) {
        for (int i = 7; i <= 15; i++) {
            if (phone.charAt(i) == ' ') {
                return phone.substring(0, i + 1);
                //phone.substring(x + 1);
                //x = 15;
            }
        }
        return "";
    }

    /**
     * not in use
     */
    public String getSecondPhone(String phone) {
        for (int i = 7; i <= 15; i++) {
            if (phone.charAt(i) == ' ') {
                //phone.substring(0, i + 1);
                return phone.substring(i + 1);
                //x = 15;
            }
        }
        return "";
    }

    /**
     * sets company's 'razon' info
     */
    public void setRazon(String razon) {
        if (razon.length() > 34) {
            for (int x = 30; x > 0; x--) {
                if (razon.charAt(x) == ' ') {
                    datoempresa1 = razon.substring(0, x + 1);
                    datoempresa2 = razon.substring(x + 1);
                    x = 0;
                }
            }

        } else {
            datoempresa1 = razon;
            datoempresa2 = "";
        }
    }

    //377_address_dependency

    /**
     * sets company's address info
     *
     * @param direccion company's address
     */
    public void setDireccion(String direccion) {
        datoempresa3 = direccion;
        this.direccion = new String[7];
        switch (com.jaguarlabs.pabs.util.BuildConfig.getTargetBranch()) {
            case GUADALAJARA:
                if (empresa.equals("05")) {
                    this.direccion[0] = "WASHINGTON No. 404";
                    this.direccion[1] = "COL. LA AURORA";
                    this.direccion[2] = "C.P.44460, GUADALAJARA,";
                    this.direccion[3] = "Jalisco, México";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                } else {
                    this.direccion[0] = "Av. Federalismo Norte 1485";
                    this.direccion[1] = "Piso 1 Oficina 3 Colonia ";
                    this.direccion[2] = "San Miguel de Mezquitan";
                    this.direccion[3] = "CP 44260   Guadalajara";
                    this.direccion[4] = "Jalisco, México";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;
            case TAMPICO:
                if (empresa.equals("03")) {
                    this.direccion[0] = "AV. HIDALGO NO.3402, ENTRE";
                    this.direccion[1] = "CALLE VIOLETA Y CALLE ROSA,";
                    this.direccion[2] = "COLONIA FLORES TAMPICO,";
                    this.direccion[3] = "TAMAULIPAS, MÉXICO";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                } else {
                    this.direccion[0] = "AV. HIDALGO NO.3402, ENTRE";
                    this.direccion[1] = "CALLE VIOLETA Y CALLE ROSA,";
                    this.direccion[2] = "COLONIA FLORES TAMPICO,";
                    this.direccion[3] = "TAMAULIPAS, MÉXICO";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;
                    /*
                    Av. Hidalgo No. 3402 , entre la calle Violeta y la calle Rosa en la Colonia Flores Tampico , Tamaulipas.
                    En el edificio Planta alta .
                    Entrada por Av. Hidalgo.
                    */

            case TOLUCA:
                if (empresa.equals("03")) {
                    this.direccion[0] = "PASEO TOLLOCAN 101";
                    this.direccion[1] = "COL. SANTA MARIA DE LAS ROSAS";
                    this.direccion[2] = "CP. 50140, TOLUCA,";
                    this.direccion[3] = "ESTADO DE MÉXICO, MÉXICO";
                    this.direccion[4] = "";
                } else /*empresa 01*/ {
                    this.direccion[0] = "PASEO TOLLOCAN 101";
                    this.direccion[1] = "SANTA MARIA DE LAS ROSAS";
                    this.direccion[2] = "CP. 50140, TOLUCA,";
                    this.direccion[3] = "ESTADO DE MÉXICO, MÉXICO";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;
            case QUERETARO:
                if (empresa.equals("03")) {
                    this.direccion[0] = "2DA PRIVADA DE 20 DE NOVIEMBRE";
                    this.direccion[1] = "No. 15 2DO PISO; COL. SAN ";
                    this.direccion[2] = "FRANCISQUITO; C.P. 76058,";
                    this.direccion[3] = "QUERÉTARO, QUERÉTARO, MÉXICO";
                    this.direccion[4] = "";
                } else /*empresa 01*/ {
                    this.direccion[0] = "2DA PRIVADA DE 20 DE NOVIEMBRE";
                    this.direccion[1] = "No. 15 2DO PISO; COL. SAN ";
                    this.direccion[2] = "FRANCISQUITO; C.P. 76058, ";
                    this.direccion[3] = "QUERÉTARO, QUERÉTARO, MÉXICO";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;


            case IRAPUATO:
                if (empresa.equals("03")) {
                    this.direccion[0] = "BLVD. DIAZ ORDAZ # 1370; M2; ";
                    this.direccion[1] = "INTERIOR 2, C.P. 36660, ";
                    this.direccion[2] = "JARDINES DE IRAPUATO,";
                    this.direccion[3] = "IRAPUATO GUANAJUATO, MÉXICO";
                    this.direccion[4] = "";
                } else /*empresa 01*/ {
                    this.direccion[0] = "BLVD. DIAZ ORDAZ # 1370; M2; ";
                    this.direccion[1] = "INTERIOR 2, C.P. 36660, ";
                    this.direccion[2] = "JARDINES DE IRAPUATO,";
                    this.direccion[3] = "IRAPUATO GUANAJUATO, MÉXICO";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;


            case LEON:
                if (empresa.equals("03")) {
                    this.direccion[0] = "NICARAGUA #917, LOCAL 12, ";
                    this.direccion[1] = "2DO NIVEL, C.P. 37366, ";
                    this.direccion[2] = "VILLA RESIDENCIAL ARBIDE, ";
                    this.direccion[3] = "LEON, GUANAJUATO, MÉXICO";
                    this.direccion[4] = "";
                } else /*empresa 01*/ {
                    this.direccion[0] = "NICARAGUA #917, LOCAL 12, ";
                    this.direccion[1] = "2DO NIVEL, C.P. 37366, ";
                    this.direccion[2] = "VILLA RESIDENCIAL ARBIDE, ";
                    this.direccion[3] = "LEON, GUANAJUATO, MÉXICO";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;

            case SALAMANCA:
                if (empresa.equals("03")) {
                    this.direccion[0] = "JUAN ALDAMA #1008; 2DO PISO; ";
                    this.direccion[1] = "CENTRO, C.P. 36860, ";
                    this.direccion[2] = "SALAMANCA, GUANAJUATO, MÉXICO";
                    this.direccion[3] = "";
                    this.direccion[4] = "";
                } else /*empresa 01*/ {
                    this.direccion[0] = "JUAN ALDAMA #1008; 2DO PISO; ";
                    this.direccion[1] = "CENTRO, C.P. 36860, ";
                    this.direccion[2] = "SALAMANCA, GUANAJUATO, MÉXICO";
                    this.direccion[3] = "";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;

             /*
                DatabaseAssistant.insertCompany(
						"03",
						"",
						"Cooperativa",
						"GRUPO ASISTENCIAL MEMORY SCP de RL de C.V.",
						"PROLONGACIÓN HIDALGO #705-A, C.P. 38040, CENTRO, CELAYA, GUANAJUATO, MÉXICO",
						"4616158702 - 4616158703");
                 */
            case CELAYA:
                if (empresa.equals("03")) {
                    this.direccion[0] = "PROLONGACIÓN HIDALGO #705-A, ";
                    this.direccion[1] = "CENTRO, C.P. 38040, ";
                    this.direccion[2] = "CELAYA, GUANAJUATO, MÉXICO";
                    this.direccion[3] = "";
                    this.direccion[4] = "";
                } else /*empresa 01*/ {
                    this.direccion[0] = "PROLONGACIÓN HIDALGO #705-A, ";
                    this.direccion[1] = "CENTRO, C.P. 38040, ";
                    this.direccion[2] = "CELAYA, GUANAJUATO, MÉXICO";
                    this.direccion[3] = "";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;


            case PUEBLA:
                if (empresa.equals("03")) {
                    this.direccion[0] = "FRANCISCO I. MADERO No. 428";
                    this.direccion[1] = "COL. SAN BALTAZAR CAMPECHE";
                    this.direccion[2] = "C.P. 72550, PUEBLA";
                    this.direccion[3] = "PUEBLA, MÉXICO";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                } else {
                    this.direccion[0] = "FRANCISCO I. MADERO No. 428";
                    this.direccion[1] = "COL. SAN BALTAZAR CAMPECHE";
                    this.direccion[2] = "C.P. 72550, PUEBLA";
                    this.direccion[3] = "PUEBLA, MÉXICO";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;
            case CUERNAVACA:
                if (empresa.equals("03")) {
                    this.direccion[0] = "FRANCISCO I. MADERO No. 719";
                    this.direccion[1] = "COL. MIRAVAL C.P. 62270.";
                    this.direccion[2] = "DELEGACIÓN BENITO JUÁREZ,";
                    this.direccion[3] = "CUERNAVACA, MÉXICO";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                } else {
                    this.direccion[0] = "FRANCISCO I. MADERO No. 719";
                    this.direccion[1] = "COL. MIRAVAL C.P. 62270.";
                    this.direccion[2] = "DELEGACIÓN BENITO JUÁREZ,";
                    this.direccion[3] = "CUERNAVACA, MÉXICO";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;
            case NAYARIT:
                if (empresa.equals("04")) {
                    this.direccion[0] = "AV. MEXICO N° 393 NTE.";
                    this.direccion[1] = "CALLE NIÑOS HÉROES Nº 82,";
                    this.direccion[2] = "JAVIER MINA Nº 69 ORIENTE";
                    this.direccion[3] = "COL. CENTRO";
                    this.direccion[4] = "CP. 63000, TEPIC,";
                    this.direccion[5] = "NAYARIT, MEXICO";
                    this.direccion[6] = "TELS: 212 1080 Y 216 9102";
                } else {
                    this.direccion[0] = "AV. MEXICO N° 393 NTE.";
                    this.direccion[1] = "COL. CENTRO";
                    this.direccion[2] = "CP. 63000, TEPIC,";
                    this.direccion[3] = "NAYARIT, MEXICO";
                    this.direccion[4] = "TELS 212 1080 Y 216 9102";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;
            case MERIDA:
                if (empresa.equals("01")) {
                    this.direccion[0] = "Calle 33 No. 503C";
                    this.direccion[1] = "Col. Alcala Martin";
                    this.direccion[2] = "CP 97000 Merida";
                    this.direccion[3] = "Yucatan, México";
                    this.direccion[4] = "TELS: 9201269 y 9201268";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                } else {
                    this.direccion[0] = "AV. MEXICO N° 393 NTE.";
                    this.direccion[1] = "COL. CENTRO";
                    this.direccion[2] = "CP. 63000, TEPIC,";
                    this.direccion[3] = "NAYARIT, MEXICO";
                    this.direccion[4] = "TELS 212 1080 Y 216 9102";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;
            case SALTILLO:
                if (empresa.equals("01")) {
                    this.direccion[0] = "BOULEVARD NAZARIO ORTIZ GARZA";
                    this.direccion[1] = "NO 1265 COL. ALPES";
                    this.direccion[2] = "CP 25270";
                    this.direccion[3] = "SALTILLO COAHUILA";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                } else {
                    this.direccion[0] = "BOULEVARD NAZARIO ORTIZ GARZA";
                    this.direccion[1] = "NO 1265 COL. ALPES";
                    this.direccion[2] = "CP 25270";
                    this.direccion[3] = "SALTILLO COAHUILA";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;
            case CANCUN:
                if (empresa.equals("01")) {
                    this.direccion[0] = "CALLE CEDRO MZ. 41";
                    this.direccion[1] = "LOTE 1-08";
                    this.direccion[2] = "SM. 311";
                    this.direccion[3] = "CANCÚN, QUINTANA ROO";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                } else {
                    this.direccion[0] = "CALLE CEDRO MZ. 41";
                    this.direccion[1] = "LOTE 1-08";
                    this.direccion[2] = "SM. 311";
                    this.direccion[3] = "CANCÚN, QUINTANA ROO";
                    this.direccion[4] = "";
                    this.direccion[5] = "";
                    this.direccion[6] = "";
                }
                break;
            case MEXICALI:
                this.direccion[0] = "Blvd Anahuac #800 Int.A";
                this.direccion[1] = "Col. Esperanza C.P. 21140";
                this.direccion[2] = "Mexicali, B.C., MÉXICO";
                this.direccion[3] = "";
                this.direccion[4] = "";
                this.direccion[5] = "";
                this.direccion[6] = "";
                break;


            case TORREON:
                this.direccion[0] = "CLZ. MATIAS ROMAN RIOS # 425,";
                this.direccion[1] = "L-17, C.P 27000, PLAZA PABELLON";
                this.direccion[2] = "HIDALGO, TORREON COAHUILA, MEXICO";
                this.direccion[3] = "";
                this.direccion[4] = "";
                this.direccion[5] = "";
                this.direccion[6] = "";
                break;


            case MORELIA:
                this.direccion[0] = "AV. GUADALUPE VICTORIA";
                this.direccion[1] = "NO. 364 COL. CENTRO";
                this.direccion[2] = "C.P. 58000 MORELIA, MICHOACAN";
                this.direccion[3] = "";
                this.direccion[4] = "";
                this.direccion[5] = "";
                this.direccion[6] = "";
                break;
        }
    }

    TSCActivity TscDll;

    /**
     * prints address on the ticket
     * this address is divided so that it fits on the ticket
     *
     * @param line1 first line
     * @param line2 second line
     * @param line3 third line
     */
    private void printDireccion(String line1, String line2, String line3) {
        TscDll.printerfont(10, 160, "2", 0, 1, 1, line1);
        TscDll.printerfont(10, 205, "2", 0, 1, 1, line2);
        TscDll.printerfont(10, 250, "2", 0, 1, 1, line3);
    }

    /**
     * prints address on the ticket
     * this address is divided so that it fits on the ticket
     *
     * @param line1 first line
     * @param line2 second line
     * @param line3 third line
     * @param line4 fourth line
     * @param line5 fifth line
     */
    private void printDireccion(String line1, String line2, String line3, String line4, String line5) {
        TscDll.printerfont(10, 205, "2", 0, 1, 1, line1);
        TscDll.printerfont(10, 250, "2", 0, 1, 1, line2);
        TscDll.printerfont(10, 295, "2", 0, 1, 1, line3);
        TscDll.printerfont(10, 340, "2", 0, 1, 1, line4);
        TscDll.printerfont(10, 385, "2", 0, 1, 1, line5);
    }

    /**
     * checks if more than 59 minutes have passed
     *
     * @return true -> difference is more than limit
     * false -> difference is under limit
     */
    public boolean checkDate() {
        long newTime = new Date().getTime();
        long differences = Math.abs(dateAux - newTime);
        return (differences > (1000 * 59));
    }

    /**
     * disables bluetooth
     */
    public void disableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
                .getDefaultAdapter();
        mBluetoothAdapter.disable();
    }

    /**
     * enables bluetooth
     */
    public void enableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Util.Log.ih("enabling bluetooth");
            mBluetoothAdapter.enable();
        }
    }

    /**
     * writes visit into '.txt' file
     * writes visit into database
     * and finishes activity returning to main activity (Clientes)
     */
    @SuppressLint("SimpleDateFormat")
    public void procesarNoAporto() {

        if (ApplicationResourcesProvider.getLocationProvider().getGPSPosition() != null) { // si tenemos datos GPS

            mainActivity.log("lat: " + ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLatitude(),
                    "long: " + ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLongitude());

            double auxLatitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLatitude();
            double auxLongitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLongitude();

            latitud = ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLatitude();
            longitud = ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLongitude();

            if ((latitud == auxLatitud) && (longitud == auxLongitud)) {
                latitud = auxLatitud + 0.0001;
                longitud = auxLongitud + 0.0001;
                ApplicationResourcesProvider.setLastKnownLocation(latitud, longitud);
            }
        }
        else // si el metodo nos retorna nullos
        {
            double auxLatitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLatitude();
            double auxLongitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLongitude();

            if ((latitud != 0) && (longitud != 0))
            {
                latitud = auxLatitud;
                longitud = auxLongitud;
                ApplicationResourcesProvider.setLastKnownLocation(latitud, longitud);
            }
        }

        String id_cobrador = "";
        String id_cliente = "";
        String serieCliente = "";
        try {
            id_cobrador = datosCobrador.getString("no_cobrador");
            id_cliente = jsonDatos.getString("no_contrato");
            serieCliente = jsonDatos.getString("serie");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String seriecobrador = "=(";
        try {
            if (empresa.equals("01")) {
                seriecobrador = datosCobrador
                        .getString("serie_programa");
            } else {
                seriecobrador = datosCobrador.getString(
                        DatabaseAssistant.getSingleCompany(empresa).getName());
            }

            if (seriecobrador.equals("3M") && new_folio.equals("71935")) {
                new_folio = "72155";
            } else if (seriecobrador.equals("3M,") && new_folio.equals("24280")) {
                new_folio = "24410";
            } else if (seriecobrador.equals("V1") && new_folio.equals("59211")) {
                new_folio = "59291";
            } else if (seriecobrador.equals("6C") && new_folio.equals("63063")) {
                new_folio = "63071";
            } else if (seriecobrador.equals("1A1") && new_folio.equals("96874")) {
                new_folio = "96896";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            seriecobrador = "=D";
        }

        //Obtener el ultimo periodo desde la base de datos
        int periodo = mainActivity.getEfectivoPreference().getInt("periodo", 0);
        List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();
        //int periodo = periods.get(periods.size() - 1).getPeriodNumber();

        //register payment backup
        try {

            //folio, contrato, monto, empresa, periodo, tipo de cobro
            if (!pendingTicket) //No hay ticket pendiente
            {
                LogRegister.paymentRegistrationBackup(
                        seriecobrador + new_folio,
                        jsonDatos.getString("serie") + no_contrato,
                        "----",
                        jsonDatos.getString("id_grupo_base"),
                        "" + periodo,
                        "Dentro de Cartera",
                        "" + String.valueOf(jsonDatos.getDouble("saldo") - 0)

                );
            } else {

                Log.i("IMPRESION", "TIENES un TICKET PENDOENTE, no podemos guardar el Log Register de nuevo.");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        //end register payment backup

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");
            Date date = new Date();

            //toggleGPSUpdates();

            DatabaseAssistant.insertPayment(
                    jsonDatos.getString("id_grupo_base"), // empresa
                    "" + latitud, // latitud
                    "" + longitud, // longitud
                    "" + typeStatus, // status
                    "" + id_cobrador, // id_cobrador
                    "" + 0, // monto
                    "" + id_cliente, // id_cliente
                    dateFormat.format(date), // fecha
                    serieCliente, // serie
                    "----",// folio
                    seriecobrador, // serie_cobro
                    jsonDatos.getString("tipo_bd"), //tipo bd
                    periodo,
                    paymentReferenceString,
                    "" + DatabaseAssistant.setSaldoPorCobro(jsonDatos.getDouble("saldo"), 0),
                    "Dentro de cartera"
            );

            if (!DatabaseAssistant.getSingleVisit(dateFormat.format(date))) {
                Util.Log.ih("payment not found");
                Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_call, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return;
            } else {
                Util.Log.ih("payment found");
            }

            if (BuildConfig.isWalletSynchronizationEnabled()) {
                Runnable runnable = new Runnable() {

                    String contractID;

                    @Override
                    public void run() {
                        DatabaseAssistant.updateVisitMade(contractID);
                    }

                    public Runnable setData(String contractID) {
                        this.contractID = contractID;
                        return this;
                    }

                }.setData(jsonDatos.getString("id_contrato"));


                Thread thread = new Thread(runnable);
                thread.start();
            }
            pendingCobro = false;
            Intent intent = new Intent();
            intent.putExtra("paymentDone", false);
            if (commentAdded) {
                intent.putExtra("id_contract", jsonDatos.getString("id_contrato"));
                intent.putExtra("comments", comment);
            }
            //setResult(RESULT_OK, intent);
            fragmentResult.onFragmentResult(requestCode, Activity.RESULT_OK, intent);
            mthis = null;
            FragmentManager fm = getFragmentManager();
            fm = null;

            if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                fm.popBackStack();
                //Toast.makeText(mainActivity, "Entro a if", Toast.LENGTH_SHORT).show();
            } else if (fm == null && isAdded()) {
                fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                //Toast.makeText(mainActivity, "Entro a else es NULLO", Toast.LENGTH_SHORT).show();
                fm.popBackStack();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        //}
        //});

    }

    @Override
    public void onResume() {
        super.onResume();
        mthis = this;

        MainActivity.CURRENT_TAG = MainActivity.TAG_CLIENTE_DETALLES;

        if (pendingCobro) {
            if (!mainActivity.isFinishing()) {
                if (newsecondDialog != null && newsecondDialog.isShowing()) {
                    Log.e("si entro", "hola");
                    newsecondDialog.dismiss();
                    newsecondDialog.show();
                }
            }
        }

    }

    @Override
    public void onPause() {
        super.onPause();

        if (MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();

            if (newsecondDialog != null && newsecondDialog.isShowing())
                newsecondDialog.dismiss();

            if (secondDialog != null && secondDialog.isShowing())
                secondDialog.dismiss();

            if (commentsDialog != null && commentsDialog.isShowing())
                commentsDialog.dismiss();

            if (myDialog != null && myDialog.isShowing())
                myDialog.dismiss();

            if (myDialog2 != null && myDialog2.isShowing())
                myDialog2.dismiss();

            if (myDialog3 != null && myDialog3.isShowing())
                myDialog3.dismiss();

            //if (dialogo_suspencion != null && dialogo_suspencion.isShowing())
                //dialogo_suspencion.dismiss();

            if (dialogo_cobros_realizados != null && dialogo_cobros_realizados.isShowing())
                dialogo_cobros_realizados.dismiss();

            FragmentManager fm = getFragmentManager();
            fm = null;

            if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                fm.popBackStack();
                //Toast.makeText(mainActivity, "Entro a if", Toast.LENGTH_SHORT).show();
            } else if (fm == null && isAdded()) {
                fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                //Toast.makeText(mainActivity, "Entro a else es NULLO", Toast.LENGTH_SHORT).show();
                fm.popBackStack();
            }
            //getFragmentManager().popBackStack();
        }
    }

    /**
     * fills fields with the result of the coincidence (client's info)
     */
    private void llenarCampos() {
        try {

            nombre = jsonDatos.getString("nombre");
            apellido = jsonDatos.getString("apellido_pat") + " " + jsonDatos.getString("apellido_mat");
            serie = jsonDatos.getString("serie");
            no_contrato = jsonDatos.getString("no_contrato");

            telefono = jsonDatos.getString("phone");//*************************************************************************************
            setEmpresa("01");

            ((TextView) getView().findViewById(R.id.tv_aportacion)).setText("$" + mainActivity.formatMoney(Double.parseDouble(jsonDatos.getString("monto_pago_actual"))));
            ((TextView) getView().findViewById(R.id.textViewNombre)).setText(jsonDatos.getString("nombre") + " " + jsonDatos.getString("apellido_pat") + " " + jsonDatos.getString("apellido_mat"));
            ((TextView) getView().findViewById(R.id.tv_num_contrato)).setText(jsonDatos.getString("serie") + jsonDatos.getString("no_contrato"));
            ((TextView) getView().findViewById(R.id.tvTelefono)).setText(jsonDatos.getString("phone"));

            ((TextView) getView().findViewById(R.id.tvFechaReactiva)).setText(jsonDatos.getString("fechareactiva"));

            /**
             * Checks if it is the same contract number
             * and
             * checks if payment was made and printed (is synchronized)
             * receives false: is not synchronized
             * receives true: is synchronized
             */
            if (!preferences.isThereAPaymentAndIsSynchronized()) {

                /**
                 * Checks if payment was made and printed (is synchronized)
                 * receives false: is not synchronized
                 * receives true: is synchronized
                 */
                //if ( !preferences.isSynchronized() ) {

                /**
                 * If it's not synchronized
                 * use folio from last payment
                 */

                /**
                 * Gets last payment registered
                 */
                JSONObject lastPayment = preferences.getPayment();

                try {

                    /**
                     * and checks if same contract number was chosen
                     */
                    if (lastPayment.getString(getString(R.string.key_contract_number)).equals(jsonDatos.getString("serie") + jsonDatos.getString("no_contrato"))) {

                        try {

                            /**
                             * Gets amount from last payment registered
                             * and disables EditText
                             */
                            //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
                            etAmount.setText(lastPayment.getString(getString(R.string.key_amount)));
                            etAmount.setEnabled(false);

                            pendingTicket = true;

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //}
            }

            try {
                if (jsonDatos.getString("fecha_ultimo_abono").equals("null") || jsonDatos.getString("fecha_ultimo_abono").equals("0000-00-00")) {
                    ((TextView) getView().findViewById(R.id.textViewUltimoAbono))
                            .setText("No disponible");

                } else {
                    ((TextView) getView().findViewById(R.id.textViewUltimoAbono))
                            .setText(jsonDatos.getString("fecha_ultimo_abono"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                ((TextView) getView().findViewById(R.id.textViewUltimoAbono))
                        .setText("No disponible");

            }
            String calle = "";
            try {
                calle = jsonDatos.getString("calle_cobro");
            } catch (Exception e) {
                e.printStackTrace();
            }

            String noExt = "";
            try {
                noExt = jsonDatos.getString("no_ext_cobro");
            } catch (Exception e) {
                e.printStackTrace();
            }

            String colonia = "";

            try {
                colonia = jsonDatos.getString("colonia");
            } catch (Exception e) {
                e.printStackTrace();
            }

            String interior = "";
            try {
                interior = jsonDatos.getString("numerointerior");
            } catch (Exception e) {
                e.printStackTrace();
            }

            String entreCalles = "";
            try {
                entreCalles = jsonDatos.getString("entre_calles");
            } catch (Exception e) {
                e.printStackTrace();
            }
            String saldoAtrasado = "";
            try {
                if (jsonDatos.has("saldo_atrasado")) {
                    if (!jsonDatos.getString("saldo_atrasado").equals("null")) {
                        saldoAtrasado = "$" + jsonDatos.getString("saldo_atrasado");

                    } else {
                        saldoAtrasado = "No disponible";

                    }
                } else {
                    saldoAtrasado = "No disponible";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ((TextView) getView().findViewById(R.id.TextViewAtrasado))
                    .setText(saldoAtrasado);

            String localidad = "";

            localidad = jsonDatos.getString("localidad");


            //jsonDatos.getString("no_contrato")
            String sintax = "SELECT * FROM CLIENTS WHERE id_Contract = '" + idContrato + "'";
            List<Clients> listClients = Clients.findWithQuery(Clients.class, sintax);
            if (listClients.size() > 0) {
                interior = listClients.get(0).getNumerointerior();
            }


            String domicilio = calle + " " + noExt + ", Colonia: " + colonia + ", Interior: " + interior + ", Entre calles: " + entreCalles + ", Localidad: " + localidad;
            ((TextView) getView().findViewById(R.id.tv_domicilio_cob)).setText(domicilio);
            //SqlQueryAssistant query = new SqlQueryAssistant(
            //       ApplicationResourcesProvider.getContext());
            String empr = DatabaseAssistant.getSingleCompany(jsonDatos.getString("id_grupo_base")).getName();
            //empr = query.mostrarEmpresa(jsonDatos.getString("id_grupo_base"))
            //        .getString(Empresa.nombre_empresa);

            //query = null;

            empr = ExtendedActivity.getCompanyNameFormatted(empr, false);

            ((TextView) getView().findViewById(R.id.tv_empresa)).setText(empr);

            int tipoPago = jsonDatos.getInt("forma_pago_actual");
            String formaPago = "";
            if (tipoPago == 1) {
                formaPago = "Semanal";
            } else if (tipoPago == 2) {
                formaPago = "Quincenal";
            } else if (tipoPago == 3) {
                formaPago = "Mensual";
            }
            ((TextView) getView().findViewById(R.id.tv_tipo_aportacion))
                    .setText(formaPago);

            String estatus = jsonDatos.getString("contrato_estatus");
            if (estatus.equals("")) {
                int tipoEstatus = jsonDatos.getInt("estatus");
                if (tipoEstatus == 1) {
                    estatus = "Activo";
                } else if (tipoEstatus == 5) {
                    estatus = "Suspendido temporal";
                } else if (tipoEstatus == 6) {
                    estatus = "Suspendido por cancelar";
                } else if (tipoEstatus == 15) {
                    estatus = "Verificacion Temp";
                } else if (tipoEstatus == 16) {
                    estatus = "Verificacion SC";
                }
            }

            ((TextView) getView().findViewById(R.id.tv_estatus)).setText(estatus);
            ((TextView) getView().findViewById(R.id.tv_saldo_pendiente)).setText("$" + mainActivity.formatMoney(Double.parseDouble(jsonDatos.getString("saldo"))));


            if (jsonDatos.has("email")) {
                ((TextView) getView().findViewById(R.id.tvCorreo)).setText(jsonDatos.getString("email"));
            } else {
                String queryz = "SELECT * FROM ACTUALIZADOS WHERE numerocontrato ='" + idContrato + "' order by id desc limit 1";
                List<Actualizados> listaActualizadoss = Actualizados.findWithQuery(Actualizados.class, queryz);
                if (listaActualizadoss.size() > 0) {
                    //((TextView) getView().findViewById(R.id.tvCorreo)).setText(listaActualizadoss.get(0).getCorreo());
                    ((TextView) getView().findViewById(R.id.tvCorreo)).setText(DatabaseAssistant.getLastEmail());
                }
            }

            if (jsonDatos.has("n_dias")) {
                nDias = jsonDatos.getString("n_dias");
            } else
                nDias = "10";


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }

    /**
     * prints account status
     *
     * @return
     */
    public boolean ServicioImpresionEstadoDeCuenta() {
        try {
            Looper.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //TSCActivity TscDll = new TSCActivity();
        //SqlQueryAssistant query = new SqlQueryAssistant(this);
        //String mac = query.getMacAddress();
        String mac = DatabaseAssistant.getPrinterMacAddress();
        //query = null;
        boolean blue = true;
        int intentos = 0;
        while (blue && intentos < 100) {

            TSCActivity TscDll = new TSCActivity();

            try {

                dateAux = new Date().getTime();

                //timer that closes printer port if got stuck
                //trying to print
                //has one minute of limit before closing port
                //closing port causes NullPointerException on openPort()
                //so it tries again to print
                timer = new Timer();
                timer.schedule(new TimerTask() {

                    String mac;
                    int attempt;
                    TSCActivity tscDll;

                    @Override
                    public void run() {
                        if (checkDate()) {
                            Util.Log.ih("Closing port");
                            //disableBluetooth();
                            try {
                                tscDll.closeport();
                            } catch (Exception e) {
                                e.printStackTrace();
                                timer.cancel();
                            }
                        }
                        Util.Log.ih("Hora, attempt: " + attempt);
                    }

                    public TimerTask setData(int attempt, TSCActivity tscDll) {
                        this.attempt = attempt;
                        this.tscDll = tscDll;
                        return this;
                    }
                }.setData(intentos, TscDll), (1000 * 60));

                TscDll.openport(mac);
                Util.Log.ih("despues de openPort");
                timer.cancel();

                //Log.i("PRINT STATUS", TscDll.status());
                TscDll.setup(Printer.width,
                        50,
                        Printer.speed,
                        Printer.density,
                        Printer.sensorType,
                        Printer.gapBlackMarkVerticalDistance,
                        Printer.gapBlackMarkShiftDistance);

                //Log.i("PRINT STATUS", TscDll.status());

                TscDll.clearbuffer();
                TscDll.sendcommand("GAP 0,0\n");
                TscDll.sendcommand("CLS\n");
                TscDll.sendcommand("CODEPAGE UTF-8\n");
                TscDll.sendcommand("SET TEAR ON\n");
                TscDll.sendcommand("SET COUNTER @1 1\n");

                for (int conter = 0; conter < 600; conter = conter + 30) {
                    TscDll.printerfont(conter, 10, "3", 0, 1, 1, "-");
                }
                TscDll.printerfont(10, 55, "3", 0, 1, 1, serie
                        + jsonDatos.getString("no_contrato"));
                String nombre = jsonDatos.getString("nombre") + " "
                        + jsonDatos.getString("apellido_pat") + " "
                        + jsonDatos.getString("apellido_mat");
                TscDll.printerfont(10, 100, "3", 0, 1, 1, nombre);
                final SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "dd/MM/yyyy");
                Date date = new Date();
                Log.i("PRINT STATUS", TscDll.status());

                TscDll.printerfont(10, 145, "3", 0, 1, 1,
                        "El saldo de su contrato al día ");
                TscDll.printerfont(10, 190, "3", 0, 1, 1,
                        dateFormat.format(date) + " a las 6 am, es de $"
                                + jsonDatos.getString("saldo"));
                TscDll.printerfont(180, 240, "3", 0, 1, 1, "Atentamente");
                String nombre_cobrador = datosCobrador.getString("nombre");
                TscDll.printerfont(50, 285, "2", 0, 1, 1, nombre_cobrador);

                Log.i("PRINT STATUS", TscDll.status());
                TscDll.printlabel(1, 1);

                //TscDll.clearbuffer();

                Log.i("PRINT STATUS", TscDll.status());
                //Log.e("saldo imprimiendo",
                //        "aaaa" + jsonDatos.getString("saldo"));
                TscDll.closeport();

                return true;
            } catch (Exception e) {

                Util.Log.ih("intento = " + intentos);
                enableBluetooth();

                intentos++;
                e.printStackTrace();
            }
        }
        Toast.makeText(getContext(),
                getString(R.string.error_message_printing), Toast.LENGTH_SHORT)
                .show();
        return false;
    }

    /**
     * returns an instance of Dialog
     *
     * @return instance of Dialog
     */
    private Dialog getDialog() {
        Dialog mDialog = new Dialog(getContext());
        return mDialog;
    }

    /**
     * returns an instance of RazonesDialog
     *
     * @return instance of RazonesDialog
     */
    private RazonesDialog getRazonesDialog() {
        RazonesDialog mRazonesDialog = new RazonesDialog(getContext(), this);
        return mRazonesDialog;
    }

    /**
     * returns an instance of AlertDialogBuilder
     *
     * @return instance of AlertDialogBuilder
     */
    private AlertDialog.Builder getAlertDialogBuilder() {
        AlertDialog.Builder mAlertDialogBuilder = new AlertDialog.Builder(getContext());
        return mAlertDialogBuilder;
    }

    /**
     * returns an instance of ProgressDialog
     *
     * @return instance of ProgressDialog
     */
    private ProgressDialog getProgressDialog() {
        return new ProgressDialog(getContext());
    }

    /**
     * sets listener of resources on screen as
     * account status
     * back button on screen
     * 'no aporto' button
     * 'aporto' button
     */
    void setListeners() {

        mthis = this;
        imgActualizarDatos = (ImageView) getView().findViewById(R.id.imgActualizarDatos);
        //layoutGuardado=(LinearLayout) getView() .findViewById(R.id.layoutGuardado);
        imgActualizarDatos.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialogoCancelable)
                    mostrarDomicilio(v, true);
                else
                    mostrarDomicilio(v, false);

                dialogoCancelable = false;
            }
        });


        estado = (ImageView) getView().findViewById(R.id.imageViewEstado);
        estado.setOnClickListener(v ->
        {
            secondDialog = getDialog();
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "dd/MM/yyyy");
            Date date = new Date();

            secondDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            secondDialog.setContentView(R.layout.previewsaldo);
            secondDialog.setCancelable(true);
            secondDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(
                            Color.TRANSPARENT));
            secondDialog.show();

            TextView contrato = (TextView) secondDialog
                    .findViewById(R.id.textViewContrato);
            TextView titulor = (TextView) secondDialog
                    .findViewById(R.id.textViewTitulor);
            TextView fecha = (TextView) secondDialog
                    .findViewById(R.id.textViewFecha);
            TextView saldo = (TextView) secondDialog
                    .findViewById(R.id.textViewSaldo);
            TextView nombre = (TextView) secondDialog
                    .findViewById(R.id.textViewNombre);
            try {
                contrato.setText(serie
                        + jsonDatos.getString("no_contrato"));
                titulor.setText(jsonDatos.getString("nombre") + " "
                        + jsonDatos.getString("apellido_pat") + " "
                        + jsonDatos.getString("apellido_mat"));
                fecha.setText(dateFormat.format(date));
                saldo.setText(jsonDatos.getString("saldo"));
                nombre.setText(datosCobrador.getString("nombre"));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            imprimir = (TextView) secondDialog
                    .findViewById(R.id.textViewImprimir);
            imprimir.setOnClickListener(v1 ->
            {
                final LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                final boolean isGPSEnabled = locationManager
                        .isProviderEnabled(LocationManager.GPS_PROVIDER);
                if (isGPSEnabled) {
                    mthis = ClienteDetalle.this;
                    imprimir.setBackgroundColor(Color.WHITE);
                    //asyncSaldo = getAsyncPrintCaseSeven();
                    //asyncSaldo.execute();
                    if (!mainActivity.isFinishing()) {
                        if (secondDialog != null && secondDialog.isShowing()) {
                            Util.Log.ih("dismissing progress dialog on estado");
                            secondDialog.dismiss();
                            secondDialog = null;
                        }
                    }
                    executePrintAccountStatus();

                } else {
                    Toast.makeText(getContext(),
                            "Enciende el GPS para continuar.",
                            Toast.LENGTH_LONG).show();
                }
            });
                  /*
                  imprimir.setOnTouchListener(new OnTouchListener() {

                      @Override
                      public boolean onTouch(View arg0, MotionEvent arg1) {
                          if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
                              imprimir.setBackgroundColor(Color.BLUE);
                              return true;
                          }
                          if (arg1.getAction() == MotionEvent.ACTION_UP) {
                              final LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                              final boolean isGPSEnabled = locationManager
                                      .isProviderEnabled(LocationManager.GPS_PROVIDER);
                              if (isGPSEnabled) {
                                  mthis = ClienteDetalle.this;
                                  imprimir.setBackgroundColor(Color.WHITE);
                                  asyncSaldo = getAsyncPrintCaseSeven();
                                  asyncSaldo.execute();
                                  secondDialog.dismiss();
                                  return true;
                              } else {
                                  Toast.makeText(getApplicationContext(),
                                          "Enciende el GPS para continuar.",
                                          Toast.LENGTH_LONG).show();
                                  return false;
                              }

                          }
                          return false;
                      }

                  });
                  */
            tvCancelar = (TextView) secondDialog
                    .findViewById(R.id.textViewCancelar);
            tvCancelar.setOnClickListener(v1 -> {
                if (!mainActivity.isFinishing()) {
                    if (secondDialog != null && secondDialog.isShowing()) {
                        Util.Log.ih("dismissing progress dialog on estado");
                        secondDialog.dismiss();
                        secondDialog = null;
                    }
                }
            });
                  /*
                  tvCancelar.setOnTouchListener(new OnTouchListener() {

                      @Override
                      public boolean onTouch(View v, MotionEvent event) {
                          if (event.getAction() == MotionEvent.ACTION_DOWN) {
                              tvCancelar.setBackgroundColor(Color.RED);
                              return true;
                          }
                          if (event.getAction() == MotionEvent.ACTION_UP) {
                              tvCancelar.setBackgroundColor(Color.WHITE);
                              secondDialog.dismiss();
                              return true;
                          }
                          return false;
                      }

                  });
                  */
        });


        Button editClientInfo = (Button) getView().findViewById(R.id.editClientInformation);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            editClientInfo.setOutlineProvider(new ViewOutlineProvider() {
                @Override
                public void getOutline(View view, Outline outline) {
                    outline.setRoundRect(0, 0, view.getWidth(), view.getHeight() + 3, 10);
                }
            });
            editClientInfo.setClipToOutline(true);
            editClientInfo.setVisibility(View.GONE);
        }

        cellphoneNumber = (ImageView) getView().findViewById(R.id.imageViewCellphoneNumber);
        cellphoneNumber.setOnClickListener(v ->
        {
            /*secondDialog = getDialog();
            secondDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            secondDialog.setContentView(R.layout.dialog_telefono);
            secondDialog.setCancelable(true);
            secondDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(
                            Color.TRANSPARENT));
            secondDialog.show();

            EditText cellphoneNumberGiven = (EditText) secondDialog.findViewById(R.id.editTextCellPhone);

            saveCellPhoneNumber = (TextView) secondDialog
                    .findViewById(R.id.saveCellPhoneNumber);
            saveCellPhoneNumber.setOnClickListener(v1 -> {

                try {
                    DatabaseAssistant.saveClientCellPhoneNumber(
                            Long.parseLong(jsonDatos.getString("id_contrato")),
                            serie,
                            jsonDatos.getString("no_contrato"),
                            cellphoneNumberGiven.getText().toString()
                    );

                    Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.info_message_cellphone_saved, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    if (!mainActivity.isFinishing()) {
                        if (secondDialog != null && secondDialog.isShowing()) {
                            Util.Log.ih("dismissing progress dialog on estado");
                            secondDialog.dismiss();
                            secondDialog = null;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            cancelSavingCellphoneNumber = (TextView) secondDialog
                    .findViewById(R.id.cancelSavingCellPhoneNumber);
            cancelSavingCellphoneNumber.setOnClickListener(v1 -> {
                if (!mainActivity.isFinishing()) {
                    if (secondDialog != null && secondDialog.isShowing()) {
                        Util.Log.ih("dismissing progress dialog on estado");
                        secondDialog.dismiss();
                        secondDialog = null;
                    }
                }
            });*/


            String sintax = "SELECT * FROM NOTICE";
            List<Notice> listNotice = Notice.findWithQuery(Notice.class, sintax);
            if (listNotice.size() > 0) {
                String tiketSuspencionTEXTO = listNotice.get(0).getTiketsuspencion();
                if (tiketSuspencionTEXTO.equals("vacio")) {
                    Toast.makeText(mainActivity, "Lo sentimos no existe mensaje de suspensión de pagos", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        if (jsonDatos.getString("estatus").equals("1")) {
                            mostrarAvisoSuspencionTemporalDePagos(etAmount);
                        } else {
                            Toast.makeText(mainActivity, "Este contrato no se encuentra activo", Toast.LENGTH_SHORT).show();
                            Toast.makeText(mainActivity, "Fecha de reactivación: " + jsonDatos.getString("fechareactiva"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //mostrarAvisoSuspencionTemporalDePagos(etAmount);
                }
            } else
                Log.d("IMPRESION --> ", "NO retornaron datos de NOTICE");


        });

        ImageView back = (ImageView) getView().findViewById(R.id.imageViewBack);
        back.setOnClickListener(v ->
        {
            mthis = null;
            pendingCobro = false;
            FragmentManager fm = getFragmentManager();
            fm = null;

            if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                fm.popBackStack();
            } else if (fm == null && isAdded()) {
                fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                fm.popBackStack();
            }
            getFragmentManager().popBackStack();
        });

        noaporto = (Button) getView().findViewById(R.id.buttonNoAporto);
        noaporto.setOnTouchListener((v, event) -> {
            if (!pendingTicket) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    noaporto.setBackgroundResource(R.drawable.no_aporto_pressed);
                    return true;
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    final LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                    final boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (isGPSEnabled) {
                        noaporto.setBackgroundResource(R.drawable.no_aporto);
                        dialog = getRazonesDialog();
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.show();
                    } else {
                        Toast.makeText(getContext(),
                                "Activa el GPS para continuar.",
                                Toast.LENGTH_SHORT).show();
                    }

                }
                return false;
            }

            return false;
        });
        aporto = (Button) getView().findViewById(R.id.buttonAporto);
        aporto.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                amountButtonsAvailable(false);
                aporto.setBackgroundResource(R.drawable.si_aporto_pressed);
                return true;
            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                actionAporto(aporto);
                return false;
            }
            return false;
        });
    }

    private void showPaymentReferenceDialog() {

        try {

            secondDialog = getDialog();

            secondDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            secondDialog.setContentView(R.layout.dialog_payment_reference);
            secondDialog.setCancelable(false);
            secondDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(
                            Color.TRANSPARENT));
            secondDialog.show();

            EditText paymentReferenceGiven = (EditText) secondDialog.findViewById(R.id.paymentReference);

            savePaymentReference = (TextView) secondDialog
                    .findViewById(R.id.savePaymentReference);
            savePaymentReference.setOnClickListener(v1 -> {

                try {

                    paymentReferenceString = paymentReferenceGiven.getText().toString();

                    if (!paymentReferenceString.equals("")) {

                        if (!mainActivity.isFinishing()) {

                            /*
                            SAME PROCESS AS BEFORE
                             */
                            /**
                             * Checks if there's a pendint ticket to print
                             */
                            if (pendingTicket) {

                                /**
                                 * Gets last payment registered
                                 */
                                JSONObject lastPayment = preferences.getPayment();

                                try {

                                    /**
                                     * Gets folio from last payment registered
                                     */
                                    new_folio = lastPayment.getString(getString(R.string.key_folio));


                                    /**
                                     * Prints ticket
                                     */
                                    if (dialog != null) {
                                        dialog.dismiss();
                                        dialog = null;
                                    }
                                    executeServicioImpresion();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (guardarPago()) {
                                    aceptarEIniciarCobro(0, tvAceptar);
                                } else {
                                    Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_call, Toast.LENGTH_LONG);
                                    //toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                }
                            }
                            /*
                            END PROCESS
                             */

                            if (secondDialog != null && secondDialog.isShowing()) {
                                Util.Log.ih("dismissing progress dialog on estado");
                                secondDialog.dismiss();
                                secondDialog = null;
                            }
                        }

                    } else {
                        Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_add_payment_reference, Toast.LENGTH_SHORT);
                        //toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            cancelSavingPaymentReference = (TextView) secondDialog
                    .findViewById(R.id.cancelAddingPaymentReference);
            cancelSavingPaymentReference.setOnClickListener(v1 -> {
                if (!mainActivity.isFinishing()) {
                    if (secondDialog != null && secondDialog.isShowing()) {
                        Util.Log.ih("dismissing progress dialog on estado");
                        secondDialog.dismiss();
                        secondDialog = null;
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            mainActivity.toastS("Hubo un error. Por favor intentalo de nuevo");
        }
    }


    /**
     * checks that a client was selected and an amount given and
     * that cash is under cash limit, if so
     * shows ticket preview
     */
    private void actionAporto(Button aporto) {
        if (aporto != null) {
            aporto.setBackgroundResource(R.drawable.si_aporto);
            aporto.setEnabled(false);
        }

        List<Clients> clients = Clients.find(Clients.class, "number_contract = ? AND serie = ?", no_contrato, serie);
        if (clients.size() > 0) {
            double sumaCobrosPorPeriodo = 0.0, sumaGeneral = 0.0;
            Clients client = clients.get(0);
            double balance = Double.parseDouble(client.getBalance());
            amount = etAmount.getText().toString();

            //obtenemos los cobros del contrato dentro del periodo activo
            sumaCobrosPorPeriodo = DatabaseAssistant.getTotalCobrosPorContratoDentroDelPeriodoActivo(no_contrato, String.valueOf(mainActivity.getEfectivoPreference().getInt("periodo", 0)));

            try {
                sumaGeneral = ((jsonDatos.getDouble("saldo") - sumaCobrosPorPeriodo) - Double.parseDouble(amount));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (sumaGeneral >= 0) {

                if (!amount.equals("") && Double.parseDouble(amount) > balance) {
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mainActivity);
                    //builder.setMessage("Verifica que la cantidad a cobrar no exceda el saldo pendiente.");
                    builder.setMessage("Verifica que la cantidad a cobrar no exceda el saldo pendiente.\n\nSaldo pendiente: $" + balance);
                    builder.setNeutralButton("Aceptar", null);
                    builder.setCancelable(false);
                    builder.show();
                    aporto.setEnabled(true);
                    return;
                }

                if (BuildConfig.isAutoPrintingEnable()) {
                    etAmount.setText("1");
                }

                if (mainActivity.isMoneyType(amount)) {
                    if (amount.length() == 0) {
                        mainActivity.showAlertDialog("", getString(R.string.error_message_amount), true);
                        amountButtonsAvailable(true);
                        aporto.setEnabled(true);

                    } else {
                        DecimalFormat df = new DecimalFormat();
                        df.setMaximumFractionDigits(2);
                        df.setGroupingUsed(false);

                        if (amount.contains(".")) {
                            etAmount.setText(df.format(Float.parseFloat(amount)));
                        }
                        //if (aporto != null){
                        //    aporto.setBackgroundResource(R.drawable.si_aporto);
                        //}
                        double max;
                        try {
                            max = datosCobrador.getDouble("max_efectivo");
                        } catch (JSONException e) {
                            max = 0;
                            e.printStackTrace();
                        }


                        double aportacion = Double.parseDouble(amount);


                        if (aportacion > max) {
                            mainActivity.showAlertDialog("", "La cantidad de aportación excede la cantidad máxima de efectivo que puedes cargar.", true);
                            amountButtonsAvailable(true);
                            aporto.setEnabled(true);
                        } else {
                            final LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                            final boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                            if (isGPSEnabled) {

                                imprimir(0);


                                //Metodo de verificacion para saber si el monto a abonar es mayor o igual al saldo del cliente
                                if (Double.parseDouble(amount) >= balance) {
                                    String message = "", tituloPercapita = "", mensajeCobradorPercapita = "";
                                    String arregloDatosCobrador[] = DatabaseAssistant.consultarDatosDeCobrador();
                                    if (arregloDatosCobrador.length > 0) {
                                        if (arregloDatosCobrador[6].equals("vacio")) {
                                            Log.d("IMPRESION -- >", "CAMPOS VACIOS");
                                        } else {
                                            message = arregloDatosCobrador[0];
                                            tituloPercapita = arregloDatosCobrador[1];
                                            mensajeCobradorPercapita = arregloDatosCobrador[2];
                                            if (!showMessagePercapita) {
                                                mostrarCreditoPercapita(etAmount, message, mensajeCobradorPercapita, tituloPercapita);
                                                showMessagePercapita = false;
                                            } else {
                                                Log.d("IMPRESION -- >", "YA FUE MOSTRADO EL MENSAJE DE PERCAPITA");
                                            }
                                        }
                                    } else {
                                        Log.d("IMPRESION -- >", "NO SE RETORNARON DATOS");
                                    }
                                }

                            } else {
                                amountButtonsAvailable(true);
                                aporto.setEnabled(true);
                                Toast.makeText(
                                        getContext(),
                                        "Enciende el GPS e intentalo de nuevo.",
                                        Toast.LENGTH_LONG).show();
                            }
                        }

                    }
                } else {
                    mainActivity.showAlertDialog("", getString(R.string.error_message_amount), true);
                    amountButtonsAvailable(true);
                    aporto.setEnabled(true);
                }

            } else {
                verAfiliaciones(
                        aporto,
                        no_contrato,
                        String.valueOf(mainActivity.getEfectivoPreference().getInt("periodo", 0)),
                        DatabaseAssistant.getCobrosRealizadosEnElPeriodo(no_contrato, String.valueOf(mainActivity.getEfectivoPreference().getInt("periodo", 0))));

                amountButtonsAvailable(true);
                aporto.setEnabled(true);
            }
        }
    }

    public void verAfiliaciones(View view, String contrato, String periodo, List<ModelCobrosRealizadosPorPeriodo> listaDeCobros) {
        //Realizar el layout para la lista del recycler View  y complementar el codigo

        GridLayoutManager gridLayoutManager;
        final Button btEnterado;
        RecyclerView rvListaCobros;
        TextView tvPeriodo;

        AdapterCobrosRealizadosEnElPeriodo adapter;

        dialogo_cobros_realizados.setContentView(R.layout.aviso_cobros_realizados_en_periodo);
        dialogo_cobros_realizados.setCancelable(true);
        rvListaCobros = (RecyclerView) dialogo_cobros_realizados.findViewById(R.id.rvListaCobros);
        tvPeriodo = (TextView) dialogo_cobros_realizados.findViewById(R.id.tvPeriodo);
        btEnterado = (Button) dialogo_cobros_realizados.findViewById(R.id.btEnterado);

        tvPeriodo.setText(periodo);

        rvListaCobros.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(mainActivity, 1);
        rvListaCobros.setLayoutManager(gridLayoutManager);

        adapter = new AdapterCobrosRealizadosEnElPeriodo(mainActivity, listaDeCobros);
        rvListaCobros.setAdapter(adapter);


        btEnterado.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogo_cobros_realizados.dismiss();
            }
        });

        dialogo_cobros_realizados.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogo_cobros_realizados.show();

    }


    /**
     * writes visit into '.txt' file
     * writes visit into database
     * and finishes activity returning to main activity (Clientes)
     */
    private void realizarNoAportacion() {
        //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
        etAmount.setText("0");
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null) {
            mainActivity.showAlertDialog("Bluetooth", getString(R.string.error_message_bluetooth_unsoported), true);

        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                mainActivity.showAlertDialog("Bluetooth", getString(R.string.error_message_bluetooth_unactivated), true);

            } else {

                if (ApplicationResourcesProvider.getLocationProvider().getGPSPosition() != null) {

                    mainActivity.log("lat: " + ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLatitude(), "long: " + ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLongitude());

                    Double auxLatitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLatitude();
                    Double auxLongitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLongitude();

                    latitud = ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLatitude();
                    longitud = ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLongitude();

                    if ((latitud == auxLatitud) && (longitud == auxLongitud)) {
                        latitud = auxLatitud + 0.00001;
                        longitud = auxLongitud + 0.00001;
                        ApplicationResourcesProvider.setLastKnownLocation(latitud, longitud);
                    }
                }
                else // si el metodo nos retorna nullos
                {
                    double auxLatitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLatitude();
                    double auxLongitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLongitude();

                    if ((latitud != 0) && (longitud != 0))
                    {
                        latitud = auxLatitud;
                        longitud = auxLongitud;
                        ApplicationResourcesProvider.setLastKnownLocation(latitud, longitud);
                    }
                }


                String id_cobrador = "";
                String id_cliente = "";
                String serieCliente = "";
                try {
                    id_cobrador = datosCobrador.getString("no_cobrador");
                    id_cliente = jsonDatos.getString("no_contrato");
                    serieCliente = jsonDatos.getString("serie");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    String seriecobrador = "=(";
                    try {
                        if (empresa.equals("01")) {
                            seriecobrador = datosCobrador
                                    .getString("serie_programa");
                        } else {
                            seriecobrador = datosCobrador.getString(
                                    DatabaseAssistant.getSingleCompany(empresa).getName());
                        }

                        if (seriecobrador.equals("3M") && new_folio.equals("71935")) {
                            new_folio = "72155";
                        } else if (seriecobrador.equals("3M,") && new_folio.equals("24280")) {
                            new_folio = "24410";
                        } else if (seriecobrador.equals("V1") && new_folio.equals("59211")) {
                            new_folio = "59291";
                        } else if (seriecobrador.equals("6C") && new_folio.equals("63063")) {
                            new_folio = "63071";
                        } else if (seriecobrador.equals("1A1") && new_folio.equals("96874")) {
                            new_folio = "96896";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        seriecobrador = "=D";
                    }
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();

                    //Obtener el ultimo periodo desde la base de datos
                    int periodo = mainActivity.getEfectivoPreference().getInt("periodo", 0);
                    List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();
                    //int periodo = periods.get(periods.size() - 1).getPeriodNumber();

                    try {

                        if (!pendingTicket) //No hay ticket pendiente
                        {
                            LogRegister.paymentRegistrationBackup(
                                    seriecobrador + new_folio,
                                    jsonDatos.getString("serie") + no_contrato,
                                    "----",
                                    jsonDatos.getString("id_grupo_base"),
                                    "" + periodo,
                                    "Dentro de Cartera",
                                    "" + String.valueOf(jsonDatos.getDouble("saldo") - 0)
                            );
                        } else {

                            Log.i("IMPRESION", "TIENES un TICKET PENDOENTE, no podemos guardar el Log Register de nuevo.");
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //end register payment backup


                    DatabaseAssistant.insertPayment(
                            jsonDatos.getString("id_grupo_base"), //empresa
                            "" + latitud, // latitud
                            "" + longitud, //longitud
                            "" + typeStatus, // estatus
                            "" + id_cobrador, // id cobrador
                            "" + 0,  //monto
                            "" + id_cliente, //id cliente
                            dateFormat.format(date), // fecha
                            serieCliente, // serie cliente
                            "----", // folio
                            seriecobrador, // serie cobrador
                            jsonDatos.getString("tipo_bd"), // tipo bd
                            periodo, //periodo
                            paymentReferenceString,
                            "" + DatabaseAssistant.setSaldoPorCobro(jsonDatos.getDouble("saldo"), 0),
                            "Dentro de cartera"

                    );

                    if (!DatabaseAssistant.getSingleVisit(dateFormat.format(date))) {
                        Util.Log.ih("payment not found");
                        Toast toast = Toast.makeText(mainActivity, R.string.error_message_call, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        return;
                    } else {
                        Util.Log.ih("payment found");
                    }

                    if (BuildConfig.isWalletSynchronizationEnabled()) {
                        Runnable runnable = new Runnable() {

                            String contractID;

                            @Override
                            public void run() {
                                DatabaseAssistant.updateVisitMade(contractID);
                            }

                            public Runnable setData(String contractID) {
                                this.contractID = contractID;
                                return this;
                            }

                        }.setData(jsonDatos.getString("id_contrato"));
                        Thread thread = new Thread(runnable);
                        thread.start();
                    }

                    Intent intent = new Intent();
                    intent.putExtra("paymentDone", false);
                    if (commentAdded) {
                        intent.putExtra("id_contract", jsonDatos.getString("id_contrato"));
                        intent.putExtra("comments", comment);
                    }
                    fragmentResult.onFragmentResult(requestCode, Activity.RESULT_OK, intent);

                    mthis = null;
                    pendingCobro = false;


                    FragmentManager fm = getFragmentManager();
                    fm = null;

                    if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                        fm.popBackStack();
                    } else if (fm == null && isAdded()) {
                        fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                        fm.popBackStack();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    /**
     * sets listener of resources on screen as
     * $50.00
     * $75.00
     * $100.00
     * $200.00
     * $400.00
     * when one of this resources is touched, puts on the aomunt field the amount
     * of the resource touched and changes its image so that it looks as pressed
     * calling method selectOption()
     */
    void setListenersOpciones() {

        final ImageView op1 = (ImageView) getView().findViewById(R.id.imageViewOp1);
        final ImageView op2 = (ImageView) getView().findViewById(R.id.imageViewOp2);
        final ImageView op3 = (ImageView) getView().findViewById(R.id.imageViewOp3);
        final ImageView op4 = (ImageView) getView().findViewById(R.id.imageViewOp4);
        final ImageView op5 = (ImageView) getView().findViewById(R.id.imageViewOp5);
        final ImageView op6 = (ImageView) getView().findViewById(R.id.imageViewOp6);
        final ImageView op7 = (ImageView) getView().findViewById(R.id.imageViewOp7);


        op1.setOnClickListener(v -> {
            if (!pendingTicket) {
                //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
                etAmount.setText("50");
                if (selected != 1) {
                    selectOption(1);
                } else {
                    selectOption(0);
                }
            }
        });
        op2.setOnClickListener(v -> {
            if (!pendingTicket) {
                //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
                etAmount.setText("100");
                if (selected != 2) {
                    selectOption(2);
                } else {
                    selectOption(0);
                }
            }
        });
        op3.setOnClickListener(v -> {
            if (!pendingTicket) {
                //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
                etAmount.setText("200");
                if (selected != 3) {
                    selectOption(3);
                } else {
                    selectOption(0);
                }
            }
        });
        op4.setOnClickListener(v -> {
            if (!pendingTicket) {
                //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
                etAmount.setText("400");
                if (selected != 4) {
                    selectOption(4);
                } else {
                    selectOption(0);
                }
            }
        });
        op5.setOnClickListener(v -> {
            if (!pendingTicket) {
                //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
                etAmount.setText("75");
                if (selected != 5) {
                    selectOption(5);
                } else {
                    selectOption(0);
                }
            }
        });
        op6.setOnClickListener(v -> {
            if (!pendingTicket) {
                //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
                etAmount.setText("150");
                if (selected != 6) {
                    selectOption(6);
                } else {
                    selectOption(0);
                }
            }
        });
        op7.setOnClickListener(v -> {
            if (!pendingTicket) {
                //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
                etAmount.setText("300");
                if (selected != 7) {
                    selectOption(7);
                } else {
                    selectOption(0);
                }
            }
        });
    }

    /**
     * enables or disables amount buttons
     *
     * @param flag boolean type, true or false to set amount buttons
     *             availability
     */
    private void amountButtonsAvailable(boolean flag) {
        amountButton200.setEnabled(flag);
        amountButton300.setEnabled(flag);
        amountButton400.setEnabled(flag);
        amountButton500.setEnabled(flag);
        amountButton75.setEnabled(flag);
        amountButton150.setEnabled(flag);
        amountButton300New.setEnabled(flag);
    }

    /**
     * sets default amounts as pressed
     *
     * @param selected default amount pressed
     */
    void selectOption(int selected) {
        this.selected = selected;
        //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
        final ImageView op1 = (ImageView) getView().findViewById(R.id.imageViewOp1);
        final ImageView op2 = (ImageView) getView().findViewById(R.id.imageViewOp2);
        final ImageView op3 = (ImageView) getView().findViewById(R.id.imageViewOp3);
        final ImageView op4 = (ImageView) getView().findViewById(R.id.imageViewOp4);
        final ImageView op5 = (ImageView) getView().findViewById(R.id.imageViewOp5);
        final ImageView op6 = (ImageView) getView().findViewById(R.id.imageViewOp6);
        final ImageView op7 = (ImageView) getView().findViewById(R.id.imageViewOp7);
        op1.setImageResource(R.drawable.btn_50);
        op2.setImageResource(R.drawable.btn_100);
        op3.setImageResource(R.drawable.btn_200);
        op4.setImageResource(R.drawable.btn_400);
        op5.setImageResource(R.drawable.btn_75);
        op6.setImageResource(R.drawable.btn_150);
        op7.setImageResource(R.drawable.btn_300_middle);
        switch (selected) {
            case 0:
                etAmount.setText("");
                break;
            case 1:
                op1.setImageResource(R.drawable.btn_50_pressed);
                break;
            case 2:
                op2.setImageResource(R.drawable.btn_100_pressed);
                break;
            case 3:
                op3.setImageResource(R.drawable.btn_200_pressed);
                break;
            case 4:
                op4.setImageResource(R.drawable.btn_400_pressed);
                break;
            case 5:
                op5.setImageResource(R.drawable.btn_75_pressed);
                break;
            case 6:
                op6.setImageResource(R.drawable.btn_150_pressed);
                break;
            case 7:
                op7.setImageResource(R.drawable.btn_300_pressed_middle);
                break;
            default:
                break;
        }
    }

    /**
     * Gets context from this class.
     * In some cases you cannot just call 'this', because another context is envolved
     * or you want a reference to an object of this class from another class,
     * then this context is helpful.
     * <p>
     * NOTE
     * Not recommended, it may cause NullPointerException though
     *
     * @return context from ClienteDetalle
     */
    public static ClienteDetalle getMthis() {
        return mthis;
    }


    //Dialogo con el resumen de lo que se quiere imprimir, presionar aceptar para imprimir
    private void imprimir(final int Case) {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
                .getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            amountButtonsAvailable(true);
            aporto.setEnabled(true);

            mainActivity.showAlertDialog("Bluetooth",
                    getString(R.string.error_message_bluetooth_unsoported), true);
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                amountButtonsAvailable(true);
                aporto.setEnabled(true);

                mainActivity.showAlertDialog("Bluetooth",
                        getString(R.string.error_message_bluetooth_unactivated), true);
            } else {
                try {
                    final SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "dd/MM/yyyy");
                    final SimpleDateFormat dateHora = new SimpleDateFormat(
                            "HH:mm");
                    final Date date = new Date();

                    if (!BuildConfig.isAutoPrintingEnable()) {
                        mainActivity.runOnUiThread(() -> {
                            dialog = getDialog();
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.ticketpreview);
                            dialog.setCancelable(false);
                            dialog.getWindow()
                                    .setBackgroundDrawable(
                                            new ColorDrawable(
                                                    Color.TRANSPARENT));
                            dialog.show();
                        });
                    }

                    //fills ticket preview's fields with client's info
                    setEmpresa(empresa);
                    String seriecobrador = "=(";
                    //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
                    try {

                        switch (com.jaguarlabs.pabs.util.BuildConfig.getTargetBranch()) {
                            case NAYARIT:
                            case MEXICALI:
                            case MORELIA:
                            case TORREON:
                                if (empresa.equals("01")) {
                                    seriecobrador = datosCobrador
                                            .getString("serie_programa");
                                } else if (empresa.equals("03")) {
                                    seriecobrador = datosCobrador
                                            .getString("serie_malba");
                                } else if (empresa.equals("04")) {
                                    seriecobrador = datosCobrador
                                            .getString("serie_empresa4");
                                } else {
                                    seriecobrador = datosCobrador.getString(
                                            DatabaseAssistant.getSingleCompany(empresa).getName());
                                }
                                break;
                            default:
                                if (empresa.equals("01")) {
                                    seriecobrador = datosCobrador
                                            .getString("serie_programa");
                                } else {
                                    seriecobrador = datosCobrador.getString(
                                            DatabaseAssistant.getSingleCompany(empresa).getName());
                                }
                        }


                        if (seriecobrador.equals("3M") && new_folio.equals("71935")) {
                            new_folio = "72155";
                        } else if (seriecobrador.equals("3M,") && new_folio.equals("24280")) {
                            new_folio = "24410";
                        } else if (seriecobrador.equals("V1") && new_folio.equals("59211")) {
                            new_folio = "59291";
                        } else if (seriecobrador.equals("6C") && new_folio.equals("63063")) {
                            new_folio = "63071";
                        } else if (seriecobrador.equals("1A1") && new_folio.equals("96874")) {
                            new_folio = "96896";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        seriecobrador = "=D";
                    }

                    //query = null;

                    if (dialog != null) {
                        TextView serie = (TextView) dialog
                                .findViewById(R.id.textViewSerie);

                        /**
                         * If there's a pending ticket to print
                         * will get folio from last payment registered
                         */
                        if (pendingTicket) {
                            JSONObject lastPayment = preferences.getPayment();

                            try {

                                /**
                                 * Gets folio from last payment registered
                                 */
                                serie.setText("Folio: " + seriecobrador + lastPayment.getString(getString(R.string.key_folio)));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            serie.setText("Folio: " + seriecobrador + new_folio);
                        }


                        TextView fecha = (TextView) dialog
                                .findViewById(R.id.textViewFecha);
                        fecha.setText(dateFormat.format(date).toString());
                        TextView hora = (TextView) dialog
                                .findViewById(R.id.textViewHora);
                        hora.setText(dateHora.format(date).toString());
                        TextView Malba = (TextView) dialog
                                .findViewById(R.id.textViewMalba);
                        Malba.setText(ExtendedActivity.getCompanyNameFormatted(empresaString));
                        TextView Cliente = (TextView) dialog
                                .findViewById(R.id.textViewCliente);
                        Cliente.setText(nombre);
                        TextView monto = (TextView) dialog.findViewById(R.id.textViewMonto);
                        TextView datofiscal = (TextView) dialog
                                .findViewById(R.id.textViewDatosFiscales);
                        datofiscal.setText(datoempresa1 + "\n" + datoempresa2
                                + "\n" + rfc + "\n" + datoempresa3 + "\n"
                                + datoempresa4 + "\n" + datoempresa5 + "\n"
                                + datoempresa6 + "\n " + datoempresa7);
                        TextView total = (TextView) dialog
                                .findViewById(R.id.textViewTotal);
                        TextView txcontacto = (TextView) dialog
                                .findViewById(R.id.textViewContacto);
                        txcontacto.setText("No. de Contrato: "
                                + jsonDatos.getString("serie") + no_contrato);
                        total.setText("Total: $" + amount);
                        total.setVisibility(View.INVISIBLE);

                        //monto.setText("Monto de Aportación: $" + this.total);
                        monto.setText("Monto de Aportación: $" + amount);

                        String montoToLog = amount;
                        tvCancelar = (TextView) dialog.findViewById(R.id.textView12);


                        tvCancelar.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!mainActivity.isFinishing()) {
                                    if (dialog != null && dialog.isShowing()) {
                                        Util.Log.ih("dismissing progress dialog");
                                        dialog.dismiss();
                                        dialog = null;
                                    }
                                }
                                amountButtonsAvailable(true);
                                aporto.setEnabled(true);
                            }
                        });

                        /*tvCancelar.setOnClickListener(v ->
                        {
                            if (!mainActivity.isFinishing()) {
                                if (dialog != null && dialog.isShowing()) {
                                    Util.Log.ih("dismissing progress dialog");
                                    dialog.dismiss();
                                    dialog = null;
                                }
                            }
                            //dialog.dismiss();

                            amountButtonsAvailable(true);
                            aporto.setEnabled(true);
                        });*/

                        tvAceptar = (TextView) dialog
                                .findViewById(R.id.textView13);
                        tvAceptar.setOnClickListener(new OnClickListener() {

                            String serieCobrador;
                            String monto;

                            @Override
                            public void onClick(View v) {

                                //if (BluetoothPrinter.printerConnected) {
                                LocationManager locationManager = null;
                                boolean isGPSEnabled;
                                if (getContext() != null)
                                    locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                                if (locationManager != null)
                                    isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                                else
                                    isGPSEnabled = false;

                                if (isGPSEnabled) {
                                    if (mainActivity.checkBluetoothAdapter()) {

                                        //register payment backup
                                        try {
                                            //folio, contrato, monto, empresa, periodo, tipo de cobro

                                            if (!pendingTicket) //No hay ticket pendiente
                                            {
                                                LogRegister.paymentRegistrationBackup(
                                                        serieCobrador + new_folio,
                                                        jsonDatos.getString("serie") + no_contrato,
                                                        monto,
                                                        jsonDatos.getString("id_grupo_base"),
                                                        "" + mainActivity.getEfectivoPreference().getInt("periodo", 0),
                                                        "Dentro de Cartera",
                                                        "" + String.valueOf(jsonDatos.getDouble("saldo") - Float.parseFloat(monto)) //""+ String.valueOf(jsonDatos.getDouble("saldo") -  0)

                                                );
                                            } else {

                                                Log.i("IMPRESION", "TIENES un TICKET PENDOENTE, no podemos guardar el Log Register de nuevo.");
                                            }


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        //end register payment backup

                                              /*
                                              START ADD PAYMENT REFERENCE
                                               */
                                        if (BuildConfig.getTargetBranch() == BuildConfig.Branches.QUERETARO || BuildConfig.getTargetBranch() == BuildConfig.Branches.IRAPUATO
                                                || BuildConfig.getTargetBranch() == BuildConfig.Branches.SALAMANCA || BuildConfig.getTargetBranch() == BuildConfig.Branches.CELAYA
                                                || BuildConfig.getTargetBranch() == BuildConfig.Branches.LEON) {
                                            showPaymentReferenceDialog();
                                        } else {
                                              /*
                                              END ADD PAYMENT REFERENCE
                                               */

                                            /**
                                             * Checks if there's a pendint ticket to print
                                             */
                                            if (pendingTicket) {

                                                /**
                                                 * Gets last payment registered
                                                 */
                                                JSONObject lastPayment = preferences.getPayment();

                                                try {

                                                    /**
                                                     * Gets folio from last payment registered
                                                     */
                                                    new_folio = lastPayment.getString(getString(R.string.key_folio));


                                                    /**
                                                     * Prints ticket
                                                     */
                                                    if (dialog != null) {
                                                        dialog.dismiss();
                                                        dialog = null;
                                                    }
                                                    executeServicioImpresion();


                                                    //register payment backup
                                                    try {
                                                        if (pendingTicket) //No hay ticket pendiente
                                                        {
                                                            LogRegister.paymentRegistrationBackup(
                                                                    serieCobrador + new_folio,
                                                                    jsonDatos.getString("serie") + no_contrato,
                                                                    monto,
                                                                    jsonDatos.getString("id_grupo_base"),
                                                                    "" + mainActivity.getEfectivoPreference().getInt("periodo", 0),
                                                                    "Reimpresion de folio",
                                                                    "-");
                                                        } else {
                                                            Log.i("IMPRESION", "NO TIENES TICKET PENDIENTE.");
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    //end register payment backup

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            } else {
                                                if (guardarPago()) {
                                                    aceptarEIniciarCobro(Case, tvAceptar);
                                                } else {
                                                    Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_call, Toast.LENGTH_LONG);
                                                    //toast.setGravity(Gravity.CENTER, 0, 0);
                                                    toast.show();
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    mainActivity.toastL(R.string.info_message_gps);
                                }
                            }


                            public OnClickListener setData(String monto, String serieCobrador) {
                                this.serieCobrador = serieCobrador;
                                this.monto = monto;

                                return this;
                            }
                        }.setData(amount, seriecobrador));
                    }

                    //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);

                    if (BuildConfig.isAutoPrintingEnable()) {
                        etAmount.setText("1");
                    }

                    this.total = amount;

                    if (BuildConfig.isAutoPrintingEnable()) {
                        aceptarEIniciarCobro(Case, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String getPrinterSerial() {
        String serial = "";
        int retries = 0;
        while (retries < 2) {
            try {
                BluetoothPrinter.getInstance().connect();
                serial = BluetoothPrinter.getInstance().sendcommand_getString("OUT \"\";_SERIAL$").split("\r\n")[0];

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
                    BluetoothPrinter.getInstance().disconnect();

                break;
            } catch (Exception ex) {
                ex.printStackTrace();
                retries++;
            }
        }

        return serial;
    }
    public void insertSerialPrinter()
    {
        List<Collectors> listaCollectors = Collectors.findWithQuery(Collectors.class, "SELECT * FROM COLLECTORS");
        if (listaCollectors.size() > 0)
        {
            Configuraciones.deleteAll(Configuraciones.class);
            DatabaseAssistant.insertarConfiguracion(getPrinterSerial(), listaCollectors.get(0).getMacAddress());
            Log.e("saliendo", "PrinterSerial registrado");

        } else
            Log.i("IMPRESION-->", "No se obtuvieron registros de Collectors");
    }


    /**
     * writes payment into database
     * writes cash plus amount of ongoing payment into sharedPreferences file
     * writes new folio with the company into sharePreferences file
     */

    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitud = location.getLongitude();
            latitud = location.getLatitude();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private boolean guardarPago()
    {
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (isGPSEnabled)
        {
            if (ApplicationResourcesProvider.getLocationProvider().getGPSPosition() != null) {

                Double auxLatitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLatitude();
                Double auxLongitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLongitude();

                latitud = ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLatitude();
                longitud = ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLongitude();

                if ((latitud == auxLatitud) && (longitud == auxLongitud)) {
                    latitud = auxLatitud + 0.00001;
                    longitud = auxLongitud + 0.00001;
                    ApplicationResourcesProvider.setLastKnownLocation(latitud, longitud);
                }
                Util.Log.ih("latitud = " + latitud);
                Util.Log.ih("longitud = " + longitud);
            }
            else // si el metodo nos retorna nullos
            {
                double auxLatitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLatitude();
                double auxLongitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLongitude();

                if ((latitud != 0) && (longitud != 0))
                {
                    latitud = auxLatitud;
                    longitud = auxLongitud;
                    ApplicationResourcesProvider.setLastKnownLocation(latitud, longitud);
                }
            }
        }

        /*We Apply new process for locations into payments success *******************************************************************************************************
        LocationManager locationManagerFromLocalService = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        Location location = null;
        boolean isGPSEnabledd = locationManagerFromLocalService.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManagerFromLocalService.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Util.Log.ih("latitud = " + latitud);
            Util.Log.ih("longitud = " + longitud);
        }

        try {
            if (!isGPSEnabledd && !isNetworkEnabled)
            {
                Log.e("PERMISO GPS", "El servicio GPS y de Red ya estan habilitados");
            }
            else
            {
                if (isNetworkEnabled)
                {
                    locationManagerFromLocalService.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 1, locationListener);
                    location = locationManagerFromLocalService.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null)
                    {
                        latitud = location.getLatitude();
                        longitud = location.getLongitude();
                    }
                }
                if (isGPSEnabledd)
                {
                    if (location == null)
                    {
                        locationManagerFromLocalService.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 1, locationListener);
                        location = locationManagerFromLocalService.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null)
                        {
                            latitud = location.getLatitude();
                            longitud = location.getLongitude();
                        }
                    }
                }
                latitud = latitud + 0.00001;
                longitud = longitud + 0.00001;
                ApplicationResourcesProvider.setLastKnownLocation(latitud, longitud);
                Log.e("PERMISO GPS", "1: "+ latitud + "  Longitud: "+ longitud);
            }
            if(location != null)
            {
                latitud =  location.getLatitude();
                longitud = location.getLongitude();
            }

            Log.e("PERMISO GPS", "2: "+ latitud + "  Longitud: "+ longitud);
        }
        catch (Exception e)
        {
            Log.e("Error", e.toString());
        }
        //******************************************************************************************************************************************************************/
        String id_cobrador = "";
        String id_cliente = "";
        String serieCliente = "";
        try {
            id_cobrador = datosCobrador.getString("no_cobrador");
            id_cliente = jsonDatos.getString("no_contrato");
            serieCliente = jsonDatos.getString("serie");

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

        try {
            String seriecobrador = "=(";
            try {
                switch (com.jaguarlabs.pabs.util.BuildConfig.getTargetBranch()) {
                    case NAYARIT:
                    case MEXICALI:
                    case MORELIA: case TORREON:
                        if (empresa.equals("01")) {
                            seriecobrador = datosCobrador
                                    .getString("serie_programa");
                        } else if (empresa.equals("03")) {
                            seriecobrador = datosCobrador
                                    .getString("serie_malba");
                        } else if (empresa.equals("04")) {
                            seriecobrador = datosCobrador
                                    .getString("serie_empresa4");
                        } else {
                            seriecobrador = datosCobrador.getString(
                                    DatabaseAssistant.getSingleCompany(empresa).getName());
                        }
                        break;
                    default:
                        if (empresa.equals("01")) {
                            seriecobrador = datosCobrador
                                    .getString("serie_programa");
                        } else {
                            seriecobrador = datosCobrador.getString(
                                    DatabaseAssistant.getSingleCompany(empresa).getName());
                        }
                }


                if (seriecobrador.equals("3M") && new_folio.equals("71935")) {
                    new_folio = "72155";
                } else if (seriecobrador.equals("3M,") && new_folio.equals("24280")) {
                    new_folio = "24410";
                } else if (seriecobrador.equals("V1") && new_folio.equals("59211")) {
                    new_folio = "59291";
                } else if (seriecobrador.equals("6C") && new_folio.equals("63063")) {
                    new_folio = "63071";
                } else if (seriecobrador.equals("1A1") && new_folio.equals("96874")) {
                    new_folio = "96896";
                }
            } catch (JSONException e) {
                e.printStackTrace();
                seriecobrador = "=D";
                return false;
            }

            if (DatabaseAssistant.checkIfPaymentExists(new_folio, seriecobrador)) {
                SharedPreferences preference = getContext().getSharedPreferences("folios", Context.MODE_PRIVATE);
                Editor editor = preference.edit();

                try {
                    editor.putString(
                            DatabaseAssistant.getSingleCompany(jsonDatos.getString("id_grupo_base")).getName(),
                            new_folio
                    );

                    editor.apply();
                } catch (Exception e) {
                    e.printStackTrace();

                    //return false;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);

                builder.setTitle("Folio");
                builder.setMessage("El folio se encuentra repetido");
                builder.setNeutralButton("Aceptar", (dialog1, which) -> {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    FragmentManager fm = getFragmentManager();
                    fm = null;

                    if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                        fm.popBackStack();
                    } else if (fm == null && isAdded()) {
                        fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                        fm.popBackStack();
                    }
                });
                builder.setCancelable(false);
                builder.show();
                return false;
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            final Date date = new Date();
            String nombreEmpresa = DatabaseAssistant.getSingleCompany(jsonDatos.getString("id_grupo_base")).getName();

            final String contractID;
            try {
                contractID = jsonDatos.getString("id_contrato");

                //Obtener el ultimo periodo desde la base de datos
                List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();
                //int periodo = periods.get(periods.size() - 1).getPeriodNumber();

                Util.Log.ih("===================================================================");
                Util.Log.ih("latitude = " + latitud);
                Util.Log.ih("longitude = " + longitud);

                DecimalFormat df = new DecimalFormat();
                df.setMaximumFractionDigits(2);
                df.setGroupingUsed(false);


                DatabaseAssistant.insertPayment(
                        jsonDatos.getString("id_grupo_base"),
                        "" + latitud,
                        "" + longitud,
                        "1",
                        "" + id_cobrador,
                        "" + df.format(Float.parseFloat(amount)),
                        "" + id_cliente,
                        dateFormat.format(date),
                        serieCliente,
                        new_folio,
                        seriecobrador,
                        jsonDatos.getString("tipo_bd"),
                        mainActivity.getEfectivoPreference().getInt("periodo", 0),
                        paymentReferenceString,
                        ""+ DatabaseAssistant.setSaldoPorCobro(jsonDatos.getDouble("saldo"), Float.parseFloat(amount)),
                        "Dentro de cartera"

                );

                //*********actualizacion de cobrosMasCancelados
                //float cobrosMasCanceladosMasMontoACobrar = DatabaseAssistant.getTotalCobrosPorCompania(jsonDatos.getString("id_grupo_base")) + Float.valueOf(amount);
                String queryActualizacion="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET cobrosMasCancelados = cobrosMasCancelados + " +Float.valueOf(amount)+ " WHERE periodo='"+mainActivity.getEfectivoPreference().getInt("periodo", 0)+"' and empresa='"+ jsonDatos.getString("id_grupo_base")+"'";
                MontosTotalesDeEfectivo.executeQuery(queryActualizacion);

                //*********actualizacion de totalCobrosMenosDepositos
                //float total_Cobros_Menos_Depositos_Menos_Monto_A_Cobrar = DatabaseAssistant.getTotalCobrosMenosDepositosPorEmpresa(jsonDatos.getString("id_grupo_base")) - Float.valueOf(amount);
                String queryActualizacion2="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET totalCobrosMenosDepositos = totalCobrosMenosDepositos - " + Float.valueOf(amount) +" WHERE periodo='"+mainActivity.getEfectivoPreference().getInt("periodo", 0)+"' and empresa='"+ jsonDatos.getString("id_grupo_base")+"'";
                MontosTotalesDeEfectivo.executeQuery(queryActualizacion2);

                //*********actualizacion de cobrosMenosDepositos
                //double cobros_Menos_Depositos_Mas_Monto_A_Cobrar_Menos_Depositos = (DatabaseAssistant.getCobrosMenosDepositosPorEmpresa(jsonDatos.getString("id_grupo_base")) + Float.valueOf(amount) - DatabaseAssistant.getTotalDepositsByCompany(jsonDatos.getString("id_grupo_base"))) ;//depositos);
                String queryActualizacion3="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET cobrosMenosDepositos = cobrosMenosDepositos + "+ Float.valueOf(amount) +" WHERE periodo='"+mainActivity.getEfectivoPreference().getInt("periodo", 0)+"' and empresa='"+ jsonDatos.getString("id_grupo_base")+"'";
                MontosTotalesDeEfectivo.executeQuery(queryActualizacion3);






                //Actualizar los demas campos, dentro de los Updates de cobros y depositos

                int asas=0;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }

            ExportImportDB exportImportDB = new ExportImportDB();
            try {
                if (exportImportDB.exportDBHidden()) {
                    Util.Log.i("BDS!");
                } else {
                    Util.Log.i("BDE!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (!DatabaseAssistant.getSinglePayment(new_folio, dateFormat.format(date))) {
                Util.Log.ih("payment not found");
                return false;
            } else {
                Util.Log.ih("payment found");
            }

            if (BuildConfig.isWalletSynchronizationEnabled()) {
                Runnable runnable = () -> DatabaseAssistant.updatePaymentMade(contractID);
                Thread thread = new Thread(runnable);
                thread.start();
                //DatabaseAssistant.updatePaymentMade(contractID);
            }


            /*
            SharedPreferences efectivoPreference = getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
            Editor efectivoEditor = efectivoPreference.edit();
            efectivoEditor.putFloat(values.empresaString, efectivoPreference.getFloat(values.empresaString, 0) + Float.parseFloat(values.total)).apply();
            */

            //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);

            //float efectivo = mainActivity.getEfectivoPreference().getFloat(empresaString, 0);
            //cashPreferenceBeforeChange = efectivo;
            double nuevoEfectivo = DatabaseAssistant.getTotalEfectivoAcumulado() + Double.parseDouble(amount);

            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(2);
            df.setGroupingUsed(false);

            mainActivity.getEfectivoEditor().putFloat(empresaString, Float.parseFloat(df.format(nuevoEfectivo))).apply();

            SharedPreferences preference = getContext().getSharedPreferences("folios", Context.MODE_PRIVATE);
            Editor editor = preference.edit();

            try {
                editor.putString(nombreEmpresa, new_folio);
                editor.apply();
            } catch (Exception e) {
                e.printStackTrace();

                return false;
            }

            if (seriecobrador.equals("")) {
                throw new JSONException("");
            }


            //save payment into SharedPreference file
            savePaymentToSharedPreferences(new_folio,  String.valueOf(df.format(Float.parseFloat(amount))), serieCliente + id_cliente);

            return true;

        } catch (JSONException e) {

            //write stacktrace into a .txt file
            //StackTraceHandler writeStackTrace = new StackTraceHandler();
            //writeStackTrace.uncaughtException("CLIENTEDETALLE.GuardarPago-excep", e);

            e.printStackTrace();

            return false;
        }
    }

    /**
     * This method saves important info from ongoing payment in another
     * sharedPreferences file, so that if something goes wrong during dialog_copies ticket (dialog_copies failed
     * for some reason), it will be possible to print that ticket again, chosing account from Clientes
     * activity and pressing 'print'
     *
     * @param folio      folio from ongoing payment
     * @param monto      amount from ongoing payment
     * @param id_cliente cliente id from ongoing payment
     */
    public void savePaymentToSharedPreferences(String folio, String monto, String id_cliente) {
        //Preferences preferences = new Preferences(mthis);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setGroupingUsed(false);

        preferences.savePayment(folio, String.valueOf(df.format(Float.parseFloat(amount))), id_cliente);
    }

    /**
     * just calls executeServicioImpresion() method to start
     * android service
     *
     * @param Case
     * @param aceptar
     */
    private void aceptarEIniciarCobro(final int Case, TextView aceptar) {

        pendingCobro = true;
        if (!mainActivity.isFinishing()) {
            if (dialog != null && dialog.isShowing()) {
                Util.Log.ih("dismissing progress dialog");
                dialog.dismiss();
                dialog = null;
            }
        }
        if (aceptar != null) {
            aceptar.setBackgroundColor(Color.GRAY);
        }

        //TODO Migrando a Service Spice
        executeServicioImpresion();

        if (aceptar != null) {
            aceptar.setBackgroundColor(Color.WHITE);
        }

    }

    /**
     * loads values which will be sent to android service
     *
     * @return Values object with all info
     */
    private ModelPrintPaymentRequest loadValuesToRequest() {
        ModelPrintPaymentRequest modelPrintPaymentRequest = new ModelPrintPaymentRequest();
        modelPrintPaymentRequest.setJsonInfo(jsonDatos);
        modelPrintPaymentRequest.setDatosCobrador(datosCobrador);
        modelPrintPaymentRequest.setNew_folio(new_folio);
        modelPrintPaymentRequest.setEmpresaString(empresaString);
        modelPrintPaymentRequest.setDatoempresa1(datoempresa1);
        modelPrintPaymentRequest.setDatoempresa2(datoempresa2);
        modelPrintPaymentRequest.setRfc(rfc);
        modelPrintPaymentRequest.setDireccion(direccion);
        modelPrintPaymentRequest.setDatoempresa6(datoempresa6);
        modelPrintPaymentRequest.setDatoempresa7(datoempresa7);
        modelPrintPaymentRequest.setImpreso(impreso);
        modelPrintPaymentRequest.setMensaje(mensaje);
        modelPrintPaymentRequest.setImprimio(imprimio);
        modelPrintPaymentRequest.setTotal(amount);
        modelPrintPaymentRequest.setMensajepercapita(mensajePercapita);
        modelPrintPaymentRequest.setTituloPercapita(tituloPercapitaGeneral);
        try {
            modelPrintPaymentRequest.setSaldo(jsonDatos.getString("saldo"));
        } catch (JSONException e) {
            modelPrintPaymentRequest.setSaldo("0.0");
        }
        return modelPrintPaymentRequest;
    }

    /**
     * starts android service to print ticket in another thread
     */
    private void executePrintAccountStatus()
    {

        try {
            String collectorname = datosCobrador.getString("nombre");
            String clientName = jsonDatos.getString("nombre");
            String clientLastName = jsonDatos.getString("apellido_pat") + " "
                    + jsonDatos.getString("apellido_mat");
            String clientSerie = serie;
            String clientContractNumber = jsonDatos.getString("no_contrato");
            String clientBalance = jsonDatos.getString("saldo");

            PrintAccountStatusTicket printAccountStatusTicket = new PrintAccountStatusTicket(
                    collectorname, clientName, clientLastName, clientContractNumber,
                    clientSerie, clientBalance);

            spicePrintAccountStatusTicket.executeRequest(printAccountStatusTicket);

        } catch (JSONException e) {
            mainActivity.toastL(R.string.error_message_call);
            e.printStackTrace();
        }
    }

    /**
     * starts android service to print ticket in another thread
     */
    private void executeServicioImpresion() {
        PrintPaymentTicketRequest printPaymentTicketRequest = new PrintPaymentTicketRequest(loadValuesToRequest());
        spicePrintPaymentTicket.executeRequest(printPaymentTicketRequest);
        //insertSerialPrinter();
    }
    /**
     * starts android service to print ticket in another thread
     */
    private void executePrintVisitTicket() {
        String collectorName;
        String telefono = "";
        String no_cobrador = "";
        try {
            collectorName = datosCobrador.getString("nombre");
            no_cobrador = datosCobrador.getString("no_cobrador");

            if (datosCobrador.has("telefono"))
                telefono = datosCobrador.getString("telefono");
        } catch (JSONException e) {
            collectorName = "Probenso";
            e.printStackTrace();
        }
        String clientName = nombre;
        String clientLastName = apellido;
        String clientContractNumber = no_contrato;
        String clientSerie = serie;
        String idCompany = empresa;
        String businessNameFirstLine = datoempresa1;
        String businessNameSecondLine = datoempresa2;

        PrintVisitTicketRequest printVisitTicketRequest = new PrintVisitTicketRequest(
                collectorName,
                clientName,
                clientLastName,
                clientContractNumber,
                clientSerie,
                idCompany,
                businessNameFirstLine,
                businessNameSecondLine,
                telefono,
                no_cobrador
        );

        spicePrintVisitTicket.executeRequest(printVisitTicketRequest);
    }

    /**
     * android service
     * an error happened
     * show up a dialog with option to retry to print
     *
     * @param exception error thrown
     */
    private void onServicioImpresionFailure(SpiceException exception) {
        pendingPrintingRequest = false;

        mainActivity.LogEh(R.string.payment_failed);

        AlertDialog.Builder tryAgainDialog = getAlertDialogBuilder()
                .setMessage(R.string.error_message_printing_try_again)
                .setPositiveButton(R.string.error_title_try_again, (dialog1, which) -> {
                });
        tryAgainDialog.create().show();
    }

    private int cont = 0;

    /**
     * this method gets called when android service for dialog_copies tickets finishes
     * either with an error or with success
     *
     * @param response response with the result of the android service
     *                 what is needed is response.result which is a boolean
     *                 true -> successfull
     *                 false -> error
     */
    private void onServicioImpresionSuccess(boolean response)
    {
        //Toast.makeText(mainActivity, "onServicioImpresionSuccess", Toast.LENGTH_SHORT).show();
        pendingPrintingRequest = false;

        if (response)
        {
            if (BuildConfig.isPrintingCopiesEnabled()) {
                showSecondPrintingDialog();
            }

            if (!BuildConfig.isPrintingCopiesEnabled())
            {
                try {
                    Log.e("saliendo", "de cobro dentro de cartera");

                    Intent intent = new Intent();

                    double currentCash = datosCobrador.getDouble("efectivo") + Double.parseDouble(amount);

                    intent.putExtra("nuevo_efectivo", Double.toString(currentCash));

                    if (BuildConfig.isAutoPrintingEnable()) {
                        etAmount.setText("1");
                    }
                    intent.putExtra("paymentDone", true);
                    fragmentResult.onFragmentResult(requestCode, Activity.RESULT_OK, intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                insertSerialPrinter();

                mthis = null;
                FragmentManager fm = getFragmentManager();
                fm = null;

                if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                    fm.popBackStack();
                } else if (fm == null && isAdded()) {
                    fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                    fm.popBackStack();
                }
            }
        } else {
            Mint.logEvent("Mensaje Reintentar - folio: " + new_folio + " - user: " + loadUser(), MintLogLevel.Info);
            mainActivity.LogEh(R.string.error_message_printing_try_again);

            try {
                if (tryAgainDialog != null) {
                    tryAgainDialog.show();
                    Util.Log.ih("tryAgainDialog != null -> show");
                } else {
                    tryAgainDialog = getAlertDialogBuilder();
                    tryAgainDialog.setMessage(R.string.error_message_printing_try_again)
                            .setPositiveButton(R.string.error_title_try_again, (dialog1, which) -> executeServicioImpresion());
                    tryAgainDialog.setCancelable(false);
                    tryAgainDialog.create().show();
                }

            } catch (Exception e) {
                Util.UI.toastL(getContext(), getString(R.string.error_message_printing_try_again));
                mthis = null;
                FragmentManager fm = getFragmentManager();
                fm = null;

                if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                    fm.popBackStack();
                } else if (fm == null && isAdded()) {
                    fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                    fm.popBackStack();
                }
                e.printStackTrace();
            }
        }
    }

    /**
     * Loads user name of collector.
     * This is focused to Splunk Mint, in order to report error by user name
     *
     * @return collector's user name
     */
    private String loadUser() {
        return getContext().getSharedPreferences("PABS_Login", Context.MODE_PRIVATE).getString("user_login", "");
    }

    //Dialogo para imprimir copia
    public void showSecondPrintingDialog()
    {
        mainActivity.runOnUiThread(() -> {
            copies++;

            if (copies > 1) {
                try {
                    pendingCobro = false;

                    Log.e("saliendo", "de aportación fuera de cartera");

                    double currentCash = datosCobrador.getDouble("efectivo")
                            + Double.parseDouble(amount);

                    Intent intent = new Intent();
                    intent.putExtra("nuevo_efectivo", Double.toString(currentCash));
                    intent.putExtra("paymentDone", true);
                    if (commentAdded) {
                        intent.putExtra("id_contract", jsonDatos.getString("id_contrato"));
                        intent.putExtra("comments", comment);
                    }

                    //setResult(RESULT_OK, intent);
                    fragmentResult.onFragmentResult(requestCode, Activity.RESULT_OK, intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mthis = null;
                //finish();

                FragmentManager fm = getFragmentManager();
                fm = null;

                if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                    fm.popBackStack();
                    //Toast.makeText(mainActivity, "Entro a if", Toast.LENGTH_SHORT).show();
                } else if (fm == null && isAdded()) {
                    fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                    //Toast.makeText(mainActivity, "Entro a else es NULLO", Toast.LENGTH_SHORT).show();
                    fm.popBackStack();
                }
                //getFragmentManager().popBackStack();
            } else {
                try {
                    newsecondDialog = getDialog();
                    newsecondDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    newsecondDialog.setContentView(R.layout.dialog_copies);
                    newsecondDialog.setCancelable(false);
                    newsecondDialog.show();

                    TextView text = (TextView) newsecondDialog
                            .findViewById(R.id.textView2);
                    text.setText(getString(R.string.info_message_copy));
                    TextView textc = (TextView) newsecondDialog
                            .findViewById(R.id.textViewCancel);
                    textc.setVisibility(View.VISIBLE);
                    textc.setText("No");
                    textc.setGravity(Gravity.CENTER_HORIZONTAL);
                    textc.setOnClickListener(v -> {
                        try {
                            pendingCobro = false;

                            Log.e("saliendo", "de aportación fuera de cartera");

                            double currentCash = datosCobrador.getDouble("efectivo")
                                    + Double.parseDouble(amount);

                            Intent intent = new Intent();
                            intent.putExtra("nuevo_efectivo", Double.toString(currentCash));
                            intent.putExtra("paymentDone", true);
                            if (commentAdded) {
                                intent.putExtra("id_contract", jsonDatos.getString("id_contrato"));
                                intent.putExtra("comments", comment);
                            }
                            //setResult(RESULT_OK, intent);
                            fragmentResult.onFragmentResult(requestCode, Activity.RESULT_OK, intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (!mainActivity.isFinishing()) {
                            if (newsecondDialog != null && newsecondDialog.isShowing()) {
                                newsecondDialog.dismiss();
                                newsecondDialog = null;
                            }
                        }

                        mthis = null;
                        //finish();

                        FragmentManager fm = getFragmentManager();
                        fm = null;

                        if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                            fm.popBackStack();
                            //Toast.makeText(mainActivity, "Entro a if", Toast.LENGTH_SHORT).show();
                        } else if (fm == null && isAdded()) {
                            fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                            //Toast.makeText(mainActivity, "Entro a else es NULLO", Toast.LENGTH_SHORT).show();
                            fm.popBackStack();
                        }

                        //getFragmentManager().popBackStack();
                    });
                    TextView test = (TextView) newsecondDialog.findViewById(R.id.textViewAceptar);
                    test.setText("Sí");
                    test.setOnClickListener(v -> {
                        if (!mainActivity.isFinishing()) {
                            if (newsecondDialog != null
                                    && newsecondDialog.isShowing()) {
                                newsecondDialog.dismiss();
                                newsecondDialog = null;
                            }
                        }
                        executeServicioImpresion();
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    //if (copiesCounter <= 1)
                    //executeServicioImpresion();
                    mainActivity.toastL("Se imprimió copia de ticket");
                    //finish();

                    FragmentManager fm = getFragmentManager();
                    fm = null;

                    if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                        fm.popBackStack();
                        //Toast.makeText(mainActivity, "Entro a if", Toast.LENGTH_SHORT).show();
                    } else if (fm == null && isAdded()) {
                        fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                        //Toast.makeText(mainActivity, "Entro a else es NULLO", Toast.LENGTH_SHORT).show();
                        fm.popBackStack();
                    }

                    //getFragmentManager().popBackStack();
                }
            }
        });

    }

    /**
     * returns an AsyncPrint instance with '2' as first param
     * @return AsyncPrint instance
     */
    /*
    private AsyncPrint getAsyncPrintCaseTwo(){
        return new AsyncPrint(2, this, getProgressDialog());
    }
    */

    /**
     * returns an AsyncPrint instance with '7' as first param
     *
     * @return AsyncPrint instance
     */
    /*
    private AsyncPrint getAsyncPrintCaseSeven(){
        return new AsyncPrint(7, this, getProgressDialog());
    }
    */
    @Override
    public void posicionSeleccionada(int position)
    {
        String clientID = "";
        try {
            clientID = jsonDatos.getString("no_contrato");
        } catch (JSONException e) {
            clientID = "";
            e.printStackTrace();
        }

        if (!clientID.equals("") && !DatabaseAssistant.isthereAnyVisitMade30MinutesAgo(clientID))
        {
            switch (position)
            {
                case 0:
                case 1:
                    typeStatus = 2 + position;
                    break;
                case 2:
                case 3:
                    typeStatus = 3 + position;
                    break;
                case 4:
                case 5:
                case 6:
                    typeStatus = 4 + position;
                    break;
            }

            Util.Log.ih(":::::::::::typeStatus = " + typeStatus);

            if (typeStatus == 2 && BuildConfig.getTargetBranch()!= BuildConfig.Branches.MORELIA)
            {
                BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                if (mBluetoothAdapter == null)
                {
                    mainActivity.showAlertDialog("Bluetooth", getString(R.string.error_message_bluetooth_unsoported), true);
                } else {
                    if (!mBluetoothAdapter.isEnabled())
                    {
                        mainActivity.showAlertDialog("Bluetooth", getString(R.string.error_message_bluetooth_unactivated), true);
                    } else {
                        setEmpresa(empresa);

                        secondDialog = getDialog();
                        secondDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        secondDialog.setContentView(R.layout.previewnoaporto);
                        secondDialog.setCancelable(false);
                        secondDialog.getWindow().setBackgroundDrawable(
                                new ColorDrawable(
                                        Color.TRANSPARENT));
                        secondDialog.show();


                        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        SimpleDateFormat dateHora = new SimpleDateFormat("HH:mm");
                        final Date date = new Date();
                        TextView razon = (TextView) secondDialog.findViewById(R.id.textView2Razon);
                        razon.setText(datoempresa1 + " " + datoempresa2 + "\n" + datoempresa3 + " " + datoempresa4);
                        TextView fecha = (TextView) secondDialog.findViewById(R.id.textViewFecha);
                        fecha.setText(dateHora.format(date) + " " + dateFormat.format(date));
                        TextView cliente = (TextView) secondDialog.findViewById(R.id.textViewCliente);
                        TextView nocontrato = (TextView) secondDialog.findViewById(R.id.textViewContrato);
                        TextView cobrador = (TextView) secondDialog.findViewById(R.id.textViewCobrador);

                        try {
                            cobrador.setText(datosCobrador.getString("nombre"));
                            cliente.setText(jsonDatos.getString("nombre"));
                            nocontrato.setText(jsonDatos.getString("serie") + "-" + jsonDatos.getString("no_contrato"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        tvAceptar = (TextView) secondDialog.findViewById(R.id.textViewAceptar);

                        mthis = this;

                        tvAceptar.setOnClickListener(v ->
                        {
                            pendingCobro = true;
                            if (!mainActivity.isFinishing()) {
                                if (secondDialog != null && secondDialog.isShowing()) {
                                    secondDialog.dismiss();
                                    secondDialog = null;
                                }
                            }
                            executePrintVisitTicket();
                        });

                        tvCancelar = (TextView) secondDialog.findViewById(R.id.textViewCancelar);
                        tvCancelar.setOnClickListener(v ->
                        {
                            pendingCobro = false;
                            if (!mainActivity.isFinishing()) {
                                if (secondDialog != null && secondDialog.isShowing()) {
                                    secondDialog.dismiss();
                                    secondDialog = null;
                                }
                            }
                        });
                    }
                }
            } else {
                realizarNoAportacion();
            }
        } else if (clientID.equals("")) {
            Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_call, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_visit_not_allowed, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    public void setFragmentResult(OnFragmentResult fragmentResult) {
        this.fragmentResult = fragmentResult;
    }

    /**
     * Class with variables that helps to print when are sent
     * to android service
     */
    /*
      public class ModelPrintPaymentRequest {
        public String new_folio;
        public JSONObject datosCobrador;
        public JSONObject jsonInfo;
        public String mensaje;
        public String empresa;
        public String empresaString;
        public String datoEmpresa1;
        public String datoEmpresa2;
        public String datoEmpresa6;
        public String datoEmpresa7;
        public String rfc;
        public String[] direccion;
        public String total;
        public boolean imprimio;

        public String saldo;
      }
    */

    public static final int A1 = 1;
    public static final int A2 = 2;
    public static final int A3 = 3;
    public static final int A4 = 4;
    public static final int A5 = 5;
    public static final int A6 = 6;
    public static final int A7 = 7;
    public static final int A8 = 8;
    public static final int A9 = 9;


    public void mostrarDomicilio(View view, boolean bloqueo)
    {
        ArrayList<String> CIUDADES = new ArrayList<String>();

        LinearLayout layoutGuardado, layout1;
        CheckBox cbDireccion, cbTelefono, cbCorreo;
        JSONObject jsonDatosGenerales = new JSONObject();
        final boolean[] soloHayUnaPosicion = {false};
        Button tvCancelar;
        Button btGuardar, btGuardarCorreo, btGuardarTelefono;
        EditText etCalle, etNumero, etInterior, etCorreo, etTelefono, etEntreCalles;
        AutoCompleteTextView etColonia;
        Spinner etLocalidad;
        CardView carDomicilio, cardCorreo, cardTelefono;
        myDialog.setContentView(R.layout.editar_domicilio);
        myDialog.setCancelable(false);
        tvCancelar=(Button)myDialog.findViewById(R.id.tvCancelar);
        btGuardar = (Button) myDialog.findViewById(R.id.btGuardar);
        etCalle = (EditText) myDialog.findViewById(R.id.etCalle);
        etNumero = (EditText) myDialog.findViewById(R.id.etNumero);
        etColonia = (AutoCompleteTextView) myDialog.findViewById(R.id.etColonia);
        etInterior = (EditText) myDialog.findViewById(R.id.etInterior);
        etCorreo=(EditText)myDialog.findViewById(R.id.etCorreo);
        etTelefono=(EditText)myDialog.findViewById(R.id.etTelefono);
        etLocalidad=(Spinner) myDialog.findViewById(R.id.etLocalidad);
        etEntreCalles=(EditText)myDialog.findViewById(R.id.etEntreCalles);
        carDomicilio=(CardView) myDialog.findViewById(R.id.cardDomicilio);
        layoutGuardado=(LinearLayout) myDialog.findViewById(R.id.layoutGuardado);
        cbDireccion=(CheckBox)myDialog.findViewById(R.id.cbDireccion);
        cbTelefono=(CheckBox)myDialog .findViewById(R.id.cbTelefono);
        cbCorreo=(CheckBox) myDialog.findViewById(R.id.cbCorreo);
        layout1=(LinearLayout) myDialog.findViewById(R.id.layout1);
        String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        //final boolean[] bandera = {false};

        etColonia.setThreshold(1);
        ArrayAdapter<String> adapterColonias = new ArrayAdapter<String>(mainActivity, android.R.layout.simple_dropdown_item_1line, llenarCamposDeColonias());
        etColonia.setAdapter(adapterColonias);

        etColonia.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                coloniaSelecionada = null;
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });


        etColonia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                coloniaSelecionada = adapterView.getItemAtPosition(i).toString();

                etLocalidad.setVisibility(View.VISIBLE);
                ArrayList<String> LOCALIDADES = new ArrayList<String>();
                LOCALIDADES.add("Selecciona una localidad");
                String query="SELECT * FROM LocalidadColonia where colonia = '" + coloniaSelecionada + "'group by localidad"; //buscamos y agrupamos ciudades
                List<Localidad_Colonia> listaLocalidades = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query);
                if(listaLocalidades.size()>0)
                {
                    if(listaLocalidades.size()==1)
                    {
                        soloHayUnaPosicion[0] = true;
                        LOCALIDADES.clear();
                        LOCALIDADES.add(listaLocalidades.get(0).getLocalidad());
                        ArrayAdapter<String> adapterLocalidades = new ArrayAdapter<String>(mainActivity, android.R.layout.simple_list_item_1, LOCALIDADES);
                        etLocalidad.setAdapter(adapterLocalidades);
                    }
                    else {
                        soloHayUnaPosicion[0] = false;
                        for (int t = 0; t < listaLocalidades.size(); t++) {
                            try {
                                LOCALIDADES.add(listaLocalidades.get(t).getLocalidad());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        ArrayAdapter<String> adapterLocalidades = new ArrayAdapter<String>(mainActivity, android.R.layout.simple_list_item_1, LOCALIDADES);
                        etLocalidad.setAdapter(adapterLocalidades);
                    }
                }
                else
                {
                    Log.i("SIN REGISTROS", "SIN REGISTROS EN LOCALIDADES");
                }

            }
        });

        tvCancelar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

               // if(dialogoCancelable)
                //    myDialog.dismiss();
                //else


                if(bloqueo){
                //Toast.makeText(mainActivity, "No puedes cancelar, termina de actualizar los datos.", Toast.LENGTH_SHORT).show();
                    myDialog.dismiss();
                mthis = null;
                pendingCobro = false;
                FragmentManager fm = getFragmentManager();
                fm = null;

                if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                    fm.popBackStack();
                } else if (fm == null && isAdded()) {
                    fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                    fm.popBackStack();
                }
                getFragmentManager().popBackStack();}
                else
                    myDialog.dismiss();
            }
        });

        cbDireccion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    String sintax = "SELECT * FROM CLIENTS WHERE id_Contract = '" + idContrato + "'";
                    List<Clients> listClients = Clients.findWithQuery(Clients.class, sintax);
                    if(listClients.size()>0)
                    {
                        String idContract = listClients.get(0).getIdContract();
                        //String locality = listClients.get(0).getLocality();
                        String streetToPay = listClients.get(0).getStreetToPay();
                        String numberExt = listClients.get(0).getNumberExt();
                        String betweenStreets = listClients.get(0).getBetweenStreets();
                        //String neighborhood = listClients.get(0).getNeighborhood();
                        String phone = listClients.get(0).getTelefono();
                        String correo = listClients.get(0).getEmail();
                        String interior = listClients.get(0).getNumerointerior();

                        etCalle.setText(streetToPay);
                        etNumero.setText(numberExt);
                        //etColonia.setText(neighborhood);
                        //etLocalidad.setText(locality);
                        etEntreCalles.setText(betweenStreets);
                        etInterior.setText(interior);

                    }
                }
                else
                {
                    etCalle.setText("");
                    etNumero.setText("");
                    //etColonia.setText("");
                    //etLocalidad.setText("");
                    etEntreCalles.setText("");
                    etInterior.setText("");
                }
            }
        });

        cbCorreo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    String sintax = "SELECT * FROM CLIENTS WHERE id_Contract = '" + idContrato + "'";
                    List<Clients> listClients = Clients.findWithQuery(Clients.class, sintax);
                    if(listClients.size()>0)
                    {
                        String correo = listClients.get(0).getEmail();
                        etCorreo.setText(correo);
                    }
                }
                else
                {
                    etCorreo.setText("");
                }
            }
        });

        cbTelefono.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    String sintax = "SELECT * FROM CLIENTS WHERE id_Contract = '" + idContrato + "'";
                    List<Clients> listClients = Clients.findWithQuery(Clients.class, sintax);
                    if(listClients.size()>0)
                    {
                        String phone = listClients.get(0).getTelefono();
                        if(phone.equals("Sin teléfono"))
                            Toast.makeText(mainActivity, "No se encontro ningún teléfono registrado", Toast.LENGTH_LONG).show();
                        else
                            etTelefono.setText(phone);
                    }
                    else
                        Toast.makeText(mainActivity, "No se encontró teléfono.", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    etTelefono.setText("");

                }
            }
        });


        btGuardar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(coloniaSelecionada == null)
                {
                    Toast.makeText(mainActivity, "Tienes que seleccionar una colonia", Toast.LENGTH_LONG).show();
                    etColonia.setText("");
                }
                else
                {
                    if(etLocalidad.getSelectedItemPosition() == 0 && !soloHayUnaPosicion[0])
                    {
                        Toast.makeText(mainActivity, "Por favor selecciona una localidad", Toast.LENGTH_LONG).show();
                    }
                    else {
                        boolean guardarDatos = false;
                        final String validemail = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+";
                        String email = etCorreo.getText().toString();
                        Matcher matcher = Pattern.compile(validemail).matcher(email);

                        String[] arregloDatosCobrador;
                        arregloDatosCobrador = DatabaseAssistant.consultarDatosDeCobrador();
                        if (arregloDatosCobrador.length > 0) {
                            codigoCobrador = arregloDatosCobrador[3];
                        } else
                            Log.d("IMPRESION -- >", "NO SE RETORNARON DATOS");


                        if (etCalle.getText().toString().equals("")
                                || etColonia.getText().toString().equals("")
                                || etLocalidad.getSelectedItem().toString().equals("")
                                || etEntreCalles.getText().toString().equals("")
                                || etTelefono.getText().toString().equals("")
                                || etTelefono.getText().toString().equals("Sin teléfono")

                        ){
                            Toast.makeText(mainActivity, "Por favor completa los campos correctamente", Toast.LENGTH_LONG).show();
                        } else {
                            String idContract = "", locality = "", streetToPay = "", numberExt = "", betweenStreets = "", neighborhood = "", phone = "", correo = "", numeroInterior = "";
                            String sintax = "SELECT * FROM CLIENTS WHERE id_Contract = '" + idContrato + "'";
                            List<Clients> listClients = Clients.findWithQuery(Clients.class, sintax);
                            if (listClients.size() > 0) {
                                idContract = listClients.get(0).getIdContract();
                                locality = listClients.get(0).getLocality();
                                streetToPay = listClients.get(0).getStreetToPay();
                                numberExt = listClients.get(0).getNumberExt();
                                betweenStreets = listClients.get(0).getBetweenStreets();
                                neighborhood = listClients.get(0).getNeighborhood();
                                phone = listClients.get(0).getTelefono();
                                correo = listClients.get(0).getEmail();
                                numeroInterior = listClients.get(0).getNumerointerior();

                                if (cbDireccion.isChecked()) {
                                    if (etCorreo.getText().toString().length() > 0) {
                                        if (matcher.matches()) {

                                            if(etTelefono.getText().toString().length() > 0)
                                            {
                                                if(etTelefono.getText().toString().length()!=10)
                                                {
                                                    guardarDatos = false;
                                                    Toast.makeText(mainActivity, "Número de teléfono incorrecto, se necesitan 10 digitos.", Toast.LENGTH_LONG).show();
                                                }
                                                else
                                                {
                                                    guardarDatos=true;
                                                }
                                            }
                                            else
                                            {
                                                guardarDatos = false;
                                                Toast.makeText(mainActivity, "No podemos dejar el teléfono vacío.", Toast.LENGTH_LONG).show();
                                            }


                                            /*if (etTelefono.getText().toString().length() > 0)
                                            {
                                                if (etTelefono.getText().toString().length() == 10)
                                                {
                                                    guardarDatos = true;
                                                }
                                                else {
                                                    //El telefono no tiene los 10 digitos
                                                    if (etTelefono.getText().toString().equals("Sin teléfono")) {
                                                        guardarDatos = false;
                                                    } else {
                                                        if (etTelefono.getText().toString().equals("Sin teléfono")) {
                                                            guardarDatos = false;
                                                        } else {
                                                            guardarDatos = false;
                                                            Toast.makeText(mainActivity, "Número de teléfono incorrecto, se necesitan 10 digitos.", Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                }
                                            } else {
                                                guardarDatos = true;
                                            }*/
                                        } else {
                                            guardarDatos = false;
                                            //Correo no coincide
                                            Toast.makeText(mainActivity, "Correo invalido, verifica nuevamente.", Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                        //verificar  que el telefono sean 10 digitos
                                        //Correo vacio
                                        if (etTelefono.getText().toString().length() > 0) {
                                            if (etTelefono.getText().toString().length() == 10) {
                                                guardarDatos = true;
                                            } else {
                                                if (etTelefono.getText().toString().equals("Sin teléfono")) {
                                                    guardarDatos = true;
                                                } else {
                                                    guardarDatos = false;
                                                    Toast.makeText(mainActivity, "Ingresa un número valido de 10 dígitos", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        } else
                                            guardarDatos = true;
                                    }
                                } else {
                                    guardarDatos = true;
                                    //Direccion NO seleccionada
                                }
                            } else {
                                Log.d("SIN DATOS DE --->", "CLIENTES");
                            }

                            if (guardarDatos) {
                                try {

                                } catch (Exception ex) {
                                    Log.w("guardarDatosMostrarDom", "Ocurrio un error en OnClick, datos incorrectos por el usuario: " + ex.toString());
                                }
                                DatabaseAssistant.insertDatosActualizados(
                                        codigoCobrador,
                                        idContract,
                                        etCalle.getText().toString().equals("") ? streetToPay : etCalle.getText().toString(),
                                        etNumero.getText().toString().equals("") ? numberExt : etNumero.getText().toString(),
                                        etColonia.getText().toString().equals("") ? neighborhood : etColonia.getText().toString(),
                                        etLocalidad.getSelectedItem().toString().equals("") ? locality : etLocalidad.getSelectedItem().toString(),
                                        etEntreCalles.getText().toString().equals("") ? betweenStreets : etEntreCalles.getText().toString(),
                                        etCorreo.getText().toString().equals("") ? correo : etCorreo.getText().toString(),
                                        etTelefono.getText().toString().equals("") || etTelefono.getText().toString().equals("Sin teléfono") ? phone : etTelefono.getText().toString(),
                                        0,
                                        etInterior.getText().toString().equals("") ? numeroInterior : etInterior.getText().toString());

                                layoutGuardado.setVisibility(View.VISIBLE);
                                layout1.setEnabled(false);

                                finalizar_actualizarDatos(
                                        etCalle.getText().toString().equals("") ? streetToPay : etCalle.getText().toString(),
                                        etNumero.getText().toString().equals("") ? numberExt : etNumero.getText().toString(),
                                        etColonia.getText().toString().equals("") ? neighborhood : etColonia.getText().toString(),
                                        etLocalidad.getSelectedItem().toString().equals("") ? locality : etLocalidad.getSelectedItem().toString(),
                                        etTelefono.getText().toString().equals("") ? phone : etTelefono.getText().toString(),
                                        etEntreCalles.getText().toString().equals("") ? betweenStreets : etEntreCalles.getText().toString(),
                                        fecha,
                                        etCorreo.getText().toString().equals("") ? correo : etCorreo.getText().toString(),
                                        etInterior.getText().toString().equals("") ? numeroInterior : etInterior.getText().toString(),
                                        idContrato);
                            }

                        }
                    }
                }


            }
        });

        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }

    private ArrayList<String> llenarCamposDeLocalidades(String colonia)
    {
        ArrayList<String> LOCALIDADES = new ArrayList<String>();
        //COLONIAS = new ArrayList<String>();
        String query="SELECT * FROM LocalidadColonia where colonia = '" + colonia + "'group by localidad"; //buscamos y agrupamos ciudades
        List<Localidad_Colonia> listaLocalidades = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query);
        if(listaLocalidades.size()>0)
        {
            if(listaLocalidades.size()==1)
            {
                return LOCALIDADES;
            }
            else {
                for (int i = 0; i < listaLocalidades.size(); i++) {
                    try {
                        LOCALIDADES.add(listaLocalidades.get(i).getLocalidad());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else
        {
           Log.i("SIN REGISTROS", "SIN REGISTROS EN LOCALIDADES");
        }

        return LOCALIDADES;
    }
    private ArrayList<String> llenarCamposDeColonias()
    {
        ArrayList<String> COLONIAS = new ArrayList<String>();
        COLONIAS = new ArrayList<String>();
        String query2="SELECT * FROM LocalidadColonia"; //buscamos y agrupamos colonias
        List<Localidad_Colonia> listaLocalidades2 = Localidad_Colonia.findWithQuery(Localidad_Colonia.class, query2);
        if(listaLocalidades2.size()>0)
        {
            for (int i = 0; i < listaLocalidades2.size(); i++)
            {
                try
                {
                    COLONIAS.add(listaLocalidades2.get(i).getColonia());
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        else
        {
            Log.i("SIN REGISTROS", "SIN REGISTROS EN LOCALIDADES");
        }
        return COLONIAS;
    }




    private void verificarFechasde3Meses(String idCont)
    {
        String fechaActualizacion = "", rango_de_dias_de_aviso = "", mostrar_actualizar_datos="";
        String arregloDatosCobrador[] = DatabaseAssistant.consultarDatosDeCobrador();
        String[] arregloEmpresasExcluidas =     empresasExcluidas(arregloDatosCobrador[9]);
        if(arregloDatosCobrador.length>0)
        {
            rango_de_dias_de_aviso = arregloDatosCobrador[5];

            String querX = "SELECT * FROM CLIENTS WHERE id_Contract = '" + idContrato + "' and lastupdate >= datetime('now', '-" + Integer.parseInt(rango_de_dias_de_aviso) + " day')";
            List<Clients> lista = Clients.findWithQuery(Clients.class, querX);
            if (lista.size() > 0)
            {
                Util.Log.d("MENSAJE --->" + "SI ESTA ACTUALIZADO");
            } else
                {
                    String queryClientsForKnowsCompany = "SELECT * FROM CLIENTS WHERE id_Contract = '" + idContrato + "'";
                    List<Clients> listClientWithCompany = Clients.findWithQuery(Clients.class, queryClientsForKnowsCompany);
                    if (listClientWithCompany.size() > 0) {

                        if(listClientWithCompany.get(0).getTypeBD().equals(arregloEmpresasExcluidas[0])
                               || listClientWithCompany.get(0).getTypeBD().equals(arregloEmpresasExcluidas[1])
                               || listClientWithCompany.get(0).getTypeBD().equals(arregloEmpresasExcluidas[2])
                               || listClientWithCompany.get(0).getTypeBD().equals(arregloEmpresasExcluidas[3]))
                       {
                           Log.v("EMPRESA EXCLUIDA", "Se excluyo la actualizacion de datos de este contrato");
                       }
                       else
                       {
                           mostrarDesactualizado(etAmount, no_contrato);
                       }
                    }
                    else
                        Log.e("PRACTICAMENTE","no entraria aqui, porque si encontrara el contrato");
                }
        }
        else
        {
            Log.d("IMPRESION -- >", "NO SE RETORNARON DATOS");
        }
    }

    public String[] empresasExcluidas(String cadenaCompletaDeEmpresasExcluidas)
    {
        String [] arregloEmpresasExcluidas = new String[4];
        arregloEmpresasExcluidas[0] = "";
        arregloEmpresasExcluidas[1] = "";
        arregloEmpresasExcluidas[2] = "";
        arregloEmpresasExcluidas[3] = "";

        externo:
        for(int posicionDeArreglo=0;posicionDeArreglo<=3;)
        {
            for(int i=0;i<cadenaCompletaDeEmpresasExcluidas.length();i++)
            {
                if(cadenaCompletaDeEmpresasExcluidas.charAt(i)=='*')
                {
                    break externo;
                }
                else if (cadenaCompletaDeEmpresasExcluidas.charAt(i)==',')
                {
                    posicionDeArreglo++;
                }
                else
                {
                    arregloEmpresasExcluidas[posicionDeArreglo] =  arregloEmpresasExcluidas[posicionDeArreglo] + cadenaCompletaDeEmpresasExcluidas.charAt(i);
                }
            }
        }
        return arregloEmpresasExcluidas;
    }


    private void mostrarDesactualizado(View view, String contrato)
    {
        Button btActualizar, btSalir;
        TextView tvContrato;
        myDialog2.setContentView(R.layout.aviso_desactualizado);
        if(dialogoCancelable)
            myDialog2.setCancelable(true);
            else
            myDialog2.setCancelable(false);

        btActualizar = (Button) myDialog2.findViewById(R.id.btActualizar);
        btSalir= (Button) myDialog2.findViewById(R.id.btSalir);
        tvContrato = (TextView) myDialog2.findViewById(R.id.tvContrato);
        tvContrato.setText("Contrato: " + serie + contrato);

        btSalir.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                FragmentManager fm = getFragmentManager();
                fm = null;
                if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                    fm.popBackStack();
                } else if (fm == null && isAdded()) {
                    fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                    fm.popBackStack();
                }
                myDialog2.dismiss();
            }
        });

        btActualizar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog2.dismiss();
                mostrarDomicilio(v, true);
            }
        });

        myDialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog2.show();
    }



    public void verificarCreditoPercapita(String contratillo)
    {
        String numeroPagos = "", saldo = "", montoPagoActual = "", mensaje = "", montoConcatenado = "", tituloPercapita = "", mensajeCobradorPercapita = "";
        String saldoAux="", montoAux="";//, mostrar_actualizar_datos="";
        String arregloDatosCobrador[] = DatabaseAssistant.consultarDatosDeCobrador();
        try
        {
            if(arregloDatosCobrador.length>0)
            {
                mensaje = arregloDatosCobrador[0];
                tituloPercapita = arregloDatosCobrador[1];
                mensajeCobradorPercapita = arregloDatosCobrador[2];
                numeroPagos =arregloDatosCobrador[6];

                if(numeroPagos.equals("vacio"))
                {
                    Log.d("IMPRESION --->", "Vacio");
                }
                else
                {
                    List<Clients> listaContratos = Clients.findWithQuery(Clients.class, "SELECT * FROM CLIENTS WHERE id_Contract= '" + contratillo + "'");
                    if (listaContratos.size() > 0)
                    {
                        saldo = listaContratos.get(0).getBalance();
                        montoPagoActual = listaContratos.get(0).getActualPayment();


                        int montillo = (int) Integer.parseInt(montoPagoActual.replace(".00", "").replace(".0", ""));
                        String saldoaAux = saldo.substring( 0, saldo.lastIndexOf("."));
                        int saldillo = Integer.parseInt(saldoaAux);
                        int diasResultado = saldillo / montillo;
                        if (diasResultado <= Integer.parseInt(numeroPagos))
                        {
                            try {
                                if(jsonDatos.getString("id_grupo_base").equals("05"))
                                {
                                    Log.d("IMPRESION --->", "CONTRATO CON EMPRESA 05");
                                    verificarFechasde3Meses(idContrato);
                                }
                                else
                                {
                                    mostrarCreditoPercapita(tvAceptar, mensaje, mensajeCobradorPercapita, tituloPercapita);
                                    mensajePercapita = mensaje;
                                    tituloPercapitaGeneral = tituloPercapita;
                                }

                            }catch (JSONException e)
                            {
                                e.printStackTrace();
                            }


                        }else
                        {
                            verificarFechasde3Meses(contratillo);
                            Log.d("IMPRESION --->", "TODAVIA NO ALCANZA EL CREDITO DE PERCAPITA");
                        }


                    } else {
                        Log.d("IMPRESION --->", "NO SE ENCONTRARON DATOS DEL CLIENTE");
                        noExisteInformacionDelCliente();
                    }
                }
            }
            else
            {
                Log.d("IMPRESION -- >", "NO SE RETORNARON DATOS");
            }
        }catch (Exception ex)
        {
            Log.w("VerificarCreditoPercap", "Ocurrio un error aqui: " + ex.toString());
        }
    }



    private void mostrarCreditoPercapita(View view, String mensaje, String mensajeCobradorPercapita, String tituloPercapita)
    {
        Button btEnterado;
        TextView tvMensaje, tvMensajeCobradorPercapita, tvTitulo;
        myDialog3.setContentView(R.layout.aviso_credito_percapita);
        myDialog3.setCancelable(false);
        btEnterado = (Button) myDialog3.findViewById(R.id.btEnterado);
        tvMensaje = (TextView) myDialog3.findViewById(R.id.tvMensaje);
        tvTitulo = (TextView) myDialog3.findViewById(R.id.tvTitulo);
        tvMensajeCobradorPercapita = (TextView) myDialog3.findViewById(R.id.tvMensajeCobradorPercapita);
        tvMensaje.setText(mensaje);
        tvMensajeCobradorPercapita.setText(mensajeCobradorPercapita);
        tvTitulo.setText(tituloPercapita);
        showMessagePercapita=true;

        btEnterado.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog3.dismiss();
                verificarFechasde3Meses(idContrato);
            }
        });

        myDialog3.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog3.show();
    }




    public void finalizar_actualizarDatos(String calle, String numero, String colonia, String localidad, String telefonos, String entreCalles, String fecha, String correos, String interior, String contrato)
    {
        dialogo=new android.support.v7.app.AlertDialog.Builder(mainActivity);
        dialogo.setTitle("¡Información!");
        dialogo.setMessage("Datos guardados correctamente");
        dialogo.setCancelable(false);
        dialogo.setPositiveButton("Listo", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                String update="UPDATE CLIENTS SET street_To_Pay = '" + calle
                        + "', number_Ext = '" + numero
                        + "', neighborhood = '" + colonia
                        + "', locality = '" + localidad
                        + "', phone = '" + telefonos
                        + "', between_Streets = '"+ entreCalles
                        + "', lastupdate = '" + fecha
                        + "', email = '" + correos
                        + "', numerointerior = '" + interior
                        +"' WHERE id_Contract='" + contrato +"'";
                String hola="";
                Clients.executeQuery(update);

                myDialog.dismiss();
                FragmentManager fm = getFragmentManager();
                fm = null;
                if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                    fm.popBackStack();
                } else if (fm == null && isAdded()) {
                    fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                    fm.popBackStack();
                }
            }
        });
        dialogo.show();
    }

    public void noExisteInformacionDelCliente()
    {
        dialogo=new android.support.v7.app.AlertDialog.Builder(mainActivity);
        dialogo.setTitle("¡Información!");
        dialogo.setMessage("Uuups, parece que no existe información del cliente.");
        dialogo.setCancelable(false);
        dialogo.setPositiveButton("Listo", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                myDialog.dismiss();
                FragmentManager fm = getFragmentManager();
                fm = null;
                if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CLIENTE_DETALLES)) {
                    fm.popBackStack();
                } else if (fm == null && isAdded()) {
                    fm = ((AppCompatActivity) getActivity()).getSupportFragmentManager();
                    fm.popBackStack();
                }
            }
        });
        dialogo.show();
    }





    //*********************************************************************

    public void mostrarAvisoSuspencionTemporalDePagos(View view)
    {

        final Button btCancelar, btImprimir;
        EditText etMotivo, etFecha;
        ImageView imgCalendario;
        Spinner spLista;
        ArrayList<String> esquemas;

        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getContext());
        mBottomSheetDialog.setContentView(R.layout.dialog_aviso_suspencion);

        //dialogo_suspencion.setContentView(R.layout.dialog_aviso_suspencion);
        //dialogo_suspencion.setCancelable(false);
        btCancelar = (Button) mBottomSheetDialog.findViewById(R.id.btCancelar);
        btImprimir = (Button) mBottomSheetDialog.findViewById(R.id.btImprimir);
        etMotivo=(EditText) mBottomSheetDialog.findViewById(R.id.etMotivo);
        etFecha=(EditText)mBottomSheetDialog.findViewById(R.id.etFecha);
        spLista=(Spinner)mBottomSheetDialog.findViewById(R.id.spLista);
        imgCalendario=(ImageView) mBottomSheetDialog.findViewById(R.id.imgCalendario);

        esquemas = new ArrayList<String>();
        esquemas.add("Motivo de suspensión...");
        List<Suspensionitem> lista = Suspensionitem.listAll(Suspensionitem.class);
        if(lista.size()>0) {
            for (int i = 0; i < lista.size(); i++) {
                try {
                    esquemas.add(lista.get(i).getDescripcion().toUpperCase());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            Snackbar.make(spLista, "Registros menores a 0", Snackbar.LENGTH_LONG).show();
        }

        ArrayAdapter<CharSequence> adaptador = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, esquemas);
        spLista.setAdapter(adaptador);



        /*calendario.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                etFecha.setText(dayOfMonth+"/"+(month+1)+"/"+year);
            }
        });*/

        btCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                mBottomSheetDialog.dismiss();
            }
        });






        btImprimir.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (etFecha.getText().toString().equals(""))
                {
                    Toast.makeText(mainActivity, "Completa los campos correctamente", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    //String fechaReanudacion=diia+"/"+mess+"/"+ano;
                    String fechaReanudacion=etFecha.getText().toString();
                    String[] arregloDatosCliente = DatabaseAssistant.consultaDatosCliente(idContrato);
                    if(arregloDatosCliente.length>0)
                    {
                        String nombreCompletoCliente=arregloDatosCliente[1]+" "+ arregloDatosCliente[2]+ " "+ arregloDatosCliente[3];
                        String serialAndContract=arregloDatosCliente[7] +arregloDatosCliente[5];

                        String sintax = "SELECT * FROM NOTICE";
                        List<Notice> listNotice = Notice.findWithQuery(Notice.class, sintax);
                        if(listNotice.size()>0)
                        {
                            String tiketSuspencionTEXTO = listNotice.get(0).getTiketsuspencion();

                            try
                            {
                                String nombreCobrador= datosCobrador.getString("nombre");
                                String saldo = jsonDatos.getString("saldo");
                                String fechaUltimoAbono= jsonDatos.getString("fecha_ultimo_abono");
                                String abonado= jsonDatos.getString("abono");

                                if(spLista.getSelectedItemPosition()==0)
                                {
                                    Toast.makeText(mainActivity, "SELECCIONA UNA OPCIÓN", Toast.LENGTH_LONG).show();
                                }
                                else
                                {
                                    Date fechaSeleccionada = new Date();
                                    //validacion de las fechas anteriores
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                                    Date dateActual = new Date();
                                    dateFormat.format(dateActual);
                                    System.out.println(dateActual);
                                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
                                    try {
                                        fechaSeleccionada= dateFormatter.parse(fechaReanudacion);
                                    }catch (ParseException e)
                                    {
                                        e.printStackTrace();
                                    }

                                    if (fechaSeleccionada.compareTo(dateActual)<0)
                                    {
                                        Log.d("NO ENTRO", "Fecha seleccionada es menor a la actual");
                                        Toast.makeText(mainActivity, "Debes seleccionar una fecha posterior a la actual.", Toast.LENGTH_SHORT).show();
                                        fechaReanudacion="";
                                        diia=0;
                                        mess=0;
                                        ano=0;
                                        etFecha.setText("");

                                    }
                                    else{
                                        Log.d("ENTRO", "Entro");
                                        //Toast.makeText(mainActivity, "SI ENTRO", Toast.LENGTH_SHORT).show();
                                        PrintTemporalSuspensionNotice printTemporalSuspensionNotice = new PrintTemporalSuspensionNotice(
                                                nombreCompletoCliente,
                                                serialAndContract,
                                                spLista.getSelectedItem().toString(),
                                                fechaReanudacion,
                                                tiketSuspencionTEXTO,
                                                nombreCobrador,
                                                saldo,
                                                fechaUltimoAbono,
                                                abonado);

                                        spicePrintAccountStatusTicket.executeRequest(printTemporalSuspensionNotice);
                                        mBottomSheetDialog.dismiss();
                                    }
                                }
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }

                        }
                    }
                    else
                        Log.d("IMPRESION -- >", "NO SE RETORNARON DATOS");
                }
            }
        });




        spLista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                opcionSeleccionada=spLista.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        etFecha.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(mainActivity, android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListener, year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;

                String date = dayOfMonth + "/" + month + "/" + year;
                etFecha.setText(date);
            }
        };

        mBottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mBottomSheetDialog.show();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        //super.onSaveInstanceState(outState);
        outState.putString("closeFrame", TAG);
        super.onSaveInstanceState(outState);
    }


}





















