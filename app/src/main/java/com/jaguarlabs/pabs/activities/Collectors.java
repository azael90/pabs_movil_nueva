package com.jaguarlabs.pabs.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.google.gson.Gson;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.CollectorsAdapter;
import com.jaguarlabs.pabs.database.Collector;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.rest.SpiceHelper;
import com.jaguarlabs.pabs.rest.models.ModelGetCollectorsRequest;
import com.jaguarlabs.pabs.rest.models.ModelGetCollectorsResponse;
import com.jaguarlabs.pabs.rest.request.GetCollectorsRequest;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by jordan on 26/08/16.
 */
@SuppressWarnings("SpellCheckingInspection")
public class Collectors extends ExtendedActivity implements CollectorsAdapter.OnCollectorClickListener{

    private List<Collector> collectorsList;
    private JSONObject datosCobrador;
    private boolean updateCashInfo;
    private boolean isConnected;
    private String collectorType;
    private SpiceManager spiceManager = getSpiceManager();
    private SpiceHelper<ModelGetCollectorsResponse> spiceGetCollectors;
    private ListView lvCollectors;
    private CollectorsAdapter collectorsAdapter;
    private CollectorsAdapter.OnCollectorClickListener listener;
    private PullRefreshLayout pullRefreshLayout;

    @Override
    public void onStart() {
        super.onStart();

        pullRefreshLayout.setRefreshing(true);
        if (!DatabaseAssistant.isThereAnyCollector()) {
            executeGetContracts();
        }
        else {
            this.collectorsList = DatabaseAssistant.getCollectorsBySupervisor();
            fillList();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        spiceGetCollectors = new SpiceHelper<ModelGetCollectorsResponse>(spiceManager, "getCollectos", ModelGetCollectorsResponse.class, DurationInMillis.ONE_HOUR) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                fail();
            }

            @Override
            protected void onSuccess(ModelGetCollectorsResponse modelGetCollectorsResponse) {
                super.onSuccess(modelGetCollectorsResponse);
                Util.Log.ih("request finished");
                pullRefreshLayout.setRefreshing(false);
                fillDatabase(modelGetCollectorsResponse.getCollectors());
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                super.onProgressUpdate(progress);
                progressUpdate(progress);
            }
        };
    }

    @Override
    protected void init() {
        super.init();

        setContentView(R.layout.activity_cobradores);

        Bundle bundle = getIntent().getExtras();
        if ( bundle != null ) {
            if (bundle.containsKey("datos_cobrador")) {
                try {
                    datosCobrador = new JSONObject(bundle.getString("datos_cobrador"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (bundle.containsKey("periodo")) {
                updateCashInfo = bundle.getBoolean("periodo", false);
            }
            if (bundle.containsKey("isConnected")) {
                isConnected = bundle.getBoolean("isConnected", false);
            }
            if (bundle.containsKey("collectorType")) {
                collectorType = bundle.getString("collectorType");
            }
        }

        setListeners();

        /*
        if (isConnected && !DatabaseAssistant.isThereAnyCollector()){
            try{
                showMyCustomDialog();
            } catch (Exception e){
                Toast toast = Toast.makeText(this, R.string.info_message_getting_ready, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                e.printStackTrace();
            }
            getListCollectors();
        }
        */

    }

    private void executeGetContracts(){
        //if (isConnected && !DatabaseAssistant.isThereAnyCollector()){
        //try{
            //pullRefreshLayout.setRefreshing(true);
            //showMyCustomDialog();
        /*} catch (Exception e){
            Toast toast = Toast.makeText(this, R.string.info_message_getting_ready, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            e.printStackTrace();
        }
        */
        //getListCollectors();
        try {
            ModelGetCollectorsRequest modelGetCollectorsRequest = new ModelGetCollectorsRequest(
                    Integer.parseInt(datosCobrador.getString("no_cobrador")), collectorType);
            GetCollectorsRequest getCollectorsRequest = new GetCollectorsRequest(modelGetCollectorsRequest);
            spiceGetCollectors.executeRequest(getCollectorsRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //}
    }

    /**
     * returns an instance of Builder
     * @return instance of Builder
     */
    private AlertDialog.Builder getBuilder(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            return new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        }
        else{
            return new AlertDialog.Builder(this);
        }
    }

    public void setListeners(){
        ImageView ivExit = (ImageView) findViewById(R.id.ivExit);
        ImageButton ibEraseText = (ImageButton) findViewById(R.id.ibEraseText);
        final EditText etSearch = (EditText) findViewById(R.id.etSearch);
        // listen refresh event
        pullRefreshLayout = (PullRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);
        pullRefreshLayout.setOnRefreshListener(this::executeGetContracts);

        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                Collectors.this.collectorsAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        ivExit.setOnClickListener(v -> {
            AlertDialog.Builder alert = getBuilder();
            alert.setMessage(R.string.info_message_close);
            alert.setPositiveButton(R.string.accept, (dialog, which) -> endSession());
            alert.setNegativeButton(R.string.cancel, null);
            alert.show();
        });

        ibEraseText.setOnClickListener(v -> etSearch.setText(""));
    }

    private void endSession(){
        Intent intent = new Intent(getApplicationContext(), Login.class);
        startActivity(intent);
        finish();
    }

    private CollectorsAdapter getCollectorsAdapter(){
        return new CollectorsAdapter(this, collectorsList);
    }

    private void fillList(){
        collectorsAdapter = getCollectorsAdapter();
        this.listener = this;
        collectorsAdapter.setOnCollectorClickListener(this);

        lvCollectors = (ListView) findViewById(R.id.lvCollectors);
        lvCollectors.setAdapter(collectorsAdapter);
        lvCollectors.setDivider(getResources().getDrawable(R.drawable.divider_list_clientes));

        pullRefreshLayout.setRefreshing(false);

        //dismissMyCustomDialog();
    }

    private void fillDatabase(List<ModelGetCollectorsResponse.SingleCollector> collectors){
        DatabaseAssistant.deleteCollectorsBySupervisor();
        DatabaseAssistant.insertCollectorsBySupervisor(collectors);
        this.collectorsList = DatabaseAssistant.getCollectorsBySupervisor();

        fillList();
    }


    private void fail(){
        //dismissMyCustomDialog();
        toastL(R.string.error_message_network_call);
        pullRefreshLayout.setRefreshing(false);
    }

    private void progressUpdate(RequestProgress progress){
        Util.Log.ih("inside progressUpdate method");
        Util.Log.ih("(int) progress.getProgress() = " + (int) progress.getProgress());
        switch ( (int) progress.getProgress() ){
            case ConstantsPabs.START_REQUEST:
                Util.Log.ih("case START_REQUEST");
                toastS(R.string.info_message_getting_ready);
                showMyCustomDialog();
                break;
            case ConstantsPabs.FINISHED_REQUEST:
                Util.Log.ih("case FINISHED_REQUEST");
                dismissMyCustomDialog();
                break;
            default:
                Util.Log.ih("default");
                //nothing
        }
    }

    /**
     * shows framelayout as progress dialog
     */
    private void showMyCustomDialog(){
        final FrameLayout flLoading = (FrameLayout) Collectors.this.findViewById(R.id.flLoading);
        flLoading.setVisibility(View.VISIBLE);
    }

    /**
     * hides framelayout used as progress dialog
     */
    private void dismissMyCustomDialog(){
        final FrameLayout flLoading = (FrameLayout) Collectors.this.findViewById(R.id.flLoading);
        flLoading.setVisibility(View.GONE);
    }

    Collector selectedCollector;

    @Override
    public void onCollectorClick(Collector collector) {

        this.selectedCollector = collector;

        Util.Log.ih("name clicked = " + collector.getName());
        Util.Log.ih("code clicked = " + collector.getCode());



        SharedPreferences.Editor preferencesSplash = getSharedPreferences(
                "PABS_Login", Context.MODE_PRIVATE).edit();
        preferencesSplash.putString("clientsCollectorNumber", selectedCollector.getCollectorNumber());
        preferencesSplash.apply();





        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(((EditText) findViewById(R.id.etSearch)).getWindowToken(), 0);


        AlertDialog.Builder alert = getBuilder();
        alert.setMessage("Cobrador seleccionado:\n" + collector.getName() + "\nCódigo: " + collector.getCode());
        alert.setPositiveButton(R.string.accept, (dialog, which) -> {
            Gson gson = new Gson();
            JSONObject jsonObject = null;

            //try {
            if (collectorsList != null && collectorsList.size() > 0) {
                //jsonObject = new JSONObject( gson.toJson(selectedCollector) );

                Intent intent = new Intent();
                intent.putExtra("datos_cobrador", datosCobrador.toString());
                intent.putExtra("periodo", updateCashInfo);
                intent.putExtra("isConnected", isConnected);
                intent.putExtra("isSupervisorOrSubstitute", true);
                intent.putExtra("clientsCollectorNumber", selectedCollector.getCollectorNumber());
                intent.setClass(ApplicationResourcesProvider.getContext(), MainActivity.class);

                startActivity(intent);
                finish();

            }
            //} catch (JSONException e) {
            //    e.printStackTrace();
            //}
        });
        alert.setNegativeButton(R.string.cancel, null);
        alert.show();


        /*
        Gson gson = new Gson();
        JSONObject jsonObject = null;

        try {
            if (collectorsList != null && collectorsList.size() > 0) {
                jsonObject = new JSONObject( gson.toJson(collector) );

                Intent intent = new Intent();
                intent.putExtra("datos_cobrador", datosCobrador.toString());
                intent.putExtra("periodo", updateCashInfo);
                intent.putExtra("isConnected", isConnected);
                intent.putExtra("isSupervisor", true);
                intent.putExtra("supervisorCollectorNumber", collector.getCollectorNumber());
                intent.setClass(ApplicationResourcesProvider.getContext(), Clientes.class);

                startActivity(intent);
                finish();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        */

    }
}
