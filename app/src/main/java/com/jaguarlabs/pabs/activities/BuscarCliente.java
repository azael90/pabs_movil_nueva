package com.jaguarlabs.pabs.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.BuscarClientesAdapter;
import com.jaguarlabs.pabs.net.NetHelper;
import com.jaguarlabs.pabs.net.NetHelper.onWorkCompleteListener;
import com.jaguarlabs.pabs.net.NetService;
import com.jaguarlabs.pabs.rest.SpiceHelper;
import com.jaguarlabs.pabs.rest.models.ModelSearchForClientRequest;
import com.jaguarlabs.pabs.rest.models.ModelSearchForClientResponse;
import com.jaguarlabs.pabs.rest.models.ModelSemaforoResponse;
import com.jaguarlabs.pabs.rest.request.SearchForClientRequest;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.RequestProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * This class is in charge of searching on the server for a given name or contract number
 * of a client using a Web Service. Manages to show all clients related to the info
 * given on the fields and returning all info of client selected to Aportacion screen.
 * Only shows 500 clients as max, so the more specific, the more accurate.
 */
public class BuscarCliente extends ExtendedActivity implements OnItemClickListener {

	private static BuscarCliente mthis = null;;
	private String nombre;
	private String contrato;
	private JSONArray arrayClientes;

	private List<ModelSearchForClientResponse.ContractsFound> contractsFound;

	private SpiceManager spiceManager = getSpiceManager();
	private SpiceHelper<ModelSearchForClientResponse> spiceSearchForClient;

	private LinearLayout buscarClientesLayout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		spiceSearchForClient = new SpiceHelper<ModelSearchForClientResponse>(spiceManager, "searchForClient", ModelSearchForClientResponse.class, DurationInMillis.ONE_HOUR) {
			@Override
			protected void onFail(SpiceException exception) {
				super.onFail(exception);
				fail();
			}

			@Override
			protected void onSuccess(ModelSearchForClientResponse modelSearchForClientResponse) {
				try {
					super.onSuccess(modelSearchForClientResponse);
					//Util.Log.ih("result = " + modelSearchForClientResponse.getResult().toString());
					//Util.Log.ih("result.size = " + modelSearchForClientResponse.getResult().size());
					fillList(modelSearchForClientResponse);
				} catch (NullPointerException e) {
					e.printStackTrace();
					fail();
				}
			}

			@Override
			protected void onProgressUpdate(RequestProgress progress) {
				super.onProgressUpdate(progress);
				progressUpdate(progress);
			}
		};
	}

	@Override
	protected void init() {
		super.init();
		setContentView(R.layout.activity_buscar_cliente);

		buscarClientesLayout = findViewById(R.id.buscarClienteLayout);

		hasActiveInternetConnection(this, buscarClientesLayout);
		mthis = this;
		setEstoyLogin(false);
		//Toast.makeText(ApplicationResourcesProvider.getContext(), "Buscando clientes", Toast.LENGTH_SHORT).show();
		Snackbar.make(buscarClientesLayout, "Buscando clientes", Snackbar.LENGTH_SHORT).show();
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			if (bundle.containsKey("nombre")) {
				nombre = bundle.getString("nombre");
			}
			
			if (bundle.containsKey("contrato")) {
				contrato = bundle.getString("contrato");
			}
		}
	}

	private void fail(){
		dismissMyCustomDialog();
		toastL(R.string.error_message_call);
		finish();
	}

	private void progressUpdate(RequestProgress progress){
		Util.Log.ih("inside progressUpdate method");
		Util.Log.ih("(int) progress.getProgress() = " + (int) progress.getProgress());
		switch ( (int) progress.getProgress() ){
			case ConstantsPabs.START_PRINTING:
				Util.Log.ih("case START_PRINTING");
				toastS(R.string.info_message_searching_for_clients);
				showMyCustomDialog();
				break;
			case ConstantsPabs.FINISHED_PRINTING:
				Util.Log.ih("case FINISHED_PRINTING");
				dismissMyCustomDialog();
				break;
			default:
				Util.Log.ih("default");
				//nothing
		}
	}

	/**
	 * shows framelayout as progress dialog
	 */
	private void showMyCustomDialog(){
		final FrameLayout flLoading = (FrameLayout) BuscarCliente.this.findViewById(R.id.flLoading);
		flLoading.setVisibility(View.VISIBLE);
	}

	/**
	 * hides framelayout used as progress dialog
	 */
	private void dismissMyCustomDialog(){
		final FrameLayout flLoading = (FrameLayout) BuscarCliente.this.findViewById(R.id.flLoading);
		flLoading.setVisibility(View.GONE);
	}

	/**
	 * sends to server info given and waits for result
	 *
	 * Web Service
	 * 'http://50.62.56.182/ecobro/controlcartera/searchClient'
	 * JSONObject param
	 * {
	 *     "nombre": "",
	 *     "contrato": ""
	 * }
	 */
	private void buscarClientes(){


		ModelSearchForClientRequest modelSearchForClientRequest = new ModelSearchForClientRequest(nombre, contrato);
		SearchForClientRequest searchForClientRequest = new SearchForClientRequest(modelSearchForClientRequest);
		searchForClientRequest.setPriority(SpiceRequest.PRIORITY_HIGH);
		spiceSearchForClient.executeRequest(searchForClientRequest);



		/*

		JSONObject json = new JSONObject();
		try{
			json.put("nombre", nombre);
			json.put("contrato", contrato);
		}catch(Exception e){
			log("postException",""+e);
		}
		NetHelper.getInstance().ServiceExecuter(new NetService(ConstantsPabs.urlBuscarClientes, json), this, new onWorkCompleteListener() {

			@Override
			public void onError(Exception e) {

			}

			@Override
			public void onCompletion(String result) {
				try {
					manage_BuscarClientes(new JSONObject(result));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
		*/

	}
	@Override
	public void onUserInteraction() {
		View refMain = null;
		if (Clientes.getMthis() != null) {
			//refMain = Clientes.getMthis().getView().findViewById(R.id.listView_clientes);
			if (Clientes.getMthis().getView() != null)
				refMain = Clientes.getMthis().getView().findViewById(R.id.rv_clientes);
		}
		
		if (refMain != null) {
			try {
				Clientes.getMthis().llamarTimerInactividad();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
    public void onStart() {
		super.onStart();
		buscarClientes();
	}
	
	@Override
    public void onStop() {
		super.onStop();
	}
	
	public void onClickFromXML(View v) {
		//when click on the 'go back' image
		//finishes this activity
		switch (v.getId()) {
		case R.id.btn_back:
			mthis = null;
			finish();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			mthis = null;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
    public void onDestroy() {
		super.onDestroy();
	}
	
	@Override
    public void onResume() {
		super.onResume();
		mthis = this;
	}

	public void fillList(ModelSearchForClientResponse result){
		if (result.getResult() != null){
			List<ModelSearchForClientResponse.ContractsFound> contractsFound = result.getResult();
			if (contractsFound.size() > 0) {
				this.contractsFound = contractsFound;
				BuscarClientesAdapter buscarClientesAdapter = new BuscarClientesAdapter(this, contractsFound);
				ListView lvSearchForClients = (ListView)findViewById(R.id.lv_buscar_clientes);
				lvSearchForClients.setAdapter(buscarClientesAdapter);
				lvSearchForClients.setOnItemClickListener(this);
			} else {
				AlertDialog.Builder alert = getAlertDialogBuilder();
				alert.setMessage("No se encontraron coincidencias.");
				alert.setPositiveButton("Ok", (dialogInterface, i) -> {
					mthis = null;
					finish();
				});

			}
		} else {
			try {
				AlertDialog.Builder alert = getAlertDialogBuilder();
				alert.setMessage("No se encontraron coincidencias.");
				alert.setPositiveButton("Ok", (dialogInterface, i) -> {
					mthis = null;
					finish();
				});
				alert.create().show();
			} catch (NullPointerException e){
				e.printStackTrace();
				mthis = null;
				finish();
			} catch (Exception e){
				e.printStackTrace();
				mthis = null;
				finish();
			}
		}
	}

	/**
	 * manages to process response of Web Service,
	 * showing all coincidences of the result of the search on screen,
	 * if nothing matches to given name or contract number, shows dialog
	 * with error
	 * @param json response of Web Service
	 */
	public void manage_BuscarClientes(JSONObject json) {
		if (json.has("result")) {
			try {
				arrayClientes = json.getJSONArray("result");
				if (arrayClientes.length()==500){
					Toast.makeText(ApplicationResourcesProvider.getContext(), "La búsqueda muestra solo los primeros 500 resultados, favor de ser más específico.", Toast.LENGTH_LONG).show();
				}
				if (arrayClientes.length() > 0) {
					BuscarClientesAdapter adapter = new BuscarClientesAdapter(this, arrayClientes);
					ListView lvBuscarClientes = (ListView)findViewById(R.id.lv_buscar_clientes);
					lvBuscarClientes.setAdapter(adapter);
					lvBuscarClientes.setOnItemClickListener(this);
				} else {
					AlertDialog.Builder alert = getAlertDialogBuilder();
					alert.setMessage("No se encontraron coincidencias.");
					alert.setPositiveButton("Ok", (dialogInterface, i) -> {
						mthis = null;
						finish();
					});
					
				}
				
			} catch (JSONException e) {
				e.printStackTrace();
				mostrarMensajeError(ConstantsPabs.buscarClientes);
			}
		} else if (json.has("error")){
			try {
				AlertDialog.Builder alert = getAlertDialogBuilder();
				alert.setMessage("No se encontraron coincidencias.");
				alert.setPositiveButton("Ok", (dialogInterface, i) -> {
					mthis = null;
					finish();
				});
				alert.create().show();
			} catch (NullPointerException e){
				e.printStackTrace();
				mthis = null;
				finish();
			} catch (Exception e){
				e.printStackTrace();
				mthis = null;
				finish();
			}
		} else {
			mostrarMensajeError(ConstantsPabs.buscarClientes);
		}
		
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		/*
		try {
			//goes back to Aportacion with the client selected
			Intent intent = new Intent();
			intent.putExtra("json_info", arrayClientes.getJSONObject(position).toString());
			setResult(RESULT_OK, intent);
			mthis = null;
			finish();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		*/

		Intent intent = new Intent();
		intent.putExtra("json_info", new Gson().toJson(contractsFound.get(position)));
		setResult(RESULT_OK, intent);
		mthis = null;
		finish();
		
	}

	/**
	 * returns an instance of AlertDialogBuilder
	 * @return instance of AlertDialogBuilder
	 */
	private AlertDialog.Builder getAlertDialogBuilder(){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			return new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
		}
		else {
			return new AlertDialog.Builder(this);
		}
	}

	/**
	 * shows error message
	 * @param caso contant when failed
	 */
	public void mostrarMensajeError(final int caso) {
		try {
			AlertDialog.Builder alert = getAlertDialogBuilder();
			alert.setMessage("Hubo un error de comunicación con el servidor, verifique sus ajustes de red o intente más tarde.");
			alert.setPositiveButton("Salir", (dialogInterface, i) -> {
				mthis = null;
				finish();
			});
			alert.setNegativeButton("Reintentar", (dialogInterface, i) -> {
				if (!isConnected)
					mostrarMensajeError(caso);
			});
			alert.create();
			alert.setCancelable(false);
			alert.show();
		} catch (Exception e){
			e.printStackTrace();
			Toast.makeText(ApplicationResourcesProvider.getContext(), "Hubo un problema de conexión, por favor vuelve a intentarlo", Toast.LENGTH_LONG).show();
			Util.Log.ih("SERVICIO COMPLETADO CON ERROR");
			mthis = null;
			finish();
		}
	}

	/**
	 * Gets context from this class.
	 * In some cases you cannot just call 'this', because another context is envolved
	 * or you want a reference of this class from another class,
	 * then this context is helpful.
	 *
	 * NOTE
	 * Not recommended, it may cause NullPointerException though
	 *
	 * @return context from BuscarCliente
	 */
	public static BuscarCliente getMthis() {
		return mthis;
	}

	/**
	 * Sets 'mthis' global variable as BuscarCliente's context
	 * @param mthis BuscarCliente's context
	 */
	public static void setMthis(BuscarCliente mthis) {
		BuscarCliente.mthis = mthis;
	}
	
}
