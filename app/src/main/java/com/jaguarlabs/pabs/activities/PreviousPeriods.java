package com.jaguarlabs.pabs.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.PreviousPeriodsAdapter;
import com.jaguarlabs.pabs.util.OnFragmentResult;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreviousPeriods extends Fragment implements PreviousPeriodsAdapter.ItemClickListener{

    public static int currentPeriod = -1;

    public boolean goBack;

    private RecyclerView rvPeriodos;
    private MainActivity mainActivity;

    private List<Integer> periodos;
    private PreviousPeriodsAdapter adapter;

    public PreviousPeriods() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_previous_periods, container, false);

        mainActivity = (MainActivity) getActivity();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        periodos = new ArrayList<>();
        currentPeriod = mainActivity.getEfectivoPreference().getInt("periodo", 0);

        addPeriods(currentPeriod);

        adapter = new PreviousPeriodsAdapter(getContext(), periodos, this);

        rvPeriodos = view.findViewById(R.id.rvPeriodos);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);

        rvPeriodos.setLayoutManager(layoutManager);
        rvPeriodos.addItemDecoration(itemDecoration);
        rvPeriodos.setAdapter(adapter);

        goBack = true;
    }

    @Override
    public void onResume() {
        super.onResume();

        MainActivity.CURRENT_TAG = MainActivity.TAG_PREVIOUS_PERIODS;
    }

    private void addPeriods(int lastPeriod)
    {
        if (lastPeriod > 0) {
            for (int i = lastPeriod - 1; i >= lastPeriod - 3; i--)
            {
                periodos.add(i);
            }
        }
    }

    @Override
    public void onItemClick(int periodo, int position) {
        Snackbar.make(mainActivity.mainLayout, "Periodo " + periodo, Snackbar.LENGTH_SHORT).show();
        Bundle bundle = getArguments();
        mainActivity.getEfectivoPreference().edit().putInt("periodo", periodo).apply();

        CierreInformativo cierreInformativo = new CierreInformativo();

        if (getFragmentManager() != null)
        {
            cierreInformativo.setArguments(bundle);

            getFragmentManager().beginTransaction()
                    .add(R.id.root_layout, cierreInformativo, MainActivity.TAG_CIERRE_INFORMATIVO)
                    .hide(this)
                    .addToBackStack(MainActivity.TAG_CIERRE_INFORMATIVO)
                    .commitAllowingStateLoss();

            goBack = false;
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        if (!hidden)
        {
            MainActivity.CURRENT_TAG = MainActivity.TAG_PREVIOUS_PERIODS;
            mainActivity.getEfectivoPreference().edit().putInt("periodo", currentPeriod).apply();

            goBack = true;
        }
    }
}
