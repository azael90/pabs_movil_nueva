package com.jaguarlabs.pabs.activities;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.CierreAdapter;
import com.jaguarlabs.pabs.adapter.ClientesAdapter;
import com.jaguarlabs.pabs.adapter.ClientesRecyclerAdapter;
import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.components.Preferences;
import com.jaguarlabs.pabs.database.Clients;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.CompanyAmounts;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.ExportImportDB;
import com.jaguarlabs.pabs.database.Locations;
import com.jaguarlabs.pabs.database.SyncedWallet;
import com.jaguarlabs.pabs.models.ModelClient;
import com.jaguarlabs.pabs.models.ModelDeposit;
import com.jaguarlabs.pabs.models.ModelPayment;
import com.jaguarlabs.pabs.models.ModelPeriod;
import com.jaguarlabs.pabs.net.NetHelper;
import com.jaguarlabs.pabs.net.NetService;
import com.jaguarlabs.pabs.net.VolleySingleton;
import com.jaguarlabs.pabs.rest.SpiceHelper;
import com.jaguarlabs.pabs.rest.models.ModelOsticketReponse;
import com.jaguarlabs.pabs.rest.models.ModelOsticketRequest;
import com.jaguarlabs.pabs.rest.request.OsticketRequest;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.CerrarSesionCallback;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.LogRegister;
import com.jaguarlabs.pabs.util.OnFragmentResult;
import com.jaguarlabs.pabs.util.SharedConstants;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.orm.SugarRecord;
import com.tingyik90.snackprogressbar.SnackProgressBar;
import com.tingyik90.snackprogressbar.SnackProgressBarManager;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import pl.tajchert.nammu.Nammu;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientesRecycler extends Fragment implements CerrarSesionCallback, DatabaseAssistant.OnGetClientsFromSyncedWalletListener, OnFragmentResult, ClientesRecyclerAdapter.ItemClickListener
        , AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener, View.OnLongClickListener{

    private static final long deviceDateTimeDifference = 1260000;//21 minutes
    private static final boolean writable = true;
    private static final int MAX_EFECTIVO_ALERTA = 500;

    public static long mTime = new Date().getTime();
    public static int notifications = 0;

    private long newTime;

    private boolean mandarABloqueo;
    private int robado;

    private int position;

    private int cont = 0;

    public static boolean targetURLTEST = false;

    private String getCierreOfDebtCollectorCode;

    //false -> 'Cierre Informativo'
    //true -> 'Cierre de Periodo'
    private boolean printCierrePerido = false;

    private boolean listViewStateGotten = false;

    private boolean firstLoad = false;

    private String osticketTopicSelected;
    private int osticketTopicSelectedPosition;
    private String osticketMessage;

    //endregion

    //region Objects

    private static ClientesRecycler mthis = null;

    private static final String TAG = ClientesRecycler.class.getName();

    private ClientesRecyclerAdapter adapter;
    private Preferences preferencesPendingTicket;
    private AlertDialog pendingTicketAlert;

    private ArrayList<JSONObject> arrayCercanos;
    private JSONArray arrayClientes;

    private JSONObject datosCobrador;

    private List<Clients> clientsList;
    private List<Clients> filteredClients;
    private List<ModelClient> nearbyClientsFromSyncedWallet;
    private List<Clients> nearbyClients;
    private List<Locations> offlineLocations;
    private List<ModelPayment> offlinePayments;
    private List<SyncedWallet> clientListFromSyncedWallet;
    //private List<SyncedWallet> clientListFromSyncedWalletWithoutAnalysis;
    //private List<SyncedWallet> clientListFromSyncedWalletForAdapter;

    private PowerManager pm;
    private PowerManager.WakeLock wl;

    private Timer timer;
    private Timer timerNotificaciones;
    private Timer timerSendPaymentsEachTenMinutes;
    private Timer timerUpdate;

    private Random rand = new Random();

    private RecyclerView rvClientes;

    private Parcelable listViewState;

    private DatabaseAssistant.OnGetClientsFromSyncedWalletListener onGetClientsFromSyncedWalletListener;

    private Dialog dialog;

    private Spinner spinnerOsticketTopics;
    private TextView tvAceptar;
    private TextView tvCancelar;
    private EditText osticketMessageEdiText;

    private MainActivity mainActivity;
    private SpiceManager spiceManager;
    private SpiceHelper<ModelOsticketReponse> spiceSendOsticket;

    private boolean updatePostponed = false;

    private String fechaCierre;

    private Handler previousPeriodsHandler = new Handler();
    private Runnable previousRunnable = () -> {cierreLongClicked = false;efectivoLongClicked = false;};
    private boolean cierreLongClicked, efectivoLongClicked;
    private EditText etFiltrado;


    public static ClientesRecycler newInstance()
    {
        return new ClientesRecycler();
    }


    public ClientesRecycler() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_clientes_recycler, container, false);

        mainActivity = (MainActivity)getActivity();
        spiceManager = mainActivity.getSpiceManager();

        return inflater.inflate(R.layout.fragment_clientes_recycler, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spiceSendOsticket = new SpiceHelper<ModelOsticketReponse>(spiceManager, "sendOsticket", ModelOsticketReponse.class, DurationInMillis.ONE_HOUR) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
            }

            @Override
            protected void onSuccess(ModelOsticketReponse result) {
                try {
                    if (result != null) {
                        super.onSuccess(result);
                        if (result.getResult() != null) {
                            DatabaseAssistant.updateOsticketAsSent(result.getResult());
                        } else {
                            //fail();
                        }
                    } else {
                        super.onSuccess(result);
                        //DatabaseAssistant.updateOsticketAsSent();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                super.onProgressUpdate(progress);
            }
        };

		/*
		IntentFilter filter = new IntentFilter(
				"android.bluetooth.device.action.PAIRING_REQUEST");

		registerReceiver(
				new PairingRequest(), filter);
		*/


        /**
         * Broadcast
         * This broadcast gets called when time or timezone changes
         */
		/*
		m_timeChangedReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {

				/**
				 * After
				 *
				 * If time changes for more than 21 minutes,
				 * ends session
				 *
				 * Jordan Alvarez
				 */
		/*
				if ( mTime != 0 ){
					if ( checkDate() ){
						final String action = intent.getAction();

						if (action.equals(Intent.ACTION_TIME_CHANGED)
								|| action.equals(Intent.ACTION_TIMEZONE_CHANGED)) {

							Mint.logEvent("Cierre de Sesion - broadcast " + loadUser() , MintLogLevel.Info);
							//LogRegister.endSessionByBroadcast(mTime, newTime);

							Util.Log.ih("Restarting Application!");
							Util.Log.ih("Hora SEGUNDA: " + System.currentTimeMillis());
							finish();
							android.os.Process.killProcess(android.os.Process.myPid());
						}
					}
				}
				mTime = new Date().getTime();

				/*
				Before
				final String action = intent.getAction();

				 if (action.equals(Intent.ACTION_TIME_CHANGED)
				 || action.equals(Intent.ACTION_TIMEZONE_CHANGED)
				 || action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED
				 || action.equals(Intent.ACTION_DATE_CHANGED)

				if (action.equals(Intent.ACTION_TIME_CHANGED)
						|| action.equals(Intent.ACTION_TIMEZONE_CHANGED)) {
					Util.Log.ih("Restarting Application!");
					Util.Log.ih("Hora SEGUNDA: " + System.currentTimeMillis());
					finish();
					android.os.Process.killProcess(android.os.Process.myPid());
				}
				*//*
			}
		};
		*/
        //registerReceiver(m_timeChangedReceiver, s_intentFilter);

        //init
        mthis = this;
        Deposito.depositosIsActive = false;
        //canceladoPreference =  getSharedPreferences("cancelado", Context.MODE_PRIVATE);
        mainActivity.isConnected = getArguments().getBoolean("isConnected", false);

        mainActivity.hasActiveInternetConnection(getContext(), mainActivity.mainLayout);


        Util.Log.ih("periodo: " + mainActivity.getEfectivoPreference().getInt("periodo", 0));


        //setSharedPreferencesForDateWhenNoInternetAccess();
        //registerReceiver(m_timeChangedReceiver, s_intentFilter);
		/*
		PackageInfo pInfo;
		try {
			//pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			//String version = pInfo.versionName;
			//writeToFile("version: " + version);

		} catch (NameNotFoundException e1) {
		}
		*/
        //writeToFile("inicio_sesion");
        mthis = this;
        ExtendedActivity.setEstoyLogin(false);
        mandarABloqueo = false;
        //gpsProvider = new GPSProvider(this);

        timerSendPaymentsEachTenMinutes = null;
        enviarCobrosDiezMinutos();
        pm = (PowerManager) getContext().getSystemService(
                Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "tag");
        wl.acquire();
        robado = 0;
        //iniciarHandlerInactividad();
        llamarTimerInactividad();
        //alert = new Builder(this);
        //arrayFiltrados = null;
        filteredClients = null;
        timer = null;

		/*
		if ( DatabaseAssistant.isThereMoreThanOnePeriodActive() ) {
			List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();
			getEfectivoEditor().putInt("periodo", periods.get( periods.size()-1 ).getPeriodNumber()).apply();
		}
		*/

        ImageView ivTexto = (ImageView) view.findViewById(R.id.iv_efectivo_texto);
        Animation animAlpha = AnimationUtils.loadAnimation(getContext(), R.anim.cash_animation);
        ivTexto.startAnimation(animAlpha);

        //lvClientes = (ListView) view.findViewById(R.id.listView_clientes);
        rvClientes = view.findViewById(R.id.rv_clientes);
        RecyclerView.LayoutManager layoutManager = new CustomLinearLayoutManager(getContext());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        layoutManager.setItemPrefetchEnabled(true);

        rvClientes.setLayoutManager(layoutManager);
        rvClientes.setHasFixedSize(true);
        rvClientes.addItemDecoration(itemDecoration);
        rvClientes.setItemViewCacheSize(500);
        rvClientes.setDrawingCacheEnabled(true);
        rvClientes.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        sendLocation();
        Bundle bundle = getArguments();

        /**
         * Instantiates datosCobrador variable giving it
         * company name and its serie
         *
         * NOTE*
         * 		ADDS COMPANIES
         *
         * NOTE**
         * 		USELESS CODE - REMOVE
         */
        if (bundle != null) {
            if (bundle.containsKey("datos_cobrador")) {
                try {
					/*
					SqlQueryAssistant query = new SqlQueryAssistant(
							ApplicationResourcesProvider.getContext());
					datosCobrador = new JSONObject(
							bundle.getString("datos_cobrador"));
					datosCobrador.put(
							query.mostrarEmpresa("01").getString(
									Empresa.nombre_empresa),
							datosCobrador.getString("serie_programa"));
					datosCobrador.put(
							query.mostrarEmpresa("03").getString(
									Empresa.nombre_empresa),
							datosCobrador.getString("serie_malba"));
					datosCobrador.put(
							query.mostrarEmpresa("04").getString(
									Empresa.nombre_empresa),
							datosCobrador.getString("serie_empresa4"));
					datosCobrador.put(
							query.mostrarEmpresa("05").getString(
									Empresa.nombre_empresa),
							datosCobrador.getString("serie_empresa5"));
					getEfectivoEditor().putString("no_cobrador",
							datosCobrador.getString("no_cobrador")).apply();
					*/

                    datosCobrador = new JSONObject(
                            bundle.getString("datos_cobrador"));

                    DatabaseAssistant.setCobrador(datosCobrador.getString("no_cobrador"));

                    switch (BuildConfig.getTargetBranch()) {
                        case SALTILLO:
                        case CANCUN:
                            datosCobrador.put("Programa", datosCobrador.getString("serie_programa"));
                            datosCobrador.put("PBJ", datosCobrador.getString("serie_malba"));
                            datosCobrador.put("Cooperativa", datosCobrador.getString("serie_programa"));
                            datosCobrador.put("PABS LATINO", datosCobrador.getString("serie_empresa5"));
                            break;
                        default:
                            datosCobrador.put("Programa", datosCobrador.getString("serie_programa"));
                            datosCobrador.put("PBJ", datosCobrador.getString("serie_malba"));
                            datosCobrador.put("Cooperativa", datosCobrador.getString("serie_empresa4"));
                            datosCobrador.put("PABS LATINO", datosCobrador.getString("serie_empresa5"));
                            break;

                    }
					/*
					datosCobrador.put("Programa", datosCobrador.getString("serie_programa"));
					datosCobrador.put("PBJ", datosCobrador.getString("serie_malba"));
					datosCobrador.put("Cooperativa", datosCobrador.getString("serie_empresa4"));
					datosCobrador.put("PABS LATINO", datosCobrador.getString("serie_empresa5"));
					*/
                    mainActivity.getEfectivoEditor().putString("no_cobrador",
                            datosCobrador.getString("no_cobrador")).apply();

                    //query = null;
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    try {
                        datosCobrador = new JSONObject(
                                bundle.getString("datos_cobrador"));
                        datosCobrador.put("Programa", datosCobrador.getString("serie_programa"));
                        datosCobrador.put("PBJ", datosCobrador.getString("serie_malba"));
                        datosCobrador.put("Cooperativa", datosCobrador.getString("serie_empresa4"));
                        datosCobrador.put("PABS LATINO", datosCobrador.getString("serie_empresa5"));
                        mainActivity.getEfectivoEditor().putString("no_cobrador",
                                datosCobrador.getString("no_cobrador")).apply();

                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }

                if (getArguments().containsKey("query"))
                {
                    try
                    {
                        String query = getArguments().getString("query", "");

                        Log.d("ServerQuery", query);

                        if (!query.equals("") && !query.equals("null"))
                        {
                            SugarRecord.executeQuery(query);

                            getArguments().remove("query");
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }
            }
        }
        /**
         * Timer to get notifications, starts
         */
        timerNotificaciones = null;
        timerNotificaciones = new Timer();
        timerNotificaciones.schedule(new TimerTask() {

            @Override
            public void run() {
                // TODO

				/*
				try {
					Looper.prepare();
				} catch (Exception e) {
					e.printStackTrace();
				}
				*/

                if (mainActivity.isConnected) {
                    Util.Log.ih("getting personal notifications");
                    getNumNotificaciones();
                }

				/*
				try {
					Looper.loop();
				} catch (Exception e) {
					e.printStackTrace();
				}
				*/

            }
        }, 1000, (1000 * 30) * 30);//}, 1000, (1000 * 60) * 5 );
        SharedPreferences preferences = mainActivity.getSharedPreferences("PABS_Main",
                Context.MODE_PRIVATE);
        if (preferences.contains("numero_notificaciones")) {
            TextView tvICon = (TextView) view.findViewById(R.id.tv_icon_notificaciones);
            tvICon.setText(preferences.getString("numero_notificaciones", "" + 0));
        }
        //final SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());

        try {
            putInfo();
        } catch (JSONException e1) {
            e1.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setListeners();

        /**
         * Search Clients field
         *
         * looks for given text on the databasse
         */
        etFiltrado = view.findViewById(R.id.et_filtrado);

        etFiltrado.addTextChangedListener(new TextWatcher() {

            //private Timer timerToSearch = new Timer();
            //private final long DELAY = 10;

            Thread thread;

            @Override
            public void afterTextChanged(Editable arg0) {

                if (thread != null && thread.isAlive())
                    thread.interrupt();

                thread = new Thread(() -> {


			/*
				filteredClients = new ArrayList<>();

				adapter = getClientesAdapterFilteredClients();
				lvClientes = (ListView) findViewById(R.id.listView_clientes);
				lvClientes.setAdapter(adapter);
				lvClientes.setDivider(getResources().getDrawable(
						R.drawable.divider_list_clientes));
				lvClientes.setOnItemClickListener(mthis);
			*/

                    //ADD NEW TIMER TO SEARCH AFTER DELAY TIME

                    //timerToSearch.cancel();
                    //timerToSearch = new Timer();
                    //timerToSearch.schedule(new TimerTask() {

                    //Editable arg0;

                    //@Override
                    //public void run() {

                    //runOnUiThread(new Runnable() {
                    //@Override
                    //public void run() {

                    if (etFiltrado.length() > 0) {

                        // TODO
                        if (nearbyClients != null || nearbyClientsFromSyncedWallet != null) {// si el filtro de cercanos esta
                            // activado, se pasa a
                            // desactivar.
                            Log.d("nearbyClients", "!= NULL");
                            //runOnUiThread(new Runnable() {
                            //	@Override
                            //	public void run() {
                            final ImageButton cercanos = (ImageButton) view.findViewById(R.id.btn_buscar_cercanos);
                            mainActivity.runOnUiThread(cercanos::performClick);
                            //	}
                            //});
								/*
								final ImageButton cercanos = (ImageButton) findViewById(R.id.btn_buscar_cercanos);
								cercanos.performClick();
								*/
                        } else {
                            if (!listViewStateGotten) {
                                mainActivity.runOnUiThread(() -> {
                                    /*listViewState = lvClientes.onSaveInstanceState();
                                    listViewStateGotten = !listViewStateGotten;*/
                                });
                            }
                            Log.d("nearbyClients", "NULL");
                        }

                        //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
                        //arrayFiltrados = null;
                        filteredClients = null;

                        {
                            /**
                             * ==============================
                             * |           CHEATS			|
                             * ==============================
                             * This is for debugging purposes
                             */

                            /**
                             * changes period
                             */
                            if (arg0.length() > 0 && arg0.toString().contains("changePeriod%")) {
                                try {
                                    String period = arg0.toString().substring(
                                            arg0.toString().indexOf("%") + 1,
                                            arg0.toString().lastIndexOf("%")
                                    );
                                    Util.Log.ih("periodo obtenido = " + period);
                                    String previoudPeriod = Integer.toString(mainActivity.getEfectivoPreference().getInt("periodo", 0));
                                    LogRegister.registrarCheat(arg0.toString() + " Periodo anterior: " + previoudPeriod);
                                    mainActivity.getEfectivoEditor().putInt("periodo", Integer.parseInt(period)).apply();
                                    DatabaseAssistant.eraseDatabaseAfterChangingPeriod();
                                    mainActivity.toastL("El periodo ha cambiado a: " + period + "; periodo anterior: " + previoudPeriod);
                                    printCierrePerido = !printCierrePerido;
                                } catch (StringIndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            /**
                             * exports database to device memory
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("exportDB%")) {
                                ExportImportDB exportImportDB = new ExportImportDB();
                                LogRegister.registrarCheat(arg0.toString());
                                try {
                                    if (exportImportDB.exportDB()) {
                                        mainActivity.toastL("Base de datos exportada correctamente");
                                    } else {
                                        mainActivity.toastL("Hubo un error al exportar la base de datos.");
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            /**
                             * exports database to device memory
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("exportDBHidden%")) {
                                ExportImportDB exportImportDB = new ExportImportDB();
                                LogRegister.registrarCheat(arg0.toString());
                                try {
                                    if (exportImportDB.exportDBHidden()) {
                                        mainActivity.toastL("Base de datos exportada correctamente");
                                    } else {
                                        mainActivity.toastL("Hubo un error al exportar la base de datos.");
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            /**
                             * exports database to device memory
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("files%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                File sdCardRoot = Environment.getExternalStorageDirectory();
                                File yourDir = new File(sdCardRoot, "/PABS_LOGS/.TestDataBase Backup/");
                                for (File f : yourDir.listFiles()) {
                                    if (f.isFile()) {
                                        String name = f.getName();
                                        Util.Log.ih("name = " + name);
                                        Util.Log.ih("lastModified = " + new Date(f.lastModified()));
                                        // Do your stuff
                                    } else {
                                        String name = f.getName();
                                        Util.Log.ih("f name = " + name);
                                        Util.Log.ih("lastModified = " + new Date(f.lastModified()));
                                    }
                                }
                            }
                            /**
                             * imports databasse from device memory to app
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("importDB%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                ExportImportDB exportImportDB = new ExportImportDB();
                                try {
                                    if (exportImportDB.importDB()) {
                                        mainActivity.toastL("Base de datos importada correctamente");
                                    } else {
                                        mainActivity.toastL("Hubo un error al importar la base de datos.");
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            /**
                             * imports databasse from device memory to app
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("importDBHidden%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                ExportImportDB exportImportDB = new ExportImportDB();
                                try {
                                    if (exportImportDB.importDBHidden()) {
                                        mainActivity.toastL("Base de datos importada correctamente");
                                    } else {
                                        mainActivity.toastL("Hubo un error al importar la base de datos.");
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            /**
                             * change target URL to ecobrotest
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("changeURL%TEST%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                targetURLTEST = true;
                                Preferences p = new Preferences(ApplicationResourcesProvider.getContext());
                                p.saveURLTEST();
                                mainActivity.toastL("Has cambiado a servidor de TEST. Por favor reinicia la app");
                            }
                            /**
                             * change target URL to ecobro
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("changeURL%PROD%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                targetURLTEST = false;
                                Preferences p = new Preferences(ApplicationResourcesProvider.getContext());
                                p.clearURL();
                                mainActivity.toastL("Has cambiado a servidor de PRODUCCION. Por favor reinicia la app");
                            }
                            /**
                             * delete all payments
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("deleteAllPayments%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                DatabaseAssistant.deleteAllPayments();
                                mainActivity.toastL("Eliminando todos los cobros");
                            }
                            /**
                             * set contract as paid
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("setContractAsPaid%")) {
                                try {
                                    String contractNumber = arg0.toString().substring(
                                            arg0.toString().indexOf("%") + 1,
                                            arg0.toString().lastIndexOf("%")
                                    );
                                    LogRegister.registrarCheat(arg0.toString());
                                    Util.Log.ih("contrato obtenido = " + contractNumber);
                                    String contractID = DatabaseAssistant.getContractID(contractNumber);
                                    if (contractID != null) {
                                        DatabaseAssistant.updatePaymentMade(contractID);
                                        mainActivity.toastL("El contrato " + contractNumber + " se ha seteado como pagado en bd");
                                    } else {
                                        mainActivity.toastL("El contrato " + contractNumber + " no existe");
                                    }
                                } catch (StringIndexOutOfBoundsException e) {
                                    //nothing here
                                } catch (Exception e) {
                                    //nothing here
                                }
                            }
                            /**
                             * set contract as visited
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("setContractAsVisited%")) {
                                try {
                                    String contractNumber = arg0.toString().substring(
                                            arg0.toString().indexOf("%") + 1,
                                            arg0.toString().lastIndexOf("%")
                                    );
                                    LogRegister.registrarCheat(arg0.toString());
                                    Util.Log.ih("contracto obtenido = " + contractNumber);
                                    String contractID = DatabaseAssistant.getContractID(contractNumber);
                                    if (contractID != null) {
                                        DatabaseAssistant.updateVisitMade(contractID);
                                        mainActivity.toastL("El contrato " + contractNumber + " se ha seteado como visitado en bd");
                                        mainActivity.runOnUiThread(() -> adapter.notifyDataSetChanged());
                                    } else {
                                        mainActivity.toastL("El contrato " + contractNumber + " no existe");
                                    }
                                } catch (StringIndexOutOfBoundsException e) {
                                    //nothing here
                                } catch (Exception e) {
                                    //nothing here
                                }
                            }
                            /**
                             * do not show yellow and red contracts
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("deleteYellowsAndReds%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                DatabaseAssistant.resetContractsNotVisitedFromYesterdayAndBeforeYesterday();
                                mainActivity.toastL("Modificando contratos, espera...");
                                updateListView(true);
                                mainActivity.toastL("Contratos modificados");
                            }
                            /**
                             * resends all payments
                             */
                            if (arg0.length() > 0 && arg0.toString().contains("resendPayment%")) {
                                try {
                                    String folio = arg0.toString().substring(
                                            arg0.toString().indexOf("%") + 1,
                                            arg0.toString().indexOf(",")
                                    );
                                    String serie = arg0.toString().substring(
                                            arg0.toString().indexOf(",") + 1,
                                            arg0.toString().lastIndexOf("%")
                                    );
                                    LogRegister.registrarCheat(arg0.toString());
                                    Util.Log.ih("folio obtenido = " + folio);
                                    Util.Log.ih("serie obtenida = " + serie);
                                    boolean response = DatabaseAssistant.updatePaymentAsNotSyncedToSendAgain(serie, folio);
                                    mainActivity.toastL(response ? "Se modificó el contrato. Ingresa al cierre informativo." : "No se encontró el contrato");
                                    printCierrePerido = !printCierrePerido;
                                } catch (StringIndexOutOfBoundsException e) {
                                    //nothing here
                                } catch (Exception e) {
                                    //nothing here
                                }
                            }

                            /**
                             * Execute raw query
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("executeQuery%"))
                            {
                                try
                                {
                                    String query = arg0.toString().substring(arg0.toString().indexOf("%") + 1, arg0.toString().lastIndexOf("%"));
                                    LogRegister.registrarCheat(arg0.toString());
                                    SugarRecord.executeQuery(query);
                                    mainActivity.toastL("Query ejecutado: " + query);
                                }
                                catch (Exception ex)
                                {
                                    ex.printStackTrace();
                                }

                            }
                            /**
                             * create 500 payments
                             */

                            else if (arg0.length() > 0 && arg0.toString().contains("createPayments%"))
                            {
                                try {
                                    int numberOfContracts = Integer.parseInt(arg0.toString().substring(arg0.toString().indexOf("%") + 1,
                                            arg0.toString().lastIndexOf("%")));
                                    LogRegister.registrarCheat(arg0.toString());
                                    mainActivity.toastL("Creando pagos...");

                                    //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                    SharedPreferences folios = mainActivity.getSharedPreferences("folios", Context.MODE_PRIVATE);

                                    int folio = Integer.parseInt(folios.getString("Cooperativa", "0"));
                                    //int folio = 1000;
                                    DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                                    LocalDateTime date = LocalDateTime.now();
                                    for (int i = 0; i < numberOfContracts; i++)
                                    {
                                        folio++;

                                        DatabaseAssistant.insertPayment(
                                                "04",
                                                "20.6826024",
                                                "-103.3821053",
                                                "1",
                                                datosCobrador.getString("no_cobrador"),
                                                "1",
                                                "04257",
                                                formatter.print(date),
                                                "1CJ",
                                                Integer.toString(folio),
                                                datosCobrador.getString("serie_empresa4"),
                                                "1",
                                                mainActivity.getEfectivoPreference().getInt("periodo", 0),
                                                ""
                                        );

                                        date = date.plusMinutes(1);
                                    }

                                    /*folio = Integer.parseInt(folios.getString("Programa", "0"));
                                    for (int i = 0; i < numberOfContracts; i++)
                                    {
                                        folio++;

                                        DatabaseAssistant.insertPayment(
                                                "01",
                                                "20.6826024",
                                                "-103.3821053",
                                                "1",
                                                datosCobrador.getString("no_cobrador"),
                                                "1",
                                                "04257",
                                                dateFormat.format(new Date()),
                                                "1CJ",
                                                Integer.toString(folio),
                                                datosCobrador.getString("serie_programa"),
                                                "1",
                                                mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                        );
                                    }

                                    folio = Integer.parseInt(folios.getString("PBJ", "0"));
                                    for (int i = 0; i < numberOfContracts; i++)
                                    {
                                        folio++;

                                        DatabaseAssistant.insertPayment(
                                                "03",
                                                "20.6826024",
                                                "-103.3821053",
                                                "1",
                                                datosCobrador.getString("no_cobrador"),
                                                "1",
                                                "04257",
                                                dateFormat.format(new Date()),
                                                "1CJ",
                                                Integer.toString(folio),
                                                datosCobrador.getString("serie_malba"),
                                                "1",
                                                mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                        );
                                    }

                                    folio = Integer.parseInt(folios.getString("PABS LATINO", "0"));
                                    for (int i = 0; i < numberOfContracts; i++)
                                    {
                                        folio++;

                                        DatabaseAssistant.insertPayment(
                                                "05",
                                                "20.6826024",
                                                "-103.3821053",
                                                "1",
                                                datosCobrador.getString("no_cobrador"),
                                                "1",
                                                "04257",
                                                dateFormat.format(new Date()),
                                                "1CJ",
                                                Integer.toString(folio),
                                                datosCobrador.getString("serie_empresa5"),
                                                "1",
                                                mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                        );
                                    }*/

                                    mainActivity.toastL("Pagos creados");
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                            /*else if (arg0.length() > 0 && arg0.toString().contains("createPayments%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                mainActivity.toastL("Creando pagos...");

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                int folio4 = 3333;
                                for (int i = 0; i < 1; i++) {
                                    folio4 += 1;
                                    String folioString = "" + folio4;
                                    DatabaseAssistant.insertPayment(
                                            "04",
                                            "20.6826024",
                                            "-103.3821053",
                                            "1",
                                            "27561",
                                            "1",
                                            "042753",
                                            dateFormat.format(new Date()),
                                            "1CJ",
                                            folioString,
                                            "E",
                                            "1",
                                            mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                    );
                                }

                                int folio3 = 1111;
                                for (int i = 0; i < 1; i++) {
                                    folio3 += 1;
                                    String folioString = "" + folio3;
                                    DatabaseAssistant.insertPayment(
                                            "03",
                                            "20.6826024",
                                            "-103.3821053",
                                            "1",
                                            "27561",
                                            "1",
                                            "042753",
                                            dateFormat.format(new Date()),
                                            "1CJ",
                                            folioString,
                                            "E",
                                            "1",
                                            mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                    );
                                }

                                int folio2 = 9999;
                                for (int i = 0; i < 1; i++) {
                                    folio2 += 1;
                                    String folioString = "" + folio2;
                                    DatabaseAssistant.insertPayment(
                                            "02",
                                            "20.6826024",
                                            "-103.3821053",
                                            "1",
                                            "27561",
                                            "1",
                                            "042753",
                                            dateFormat.format(new Date()),
                                            "1CJ",
                                            folioString,
                                            "E",
                                            "1",
                                            mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                    );
                                }

                                mainActivity.toastL("Pagos creados");
                            }*/
                            /**
                             * sets internet connection as reachable
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("internet%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                ExtendedActivity.setInternetAsReachable = !ExtendedActivity.setInternetAsReachable;
                                mainActivity.toastL("Internet " + (ExtendedActivity.setInternetAsReachable ? "siempre activo" : ""));
                            }
                            /**
                             * runs method to change visited or paid status
                             * (show contracts that have to pay today)
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("changeVisitedOrPaidStatus%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                Calendar calendar = Calendar.getInstance();
                                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                                int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);

                                mainActivity.toastL("Modificando contratos, espera...");
                                DatabaseAssistant.changeVisitedOrPaidStatusFromContractsManual(dayOfWeek, dayOfMonth, dayOfYear);
                                //toastL("Modificando contratos, espera...");
                                updateListView(true);
                                mainActivity.toastL("Contratos modificados");
                            }
                            /**
                             * updates list view
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("updateWallet%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                mainActivity.toastS("Actualizando cartera...");
                                updateListView(true);
                                mainActivity.toastS("Listo");
                            }
                            /**
                             * create GeoJson file
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("geoJson%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                mainActivity.toastS("Creando archivo...");
                                try {
                                    //ApplicationResourcesProvider.saveGeoJSONFile(datosCobrador.getString("periodo"), datosCobrador.getString("no_cobrador"));
                                    ApplicationResourcesProvider.saveGeoJSONFile("" + mainActivity.getEfectivoPreference().getInt("periodo", 0), datosCobrador.getString("no_cobrador"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                mainActivity.toastS("Listo");
                            }
                            /**
                             * create GeoJson file
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("resetContracts%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                DatabaseAssistant.resetContractsPerWeek();
                                mainActivity.toastS("contratos reiniciados");
                            }
                            /**
                             * send payments
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("sendPaymentstxt%")) {
                                LogRegister.registrarCheat(arg0.toString());
                                registerPagostxt();
                            }
                            /**
                             * get 'cierre informativo' of another debt colletor
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("getCierre%")) {
                                try {
                                    getCierreOfDebtCollectorCode = arg0.toString().substring(
                                            arg0.toString().indexOf("%") + 1,
                                            arg0.toString().lastIndexOf("%"));
                                    LogRegister.registrarCheat(arg0.toString());
                                    Util.Log.ih("codigo obtenido = " + getCierreOfDebtCollectorCode);
                                    mainActivity.toastL("Ingresa al cierre informativo para obtener el cierre del cobrador con el código: " + getCierreOfDebtCollectorCode);
                                } catch (StringIndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            /**
                             * add new period to db
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("addNewPeriod%")) {
                                try {
                                    String period = arg0.toString().substring(
                                            arg0.toString().indexOf("%") + 1,
                                            arg0.toString().lastIndexOf("%")
                                    );
                                    LogRegister.registrarCheat(arg0.toString());
                                    Util.Log.ih("periodo obtenido = " + period);
                                    DatabaseAssistant.insertNewPeriodIfNeeded(Integer.parseInt(period));
                                    mainActivity.toastL(DatabaseAssistant.checkLastThreePeriodsStatus());
                                    //DatabaseAssistant.updateEndDatePeriodButKeepActive(60);
                                    //toastL("Nuevo periodo insertado");
                                } catch (StringIndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            /**
                             * close active period
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("closePeriod%")) {
                                try {
                                    String period = arg0.toString().substring(
                                            arg0.toString().indexOf("%") + 1,
                                            arg0.toString().lastIndexOf("%")
                                    );
                                    LogRegister.registrarCheat(arg0.toString());
                                    Util.Log.ih("periodo obtenido = " + period);
                                    DatabaseAssistant.closePeriod(period);
                                    mainActivity.toastL(DatabaseAssistant.checkLastThreePeriodsStatus());
                                    //DatabaseAssistant.updateEndDatePeriodButKeepActive(60);
                                    //toastL("Nuevo periodo insertado");
                                } catch (StringIndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            /**
                             * change server ip manually
                             */
                            else if (arg0.length() > 0 && arg0.toString().contains("changeServerIP%")) {

                                try {
                                    String IP = arg0.toString().substring(
                                            arg0.toString().indexOf("%") + 1,
                                            arg0.toString().lastIndexOf("%")
                                    );
                                    LogRegister.registrarCheat(arg0.toString());
                                    Util.Log.ih("IP obtenida = " + IP);

                                    Preferences preferencesServerIP = new Preferences(ApplicationResourcesProvider.getContext());
                                    String oldServerIP = preferencesServerIP.getServerIP();

                                    preferencesServerIP.changeServerIP(IP);
                                    mainActivity.toastL("La IP a cambiado a: " + IP + "\nIP anterior: " + oldServerIP);
                                } catch (StringIndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            else if (arg0.length() > 0 && arg0.toString().contains("printerCommand%")) {
                                try {

                                    String command = arg0.toString().substring(arg0.toString().indexOf("%") + 1,
                                            arg0.toString().lastIndexOf("%"));

                                    String tspl = "";
                                    String prefix = "";
                                    int counter = 0;

                                    switch (command.toUpperCase()) {
                                        case "SERIAL":
                                            tspl = "_SERIAL$";
                                            prefix = "Serial No: ";
                                            break;
                                        case "MODEL":
                                            tspl = "_MODEL$";
                                            prefix = "Model: ";
                                            break;
                                        case "VERSION":
                                            tspl = "_VERSION$";
                                            prefix = "Version: ";
                                            break;
                                        case "ADDRESS":
                                            tspl = "GETSETTING$(\"CONFIG\", \"BT\", \"MAC ADDRESS\")";
                                            prefix = "BT MAC Address: ";
                                            break;
                                        case "CHECKSUM":
                                            tspl = "GETSETTING$(\"SYSTEM\", \"INFORMATION\", \"CHECKSUM\")";
                                            prefix = "Checksum: ";
                                            break;
                                        case "DPI":
                                            tspl = "GETSETTING$(\"SYSTEM\", \"INFORMATION\", \"DPI\")";
                                            prefix = "DPI: ";
                                            break;
                                        case "RESET":
                                            tspl = (char)27 + "!R";
                                            break;
                                        case "FEED":
                                            tspl = (char)27 + "!F";
                                            break;
                                        case "PAUSE":
                                            tspl = (char)27 + "!P";
                                            break;
                                        case "CONTINUE":
                                            tspl = (char)27 + "!O";
                                            break;
                                    }

                                    while (counter < 5) {
                                        Log.d("SerialCounter", Integer.toString(counter));

                                        try {

                                            BluetoothPrinter.getInstance().connect();

                                            BluetoothPrinter.getInstance().setup(
                                                    SharedConstants.Printer.width,
                                                    40,
                                                    SharedConstants.Printer.speed,
                                                    SharedConstants.Printer.density,
                                                    SharedConstants.Printer.sensorType,
                                                    SharedConstants.Printer.gapBlackMarkVerticalDistance,
                                                    SharedConstants.Printer.gapBlackMarkShiftDistance
                                            );

                                            //String serial = BluetoothPrinter.getInstance().sendcommand_getString("OUT \"\";_SERIAL$").split("\r\n")[0];
                                            String serial = BluetoothPrinter.getInstance().sendcommand_getString("OUT \"\";" + tspl).split("\r\n")[0];

                                            if (!serial.equals(" ")) {
                                                BluetoothPrinter.getInstance().clearbuffer();

                                                BluetoothPrinter.getInstance().printerfont(100, 150, "3", 0, 0, 0, prefix + serial);

                                                BluetoothPrinter.getInstance().printlabel(1, 1);

                                                mainActivity.toastS(prefix + serial);
                                            }

                                            break;
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                            counter++;
                                        }
                                    }

                                    if (counter == 5) {
                                        Log.d("SerialCounter", "No se pudo conectar con la impresora");
                                        mainActivity.toastS("No se pudo conectar con la impresora");
                                    }
                                } catch (StringIndexOutOfBoundsException ex) {
                                    ex.printStackTrace();
                                }
                            }
                            /**
                             *
                             */
                            /**
                             * Here is where looks for given text on database
                             */
                            else {
							/*
							arrayFiltrados = query.mostrarClientesFiltrados(
									etFiltrado.getText().toString()).getJSONArray(
									"clientes");
							*/
							/*filteredClients = new ArrayList<>();

							for (Clients c: clientsList){
								if ( (c.getName() + " " + c.getFirstLastName() + " " + c.getSecondLastName()).contains(arg0.toString().toUpperCase()) ){
									filteredClients.add(c);
								} else if ( (c.getSerie() + c.getNumberContract()).contains(arg0.toString().toUpperCase()) ){
									filteredClients.add(c);
								} else if ( (c.getStreetToPay() + " " + c.getNumberExt() + " " + c.getNeighborhood() + " " + c.getBetweenStreets() + " " + c.getLocality()).contains(arg0.toString().toUpperCase()) ){
									filteredClients.add(c);
								}
							}
							*/
                                try {
                                    //filteredClients = DatabaseAssistant.getFilteredClients(arg0.toString());
                                } catch (SQLiteException e) {
                                    e.printStackTrace();
                                    filteredClients = null;
                                }
                            }
                        }

                        if (filteredClients == null) {
                            filteredClients = new ArrayList<>();
                        }
                        if (filteredClients.size() > -1) {
                            //ClientesAdapter adapter = getClientesAdapterFilteredClients();
                            /*adapter = getClientesAdapterFilteredClients();

                            //runOnUiThread(new Runnable() {
                            //	@Override
                            //	public void run() {
                            mainActivity.runOnUiThread(() -> {
                                lvClientes = (ListView) view.findViewById(R.id.listView_clientes);
                                if (lvClientes != null) {
                                    lvClientes.setAdapter(adapter);
                                    lvClientes.setDivider(getResources().getDrawable(
                                            R.drawable.divider_list_clientes));
                                    lvClientes.setOnItemClickListener(mthis);
                                }
                            });*/
                            //	}
                            //});
								/*
								lvClientes = (ListView) findViewById(R.id.listView_clientes);
								lvClientes.setAdapter(adapter);
								lvClientes.setDivider(getResources().getDrawable(
										R.drawable.divider_list_clientes));
								lvClientes.setOnItemClickListener(mthis);
								*/
                        } else {
                            etFiltrado.setText("");
                            try {
                                mainActivity.toastS("Sin resultados para la busqueda, intenta de nuevo con otra opción");
                            } catch (Exception e) {
                                mainActivity.runOnUiThread(() -> {
                                    Toast toast = Toast.makeText(getContext(), "Sin resultados para la busqueda, intenta de nuevo con otra opción", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                });
                                e.printStackTrace();
                            }
                        }

                        /**
                         * Erases given text when touching on cancel icon
                         * and shows accounts within wallet
                         */

                        //runOnUiThread(new Runnable() {
                        //	@Override
                        //	public void run() {
                        mainActivity.runOnUiThread(() -> {
                            final ImageButton btnCancelarFiltrado = (ImageButton) view.findViewById(R.id.ib_cancelar_filtro);
                            btnCancelarFiltrado.setVisibility(View.VISIBLE);
                            btnCancelarFiltrado
                                    .setOnClickListener(view -> {
							    /*
									arrayFiltrados = null;
									SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());

									if (BuildConfig.isWalletSynchronizationEnabled()) {
										JSONObject myWallet;
										try{
											myWallet = query.getTodaysContracts();
											if (myWallet.has("MyTodaysWallet")){
												arrayClientes = myWallet.getJSONArray("MyTodaysWallet");
											}
											else{
												arrayClientes = new JSONArray();
											}
										} catch (JSONException e){
											e.printStackTrace();
										}
									}
									*/
									/*
									if (query.db != null){
										query.db.close();
									}
									query = null;
									*/

                                        filteredClients = null;

                                        //ClientesAdapter adapter = getAdapterSyncedWalletClients();
                                        if (BuildConfig.isWalletSynchronizationEnabled()) {
                                            adapter = getClientesRecyclerAdapterSyncedWalletClients();
                                        } else {
                                            adapter = getClientesRecyclerAdapterClientsList();
                                        }

                                        mainActivity.runOnUiThread(() -> {
                                            /*lvClientes = (ListView) view.findViewById(R.id.listView_clientes);
                                            if (lvClientes != null) {
                                                lvClientes.setAdapter(adapter);
                                                if (listViewState != null) {
                                                    lvClientes.onRestoreInstanceState(listViewState);
                                                }
                                                lvClientes
                                                        .setDivider(getResources()
                                                                .getDrawable(
                                                                        R.drawable.divider_list_clientes));
                                                lvClientes.setOnItemClickListener(mthis);
                                            }*/
                                            if (rvClientes != null)
                                            {
                                                rvClientes.setAdapter(adapter);
                                            }
                                            btnCancelarFiltrado
                                                    .setVisibility(View.GONE);
                                            etFiltrado.setText("");

                                            listViewStateGotten = false;
                                        });
                                    });
											/*
											lvClientes = (ListView) findViewById(R.id.listView_clientes);
											lvClientes.setAdapter(adapter);
											if (listViewState != null){
												lvClientes.onRestoreInstanceState(listViewState);
											}
											lvClientes
													.setDivider(getResources()
															.getDrawable(
																	R.drawable.divider_list_clientes));
											lvClientes.setOnItemClickListener(mthis);
											btnCancelarFiltrado
													.setVisibility(View.GONE);
											etFiltrado.setText("");

											listViewStateGotten = false;
											*/
                        });
                        //	}
                        //});


                    } else if (etFiltrado.length() == 0) {
                        //arrayFiltrados = null;
                        filteredClients = null;
                        //ClientesAdapter adapter = getAdapterSyncedWalletClients();
                        if (BuildConfig.isWalletSynchronizationEnabled()) {
                            adapter = getClientesRecyclerAdapterSyncedWalletClients();
                        } else {
                            adapter = getClientesRecyclerAdapterClientsList();
                        }

                        //runOnUiThread(new Runnable() {
                        //	@Override
                        //	public void run() {
                        mainActivity.runOnUiThread(() -> {
                            /*lvClientes = (ListView) view.findViewById(R.id.listView_clientes);
                            if (lvClientes != null) {
                                lvClientes.setAdapter(adapter);
                                if (listViewState != null) {
                                    lvClientes.onRestoreInstanceState(listViewState);
                                }
                                lvClientes.setDivider(getResources().getDrawable(
                                        R.drawable.divider_list_clientes));
                                lvClientes.setOnItemClickListener(mthis);
                            }*/
                            if (rvClientes != null)
                            {
                                rvClientes.setAdapter(adapter);
                            }
                            ImageButton btnCancelarFiltrado = (ImageButton) view.findViewById(R.id.ib_cancelar_filtro);
                            btnCancelarFiltrado.setVisibility(View.GONE);

                            listViewStateGotten = false;
                        });
                        //	}
                        //});
							/*
							lvClientes = (ListView) findViewById(R.id.listView_clientes);
							lvClientes.setAdapter(adapter);
							if (listViewState != null){
								lvClientes.onRestoreInstanceState(listViewState);
							}
							lvClientes.setDivider(getResources().getDrawable(
									R.drawable.divider_list_clientes));
							lvClientes.setOnItemClickListener(mthis);
							ImageButton btnCancelarFiltrado = (ImageButton) findViewById(R.id.ib_cancelar_filtro);
							btnCancelarFiltrado.setVisibility(View.GONE);

							listViewStateGotten = false;
							*/

                    } else {

                        //runOnUiThread(new Runnable() {
                        //	@Override
                        //	public void run() {
                        final ImageButton btnCancelarFiltrado = (ImageButton) view.findViewById(R.id.ib_cancelar_filtro);
                        btnCancelarFiltrado.setVisibility(View.GONE);
                        //	}
                        //});
							/*
							final ImageButton btnCancelarFiltrado = (ImageButton) findViewById(R.id.ib_cancelar_filtro);
							btnCancelarFiltrado.setVisibility(View.GONE);
							*/
                        //ClientesAdapter adapter = getAdapterSyncedWalletClients();
                        if (BuildConfig.isWalletSynchronizationEnabled()) {
                            adapter = getClientesRecyclerAdapterSyncedWalletClients();
                        } else {
                            adapter = getClientesRecyclerAdapterClientsList();
                        }

                        //runOnUiThread(new Runnable() {
                        //	@Override
                        //	public void run() {
                        mainActivity.runOnUiThread(() -> {
                            /*lvClientes = (ListView) view.findViewById(R.id.listView_clientes);
                            if (lvClientes != null) {
                                lvClientes.setAdapter(adapter);
                                if (listViewState != null) {
                                    lvClientes.onRestoreInstanceState(listViewState);
                                }
                                lvClientes.setDivider(getResources().getDrawable(
                                        R.drawable.divider_list_clientes));
                                lvClientes.setOnItemClickListener(mthis);
                            }*/
                            rvClientes.setAdapter(adapter);
                        });
                        //	}
                        //});
							/*
							lvClientes = (ListView) mthis.findViewById(R.id.listView_clientes);
							lvClientes.setAdapter(adapter);
							if (listViewState != null){
								lvClientes.onRestoreInstanceState(listViewState);
							}
							lvClientes.setDivider(getResources().getDrawable(
									R.drawable.divider_list_clientes));
							lvClientes.setOnItemClickListener(mthis);
							*/
                    }

                    //}
                    //});


                    //}

                    //public TimerTask setData(Editable arg0) {
                    //	this.arg0 = arg0;
                    //	return this;
                    //}
                    //}.setData(arg0), DELAY);
                });

                thread.start();
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                if (arg0.length() > 0) {
                    adapter = new ClientesRecyclerAdapter<>(mainActivity.getApplicationContext(), DatabaseAssistant.getFilteredClients(arg0.toString()), ClientesRecyclerAdapter.WALLET_CLIENTS, ClientesRecycler.this);
                    rvClientes.setAdapter(adapter);
                }
            }

        });

        if (mainActivity.isConnected && !DatabaseAssistant.isThereAnyClient()) {
            try {
                showMyCustomDialog();
            } catch (Exception e) {
                Toast toast = Toast.makeText(getContext(), R.string.info_message_network_offline, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                e.printStackTrace();
            }
            getListClients();
        } else if (getArguments().getBoolean("isSupervisorOrSubstitute", false) && mainActivity.isConnected && !DatabaseAssistant.clientsFromSameCollector(getArguments().getString("clientsCollectorNumber"))) {
            try {
                showMyCustomDialog();
            } catch (Exception e) {
                Toast toast = Toast.makeText(getContext(), R.string.info_message_network_offline, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                e.printStackTrace();
            }
            getListClients();
        } else {
            Log.e("isconnected", "" + mainActivity.isConnected);

            //List<SyncedWallet> syncedClientsFromWallet = DatabaseAssistant.getClientsFromSyncedWallet();
            //Util.Log.ih("size: " + syncedClientsFromWallet.size());
            //for (int i = 0; i < syncedClientsFromWallet.size(); i++) {
            //	Util.Log.ih("wallet client: " + syncedClientsFromWallet.get(i).getClient().getName());
            //}

            if (!mainActivity.isConnected) {
                try {
                    mainActivity.showAlertDialog(
                            "",
                            getResources().getString(
                                    R.string.info_message_network_offline), true);
                } catch (Exception e) {
                    Toast toast = Toast.makeText(getContext(), R.string.info_message_network_offline, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    e.printStackTrace();
                }
            }

            try {
                showMyCustomDialog();

                //mainActivity.toastSReuse("Cargando...");
                Snackbar.make(mainActivity.mainLayout, "Cargando...", Snackbar.LENGTH_SHORT).show();

                Runnable runnable = () -> {
                    //JSONObject json;
                    //json = query.mostrarTodosLosClientes();
                    //arrayClientes = json.getJSONArray("clientes");

                    if (BuildConfig.isWalletSynchronizationEnabled()) {
                        clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
                        //clientsList = DatabaseAssistant.getAllClients();

                        mainActivity.runOnUiThread(() -> {
                            adapter = getClientesRecyclerAdapterSyncedWalletClients();
                            /*lvClientes = (ListView) view.findViewById(R.id.listView_clientes);
                            if (lvClientes != null) {
                                lvClientes.setAdapter(adapter);
                                /*lvClientes
                                        .setDivider(getResources()
                                                .getDrawable(
                                                        R.drawable.divider_list_clientes));
                                lvClientes.setOnItemClickListener(mthis);
                            }*/
                            rvClientes.setAdapter(adapter);

                            dismissMyCustomDialog();
                        });
                    } else {
                        clientsList = DatabaseAssistant.getAllClients();

                        mainActivity.runOnUiThread(() -> {
                            adapter = getClientesRecyclerAdapterClientsList();
                            /*lvClientes = (ListView) view.findViewById(R.id.listView_clientes);
                            if (lvClientes != null) {
                                lvClientes.setAdapter(adapter);
                                lvClientes
                                        .setDivider(getResources()
                                                .getDrawable(
                                                        R.drawable.divider_list_clientes));
                                lvClientes.setOnItemClickListener(mthis);
                            }*/
                            rvClientes.setAdapter(adapter);

                            dismissMyCustomDialog();
                        });
                    }
                };
                Thread thread = new Thread(runnable);

                thread.start();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        /**
         * Updates cash
         * this if statement is true after doing 'cierre de periodo'
         *
         * Web Service
         * http://50.62.56.182/ecobro/controlusuarios/getBalanceEmpresasPorCobradorYPeriodo'
         * JSONObject param
         * {
         * 		"periodo": ,
         * 		"no_cobrador": ""
         * }
         *
         */
        Log.e("va a sincronizar?", "" + getArguments().getBoolean("periodo", false));
        if (getArguments().getBoolean("periodo", false)) {

            //new Handler().postDelayed(this::saveDepositos, 1000);
            saveDepositos();
            Log.e("sincroniza,", "efectivo");
            //mainActivity.toastSReuse("Sincronizando efectivo con el servidor... espera un momento");
            Snackbar.make(mainActivity.mainLayout, "Sincronizando efectivo con el servidor... espera un momento", Snackbar.LENGTH_SHORT).show();
            JSONObject params = new JSONObject();

            try {
                if (DatabaseAssistant.isThereMoreThanOnePeriodActive()) {
                    params.put("periodo", new JSONArray(new Gson().toJson(DatabaseAssistant.getAllPeriodsActive())));
                } else {
                    params.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
                }
                params.put("no_cobrador", mainActivity.getEfectivoPreference().getString("no_cobrador", ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }

			/*RequestResult requestResult = new RequestResult(getContext());

			requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
				@Override
				public void onRequestResult(NetworkResponse response) {

					JSONArray empresas;
					JSONObject jsonResult;
					try {
						jsonResult = new JSONObject(VolleyTickle.parseResponse(response));

						for (Iterator<String> iter = jsonResult.keys(); iter.hasNext(); ) {
							String key = iter.next();
							empresas = jsonResult.getJSONArray(key);
							Util.Log.ih("empresas = " + empresas);

							SharedPreferences.Editor efectivoEditor = mainActivity.getEfectivoEditor();

							try {
								for (int i = 0; i < empresas.length(); i++) {
									JSONObject empresa;
									try {
										empresa = empresas.getJSONObject(i);

										String nombre = DatabaseAssistant.getSingleCompany(empresa.getString("empresa")).getName();

										float cantidad = (float) empresa.getDouble("cobros");
										cantidad = cantidad - (float) empresa.getDouble("depositos");

										efectivoEditor.putFloat(nombre, cantidad).apply();
										//Util.Log.ih("commit boolean = " + commitB);
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}

							} catch (NullPointerException e) {
								e.printStackTrace();
								onRequestError(response);
							}
						}
					} catch (JSONException e) {
						//nothing here...
					}

					mainActivity.toastSReuse("Efectivo Sincronizado.");
					try {
						putInfo();
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onRequestError(NetworkResponse error) {
					Log.e("balanceEmpresas", "Error al obtener balance por empresas");
				}
			});

			requestResult.executeRequest(ConstantsPabs.urlGetBalanceEmpresaPorCobradorYPeriodo, Request.Method.POST, params);*/

            NetService service = new NetService(
                    ConstantsPabs.urlGetBalanceEmpresaPorCobradorYPeriodo,
                    params);
            NetHelper helper = NetHelper.getInstance();
            helper.ServiceExecuter(service, getContext(), new NetHelper.onWorkCompleteListener() {

                @Override
                public void onError(Exception e) {
                    Log.e("balanceEmpresas", "Error al obtener balance por empresas");
                }

                @Override
                public void onCompletion(String result) {
                    //SqlQueryAssistant query = new SqlQueryAssistant(
                    //		ApplicationResourcesProvider.getContext());

                    //NEW PROCESS
                    //NEW PROCESS
                    JSONArray empresas;
                    JSONObject jsonResult;
                    try {
                        jsonResult = new JSONObject(result);

                        for(Iterator<String> iter = jsonResult.keys(); iter.hasNext(); ) {
                            String key = iter.next();
                            empresas = jsonResult.getJSONArray(key);
                            Util.Log.ih("empresas = " + empresas);

                            SharedPreferences.Editor efectivoEditor = mainActivity.getEfectivoEditor();

                            try {
                                for (int i = 0; i < empresas.length(); i++) {
                                    JSONObject empresa;
                                    try {
                                        empresa = empresas.getJSONObject(i);

                                        String nombre = DatabaseAssistant.getSingleCompany(empresa.getString("empresa")).getName();

                                        float cantidad = (float) empresa.getDouble("cobros");
                                        cantidad = cantidad - (float) empresa.getDouble("depositos");

                                        efectivoEditor.putFloat(nombre, cantidad).apply();
                                        //Util.Log.ih("commit boolean = " + commitB);
                                    } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                onError(e);
                            }
                        }
                    } catch (JSONException e) {
                        //nothing here...
                    }

                    //OLD PROCESS
					/*
					JSONArray empresas = null;
					try {
						empresas = new JSONObject(result)
								.getJSONArray("result");
					} catch (JSONException e) {
						e.printStackTrace();
					}
					Editor efectivoEditor = getEfectivoEditor();
					//float cash = 0;
					for (int i = 0; i < empresas.length(); i++) {
						JSONObject empresa;
						try {
							empresa = empresas.getJSONObject(i);
							String nombre = DatabaseAssistant.getSingleCompany(
									empresa.getString("empresa")).getName();
							//String nombre = query.mostrarEmpresa(
							//		empresa.getString("empresa")).getString(
							//		Empresa.nombre_empresa);
							float cantidad = (float) empresa
									.getDouble("cobros");


							//cash += cantidad;

							cantidad = cantidad
									- (float) empresa.getDouble("depositos");
							efectivoEditor.putFloat(nombre, cantidad).apply();

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					*/

					/*
					if (query.db != null){
						query.db.close();
					}
					query = null;
					*/

					/*
					if (cash == 0) {
						Preferences preferencesCompleteAmounts = new Preferences(ApplicationResourcesProvider.getContext());
						preferencesCompleteAmounts.saveSetCompleteAmount(false);
					}*/


                    //mainActivity.toastSReuse("Efectivo Sincronizado.");
                    Snackbar.make(mainActivity.mainLayout, "Efectivo sincronizado", Snackbar.LENGTH_SHORT).show();
                    try {
                        putInfo();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

		/*
		ProgressDialog progressDialog = new ProgressDialog(this);
		if (progressDialog != null) {
			progressDialog.setCancelable(false);
			progressDialog.show();
		}
		checkDateToSetWalletSynced();
		if (!isFinishing()){
			if (progressDialog != null && progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}
		*/

        enviarDepositos();

        if (com.jaguarlabs.pabs.util.BuildConfig.isAutoPrintingEnable()) {
            Handler handler = new Handler();
            handler.postDelayed(this::randomListClick, 10000);
        }

        preferencesPendingTicket = new Preferences(getContext());

        //getNotificationAndShow();

        Util.Log.ih("datosCobrador = " +  datosCobrador != null ? datosCobrador.toString() : "null");

        if (datosCobrador.has("fecha_cierre"))
        {
            try {
                fechaCierre = datosCobrador.getString("fecha_cierre");
            }
            catch (JSONException ex)
            {
                ex.printStackTrace();
            }
        }
        else
        {
            fechaCierre = "2018-01-31 10:52:47";
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (timerNotificaciones != null) {
            timerNotificaciones.cancel();
            timerNotificaciones = null;
        }
        if (timerSendPaymentsEachTenMinutes != null){
            //Util.Log.ih("onDestroy() -> timer = " + timerSendPaymentsEachTenMinutes);
            timerSendPaymentsEachTenMinutes.cancel();
            timerSendPaymentsEachTenMinutes = null;
            //Util.Log.ih("onDestroy()_ -> timer = " + timerSendPaymentsEachTenMinutes);
        }
        wl.release();

        if (previousPeriodsHandler != null)
            previousPeriodsHandler.removeCallbacks(previousRunnable);

        /**
         * Broadcast
         * This broadcast gets call when time or timezone changes
         */
		/*
		m_timeChangedReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {

				/**
				 * After
				 *
				 * If time changes for more than 21 minutes,
				 * ends session
				 *
				 * Jordan Alvarez
				 */
		/*
				if ( mTime != 0 ){
					if ( checkDate() ){
						final String action = intent.getAction();

						if (action.equals(Intent.ACTION_TIME_CHANGED)
								|| action.equals(Intent.ACTION_TIMEZONE_CHANGED)) {

							//LogRegister.endSessionByBroadcast(mTime, newTime);
							Mint.logEvent("Cierre de Sesion - broadcast " + loadUser() , MintLogLevel.Info);

							Util.Log.ih("Restarting Application!");
							Util.Log.ih("Hora SEGUNDA: " + System.currentTimeMillis());
							finish();
							android.os.Process.killProcess(android.os.Process.myPid());
						}
					}
				}
				mTime = new Date().getTime();

				/*
				Before
				final String action = intent.getAction();

				 if (action.equals(Intent.ACTION_TIME_CHANGED)
				 || action.equals(Intent.ACTION_TIMEZONE_CHANGED)
				 || action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED
				 || action.equals(Intent.ACTION_DATE_CHANGED)

				if (action.equals(Intent.ACTION_TIME_CHANGED)
						|| action.equals(Intent.ACTION_TIMEZONE_CHANGED)) {
					Util.Log.ih("Restarting Application!");
					Util.Log.ih("Hora SEGUNDA: " + System.currentTimeMillis());
					finish();
					android.os.Process.killProcess(android.os.Process.myPid());
				}
				*//*
			}
		};
		*/

        //registerReceiver(m_timeChangedReceiver, s_intentFilter);

        //unregisterReceiver(m_timeChangedReceiver);

        mthis = null;

    }

    @Override
    public void onResume() {
        super.onResume();

        MainActivity.CURRENT_TAG = MainActivity.TAG_CIERRE_INFORMATIVO;

        if (Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION) && Nammu.checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION))
            ApplicationResourcesProvider.startLocationUpdates();

        Deposito.depositosIsActive = false;
        //enviarCancelado();
        //hasActiveInternetConnection(this);
        mthis = this;
        calcularTotal();
        try {
            putInfo();
        } catch (JSONException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        final LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        final boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);


        mainActivity.getEfectivoEditor().putInt("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0)).commit();

        /**
         * when the activity calls onResume, checks if GPS is enabled, if not, shows a dialog
         * telling to enable it until it's enabled
         */
        if (!isGPSEnabled) {
            final AlertDialog.Builder dialog = getBuilder();
            try {
                dialog.setTitle("Advertencia:");
                dialog.setMessage("Enciende el GPS para continuar.");
                dialog.setPositiveButton("Aceptar", (dialog1, which) -> {
                    boolean isGPSEnabled2 = locationManager
                            .isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (!isGPSEnabled2) {
                        dialog.create().show();
                    }
                });
                dialog.create().show();
            } catch (Exception e) {
                Toast toast = Toast.makeText(getContext(), "Advertencia: Enciende el GPS para continuar.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                e.printStackTrace();
            }
        }

        //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());

        // enviando los ubicaciones offline
		/*
		try {
			JSONObject jsonUbicaciones = query.mostrarTodasLasUbicaciones();
			if (jsonUbicaciones.has("UBICACION")) {
				arrayUbicacionesOffline = jsonUbicaciones
						.getJSONArray("UBICACION");
				if (arrayUbicacionesOffline.length() > 0) {
					if (isConnected) {
						Toast.makeText(ApplicationResourcesProvider.getContext(),
								"Enviando ubicaciones.", Toast.LENGTH_SHORT)
								.show();
						saveUbicaciones();
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		*/

        // enviando los notificaciones leidas
        try {
            String viewedNotifications = DatabaseAssistant.getViewedNotifications();
            //JSONObject jsonTmp = query.mostrarNotificacionesLeidas();
            if (viewedNotifications.length() > 0) {
                if (mainActivity.isConnected) {
                    registarNotificacionesLeidas();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        checkPendingTicket();

        checkPendingPeriod();

        checkForUpdate();

        //mainActivity.checkIfShouldClosePeriod(mainActivity, fechaCierre);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        if (!hidden) {

            if (etFiltrado.length() > 0 && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CIERRE_INFORMATIVO))
            {
                etFiltrado.setText(etFiltrado.getText().toString());
            }

            MainActivity.CURRENT_TAG = MainActivity.TAG_CLIENTES;

            //updateCurrentMonto();

            try {
                putInfo();
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {

        /**
         * Broadcast
         * This broadcast gets call when time or timezone changes
         */
		/*
		m_timeChangedReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {

				/**
				 * After
				 *
				 * If time changes for more than 21 minutes,
				 * ends session
				 *
				 * Jordan Alvarez
				 */
		/*
				if ( mTime != 0 ){
					if ( checkDate() ){
						final String action = intent.getAction();

						if (action.equals(Intent.ACTION_TIME_CHANGED)
								|| action.equals(Intent.ACTION_TIMEZONE_CHANGED)) {

							Mint.logEvent("Cierre de Sesion - broadcast " + loadUser() , MintLogLevel.Info);
							//LogRegister.endSessionByBroadcast(mTime, newTime);

							Util.Log.ih("Restarting Application!");
							Util.Log.ih("Hora SEGUNDA: " + System.currentTimeMillis());
							finish();
							android.os.Process.killProcess(android.os.Process.myPid());
						}
					}
				}
				mTime = new Date().getTime();

				/*
				Before
				final String action = intent.getAction();

				 if (action.equals(Intent.ACTION_TIME_CHANGED)
				 || action.equals(Intent.ACTION_TIMEZONE_CHANGED)
				 || action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED
				 || action.equals(Intent.ACTION_DATE_CHANGED)

				if (action.equals(Intent.ACTION_TIME_CHANGED)
						|| action.equals(Intent.ACTION_TIMEZONE_CHANGED)) {
					Util.Log.ih("Restarting Application!");
					Util.Log.ih("Hora SEGUNDA: " + System.currentTimeMillis());
					finish();
					android.os.Process.killProcess(android.os.Process.myPid());
				}
				*//*
			}
		};
		*/

        //registerReceiver(m_timeChangedReceiver, s_intentFilter);

        //unregisterReceiver(m_timeChangedReceiver);

        super.onStop();
    }

    //endregion

    //region Methods

    //region Public
    private int arrayListSelected = 0;

    private void updateCurrentMonto()
    {
        JSONObject json = new JSONObject();
        try {
            json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
            json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
        } catch (Exception e) {
            e.printStackTrace();
        }

		/*JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlListaPagosCobrador, json, response -> {
				Util.Log.eh("onCompletion");
				Util.Log.eh(response.toString());

				try {
					arrayCierreList.put(
							response.getString("periodo"),
							response);
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if ( !DatabaseAssistant.isThereMoreThanOnePeriodActive() ) {

					manage_GetListaPagosByCobrador(response);
				} else {
					periodsCounter += 1;
					if (periodsCounter == periodsActive) {
						Util.Log.eh("setSelection " + (periodsActive - 1));
						spinnerPeriodSelector.setSelection( periodsActive - 1 );
						Util.Log.ih("spinnerPeriodSelector.getSelectedItem() = " + spinnerPeriodSelector.getSelectedItem());
					}
					Util.Log.eh("no entro en manage_GetListaPagosByCobrador");
				}
		}, error -> {
			Log.e(null, "Error: listaPagosCobrador");
			mostrarMensajeError(ConstantsPabs.listaPagosCobrador);
		});

		request.setShouldCache(false);

		VolleySingleton.getInstance(mainActivity.getApplicationContext()).addToRequestQueue(request);*/

        NetHelper.getInstance().ServiceExecuter(
                new NetService(ConstantsPabs.urlListaPagosCobrador, json),
                getContext(), new NetHelper.onWorkCompleteListener() {

                    @Override
                    public void onCompletion(String result) {
                        try {
                            if ( !DatabaseAssistant.isThereMoreThanOnePeriodActive() ) {
                                JSONObject json = new JSONObject(result);

                                if (json.has("result"))
                                    llenarLista(json);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e(null, "Error: listaPagosCobrador");
                        mostrarMensajeError(ConstantsPabs.listaPagosCobrador);
                    }
                });
    }

    private void llenarLista(JSONObject json) {

        JSONArray arrayCierre;
        try {
            //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
            //try {
            //query.deleteCancelados();
            DatabaseAssistant.deleteAllCanceled();
            //} catch (JSONException e) {
            //	e.printStackTrace();
            //}catch(NullPointerException e){
            //	e.printStackTrace();
            //}

            arrayCierre = json.getJSONArray("result");

			/*
			JSONArray asd = new JSONArray();
			for (int r = 0; r < 450; r++){
				JSONObject a = new JSONObject()
						.put("no_contrato", "00472124" + r)
						.put("nombre", "qweasdzxc" + r)
						.put("no_cobro", "6131348" + r)
						.put("no_cliente", "1AF2400472124" + r)
						.put("folio", "----")
						.put("copias", "0")
						.put("monto", "0")
						.put("status", "3")
						.put("empresa", "01")
						.put("tiempo", "2016-03-08 17:46:24");
				asd.put(a);
			}
			JSONObject qwe = new JSONObject();
			qwe.put("result",asd);
			arrayCierre = qwe.getJSONArray("result");
			*/
            boolean cancellationAvailable = true;
            try {
                if (json.has("result_active_period")) {
                    JSONObject resultActivePeriod = json.getJSONArray("result_active_period").getJSONObject(0);
                    if (resultActivePeriod.getString("fecha_fin_periodo").length() > 4) {
                        Util.Log.ih("cancellation not available");
                        cancellationAvailable = false;
                    }
                } else {
                    Util.Log.ih("no tiene result_active_period");
                }
            } catch (JSONException e){
                e.printStackTrace();
            }

            //Preferences preferencesCompleteAmounts = new Preferences(ApplicationResourcesProvider.getContext());

            for (int cont = 0; cont < arrayCierre.length(); cont++) {
                JSONObject jsonTmp = arrayCierre.getJSONObject(cont);

                // Log.e("incierre Programa" + jsonTmp.getInt("inCierre"),
                // jsonTmp.getString("no_contrato"));
                if (jsonTmp.getInt("status") == 7) {
                    Log.d("tickeCancelado Programa", "" + jsonTmp.toString(1));
                    String monto = jsonTmp.getString("monto");
                    String empresa = jsonTmp.getString("empresa");
                    String noCliente = jsonTmp.getString("no_cliente");
                    String paymentDate = jsonTmp.getString("tiempo");
                    //query.insertarTicketCancelado(monto, empresa, noCliente);
                    DatabaseAssistant.insertCanceledPaymentFromCierre(monto, empresa, noCliente, paymentDate, mainActivity.getEfectivoPreference().getInt("periodo", 0), "");

                    /**
                     * Sums the amount of the cancelled payment to the company it belongs.
                     * This process is necesary when the application data is erased
                     * because if not, there will be a problem with the getTotalEfectivo() method
                     * which calculates final cash to deposit either on a bank or office.
                     *
                     * This process will be only done if application data was erased
                     */

                        float efectivo = mainActivity.getEfectivoPreference().getFloat(
                                DatabaseAssistant.getSingleCompany(empresa).getName(), 0
                                //query.mostrarEmpresa(empresa).getString(Empresa.nombre_empresa), 0
                        );
                        efectivo += Float.parseFloat(monto);
                        mainActivity.getEfectivoEditor().putFloat(
                                DatabaseAssistant.getSingleCompany(empresa).getName(), efectivo
                                //query.mostrarEmpresa(empresa).getString(Empresa.nombre_empresa), efectivo
                        );
                        mainActivity.getEfectivoEditor().apply();

                }
            }

            putInfo();

        } catch (JSONException e) {

            //write stacktrace into a .txt file
            //StackTraceHandler writeStackTrace = new StackTraceHandler();
            //writeStackTrace.uncaughtException("CIERREINFORMATIVO.LlenarLista", e);

            e.printStackTrace();
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    private void checkForUpdate()
    {
        JSONObject data = new JSONObject();

        try {

            data.put("no_cobrador", datosCobrador.getString("no_cobrador"));

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlCheckUpdate, data, response -> {
                if (!response.has("error")) {
                    try {
                        int checkUpdate = response.getInt("actualizar");

                        Log.d("UpdateCheck", "Debe actualizar: " + (checkUpdate == 1 ? "Si" : "No"));

                        if (checkUpdate == 1 && !updatePostponed)
                            checkVersion();
                        else if (checkUpdate == 2)
                            forceUpdate();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, error -> {
                if (timerUpdate != null)
                    timerUpdate.cancel();

                timerUpdate = new Timer();

                timerUpdate.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        updatePostponed = false;
                        Log.d("UpdatePostponed", "Time Reached");
                    }
                }, (1000 * 60) * 60);

                updatePostponed = true;

                error.printStackTrace();
            });
            if (!updatePostponed)
                VolleySingleton.getInstance(mainActivity.getApplicationContext()).addToRequestQueue(request);
        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    private void checkVersion()
    {
        StringRequest request = new StringRequest(Request.Method.GET, ConstantsPabs.urlCheckVersion, response -> {
            try {
                PackageInfo info = mainActivity.getPackageManager().getPackageInfo(mainActivity.getPackageName(), 0);
                String version = info.versionName;

                int comparison = version.compareTo(response);

                Log.d("UpdateComparison", "Comparison: " + comparison);

                if (!version.equals(response) && comparison < 0)
                {
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext());

                    builder.setTitle("Actualización");
                    builder.setMessage("Hay una nueva versión de la aplicación lista para descargar e instalar. \n\nVersión actual: " + version +"\n\nVersión nueva: " + response);
                    builder.setCancelable(false);
                    builder.setPositiveButton("Actualizar", (dialog1, which) -> {
                        String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS ) + "/";
                        String fileName = "pabs.apk";
                        final Uri uri = Uri.parse("file://" + destination + fileName);

                        showMyCustomDialog();

                        LogRegister.registrarActualizacion("Aceptada");

                        File file = new File(destination, fileName);

                        if (file.exists())
                            file.delete();

                        DownloadManager.Request downloadRequest = new DownloadManager.Request(Uri.parse(ConstantsPabs.urlUpdateApp + response + ".apk"));
                        //downloadRequest.setDescription("Descargando PABS");
                        downloadRequest.setTitle("Actualización PABS");

                        downloadRequest.setDestinationUri(uri);
                        downloadRequest.allowScanningByMediaScanner();
                        downloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                        final DownloadManager manager = (DownloadManager) mainActivity.getSystemService(Context.DOWNLOAD_SERVICE);
                        if (manager != null) {
                            final long downloadId = manager.enqueue(downloadRequest);

                            BroadcastReceiver onComplete = new BroadcastReceiver() {
                                @Override
                                public void onReceive(Context context, Intent intent) {
                                    if (file.exists()) {
                                        Uri apkUri = Uri.fromFile(file);
                                        Intent update = new Intent();
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            apkUri = FileProvider.getUriForFile(getContext(), com.jaguarlabs.pabs.BuildConfig.APPLICATION_ID + ".provider", file);
                                            update.setAction(Intent.ACTION_INSTALL_PACKAGE);
                                            update.setDataAndType(apkUri, "application/vnd.android.package-archive");
                                            update.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                            startActivity(update);
                                        } else {
                                            update.setAction(Intent.ACTION_VIEW);
                                            update.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            update.setDataAndType(apkUri, "application/vnd.android.package-archive");
                                            startActivity(update);
                                        }

                                        Log.i("ApkUpdate", manager.getMimeTypeForDownloadedFile(downloadId));

                                        mainActivity.unregisterReceiver(this);
                                        mainActivity.finish();
                                    }
                                    else
                                    {
                                        dismissMyCustomDialog();

                                        if (timerUpdate != null)
                                            timerUpdate.cancel();

                                        timerUpdate = new Timer();

                                        manager.remove(downloadId);

                                        timerUpdate.schedule(new TimerTask() {
                                            @Override
                                            public void run() {
                                                updatePostponed = false;
                                                Log.d("UpdatePostponed", "Time Reached");
                                            }
                                        }, (1000 * 60) * 60);
                                        updatePostponed = true;

                                        mainActivity.toastL("No se encontro el archivo de actualización");
                                    }
                                }
                            };

                            mainActivity.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                        }
                    });
                    builder.setNegativeButton("Posponer 1 hora", (dialog1, which) -> {

                        if (timerUpdate != null)
                            timerUpdate.cancel();

                        timerUpdate = new Timer();

                        timerUpdate.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                updatePostponed = false;
                                Log.d("UpdatePostponed", "Time Reached");
                            }
                        }, (1000 * 60) * 60);

                        updatePostponed = true;

                        Log.d("UpdatePostponed", "true");
                        LogRegister.registrarActualizacion("Pospuesta");
                    });

                    builder.show();
                }
            }
            catch (PackageManager.NameNotFoundException ex)
            {
                ex.printStackTrace();
            }
        }, error -> {

        });

        VolleySingleton.getInstance(mainActivity.getApplicationContext()).addToRequestQueue(request);
    }

    private void forceUpdate() {
        StringRequest request = new StringRequest(Request.Method.GET, ConstantsPabs.urlCheckVersion, response -> {
            String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
            String fileName = "pabs.apk";
            final Uri uri = Uri.parse("file://" + destination + fileName);

            showMyCustomDialog();

            File file = new File(destination, fileName);

            if (file.exists())
                file.delete();

            DownloadManager.Request downloadRequest = new DownloadManager.Request(Uri.parse(ConstantsPabs.urlUpdateApp + response + ".apk"));
            //downloadRequest.setDescription("Descargando PABS");
            downloadRequest.setTitle("Actualización PABS");

            downloadRequest.setDestinationUri(uri);
            downloadRequest.allowScanningByMediaScanner();
            downloadRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

            final DownloadManager manager = (DownloadManager) mainActivity.getSystemService(Context.DOWNLOAD_SERVICE);
            if (manager != null) {
                final long downloadId = manager.enqueue(downloadRequest);

                BroadcastReceiver onComplete = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (file.exists()) {
                            Uri apkUri = Uri.fromFile(file);
                            Intent update = new Intent();
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                apkUri = FileProvider.getUriForFile(getContext(), com.jaguarlabs.pabs.BuildConfig.APPLICATION_ID + ".provider", file);
                                update.setAction(Intent.ACTION_INSTALL_PACKAGE);
                                update.setDataAndType(apkUri, "application/vnd.android.package-archive");
                                update.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                startActivity(update);
                            } else {
                                update.setAction(Intent.ACTION_VIEW);
                                update.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                update.setDataAndType(apkUri, "application/vnd.android.package-archive");
                                startActivity(update);
                            }

                            Log.i("ApkUpdate", manager.getMimeTypeForDownloadedFile(downloadId));

                            mainActivity.unregisterReceiver(this);
                            mainActivity.finish();
                        } else {
                            dismissMyCustomDialog();
                            timerUpdate = new Timer();

                            manager.remove(downloadId);

                            timerUpdate.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    updatePostponed = false;
                                }
                            }, (1000 * 60) * 60);
                            updatePostponed = true;

                            mainActivity.toastL("No se encontro el archivo de actualización");
                        }
                    }
                };

                mainActivity.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
            }
        }, error -> {

        });

        VolleySingleton.getInstance(mainActivity.getApplicationContext()).addToRequestQueue(request);
    }

    /**
     * Sets the number of notifications on the notifications counter
     * and updates number of notification on the SharedPreferences file.
     * if it's connected to internet, will call registarNotificacionesLeidas()
     * to try to register read notification to server.
     */
    public void actualizarNotificaciones() {
        TextView tvICon = (TextView) getView().findViewById(R.id.tv_icon_notificaciones);
        int notificacionesActuales = Integer.parseInt(tvICon.getText()
                .toString());
        notificacionesActuales = notificacionesActuales - 1;
        tvICon.setText("" + notificacionesActuales);
        SharedPreferences.Editor editor = getContext().getSharedPreferences("PABS_Main",
                Context.MODE_PRIVATE).edit();
        editor.putString("numero_notificaciones", "" + notificacionesActuales);
        editor.apply();
        if (mainActivity.isConnected) {
            registarNotificacionesLeidas();
        }
    }

    /**
     * update collector's cash
     * @param efectivo
     */
    public void agregarEfectivoDatosCobrador(String efectivo) {
        if (efectivo.length() > 0) {
            try {
                datosCobrador.put("efectivo", efectivo);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        modificarInfoToLoginAndSplash();
    }

    /**
     * start inactivity timer
     * timer which ends session after 20 minutes
     */
    public void llamarTimerInactividad() {
        //iniciarHandlerInactividad();
    }

    /**
     * sends to server all payments made that are not sync yet
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controlpagos/registerPagos'
     * JSONObject param
     * {
     *     "no_cobrador": "",
     *     "pagos": {}
     *     "periodo":
     * }
     */
    public void registerPagos() {

        Util.Log.ih("register pagos");
        JSONObject json = new JSONObject();

        try {
            //if (arrayCobrosOffline == null) {
            //	arrayCobrosOffline = new SqlQueryAssistant(ApplicationResourcesProvider.getContext())
            //			.obtenerCobrosDesincronizados().getJSONArray("cobros");
            //}
            try {
                TelephonyManager manager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
                String deviceid = null;
                if (Nammu.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        deviceid = manager.getImei();
                    else
                        deviceid = manager.getDeviceId();
                }
                json.put("imei", deviceid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //if (offlinePayments == null){
            offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();
            //}
            json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
            //json.put("pagos", arrayCobrosOffline);
            json.put("pagos", new JSONArray( new Gson().toJson( offlinePayments ) ));
            json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));

            Log.e("cobros", new Gson().toJson(offlinePayments));
        } catch (Exception e) {
            e.printStackTrace();
        }

		/*RequestResult requestResult = new RequestResult(getContext());

		requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
			@Override
			public void onRequestResult(NetworkResponse response) {
				try
				{
					manage_RegisterPagosOffiline(new JSONObject(VolleyTickle.parseResponse(response)));
				}
				catch (JSONException ex)
				{
					ex.printStackTrace();
				}
			}

			@Override
			public void onRequestError(NetworkResponse error) {
				Log.e("regPagosOffline", "Error al registrar pagos offline");
			}
		});

		requestResult.executeRequest(ConstantsPabs.urlregisterPagosOffline, Request.Method.POST, json);*/

        NetService service = new NetService(
                ConstantsPabs.urlregisterPagosOfflineMain, json);
        NetHelper helper = NetHelper.getInstance();
        helper.ServiceExecuter(service, getContext(), new NetHelper.onWorkCompleteListener() {

            @Override
            public void onCompletion(String result) {
                try {
                    manage_RegisterPagosOffiline(new JSONObject(result));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Exception e) {
                Log.e("regPagosOffline", "Error al registrar pagos offline");
            }
        });
    }

    public void registerPagostxt() {

        Util.Log.ih("register pagos");
        JSONObject json = new JSONObject();

        try {
            //if (arrayCobrosOffline == null) {
            //	arrayCobrosOffline = new SqlQueryAssistant(ApplicationResourcesProvider.getContext())
            //			.obtenerCobrosDesincronizados().getJSONArray("cobros");
            //}
            try {
                TelephonyManager manager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
                String deviceid = null;
                if (Nammu.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        deviceid = manager.getImei();
                    else
                        deviceid = manager.getDeviceId();
                }
                json.put("imei", deviceid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (offlinePayments == null){
                offlinePayments = LogRegister.getPaymentsFromTxt(datosCobrador.getString("no_cobrador"));
            }
            json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
            //json.put("pagos", arrayCobrosOffline);
            json.put("pagos", new JSONArray( new Gson().toJson( offlinePayments ) ));
            json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));

            Log.e("cobros", offlinePayments.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

		/*RequestResult requestResult = new RequestResult(getContext());

        requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
            @Override
            public void onRequestResult(NetworkResponse response) {
                try
                {
                    manage_RegisterPagosOffiline(new JSONObject(VolleyTickle.parseResponse(response)));
                }
                catch (JSONException ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onRequestError(NetworkResponse error) {
                Log.e("regPagosOffline", "Error al registrar pagos offline");
            }
        });

        requestResult.executeRequest(ConstantsPabs.urlregisterPagosOfflineMain, Request.Method.POST, json);*/

        NetService service = new NetService(
                ConstantsPabs.urlregisterPagosOfflineMain, json);
        NetHelper helper = NetHelper.getInstance();
        helper.ServiceExecuter(service, getContext(), new NetHelper.onWorkCompleteListener() {

            @Override
            public void onCompletion(String result) {
                try {
                    manage_RegisterPagosOffiline(new JSONObject(result));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Exception e) {
                Log.e("regPagosOffline", "Error al registrar pagos offline");
            }
        });
    }

    /**
     * finish refresh button animation
     * @param refreshButon
     */
    public void finishRefreshButtonAnimation(ImageView refreshButon){
        refreshButon.clearAnimation();
    }

    /**
     * Set number of notifications
     * @param notificaciones
     */
    public void setNumeroNotificaciones(int notificaciones) {
        TextView tvICon = (TextView) getView().findViewById(R.id.tv_icon_notificaciones);
        tvICon.setText(String.format(Locale.getDefault(), "%d", notificaciones));
    }

    //endregion

    //region Private

    private void executeSendOsticket(){
        try {
            Util.Log.ih("trying to send tickets...");

            ModelOsticketRequest modelOsticketRequest = new ModelOsticketRequest();
            modelOsticketRequest.setCollectorNumber( Integer.parseInt( datosCobrador.getString("no_cobrador") ) );
            modelOsticketRequest.setOsticketTickets( DatabaseAssistant.getAllOsticketsPendingToSend() );

            if ( modelOsticketRequest.getOsticketTickets().size() > 0 ) {
                OsticketRequest osticketRequest = new OsticketRequest(modelOsticketRequest);
                spiceSendOsticket.executeRequest(osticketRequest);
            } else {
                Util.Log.ih("No hay tickets para enviar a osticket");
            }
        } catch (JSONException e){
            Util.Log.eh("Error: No se pudo obtener el numero de cobrador");
        } catch (NullPointerException e){
            e.printStackTrace();
            Util.Log.eh("Error: No se enviaron los tickets");
        }
    }

    private void updateClientList(int position, boolean isVisit) {
        SyncedWallet sw = clientListFromSyncedWallet.get(position);

        if (isVisit) {
            if (sw.isFPAPending() || (!sw.isFPA() && !sw.isFPAPending()) && !sw.getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT)) {
                sw.setVisitCounter(sw.getVisitCounter() + 1);
                if (sw.getVisitCounter() >= 2) {

                    Util.Log.ih("aquiaquiaquiaquiaquiaquiaquiaquiaquiaquiaquiaquiaqui");

                    sw.setShowColor(false);
                    sw.setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);

                    clientListFromSyncedWallet.remove(position);

                    for (int i = clientListFromSyncedWallet.size() - 1; i >= 0; i--) {
                        if (!clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT) &&
                                !clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT)) {
                            clientListFromSyncedWallet.add(i + 1, sw);
                            break;
                        }
                    }
                } else {
                    if (sw.getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT)) {
                        //sw.setShowColor(false);
                        //if (sw.isFPAPending() || (!sw.isFPA() && !sw.isFPAPending())) {
                        sw.setColor(SyncedWallet.COLOR_TODAY_CONTRACT);
                        //sw.setVisitCounter(sw.getVisitCounter() + 1);
                        sw.setShowColor(true);
                        clientListFromSyncedWallet.remove(position);
                        for (int i = clientListFromSyncedWallet.size() - 1; i >= 0; i--) {
                            if (clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_TODAY_CONTRACT) ||
                                    clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_PENDING_CONTRACT) ||
                                    clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_PENDING_LEVEL_2_CONTRACT)) {
                                clientListFromSyncedWallet.add(i + 1, sw);
                                break;
                            } else if (i == 0) {
                                clientListFromSyncedWallet.add(i, sw);
                                break;
                            }
                        }
                        //} else {
                        //sw.setVisitCounter(sw.getVisitCounter() - 1);
                        //}
                        //} else if (sw.getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT)) {
                        //sw.setVisitCounter(sw.getVisitCounter() - 1);
                        //}
                    }
                }
            }
        } else {
            sw.setShowColor(false);
            sw.setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);
            sw.setFPA(false);
            sw.setFPAPending(false);

            clientListFromSyncedWallet.remove(position);

            int media = clientListFromSyncedWallet.size() / 2 - 1;

            for (int i = clientListFromSyncedWallet.size() - 1; i >= 0; i--) {
                if (!clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT) &&
                        !clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_SUSPENDED_CONTRACT)) {
                    clientListFromSyncedWallet.add(i + 1, sw);
                    break;
                }
            }
        }
    }

    private void updateListView(Boolean... reloadClientsFromSyncedWallet){
        //reload clients if needed
        if (reloadClientsFromSyncedWallet.length > 0){
            /*new Thread(() -> {
                clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
                mainActivity.runOnUiThread(() -> adapter.setItemList(clientListFromSyncedWallet));
            }).start();*/
            clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
            //mainActivity.runOnUiThread(() -> adapter.setItemList(DatabaseAssistant.getClientsFromSyncedWallet()));
            adapter.setItemList(DatabaseAssistant.getClientsFromSyncedWallet());
        }

        mainActivity.runOnUiThread(() ->{
            adapter = getClientesRecyclerAdapterSyncedWalletClients();
            //adapter.notifyDataSetChanged();
            /*lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);
            if (lvClientes != null) {
                lvClientes.setAdapter(adapter);
                if (listViewState != null) {
                    Util.Log.ih("trying to restore ListView state...");
                    lvClientes.onRestoreInstanceState(listViewState);
                }
                lvClientes.setDivider(getResources().getDrawable(R.drawable.divider_list_clientes));
                lvClientes.setOnItemClickListener(Clientes.this);
            }*/
            rvClientes.setAdapter(adapter);
        });
        clientListFromSyncedWallet = (List<SyncedWallet>) adapter.getItemList();
    }

    /**
     * CustomComparator class which is used to compare two objects to determine their
     * ordering with respect to each other.
     */
    private class CustomComparatorToSortByDistanceFromSyncedWallet implements Comparator<ModelClient> {

        /**
         * Compares the two specified objects to determine their relative ordering.
         * The ordering implied by the return value of this method for all possible
         * pairs of (lhs, rhs) should form an equivalence relation.
         * @param lhs an Object
         * @param rhs an Object to compare with lhs
         * @return an integer < 0 if lhs is less than rhs, 0 if they are equal, and > 0 if lhs is greater than rhs.
         */
        @Override
        public int compare(ModelClient lhs, ModelClient rhs) {
            Double firstDistance = 0.0;
            firstDistance = Double.parseDouble( lhs.getDistance() );

            Double secondDistance = 0.0;
            secondDistance = Double.parseDouble( rhs.getDistance() );

            int comparisonResult = -1;
            if (firstDistance > secondDistance) {
                comparisonResult = 1;
            } else if (firstDistance.equals(secondDistance)) {
                comparisonResult = 0;
            }
            return comparisonResult;
        }
    }

    /**
     * CustomComparator class which is used to compare two objects to determine their
     * ordering with respect to each other.
     */
    private class CustomComparatorToSortByDistance implements Comparator<Clients>{

        /**
         * Compares the two specified objects to determine their relative ordering.
         * The ordering implied by the return value of this method for all possible
         * pairs of (lhs, rhs) should form an equivalence relation.
         * @param lhs an Object
         * @param rhs an Object to compare with lhs
         * @return an integer < 0 if lhs is less than rhs, 0 if they are equal, and > 0 if lhs is greater than rhs.
         */
        @Override
        public int compare(Clients lhs, Clients rhs) {
            Double firstDistance = 0.0;
            firstDistance = Double.parseDouble( lhs.getDistance() );

            Double secondDistance = 0.0;
            secondDistance = Double.parseDouble( rhs.getDistance() );

            int comparisonResult = -1;
            if (firstDistance > secondDistance) {
                comparisonResult = 1;
            }
            return comparisonResult;
        }
    }

    /**
     * Sets all deposits stored in arrayCobrosOffline variable which were just sent
     * to server as sync (sync <- 1) so that does not resend payments made
     * causing a lot of data usage.
     */
    private void sincronizarPagosRegistrados() {
        try {
            for (ModelPayment p: offlinePayments){
                //sDatabaseAssistant.updatePaymentsAsSynced(p.getDate());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets all deposits that were just sent
     * to server as sync (sync <- 1) so that does not resend deposits made
     * causing data usage.
     */
    private void syncDepositsSavedToServer(JSONArray almacenados) {
        try {
            for (int i = 0; i < almacenados.length(); i++) {
                JSONObject deposito = almacenados.getJSONObject(i);
                DatabaseAssistant.updateDepositsAsSynced(deposito.getString("fecha"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method updates ListView of accounts (accounts within wallet)
     * getting new info from database
     */
    private void actualizarListView() {
        //runOnUiThread(new Runnable() {

        //@Override
        //public void run() {



        if (BuildConfig.isWalletSynchronizationEnabled()) {

            clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
            //clientsList = DatabaseAssistant.getAllClients();
            //clientListFromSyncedWalletWithoutAnalysis = DatabaseAssistant.getClientsFromSyncedWalletWithoutAnalysis();
        } else {
            clientsList = DatabaseAssistant.getAllClients();
        }



				/*
				if (BuildConfig.isWalletSynchronizationEnabled()) {
					//List<SyncedWallet> syncedClientsFromWallet = DatabaseAssistant.getClientsFromSyncedWallet();
					Util.Log.ih("size: " + clientListFromSyncedWallet.size());
					for (int i = 0; i < clientListFromSyncedWallet.size(); i++) {
						Util.Log.ih("wallet client: " + clientListFromSyncedWallet.get(i).getClient().getName());
					}
				}
				*/
        //for (int i = 0; i < clientListFromSyncedWalletWithoutAnalysis.size(); i++) {
        //	Util.Log.ih("wallet client W/A: " + clientListFromSyncedWalletWithoutAnalysis.get(i).getClient().getName());
        //}
        //clientListFromSyncedWalletForAdapter = clientListFromSyncedWallet;
        //clientListFromSyncedWalletForAdapter.add(clientListFromSyncedWalletWithoutAnalysis.get(0));

        //Util.Log.ih("clientListFromSyncedWallet = " + clientListFromSyncedWallet.size());
        //Util.Log.ih("clientListFromSyncedWalletForAdapter = " + clientListFromSyncedWalletForAdapter.size());

        //ClientesAdapter adapter;

        mainActivity.runOnUiThread(() -> {
            if (BuildConfig.isWalletSynchronizationEnabled()) {
                //TODO: update ClientesAdapter
                adapter = getClientesRecyclerAdapterSyncedWalletClients();
            } else {
                adapter = getClientesRecyclerAdapterClientsList();
            }
            if (getView() != null)
                /*lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);

            if (lvClientes != null) {
                lvClientes.setAdapter(adapter);
                //lvClientes.setDivider(getResources().getDrawable(
                        //R.drawable.divider_list_clientes));
                lvClientes.setOnItemClickListener(mthis);
            }*/
                rvClientes.setAdapter(adapter);

            dismissMyCustomDialog();

            if (BuildConfig.isWalletSynchronizationEnabled()) {
                if (clientListFromSyncedWallet.size() == 500) {
                    mainActivity.toastLReuse("El número total de clientes es bastante alta, se ha limitado la vista a 500, si un cliente asignado no se ve en la lista, puedes encontrarlo utilizando la barra de búsqueda.");
                } else {
                    //mainActivity.toastSReuse("Lista de clientes actualizada.");
                    Snackbar.make(mainActivity.mainLayout, "Lista de clientes actualizada", Snackbar.LENGTH_SHORT).show();
                }
            } else {
                if (clientsList.size() == 500) {
                    mainActivity.toastLReuse("El número total de clientes es bastante alta, se ha limitado la vista a 500, si un cliente asignado no se ve en la lista, puedes encontrarlo utilizando la barra de búsqueda.");
                } else {
                    //mainActivity.toastSReuse("Lista de clientes actualizada.");
                    Snackbar.make(mainActivity.mainLayout, "Lista de clientes actualizada", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
		/*
				if (BuildConfig.isWalletSynchronizationEnabled()) {
					//TODO: update ClientesAdapter
					adapter = getAdapterSyncedWalletClients();
				} else {
					adapter = getClientesAdapterClientsList();
				}
				lvClientes = (ListView) findViewById(R.id.listView_clientes);
				lvClientes.setAdapter(adapter);
				lvClientes.setDivider(getResources().getDrawable(
						R.drawable.divider_list_clientes));
				lvClientes.setOnItemClickListener(mthis);

				dismissMyCustomDialog();

				if (BuildConfig.isWalletSynchronizationEnabled()) {
					if (clientListFromSyncedWallet.size() == 500) {
						toastLReuse("El número total de clientes es bastante alta, se ha limitado la vista a 500, si un cliente asignado no se ve en la lista, puedes encontrarlo utilizando la barra de búsqueda.");
					} else {
						toastSReuse("Lista de clientes actualizada.");
					}
				} else {
					if (clientsList.size() == 500) {
						toastLReuse("El número total de clientes es bastante alta, se ha limitado la vista a 500, si un cliente asignado no se ve en la lista, puedes encontrarlo utilizando la barra de búsqueda.");
					} else {
						toastSReuse("Lista de clientes actualizada.");
					}
				}

		*/
        //}
        //});

    }

    /**
     * Deletes all data from database recolected by app and closes session.
     */
    private void borrarDatos() {
        DatabaseAssistant.eraseDatabaseToBlock();
        cerrarSesionTimeOut();
    }

    /**
     * sets collector's cash
     */
    private void calcularTotal() {
		/*
		//SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
		try {
			SharedPreferences preferencesLogin = getSharedPreferences(
					"PABS_Login", Context.MODE_PRIVATE);
			double efetivoInicial = Double.parseDouble(preferencesLogin
					.getString("efectivo_inicial", "0.0"));

			//String saldoPrograma = formatMoney(query.getTotalPrograma());
			//String saldoMalba = formatMoney(query.getTotalMalba());

			//double totalDepositos = query.getTotalDepositos();
			double totalDepositos = DatabaseAssistant.getTotalDeposits();
			//double totalCancMalba = query.getTotalCanceladosMalba();
			//double totalCancPrograma = query.getTotalCanceladosPrograma();
			//JSONArray empresas = query.mostrarEmpresas();
			List<Companies> companies = DatabaseAssistant.getCompanies();
			//String efectivoEmpresas = new String();
			//String cancelados = new String();
			double cantidadempresas = 0;
			double cantidadcancelados = 0;
			for (int cont = 0; cont < companies.size(); cont++) {
				//JSONObject empresa = empresas.getJSONObject(cont);
				Companies company = companies.get(cont);
				//cantidadempresas += query.getTotalEmpresa(empresa
				//		.getString(Empresa.id_empresa));
				cantidadempresas += DatabaseAssistant.getTotalByCompany(company.getIdCompany());
				//cantidadcancelados += query.getTotalCanceladosEmpresa(empresa
				//		.getString(Empresa.id_empresa));
				cantidadcancelados += DatabaseAssistant.getTotalCanceledByCompany(company.getIdCompany());

                /*
				efectivoEmpresas += "+"
						+ empresa.getString(Empresa.nombre_empresa)
						+ ": $"
						+ formatMoney(query.getTotalEmpresa(empresa
								.getString(Empresa.id_empresa))) + "\n";
				cancelados += "-Cancelados "
						+ empresa.getString(Empresa.nombre_empresa).charAt(0)
						+ " : $"
						+ query.getTotalCanceladosEmpresa(empresa
								.getString(Empresa.id_empresa)) + "\n";
				*/
			/*}
			double totalMomento = efetivoInicial + cantidadempresas
					- cantidadcancelados - totalDepositos;
			datosCobrador.put("efectivo", totalMomento);

			//query = null;

		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
    }


    /**
     * Closes session by cancelling all timers (to make sure nothing is running
     * in the background) and finishing current activity
     * going back to login screen.
     */
    private void cerrarSesion() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (timerNotificaciones != null) {
            timerNotificaciones.cancel();
            timerNotificaciones = null;
        }
        if (timerSendPaymentsEachTenMinutes != null){
            timerSendPaymentsEachTenMinutes.cancel();
            timerSendPaymentsEachTenMinutes = null;
        }

        modificarInfoToLoginAndSplash();

        View refLogin = null;
        if (Login.getMthis() != null) {
            refLogin = Login.getMthis().findViewById(R.id.editTextUser);
        }

        if (refLogin != null) {
            Login.getMthis().finish();
        }

        Intent intent = new Intent(getContext(), Login.class);
        if (datosCobrador != null) {
            intent.putExtra("datos_cobrador", datosCobrador.toString());
        }
        if (robado == 1) {
            intent.putExtra("robado", true);
        }
        startActivity(intent);

        Util.Log.ih("SESION TERMINADA CORRECTAMENTE");

        mthis = null;
        mainActivity.finish();
    }

    /**
     * Blocks app.
     * This mehtod gets called when cash is greater than permitted,
     * preventing the app from letting the collector collect more than his limit.
     *
     * finishes all activities loaded by reference 'mthis' on each activity if they weren't finished yet
     * and cancels all timers (to make sure nothing is running in the background) and finishing current activity
     * going back to login screen.
     */
    private void cerrarTodoYMandarBloqueo() {
        Intent intent = new Intent(getContext(), Bloqueo.class);
        calcularTotal();
        intent.putExtra("datos_cobrador", datosCobrador.toString());
        intent.putExtra("iniciar_time_sesion", true);
        intent.putExtra("isConnected", true);
        startActivity(intent);
        if (mainActivity.handlerSesion != null) {
            mainActivity.handlerSesion.removeCallbacks(mainActivity.myRunnable);
            mainActivity.handlerSesion = null;
        }

        View refBuscarClientes = null;
        if (BuscarCliente.getMthis() != null) {
            refBuscarClientes = BuscarCliente.getMthis().findViewById(
                    R.id.lv_buscar_clientes);
        }

        View refAportacion = null;
        if (Aportacion.getMthis() != null) {
            refAportacion = Aportacion.getMthis().getView().findViewById(
                    R.id.et_nombre_apellidos);
        }

        View refCierre = null;
        if (Cierre.getMthis() != null) {
            refCierre = Cierre.getMthis().getView().findViewById(R.id.lv_cierre);
        }

        View refDeposito = null;
        if (Deposito.getMthis() != null) {
            refDeposito = Deposito.getMthis().getView().findViewById(R.id.tv_banco);
        }

        View refDetalleCliente = null;
        if (ClienteDetalle.getMthis() != null) {
            refDetalleCliente = ClienteDetalle.getMthis().getView().findViewById(
                    R.id.et_cantidad_aportacion);
        }

        View refNotificaciones = null;
        if (Notificaciones.getMthis() != null) {
            refNotificaciones = Notificaciones.getMthis().findViewById(
                    R.id.lv_notification);
        }

        if (refBuscarClientes != null) {
            BuscarCliente.getMthis().finish();
        } else if (refAportacion != null) {
            Aportacion.getMthis().getActivity().finish();
        }

        if (refCierre != null) {
            Cierre.getMthis().getActivity().finish();
        }

        if (refDeposito != null) {
            Deposito.getMthis().getActivity().finish();
        }

        if (refDetalleCliente != null) {
            ClienteDetalle.getMthis().getActivity().finish();
        }

        if (refNotificaciones != null) {
            Notificaciones.getMthis().finish();
        }

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (timerNotificaciones != null) {
            timerNotificaciones.cancel();
            timerNotificaciones = null;
        }
        if (timerSendPaymentsEachTenMinutes != null){
            timerSendPaymentsEachTenMinutes.cancel();
            timerSendPaymentsEachTenMinutes = null;
        }
        mthis = null;
        mainActivity.finish();
    }

    /*
     * Este metodo enviar cobros offline cada 10 minutos
     */
    private void enviarCobrosDiezMinutos() {
        timerSendPaymentsEachTenMinutes = new Timer();
        timerSendPaymentsEachTenMinutes.schedule(new TimerTask() {

            @Override
            public void run() {
                mainActivity.runOnUiThread(() -> {
                    Util.Log.ih("trying to send payments...");
                    //if ((System.currentTimeMillis() - ExtendedActivity.userInteractionTime) > ((1000 * 60) * 5)) {
                    //Util.Log.ih("15 minutes of inactivity");

                    if (!Deposito.depositosIsActive) {
                        // if ((System.currentTimeMillis() -
                        // ExtendedActivity.userInteractionTime) > (1000 *
                        // 10
                        // * 1)) {
                        //SqlQueryAssistant query = new SqlQueryAssistant(
                        //		ApplicationResourcesProvider.getContext());
                        //try {
                        ExtendedActivity.userInteractionTime = System
                                .currentTimeMillis();
                        //JSONObject json = query
                        //		.obtenerCobrosDesincronizados();
                        offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();
                        //query = null;
                        //Log.e("has cobros", "" + json.has("cobros"));
                        //if (json.has("cobros")) {
                        //	arrayCobrosOffline = json
                        //			.getJSONArray("cobros");
										/*
										HashMap<String, String> hm = new HashMap<String, String>();
										hm.put("event",
												"inicio sincronizacion cobros offline");
										for (int i = 0; i < arrayCobrosOffline
												.length(); i++) {
											JSONObject cobro = arrayCobrosOffline
													.getJSONObject(i);
											hm = new HashMap<String, String>();
											hm.put("cobro", cobro.toString());

										}
										*/

                        if (offlinePayments != null && offlinePayments.size() > 0) {
                            //if (isConnected) {
											/*
											writeToFile((new Date())
													+ "se envian datos a internet desde clientes "
													+ arrayCobrosOffline);
											*/
											/*
											Util.Log.ih("sending payments");
											Toast.makeText(
													getApplicationContext(),
													"Iniciando sincronización.",
													Toast.LENGTH_SHORT).show();
											registerPagos();
											*/
                            //}
                            mainActivity.sendPaymentsIfActiveInternetConnection();
                        } else {
                            Util.Log.ih("no hay cobros pendientes...");
                            enviarDepositos();
                        }
                        //} catch (JSONException e) {
                        //	e.printStackTrace();
                        //}
                    }
                    //}
                    executeSendOsticket();
                });
            }
        }, (1000 * 60), (1000 * 60) * 10);//}, (1000 * 60) * 10 , (1000 * 60) * 10 );

    }

    /**
     * Sends to server all deposits made using Web Service
     *
     * 'http://50.62.56.182/ecobro/controldepositos/registerDepositosOffline'
     * JSONObject param
     * {
     *     "no_cobrador": "";
     *     "depositos": {},
     *     "periodo": ,
     * }
     */
    private void enviarDepositos() {
        //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
        //if (query.hayDepositos()) {
        if (DatabaseAssistant.isThereAnyDepositNotSynced()) {

            List<LogRegister.LogDeposito> logDepositos = null;
            JSONObject json = new JSONObject();

            try {
                //JSONArray depositos = query.mostrarTodosLosDepositosDesincronizados().getJSONArray("DEPOSITO");
                List<ModelDeposit> deposits = DatabaseAssistant.getAllDepositsNotSynced();
                if (deposits != null) {

                    json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
                    json.put("depositos", new JSONArray( new Gson().toJson( deposits ) ));
                    json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
                    //Crear arraylist de depositos y registrarlos en log si el server responde
                    logDepositos = new ArrayList<>();
					/*
					for (int i = 0; i < depositos.length(); i++) {
						LogRegister.LogDeposito logDeposito;
						JSONObject jDeposito = depositos.getJSONObject(i);

						logDeposito = new LogRegister.LogDeposito(
								"" + getEfectivoPreference().getInt("periodo", 0),                                                //Periodo
								jDeposito.getString("monto"),                                    //Monto
								jDeposito.getString("empresa"), //Empresa
								getEfectivoPreference().getString("no_cobrador", ""),                                                                                    //Cobrador
								jDeposito.getString("no_banco"),                                                                    //Banco
								jDeposito.getString("referencia"),                                    //Referencia
								true                                                                                            //Ya fue enviado al server
						);
						logDepositos.add(logDeposito);
					}
					*/
                    for (ModelDeposit d: deposits){
                        LogRegister.LogDeposito logDeposito;

                        logDeposito = new LogRegister.LogDeposito(
                                "" + mainActivity.getEfectivoPreference().getInt("periodo", 0),//Periodo
                                d.getAmount(),//Monto
                                d.getCompany(), //Empresa
                                mainActivity.getEfectivoPreference().getString("no_cobrador", ""),                                                                                    //Cobrador
                                d.getNumberBank(),//Banco
                                d.getReference(),//Referencia
                                true//Ya fue enviado al server
                        );
                        logDepositos.add(logDeposito);
                    }
                    Util.Log.ih("depositos json = " + json.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            /*RequestResult requestResult = new RequestResult(getContext());

            requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {

                List<LogRegister.LogDeposito> logDepositos;

                @Override
                public void onRequestResult(NetworkResponse response) {
                    try
                    {
                        manageRegisterDepositosOffline(new JSONObject(VolleyTickle.parseResponse(response)), logDepositos);
                    }
                    catch (JSONException ex)
                    {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onRequestError(NetworkResponse error) {
                    Log.e("EnviarDepositos", "Error al enviar depositos");
                }

                private RequestResult.ResultListener setData(List<LogRegister.LogDeposito> logDepositos)
                {
                    this.logDepositos = logDepositos;
                    return this;
                }
            }.setData(logDepositos));

            requestResult.executeRequest(ConstantsPabs.urlRegisterDepositosOffline, Request.Method.POST, json);*/

            NetService service = new NetService(
                    ConstantsPabs.urlRegisterDepositosOffline, json);
            NetHelper.getInstance().ServiceExecuter(service, getContext(),
                    new NetHelper.onWorkCompleteListener() {

                        List<LogRegister.LogDeposito> logDepositos;

                        @Override
                        public void onCompletion(String result) {
                            try {
                                manageRegisterDepositosOffline(new JSONObject(result), logDepositos);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e("EnviarDepositos", "Error al enviar depositos");

                        }

                        public NetHelper.onWorkCompleteListener setData(List<LogRegister.LogDeposito> logDepositos){
                            this.logDepositos = logDepositos;
                            return this;
                        }
                    }.setData(logDepositos));

        }
        //query = null;
    }

    /**
     * Sends to server all locations saved 'till now
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controlubicaciones/saveUbicacion'
     *
     * @param params JSONObject with locations and cash of every company
     */
    private void enviarUbicacion(JSONObject params) {

        /*RequestResult requestResult = new RequestResult(getContext());

        requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
            @Override
            public void onRequestResult(NetworkResponse response) {
                try
                {
                    manage_SaveUbicacion(new JSONObject(VolleyTickle.parseResponse(response)));
                }
                catch (JSONException ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onRequestError(NetworkResponse error) {
                Log.e("EnviarUbicacion", "Error al enviar ubicaciones");
            }
        });

        requestResult.executeRequest(ConstantsPabs.urlSaveGeoJSONLocations, Request.Method.POST, params);*/

        NetService service = new NetService(ConstantsPabs.urlSaveGeoJSONLocations,
                params);
        NetHelper.getInstance().ServiceExecuter(service, getContext(),
                new NetHelper.onWorkCompleteListener() {

                    @Override
                    public void onCompletion(String result) {
                        try {
                            manage_SaveUbicacion(new JSONObject(result));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("EnviarUbicacion", "Error al enviar ubicaciones");
                    }
                });
    }

    /**
     * Gets distance from current location to client location
     * @param myLocation current location
     * @param latitude client's  latitude
     * @param longitude client's longitude
     * @return distance to client from current location
     */
    private double getDistance(Location myLocation, double latitude,
                               double longitude) {
        Location locationTmp = new Location("other");
        locationTmp.setLatitude(latitude);
        locationTmp.setLongitude(longitude);

        return myLocation.distanceTo(locationTmp);
    }

    private boolean goBack = true;

    /**
     * shows framelayout as progress dialog
     */
    private void showMyCustomDialog(){

        goBack = false;

        if (getView() != null) {
            final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
            flLoading.setVisibility(View.VISIBLE);
        }

        //final Animation qwe = AnimationUtils.loadAnimation(this, R.animator.rotate_around_center_point);
        //final ImageView ivLoadingIcon = (ImageView) ClienteDetalle.this.findViewById(R.id.ivLoading);
        //ivLoadingIcon.startAnimation(qwe);
    }

    /**
     * hides framelayout used as progress dialog
     */
    private void dismissMyCustomDialog(){

        goBack = true;

        if (getView() != null) {
            final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
            flLoading.setVisibility(View.GONE);
        }

        //final ImageView ivLoadingIcon = (ImageView) ClienteDetalle.this.findViewById(R.id.ivLoading);
        //ivLoadingIcon.clearAnimation();
    }

    /**
     * Gets list of clients assigned to collector using a
     * Web Serrvice 'http://50.62.56.182/ecobro/controlcartera/getListClient'
     *
     * JSONObject param
     * {
     *     "no_cobrador": "";
     * }
     */
    private void getListClients() {
        SharedPreferences preferencesSplash = getContext().getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
        String clientsCollectorNumber = preferencesSplash.getString("clientsCollectorNumber", null);



        JSONObject json = new JSONObject();
        try {
            String idCobrador = "";
            try {
				/*
				if ( !getIntent().getBooleanExtra("isSupervisor", false) ) {
					Util.Log.ih("if ( !getIntent().getBooleanExtra(\"isSupervisor\", false) ) {");
					idCobrador = datosCobrador.getString("no_cobrador");
				} else {
					Util.Log.ih("} else {");
					idCobrador = getIntent().getStringExtra("clientsCollectorNumber");
				}
				*/
                if ( clientsCollectorNumber == null ) {
                    Util.Log.ih("if ( !getIntent().getBooleanExtra(\"isSupervisor\", false) ) {");
                    idCobrador = datosCobrador.getString("no_cobrador");
                } else {
                    Util.Log.ih("} else {");
                    idCobrador = clientsCollectorNumber;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            json.put("no_cobrador", idCobrador);
        } catch (Exception e) {
            e.printStackTrace();
        }

		/*RequestResult requestResult = new RequestResult(getContext());

		requestResult.clearCache();

        requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
            @Override
            public void onRequestResult(NetworkResponse response) {
                try
                {
                    manage_ListClients(new JSONObject(VolleyTickle.parseResponse(response)));
                }
                catch (JSONException ex)
                {
                    ex.printStackTrace();
                    onRequestError(response);

                    mainActivity.runOnUiThread(() -> dismissMyCustomDialog());
                }
            }

            @Override
            public void onRequestError(NetworkResponse error) {
                mainActivity.runOnUiThread(() -> dismissMyCustomDialog());

                if (BuildConfig.isWalletSynchronizationEnabled()){
                    if (clientListFromSyncedWallet == null) {
                        clientListFromSyncedWallet = new ArrayList<>();
                    }
                }
                else {
                    if (clientsList == null) {
                        clientsList = new ArrayList<>();
                    }
                }
                //ClientesAdapter adapter;
                if (BuildConfig.isWalletSynchronizationEnabled()){
                    adapter = getAdapterSyncedWalletClients();
                }
                else {
                    adapter = getClientesAdapterClientsList();
                }
                lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);
                if (lvClientes != null) {
                    lvClientes.setAdapter(adapter);
                    lvClientes.setDivider(getResources().getDrawable(
                            R.drawable.divider_list_clientes));
                    lvClientes.setOnItemClickListener(mthis);
                }

                Toast toast = Toast.makeText(getContext(), "Hubo un problema de conexión con internet. Por favor inténtalo de nuevo.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        });

        requestResult.executeRequest(ConstantsPabs.urlListClient, Request.Method.POST, json);*/


        NetService service = new NetService(ConstantsPabs.urlListClient, json);
        NetHelper.getInstance().ServiceExecuter(service, getContext(),
                new NetHelper.onWorkCompleteListener() {
                    private ProgressDialog dialog;

                    @Override
                    public void onCompletion(String result) {
                        try {
                            manage_ListClients(new JSONObject(result));
							/*
							if (!isFinishing()) {
								if (dialog != null && dialog.isShowing()) {
									dialog.dismiss();
								}
							}*/
                            //dismissMyCustomDialog();
                        } catch (JSONException e) {
                            onError(e);
                            e.printStackTrace();
							/*
							if (!isFinishing()) {
								if (dialog != null && dialog.isShowing()) {
									dialog.dismiss();
								}
							}*/
                            mainActivity.runOnUiThread(() -> dismissMyCustomDialog());
                        }
                    }

                    @Override
                    public void onError(Exception e) {
						/*
						if (dialog != null && dialog.isShowing()) {
							dialog.dismiss();
						}*/

                        mainActivity.runOnUiThread(() -> dismissMyCustomDialog());

                        if (BuildConfig.isWalletSynchronizationEnabled()){
                            if (clientListFromSyncedWallet == null) {
                                clientListFromSyncedWallet = new ArrayList<>();
                            }
                        }
                        else {
                            if (clientsList == null) {
                                clientsList = new ArrayList<>();
                            }
                        }
                        //ClientesAdapter adapter;
                        if (BuildConfig.isWalletSynchronizationEnabled()){
                            adapter = getClientesRecyclerAdapterSyncedWalletClients();
                        }
                        else {
                            adapter = getClientesRecyclerAdapterClientsList();
                        }
                        /*lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);
                        lvClientes.setAdapter(adapter);
                        lvClientes.setDivider(getResources().getDrawable(
                                R.drawable.divider_list_clientes));
                        lvClientes.setOnItemClickListener(mthis);*/
                        rvClientes.setAdapter(adapter);

                        Toast toast = Toast.makeText(getContext(), "Hubo un problema de conexión con internet. Por favor inténtalo de nuevo.", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }

                    public NetHelper.onWorkCompleteListener showDialog() {
                        //Drawable drawable = getResources().getDrawable(R.drawable.btn_actualizar);
						/*
						this.dialog = dialog;
						this.dialog.setCancelable(false);
						this.dialog.show();*/

                        showMyCustomDialog();
                        return this;
                    }
                });
    }

    /**
     * Gets number of new personal notifications
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controlnotificaciones/getNumNotification'
     * JSONObject param
     * {
     *     "no_cobrador": ""
     * }
     */
    private void getNumNotificaciones() {
        JSONObject json = new JSONObject();
        try {
            String idCobrador = "";
            try {
                idCobrador = datosCobrador.getString("no_cobrador");
            } catch (Exception e) {
                e.printStackTrace();
            }
            json.put("no_cobrador", idCobrador);
        } catch (Exception e) {
            e.printStackTrace();
        }

		/*RequestResult requestResult = new RequestResult(getContext());

        requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
            @Override
            public void onRequestResult(NetworkResponse response) {
                try
                {
                    manage_GetNumNotificaciones(new JSONObject(VolleyTickle.parseResponse(response)));
                }
                catch (JSONException ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onRequestError(NetworkResponse error) {
                Log.e("getNumNotificaciones", "Error al obtener numero de notificaciones");
            }
        });

        requestResult.executeRequest(ConstantsPabs.urlGetNumNotificaciones, Request.Method.POST, json);*/

        NetService service = new NetService(
                ConstantsPabs.urlGetNumNotificaciones, json);
        NetHelper.getInstance().ServiceExecuter(service, getContext(),
                new NetHelper.onWorkCompleteListener() {

                    @Override
                    public void onCompletion(String result) {
                        try {
                            manage_GetNumNotificaciones(new JSONObject(result));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("getNumNotificaciones", "Error al obtener numero de notificaciones");
                    }
                });
    }

    /**
     * Checks if date-time changes
     *
     * @return true: the difference is more than 21 minutes
     * @return false: otherwise
     */
    private boolean checkDate() {
        newTime = new Date().getTime();
        long differences = Math.abs(mTime - newTime);
        return (differences > deviceDateTimeDifference);
    }

    /**
     * Loads user name of collector.
     * This is focused to Splunk Mint, in order to report error by user name
     *
     * @return collector's user name
     */
    private String loadUser() {
        return getContext().getSharedPreferences("PABS_Login", Context.MODE_PRIVATE).getString("user_login", "no hay datos");
    }

    /**
     * Gets and saves deposit into database
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controldepositos/depositosPeriodo'
     *
     * JSONObject param
     * {
     *     "periodo": ,
     *     "no_cobrador":
     * }
     */
    private void saveDepositos() {
        JSONObject params = new JSONObject();

        try {
            params.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
            params.put("no_cobrador",
                    mainActivity.getEfectivoPreference().getString("no_cobrador", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }

		/*RequestResult requestResult = new RequestResult(getContext());

		requestResult.clearCache();

        requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
            @Override
            public void onRequestResult(NetworkResponse response) {
                try {

                    JSONObject result = new JSONObject(VolleyTickle.parseResponse(response));

                    if (result.has("error")) {
                        DatabaseAssistant.deleteAllDeposits();
                    } else {
                        JSONArray results = result.getJSONArray("result");
                        DatabaseAssistant.deleteAllDeposits();
                        for (int q = 0; q < results.length(); q++) {
                            JSONObject deposito = results.getJSONObject(q);
                            if (deposito.getString("status").equals("1"))
                                DatabaseAssistant.insertDeposit(
                                        deposito.getString("monto"),
                                        deposito.getString("fecha"),
                                        deposito.getString("referencia"),
                                        deposito.getString("no_banco"),
                                        deposito.getString("status"),
                                        deposito.getString("empresa"),
                                        mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                );
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestError(NetworkResponse error) {

            }
        });

        requestResult.executeRequest(ConstantsPabs.urlGetListaDepositosPorPeriodo, Request.Method.POST, params);*/

        NetService service = new NetService(
                ConstantsPabs.urlGetListaDepositosPorPeriodo, params);
        NetHelper helper = NetHelper.getInstance();
        helper.ServiceExecuter(service, getContext(), new NetHelper.onWorkCompleteListener() {

            @Override
            public void onError(Exception e) {
                // TODO Auto-generated method stub

            }

            // {"result":[{"no_deposito":"32","monto":"55","no_cobrador":"4438","fecha":"2013-06-26 11:14:42","no_banco":"5","status":"2","referencia":"xd","empresa":null},{"no_deposito":"31","monto":"9885","no_cobrador":"4438","fecha":"2013-06-26 10:56:45","no_banco":"4","status":"3","referencia":"fff","empresa":null}]}
            @Override
            public void onCompletion(String result) {
                try {
                    if (new JSONObject(result).has("error")) {
						/*
						SqlQueryAssistant query = new SqlQueryAssistant(
								ApplicationResourcesProvider.getContext());
						try {
							query.deleteDepositos();
						} catch (JSONException e1) {
							e1.printStackTrace();
						}*/

                        DatabaseAssistant.deleteAllDeposits();
                        //query = null;
                    } else {
                        JSONArray results = new JSONObject(result)
                                .getJSONArray("result");
                        //SqlQueryAssistant query = new SqlQueryAssistant(
                        //ApplicationResourcesProvider.getContext());
                        //query.deleteDepositos();
                        DatabaseAssistant.deleteAllDeposits();
                        for (int q = 0; q < results.length(); q++) {
                            JSONObject deposito = results.getJSONObject(q);
                            if (deposito.getString("status").equals("1"))
                                DatabaseAssistant.insertDeposit(
                                        deposito.getString("monto"),
                                        deposito.getString("fecha"),
                                        deposito.getString("referencia"),
                                        deposito.getString("no_banco"),
                                        deposito.getString("status"),
                                        deposito.getString("empresa"),
                                        mainActivity.getEfectivoPreference().getInt("periodo", 0)
                                );
							/*
								query.insertarDeposito(deposito.getString("monto"),
										deposito.getString("fecha"),
										deposito.getString("referencia"),
										deposito.getString("no_banco"),
										deposito.getString("status"),
										deposito.getString("empresa"));*/

                        }
                        //query = null;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Manages response of Web Service
     *
     * if there's new notifications, will change number of
     * notifications on the notifications counter and
     * also will show them on the notifications bar and drawer
     *
     * @param json JSONObject with reponse of Web Service
     */
    private void manage_GetNumNotificaciones(JSONObject json) {
        Log.e("json notificatiosn", json.toString());
        if (json.has("result")) {
            try {
                robado = json.getInt("robado");
                if (robado == 999) {
                    borrarDatos();
                } else {
                    try {
                        String numNotifications = json.getString("result");
                        if (getView() != null) {
                            ((TextView) getView().findViewById(R.id.tv_icon_notificaciones))
                                    .setText(numNotifications);
                            SharedPreferences.Editor editor = getContext().getSharedPreferences(
                                    "PABS_Main", Context.MODE_PRIVATE).edit();
                            editor.putString("numero_notificaciones", ""
                                    + numNotifications);
                            editor.apply();
                            int notificationsCounter = Integer.parseInt(numNotifications);

                            if (notificationsCounter > 0) {//if (notificationsCounter > 0 && notifications != notificationsCounter) {
                                notifications = notificationsCounter;
                                getNotificationAndShow(notificationsCounter);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * Shows notifications on notifications drawer
     * @param notificationMessage notifications message
     * @param notificationId notification id
     */
    private void showNotification(String notificationMessage, String notificationId) {
        long[] vibrate = {500, 500, 0, 0, 500, 500};
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String channelId = "pabsNotification";
        Intent notificationIntent = new Intent(mainActivity.getApplicationContext(), Notificaciones.class);
        try {
            notificationIntent.putExtra("id_cobrador", datosCobrador.getString("no_cobrador"));
        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(mainActivity.getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext(), channelId  )
                .setSmallIcon(getIcon());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setLargeIcon(getLargeIcon())
                    .setColor(getResources().getColor(R.color.base_color));
        }
        mBuilder.setWhen(System.currentTimeMillis())
                .setContentTitle(getString(R.string.app_name))
                .setContentText(notificationMessage)
                .setVibrate(vibrate)
                .setSound(alarmSound)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationMessage))
                .setContentIntent(intent)
                .setAutoCancel(true);

		/*
		SimpleDateFormat dateHora = new SimpleDateFormat("HH:mm");
		Date date = new Date();

		RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_notification);
		contentView.setImageViewResource(R.id.image, R.drawable.icono_hdpi);
		contentView.setTextViewText(R.id.title, "PABS");
		contentView.setTextViewText(R.id.tvDate, dateHora.format(date));
		contentView.setTextViewText(R.id.text, notificationMessage);
		mBuilder.setContent(contentView);
		mBuilder.setSmallIcon(R.drawable.icono_hdpi);
		mBuilder.setVibrate(vibrate);
		mBuilder.setSound(alarmSound);
		*/


        //mBuilder.setStyle(new Notification.BigTextStyle().bigText(notificationMessage).setBigContentTitle("PABS"));

        //Intent intent = new Intent(this, Clientes.class);
        //PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        //mBuilder.setContentIntent(pendingIntent);

        NotificationManager mNotificationManager =
                (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);

            if (mNotificationManager != null)
                mNotificationManager.createNotificationChannel(channel);
        }
        else
        {
            mBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
        }

        //Util.Log.ih("mId = " + mId);

        if (mNotificationManager != null)
            mNotificationManager.notify(Integer.parseInt(notificationId), mBuilder.build());

        //Util.Log.ih("mId = " + mId);

        Util.Log.ih("notificacion creada");
    }

    private void showSummaryNotifications(JSONArray notifications)
    {
        try
        {
            long[] vibrate = {500, 500, 0, 0, 500, 500};
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            String channelId = "pabsNotification";
            Intent notificationIntent = new Intent(mainActivity.getApplicationContext(), Notificaciones.class);

            notificationIntent.putExtra("id_cobrador", datosCobrador.getString("no_cobrador"));

            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent intent = PendingIntent.getActivity(mainActivity.getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext(), channelId  )
                    .setSmallIcon(getIcon());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBuilder.setLargeIcon(getLargeIcon())
                        .setColor(getResources().getColor(R.color.base_color));
            }

            mBuilder.setWhen(System.currentTimeMillis())
                    .setContentTitle(getString(R.string.app_name))
                    .setVibrate(vibrate)
                    .setSound(alarmSound)
                    .setContentIntent(intent)
                    .setAutoCancel(true);

            int numNotifications = 0;
            NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle("PABS");
            LocalDateTime now = LocalDateTime.now();
            JSONObject notification;

            for (int i = 0; i < notifications.length(); i++)
            {
                notification = notifications.getJSONObject(i);

                if (notification.getString("estatus").equals("0")) {
                    LocalDateTime date = LocalDateTime.parse(notification.getString("fecha_creacion"), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));

                    if (numNotifications + 1 < 7) {
                        if (notification.getString("no_cobrador").equals("-1")) {
                            Log.d("Diferencia de meses", "Resultado: " + (now.getMonthOfYear() - date.getMonthOfYear()));
                            if ((date.getMonthOfYear() == now.getMonthOfYear() || now.getMonthOfYear() - date.getMonthOfYear() < 2) && date.getYear() == now.getYear())
                                messagingStyle.addMessage(notification.getString("notificacion"), date.toDate().getTime(), "");
                            else
                                continue;
                        }
                        else
                            messagingStyle.addMessage(notification.getString("notificacion"), date.toDate().getTime(), "Personal");
                    }

                    numNotifications++;
                }
            }

            messagingStyle.setConversationTitle(numNotifications + " notificaciones no leídas");

            mBuilder.setStyle(messagingStyle);

            NotificationManager mNotificationManager =
                    (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);

                if (mNotificationManager != null)
                    mNotificationManager.createNotificationChannel(channel);
            }
            else
            {
                mBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
            }

            //Util.Log.ih("mId = " + mId);

            if (mNotificationManager != null)
                mNotificationManager.notify(999, mBuilder.build());
        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * retrieves an icon to use with {@link Notification.Builder#setSmallIcon(int)}
     * according to SDK
     * @return
     * 		drawable with respective icon
     */
    private int getIcon(){
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
            return R.drawable.white_notification_icon;
        }
        else{
            return R.drawable.icono_hdpi;
        }
    }

    /**
     * retrieves an icon to use with {@link Notification.Builder#setLargeIcon(Bitmap)}
     * according to SDK
     * @return
     * 		drawable with respective icon
     */
    private Bitmap getLargeIcon(){
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.icono_hdpi);
            return getCircleBitmap(largeIcon);
        }
        else{
            return null;
        }
    }

    /**
     * This method will create a circle shaped bitmap
     * @param bitMap
     * 			bitmap object
     * @return
     * 			bitmap object customized
     */
    private Bitmap getCircleBitmap(Bitmap bitMap){
        final Bitmap output = Bitmap.createBitmap(bitMap.getWidth(),
                bitMap.getHeight(), Bitmap.Config.ARGB_8888);

        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitMap.getWidth(), bitMap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitMap, rect, rect, paint);

        bitMap.recycle();

        return output;
    }

    /**
     * downloads notifications and get new ones
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controlnotificaciones/getListNotification'
     *
     * JSONObject param
     * {
     *     "no_cobrador": "",
     *     "pag": ""
     * }
     * @param notificationsCounter
     */
    private void getNotificationAndShow(int notificationsCounter){
        JSONObject json = new JSONObject();
        try {
            json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
            json.put("pag", "" + 1);

        } catch (Exception e) {
            e.printStackTrace();
        }

		/*RequestResult requestResult = new RequestResult(getContext());

        requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {

            int notificationsCounter = 1;

            @Override
            public void onRequestResult(NetworkResponse response) {
                try
                {
                    manage_GetNotifications(new JSONObject(VolleyTickle.parseResponse(response)), notificationsCounter);
                }
                catch (JSONException ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onRequestError(NetworkResponse error) {

            }

            private RequestResult.ResultListener setData(int notificationsCounter)
            {
                this.notificationsCounter = notificationsCounter;
                return this;
            }
        }.setData(notificationsCounter));

        requestResult.executeRequest(ConstantsPabs.urlNotificactiones, Request.Method.POST, json);*/

        NetService service = new NetService(ConstantsPabs.urlNotificactiones, json);
        NetHelper.getInstance().ServiceExecuter(service, getContext(), new NetHelper.onWorkCompleteListener() {

            int notificationsCounter = 1;

            @Override
            public void onError(Exception e) {
                Log.e("getNotificatio", "Error al obtener notificaciones");
            }

            @Override
            public void onCompletion(String result) {
                try {
                    manage_GetNotifications(new JSONObject(result), notificationsCounter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            public NetHelper.onWorkCompleteListener setData(int notificationsCounter) {
                this.notificationsCounter = notificationsCounter;
                return this;
            }
        }.setData(notificationsCounter));
    }

    /**
     * Manages response of Web Service
     *
     * gets message and id of new notifications and calls showNotification() method
     *
     * @param json web service's response
     * @param notificationsCounter number of new notifications
     */
    private void manage_GetNotifications(JSONObject json, int notificationsCounter) {
        if (json.has("result")) {
            try {
                JSONArray arrayNotifications = json.getJSONArray("result");
                if (arrayNotifications.length() > 0) {
                    /*for ( int i = 0; i < notificationsCounter; i++ ){
                        JSONObject jsonTmp = arrayNotifications.getJSONObject(i);
                        String notificationMessage = jsonTmp.getString("notificacion");
                        String notificationId = jsonTmp.getString("id");

                        showNotification(notificationMessage, notificationId);
                    }*/
                    if (arrayNotifications.length() > 1)
                        showSummaryNotifications(arrayNotifications);
                    else
                    {
                        JSONObject jsonTmp = arrayNotifications.getJSONObject(0);
                        String notificationMessage = jsonTmp.getString("notificacion");
                        String notificationId = jsonTmp.getString("id");

                        showNotification(notificationMessage, notificationId);
                    }
                }
            } catch (JSONException e){
                e.printStackTrace();
            }
        } else {
            mostrarMensajeError(ConstantsPabs.getNotificaciones);
        }
    }

    /**
     * Este metodo procesa el response y obtiene la respuesta del servidor
     *
     * @param json
     *            JSON que contiene el saldo del dia anterior
     */
    private void manage_ListClients(JSONObject json) {
        Log.e("descarga", "clientes");
        if (json.has("result")) {

            //it updates date from device when logging in without internet access
            // but after updating contracts,
            // which mean that  the device has now internet access
            //setSharedPreferencesForDateWhenNoInternetAccess();

            saveListClients(json);
        } else if (json.has("error")) {
            dismissMyCustomDialog();
            String error = null;
            try {
                error = json.getString("error");
                if (error.equals("No hay Clientes disponibles") || error.equals("No se obtuvieron depositos")) {
                    if (BuildConfig.isWalletSynchronizationEnabled()){
                        if (clientListFromSyncedWallet == null) {
                            clientListFromSyncedWallet = new ArrayList<>();
                        }
                    }
                    else {
                        if (clientsList == null) {
                            clientsList = new ArrayList<>();
                        }
                    }
                    try {
                        mainActivity.showAlertDialog("", error, true);
                    } catch (Exception e){
                        Toast toast = Toast.makeText(getContext(), "Error: " + error, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        e.printStackTrace();
                    }
                    //ClientesAdapter adapter;
                    if (BuildConfig.isWalletSynchronizationEnabled()){
                        adapter = getClientesRecyclerAdapterSyncedWalletClients();
                    }
                    else {
                        adapter = getClientesRecyclerAdapterClientsList();
                    }
                    /*lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);
                    if (lvClientes != null) {
                        lvClientes.setAdapter(adapter);
                        lvClientes.setDivider(getResources().getDrawable(
                                R.drawable.divider_list_clientes));
                        lvClientes.setOnItemClickListener(mthis);
                    }*/
                    rvClientes.setAdapter(adapter);
                } else {
                    mostrarMensajeError(ConstantsPabs.listClients);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            dismissMyCustomDialog();
            mostrarMensajeError(ConstantsPabs.listClients);
        }

    }

    /**
     * Manages response of web service
     * if failed to send payments, it will try to send them again,
     * if not, will set them as sync
     * @param json JSONObject with response of web service
     */
    private void manage_RegisterPagosOffiline(JSONObject json) {
        Log.e("result", json.toString());
		/*
		if(Clientes.getMthis() != null){
			Clientes.getMthis().writeToFile(
					(new Date()) + "se recibio de internet en clientes"
							+ json.toString());
		}
		*/
        ExtendedActivity.synched = true;
        if (json.has("result")) {
            //if (Cierre.getMthis() != null) {
            //	Cierre.getMthis().manage_RegisterPagosOffiline(json);
            //}
            try {

                JSONArray arrayPagosFallidos = json.getJSONArray("result");
                if (arrayPagosFallidos.length() > 0) {
                    mainActivity.runOnUiThread(this::registerPagos );

                } else {
                    sincronizarPagosRegistrados();
                    enviarDepositos();

                    Util.Log.ih("payments registered");
					/*
                    if (isAlive && !isFinishing()){
                        Builder builder = new Builder(this);
                        builder.setTitle("Datos registrados");
                        builder.setMessage("Los datos se han registrado correctamente");
                        builder.setNeutralButton("Aceptar",
                                new Dialog.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.create().show();
                    }
					*/

                    if (mandarABloqueo) {
                        mandarABloqueo = false;
                        cerrarTodoYMandarBloqueo();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Manages response of web service
     * @param json JSONObject with response of web service
     */
    private void manage_RegistrarNotificacionesLeidas(JSONObject json) {
        Log.e("result", json.toString());
        if (json.has("result")) {
            try {
                JSONArray arrayNotificacionesFallidos = json
                        .getJSONArray("result");
                if (arrayNotificacionesFallidos.length() > 0) {
                    TextView tvICon = (TextView) getView().findViewById(R.id.tv_icon_notificaciones);
                    int notificacionesActuales = Integer.parseInt(tvICon
                            .getText().toString());
                    notificacionesActuales = notificacionesActuales
                            + arrayNotificacionesFallidos.length();
                    tvICon.setText("" + notificacionesActuales);

                    SharedPreferences.Editor editor = getContext().getSharedPreferences(
                            "PABS_Main", Context.MODE_PRIVATE).edit();
                    editor.putString("numero_notificaciones", ""
                            + notificacionesActuales);
                    editor.apply();
                    mainActivity.runOnUiThread(this::registarNotificacionesLeidas);

                } else {
                    SharedPreferences.Editor editor = getContext().getSharedPreferences(
                            "PABS_Main", Context.MODE_PRIVATE).edit();
                    editor.putString("numero_notificaciones", "" + 0);
                    editor.apply();
                    //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
                    //query.deleteNotificacionesLeidas();
                    //query = null;
                    DatabaseAssistant.deleteViewedNotifications();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Manages response of web service
     * @param json JSONObject with response of web service
     */
    private void manage_SaveUbicacion(JSONObject json) {
        //TODO: BEFORE
		/*
		if (json.has("result")) {
			try {
				String numNotifications = json.getString("notifications");
				((TextView) findViewById(R.id.tv_icon_notificaciones))
						.setText(numNotifications);
				Editor editor = getSharedPreferences(
						"PABS_Main", Context.MODE_PRIVATE).edit();
				editor.putString("numero_notificaciones", "" + numNotifications);
				editor.apply();
				toastSReuse("Ubicación almacenada a servidor.");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		*/
        //TODO: AFTER
        if (json.has("result")) {
            //try {
            Util.Log.ih("deleting geoJSON file");
            //LogRegister.deleteGeoJsonFile(datosCobrador.getString("periodo"));
            LogRegister.deleteGeoJsonFile( "" + mainActivity.getEfectivoPreference().getInt("periodo", 0) );
            //} catch (JSONException e) {
            //	e.printStackTrace();
            //}
        }
    }

    /**
     * Manages response of web service
     * @param json JSONObject with response of web service
     */
    private void manage_SaveUbicacionesOffline(JSONObject json) {
        if (json.has("result")) {
            try {
                JSONArray arrayUbicacionesFallidas = json
                        .getJSONArray("result");
                if (arrayUbicacionesFallidas.length() > 0) {
                    mainActivity.runOnUiThread(this::saveUbicaciones);

                } else {
                    //mainActivity.toastSReuse("Ubicaciones enviadas correctamente.");
                    Snackbar.make(mainActivity.mainLayout, "Ubicaciones enviadas correctamente", Snackbar.LENGTH_SHORT).show();
                    //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
                    //query.deleteUbicaciones();
                    //query = null;
                    DatabaseAssistant.deleteLocations();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Manages response of web service
     * if failed to send deposits, it will try to send them again,
     * @param json JSONObject with response of web service
     * @param logDepositos List<LogRegister.LogDeposito> instance. deprecated in 3.0
     */
    private void manageRegisterDepositosOffline(JSONObject json, List<LogRegister.LogDeposito> logDepositos) {
        if (json.has("result")) {
            Log.e("result", json.toString());
            try {
                JSONObject result = json.getJSONObject("result");
                JSONArray almacenados = result.getJSONArray("almacenados");
                if (almacenados.length() > 0) {
                    syncDepositsSavedToServer(almacenados);
                }
                JSONArray repetidos = result.getJSONArray("repetidos");
                Util.Log.ih("repetidos = " + repetidos.toString());
                if (repetidos.length() > 0) {
                    syncDepositsSavedToServer(repetidos);
                }
                JSONArray noAlmacenados = result.getJSONArray("no_almacenados");
                if (noAlmacenados.length() > 0) {
                    //mainActivity.toastSReuse("Enviando depositos bancarios.");
                    Snackbar.make(mainActivity.mainLayout, "Enviando depositos bancarios", Snackbar.LENGTH_SHORT).show();
                    enviarDepositos();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * gets and sets info to log in
     */
    private void modificarInfoToLoginAndSplash() {
        SharedPreferences preferencesSplash = getContext().getSharedPreferences(
                "PABS_SPLASH", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorSplash = preferencesSplash.edit();
        try {
            editorSplash.putString("no_cobrador",
                    datosCobrador.getString("no_cobrador"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        editorSplash.putString("datos_cobrador", datosCobrador.toString());
        editorSplash.apply();
        SharedPreferences preferencesLogin = getContext().getSharedPreferences("PABS_Login",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editorLogin = preferencesLogin.edit();
        try {
            editorLogin.putString("no_cobrador",
                    datosCobrador.getString("no_cobrador"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        editorLogin.putString("datos_cobrador", datosCobrador.toString());
        editorLogin.apply();
    }

    /**
     * shows error message
     * @param caso constant when failed
     */
    private void mostrarMensajeError(final int caso) {
        try {
            mainActivity.hasActiveInternetConnection(ApplicationResourcesProvider.getContext(), mainActivity.mainLayout);
            AlertDialog.Builder alert = getBuilder();
            alert.setMessage("Hubo un error de comunicación con el servidor, verifique sus ajustes de red o intente más tarde.");
            alert.setPositiveButton("Aceptar",(dialogInterface, which) -> {});
			/*
			alert.setNegativeButton("Reintentar",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if (isConnected) {

							} else {
								mostrarMensajeError(caso);
							}
						}
					});
			*/
            alert.create();
            alert.setCancelable(false);
            alert.show();
        } catch (Exception e){
            //nothing...
            e.printStackTrace();
        }
    }

    /**
     * gets and shows cash at the moment when pressing cash amount on the screen
     *
     * company's amount -> total from company
     * company's canceled amount -> total canceled from company
     * company's deposited amount -> total deposited from company
     *
     * (company's amount) - (company's canceled amount) - (company's deposited amount) = cash at the moment
     */
    private void mostrarSaldoAlMomento() {
        double totalDepositos = DatabaseAssistant.getTotalDeposits();
        List<Companies> companies = DatabaseAssistant.getCompanies();
        String efectivoEmpresas = new String();
        String cancelados = new String();

        int size;
        switch (BuildConfig.getTargetBranch()){
            case GUADALAJARA:
                size = companies.size();
                break;
            case TOLUCA:
                size = 2;
                break;
            case QUERETARO:case IRAPUATO:case SALAMANCA:case CELAYA: case LEON:
                size = 2;
                break;
            case PUEBLA:case CUERNAVACA:
                size = 2;
                break;
            case MERIDA:
                size = 1;
                break;
            case SALTILLO:
                size = 1;
                break;
            case CANCUN:
                size = 1;
                break;
            case MEXICALI: case TORREON:
                size = 3;
                break;
            case MORELIA:
                size = 3;
                break;
            default:
                size = 4;
        }

        if ( DatabaseAssistant.isThereMoreThanOnePeriodActive() ) {

            List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();

            for (ModelPeriod mp: periods) {

                efectivoEmpresas = "";
                cancelados = "";

                Util.Log.ih("adding period..." + mp.getPeriodNumber());

                mainActivity.getEfectivoEditor().putInt("periodo", mp.getPeriodNumber()).apply();
                for (int cont = 0; cont < size; cont++) {

                    Companies company = companies.get(cont);

                    float depositos = (float) DatabaseAssistant.getTotalDepositsByCompany(
                            company.getIdCompany()
                    );
                    efectivoEmpresas += "+"
                            + company.getName()
                            + ": $"
                            + mainActivity.formatMoney(DatabaseAssistant.getTotalCompanyWhenMoreThanOnePeriodIsActive(company.getName(), mp.getPeriodNumber())
                            + depositos) + "\n";
                    cancelados += "-Cancelados "
                            + company.getName().charAt(0)
                            + " : $"
                            + DatabaseAssistant.getTotalCanceledByCompany(company.getIdCompany())
                            + "\n";
                }
                try {
                    mainActivity.showAlertDialog("Efectivo al momento - " + mp.getPeriodNumber(),
                            efectivoEmpresas
                                    + "-Depositos: $" + totalDepositos + "\n" + cancelados
                                    + "=Total: $" + (ExtendedActivity.getTotalEfectivoWhenMoreThanOnePeriodIsActive(mp.getPeriodNumber())), true);
                } catch (Exception e) {
                    Toast toast = Toast.makeText(getContext(), "Error: Vuelve a intentarlo", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    e.printStackTrace();
                }
            }
        } else {
            for (int cont = 0; cont < size; cont++) {

                Companies company = companies.get(cont);

                float depositos = (float) DatabaseAssistant.getTotalDepositsByCompany(
                        company.getIdCompany()
                );

                efectivoEmpresas += "+"
                        + company.getName()
                        + ": $"
                        + mainActivity.formatMoney(mainActivity.getEfectivoPreference().getFloat(
                        company.getName(), 0)
                        + depositos) + "\n";
                cancelados += "-Cancelados "
                        + company.getName().charAt(0)
                        + " : $"
                        + DatabaseAssistant.getTotalCanceledByCompany(company.getIdCompany())
                        + "\n";

            }

            try {
                mainActivity.showAlertDialog("Efectivo al momento - " + mainActivity.getEfectivoPreference().getInt("periodo", 0),
                        efectivoEmpresas
                                + "-Depositos: $" + totalDepositos + "\n" + cancelados
                                + "=Total: $" + (mainActivity.getTotalEfectivo()), true);
            } catch (Exception e) {
                Toast toast = Toast.makeText(getContext(), "Error: Vuelve a intentarlo", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                e.printStackTrace();
            }
        }
    }

    /**
     * This is for debugging purposes
     */
    private void randomListClick(){
        if (cont < 10){
            cont++;
            int min = 0;
            //int max = arrayClientes.length();
            int max = clientListFromSyncedWallet.size();
            int position = rand.nextInt((max - min) + 1) + min;
            JSONObject jsonTmp = null;
            Gson gson = new Gson();
            try {
                if (filteredClients != null) {
                    jsonTmp = new JSONObject( gson.toJson( filteredClients.get( position ) ) );
                } else if (arrayCercanos != null) {
                    jsonTmp = arrayCercanos.get(position);
                } else {
                    //jsonTmp = arrayClientes.getJSONObject(position);
                    jsonTmp = new JSONObject(gson.toJson( clientListFromSyncedWallet.get(position) ));
                }
                Log.d("Cliente", jsonTmp.toString(1));

                ClienteDetalle clienteDetalle = new ClienteDetalle();

                Bundle args = new Bundle();
                //Intent intent = new Intent(getContext(), ClienteDetalle.class);
                args.putString("datos", jsonTmp.toString());
                args.putString("datoscobrador", datosCobrador.toString());
                args.putBoolean("isConnected", mainActivity.isConnected);
                args.putString("callback", this.toString());
                args.putInt("requestCode", 1);
                //startActivityForResult(intent, 1);
                clienteDetalle.setArguments(args);
                clienteDetalle.setFragmentResult(this);
                getFragmentManager().beginTransaction()
                        .add(R.id.root_layout, clienteDetalle, MainActivity.TAG_CLIENTE_DETALLES)
                        .hide(this)
                        .addToBackStack("cliente_detalle")
                        .commitAllowingStateLoss();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * updates total cash and max cash on the screen
     * @throws JSONException
     */
    private void putInfo() throws JSONException {

        //mainActivity.hasActiveInternetConnection(getContext(), mainActivity.mainLayout);

        try {
            if (DatabaseAssistant.isThereMoreThanOnePeriodActive()) {
                ((TextView) getView().findViewById(R.id.tv_total_efec)).setText("$"
                        + (ExtendedActivity.getTotalEfectivoFromAllPeriods())
                        + "/$"
                        + mainActivity.formatMoney(Double.parseDouble(datosCobrador
                        .getString("max_efectivo"))));
            } else {
                if (mainActivity.isConnected) {
                    ((TextView) getView().findViewById(R.id.tv_total_efec)).setText("$"
                            + (mainActivity.getTotalEfectivo())
                            + "/$"
                            + mainActivity.formatMoney(Double.parseDouble(datosCobrador
                            .getString("max_efectivo"))));
                }
                else
                {
                    TextView txtEfectivo = getView().findViewById(R.id.tv_total_efec);

                    try {
                        txtEfectivo.setText("$" + mainActivity.getTotalEfectivo() +"/$" + mainActivity.formatMoney(Double.parseDouble(datosCobrador.getString("max_efectivo"))));
                    }
                    catch (JSONException ex)
                    {
                        ex.printStackTrace();
                        txtEfectivo.setText("$-/-");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * sends to server read notifications
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controlnotificaciones/updateNotification'
     * JSONObject param
     * {
     *     "notificaciones": {}
     * }
     */
    private void registarNotificacionesLeidas() {

        try {
            //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
            String viewedNotifications = DatabaseAssistant.getViewedNotifications();
            //JSONObject jsonTmp = query.mostrarNotificacionesLeidas();
            //query = null;
            JSONArray jsonArrayNotificaciones = null;
            if (viewedNotifications.length() > 0) {
                jsonArrayNotificaciones = new JSONArray( viewedNotifications );
            } else {
                jsonArrayNotificaciones = new JSONArray();
            }
            JSONObject json = new JSONObject();
            json.put("notificaciones", jsonArrayNotificaciones);

            /*RequestResult requestResult = new RequestResult(getContext());

            requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
                @Override
                public void onRequestResult(NetworkResponse response) {
                    try
                    {
                        manage_RegistrarNotificacionesLeidas(new JSONObject(VolleyTickle.parseResponse(response)));
                    }
                    catch (JSONException ex)
                    {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onRequestError(NetworkResponse error) {
                    Log.e("regNotificaciones", "Error al registrar notificaciones leidas");
                }
            });

            requestResult.executeRequest(ConstantsPabs.urlRegistrarNotificacionesLeidas, Request.Method.POST, json);*/

            NetService service = new NetService(
                    ConstantsPabs.urlRegistrarNotificacionesLeidas, json);
            NetHelper.getInstance().ServiceExecuter(service, getContext(),
                    new NetHelper.onWorkCompleteListener() {

                        @Override
                        public void onCompletion(String result) {
                            try {
                                manage_RegistrarNotificacionesLeidas(new JSONObject(
                                        result));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e("regNotificaciones", "Error al registrar notificaciones leidas");
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * saves list of clients into database
     * @param jsonobject
     */
    private void saveListClients(final JSONObject jsonobject) {

        //mainActivity.toastSReuse("Guardando clientes... Espere un momento.");
        Snackbar.make(mainActivity.mainLayout, "Guardando clientes... Espere un momento", Snackbar.LENGTH_SHORT).show();
        Runnable run = () -> {
            DatabaseAssistant.deleteClients();

            try {
                Log.d("ALMACENANDO CLIENTES", ":)");
                JSONArray arrayClientes = jsonobject.getJSONArray("result");


                for (int cont = 0; cont < arrayClientes.length(); cont++) {

                    JSONObject json = arrayClientes.getJSONObject(cont);
                    String localidad = json.getString("localidad");
                    String no_contrato = json.getString("no_contrato");
                    String no_ext_cobro = json.getString("no_ext_cobro");
                    String apellido_pat = json.getString("apellido_pat");
                    String monto_pago_actual = json
                            .getString("monto_pago_actual");
                    String colonia = json.getString("colonia");
                    String calle_cobro = json.getString("calle_cobro");
                    String nombre = json.getString("nombre");
                    String serie = json.getString("serie");
                    String forma_pago_actual = json
                            .getString("forma_pago_actual");
                    String apellido_mat = json.getString("apellido_mat");
                    String id_contrato = json.getString("id_contrato");
                    String no_cobrador = json.getString("no_cobrador");
                    String abono = json.getString("Abonado");
                    String saldo = json.getString("Saldo");
                    String entre_calles = json.getString("entre_calles");
                    String id_grupo_base = json.getString("id_grupo_base");
                    String tipo_bd = json.getString("tipo_bd");
                    String latitude = json.getString("latitud");
                    String longitude = json.getString("longitud");
                    String estatus = json.getString("estatus");
                    String fecha_ultimo_abono = json.getString("tiempo");
                    String comments = "";
                    String fecha_primer_abono = json.getString("fecha_primer_abono");
                    String phone = json.getString("phone");
                    String contractStatus = "";


                    if (json.has("monto_atrasado")) {
                        String saldo_atrasado = json
                                .getString("monto_atrasado");


                        DatabaseAssistant.insertClient(id_contrato, nombre, apellido_pat, apellido_mat,
                                localidad, no_contrato, no_cobrador, calle_cobro, no_ext_cobro,
                                entre_calles, colonia, abono, saldo, monto_pago_actual, serie,
                                forma_pago_actual, id_grupo_base, tipo_bd, latitude, longitude,
                                estatus, fecha_ultimo_abono, saldo_atrasado, comments, fecha_primer_abono, "",  phone, contractStatus, "fecha", "correo", "999999");
                    } else {

                        DatabaseAssistant.insertClient(id_contrato, nombre, apellido_pat, apellido_mat,
                                localidad, no_contrato, no_cobrador, calle_cobro, no_ext_cobro,
                                entre_calles, colonia, abono, saldo, monto_pago_actual, serie,
                                forma_pago_actual, id_grupo_base, tipo_bd, latitude, longitude,
                                estatus, fecha_ultimo_abono, comments, fecha_primer_abono, phone, contractStatus,"",  "fechaUltima", "correo", "999999");
                    }


                }

                boolean updatePaydays = false;
                //inserts paydays into each client
                if (BuildConfig.isWalletSynchronizationEnabled()) {
                    Util.Log.ih("SEMAFORO ACTIVADO");
                    if (jsonobject.has("result_paydays")) {

                        updatePaydays = true;

                        Util.Log.ih("jsonobject does have (\"result_paydays\")");
                        JSONArray arrayPaydays = jsonobject.getJSONArray("result_paydays");
                        for (int j = 0; j < arrayPaydays.length(); j++) {
                            JSONObject contract = arrayPaydays.getJSONObject(j);
                            DatabaseAssistant.insertPaydaysIntoClients(
                                    contract.getString("id_contrato"),
                                    contract.getString("forma_pago_actual"),
                                    contract.getString("semanal"),
                                    contract.getString("quincenal1"),
                                    contract.getString("quincenal2"),
                                    contract.getString("mensual")
                            );
                        }
                    } else {
                        Util.Log.ih("jsonobject does not have (\"result_paydays\")");
                    }
                } else {
                    Util.Log.ih("SEMAFORO DEASCTIVADO");
                }

                if (BuildConfig.isWalletSynchronizationEnabled()) {
                    Util.Log.ih("UPDATING SYNCED_WALLET TABLE...");

                    DatabaseAssistant.updateClientsInfoFromSyncedWallet(updatePaydays);
                    DatabaseAssistant.deleteContractsNotAssigned();
                    DatabaseAssistant.insertNewClientsIntoSyncedWallet();

                    Util.Log.ih("FINISH UPDATING SYNCED_WALLET TABLE...");
                } else {
                    Util.Log.ih("SEMAFORO DEASCTIVADO");
                }

                firstLoad = true;
					/*
					if (BuildConfig.isWalletSynchronizationEnabled()) {
						DatabaseAssistant.getClientsFromSyncedWalletThread();
					} else {
						DatabaseAssistant.getAllClients();
					}
					*/


                if (jsonobject.has("result_topics")) {

                    Util.Log.ih("jsonobject does have result_topics");

                    DatabaseAssistant.deleteAllOsticketTopics();

                    JSONArray arrayOsticketTopics = jsonobject.getJSONArray("result_topics");
                    for (int j = 0; j < arrayOsticketTopics.length(); j++) {
                        JSONObject topic = arrayOsticketTopics.getJSONObject(j);

                        DatabaseAssistant.insertOsticketTopics(
                                topic.getString("osticket_topic")
                        );
                    }
                } else {
                    Util.Log.ih("jsonobject does not have (\"result_paydays\")");
                }


                actualizarListView();

            } catch (Exception e){
                e.printStackTrace();

                Log.d("Clientes Error", e.getMessage());

                mainActivity.runOnUiThread(() -> {
                    try {
                        dismissMyCustomDialog();
                        mainActivity.showAlertDialog("Error", "No se pudieron obtener los clientes. Por favor presiona el botón de actualizar (flecha circular en la parte superior)", false);
                    } catch (Exception ex){
                        Toast toast = Toast.makeText(getContext(), "Error: No se pudieron obtener los clientes. Por favor presiona el botón de actualizar (flecha circular en la parte superior)", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                });
            }
        };
        Thread thread = new Thread(run);
        thread.start();

    }

    /**
     * sends to server all locations saved plus cash of every company
     *
     * Web Service
     * 'http://50.62.56.182/ecobro/controlubicaciones/saveUbicacionOffline'
     * JSONObject param
     * {
     *     "no_cobrador": "",
     *     "ubicaciones": {},
     *     "efectivo": ,
     *     "efectivoMalba": ,
     *     "efectivo_empresa4": ,
     *     "efectivo_empresa5": ,
     *     "cancelados_empresa4": ,
     *     "cancelados_empresa5": ,
     *     "efectivo_depositos": ,
     *     "efectivo_inicial": ,
     *     "cancelados_malba": ,
     *     "cancelados_programa":
     * }
     *
     * NOTE*
     * 		ADDS COMPANIES
     */
    private void saveUbicaciones() {
        JSONObject json = new JSONObject();

        try {
            //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());

            SharedPreferences preferencesLogin = getContext().getSharedPreferences(
                    "PABS_Login", Context.MODE_PRIVATE);

            json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
            json.put("ubicaciones", new JSONArray( new Gson().toJson( offlineLocations ) ));
            json.put("efectivo", "" + DatabaseAssistant.getTotalByCompany("01"));
            json.put("efectivoMalba", "" + DatabaseAssistant.getTotalByCompany("03"));
            json.put("efectivo_empresa4", DatabaseAssistant.getTotalByCompany("04"));
            json.put("efectivo_empresa5", DatabaseAssistant.getTotalByCompany("05"));
            json.put("cancelados_empresa4", DatabaseAssistant.getTotalCanceledByCompany("04"));
            json.put("cancelados_empresa5", DatabaseAssistant.getTotalCanceledByCompany("05"));
            json.put("efectivo_depositos", DatabaseAssistant.getTotalDeposits());
            json.put("efectivo_inicial", preferencesLogin.getString("efectivo_inicial", "0.0"));
            json.put("cancelados_malba", DatabaseAssistant.getTotalCanceledByCompany("03"));
            json.put("cancelados_programa", DatabaseAssistant.getTotalCanceledByCompany("01"));

            //query = null;

        } catch (Exception e) {
            e.printStackTrace();
        }

		/*RequestResult requestResult = new RequestResult(getContext());

        requestResult.setOnRequestResultListener(new RequestResult.ResultListener() {
            @Override
            public void onRequestResult(NetworkResponse response) {
                try
                {
                    manage_SaveUbicacionesOffline(new JSONObject(VolleyTickle.parseResponse(response)));
                }
                catch (JSONException ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onRequestError(NetworkResponse error) {
                Log.e("saveUbicacion", "Error al guardar ubicacion");
            }
        });

        requestResult.executeRequest(ConstantsPabs.urlSaveUbicacionesOffline, Request.Method.POST, json);*/

        NetService service = new NetService(
                ConstantsPabs.urlSaveUbicacionesOffline, json);
        NetHelper.getInstance().ServiceExecuter(service, getContext(),
                new NetHelper.onWorkCompleteListener() {

                    @Override
                    public void onCompletion(String result) {
                        try {
                            manage_SaveUbicacionesOffline(new JSONObject(result));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("saveUbicacion", "Error al guardar ubicacion");
                    }
                });
    }

    /**
     * instantiates timer to send locations to server
     * starting 1 second after instantiated and
     * with a delay of 10 minutes to run again
     */
    private void sendLocation() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                mainActivity.runOnUiThread(() -> {
                    Util.Log.ih("trying to send locations...");
                    //if ((System.currentTimeMillis() - ExtendedActivity.userInteractionTime) > ((1000 * 60) * 15)) {
                    Util.Log.ih("15 minutes of inactivity");
                    if (!Deposito.depositosIsActive) {
                        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                        boolean isGPSEnabled = locationManager
                                .isProviderEnabled(LocationManager.GPS_PROVIDER);

                        if (isGPSEnabled) {
                            //if (ApplicationResourcesProvider.getLocationProvider().getGPSPosition() != null) {

                            //TODO: NEW PROCESS
                            try {
                                //String periodo = datosCobrador.getString("periodo");
                                String periodo = "" + mainActivity.getEfectivoPreference().getInt("periodo", 0);
                                String collectorNumber = datosCobrador.getString("no_cobrador");

										/*
										first save geo locations into file
										so that if there are pending locations to send
										they'll be appended to existing ones.
										 */
                                ApplicationResourcesProvider.saveGeoJSONFile(periodo, collectorNumber);

                                JSONObject jsonEnviar = new JSONObject();

										/*
										read geo locations either new or pending ones.
										 */
                                String geoLocations = LogRegister.readGeoJSONDataFile(periodo);
                                if (geoLocations == null)
                                    throw new JSONException("geoLocations IS NULL");

                                jsonEnviar.put("no_cobrador", collectorNumber)
                                        .put("geoLocations", geoLocations)
                                        .put("periodo", periodo);

                                if (mainActivity.isConnected) {
                                    enviarUbicacion(jsonEnviar);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

									/*
									//Log.e("ENVIANDO ", "1");
									JSONObject jsonEnviar = new JSONObject();
									String idCobrador = "";
									try {
										idCobrador = datosCobrador
												.getString("no_cobrador");
									} catch (Exception e) {
										e.printStackTrace();
									}

									SharedPreferences preferencesLogin = getSharedPreferences(
											"PABS_Login", Context.MODE_PRIVATE);
									//SqlQueryAssistant query = new SqlQueryAssistant(
									//		ApplicationResourcesProvider.getContext());
									try {

										/*
										 * NOTE*
										 * 		ADDS COMPANIES
										 *//*
										jsonEnviar.put("id_cobrador", idCobrador);
										jsonEnviar.put("latitud", ApplicationResourcesProvider.getLocationProvider()
												.getGPSPosition().getLatitude());
										jsonEnviar.put("longitud", ApplicationResourcesProvider.getLocationProvider()
												.getGPSPosition().getLongitude());
										jsonEnviar.put("efectivo",
												DatabaseAssistant.getTotalByCompany("01"));
										jsonEnviar.put("efectivoMalba",
												DatabaseAssistant.getTotalByCompany("03"));
										jsonEnviar.put("efectivo_empresa4",
												DatabaseAssistant.getTotalByCompany("04"));
										jsonEnviar.put("efectivo_empresa5",
												DatabaseAssistant.getTotalByCompany("05"));
										jsonEnviar.put("cancelados_empresa4",
												DatabaseAssistant.getTotalCanceledByCompany("04"));
										jsonEnviar.put("cancelados_empresa5",
												DatabaseAssistant.getTotalCanceledByCompany("05"));
										jsonEnviar.put("efectivo_depositos",
												DatabaseAssistant.getTotalDeposits());
										jsonEnviar.put("efectivo_inicial",
												preferencesLogin.getString(
														"efectivo_inicial", "0.0"));
										jsonEnviar.put("cancelados_malba",
												DatabaseAssistant.getTotalCanceledByCompany("03"));
										jsonEnviar.put("cancelados_programa",
												DatabaseAssistant.getTotalCanceledByCompany("01"));
									} catch (JSONException e) {
										e.printStackTrace();
									} catch (NullPointerException e) {
										e.printStackTrace();
									} catch (Exception e) {
										e.printStackTrace();
									}
									if (isConnected) {
										enviarUbicacion(jsonEnviar);
										// enviando los ubicaciones offline
										try {
											//List<Locations> locations = DatabaseAssistant.getLocations();
											//JSONObject jsonUbicaciones = query.mostrarTodasLasUbicaciones();
											//if ( locations.size() > 0 ) {
											offlineLocations = DatabaseAssistant.getLocations();
											//arrayUbicacionesOffline = jsonUbicaciones
											//		.getJSONArray("UBICACION");
											if (offlineLocations.size() > 0) {
												if (isConnected) {
													toastSReuse("Enviando ubicaciones.");
													saveUbicaciones();
												}
											}
											//}
											//} catch (JSONException e) {
											//	e.printStackTrace();
										} catch (Exception e) {
											e.printStackTrace();
										}
									} else {
										SimpleDateFormat dateFormat = new SimpleDateFormat(
												"yyyy-MM-dd HH:mm:ss");
										Date date = new Date();

										DatabaseAssistant.insertLocation(
												dateFormat.format(date) + ":00",
												"" + ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLatitude(),
												"" + ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLongitude()
										);
											/*
											query.insertarUbicacion(
													dateFormat.format(date) + ":00", ""
															+ ApplicationResourcesProvider.getLocationProvider().getGPSPosition()
															.getLatitude(), ""
															+ ApplicationResourcesProvider.getLocationProvider().getGPSPosition()
															.getLongitude());
											*/
                            //mainActivity.toastSReuse("Ubicación registrada.");
                            Snackbar.make(mainActivity.mainLayout, "Ubicación registrada", Snackbar.LENGTH_SHORT).show();

                            //}
                            //query = null;
                        }// else {
                        //Log.e("ENVIANDO ", "2");
								/*
									JSONObject jsonEnviar = new JSONObject();
									String idCobrador = "";
									try {
										idCobrador = datosCobrador
												.getString("no_cobrador");
									} catch (Exception e) {
										e.printStackTrace();
									}

									//SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
									SharedPreferences preferencesLogin = getSharedPreferences(
											"PABS_Login", Context.MODE_PRIVATE);
									try {
										jsonEnviar.put("id_cobrador", idCobrador);
										jsonEnviar.put("efectivo",
												DatabaseAssistant.getTotalByCompany("01"));
										jsonEnviar.put("efectivoMalba",
												DatabaseAssistant.getTotalByCompany("03"));
										jsonEnviar.put("efectivo_depositos",
												DatabaseAssistant.getTotalDeposits());
										jsonEnviar.put("efectivo_inicial",
												preferencesLogin.getString(
														"efectivo_inicial", "0.0"));
										jsonEnviar.put("cancelados_malba",
												DatabaseAssistant.getTotalCanceledByCompany("03"));
										jsonEnviar.put("cancelados_programa",
												DatabaseAssistant.getTotalCanceledByCompany("01"));
									} catch (Exception e) {
										e.printStackTrace();
									}
									if (isConnected) {
										enviarUbicacion(jsonEnviar);
									}
									//query = null;
								}
								*/

							/*} else {
								Toast.makeText(getApplicationContext(), "Habilita tu GPS",
										Toast.LENGTH_SHORT).show();
								//Log.e("ENVIANDO ", "3");
								JSONObject jsonEnviar = new JSONObject();
								String idCobrador = "";
								try {
									idCobrador = datosCobrador
											.getString("no_cobrador");
								} catch (Exception e) {
									e.printStackTrace();
								}

								//SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
								SharedPreferences preferencesLogin = getSharedPreferences(
										"PABS_Login", Context.MODE_PRIVATE);
								try {
									jsonEnviar.put("id_cobrador", idCobrador);
									jsonEnviar.put("efectivo",
											DatabaseAssistant.getTotalByCompany("01"));
									jsonEnviar.put("efectivoMalba",
											DatabaseAssistant.getTotalByCompany("03"));
									jsonEnviar.put("efectivo_depositos",
											DatabaseAssistant.getTotalDeposits());
									jsonEnviar.put("efectivo_inicial",
											preferencesLogin.getString(
													"efectivo_inicial", "0.0"));
									jsonEnviar.put("cancelados_malba",
											DatabaseAssistant.getTotalCanceledByCompany("03"));
									jsonEnviar.put("cancelados_programa",
											DatabaseAssistant.getTotalCanceledByCompany("01"));
								} catch (JSONException e) {
									e.printStackTrace();
								} catch (Exception e) {
									e.printStackTrace();
								}
								if (isConnected) {
									enviarUbicacion(jsonEnviar);
								}
								//query = null;
							}*/
                        //}
                    }
                });

            }
        }, 0, (1000 * 60) * 10);//1000, (1000 * 60) * 10
    }

    /**
     * sets listener of resources on screen as
     * notifications
     * nearby clients
     * deposits
     * payments 'fuera de cartera'
     * 'cierre informativo'
     * refresh
     * end session
     * cash at the moment
     */
    private void setListeners() {
        final RelativeLayout notifications = (RelativeLayout) getView().findViewById(R.id.rlNotifications);

        final ImageView deposito = (ImageView) getView().findViewById(R.id.ImageViewDeposito);

        final ImageView aportefuera = (ImageView) getView().findViewById(R.id.imageViewAporteFuera);

        final ImageView refresh = (ImageView) getView().findViewById(R.id.imageViewRefresh);

        final ImageView cierre = (ImageView) getView().findViewById(R.id.imageViewCierre);

        final ImageButton cercanos = (ImageButton) getView().findViewById(R.id.btn_buscar_cercanos);

        final ImageView salir = (ImageView) getView().findViewById(R.id.imageViewSalir);

        final ImageView osticket = (ImageView) getView().findViewById(R.id.btn_ticket_support_iv);



        FrameLayout imageSaldoMomento = (FrameLayout) getView().findViewById(R.id.btn_show_saldos);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //notifications image view
            notifications.setOnClickListener(view -> setImageViewNotificationsTouchBehaviour());

            //deposits image view
            deposito.setOnClickListener(view -> setImageViewDepositsTouchBehaviour());

            //Fuera de cartera image view
            aportefuera.setOnClickListener(view -> setImageViewFueraDeCarteraTouchBehaviour());


            //refresh
            //updates clients information, adding, deleting or updating info
            //only if it is connected to internet
			/*
			refresh.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					setImageViewRefreshTouchBehaviour();
				}
			});
			*/

            //cierre informativo image view
            cierre.setOnClickListener(view -> setImageViewCierreInformativoTouchBehaviour());

            cierre.setOnLongClickListener(this);/*view -> {
                if (showPrevious) {
                    showListOfPreviousPeriods();
                    return true;
                }

                showPrevious = true;
                Snackbar.make(mainActivity.mainLayout, "Cierre Informativo", Snackbar.LENGTH_SHORT).show();

                previousPeriodsHandler.postDelayed(previousRunnable, 2500);

                return true;
            });*/
            imageSaldoMomento.setOnLongClickListener(this);
        }
        else{
            //notifications image view
            notifications.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    notifications.setAlpha(0.5f);
                    return true;
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    notifications.setAlpha(1.0f);
                    setImageViewNotificationsTouchBehaviour();
                    return true;
                }
                return false;
            });

            //deposits image view
            deposito.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    deposito.setAlpha(0.5f);
                    return true;
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    deposito.setAlpha(1.0f);
                    setImageViewDepositsTouchBehaviour();
                    return true;
                }
                return false;
            });

            //Fuera de cartera image view
            aportefuera.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    aportefuera.setAlpha(0.5f);
                    return true;
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    aportefuera.setAlpha(1.0f);
                    setImageViewFueraDeCarteraTouchBehaviour();
                    return true;
                }
                return false;
            });

            //refresh
            //updates clients information, adding, deleting or updating info
            //only if it is connected to internet
			/*
			refresh.setOnTouchListener(new OnTouchListener() {

				@SuppressWarnings("deprecation")
				@Override
				public boolean onTouch(View v, MotionEvent arg1) {
					if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
						refresh.setAlpha(125);
						return true;
					}
					if (arg1.getAction() == MotionEvent.ACTION_UP) {
						refresh.setAlpha(255);
						setImageViewRefreshTouchBehaviour();
						return true;
					}
					return false;
				}

			});
			*/

            //cierre informativo image view
            cierre.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    cierre.setAlpha(0.5f);
                    return true;
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    cierre.setAlpha(1.0f);
                    setImageViewCierreInformativoTouchBehaviour();
                    return true;
                }
                return false;
            });
        }

        cercanos.setOnClickListener(view -> {
            try {
                //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
                double radio = Double.parseDouble( DatabaseAssistant.getRadioToShowNearbyClients() );
                //query = null;
                Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();
                Log.d("GPS ACTIVADO?", "" + myLocation);
                if (myLocation != null) {
                    if (BuildConfig.isWalletSynchronizationEnabled()) {
                        if (clientListFromSyncedWallet != null && clientListFromSyncedWallet.size() > 0) {
                            //means that search field has given text
                            if (filteredClients != null) {
                                filteredClients = null;
                                getView().findViewById(R.id.ib_cancelar_filtro)
                                        .setVisibility(View.GONE);
                                ((EditText) getView().findViewById(R.id.et_filtrado))
                                        .setText("");

                                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(
                                        ((EditText) getView().findViewById(R.id.et_filtrado))
                                                .getWindowToken(), 0);

                                Log.d("filteredClients", " era != NULL");
                            } else {
                                Log.d("filteredClients", "es NULL");
                            }
                            //if (arrayCercanos == null) {
                            //	arrayCercanos = new ArrayList<JSONObject>();
                            //}
                            if (nearbyClientsFromSyncedWallet == null) {
                                nearbyClientsFromSyncedWallet = new ArrayList<>();
                            }
                            Drawable drawable = null;
                            //ClientesAdapter adapter = null;

                            StringBuilder contracts = new StringBuilder();

                            //gets all clientes that within radio
                            if (nearbyClientsFromSyncedWallet.size() == 0) {

                                //listViewState = lvClientes.onSaveInstanceState();

                                for (int i = 0; i < clientListFromSyncedWallet.size(); i++) {
                                    try {
                                        ModelClient nearbyClientTemp = clientListFromSyncedWallet.get(i).getClient();

                                        double latitude = Double.parseDouble(nearbyClientTemp.getLatitude());
                                        double longitude = Double.parseDouble(nearbyClientTemp.getLongitude());
                                        double distance = getDistance(myLocation, latitude, longitude);

                                        if (distance <= radio) {
                                            nearbyClientTemp.setDistance(Double.toString(distance));
                                            nearbyClientsFromSyncedWallet.add(nearbyClientTemp);
                                        }
                                    } catch (Exception e) {
                                        //e.printStackTrace();
                                    }
                                }
                                //shows clients on screen
                                if (nearbyClientsFromSyncedWallet.size() > 0) {
                                    /*Toast.makeText(getContext(),
                                            "Búsqueda finalizada",
                                            Toast.LENGTH_SHORT).show();*/
                                    Snackbar.make(mainActivity.mainLayout, "Búsqueda finalizada", Snackbar.LENGTH_SHORT).show();
                                    //Collections.sort(arrayCercanos,new CustomComparator());
                                    Collections.sort(nearbyClientsFromSyncedWallet, new ClientesRecycler.CustomComparatorToSortByDistanceFromSyncedWallet());

                                    List<SyncedWallet> nearbySyncedWallet = new ArrayList<>();

                                    for (ModelClient client: nearbyClientsFromSyncedWallet)
                                    {
                                        SyncedWallet syncedClient = new SyncedWallet(client.getIdContract(), client.getPayDay(), client.getPaymentOption(), client.getFirstPayday(), client.getSecondPayday(), client, client.getStatus());

                                        syncedClient.setClient(client);

                                        nearbySyncedWallet.add(syncedClient);
                                    }

                                    //adapter = new ClientesRecyclerAdapter<>(getContext(), nearbyClientsFromSyncedWallet, myLocation, ClientesRecyclerAdapter.NEARBY_CLIENTS, this);
                                    adapter = new ClientesRecyclerAdapter<>(getContext(), nearbySyncedWallet, myLocation, ClientesRecyclerAdapter.NEARBY_CLIENTS, this);
                                    drawable = getResources().getDrawable(
                                            R.drawable.btn_clientescer_pressed);
                                } else {//if there's no cliente near by current location, shows wallet
                                    Util.Log.ih("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                                    /*Toast.makeText(
                                            getContext(),
                                            "Sin clientes a " + radio
                                                    + " m de radio",
                                            Toast.LENGTH_SHORT).show();*/
                                    Snackbar.make(mainActivity.mainLayout, "Sin clientes a " + radio + "m de radio", Snackbar.LENGTH_SHORT).show();
                                    drawable = getResources().getDrawable(
                                            R.drawable.btn_clientescerc);
                                    //Util.Log.ih("filteredClients.size = " + clientsList.size());
                                    //adapter = getClientesRecyclerAdapterSyncedWalletClients();
                                    contracts = new StringBuilder();
                                    //arrayCercanos = null;
                                    nearbyClientsFromSyncedWallet = null;
                                }

                            } else {//when clicking again to show main list
                                Util.Log.ih("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
                                drawable = getResources().getDrawable(
                                        R.drawable.btn_clientescerc);

                                Snackbar loading = Snackbar.make(mainActivity.mainLayout,"Actualizando lista  de clientes", Snackbar.LENGTH_INDEFINITE);
                                ViewGroup contentLayout = (ViewGroup) loading.getView().findViewById(android.support.design.R.id.snackbar_text).getParent();
                                ProgressBar progressBar = new ProgressBar(mainActivity);
                                contentLayout.addView(progressBar, 0);
                                loading.show();

                                new Thread(this::actualizarListView).start();
                                adapter = getClientesRecyclerAdapterSyncedWalletClients();
                                //arrayCercanos = null;
                                nearbyClientsFromSyncedWallet = null;
                            }
                            cercanos.setImageDrawable(drawable);
                            /*lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);
                            if (lvClientes != null) {
                                lvClientes.setAdapter(adapter);
                                if (listViewState != null) {
                                    Util.Log.ih("!= null");
                                    lvClientes.onRestoreInstanceState(listViewState);
                                }
                                lvClientes.setDivider(getResources().getDrawable(
                                        R.drawable.divider_list_clientes));
                                lvClientes.setOnItemClickListener(mthis);
                            }*/
                            rvClientes.setAdapter(adapter);
                        } else {
                            /*Toast.makeText(getContext(),
                                    "No cuentas con clientes a organizar",
                                    Toast.LENGTH_SHORT).show();*/
                            Snackbar.make(mainActivity.mainLayout, "No cuentas con clientes a organizar", Snackbar.LENGTH_SHORT).show();
                        }
                    } else{
                        if (clientsList != null && clientsList.size() > 0) {
                            //means that search field has given text
								/*
								if (arrayFiltrados != null) {
									arrayFiltrados = null;
									findViewById(R.id.ib_cancelar_filtro)
											.setVisibility(View.GONE);
									((EditText) findViewById(R.id.et_filtrado))
											.setText("");

									InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
									imm.hideSoftInputFromWindow(
											((EditText) findViewById(R.id.et_filtrado))
													.getWindowToken(), 0);

									Log.d("arrayFiltrados", " era != NULL");
								} else {
									Log.d("arrayFiltrados", "es NULL");
								}
								*/
                            //means that search field has given text
                            if (filteredClients != null) {
                                filteredClients = null;
                                getView().findViewById(R.id.ib_cancelar_filtro)
                                        .setVisibility(View.GONE);
                                ((EditText) getView().findViewById(R.id.et_filtrado))
                                        .setText("");

                                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(
                                        ((EditText) getView().findViewById(R.id.et_filtrado))
                                                .getWindowToken(), 0);

                                Log.d("filteredClients", " era != NULL");
                            } else {
                                Log.d("filteredClients", "es NULL");
                            }
                            //if (arrayCercanos == null) {
                            //	arrayCercanos = new ArrayList<JSONObject>();
                            //}
                            if (nearbyClients == null) {
                                nearbyClients = new ArrayList<Clients>();
                            }
                            Drawable drawable = null;
                            //ClientesAdapter adapter = null;

                            //gets all clientes that within radio
								/*
								if (arrayCercanos.size() == 0) {
									for (int i = 0; i < arrayClientes.length(); i++) {
										Gson gson = new Gson();
										try {
											//JSONObject jsonTmp = arrayClientes
											//		.getJSONObject(i);
											JSONObject jsonTmp = new JSONObject( gson.toJson( clientsList.get( i ) ) );

											double latitude = jsonTmp
													.getDouble("latitude");
											double longitude = jsonTmp
													.getDouble("longitude");
											// Log.d("lat "+latitude,
											// "lon "+longitude);
											double distancia = getDistance(
													myLocation, latitude, longitude);
											// double distancia =
											// getDistance(myLocation,
											// 20.736007,-103.47559);
											Log.d("Nombre: "
															+ arrayClientes
															.getJSONObject(i)
															.getString("nombre")
															+ " "
															+ arrayClientes
															.getJSONObject(i)
															.getString(
																	"apellido_pat"),
													"DISTANCIA: "
															+ distancia
															+ " lat"
															+ jsonTmp
															.getDouble("latitude")
															+ " long"
															+ jsonTmp
															.getDouble("longitude"));
											if (distancia <= radio) {
												jsonTmp.put("distancia", distancia);
												// Log.d("Nombre: "+arrayClientes.getJSONObject(i).getString("nombre")+" "+arrayClientes.getJSONObject(i).getString("apellido_pat"),
												// "DISTANCIA: "+distancia+" lat"+latitude+" "+longitude);
												arrayCercanos.add(jsonTmp);
											}
										} catch (JSONException e) {
											e.printStackTrace();
										}
									}*/

                            //gets all clientes that within radio
                            if (nearbyClients.size() == 0) {
                                for (int i = 0; i < clientsList.size(); i++) {
                                    //Gson gson = new Gson();
                                    try {
                                        //JSONObject jsonTmp = arrayClientes
                                        //		.getJSONObject(i);
                                        //JSONObject jsonTmp = new JSONObject( gson.toJson( clientsList.get( i ) ) );
                                        Clients nearbyClientTemp = clientsList.get(i);

                                        double latitude = Double.parseDouble(nearbyClientTemp.getLatitude());
                                        //double latitude = jsonTmp.getDouble("latitude");
                                        //double longitude = jsonTmp.getDouble("longitude");
                                        double longitude = Double.parseDouble(nearbyClientTemp.getLongitude());
                                        // Log.d("lat "+latitude,
                                        // "lon "+longitude);
                                        //double distancia = getDistance(myLocation, latitude, longitude);
                                        double distance = getDistance(myLocation, latitude, longitude);
                                        // double distancia =
                                        // getDistance(myLocation,
                                        // 20.736007,-103.47559);
                                        Log.d("Nombre: "
                                                        + clientsList.get(i).getName()
                                                        + " "
                                                        + clientsList.get(i).getFirstLastName(),
                                                "DISTANCIA: "
                                                        + distance
                                                        + " lat"
                                                        + nearbyClientTemp.getLatitude()
                                                        + " long"
                                                        + nearbyClientTemp.getLongitude()
                                        );
                                        if (distance <= radio) {
                                            //jsonTmp.put("distancia", distance);
                                            nearbyClientTemp.setDistance(Double.toString(distance));
                                            // Log.d("Nombre: "+arrayClientes.getJSONObject(i).getString("nombre")+" "+arrayClientes.getJSONObject(i).getString("apellido_pat"),
                                            // "DISTANCIA: "+distancia+" lat"+latitude+" "+longitude);
                                            //arrayCercanos.add(jsonTmp);
                                            nearbyClients.add(nearbyClientTemp);
                                        }
                                    } catch (Exception e) {
                                        //e.printStackTrace();
                                    }
                                }

                                //shows clients on screen
									/*
									if (arrayCercanos.size() > 0) {
										Toast.makeText(getApplicationContext(),
												"Búsqueda finalizada",
												Toast.LENGTH_SHORT).show();
										//Collections.sort(arrayCercanos,new CustomComparator());
										Collections.sort(nearbyClients, new CustomComparatorToSortByDistance());

										JSONArray arrayEnviar = new JSONArray();
										for (JSONObject json : arrayCercanos) {
											arrayEnviar.put(json);
										}
										Log.e("cercanos " + arrayEnviar.length(),
												"normales: "
														+ arrayClientes.length());
										adapter = new ClientesAdapter(getApplicationContext(), arrayEnviar, myLocation);
										drawable = getResources().getDrawable(
												R.drawable.btn_clientescer_pressed);
									} else {
										Toast.makeText(
												getApplicationContext(),
												"Sin clientes a " + radio
														+ " m de radio",
												Toast.LENGTH_SHORT).show();
										drawable = getResources().getDrawable(
												R.drawable.btn_clientescerc);
										Util.Log.ih("filteredClients.size = " + clientsList.size());
										adapter = getClientesAdapterClientsList();
										arrayCercanos = null;
									}
									*/
                                //shows clients on screen
                                if (nearbyClients.size() > 0) {
                                    /*Toast.makeText(getContext(),
                                            "Búsqueda finalizada",
                                            Toast.LENGTH_SHORT).show();*/
                                    Snackbar.make(mainActivity.mainLayout, "Búsqueda finalizada", Snackbar.LENGTH_SHORT).show();
                                    //Collections.sort(arrayCercanos,new CustomComparator());
                                    Collections.sort(nearbyClients, new ClientesRecycler.CustomComparatorToSortByDistance());

										/*
										JSONArray arrayEnviar = new JSONArray();
										for (JSONObject json : arrayCercanos) {
											arrayEnviar.put(json);
										}
										Log.e("cercanos " + arrayEnviar.length(),
												"normales: "
														+ arrayClientes.length());
										*/
                                    adapter = new ClientesRecyclerAdapter<Clients>(getContext(), nearbyClients, myLocation, ClientesAdapter.NEARBY_CLIENTS, this);
                                    drawable = getResources().getDrawable(
                                            R.drawable.btn_clientescer_pressed);
                                } else {
                                    /*Toast.makeText(
                                            getContext(),
                                            "Sin clientes a " + radio
                                                    + " m de radio",
                                            Toast.LENGTH_SHORT).show();*/
                                    Snackbar.make(mainActivity.mainLayout, "Sin clientes a " + radio + "m de radio", Snackbar.LENGTH_SHORT).show();
                                    drawable = getResources().getDrawable(
                                            R.drawable.btn_clientescerc);
                                    Util.Log.ih("filteredClients.size = " + clientsList.size());
                                    adapter = getClientesRecyclerAdapterClientsList();
                                    //arrayCercanos = null;
                                    nearbyClients = null;
                                }

                            } else { //if there's no cliente near by current location, shows wallet
                                drawable = getResources().getDrawable(
                                        R.drawable.btn_clientescerc);
                                adapter = getClientesRecyclerAdapterClientsList();
                                //arrayCercanos = null;
                                nearbyClients = null;
                            }
                            cercanos.setImageDrawable(drawable);
                            /*lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);
                            if (lvClientes != null) {
                                lvClientes.setAdapter(adapter);
                                lvClientes.setDivider(getResources().getDrawable(
                                        R.drawable.divider_list_clientes));
                                lvClientes.setOnItemClickListener(mthis);
                            }*/
                            rvClientes.setAdapter(adapter);

                        } else {
                            Toast.makeText(getContext(),
                                    "No cuentas con clientes a organizar",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(getContext(),
                            "No se puede obtener tu ubicación",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    mainActivity.showAlertDialog("", "No se ha podido obtener el radio",
                            true);
                } catch (Exception e1) {
                    Toast toast = Toast.makeText(getContext(), "No se ha podido obtener el radio", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });

        salir.setOnClickListener(view -> {
            AlertDialog.Builder alert = getBuilder();
            alert.setMessage(R.string.info_message_close);
            alert.setPositiveButton(R.string.accept, (dialogInterface, which) -> cerrarSesion());
            alert.setNegativeButton(R.string.cancel, null);
            alert.show();
        });

        imageSaldoMomento
                .setOnClickListener(view -> mostrarSaldoAlMomento());

        refresh.setOnClickListener(view -> setImageViewRefreshTouchBehaviour());

        refresh.setOnLongClickListener(view -> {
            Animation rotationAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_around_center_point_animation);
            refresh.startAnimation(rotationAnimation);

            mainActivity.hasActiveInternetConnection(getContext(), mainActivity.mainLayout, refresh);

            return true;
        });

        osticket.setOnClickListener(view -> showOsticketDialog());
    }

    private void showOsticketDialog() {

        try {

            mainActivity.runOnUiThread(() -> {
                dialog = getDialogB();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_osticket);
                dialog.setCancelable(false);
                dialog.getWindow()
                        .setBackgroundDrawable(
                                new ColorDrawable(
                                        Color.TRANSPARENT));
                dialog.show();
            });

            if (dialog != null){
                osticketMessageEdiText = (EditText) dialog.findViewById(R.id.etOsticketMessage);
                spinnerOsticketTopics = (Spinner) dialog.findViewById(R.id.spinnerPeriods);

                String[] items = DatabaseAssistant.getAllOsticketTopics();

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, items);
                //ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.planets_array, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerOsticketTopics.setAdapter(adapter);
                spinnerOsticketTopics.setOnItemSelectedListener(ClientesRecycler.this);
                spinnerOsticketTopics.setSelection( 0 );

                tvCancelar = (TextView) dialog.findViewById(R.id.textView12);
                tvCancelar.setOnClickListener(view -> {
                    if (!mainActivity.isFinishing()) {
                        if (dialog != null && dialog.isShowing()) {
                            Util.Log.ih("dismissing progress dialog");
                            dialog.dismiss();
                            dialog = null;
                        }
                    }
                });

                tvAceptar = (TextView) dialog.findViewById(R.id.textView13);
                tvAceptar.setOnClickListener(view -> {
                    osticketMessage = osticketMessageEdiText.getText().toString();

                    Util.Log.ih("osticketTopicSelected = " + osticketTopicSelected);
                    Util.Log.ih("osticketTopicSelectedPosition = " + osticketTopicSelectedPosition);
                    Util.Log.ih("osticketMessage = " + osticketMessage);

                    if ( osticketMessage.length() < 5 ) {
                        Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_gotta_be_more_specific, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                    else if (!mainActivity.isFinishing()) {
                        Location myLocation = ApplicationResourcesProvider.getLocationProvider().getGPSPosition();

                        DatabaseAssistant.insertOsticket( osticketTopicSelected, osticketMessage, myLocation);
                        executeSendOsticket();

                        if (dialog != null && dialog.isShowing()) {
                            Util.Log.ih("dismissing progress dialog");
                            dialog.dismiss();
                            dialog = null;
                        }
                        Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.info_message_osticket_sending, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            mainActivity.toastS("Hubo un error. Por favor intentalo de nuevo");
        }
    }

    /**
     * returns an instance of Dialog
     * @return instance of Dialog
     */
    private Dialog getDialogB(){
        Dialog mDialog = new Dialog(getContext());
        return mDialog;
    }

    /**
     * set notifications button behaviour
     */
    private void setImageViewNotificationsTouchBehaviour(){
        Intent intent = new Intent(getContext(), Notificaciones.class);
        try {
            intent.putExtra("id_cobrador",
                    datosCobrador.getString("no_cobrador"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        startActivity(intent);
    }

    /**
     * Launch config class
     */
    public void launchConfiguration()
    {
        //---------------------------------------------------------------------------------------------------------------------------------------------
    }

    /**
     * set deposit button behaviour
     */
    private void setImageViewDepositsTouchBehaviour(){
		/*Intent intent = new Intent(getContext(), Deposito.class);
		mainActivity.isConnected = ExtendedActivity.setInternetAsReachable;
		intent.putExtra("isConnected", mainActivity.isConnected);
		intent.putExtra("datos_cobrador", datosCobrador.toString());
		startActivityForResult(intent, 2);*/

        Deposito deposito = new Deposito();
        Bundle args = new Bundle();

        args.putBoolean("isConnected", mainActivity.isConnected);
        args.putString("datos_cobrador", datosCobrador.toString());
        args.putInt("requestCode", 2);

        deposito.setArguments(args);
        deposito.setFragmentResult(this);

        getFragmentManager().beginTransaction()
                .add(R.id.root_layout, deposito, MainActivity.TAG_DEPOSITO)
                .hide(this)
                .addToBackStack("deposito")
                .commitAllowingStateLoss();
    }

    /**
     * set 'fuera de cartera' button behaviour
     */
    private void setImageViewFueraDeCarteraTouchBehaviour(){
        //it will only start Aportacion activity if it is connected to internet
        //if (isConnected) {
        Aportacion aportacion = new Aportacion();
        Bundle args = new Bundle();

        //Intent aportacionIntent = new Intent(getContext(), Aportacion.class);
        args.putString("datosCobrador", datosCobrador.toString());
        args.putBoolean("isConnected", mainActivity.isConnected);
        args.putInt("requestCode", 3);
        //startActivityForResult(aportacionIntent, 3);
        aportacion.setArguments(args);
        aportacion.setFragmentResult(this);

        getFragmentManager().beginTransaction()
                .add(R.id.root_layout, aportacion, MainActivity.TAG_APORTACION)
                .hide(this)
                .addToBackStack("aportacion")
                .commitAllowingStateLoss();

			/*
			startActivityForResult(new Intent(
					getApplicationContext(),
					Aportacion.class).putExtra(
					"datosCobrador", datosCobrador.toString()), 3);
					*/
			/*
		} else {
			try {
				showAlertDialog(
						"",
						getString(R.string.message_trabajando_modo_offline),
						true);
			} catch (Exception e) {
				Toast toast = Toast.makeText(getApplicationContext(), R.string.message_trabajando_modo_offline, Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				e.printStackTrace();
			}
		}
		*/
    }

    /**
     * set refresh button behaviour
     */
    private void setImageViewRefreshTouchBehaviour(){
        try{
            showMyCustomDialog();
        } catch (Exception e){
            Toast toast = Toast.makeText(getContext(), R.string.info_message_network_offline, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            e.printStackTrace();
        }

        if (nearbyClients != null || nearbyClientsFromSyncedWallet != null) {// si el filtro de cercanos
            // esta activado, se pasa a
            // desactivar.
            Log.d("nearbyClients", "!= NULL");
            final ImageButton cercanos = (ImageButton) getView().findViewById(R.id.btn_buscar_cercanos);
            cercanos.performClick();
        } else {
            Log.d("nearbyClients", "NULL");
        }
        //if search field has text, gets desactivated
		/*
		if (arrayFiltrados != null) {
			arrayFiltrados = null;
			findViewById(R.id.ib_cancelar_filtro)
					.setVisibility(View.GONE);
			((EditText) findViewById(R.id.et_filtrado))
					.setText("");

			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(
					((EditText) findViewById(R.id.et_filtrado))
							.getWindowToken(), 0);

			Log.d("arrayFiltrados", " era != NULL");
		} else {
			Log.d("arrayFiltrados", "es NULL");
		}
		*/
        //if search field has text, gets desactivated
        if (filteredClients != null) {
            filteredClients = null;
            getView().findViewById(R.id.ib_cancelar_filtro)
                    .setVisibility(View.GONE);
            ((EditText) getView().findViewById(R.id.et_filtrado))
                    .setText("");

            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(
                        ((EditText) getView().findViewById(R.id.et_filtrado))
                                .getWindowToken(), 0);
            }

            Log.d("filteredClients", " era != NULL");
        } else {
            Log.d("filteredClients", "es NULL");
        }



		/*
		check if it's day 1 and period hasn't been closed yet
		if so, start a new period
		 */
        DatabaseAssistant.insertNewPeriodFromScratchIfNeeded( mainActivity.getEfectivoPreference().getInt("periodo", 0) );
        if ( DatabaseAssistant.checkThatPeriodWasClosedOnLastDayOfMonth( mainActivity.getEfectivoPreference().getInt("periodo", 0) ) ) {

            List<Companies> companies = DatabaseAssistant.getCompanies();
			/*
			int size;
			switch (BuildConfig.getTargetBranch()){
				case GUADALAJARA:
					size = companies.size();
					break;
				case TOLUCA:
					size = 2;
					break;
				case PUEBLA:
					size = 2;
					break;
				case MERIDA:
					size = 1;
					break;
				case SALTILLO:
					size = 1;
					break;
				default:
					size = 4;
			}
			*/

            for (int cont = 0; cont < companies.size(); cont++) {
                Companies company = companies.get(cont);

                DatabaseAssistant.insertCompanyAmountsByPeriodWhenMoreThanOnePeriodIsActive(
                        company.getName(),
                        mainActivity.getEfectivoPreference().getFloat(company.getName(), 0),
                        mainActivity.getEfectivoPreference().getInt("periodo", 0)
                );

                mainActivity.getEfectivoEditor().putFloat(company.getName(), 0).apply();
            }

            mainActivity.getEfectivoEditor().putInt("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0) + 1).apply();

            checkPendingPeriod();
        }



        getListClients();
    }

    /**
     * set 'cierre informativo' button behaviour
     */
    private void setImageViewCierreInformativoTouchBehaviour(){
        //if (isConnected) {

        Util.Log.ih("Clientes - setInternetAsReachable = " + ExtendedActivity.setInternetAsReachable);

        CierreInformativo cierreInformativo = new CierreInformativo();

        Bundle args = new Bundle();
        try {
            args.putString("no_cobrador",
                    getCierreOfDebtCollectorCode == null ? datosCobrador.getString("no_cobrador") : getCierreOfDebtCollectorCode);
            args.putString("efectivo",
                    datosCobrador.getString("efectivo"));
            args.putString("nombre",
                    datosCobrador.getString("nombre"));
            args.putBoolean("printCierrePerido", printCierrePerido);

            //intent.putExtra("contratos", arrayClientes.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        args.putInt("requestCode", 4);

        cierreInformativo.setArguments(args);
        cierreInformativo.setFragmentResult(this);

        //startActivityForResult(intent, 4);
        getFragmentManager().beginTransaction()
                .add(R.id.root_layout, cierreInformativo, MainActivity.TAG_CIERRE_INFORMATIVO)
                .hide(this)
                .addToBackStack("cierre_informativo")
                .commitAllowingStateLoss();
		/*} else {
			try {
				showAlertDialog(
						"",
						getString(R.string.message_trabajando_modo_offline),
						true);
			} catch (Exception e) {
				Toast toast = Toast.makeText(getApplicationContext(), R.string.message_trabajando_modo_offline, Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				e.printStackTrace();
			}
		}*/
    }

    private void showListOfPreviousPeriods()
    {
        PreviousPeriods previousPeriods = new PreviousPeriods();

        Snackbar.make(mainActivity.mainLayout, "Secret Menu", Snackbar.LENGTH_SHORT).show();

        Bundle args = new Bundle();
        try {
            args.putString("no_cobrador",
                    getCierreOfDebtCollectorCode == null ? datosCobrador.getString("no_cobrador") : getCierreOfDebtCollectorCode);
            args.putString("efectivo",
                    datosCobrador.getString("efectivo"));
            args.putString("nombre",
                    datosCobrador.getString("nombre"));
            args.putBoolean("printCierrePerido",
                    !printCierrePerido);

            //intent.putExtra("contratos", arrayClientes.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        args.putInt("requestCode", 4);

        previousPeriods.setArguments(args);
        //previousPeriods.setFragmentResult(this);

        //startActivityForResult(intent, 4);
        if (getFragmentManager() != null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.root_layout, previousPeriods, MainActivity.TAG_PREVIOUS_PERIODS)
                    .hide(this)
                    .addToBackStack(MainActivity.TAG_PREVIOUS_PERIODS)
                    .commitAllowingStateLoss();
        }
    }

    /**
     * checks if cash is within limit cash
     * @param efectivo
     */
    private void verificarEfectivoAlerta(double efectivo) {
        try {
            double maxEfectivo = datosCobrador.getInt("max_efectivo");

            double diferencia = maxEfectivo - efectivo;
            if (diferencia <= MAX_EFECTIVO_ALERTA) {
                try {
                    mainActivity.showAlertDialog("", "Te resta $" + diferencia
                            + " para llegar al máximo de efectivo.", false);
                } catch (Exception e){
                    Toast toast = Toast.makeText(getContext(), "Te resta $" + diferencia + " para llegar al máximo de efectivo", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if there's a pending ticket to be printed
     */
    private void checkPendingTicket(){

        /**
         * Checks if there's a payment
         * and
         * checks if payment was made and printed (is synchronized)
         * receives false: is not synchronized
         * receives true: is synchronized
         */
        if ( !preferencesPendingTicket.isThereAPaymentAndIsSynchronized() ) {

            /**
             * Checks if payment was made and printed (is synchronized)
             * receives false: is not synchronized
             * receives true: is synchronized
             */
            //if (!preferencesPendingTicket.isSynchronized()) {

            /**
             * Gets folio from last payment registered
             * and shows it
             */
            JSONObject lastPayment = preferencesPendingTicket.getPayment();

            try {

                if (pendingTicketAlert != null && pendingTicketAlert.isShowing())
                    pendingTicketAlert.dismiss();

                pendingTicketAlert = getAlertDialogBuilder()
                        .setTitle(R.string.info_title_pending)
                        .setMessage(
                                getString(R.string.info_message_pending1, lastPayment.getString(getString(R.string.key_folio)), lastPayment.getString(getString(R.string.key_contract_number)))
                                        /*+ lastPayment.getString(getString(R.string.key_folio))
                                        + getString(R.string.info_message_pending2)
                                        + lastPayment.getString(getString(R.string.key_contract_number))
                                        + getString(R.string.info_message_pending3)*/
                        )
                        .setCancelable(false)
                        .setNeutralButton(R.string.ok, null)
                        .create();

                pendingTicketAlert.show();

            } catch (JSONException e){
                e.printStackTrace();
            }
            //}
        }
    }

    private void checkPendingPeriod() {
        if ( DatabaseAssistant.isThereMoreThanOnePeriodActive() ) {
            try {

                if (pendingTicketAlert != null && pendingTicketAlert.isShowing())
                    pendingTicketAlert.dismiss();

                pendingTicketAlert = getAlertDialogBuilder()
                        .setTitle(R.string.info_title_pending_period)
                        .setMessage(
                                getString(R.string.info_message_pending_period)
                        )
                        .setCancelable(false)
                        .setNeutralButton(R.string.ok, null)
                        .create();

                pendingTicketAlert.show();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void setNull(){
        filteredClients = null;

        arrayCercanos = null;
        nearbyClientsFromSyncedWallet = null;
        nearbyClients = null;
    }

    //endregion

    //region Protected

    //endregion

    //region Getters

    /**
     * Gets context from this class.
     * In some cases you cannot just call 'this', because another context is envolved
     * or you want a reference to an object of this class from another class,
     * then this context is helpful.
     *
     * NOTE
     * Not recommended, it may cause NullPointerException though
     *
     * @return context from Clientes
     */
    public static ClientesRecycler getMthis() {
        return mthis;
    }

    /**
     * Provides an instance of ClientesAdapter
     * using filteredClients variable
     * @return ClientesAdapter's instance
     */
    private ClientesAdapter getClientesAdapterFilteredClients(){
        //return new ClientesAdapter(this, arrayFiltrados);
        return new ClientesAdapter<Clients>(getContext(), filteredClients, ClientesAdapter.FILTERED_CLIENTS);
    }

    /**
     * Provides an instance of ClientesAdapter
     * using clientsList variable
     * @return ClientesAdapter's instance
     */
    private ClientesAdapter getClientesAdapterClientsList(){
        return new ClientesAdapter<Clients>(mainActivity.getApplicationContext(), clientsList, ClientesAdapter.WALLET_CLIENTS);
    }

    private ClientesAdapter getAdapterSyncedWalletClients(){
        //clientListFromSyncedWalletForAdapter = new ArrayList<>();
        setNull();
        return new ClientesAdapter<SyncedWallet>(mainActivity.getApplicationContext(), clientListFromSyncedWallet, ClientesAdapter.DAY_WALLET_CLIENTS);
    }

    private ClientesRecyclerAdapter getClientesRecyclerAdapterClientsList()
    {
        setNull();
        return new ClientesRecyclerAdapter<Clients>(mainActivity.getApplicationContext(), clientsList, ClientesRecyclerAdapter.WALLET_CLIENTS, this);
    }

    private ClientesRecyclerAdapter getClientesRecyclerAdapterSyncedWalletClients()
    {
        setNull();
        return new ClientesRecyclerAdapter<SyncedWallet>(mainActivity.getApplicationContext(), clientListFromSyncedWallet, ClientesRecyclerAdapter.DAY_WALLET_CLIENTS, this);
    }

    /**
     * returns an instance of Dialog
     * @return instance of Dialog
     */
    private Dialog getDialog(){
        return new Dialog(getContext());
    }

    /**
     * returns an instance of Builder
     * @return instance of Builder
     */
    private AlertDialog.Builder getBuilder(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            return new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Light_Dialog_Alert);
        }
        else{
            return new AlertDialog.Builder(getContext());
        }
    }

    /**
     * returns an instance of AlertDialogBuilder
     * @return instance of AlertDialogBuilder
     */
    private AlertDialog.Builder getAlertDialogBuilder(){
        return new AlertDialog.Builder(getContext());
    }

    /**
     * returns an instance of ProgressDialog
     * @return instance of ProgressDialog
     */
    private ProgressDialog getProgressDialog(){
        return new ProgressDialog(getContext());
    }

    //endregion

    //region Setters

    /**
     * Sets 'mthis' global variable as Clientes's context
     * @param mthis Clientes's context
     */
    public static void setMthis(ClientesRecycler mthis) {
        ClientesRecycler.mthis = mthis;
    }

    //endregion

    //region Interfaces

    @Override
    public void onGetClientsFromSyncedWallet(List<SyncedWallet> clientListFromSyncedWallet) {
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        Util.Log.ih("onGetClientsFromSyncedWallet interface");
        this.clientListFromSyncedWallet = clientListFromSyncedWallet;
        if (firstLoad) {
            this.actualizarListView();
            firstLoad = false;
        }
        updateListView();
    }

    //region CerrarSesionTimeOut

    @Override
    public void cerrarSesionTimeOut() {
        if (mainActivity.handlerSesion != null) {
            mainActivity.handlerSesion.removeCallbacks(mainActivity.myRunnable);
            mainActivity.handlerSesion = null;
        }

        View refBuscarClientes = null;
        if (BuscarCliente.getMthis() != null) {
            refBuscarClientes = BuscarCliente.getMthis().findViewById(
                    R.id.lv_buscar_clientes);
        }

        View refAportacion = null;
        if (Aportacion.getMthis() != null) {
            refAportacion = Aportacion.getMthis().getView().findViewById(
                    R.id.et_nombre_apellidos);
        }

        View refCierre = null;
        if (Cierre.getMthis() != null) {
            refCierre = Cierre.getMthis().getView().findViewById(R.id.lv_cierre);
        }

        View refDeposito = null;
        if (Deposito.getMthis() != null) {
            refDeposito = Deposito.getMthis().getView().findViewById(R.id.tv_banco);
        }

        View refDetalleCliente = null;
        if (ClienteDetalle.getMthis() != null) {
            refDetalleCliente = ClienteDetalle.getMthis().getView().findViewById(
                    R.id.et_cantidad_aportacion);
        }

        View refNotificaciones = null;
        if (Notificaciones.getMthis() != null) {
            refNotificaciones = Notificaciones.getMthis().findViewById(
                    R.id.lv_notification);
        }

        if (refBuscarClientes != null) {
            BuscarCliente.getMthis().finish();
        } else if (refAportacion != null) {
            Aportacion.getMthis().getActivity().finish();
        }

        if (refCierre != null) {
            Cierre.getMthis().getActivity().finish();
        }

        if (refDeposito != null) {
            Deposito.getMthis().getActivity().finish();
        }

        if (refDetalleCliente != null) {
            ClienteDetalle.getMthis().getActivity().finish();
        }

        if (refNotificaciones != null) {
            Notificaciones.getMthis().finish();
        }
        Util.Log.ih("SESION CERRADA");

        cerrarSesion();

    }

    //endregion

    //region OnItemSelected

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
        osticketTopicSelected = adapterView.getItemAtPosition(pos).toString();
        osticketTopicSelectedPosition = pos;
    }

    //endregion

    //region OnNothingSelected

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    //endregion

    //region OnItemClick

    //@Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

        this.position = position;
        //this.listViewState = lvClientes.onSaveInstanceState();
        JSONObject jsonTmp = null;

        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(
                ((EditText) getView().findViewById(R.id.et_filtrado))
                        .getWindowToken(), 0);

        try {

            Gson gson = new Gson();

            if (filteredClients != null && filteredClients.size() > 0) {
                //jsonTmp = arrayFiltrados.getJSONObject(position);
                Util.Log.ih( "gsonToJson = " + new JSONObject( gson.toJson( filteredClients.get( position ) ) ) );
                jsonTmp = new JSONObject( gson.toJson( filteredClients.get( position ) ) );
            } else if (nearbyClients != null || nearbyClientsFromSyncedWallet != null) {
                //jsonTmp = arrayCercanos.get(position);
                if (BuildConfig.isWalletSynchronizationEnabled()){
                    //jsonTmp = new JSONObject( gson.toJson( nearbyClientsFromSyncedWallet.get(position) ) );
                    Clients client = DatabaseAssistant.getSingleClient(nearbyClientsFromSyncedWallet.get(position).getIdContract());
                    if (client != null) {
                        jsonTmp = new JSONObject(gson.toJson(client));
                    }
                    else {
                        throw new NullPointerException();
                    }
                }
                else {
                    jsonTmp = new JSONObject( gson.toJson( nearbyClients.get(position) ) );
                }
            } else {
                //jsonTmp = arrayClientes.getJSONObject(position);
                if (BuildConfig.isWalletSynchronizationEnabled()){
                    //jsonTmp = new JSONObject(gson.toJson(clientListFromSyncedWallet.get(position).getClient()));
                    Clients client = DatabaseAssistant.getSingleClient( clientListFromSyncedWallet.get(position).getContractID() );
                    if (client != null) {
                        jsonTmp = new JSONObject(gson.toJson(client));
                    }
                    else {
                        throw new NullPointerException();
                    }
                }
                else {
                    jsonTmp = new JSONObject(gson.toJson(clientsList.get(position)));
                }
            }
            //Log.d("Cliente", jsonTmp.toString(1));
            Util.Log.ih("jsonTmp = " + jsonTmp.toString());

            ClienteDetalle clienteDetalle = new ClienteDetalle();

            Bundle args = new Bundle();
            //Intent intent = new Intent(getContext(), ClienteDetalle.class);
            args.putString("datos", jsonTmp.toString());
            args.putString("datoscobrador", datosCobrador.toString());
            mainActivity.isConnected = false;
            args.putBoolean("isConnected", mainActivity.isConnected);
            args.putString("callback", this.toString());
            args.putInt("requestCode", 1);
            //startActivityForResult(intent, 1);
            clienteDetalle.setArguments(args);
            clienteDetalle.setFragmentResult(this);

            getFragmentManager().beginTransaction()
                    .add(R.id.root_layout, clienteDetalle, MainActivity.TAG_CLIENTE_DETALLES)
                    .hide(this)
                    .addToBackStack("cliente_detalle")
                    .commitAllowingStateLoss();
        } catch (JSONException e) {
            mainActivity.toastS(R.string.error_message_call);
            e.printStackTrace();
        } catch (NullPointerException e){
            mainActivity.toastS(R.string.error_message_call);
            e.printStackTrace();
        }
    }

    //endregion

    //region OnActivityResult


    @Override
    public void onFragmentResult(int requestCode, int resultCode, Intent data) {
        //updates cash
        /**
         * came back from ClienteDetalle
         */
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String maxEfec = "";
                try {
                    maxEfec = datosCobrador.getString("max_efectivo");
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                boolean paymentDone = data.getBooleanExtra("paymentDone", false);

                //add comments to client
				/*
				Util.Log.ih("adding comments");
				if (data.getStringExtra("id_contract") != null) {
					Util.Log.ih("id_contract != null");

					if (filteredClients != null && filteredClients.size() > 0) {
						for (int i = 0; i < filteredClients.size(); i++) {
							if (filteredClients.get(i).getIdContract().equals(data.getStringExtra("id_contract"))) {
								filteredClients.get(i).setComment(data.getStringExtra("comments"));
								Util.Log.ih("comentario agregado a filteredClients");
							}
						}
					} else if (nearbyClients != null && nearbyClients.size() > 0) {
						for (int i = 0; i < nearbyClients.size(); i++) {
							if (nearbyClients.get(i).getIdContract().equals(data.getStringExtra("id_contract"))) {
								nearbyClients.get(i).setComment(data.getStringExtra("comments"));
								Util.Log.ih("comentario agregado a nearbyClients");
							}
						}
					}
					//always update clientList
					for (int i = 0; i < clientsList.size(); i++) {
						if (clientsList.get(i).getIdContract().equals(data.getStringExtra("id_contract"))) {
							clientsList.get(i).setComment(data.getStringExtra("comments"));
							Util.Log.ih("comentario agregado a clientsList");
						}
					}
				}
				Util.Log.ih("finished comments");
				*/

                if (paymentDone) {

                    //after making a payment
                    //move contract selected to last  position
                    //(bottom of screen)
                    //and give it base color
                    if (BuildConfig.isWalletSynchronizationEnabled()){

                        //if (filteredClients == null && (nearbyClients == null || nearbyClientsFromSyncedWallet == null)){
                        if (clientListFromSyncedWallet != null && clientListFromSyncedWallet.size() > 0){

                            if (filteredClients != null && filteredClients.size() > 0){
                                String contractIDFromFilteredClient = filteredClients.get(position).getIdContract();
                                for (int i = 0; i < clientListFromSyncedWallet.size(); i++){
                                    if (clientListFromSyncedWallet.get(i).getContractID().equals(contractIDFromFilteredClient)){
                                        updateClientList(i, false);
                                        break;
                                    }
                                }
                            } else if (nearbyClientsFromSyncedWallet != null){
                                String contractIDFromFilteredClient = nearbyClientsFromSyncedWallet.get(position).getIdContract();
                                for (int i = 0; i < clientListFromSyncedWallet.size(); i++){
                                    if (clientListFromSyncedWallet.get(i).getContractID().equals(contractIDFromFilteredClient)){
                                        updateClientList(i, false);
                                        break;
                                    }
                                }
                            } else {
                                updateClientList(position, false);
                                updateListView();
                            }
                        }
                    }

                    //TextView tv = (TextView) findViewById(R.id.tv_total_efec);
                    //tv.setText("$"
                    //		+ formatMoney(Double.parseDouble(data
                    //		.getStringExtra("nuevo_efectivo"))) + "/$"
                    //		+ formatMoney(Double.parseDouble(maxEfec)));
                    try {
                        // datosCobrador.put("efectivo",
                        // data.getStringExtra("nuevo_efectivo"));
                        modificarInfoToLoginAndSplash();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (mainActivity.getTotalEfectivo() > Double.parseDouble(maxEfec)) {

                        modificarInfoToLoginAndSplash();
                        cerrarTodoYMandarBloqueo();
                        // if (!isConnected) {
                        // modificarInfoToLoginAndSplash();
                        // alertaDeCobrosOffline();
                        //
                        // } else {
                        // mandarABloqueo = false;
                        // cerrarTodoYMandarBloqueo();
                        // }
                    } else {
                        try {
                            // double efectivo = Double.parseDouble(data
                            // .getStringExtra("nuevo_efectivo"));
                            calcularTotal();
                            verificarEfectivoAlerta(mainActivity.getTotalEfectivo());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    //registerPagos();
                } else {
                    //after making a visit
                    //increase visit counter
                    //and if visit counter if greater or equal to 2
                    //set not to show color
                    //and move to last position (bottom of screen)
                    //if (filteredClients == null && (nearbyClients == null || nearbyClientsFromSyncedWallet == null)) {
                    if (clientListFromSyncedWallet != null && clientListFromSyncedWallet.size() > 0) {

                        if (filteredClients != null && filteredClients.size() > 0){
                            String contractIDFromFilteredClient = filteredClients.get(position).getIdContract();
                            for (int i = 0; i < clientListFromSyncedWallet.size(); i++){
                                if (clientListFromSyncedWallet.get(i).getContractID().equals(contractIDFromFilteredClient)){
                                    updateClientList(i, true);
                                    break;
                                }
                            }
                        } else if (nearbyClientsFromSyncedWallet != null){
                            String contractIDFromFilteredClient = nearbyClientsFromSyncedWallet.get(position).getIdContract();
                            for (int i = 0; i < clientListFromSyncedWallet.size(); i++){
                                if (clientListFromSyncedWallet.get(i).getContractID().equals(contractIDFromFilteredClient)){
                                    updateClientList(i, true);
                                    break;
                                }
                            }
                        } else {
                            updateClientList(position, true);
                            updateListView();
                        }
							/*
								SyncedWallet sw = clientListFromSyncedWallet.get(position);
								sw.setVisitCounter(sw.getVisitCounter() + 1);

								if (sw.getVisitCounter() >= 2) {
									sw.setShowColor(false);
									sw.setColor(SyncedWallet.COLOR_DEFAULT_CONTRACT);

									clientListFromSyncedWallet.remove(position);

									for (int i = clientListFromSyncedWallet.size() - 1; i >= 0; i--) {
										if (!clientListFromSyncedWallet.get(i).getColor().equals(SyncedWallet.COLOR_WITHOUT_ANALYSIS_CONTRACT)) {
											clientListFromSyncedWallet.add(i + 1, sw);
											break;
										}
									}

									//clientListFromSyncedWallet.add(sw);

									setNull();

									adapter = getAdapterSyncedWalletClients();
									lvClientes = (ListView) findViewById(R.id.listView_clientes);
									lvClientes.setAdapter(adapter);
									if (listViewState != null){
										Util.Log.ih("trying to restore ListView state...");
										lvClientes.onRestoreInstanceState(listViewState);
									}
									lvClientes.setDivider(getResources().getDrawable(R.drawable.divider_list_clientes));
									lvClientes.setOnItemClickListener(this);
								}
							*/
                        //}
                    }
                    //}
                }

                if (BuildConfig.isAutoPrintingEnable()){
                    randomListClick();
                }

                registerPagos();
            }
        }
        /**
         * came back from Deposito
         */
        else if (requestCode == 2) {

            //NEW PROCESS
            if ( DatabaseAssistant.isThereMoreThanOnePeriodActive() ) {
                try {
                    SharedPreferences.Editor efectivoEditor = mainActivity.getEfectivoEditor();

                    int lastPeriod = DatabaseAssistant.getAllPeriodsActive()
                            .get(DatabaseAssistant.getNumberOfPeriodsActive() - 1)
                            .getPeriodNumber();
                    List<CompanyAmounts> companyAmounts = DatabaseAssistant.getCompanyAmountWhenMoreThanOnePeriodIsActive(
                            lastPeriod
                    );
                    mainActivity.getEfectivoEditor().putInt(
                            "periodo",
                            lastPeriod
                    ).apply();
                    for (CompanyAmounts ca : companyAmounts) {
                        efectivoEditor.putFloat(ca.getCompanyName(), ca.getAmount()).apply();
                    }

                    Util.Log.ih("onItemSelected end");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            if (resultCode == Activity.RESULT_OK) {
                String maxEfec = "";
                try {
                    maxEfec = datosCobrador.getString("max_efectivo");
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                //TextView tv = (TextView) findViewById(R.id.tv_total_efec);
				/*tv.setText("$ "
						+ formatMoney(Double.parseDouble(data
						.getStringExtra("nuevo_efectivo"))) + "/$"
						+ formatMoney(Double.parseDouble(maxEfec)));
				*/

                //tv.setText("$ "
                //		+ (getTotalEfectivo())
                //		+ "/$"
                //		+ formatMoney(Double.parseDouble(maxEfec)));
                //try {
                //	datosCobrador.put("efectivo",
                //			data.getStringExtra("nuevo_efectivo"));
                modificarInfoToLoginAndSplash();
                //} catch (JSONException e) {
                //	e.printStackTrace();
                //}
                if (mainActivity.getTotalEfectivo() > Double.parseDouble(maxEfec)) {
                    modificarInfoToLoginAndSplash();
                    cerrarTodoYMandarBloqueo();
                    // if (!isConnected) {
                    // modificarInfoToLoginAndSplash();
                    // alertaDeCobrosOffline();
                    // } else {
                    // mandarABloqueo = false;
                    // cerrarTodoYMandarBloqueo();
                    // }
                } else {
                    try {
                        //double efectivo = Double.parseDouble(data
                        //		.getStringExtra("nuevo_efectivo"));
                        verificarEfectivoAlerta(mainActivity.getTotalEfectivo());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        /**
         * came back from Aportacion
         */
        else if (requestCode == 3) {
            if (resultCode == Activity.RESULT_OK) {
                String maxEfec = "";
                try {
                    maxEfec = datosCobrador.getString("max_efectivo");
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                //TextView tv = (TextView) findViewById(R.id.tv_total_efec);
                //tv.setText("$"
                //		+ formatMoney(Double.parseDouble(data
                //		.getStringExtra("nuevo_efectivo"))) + "/$"
                //		+ formatMoney(Double.parseDouble(maxEfec)));
                try {
                    datosCobrador.put("efectivo",
                            data.getStringExtra("nuevo_efectivo"));
                    modificarInfoToLoginAndSplash();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mainActivity.getTotalEfectivo() > Double.parseDouble(maxEfec)) {
                    modificarInfoToLoginAndSplash();
                    cerrarTodoYMandarBloqueo();
                    // if (!isConnected) {
                    // modificarInfoToLoginAndSplash();
                    // alertaDeCobrosOffline();
                    // } else {
                    // mandarABloqueo = false;
                    // cerrarTodoYMandarBloqueo();
                    // }
                } else {
                    try {
                        double efectivo = Double.parseDouble(data
                                .getStringExtra("nuevo_efectivo"));
                        verificarEfectivoAlerta(mainActivity.getTotalEfectivo());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                registerPagos();
            }
        }
        /**
         * came back from CierreInformativo
         */
        else if (requestCode == 4) {

            if ( DatabaseAssistant.isThereMoreThanOnePeriodActive() ) {
                List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();
                mainActivity.getEfectivoEditor().putInt("periodo", periods.get( periods.size()-1 ).getPeriodNumber()).apply();
            }

            Util.Log.ih("came back from CierreInformativo");
            if (resultCode == Activity.RESULT_OK) {
                cerrarSesion();
            }
            else if (resultCode == 99) {
                Util.Log.ih("RESULT_FIRST_USER");

                showMyCustomDialog();

                if (BuildConfig.isWalletSynchronizationEnabled()) {

                    Util.Log.ih("isWalletSynchronizationEnabled");

                    Thread thread = new Thread(() -> {
                        clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();

                        mainActivity.runOnUiThread(() -> {
                            //clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
                            //clientsList = DatabaseAssistant.getAllClients();
                            adapter = getClientesRecyclerAdapterSyncedWalletClients();
                            /*lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);
                            if (lvClientes != null) {
                                lvClientes.setAdapter(adapter);
                                lvClientes.setDivider(getResources().getDrawable(R.drawable.divider_list_clientes));
                                lvClientes.setOnItemClickListener(mthis);
                            }*/
                            rvClientes.setAdapter(adapter);

                            dismissMyCustomDialog();
                        });
                    });
                    thread.start();

					/*
					clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
					//clientsList = DatabaseAssistant.getAllClients();
					adapter = getAdapterSyncedWalletClients();
					lvClientes = (ListView) findViewById(R.id.listView_clientes);
					lvClientes.setAdapter(adapter);
					lvClientes.setDivider(getResources().getDrawable(R.drawable.divider_list_clientes));
					lvClientes.setOnItemClickListener(mthis);
					*/
                }
                else {
                    Util.Log.ih("isWalletSynchronizationEnabled - NO");
                    //clientListFromSyncedWallet = DatabaseAssistant.getClientsFromSyncedWallet();
                    clientsList = DatabaseAssistant.getAllClients();
                    adapter = getClientesRecyclerAdapterClientsList();
                    /*lvClientes = (ListView) getView().findViewById(R.id.listView_clientes);
                    if (lvClientes != null) {
                        lvClientes.setAdapter(adapter);
                        lvClientes.setDivider(getResources().getDrawable(R.drawable.divider_list_clientes));
                        lvClientes.setOnItemClickListener(mthis);
                    }*/
                    rvClientes.setAdapter(adapter);

                    dismissMyCustomDialog();
                }
            }
        }
    }

    @Override
    public void onItemClick(Clients item, int position) {
        try {

            this.position = position;

            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(
                    ((EditText) getView().findViewById(R.id.et_filtrado))
                            .getWindowToken(), 0);

            JSONObject jsonTmp;

            Gson gson = new Gson();

            jsonTmp = new JSONObject(gson.toJson(item));

            Util.Log.ih("jsonTmp = " + jsonTmp.toString());

            ClienteDetalle clienteDetalle = new ClienteDetalle();

            Bundle args = new Bundle();
            //Intent intent = new Intent(getContext(), ClienteDetalle.class);
            args.putString("datos", jsonTmp.toString());
            args.putString("datoscobrador", datosCobrador.toString());
            mainActivity.isConnected = false;
            args.putBoolean("isConnected", mainActivity.isConnected);
            args.putString("callback", this.toString());
            args.putInt("requestCode", 1);
            //startActivityForResult(intent, 1);
            clienteDetalle.setArguments(args);
            clienteDetalle.setFragmentResult(this);

            getFragmentManager().beginTransaction()
                    .add(R.id.root_layout, clienteDetalle, MainActivity.TAG_CLIENTE_DETALLES)
                    .hide(this)
                    .addToBackStack("cliente_detalle")
                    .commitAllowingStateLoss();
        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void onItemClick(SyncedWallet item, int position) {
        try {

            this.position = position;

            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(
                    ((EditText) getView().findViewById(R.id.et_filtrado))
                            .getWindowToken(), 0);

            JSONObject jsonTmp;

            Gson gson = new Gson();

            Clients client = DatabaseAssistant.getSingleClient(item.getContractID());

            jsonTmp = new JSONObject(gson.toJson(client));

            Util.Log.ih("jsonTmp = " + jsonTmp.toString());

            ClienteDetalle clienteDetalle = new ClienteDetalle();

            Bundle args = new Bundle();
            //Intent intent = new Intent(getContext(), ClienteDetalle.class);
            args.putString("datos", jsonTmp.toString());
            args.putString("datoscobrador", datosCobrador.toString());
            mainActivity.isConnected = false;
            args.putBoolean("isConnected", mainActivity.isConnected);
            args.putString("callback", this.toString());
            args.putInt("requestCode", 1);
            //startActivityForResult(intent, 1);
            clienteDetalle.setArguments(args);
            clienteDetalle.setFragmentResult(this);

            getFragmentManager().beginTransaction()
                    .add(R.id.root_layout, clienteDetalle, MainActivity.TAG_CLIENTE_DETALLES)
                    .hide(this)
                    .addToBackStack("cliente_detalle")
                    .commitAllowingStateLoss();
        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (v.getId() == R.id.imageViewCierre)
            cierreLongClicked = true;
        if (v.getId() == R.id.btn_show_saldos)
            efectivoLongClicked = true;

        if (cierreLongClicked && efectivoLongClicked) {
            showListOfPreviousPeriods();
            return true;
        }

        previousPeriodsHandler.postDelayed(previousRunnable, 100);

        SnackProgressBar progressBar = new SnackProgressBar(SnackProgressBar.TYPE_CIRCULAR, "Actualizando cartera...");
        progressBar.setIsIndeterminate(false);
        progressBar.setProgressMax(100);
        progressBar.setShowProgressPercentage(true);

        mainActivity.progressBarManager.show(progressBar, SnackProgressBarManager.LENGTH_INDEFINITE);

        new Timer().scheduleAtFixedRate(new TimerTask() {
            int i = 0;

            @Override
            public void run() {
                i++;
                if (i <= 100)
                    mainActivity.runOnUiThread(() -> mainActivity.progressBarManager.setProgress(i));
                else {
                    mainActivity.progressBarManager.dismiss();
                    this.cancel();
                }
            }
        }, 0, 50);

        return true;
    }

    private static class CustomLinearLayoutManager extends LinearLayoutManager{

        private CustomLinearLayoutManager(Context context)
        {
            super(context);
        }

        public CustomLinearLayoutManager(Context context, int orientation, boolean reverseLayout)
        {
            super(context, orientation, reverseLayout);
        }

        public CustomLinearLayoutManager(Context context, AttributeSet attrs, int defSyleAttr, int defStyleRes)
        {
            super(context, attrs, defSyleAttr, defStyleRes);
        }

        @Override
        public boolean supportsPredictiveItemAnimations() {
            return false;
        }
    }
}
