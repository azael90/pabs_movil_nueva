package com.jaguarlabs.pabs.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidadvance.topsnackbar.TSnackbar;
import com.google.gson.Gson;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.EmpresasSpinnerAdapter;
import com.jaguarlabs.pabs.database.Banks;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.CompanyAmounts;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.MontosTotalesDeEfectivo;
import com.jaguarlabs.pabs.dialog.BanksDialog;
import com.jaguarlabs.pabs.models.ModelDeposit;
import com.jaguarlabs.pabs.models.ModelPayment;
import com.jaguarlabs.pabs.models.ModelPeriod;
import com.jaguarlabs.pabs.net.NetHelper;
import com.jaguarlabs.pabs.net.NetHelper.onWorkCompleteListener;
import com.jaguarlabs.pabs.net.NetService;
import com.jaguarlabs.pabs.rest.SpiceHelper;
import com.jaguarlabs.pabs.rest.models.ModelSemaforoRequest;
import com.jaguarlabs.pabs.rest.models.ModelSemaforoResponse;
import com.jaguarlabs.pabs.rest.request.SemaforoRequest;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.CerrarSesionCallback;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.LogRegister;
import com.jaguarlabs.pabs.util.OnFragmentResult;
import com.jaguarlabs.pabs.util.Util;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestProgress;

import org.joda.time.LocalDate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import pl.tajchert.nammu.Nammu;

import static com.jaguarlabs.pabs.util.BuildConfig.Branches.LEON;
import static com.jaguarlabs.pabs.util.BuildConfig.Branches.NAYARIT;

/**
 * This class is in charge of all deposits, either to a bank
 * or to office. if it's a to a bank, the deposit gets inserted into
 * database and send it to server wheneven the timer {@link Clientes#timerSendPaymentsEachTenMinutes}
 * runs again. if it's to office, shows a loading screen waiting for
 * Web Service to response with an status of accepted, then
 * {@link Cierre} activity appears.
 * <p>
 * NOTE*
 * when pressing back on device, sends to login or closes app,
 * see {@link #onKeyDown(int, KeyEvent)} commented section
 */
@SuppressWarnings("SpellCheckingInspection")
@SuppressLint("Wakelock")
public class Deposito extends Fragment implements CerrarSesionCallback,
        OnItemSelectedListener {

    private static Deposito mthis = null;
    private long timestamp = new Date().getTime();

    public static Deposito getMthis() {
        return mthis;
    }

    public static void setMthis(Deposito mthis) {
        Deposito.mthis = mthis;
    }

    private EmpresasSpinnerAdapter adapter;

    private boolean alertShowing;
    private JSONArray arrayBanks;
    private JSONArray arrayCobrosOffline;
    private int bancoPosicion;
    private boolean callSuperOnTouch = false;
    private boolean cobrosDescargados = false;
    private JSONObject datosCobrador;
    double efectivo;
    private String idStatus;
    private double montoDepositado;
    private PowerManager pm;
    private boolean requesterCanceled;
    private JSONObject saldo;
    private int status;
    private WakeLock wl;
    //String bancoGral="";
    //String numeroBanco="";

    private List<ModelPayment> offlinePayments;

    public static boolean depositosIsActive;

    private boolean depositAsIncidencias = false;

    public boolean goBack = true;

    private MainActivity mainActivity;
    private SpiceManager spiceManager;
    private SpiceHelper<ModelSemaforoResponse> spiceSendContractsFromSemaforo;

    private Spinner spinnerPeriodSelector;
    private TextView tv_fecha;

    private int requestCode = -1;
    private OnFragmentResult fragmentResult;

    public TextView tv_efectivo_deposito;
    public Spinner spinnerEmpresas;

    private BroadcastReceiver depositResultReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_deposito, container, false);

        tv_efectivo_deposito =(TextView) view.findViewById(R.id.tv_efectivo_deposito);
        tv_fecha=(TextView) view.findViewById(R.id.tv_fecha);
        mainActivity = (MainActivity) getActivity();
        spiceManager = mainActivity.getSpiceManager();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        requestCode = getArguments().getInt("requestCode", -1);

        ImageView back = view.findViewById(R.id.iv_back);
        FrameLayout fl_banco = view.findViewById(R.id.fl_banco);
        Button btn_cancelar = view.findViewById(R.id.btn_cancelar_pet);
        Button btn_depositar = view.findViewById(R.id.btn_depositar);
        spinnerEmpresas=(Spinner) view.findViewById(R.id.spinnerEmpresas);

        btn_cancelar.setOnClickListener(v -> cancelAnyRequest());

        tv_fecha.setEnabled(true);
        //tv_fecha.setAlpha(.5f);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        tv_fecha.setText(dateFormat.format(date));

        btn_depositar.setOnClickListener(v -> {
            if (view.findViewById(R.id.et_folio).isEnabled()) {
                // PostTask postTask = new PostTask(mthis,
                // ConstantsPabs.registerPagosOfflineMain);
                // postTask.execute();
                View view1 = mainActivity.getCurrentFocus();
                if (view1 != null) {
                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null)
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                crearPeticionDesbloqueoDeposito();
            }
        });

        fl_banco.setOnClickListener(v -> {
            if (view.findViewById(R.id.et_folio).isEnabled()) {
                reloadCompaniesSpinner();
                showSpinnerBancos();
            }
        });

        back.setOnClickListener(v -> {
            if (view.findViewById(R.id.iv_back).getVisibility() != View.GONE) {
                Bundle bundle = getArguments();
                if (bundle != null) {
                    if (bundle.containsKey("inicio_from_splash")) {
                        boolean inicioFromSplash = bundle
                                .getBoolean("inicio_from_splash");
                        if (inicioFromSplash) {
                            if (Clientes.getMthis() == null) {

                                try {
                                    datosCobrador = new JSONObject(bundle.getString("datos_cobrador"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Clientes clientes = new Clientes();
                                Bundle args = new Bundle();

								/*Intent intent = new Intent();
                                intent.setClass(ApplicationResourcesProvider.getContext(), Clientes.class);*/
                                args.putString("datos_cobrador",
                                        datosCobrador.toString());
                                args.putBoolean("descargar_info", true);
                                //startActivity(intent);
                                mthis = null;
                                //finish();
                                getFragmentManager().beginTransaction()
                                        .replace(R.id.root_layout, clientes, MainActivity.TAG_CLIENTES)
                                        .remove(this)
                                        .commitAllowingStateLoss();
                            }
                        } else {
                            mthis = null;
                            //finish();
                            getFragmentManager().popBackStack();
                        }

                    } else {
                        mthis = null;
                        //finish();
                        getFragmentManager().popBackStack();
                    }
                } else {
                    mthis = null;
                    //finish();
                    getFragmentManager().popBackStack();
                }
            }
        });


        spiceSendContractsFromSemaforo = new SpiceHelper<ModelSemaforoResponse>(spiceManager, "sendContractsFromSemaforo", ModelSemaforoResponse.class, DurationInMillis.ONE_HOUR) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                fail();
            }

            @Override
            protected void onSuccess(ModelSemaforoResponse result) {
                super.onSuccess(result);
                Util.Log.ih("spiceSendContractsFromSemaforo -> result = " + result.getResult());
                Util.Log.ih("spiceSendContractsFromSemaforo -> error = " + result.getError());
                dismissMyCustomDialog();
                try {
                    if (result.getResult() != null) {
                        enviarDepositoVoluntario();
                    } else {
                        fail();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    fail();
                }
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                super.onProgressUpdate(progress);
                progressUpdate(progress);
            }
        };

        //init

        depositosIsActive = true;

        Util.Log.ih("Deposito - setInternetAsReachable = " + ExtendedActivity.setInternetAsReachable);

        mainActivity.hasActiveInternetConnection(getContext(), mainActivity.mainLayout);

        enableTextBoxes();
        tv_fecha = (TextView) view.findViewById(R.id.tv_fecha);
        // findViewById(R.id.et_monto).setEnabled(false);

        mthis = this;

        ExtendedActivity.setEstoyLogin(false);
        pm = (PowerManager) getContext().getSystemService(
                Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "tag");
        wl.acquire();
        arrayBanks = null;
        status = 0;
        requesterCanceled = false;
        alertShowing = false;
        SharedPreferences preferences = getContext().getSharedPreferences("PABS_Deposito",
                Context.MODE_PRIVATE);
        Bundle bundle = getArguments();
        Spinner spinerEmpresas = (Spinner) view.findViewById(R.id.spinnerEmpresas);
        //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
        try {
            adapter = new EmpresasSpinnerAdapter(
                    DatabaseAssistant.getCompaniesForSpinner(depositAsIncidencias), getContext());
            spinerEmpresas.setAdapter(adapter);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        spinerEmpresas.setOnItemSelectedListener(this);
        if (bundle != null)
        {
            if (bundle.containsKey("iniciar_time_sesion")) {
                boolean iniciarTimeSesion = bundle
                        .getBoolean("iniciar_time_sesion");
                if (iniciarTimeSesion) {
                    callSuperOnTouch = true;
                    //iniciarHandlerInactividad();
                }
            }
            if (bundle.containsKey("datos_cobrador")) {
                try {

                    try {
                        datosCobrador = new JSONObject(bundle.getString("datos_cobrador"));
                    } catch (JSONException e) {
                        try {
                            datosCobrador = new JSONObject(preferences.getString("datos_cobrador", ""));
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                        e.printStackTrace();
                    }

					/*
					float efectivoTotal = 0;
					try {
						JSONArray array = query.mostrarEmpresas();
						for (int i = 0; i < array.length(); i++) {
						*/
							/*
							Log.e("efectivo de "
									+ array.getJSONObject(i).getString(
											Empresa.nombre_empresa),
									""
											+ getEfectivoPreference()
													.getFloat(
															array.getJSONObject(
																	i)
																	.getString(
																			Empresa.nombre_empresa),
															0));
							*/
					/*
							efectivoTotal += getEfectivoPreference().getFloat(
									array.getJSONObject(i).getString(
											Empresa.nombre_empresa), 0);
						}
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					((TextView) findViewById(R.id.tv_efectivo_deposito))
							.setText("$ " + formatMoney(efectivoTotal));
					*/
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (bundle.containsKey("isConnected")) {
                mainActivity.isConnected = bundle.getBoolean("isConnected");
                //mainActivity.toastL("Si hay internet");
                TSnackbar snackbar = TSnackbar.make(view, "Si hay internet", TSnackbar.LENGTH_SHORT);
                View snackView = snackbar.getView();
                TextView snackText = snackView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                snackText.setTextColor(Color.WHITE);

                snackbar.show();
            }

        } else if (preferences.contains("datos_cobrador")) {
            //findViewById(R.id.iv_back).setVisibility(View.GONE);

            try {
                datosCobrador = new JSONObject(preferences.getString(
                        "datos_cobrador", ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (preferences.contains("id_status_deposito")) {
                idStatus = preferences.getString("id_status_deposito", "");
            }

            if (preferences.contains("requesterCanceled")) {
                requesterCanceled = preferences.getBoolean("requesterCanceled",
                        false);
            }

            if (requesterCanceled) {
                requesterCanceled = false;
                verificarStatusDeposito();
                // disableTextBoxes();
            }

        }
        //try {
        if (DatabaseAssistant.getAllPaymentsNotSynced() != null) {
            //if (query.obtenerCobrosDesincronizados().getJSONArray("cobros")
            //			.length() == 0) {

            if (DatabaseAssistant.isThereAnyDepositNotSynced()) {
                enviarDepositos();
            }
            Log.e("entro", "entro");
            enableTextBoxes();
        }
        //} catch (JSONException e) {
        //	e.printStackTrace();
        //}
        JSONObject bancos;
        try {
            if (BuildConfig.getTargetBranch() == BuildConfig.Branches.TOLUCA)
            {
                if (datosCobrador.has("no_cobrador") && (datosCobrador.getInt("no_cobrador") == 47 || datosCobrador.getInt("no_cobrador") == 2786 || datosCobrador.getInt("no_cobrador") == 1900 || datosCobrador.getInt("no_cobrador") == 5416))
                {
                    bancos = new JSONObject(
                            "{\"result\":[{\"no_banco\":\"1\",\"nombre\":\"Bancomer\"},{\"no_banco\":\"2\",\"nombre\":\"Banamex\"},{\"no_banco\":\"3\",\"nombre\":\"IXE Banco\"},{\"no_banco\":\"4\",\"nombre\":\"Santander\"},{\"no_banco\":\"5\",\"nombre\":\"HSBC\"},{\"no_banco\":\"6\",\"nombre\":\"Scotia Bank\"},{\"no_banco\":\"7\",\"nombre\":\"Inverlat\"},{\"no_banco\":\"8\",\"nombre\":\"BanRegio\"},{\"no_banco\":\"9\",\"nombre\":\"Inbursa\"},{\"no_banco\":\"10\",\"nombre\":\"Banco del Baj\u00edo\"},{\"no_banco\":\"12\",\"nombre\":\"Banorte\"},{\"no_banco\":\"11\",\"nombre\":\"Oficina\"},{\"no_banco\":\"13\",\"nombre\":\"Incidencias\"},{\"no_banco\":\"17\",\"nombre\":\"OXXO\"},{\"no_banco\":\"21\",\"nombre\":\"Voucher\"},{\"no_banco\":\"24\",\"nombre\":\"No identificados\"},{\"no_banco\":\"20\",\"nombre\":\"Descuentos via nomina\"}]}");
                }
                else {
                    bancos = new JSONObject(
                            "{\"result\":[{\"no_banco\":\"1\",\"nombre\":\"Bancomer\"},{\"no_banco\":\"2\",\"nombre\":\"Banamex\"},{\"no_banco\":\"3\",\"nombre\":\"IXE Banco\"},{\"no_banco\":\"4\",\"nombre\":\"Santander\"},{\"no_banco\":\"5\",\"nombre\":\"HSBC\"},{\"no_banco\":\"6\",\"nombre\":\"Scotia Bank\"},{\"no_banco\":\"7\",\"nombre\":\"Inverlat\"},{\"no_banco\":\"8\",\"nombre\":\"BanRegio\"},{\"no_banco\":\"9\",\"nombre\":\"Inbursa\"},{\"no_banco\":\"10\",\"nombre\":\"Banco del Baj\u00edo\"},{\"no_banco\":\"12\",\"nombre\":\"Banorte\"},{\"no_banco\":\"11\",\"nombre\":\"Oficina\"},{\"no_banco\":\"13\",\"nombre\":\"Incidencias\"},{\"no_banco\":\"17\",\"nombre\":\"OXXO\"},{\"no_banco\":\"21\",\"nombre\":\"Voucher\"},{\"no_banco\":\"24\",\"nombre\":\"No identificados\"}]}");
                }

            }
            else {
                if (BuildConfig.getTargetBranch() == BuildConfig.Branches.NAYARIT || BuildConfig.getTargetBranch() == BuildConfig.Branches.MORELIA
                        || BuildConfig.getTargetBranch() == BuildConfig.Branches.MEXICALI || BuildConfig.getTargetBranch() == BuildConfig.Branches.QUERETARO
                        || BuildConfig.getTargetBranch() == BuildConfig.Branches.IRAPUATO || BuildConfig.getTargetBranch()== BuildConfig.Branches.SALAMANCA
                        || BuildConfig.getTargetBranch()== BuildConfig.Branches.TORREON || BuildConfig.getTargetBranch()== BuildConfig.Branches.CELAYA
                        || BuildConfig.getTargetBranch()== BuildConfig.Branches.LEON){
                    //---------------------------------------------------------------------------------------------
                    bancos = new JSONObject();
                    bancos.put("result", new JSONArray( new Gson().toJson(Banks.listAll(Banks.class))) );
                    Util.Log.ih(bancos.toString());

                } else {
                    bancos = new JSONObject(
                            "{\"result\":[{\"no_banco\":\"1\",\"nombre\":\"Bancomer\"},{\"no_banco\":\"2\",\"nombre\":\"Banamex\"},{\"no_banco\":\"3\",\"nombre\":\"IXE Banco\"},{\"no_banco\":\"4\",\"nombre\":\"Santander\"},{\"no_banco\":\"5\",\"nombre\":\"HSBC\"},{\"no_banco\":\"6\",\"nombre\":\"Scotia Bank\"},{\"no_banco\":\"7\",\"nombre\":\"Inverlat\"},{\"no_banco\":\"8\",\"nombre\":\"BanRegio\"},{\"no_banco\":\"9\",\"nombre\":\"Inbursa\"},{\"no_banco\":\"10\",\"nombre\":\"Banco del Baj\u00edo\"},{\"no_banco\":\"12\",\"nombre\":\"Banorte\"},{\"no_banco\":\"11\",\"nombre\":\"Oficina\"},{\"no_banco\":\"13\",\"nombre\":\"Incidencias\"}]}");
                }
            }

            manage_AllBanks(bancos);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //NEW PROCESS
        if (DatabaseAssistant.isThereMoreThanOnePeriodActive()) {
            Util.Log.dh("spinner");
            spinnerPeriodSelector = (Spinner) view.findViewById(R.id.spinnerPeriodsFromDeposits);
            LinearLayout linearLayoutForSpinner = (LinearLayout) view.findViewById(R.id.llSpinnerDeposits);

            linearLayoutForSpinner.setVisibility(View.VISIBLE);

            List<ModelPeriod> modelPeriodList = DatabaseAssistant.getAllPeriodsActive();

            String[] items = new String[modelPeriodList.size()];

            int i;
            for (i = 0; i < modelPeriodList.size(); i++)
            {
                items[i] = "" + modelPeriodList.get(i).getPeriodNumber();
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, items);
            //ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.planets_array, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerPeriodSelector.setAdapter(adapter);
            spinnerPeriodSelector.setOnItemSelectedListener(Deposito.this);
            spinnerPeriodSelector.setSelection(0);
        }

        if (datosCobrador != null) {
            try {
                if(BuildConfig.getTargetBranch() == BuildConfig.Branches.GUADALAJARA)
                {
                    tv_fecha.setOnClickListener(v -> {
                        LocalDate today = LocalDate.now();

                        DatePickerDialog dialog = new DatePickerDialog(getContext(), (v1, year, month, dayOfMonth) -> {
                            if (view.isShown()) {
                                LocalDate selectedDate = new LocalDate(year, month + 1, dayOfMonth);
                                tv_fecha.setText(selectedDate.toString());
                            }
                        }, today.getYear(), today.getMonthOfYear() - 1, today.getDayOfMonth());

                        dialog.show();
                    });
                    tv_fecha.setVisibility(View.VISIBLE);

                }
                else
                {
                    //if (datosCobrador.getString("no_cobrador").equals("35148"))
                    //{
                        tv_fecha.setOnClickListener(v -> {
                            LocalDate today = LocalDate.now();

                            DatePickerDialog dialog = new DatePickerDialog(getContext(), (v1, year, month, dayOfMonth) -> {
                                if (view.isShown()) {
                                    LocalDate selectedDate = new LocalDate(year, month + 1, dayOfMonth);
                                    tv_fecha.setText(selectedDate.toString());
                                }
                            }, today.getYear(), today.getMonthOfYear() - 1, today.getDayOfMonth());

                            dialog.show();
                        });

                        tv_fecha.setVisibility(View.VISIBLE);
                    //}
                }




            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    private void fail() {
        dismissMyCustomDialog();
        enableTextBoxes();
        //mainActivity.toastL(R.string.error_message_sending_contracts_from_semaforo);
        Snackbar.make(mainActivity.mainLayout, R.string.error_message_sending_contracts_from_semaforo, Snackbar.LENGTH_SHORT).show();
    }

    private void progressUpdate(RequestProgress progress) {
        Util.Log.ih("inside progressUpdate method");
        Util.Log.ih("(int) progress.getProgress() = " + (int) progress.getProgress());
        switch ((int) progress.getProgress()) {
            case ConstantsPabs.START_PRINTING:
                Util.Log.ih("case START_PRINTING");
                mainActivity.toastS(R.string.info_message_sending_contracts_from_semaforo);
                showMyCustomDialog();
                break;
            case ConstantsPabs.FINISHED_PRINTING:
                Util.Log.ih("case FINISHED_PRINTING");
                dismissMyCustomDialog();
                break;
            default:
                Util.Log.ih("default");
                //nothing
        }
    }

    /**
     * shows framelayout as progress dialog
     */
    private void showMyCustomDialog() {

        goBack = false;

        if (getView() != null) {
            final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
            flLoading.setVisibility(View.VISIBLE);
        }
    }

    /**
     * hides framelayout used as progress dialog
     */
    private void dismissMyCustomDialog() {

        goBack = true;

        if (getView() != null) {
            final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
            flLoading.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        //spiceManager.start(ApplicationResourcesProvider.getContext());

        //if (spiceSendContractsFromSemaforo.isRequestPending()){
        //	spiceSendContractsFromSemaforo.executePendingRequest();
        //}
    }

    @Override
    public void onStop() {
        //spiceManager.shouldStop();

        super.onStop();
    }

    /**
     * Sets all payments stored in arrayCobrosOffline variable which were just sent
     * to server as sync (sync <- 1) so that does not resend payments made
     * causing a lot of data usage.
     */
    public void sincronizarPagosRegistrados(JSONObject json) {
        try {
            //	SqlQueryAssistant assistant = new SqlQueryAssistant(this);
            //	for (int i = 0; i < arrayCobrosOffline.length(); i++) {
            //		JSONObject cobro = arrayCobrosOffline.getJSONObject(i);
            //		assistant.actualizarSincronizacion(cobro.getString("fecha"));
            //	}
            //	arrayCobrosOffline = new JSONArray();
            //for (ModelPayment p : offlinePayments) {
                DatabaseAssistant.updatePaymentsAsSynced(json);
            //}

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	/*
	@Override
	public void hasActiveInternetConnection(Context context, ImageView... asd) {
		Runnable run = new Runnable() {

			@Override
			public void run() {
				// set 'responded' to TRisConnectedUE if is able to connect with google
				// mobile (responds fast)
				new Thread() {
					@Override
					public void run() {
						HttpGet requestForTest = new HttpGet(
								"http://m.google.com");
						try {
							new DefaultHttpClient().execute(requestForTest); // can
																				// last...
							isConnected = true;
						} catch (Exception e) {
						}
					}
				}.start();

				try {
					int waited = 0;
					while (!isConnected && (waited < 1500)) {
						Thread.yield();
						Thread.sleep(50);
						if (!isConnected) {
							waited += 100;
						}
					}
				} catch (InterruptedException e) {
				} // do nothing

			}

		};
		new Thread(run).start();

	}
	*/

    /**
     * this method checks if folio and amount text fields are
     * filled with info and if a bank is selected
     *
     * @return true -> folio and/or amount are given and bank selected
     * false -> folio and/or amount are not given and bank not selected
     */
    private boolean camposDepositoLlenos() {
        boolean isValido = false;
        TextView banco = (TextView) getView().findViewById(R.id.tv_banco);
        EditText folio = (EditText) getView().findViewById(R.id.et_folio);
        EditText monto = (EditText) getView().findViewById(R.id.et_monto);

        if ((banco.getText().toString().length() > 0)
                && (folio.getText().toString().length() > 0)
                && (monto.getText().toString().length() > 0)) {

            if (tv_fecha.getVisibility() == View.VISIBLE) {
                return tv_fecha.length() > 0;
            }

            isValido = true;
        }
        return isValido;
    }

    /**
     * cancels a request made to office using a Web Service.
     * gets launched when cancel button on waiting screen is touched
     * calls {@link #changeStatusVoluntario()} to set canceled in server
     */
    private void cancelAnyRequest() {
        requesterCanceled = true;
        status = -1;
        SharedPreferences preferences = getContext().getSharedPreferences("PABS_Deposito",
                Context.MODE_PRIVATE);
        timestamp = new Date().getTime();
        Editor editor = preferences.edit();
        editor.putBoolean("requesterCanceled", false);
        editor.apply();

        if (getView() != null)
            getView().findViewById(R.id.rl_cancelar_peticion).setVisibility(View.GONE);
        if (arrayBanks == null) {
            JSONObject json;
            try {
                if (BuildConfig.getTargetBranch() == BuildConfig.Branches.TOLUCA)
                {
                    if (datosCobrador.has("no_cobrador") && (datosCobrador.getInt("no_cobrador") == 47 || datosCobrador.getInt("no_cobrador") == 2786 || datosCobrador.getInt("no_cobrador") == 1900))
                    {
                        json = new JSONObject(
                                "{\"result\":[{\"no_banco\":\"1\",\"nombre\":\"Bancomer\"},{\"no_banco\":\"2\",\"nombre\":\"Banamex\"},{\"no_banco\":\"3\",\"nombre\":\"IXE Banco\"},{\"no_banco\":\"4\",\"nombre\":\"Santander\"},{\"no_banco\":\"5\",\"nombre\":\"HSBC\"},{\"no_banco\":\"6\",\"nombre\":\"Scotia Bank\"},{\"no_banco\":\"7\",\"nombre\":\"Inverlat\"},{\"no_banco\":\"8\",\"nombre\":\"BanRegio\"},{\"no_banco\":\"9\",\"nombre\":\"Inbursa\"},{\"no_banco\":\"10\",\"nombre\":\"Banco del Baj\u00edo\"},{\"no_banco\":\"12\",\"nombre\":\"Banorte\"},{\"no_banco\":\"11\",\"nombre\":\"Oficina\"},{\"no_banco\":\"13\",\"nombre\":\"Incidencias\"},{\"no_banco\":\"17\",\"nombre\":\"OXXO\"},{\"no_banco\":\"21\",\"nombre\":\"Voucher\"},{\"no_banco\":\"24\",\"nombre\":\"No identificados\"},{\"no_banco\":\"20\",\"nombre\":\"Descuentos via nomina\"}]}");
                    }
                    else {
                        json = new JSONObject(
                                "{\"result\":[{\"no_banco\":\"1\",\"nombre\":\"Bancomer\"},{\"no_banco\":\"2\",\"nombre\":\"Banamex\"},{\"no_banco\":\"3\",\"nombre\":\"IXE Banco\"},{\"no_banco\":\"4\",\"nombre\":\"Santander\"},{\"no_banco\":\"5\",\"nombre\":\"HSBC\"},{\"no_banco\":\"6\",\"nombre\":\"Scotia Bank\"},{\"no_banco\":\"7\",\"nombre\":\"Inverlat\"},{\"no_banco\":\"8\",\"nombre\":\"BanRegio\"},{\"no_banco\":\"9\",\"nombre\":\"Inbursa\"},{\"no_banco\":\"10\",\"nombre\":\"Banco del Baj\u00edo\"},{\"no_banco\":\"12\",\"nombre\":\"Banorte\"},{\"no_banco\":\"11\",\"nombre\":\"Oficina\"},{\"no_banco\":\"13\",\"nombre\":\"Incidencias\"},{\"no_banco\":\"17\",\"nombre\":\"OXXO\"},{\"no_banco\":\"21\",\"nombre\":\"Voucher\"},{\"no_banco\":\"24\",\"nombre\":\"No identificados\"}]}");
                    }
                }
                else {
                    if (BuildConfig.getTargetBranch() == BuildConfig.Branches.NAYARIT || BuildConfig.getTargetBranch() == BuildConfig.Branches.MORELIA  || BuildConfig.getTargetBranch() == BuildConfig.Branches.MEXICALI || BuildConfig.getTargetBranch() == BuildConfig.Branches.QUERETARO || BuildConfig.getTargetBranch()== BuildConfig.Branches.TORREON
                            || BuildConfig.getTargetBranch() == BuildConfig.Branches.IRAPUATO || BuildConfig.getTargetBranch() == BuildConfig.Branches.SALAMANCA || BuildConfig.getTargetBranch() == BuildConfig.Branches.CELAYA
                            || BuildConfig.getTargetBranch() == BuildConfig.Branches.LEON) {
                        json = new JSONObject();
                        json.put("result", new JSONArray(new Gson().toJson(Banks.listAll(Banks.class))));
                        Util.Log.ih(json.toString());
                    } else {
                        json = new JSONObject(
                                "{\"result\":[{\"no_banco\":\"1\",\"nombre\":\"Bancomer\"},{\"no_banco\":\"2\",\"nombre\":\"Banamex\"},{\"no_banco\":\"3\",\"nombre\":\"IXE Banco\"},{\"no_banco\":\"4\",\"nombre\":\"Santander\"},{\"no_banco\":\"5\",\"nombre\":\"HSBC\"},{\"no_banco\":\"6\",\"nombre\":\"Scotia Bank\"},{\"no_banco\":\"7\",\"nombre\":\"Inverlat\"},{\"no_banco\":\"8\",\"nombre\":\"BanRegio\"},{\"no_banco\":\"9\",\"nombre\":\"Inbursa\"},{\"no_banco\":\"10\",\"nombre\":\"Banco del Baj\u00edo\"},{\"no_banco\":\"12\",\"nombre\":\"Banorte\"},{\"no_banco\":\"11\",\"nombre\":\"Oficina\"},{\"no_banco\":\"13\",\"nombre\":\"Incidencias\"}]}");

                    }
                }
                manage_AllBanks(json);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        enableTextBoxes();

        changeStatusVoluntario();

    }

    /**
     * sends to server status '3' which is
     * cancelled status
     * <p>
     * Web Service
     * 'http://50.62.56.182/ecobro/controldepositos/changeStatus'
     * <p>
     * JSONObject param
     * {
     * "id": >id_status>,
     * "status": 3
     * }
     */
    private void changeStatusVoluntario() {
        JSONObject json = new JSONObject();

        try {
            json.put("id", idStatus);
            json.put("status", "3");
        } catch (Exception e) {
            mainActivity.log("postException", "" + e);
        }
        requestChangeStatusDeposito(json);

        /*NetHelper.getInstance().ServiceExecuter(new NetService(ConstantsPabs.urlChangeStatusDeposito, json), getContext(), new onWorkCompleteListener()
        {
                    @Override
                    public void onError(Exception e) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onCompletion(String result) {
                        try {
                            manage_ChangeStatusDeposito(new JSONObject(result));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
        });*/
    }

    private void requestChangeStatusDeposito(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlChangeStatusDeposito, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                manage_ChangeStatusDeposito(response);
                Log.i("Request LOGIN-->", new Gson().toJson(response));
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("getNumNotificaciones", "Error al obtener numero de notificaciones");
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }

    @Override
    public void cerrarSesionTimeOut() {
        if (mainActivity.handlerSesion != null) {
            mainActivity.handlerSesion.removeCallbacks(mainActivity.myRunnable);
            mainActivity.handlerSesion = null;

            Log.e("removiendo handler", "sii");
        }
        mthis = null;
        //finish();
        getFragmentManager().popBackStack();
    }

    private void executeSendContractsFromSemaforo()
    {

        Thread thread = new Thread(() -> {
            try {
                int periodo = mainActivity.getEfectivoPreference().getInt("periodo", 0);

                ModelSemaforoRequest modelSemaforoRequest = new ModelSemaforoRequest();
                modelSemaforoRequest
                        .setSemaforoContracts(DatabaseAssistant.getContractsForSemaforoEcobro())
                        .setCollectorNumber(Integer.parseInt(datosCobrador.getString("no_cobrador")))
                        .setPeriodo(periodo);

                if (modelSemaforoRequest.getSemaforoContracts().size() > 0) {
                    SemaforoRequest semaforoRequest = new SemaforoRequest(modelSemaforoRequest);
                    spiceSendContractsFromSemaforo.executeRequest(semaforoRequest);

                } else {
                    Util.Log.ih("No hay contratos en semaforo para mandar al servidor");
                    enviarDepositoVoluntario();
                }
            } catch (JSONException ex) {
                Util.Log.eh("Error: No se pudo obtener el numero de cobrador");
            }
        });

        thread.start();
    }


    /**
     * sends to server deposit to office
     * <p>
     * deposit to office
     * <p>
     * Web Service
     * 'http://50.62.56.182/ecobro/controldepositos/registerDeposito'
     * <p>
     * JSONObject param
     * {
     * "periodo": ,
     * "empresa": "",
     * "no_cobrador": ,
     * "monto": ,
     * "no_banco": ,
     * "referencia":
     * }
     */
    private void enviarDepositoVoluntario()
    {

        try
        {
            String noCobrador = "";
            noCobrador = mainActivity.getEfectivoPreference().getString("no_cobrador", "0");
            JSONObject jsonBanco = arrayBanks.getJSONObject(bancoPosicion);
            String idBanco = jsonBanco.getString("no_banco");
            LogRegister.LogDeposito deposito = null;

            JSONObject json = new JSONObject();
            try
            {
                json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));

                if (((Spinner) getView().findViewById(R.id.spinnerEmpresas)).getSelectedItemPosition() == 0)
                {
                    json.put("empresa", "00");
                } else {
                    json.put("empresa", adapter.getEmpresaId(((Spinner) mainActivity.findViewById(R.id.spinnerEmpresas)).getSelectedItemPosition()));
                }
                json.put("no_cobrador", noCobrador);
                json.put("monto", ((EditText) mainActivity.findViewById(R.id.et_monto)).getText().toString().replace(",", ""));
                json.put("no_banco", idBanco); //Aqui manda el 11 de oficina
                json.put("referencia", ((EditText) mainActivity.findViewById(R.id.et_folio)).getText().toString());
                /**
                 * Creabdo objeto deposito para registrarlo en log si es que el server lo acepta, si el server lo acepta este será guardado en log y en base de datos
                 */
                deposito = new LogRegister.LogDeposito(
                        "" + mainActivity.getEfectivoPreference().getInt("periodo", 0),                                //Periodo
                        ((EditText) mainActivity.findViewById(R.id.et_monto)).getText().toString().replace(",", ""),      //Monto
                        adapter.getEmpresaId(((Spinner) getView().findViewById(R.id.spinnerEmpresas)).getSelectedItemPosition()),           //Empresa
                        noCobrador,                                                                                                         //Cobrador
                        jsonBanco.getString("nombre"),                                                                                //Banco
                        ((EditText) getView().findViewById(R.id.et_folio)).getText().toString(),                                            //Referencia
                        false                                                                                           //Ya fue enviado al server
                );
                deposito.esOffline = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String url;

            if (bancoPosicion >= 11)
            {
                json.put("timestamp", timestamp);
                url = ConstantsPabs.urlUnlockDepositoViejo;
            } else {
                url = ConstantsPabs.urlUnlockDeposito;
            }
            requestUnlockDeposito(json, url);
            montoDepositado = Double.parseDouble(((EditText) getView().findViewById(R.id.et_monto)).getText().toString().replace(",", ""));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void requestUnlockDeposito(JSONObject jsonParams, String url)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url, jsonParams, new Response.Listener<JSONObject>() {
            LogRegister.LogDeposito deposito;
            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    manage_DepositoVoluntario(response, deposito); //Aqui tenemos poner el guardado de depositos
                    Log.i("Request LOGIN-->", new Gson().toJson(response));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("getNumNotificaciones", "Error al obtener numero de notificaciones");
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }

    /**
     * validates that text fields are filled and is connected to internet, then
     * checks if total cash is greater than amount to be deposited, if so,
     * calls {@link #enviarDepositoVoluntario()}
     * <p>
     * deposit to office
     */
    private void crearPeticionDepositoClasico() {
        try {
            if (mainActivity.isConnected && getView() != null) {
                EditText monto = (EditText) getView().findViewById(R.id.et_monto);
                Spinner spin = (Spinner) getView().findViewById(R.id.spinnerEmpresas);
                double montoEditText = Double.parseDouble(monto.getText().toString().replace(",", ""));
                float efectivoTotal = 0;

                mainActivity.getEfectivoPreference().getInt("periodo", 0);
                List<Companies> companies = DatabaseAssistant.getCompanies();
                for (int i = 0; i < companies.size(); i++)
                {
                    efectivoTotal += DatabaseAssistant.getTotalCobrosMenosDepositosPorEmpresa(companies.get(i).getIdCompany());
                    mainActivity.getEfectivoPreference().getInt("periodo", 0);
                }

                if (efectivoTotal > montoEditText || getBancoPosicion() == 11) {


                    //TODO: UNCOMMENT WHEN AVAILABLE
                    //TODO: ********* NOTE ****************
                    //TODO: CHANGE URL IN SEMAFORO SERVICE
                    //TODO: INTERFACE
                    //TODO: *******************************
                    //send contracts from semaforo to ecobro
                    //then send deposit to office
                    if (BuildConfig.isWalletSynchronizationEnabled())
                        executeSendContractsFromSemaforo();
                    else
                    {
                        showMyCustomDialog();
                        enviarDepositoVoluntario();
                    }

                } else {
                    try {
                        mainActivity.showAlertDialog("", getString(R.string.error_message_deposit), true);
                        dismissMyCustomDialog();
                        enableTextBoxes();
                    } catch (Exception e) {
                        e.printStackTrace();
                        dismissMyCustomDialog();
                        enableTextBoxes();
                    }
                }

            } else {
                mainActivity.showAlertDialog("", getString(R.string.error_message_network), false);
                dismissMyCustomDialog();
                enableTextBoxes();
            }

        } catch (NumberFormatException e) {
            mainActivity.showAlertDialog("", "Ingresa una cantidad valida", true);
            dismissMyCustomDialog();
            enableTextBoxes();
        }

    }

    /**
     * checks what kind of deposit is trying to do.
     * If is a deposit to office and passes validations,
     * calls {@link #crearPeticionDepositoClasico()},
     * else, if is a deposit to a bank and passes validations,
     * calls {@link #depositoOffLine()}
     */
    private void crearPeticionDesbloqueoDeposito() {
        requesterCanceled = false;
        showMyCustomDialog();

        try {

            List<Companies> companies = DatabaseAssistant.getCompanies();

            if (camposDepositoLlenos()) {

                disableTextBoxes();

                if (bancoPosicion == 11)
                {
                    crearPeticionDepositoClasico();
                    return;
                }

                EditText monto = (EditText) getView().findViewById(R.id.et_monto);
                Spinner spin = (Spinner) getView().findViewById(R.id.spinnerEmpresas);
                float montoEditText = Float.parseFloat(monto.getText().toString().replace(",", ""));


                if (spin.getSelectedItemPosition() == 0 && getBancoPosicion() != 11) {
                    mainActivity.showAlertDialog("", "Ingresa los datos correctamente.", true);
                    enableTextBoxes();
                    dismissMyCustomDialog();
                    return;
                }

                /*
                 * deposit to a bank
                 *
                 * bank selected == 1
                 * and
                 * total cash from companies is greater or equal to amount to deposit
                 *///------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                //if (getBancoPosicion() == 11 || (mainActivity.getEfectivoPreference().getFloat(adapter.getEmpresaName(spin.getSelectedItemPosition()), 0) - DatabaseAssistant.getTotalCanceledByCompany(getIdCompany(adapter.getEmpresaName(spin.getSelectedItemPosition())))) >= montoEditText)
                if (getBancoPosicion() == 11 || (DatabaseAssistant.getTotalCobrosMenosDepositosPorEmpresa(adapter.getEmpresaId(spin.getSelectedItemPosition()))) >= montoEditText)
                {
                    if (montoEditText > 0 || getBancoPosicion() == 11)
                    {

                        if (getBancoPosicion() != 11)
                        {

                            try {
                                android.support.v7.app.AlertDialog.Builder confirmacion = new android.support.v7.app.AlertDialog.Builder(mainActivity);

                                confirmacion.setTitle("Aviso");
                                confirmacion.setMessage("Estás seguro que deseas enviar el siguiente deposito:\nBanco: " + arrayBanks.getJSONObject(bancoPosicion).getString("nombre") + "\nMonto: $" + montoEditText);
                                confirmacion.setCancelable(false);

                                confirmacion.setPositiveButton("Enviar deposito", (dialog, which) ->
                                {
                                    depositoOffLine();
                                });


                                confirmacion.setNegativeButton("Cancelar", (dialog, which) ->
                                {
                                    dialog.dismiss();
                                    dismissMyCustomDialog();
                                    enableTextBoxes();
                                });

                                confirmacion.show();
                            }
                            catch (JSONException ex)
                            {
                                ex.printStackTrace();
                            }
                        }
                        else
                        {
                            depositoOffLine();
                        }

                    } else {
                        mainActivity.showAlertDialog("", "Ingresa los datos correctamente.", true);
                        enableTextBoxes();
                        dismissMyCustomDialog();
                    }

                } else {
                    Util.Log.ih("first = " + mainActivity.getEfectivoPreference().getFloat(adapter.getEmpresaName(spin.getSelectedItemPosition()), 0) + "second" + (mainActivity.getEfectivoPreference().getFloat(adapter.getEmpresaName(spin.getSelectedItemPosition()), 0) - DatabaseAssistant.getTotalCanceledByCompany(getIdCompany(adapter.getEmpresaName(spin.getSelectedItemPosition())))));

                    mainActivity.showAlertDialog("", getString(R.string.error_message_deposit), true);
                    dismissMyCustomDialog();
                    enableTextBoxes();
                }

            } else {
                mainActivity.showAlertDialog("", getString(R.string.error_message_fields), true);
                dismissMyCustomDialog();
                enableTextBoxes();
            }
        } catch (NumberFormatException e) {
            mainActivity.showAlertDialog("", "Ingresa una cantidad valida", true);
            dismissMyCustomDialog();
            enableTextBoxes();
        }
    }

    /**
     * returns company id according to company name
     *
     * @param name company name
     * @return String with company id
     */
    private String getIdCompany(String name) {
        String id = "";
        switch (name) {
            case "Programa":
                id = "01";
                break;
            case "PBJ":
                id = "03";
                break;
            case "Cooperativa":
                id = "04";
                break;
            case "PABS LATINO":
                id = "05";
                break;
        }
        return id;
    }

    /**
     * inserts deposit to bank into database and send it to server

     * runs again.
     */
    public void depositoOffLine() {
        EditText monto = (EditText) getView().findViewById(R.id.et_monto);
        float montoEditText = Float.parseFloat(monto.getText().toString().replace(",", ""));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final Date date = new Date();
        String idCompany = adapter.getEmpresaId(((Spinner) getView().findViewById(R.id.spinnerEmpresas)).getSelectedItemPosition());
        try {
            if (datosCobrador.getString("no_cobrador").equals("35148")) {
                DatabaseAssistant.insertDeposit(
                        String.valueOf(montoEditText),
                        dateFormat.format(date),
                        tv_fecha.getText().toString(),
                        ((EditText) getView().findViewById(R.id.et_folio)).getText().toString(),
                        getBancoPosicion() + "",
                        "1",
                        adapter.getEmpresaId(((Spinner) getView().findViewById(R.id.spinnerEmpresas)).getSelectedItemPosition()),
                        mainActivity.getEfectivoPreference().getInt("periodo", 0)
                );
            } else {
                DatabaseAssistant.insertDeposit(
                        String.valueOf(montoEditText),
                        dateFormat.format(date),
                        tv_fecha.getText().toString(),
                        ((EditText) getView().findViewById(R.id.et_folio)).getText().toString(),
                        getBancoPosicion() + "",
                        "1",
                        adapter.getEmpresaId(((Spinner) getView().findViewById(R.id.spinnerEmpresas)).getSelectedItemPosition()),
                        mainActivity.getEfectivoPreference().getInt("periodo", 0)
                );
            }

            //double montoAActualizar = DatabaseAssistant.getTotalDepositosPorPeriodoYEmpresa(idCompany) - montoEditText;
            String queryActualizacion="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET cobrosMenosDepositos = cobrosMenosDepositos - "+ montoEditText +" WHERE periodo='"+mainActivity.getEfectivoPreference().getInt("periodo", 0)+"' and empresa='"+idCompany+"'";
            MontosTotalesDeEfectivo.executeQuery(queryActualizacion);

            //double montoDepositosParaActualizar = DatabaseAssistant.getTotalDepositsByCompany(idCompany) + montoEditText;
            String queryActualizacion2="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET depositos = depositos + "+ montoEditText +" WHERE periodo='"+mainActivity.getEfectivoPreference().getInt("periodo", 0)+"' and empresa='"+idCompany+"'";
            MontosTotalesDeEfectivo.executeQuery(queryActualizacion2);

            //Actualizar los demas campos, dentro de los Updates de cobros y depositos

        } catch (JSONException ex) {
            ex.printStackTrace();
        }


        /**
         * Registrando deposito offline que no se envía al server si no hasta que se llama al método enviarDepositos de la clase Clientes
         */
        LogRegister.LogDeposito deposito;
        try {
            deposito = new LogRegister.LogDeposito(
                    "" + mainActivity.getEfectivoPreference().getInt("periodo", 0),                                                //Periodo
                    String.valueOf(montoEditText),                                    //Monto
                    adapter.getEmpresaId(((Spinner) getView().findViewById(R.id.spinnerEmpresas)).getSelectedItemPosition()), //Empresa
                    mainActivity.getEfectivoPreference().getString("no_cobrador", ""),                                                                                    //Cobrador
                    arrayBanks.getJSONObject(bancoPosicion).getString("nombre"),                                                                    //Banco
                    ((EditText) getView().findViewById(R.id.et_folio)).getText().toString(),                                    //Referencia
                    false                                                                                            //Ya fue enviado al server
            );
            deposito.esOffline = true;
            LogRegister.registrarDeposito(deposito);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Editor edit = mainActivity.getEfectivoEditor();
        //Spinner spin = (Spinner) getView().findViewById(R.id.spinnerEmpresas);

        //double total = mainActivity.getEfectivoPreference().getFloat(adapter.getEmpresaName(spin.getSelectedItemPosition()), 0) - montoEditText;
        //edit.putFloat(adapter.getEmpresaName(spin.getSelectedItemPosition()), (float) total).apply();


        Toast.makeText(getContext(), "Depósito guardado", Toast.LENGTH_LONG).show();
        mthis = null;
        //finish();
        getFragmentManager().popBackStack();
    }

    /**
     * disables textboxes
     */
    private void disableTextBoxes() {
        if (getView() != null) {
            getView().findViewById(R.id.et_folio).setEnabled(false);
            getView().findViewById(R.id.et_monto).setEnabled(false);
            getView().findViewById(R.id.btn_depositar).setVisibility(View.GONE);
            getView().findViewById(R.id.iv_back).setEnabled(false);
            mainActivity.bandera=0;
        }
    }

    /**
     * disables textboxes
     */
    private void disableTextBoxesWhenOffice() {
        //findViewById(R.id.et_folio).setEnabled(false);
        getView().findViewById(R.id.et_monto).setEnabled(false);
        //findViewById(R.id.btn_depositar).setVisibility(View.GONE);
    }

    /**
     * enables textboxes
     */
    private void enableTextBoxes() {
        if (getView() != null) {
            getView().findViewById(R.id.et_folio).setEnabled(true);
            getView().findViewById(R.id.et_monto).setEnabled(true);
            getView().findViewById(R.id.btn_depositar).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.iv_back).setEnabled(true);
            mainActivity.bandera=1;
        }

    }

    /**
     * enables textboxes
     */
    private void enableTextBoxesWhenOffice() {
        getView().findViewById(R.id.et_folio).setEnabled(true);
        getView().findViewById(R.id.et_monto).setEnabled(true);
        getView().findViewById(R.id.btn_depositar).setVisibility(View.VISIBLE);

    }

    /**
     * returns position of bank selected
     *
     * @return Integer with postion of bank selected
     */
    public int getBancoPosicion()
    {
        try
        {
            switch (BuildConfig.getTargetBranch())
            {
                case NAYARIT:case MEXICALI:case QUERETARO:case IRAPUATO:case MORELIA:case SALAMANCA:case TORREON:case CELAYA: case LEON:
                return arrayBanks.getJSONObject(bancoPosicion).getInt("no_banco") == 99 ? 11 : arrayBanks.getJSONObject(bancoPosicion).getInt("no_banco");

                default:
                    return arrayBanks.getJSONObject(bancoPosicion).getInt("no_banco");
            }

            //return arrayBanks.getJSONObject(bancoPosicion).getInt("no_banco");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * start inactivity timer
     * timer which ends session after 20 minutes
     */
    public void llamarTimerInactividad() {
        //iniciarHandlerInactividad();
    }

    /**
     * sets 'arrayBanks' variable with all available banks,
     * banks that are shown in spinner to chose a bank to deposit to.
     *
     * @param json json with banks and position
     */
    public void manage_AllBanks(JSONObject json)
    {
        if (json.has("result"))
        {
            try
            {
                arrayBanks = json.getJSONArray("result");
                //Toast.makeText(mainActivity, arrayBanks.toString(), Toast.LENGTH_SHORT).show();
            } catch (JSONException e)
            {
                e.printStackTrace();
                mostrarMensajeError(ConstantsPabs.allBanksDepositoVol);
            }
        } else {
            mostrarMensajeError(ConstantsPabs.allBanksDepositoVol);
        }
    }

    /**
     * manages response from server when canceling deposit
     * request.
     * Modifies id_status_deposito in sharedPreference file.
     *
     * @param json response from server
     */
    public void manage_ChangeStatusDeposito(JSONObject json) {
        Util.Log.ih("manage_ChangeStatusDeposito = " + json);
        if (getView() != null) {
            EditText monto = (EditText) getView().findViewById(R.id.et_monto);
            Spinner spin = (Spinner) getView().findViewById(R.id.spinnerEmpresas);
            double montoEditText = Double.parseDouble(monto.getText().toString().replace(",", ""));
            if (json.has("result")) {
                //findViewById(R.id.iv_back).setVisibility(View.VISIBLE);
                SharedPreferences pref2 = getContext().getSharedPreferences("PABS_SPLASH",
                        Context.MODE_PRIVATE);
                Editor editor2 = pref2.edit();
                editor2.remove("id_status_deposito");
                editor2.apply();



                //Editor edit = mainActivity.getEfectivoEditor();



                //double total = mainActivity.getEfectivoPreference().getFloat(adapter.getEmpresaName(spin.getSelectedItemPosition()), 0) - montoEditText;

                //String queryActualizacion="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET cobrosMenosDepositos = cobrosMenosDepositos - "+ montoEditText +" WHERE periodo='"+
                //      mainActivity.getEfectivoPreference().getInt("periodo", 0)+ "' and empresa='"+adapter.getEmpresaName(spin.getSelectedItemPosition())+"'";
                //MontosTotalesDeEfectivo.executeQuery(queryActualizacion);


                //edit.putFloat(adapter.getEmpresaName(spin.getSelectedItemPosition()), (float) total).apply();
            } else {
                mostrarMensajeError(ConstantsPabs.changeStatusVoluntario);
            }
        }
    }

    /**
     * manages response from server when doing deposit to office
     * if it's successful, {@link #verificarStatusDeposito()} is called.
     *
     * @param json
     * @param deposito
     */
    public void manage_DepositoVoluntario(JSONObject json, LogRegister.LogDeposito deposito) {
        /**
         * Use enviarDepositos from itself instead of from Clientes class
         * because it may cause NullPointerException
         * Jordan Alvarez
         */

        enviarDepositos();

        Util.Log.ih("resultresultresultresult = " + json.toString());

        if (json.has("result")) {
            try {
                idStatus = json.getString("result");
                verificarStatusDeposito();

            } catch (JSONException e) {
                e.printStackTrace();
                enableTextBoxes();
            }
        } else {
            try {
                mainActivity.showAlertDialog("", json.getString("error"), true);
                dismissMyCustomDialog();
                enableTextBoxes();
            } catch (Exception e) {
                e.printStackTrace();
                enableTextBoxes();
            }
        }
        //}

    }

    /**
     * Sends to server all deposits made using Web Service
     * <p>
     * 'http://50.62.56.182/ecobro/controldepositos/registerDepositosOffline'
     * JSONObject param
     * {
     * "no_cobrador": "";
     * "depositos": {},
     * "periodo": ,
     * }
     */
    public void enviarDepositos() {
        //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
        //if (query.hayDepositos()) {
        if (DatabaseAssistant.isThereAnyDepositNotSynced()) {

            List<LogRegister.LogDeposito> logDepositos = null;
            JSONObject json = new JSONObject();

            try {
                //JSONArray depositos = query.mostrarTodosLosDepositosDesincronizados().getJSONArray("DEPOSITO");
                List<ModelDeposit> deposits = DatabaseAssistant.getAllDepositsNotSynced();
                if (deposits != null) {

                    json.put("no_cobrador", datosCobrador.getString("no_cobrador"));
                    json.put("depositos", new JSONArray(new Gson().toJson(deposits)));
                    json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
                    //Crear arraylist de depositos y registrarlos en log si el server responde
                    logDepositos = new ArrayList<>();

                    for (ModelDeposit d : deposits) {
                        LogRegister.LogDeposito logDeposito;

                        logDeposito = new LogRegister.LogDeposito(
                                "" + mainActivity.getEfectivoPreference().getInt("periodo", 0),//Periodo
                                d.getAmount(),//Monto
                                d.getCompany(), //Empresa
                                mainActivity.getEfectivoPreference().getString("no_cobrador", ""),                                                                                    //Cobrador
                                d.getNumberBank(),//Banco
                                d.getReference(),//Referencia
                                true//Ya fue enviado al server
                        );
                        logDepositos.add(logDeposito);
                    }
                    Util.Log.ih("depositos json = " + json.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            requestEnviarDepositos(json);


            /*NetService service = new NetService(ConstantsPabs.urlRegisterDepositosOffline, json);
            NetHelper.getInstance().ServiceExecuter(service, getContext(), new onWorkCompleteListener() {

                        List<LogRegister.LogDeposito> logDepositos;

                        @Override
                        public void onCompletion(String result) {
                            try {
                                manageRegisterDepositosOffline(new JSONObject(result), logDepositos);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Exception e) {
                            // TODO Auto-generated method stub

                        }

                        public onWorkCompleteListener setData(List<LogRegister.LogDeposito> logDepositos) {
                            this.logDepositos = logDepositos;
                            return this;
                        }
                    }.setData(logDepositos)
            );*/
        }
    }
    private void requestEnviarDepositos(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlRegisterDepositosOffline,
                jsonParams, new Response.Listener<JSONObject>() {
            List<LogRegister.LogDeposito> logDepositos;

            @Override
            public void onResponse(JSONObject response)
            {
                try {
                    manageRegisterDepositosOffline(response, logDepositos);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("Request LOGIN-->", new Gson().toJson(response));
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("getNumNotificaciones", "Error al obtener numero de notificaciones");
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }

    /**
     * Manages response of web service
     * if failed to send deposits, it will try to send them again,
     *
     * @param json         JSONObject with response of web service
     * @param logDepositos List<LogRegister.LogDeposito> instance. deprecated in 3.0
     */
    public void manageRegisterDepositosOffline(JSONObject json, List<LogRegister.LogDeposito> logDepositos) {
        if (json.has("result")) {
            Log.e("result", json.toString());
            try {
                JSONObject result = json.getJSONObject("result");
                JSONArray almacenados = result.getJSONArray("almacenados");
                if (almacenados.length() > 0) {
                    syncDepositsSavedToServer(almacenados);
                }
                JSONArray repetidos = result.getJSONArray("repetidos");
                Util.Log.ih("repetidos = " + repetidos.toString());
                if (repetidos.length() > 0) {
                    syncDepositsSavedToServer(repetidos);
                }
                JSONArray noAlmacenados = result.getJSONArray("no_almacenados");
                if (noAlmacenados.length() > 0) {
                    Toast.makeText(getContext(), "Enviando depositos bancarios", Toast.LENGTH_LONG).show();
                    enviarDepositos();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Sets all deposits that were just sent
     * to server as sync (sync <- 1) so that does not resend deposits made
     * causing data usage.
     */
    public void syncDepositsSavedToServer(JSONArray almacenados) {
        try {
            //SqlQueryAssistant assistant = new SqlQueryAssistant(this);
            for (int i = 0; i < almacenados.length(); i++) {
                JSONObject deposito = almacenados.getJSONObject(i);
                //assistant.updateDepositsSync(deposito.getString("fecha"));
                DatabaseAssistant.updateDepositsAsSynced(deposito.getString("fecha"));
            }
            //assistant = null;
            //arrayCobrosOffline = new JSONArray();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //int loops = 0;

    /**
     * manages response from server when trying to do deposit
     * to office
     *
     * @param json response from server
     */
    public void manage_GetStatusDeposito(JSONObject json) {
        Util.Log.ih("manage_GetStatusDeposito -> json = " + json.toString());
        if (status != -1 && !requesterCanceled) {
            //findViewById(R.id.iv_back).setVisibility(View.GONE);
            /*
             * status == 0
             * means that an action hasn't been performed
             * - not accepted
             * - not canceled
             * - not rejected
             */
            try {
                //loops ++;
                status = 0;
                status = json.getJSONObject("result").getInt("status");

                //TODO: delete next line after testing
				/*
				if (loops == 5) {
					status = 1;
				}
				*/


                Util.Log.ih("manage_GetStatusDeposito -> status = " + status);
            } catch (JSONException e) {
                status = 0;
                e.printStackTrace();
            }
            // status = 2;

            /*
             * status has changed its value because of
             * accepted
             * or
             * rejected
             */
            if (status == 1 || status == 2) {
                //enableTextBoxes();
                mainActivity.findViewById(R.id.rl_cancelar_peticion)
                        .setVisibility(View.GONE);
                //findViewById(R.id.iv_back).setVisibility(View.VISIBLE);
                //accepted
                if (status == 1) {

                    if (Clientes.getMthis() != null) {
                        try {

                            Clientes.getMthis().agregarEfectivoDatosCobrador(
                                    json.getJSONObject("result").getString(
                                            "efectivo"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //SqlQueryAssistant query = new SqlQueryAssistant(
                    //		ApplicationResourcesProvider.getContext());
                    // /
                    // /ELEIMINADOR

                    //always true
                    if ( true ) { //getBancoPosicion() == 11

                        try {
                            JSONObject result = json.getJSONObject("result");
                            SharedPreferences preferences = getContext().getSharedPreferences("PABS_Login",
                                    Context.MODE_PRIVATE);
                            Editor editor = preferences.edit();

                            editor.putInt("statusBloqueo", result.getInt("statusBloqueo"));

                            editor.apply();

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                        //NEW PROCESS
                        if (DatabaseAssistant.isThereMoreThanOnePeriodActive()) {
                            List<ModelPeriod> periods = DatabaseAssistant.getAllPeriodsActive();
                            Collections.sort(periods, (o1, o2) -> o2.getPeriodNumber() - o1.getPeriodNumber());

                            int newPeriod = periods.get(0).getPeriodNumber();
                            DatabaseAssistant.finishPeriod(mainActivity.getEfectivoPreference().getInt("periodo", 0));

                            List<ModelPeriod> modelPeriodList = DatabaseAssistant.getAllPeriodsActive();

                            String[] items = new String[modelPeriodList.size()];

                            int i;
                            for (i = 0; i < modelPeriodList.size(); i++) {
                                items[i] = "" + modelPeriodList.get(i).getPeriodNumber();
                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, items);
                            //ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.planets_array, android.R.layout.simple_spinner_item);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerPeriodSelector.setAdapter(adapter);
                            spinnerPeriodSelector.setOnItemSelectedListener(Deposito.this);
                            spinnerPeriodSelector.setSelection(0);

                            Toast toast = Toast.makeText(getContext(), "Periodo pendiente aceptado", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                            try {
                                SharedPreferences.Editor efectivoEditor = mainActivity.getEfectivoEditor();

                                List<CompanyAmounts> companyAmounts = DatabaseAssistant.getCompanyAmountWhenMoreThanOnePeriodIsActive(
                                        newPeriod
                                );
                                mainActivity.getEfectivoEditor().putInt("periodo", newPeriod).apply();
                                for (CompanyAmounts ca : companyAmounts) {
                                    efectivoEditor.putFloat(ca.getCompanyName(), ca.getAmount()).apply();
                                }

                                onResume();

                                Util.Log.ih("onItemSelected end");

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            int periodo = mainActivity.getEfectivoPreference().getInt("periodo",
                                    0);
                            String noTemp = mainActivity.getEfectivoPreference().getString(
                                    "no_cobrador", "");
                            mainActivity.getEfectivoEditor().clear().apply();
                            mainActivity.getEfectivoEditor().putString("no_cobrador", noTemp)
                                    .apply();
                            try {
                                // if (json.getJSONObject("result").has("periodo"))
                                // {
                                mainActivity.getEfectivoEditor().putInt("periodo", periodo)
                                        .apply();

                                // }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            int periodoAux = mainActivity.getEfectivoPreference().getInt("periodo", 0);

                            DatabaseAssistant.finishPeriod(periodoAux);
                            DatabaseAssistant.insertNewPeriodIfNeeded(periodoAux + 1, true);

                            /**
                             * Updates periodo
                             * just in case server didn't do it
                             */
                            mainActivity.getEfectivoEditor().putInt("periodo", (periodoAux + 1)).apply();
                            Util.Log.ih("DEPOSITO periodo cambio a = " + mainActivity.getEfectivoPreference().getInt("periodo", 0));

                            /**
                             * user_login is needed after doing "cierre de periodo"
                             * since the same user should log in.
                             * Jordan Alvarez
                             */
							/*
							SharedPreferences preferences = getSharedPreferences("PABS_Login", Context.MODE_PRIVATE);
							String userLogin = preferences.getString("user_login", "");
							preferences.edit().remove("fecha_sesion").clear().putString("user_login", userLogin).apply();
							//preferences.edit().remove("fecha_sesion").clear().apply();
							*/

                            /*Cierre cierre = new Cierre();
                            Bundle args = new Bundle();

                            args.putBoolean("isConnected", mainActivity.isConnected);

                            cierre.setArguments(args);*/

							/*Intent intent = new Intent(ApplicationResourcesProvider.getContext(), Cierre.class);
							intent.putExtra("isConnected", mainActivity.isConnected);
							startActivity(intent);
							finish();*/

                            /*getFragmentManager().beginTransaction()
                                    .add(R.id.root_layout, cierre, MainActivity.TAG_CIERRE)
                                    .remove(this)
                                    .commitAllowingStateLoss();*/







                            Cierre cierre = new Cierre();
                            Bundle args = new Bundle();

                            args.putBoolean("isConnected", mainActivity.isConnected);

                            //---------------------------------------------------------------------------------------

                            /**
                             * Se valido la minimizacion de la aplicacion y dejamos el proceso de request en segundo plano
                             * se valido en caso de tener argumentos en los extras.
                             * Azael Jimenez
                             */

                            if(args==null)
                            {

                                Toast.makeText(mainActivity, "Sin Argumentos en Back", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                cierre.setArguments(args);
							/*Intent intent = new Intent(ApplicationResourcesProvider.getContext(), Cierre.class);
							intent.putExtra("isConnected", mainActivity.isConnected);
							startActivity(intent);
							finish();*/

                                getFragmentManager().beginTransaction()
                                        .add(R.id.root_layout, cierre, MainActivity.TAG_CIERRE)
                                        .remove(this)
                                        .commitAllowingStateLoss();
                            }
                            //-------------------------------------------------------------------------------------------







                        }

                    }
                    /*
                     * never gets done--------------------------------------------------------------
                     * TODO: REMOVE CODE - JORDAN ALVAREZ
                     */

                    // /ELIMINADOR
					/*
					Spinner spin = (Spinner) findViewById(R.id.spinnerEmpresas);

					EditText monto = (EditText) findViewById(R.id.et_monto);
					float montoEditText = Float.parseFloat(monto.getText()
							.toString().replace(",", ""));
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					final Date date = new Date();
					DecimalFormat df = new DecimalFormat();
					df.setMaximumFractionDigits(2);
					df.setGroupingUsed(false);
					*/

					/*
					DatabaseAssistant.insertDeposit(
							String.valueOf(montoDepositado),
							dateFormat.format(date),
							((EditText) findViewById(R.id.et_folio)).getText().toString(),
							adapter.getEmpresaId(((Spinner) findViewById(R.id.spinnerEmpresas)).getSelectedItemPosition()),
							"1",
							adapter.getEmpresaId(((Spinner) findViewById(R.id.spinnerEmpresas)).getSelectedItemPosition()),
							getEfectivoPreference().getInt("periodo", 0)
					);
					*/

					/*
					query.insertarDeposito(
							df.format(montoDepositado),
							dateFormat.format(date),
							((EditText) findViewById(R.id.et_folio)).getText()
									.toString(),
							((Spinner) findViewById(R.id.spinnerEmpresas))
									.getSelectedItemPosition() + "",
							"1",
							adapter.getEmpresaId(((Spinner) findViewById(R.id.spinnerEmpresas))
									.getSelectedItemPosition()));
					*/
					/*
					Editor edit = getEfectivoEditor();
					float total = getEfectivoPreference().getFloat(
							adapter.getEmpresaName(spin
									.getSelectedItemPosition()), 0)
							- montoEditText;
					*/
					/*
					edit.putFloat(
							adapter.getEmpresaName(spin
									.getSelectedItemPosition()), (float) total)
							.apply();
					*/

					/*
                    Log.e("DEPOSITO", "" + montoDepositado);
                    Toast.makeText(getContext(), "Depósito realizado.",
                            Toast.LENGTH_LONG).show();
                    SharedPreferences pref2 = getContext().getSharedPreferences(
                            "PABS_SPLASH", Context.MODE_PRIVATE);
                    Editor editor2 = pref2.edit();
                    editor2.remove("id_status_deposito");
                    editor2.apply();
                    try {
                        Splash.mthis.finish();
                    } catch (Exception e) {

                    }
                    Intent intent = new Intent();
                    if (Clientes.getMthis() == null) {
                        try {
                            datosCobrador.put(
                                    "efectivo",
                                    json.getJSONObject("result").getString(
                                            "efectivo"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Clientes clientes = new Clientes();

                        intent.putExtra("datos_cobrador",
                                datosCobrador.toString());
                        intent.putExtra("descargar_info", true);

                        clientes.setArguments(intent.getExtras());

                        mthis = null;
                        //finish();
                        getFragmentManager().beginTransaction()
                                .replace(R.id.root_layout, clientes, MainActivity.TAG_CLIENTES)
                                .remove(this)
                                .commitAllowingStateLoss();
                    } else {
                        try {
                            intent.putExtra(
                                    "nuevo_efectivo",
                                    json.getJSONObject("result").getString(
                                            "efectivo"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //setResult(RESULT_OK, intent);
                        fragmentResult.onFragmentResult(requestCode, Activity.RESULT_OK, intent);
                        mthis = null;
                        //finish();
                        //if (MainActivity.CURRENT_TAG.equals(MainActivity.TAG_DEPOSITO))
                        //    getFragmentManager().popBackStack();
                    }
                    */

                    //------------------------------------------------------------------------------
                    /**
                     * TODO: END REMOVE CODE - JORDAN ALVAREZ
                     */

                }
                //rejected
                else if (status == 2) {
                    Toast.makeText(ApplicationResourcesProvider.getContext(), "Depósito rechazado.",
                            Toast.LENGTH_LONG).show();
                    SharedPreferences pref2 = getContext().getSharedPreferences(
                            "PABS_SPLASH", Context.MODE_PRIVATE);
                    Editor editor2 = pref2.edit();
                    editor2.remove("id_status_deposito");
                    editor2.apply();
                    if (Splash.mthis != null)
                        Splash.mthis.finish();
                    timestamp = new Date().getTime();
                    enableTextBoxes();
                }
                status = -1;
                SharedPreferences preferences = getContext().getSharedPreferences(
                        "PABS_Deposito", Context.MODE_PRIVATE);
                Editor editor = preferences.edit();
                editor.remove("id_status_deposito");
                editor.remove("datos_cobrador");
                editor.remove("monto_depositado");
                editor.putBoolean("requesterCanceled", false);
                editor.apply();
            }
        }

    }

    /**
     * Manages response of web service
     * if failed to send payments, it will try to send them again,
     * if not, will set them as sync
     *
     * @param json JSONObject with response of web service
     */
    public void manage_RegisterPagosOffiline(JSONObject json) {
        Log.e("result", json.toString());

        if (json.has("result")) {
            try {
                JSONArray arrayPagosFallidos = json.getJSONArray("result");
                if (arrayPagosFallidos.length() > 0) {
                    mainActivity.runOnUiThread(this::registerPagos);

                } else {
                    sincronizarPagosRegistrados(json);
                    mainActivity.runOnUiThread(this::enableTextBoxes);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * returns an instance of Dialog
     *
     * @return instance of Dialog
     */
    private Dialog getDialog() {
        return new Dialog(getContext());
    }

    /**
     * returns an instance of AlertDialogBuilder
     *
     * @return instance of AlertDialogBuilder
     */
    private AlertDialog.Builder getAlertDialogBuilder() {
        return new AlertDialog.Builder(getContext());
    }

    /**
     * returns an instance of ProgressDialog
     *
     * @return instance of ProgressDialog
     */
    private ProgressDialog getProgressDialog() {
        return new ProgressDialog(getContext());
    }

    /**
     * shows error message
     *
     * @param caso constant when failed
     */
    public void mostrarMensajeError(final int caso) {
        alertShowing = true;
        AlertDialog.Builder alert = getAlertDialogBuilder();
        alert.setMessage("Hubo un error de comunicación con el servidor, verifique sus ajustes de red o intente más tarde.");
        alert.setPositiveButton("Salir", (dialog, which) -> {
            View refMain = null;
            if (Clientes.getMthis() != null) {
                /*refMain = Clientes.getMthis().getView().findViewById(
                        R.id.listView_clientes);*/
                if (Clientes.getMthis().getView() != null)
                    refMain = Clientes.getMthis().getView().findViewById(
                            R.id.rv_clientes);
            }

            View refLogin = null;
            if (Login.getMthis() != null) {
                refLogin = Login.getMthis().findViewById(
                        R.id.editTextUser);
            }

            View refSplash = null;
            if (Splash.mthis != null) {
                refSplash = Splash.mthis
                        .findViewById(R.id.linear_splash);
            }

            if (refMain != null) {
                mthis = null;
                //finish();
                if (MainActivity.CURRENT_TAG.equals(MainActivity.TAG_DEPOSITO) && getFragmentManager() != null)
                    getFragmentManager().popBackStack();
            } else if (refLogin != null) {
                Bundle bundle = getArguments();
                if (bundle != null) {
                    if (bundle.containsKey("iniciar_time_sesion")) {
                        if (mainActivity.handlerSesion != null) {
                            mainActivity.handlerSesion
                                    .removeCallbacks(mainActivity.myRunnable);
                            mainActivity.handlerSesion = null;
                        }
                    }
                }
                mthis = null;
                //finish();
                if (MainActivity.CURRENT_TAG.equals(MainActivity.TAG_DEPOSITO) && getFragmentManager() != null)
                    getFragmentManager().popBackStack();
            } else if (refSplash != null) {
                Bundle bundle = getArguments();
                if (bundle != null) {
                    if (bundle.containsKey("iniciar_time_sesion")) {
                        if (mainActivity.handlerSesion != null) {
                            mainActivity.handlerSesion
                                    .removeCallbacks(mainActivity.myRunnable);
                            mainActivity.handlerSesion = null;
                        }
                    }
                }
                Splash.mthis.finish();
                mthis = null;
                //finish();
                if (MainActivity.CURRENT_TAG.equals(MainActivity.TAG_DEPOSITO) && getFragmentManager() != null)
                    getFragmentManager().popBackStack();
            }
        });
        alert.setNegativeButton("Reintentar", (dialog, which) -> {
            alertShowing = false;
            if (!mainActivity.isConnected) {
                mostrarMensajeError(caso);
            }
        });
        alert.create();
        alert.setCancelable(false);
        alert.show();
    }

    /**
     * manages clicks from screen of resources as
     * <p>
     * - iv_back ('arrow' image on left-top)
     * finishes activity
     * <p>
     * - fl_banco (where banks are selected)
     * shows list of banks to deposit to
     * <p>
     * - btn_efectuar_cierre (button 'print')
     * calls {@li
     * <p>
     * - btn_cancelar_pet (button 'cancel')
     * cancels office deposito request
     *
     * @param view of resources
     */

    @Override
    public void onDestroy() {
        super.onDestroy();

        depositosIsActive = false;

        mthis = null;

        wl.release();
    }

    /**
     * deprecated in 3.0
     *
     * @param adapterView adapter
     * @param view        view
     * @param pos         pos
     * @param arg3        arg3
     */
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long arg3)
    {

        Util.Log.ih("onItemSelected start");
        Util.Log.ih("pos = " + pos);
        try {
            SharedPreferences.Editor efectivoEditor = mainActivity.getEfectivoEditor();

            List<CompanyAmounts> companyAmounts = DatabaseAssistant.getCompanyAmountWhenMoreThanOnePeriodIsActive(
                    Integer.parseInt((String) adapterView.getItemAtPosition(pos))
            );
            mainActivity.getEfectivoEditor().putInt(
                    "periodo",
                    Integer.parseInt((String) adapterView.getItemAtPosition(pos))
            ).apply();
            for (CompanyAmounts ca : companyAmounts) {
                efectivoEditor.putFloat(ca.getCompanyName(), ca.getAmount()).apply();
            }

            onResume();

            Util.Log.ih("onItemSelected end");

        } catch (Exception e) {
            e.printStackTrace();
        }

        List<ModelPeriod> periodos = DatabaseAssistant.getAllPeriodsActive();

        try {
            if (Integer.parseInt((String) adapterView.getItemAtPosition(pos)) == periodos.get(periodos.size() - 1).getPeriodNumber()) {
                if (getView() != null) {
                    FrameLayout flDisableButtons = (FrameLayout) getView().findViewById(R.id.flDisableButtons);
                    flDisableButtons.setVisibility(View.VISIBLE);
                }
            } else {
                if (getView() != null) {
                    FrameLayout flDisableButtons = (FrameLayout) getView().findViewById(R.id.flDisableButtons);
                    flDisableButtons.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		//when true
		//an internet request was sent to web server
		if (goBack) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				SharedPreferences preferencesSplash = getContext().getSharedPreferences(
						"PABS_SPLASH", Context.MODE_PRIVATE);
				if (preferencesSplash.contains("id_status_deposito")) {
					moveTaskToBack(true);
					/**
					 * ---When click back on device closes app---
					 *
					 * want to go back to {@link Clientes} ?
					 * uncomment following code and comment
					 * moveTaskToBack(true);
					 * on previoues lines
					 */
				/*
				if (Clientes.getMthis() == null) {
					try {
						datosCobrador = new JSONObject(bundle.getString("datos_cobrador"));
					} catch (JSONException e){
						e.printStackTrace();
					}

					Intent intent = new Intent();
					intent.setClass(ApplicationResourcesProvider.getContext(), Clientes.class);
					intent.putExtra("datos_cobrador",
							datosCobrador.toString());
					intent.putExtra("descargar_info", true);
					startActivity(intent);
					mthis = null;
					finish();

				} else {
					mthis = null;
				}
			}
			return super.onKeyDown(keyCode, event);
		}
		return false;
	}*/

    /**
     * @param arg0
     */
    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    /**
     * sends to server all payments made that are not sync yet
     * <p>
     * Web Service
     * 'http://50.62.56.182/ecobro/controlpagos/registerPagos'
     * JSONObject param
     * {
     * "no_cobrador": "",
     * "pagos": {}
     * "periodo":
     * }
     */
    private void registerPagos() {
        JSONObject json = new JSONObject();

        try {
            Util.Log.ih("registrando cobros");
            json.put("no_cobrador",
                    mainActivity.getEfectivoPreference().getString("no_cobrador", ""));
            json.put("pagos", new JSONArray(new Gson().toJson(offlinePayments)));
            json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));

            try {

                TelephonyManager manager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
                String deviceid = null;
                if (Nammu.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        deviceid = manager.getImei();
                    else
                        deviceid = manager.getDeviceId();
                }
                json.put("imei", deviceid);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.e("cobros", offlinePayments.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mainActivity.isConnected)
        {
            requestRegisterPagosOfflineMain(json);


            /*NetHelper.getInstance().ServiceExecuter(new NetService(ConstantsPabs.urlregisterPagosOfflineMain, json), getContext(), new onWorkCompleteListener() {
                @Override
                public void onError(Exception e) {
                    dismissMyCustomDialog();
                    enableTextBoxes();
                }

                @Override
                public void onCompletion(String result) {
                    try {
                        manage_RegisterPagosOffiline(new JSONObject(
                                result));
                    } catch (JSONException e) {
                        dismissMyCustomDialog();
                        enableTextBoxes();
                        e.printStackTrace();
                    }
                }
            });*/
        }
    }

    private void requestRegisterPagosOfflineMain(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlregisterPagosOfflineMain, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                manage_RegisterPagosOffiline(response);
                Log.i("Request LOGIN-->", new Gson().toJson(response));
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        dismissMyCustomDialog();
                        enableTextBoxes();
                        Log.e("getNumNotificaciones", "Error al obtener numero de notificaciones");
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }

    @Override
    public void onResume() {
        super.onResume();

        MainActivity.CURRENT_TAG = MainActivity.TAG_DEPOSITO;

        if (mainActivity.isConnected) {
            mthis = this;
            //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
            //try {
            //JSONObject json = query.obtenerCobrosDesincronizados();
            offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();
            if (offlinePayments != null) {

                //if (json.getJSONArray("cobros").length() > 0) {
                //disableTextBoxes();
                //arrayCobrosOffline = json.getJSONArray("cobros");
                if (offlinePayments.size() > 0) {
                    if (mainActivity.isConnected) {
                        Util.Log.ih("se van a registrar cobros");
                        registerPagos();

                    }
                }
            }
            //} catch (JSONException e) {
            //	e.printStackTrace();
            //	enableTextBoxes();
            //}
        }
        // disableTextBoxes();
        mthis = this;
        paintSaldosOffline();
        //checkPayments();

    }

	/*@Override
	public void onUserInteraction() {
		if (callSuperOnTouch) {
			super.onUserInteraction();
		} else {
			View refMain = null;
			if (Clientes.getMthis() != null) {
				refMain = Clientes.getMthis().getView().findViewById(
						R.id.listView_clientes);
			}
			if (refMain != null) {
				try {
					if(Clientes.getMthis() != null){
						Clientes.getMthis().llamarTimerInactividad();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}*/

    /**
     * shows total cash of each company below companies selector
     */
    public void paintSaldosOffline() {
        //try {
        //saldo = new JSONObject();
        if (getView() != null) {
            LinearLayout container = getView().findViewById(R.id.saldo_container);

            container.removeAllViews();
            //SqlQueryAssistant query = new SqlQueryAssistant(
            //		ApplicationResourcesProvider.getContext());
            //JSONArray array = query.mostrarEmpresas();
            List<Companies> companies = DatabaseAssistant.getCompanies();

            int size;
            switch (BuildConfig.getTargetBranch()) {
                case GUADALAJARA: case TAMPICO:
                    size = companies.size();
                    break;
                case TOLUCA:
                    size = 4;
                    break;
                case QUERETARO:
                    size = 4;
                    break;
                case IRAPUATO:case SALAMANCA: case CELAYA: case LEON:
                    size = 2;
                    break;
                case PUEBLA:case CUERNAVACA:
                    size = 4;
                    break;
                case MERIDA:
                    size = 4;
                    break;
                case SALTILLO:
                    size = 4;
                    break;
                case CANCUN:
                    size = 4;
                    break;
                case NAYARIT:
                    size = 3;
                    break;
                case MEXICALI: case TORREON:
                    size = 3;
                    break;
                case MORELIA:
                    size = 3;
                    break;
                default:
                    size = 4;
            }

            DecimalFormat decimalFormat = new DecimalFormat();
            decimalFormat.setMaximumFractionDigits(2);
            decimalFormat.setGroupingUsed(false);

            for (int i = 0; i < size; i++) {
				/*
				Log.e("efectivo de "
						+ array.getJSONObject(i).getString(
								Empresa.nombre_empresa),
						""
								+ getEfectivoPreference().getFloat(
										array.getJSONObject(i).getString(
												Empresa.nombre_empresa), 0));
				*/
                TextView view = new TextView(getContext());
				/*
				saldo.put(
						array.getJSONObject(i)
								.getString(Empresa.nombre_empresa),
						getEfectivoPreference().getFloat(
								array.getJSONObject(i).getString(
										Empresa.nombre_empresa), 0) - query.getTotalCanceladosEmpresa(array.getJSONObject(i).getString(Empresa.id_empresa)));
				*//*
				if (DatabaseAssistant.isThereMoreThanOnePeriodActive()) {
					view.setText(
							companies.get(i).getName()
									+ " :$"
									+ decimalFormat.format(getEfectivoPreference().getFloat(
									companies.get(i).getName(), 0)));
					container.addView(view);
				} else {*/
                view.setText(ExtendedActivity.getCompanyNameFormatted( companies.get(i).getName(), false ) + " :$" + decimalFormat.format(DatabaseAssistant.getTotalEfectivoAcumuladoPorEmpresa(companies.get(i).getIdCompany())));
                view.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
                container.addView(view);
                //}
            }

        }
        //} catch (JSONException e1) {
        //	e1.printStackTrace();
        //}
    }

    /**
     * deprecated in 3.0
     *
     * @param alertShowing
     */
    public void setAlertShowing(boolean alertShowing) {
        this.alertShowing = alertShowing;
    }

    /**
     * if office is selected, total cash in set in amount text field and
     * company selector disappears,
     * else, just company selector appears.
     *
     * @param bancoPosicion bank selector position selected
     */
    public void setBancoPosicion(int bancoPosicion)
    {
        this.bancoPosicion = bancoPosicion;
        EditText etMonto = (EditText) getView().findViewById(R.id.et_monto);

        if (BuildConfig.getTargetBranch() == BuildConfig.Branches.GUADALAJARA)
        {
            switch (bancoPosicion)
            {
				/*case 4:
				case 2:
					depositAsIncidencias = false;
					((Spinner) findViewById(R.id.spinnerEmpresas)).setSelection(2);
					findViewById(R.id.spinnerEmpresas).setEnabled(false);
					enableTextBoxes();
					break;*/
                case 3:
                case 5:
                case 7:
                    depositAsIncidencias = false;
                    ((Spinner) getView().findViewById(R.id.spinnerEmpresas)).setSelection(1);
                    getView().findViewById(R.id.spinnerEmpresas).setEnabled(false);
                    enableTextBoxes();
                    break;
                case 1:
                case 9:
                    try {
                        String no_cobrador = datosCobrador.getString("no_cobrador");
                        if (no_cobrador.equals("35148")) {
                            depositAsIncidencias = false;
                            ((Spinner) getView().findViewById(R.id.spinnerEmpresas)).setSelection(0);
                            getView().findViewById(R.id.spinnerEmpresas).setEnabled(true);
                            enableTextBoxes();
                        } else {
                            depositAsIncidencias = false;
                            ((Spinner) getView().findViewById(R.id.spinnerEmpresas)).setSelection(1);
                            getView().findViewById(R.id.spinnerEmpresas).setEnabled(false);
                            enableTextBoxes();
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        depositAsIncidencias = false;
                        ((Spinner) getView().findViewById(R.id.spinnerEmpresas)).setSelection(1);
                        getView().findViewById(R.id.spinnerEmpresas).setEnabled(false);
                        enableTextBoxes();
                    }
                    break;
                case 12:
                    //incidencias

                    ((Spinner) getView().findViewById(R.id.spinnerEmpresas)).setSelection(0);
                    getView().findViewById(R.id.spinnerEmpresas).setEnabled(false);
                    disableTextBoxes();

                    final Dialog mDialog = new Dialog(getContext());
                    mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    mDialog.setContentView(R.layout.dialog_password);
                    mDialog.setCancelable(false);
                    mDialog.getWindow()
                            .setBackgroundDrawable(
                                    new ColorDrawable(
                                            Color.DKGRAY));
                    mDialog.show();

                    if (mDialog != null) {
                        TextView tvAceptar = (TextView) mDialog.findViewById(R.id.tvAccept);
                        TextView tvCancel = (TextView) mDialog.findViewById(R.id.tvCancel);
                        //EditText etPassword = (EditText) mDialog.findViewById(R.id.etPassword);

                        tvAceptar.setOnClickListener(new View.OnClickListener() {

                            EditText etPassword = (EditText) mDialog.findViewById(R.id.etPassword);
                            String password;

                            @Override
                            public void onClick(View v) {
                                password = etPassword.getText().toString();
                                Util.Log.ih("variable password = " + password);
                                if (password.equals("1n61d3")) {
                                    if (!mainActivity.isFinishing()) {
                                        if (mDialog != null && mDialog.isShowing()) {
                                            Util.Log.ih("dismissing progress dialog");
                                            mDialog.dismiss();
                                            //mDialog = null;
                                        }
                                    }
                                    depositAsIncidencias = true;
                                    enableTextBoxes();
                                    mainActivity.findViewById(R.id.spinnerEmpresas).setEnabled(true);
                                    reloadCompaniesSpinner();
                                } else {
                                    mainActivity.toastS(R.string.error_message_password);
                                }
                            }
                        });

                        tvCancel.setOnClickListener(v -> {
                            if (!mainActivity.isFinishing()) {
                                if (mDialog != null && mDialog.isShowing()) {
                                    Util.Log.ih("dismissing progress dialog");
                                    mDialog.dismiss();
                                    //mDialog = null;
                                }
                            }
                            depositAsIncidencias = false;
                            enableTextBoxes();
                            ((TextView) mainActivity.findViewById(R.id.tv_banco)).setText("");
                            mainActivity.findViewById(R.id.spinnerEmpresas).setEnabled(true);
                            reloadCompaniesSpinner();
                        });
                    }
                    break;
                default:
                    depositAsIncidencias = false;
                    ((Spinner) mainActivity.findViewById(R.id.spinnerEmpresas)).setSelection(0);
                    mainActivity.findViewById(R.id.spinnerEmpresas).setEnabled(true);
                    enableTextBoxes();
                    break;
            }
        }
        /*else if (BuildConfig.getTargetBranch() == BuildConfig.Branches.QUERETARO)
        {
            try
            {
                switch (arrayBanks.getJSONObject(bancoPosicion).getString("nombre"))
                {
                    case "Oficina":
                        Toast.makeText(getContext(), "Es Queretaro y es Oficina", Toast.LENGTH_SHORT).show();
                        spinnerEmpresas.setVisibility(View.GONE);
                        getView().findViewById(R.id.spinnerEmpresas).setVisibility(View.GONE);
                        etMonto.setText("" + mainActivity.getTotalEfectivo());
                        etMonto.setEnabled(true);
                        break;
                }
            }
            catch (JSONException ex)
            {

            }

        }*/

        mainActivity.findViewById(R.id.et_monto).setEnabled(true);
        if (bancoPosicion == 11)
        {
            //11OficinaParaGuadalajara
            //DecimalFormat formatter = new DecimalFormat("#,###,###.##");
            getView().findViewById(R.id.spinnerEmpresas).setVisibility(View.GONE);
            etMonto.setText("" + mainActivity.getTotalEfectivo());
            etMonto.setEnabled(true);

        } else {
            getView().findViewById(R.id.spinnerEmpresas).setVisibility(View.VISIBLE);
            etMonto.setEnabled(true);
        }
    }

    public void reloadCompaniesSpinner() {
        Spinner spinerEmpresas = (Spinner) getView().findViewById(R.id.spinnerEmpresas);
        //SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
        try {
            adapter = new EmpresasSpinnerAdapter(
                    DatabaseAssistant.getCompaniesForSpinner(depositAsIncidencias), getContext());
            spinerEmpresas.setAdapter(adapter);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        spinerEmpresas.setOnItemSelectedListener(this);
    }

    /**
     * shows banks in the bank selector
     */
    private void showSpinnerBancos() {
        if (arrayBanks != null) {
            ArrayList<String> listaAdapter = new ArrayList<String>();
            for (int i = 0; i < arrayBanks.length(); i++) {
                JSONObject jsonTmp;
                try {
                    jsonTmp = arrayBanks.getJSONObject(i);
                    if (jsonTmp.has("nombre")) {
                        listaAdapter.add(jsonTmp.getString("nombre"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            mthis = this;
            BanksDialog dialog = new BanksDialog(getContext(), listaAdapter, 1);
            dialog.show();
        }

    }

    /**
     * starts 5 seconds loop between requests until
     * request is accepted, rejected or canceled by user
     * <p>
     * deposit to office
     */
    private void verificarStatusDeposito() {
        getView().findViewById(R.id.rl_cancelar_peticion).setVisibility(View.VISIBLE);
        dismissMyCustomDialog();
        InputMethodManager inputManager = (InputMethodManager) ApplicationResourcesProvider.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(new View(mainActivity).getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        status = 0;
        new Thread(() -> {
            while (status == 0) {
                Util.Log.ih("iteracion en while para getStatusDeposito()");
                if (mainActivity.isConnected && getContext() != null) {
                    SharedPreferences preferences = getContext().getSharedPreferences("PABS_Deposito", Context.MODE_PRIVATE);
                    Editor editor = preferences.edit();
                    editor.putString("id_status_deposito", "" + idStatus);
                    editor.putBoolean("configuracion_por_razon", false);
                    editor.putBoolean("requesterCanceled", true);
                    editor.putString("monto_depositado", "" + montoDepositado);
                    editor.apply();

                    Log.e("monto depositado saved", "" + montoDepositado);

                    //----- comment this -------------------------------------
                    //Jordan Alvarez
                    SharedPreferences preferences2 = getContext().getSharedPreferences("PABS_SPLASH", Context.MODE_PRIVATE);
                    Editor editor2 = preferences2.edit();
                    editor2.putString("id_status_deposito", idStatus);
                    editor2.apply();
                    //--------------------------------------------------------

                    mainActivity.runOnUiThread(this::getStatusDepositoVoluntario);

                } else {
                    mainActivity.runOnUiThread(() -> {
                        if (getContext() != null) {
                            Toast toast = Toast
                                    .makeText(
                                            getContext(),
                                            getResources()
                                                    .getString(
                                                            R.string.error_message_network),
                                            Toast.LENGTH_SHORT);
                            toast.show();

                            //findViewById(R.id.rl_cancelar_peticion).setVisibility(View.VISIBLE);

                            mainActivity.hasActiveInternetConnection(ApplicationResourcesProvider.getContext(), mainActivity.mainLayout);
                        }
                    });

                    mainActivity.runOnUiThread(this::enableTextBoxes);
                }
                Util.Log.ih("esperando 5 segundos...");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Util.Log.ih("while(status == 0) -> status = " + status);
        }).start();
    }

    /**
     * sends request for deposit to office
     * <p>
     * Web Service
     * 'http://50.62.56.182/ecobro/controldepositos/getStatus'
     * <p>
     * JSONObjecta param
     * {
     * "no_deposito": ""
     * }
     */
    private void getStatusDepositoVoluntario() {
        JSONObject json = new JSONObject();
        try {
            json.put("no_deposito", "" + idStatus);
        } catch (Exception e) {
            mainActivity.log("postException", "" + e);
        }

        requestStatusDepositoVoluntario(json);


        /*NetHelper.getInstance().ServiceExecuter(new NetService(ConstantsPabs.urlGetStatusDeposito, json), getContext(), new onWorkCompleteListener() {

            @Override
            public void onError(Exception e) {
                if (getView() != null)
                    getView().findViewById(R.id.rl_cancelar_peticion).setVisibility(View.GONE);
                //mainActivity.toastL(R.string.error_message_network);
                Snackbar.make(mainActivity.mainLayout, R.string.error_message_network, Snackbar.LENGTH_SHORT).show();
                status = -1;
                enableTextBoxes();
            }

            @Override
            public void onCompletion(String result) {
                try {
                    manage_GetStatusDeposito(new JSONObject(result));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });*/
    }

    private void requestStatusDepositoVoluntario(JSONObject jsonParams)
    {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlGetStatusDeposito, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response)
            {
                manage_GetStatusDeposito(response);
                Log.i("Request LOGIN-->", new Gson().toJson(response));
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        if (getView() != null)
                            getView().findViewById(R.id.rl_cancelar_peticion).setVisibility(View.GONE);

                        Snackbar.make(mainActivity.mainLayout, R.string.error_message_network, Snackbar.LENGTH_SHORT).show();

                        status = -1;

                        enableTextBoxes();
                        Log.e("getNumNotificaciones", "Error al obtener numero de notificaciones");
                    }
                }) {

        };
        Log.d("POST REQUEST-->", postRequest.toString());
        postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
    }

    public void setFragmentResult(OnFragmentResult fragmentResult) {
        this.fragmentResult = fragmentResult;
    }
}
