
package com.jaguarlabs.pabs.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.LongDef;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tscdll.TSCActivity;
import com.google.gson.JsonIOException;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.components.Preferences;
import com.jaguarlabs.pabs.database.Clients;
import com.jaguarlabs.pabs.database.Collectors;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.MontosTotalesDeEfectivo;
import com.jaguarlabs.pabs.database.Notice;
import com.jaguarlabs.pabs.database.Suspensionitem;
import com.jaguarlabs.pabs.dialog.RazonesDialog;
import com.jaguarlabs.pabs.net.NetHelper;
import com.jaguarlabs.pabs.net.NetHelper.onWorkCompleteListener;
import com.jaguarlabs.pabs.net.NetService;
import com.jaguarlabs.pabs.rest.SpiceHelper;
import com.jaguarlabs.pabs.rest.models.ModelPrintPaymentRequest;
import com.jaguarlabs.pabs.rest.request.offline.PrintAccountStatusTicket;
import com.jaguarlabs.pabs.rest.request.offline.PrintPaymentTicketRequest;
import com.jaguarlabs.pabs.rest.request.offline.PrintTemporalSuspensionNotice;
import com.jaguarlabs.pabs.rest.request.offline.PrintVisitTicketRequest;
import com.jaguarlabs.pabs.util.*;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.SharedConstants.Printer;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import com.jaguarlabs.probenso.location.GPSProvider;

/**
 * This class is used when collector is trying to make a payment "fuera de cartera"
 * It's basically the same than ClienteDetalle.class, but the only difference is
 * that you can look for a client on the server using a part of the name, a part
 * of a contract number or both.
 */
 @SuppressLint("SimpleDateFormat")
 public class Aportacion extends Fragment implements NoAportoCallback {
     private static final String KEY_CACHE = "aportacion_no_cartera_cache";
     boolean ban1 = false;
     boolean ban2 = false;
    DatePickerDialog.OnDateSetListener mDateSetListener;
     String opcionSeleccionada="";
     boolean imprimio = false;
    private String codigoCobrador = "";
    String idContrato="";
     int selected = 0;
     private static Aportacion mthis = null;
     private JSONObject jsonInfo;
     private JSONObject datosCobrador;
     private Dialog dialog;
     String tipoEmpresa;
    Dialog myDialog, myDialog2, myDialog3;
     String empresaString = "";
     String datoempresa1 = "";
     String datoempresa2 = "";
     String[] direccion;
     public static boolean readyToPrint = false;
     String datoempresa3 = "";
     int impreso = 0;
     String datoempresa4 = "";
     String no_cobro;
     String datoempresa5 = "";
     String rfc = "";
     String mensaje = "";
     String datoempresa6 = "";
    String no_contrato = "";
     String datoempresa7 = "";
     // String folio = "";
     String ult_folio = "";
     String new_folio = "";
     String total = "";
    Dialog dialogo_suspencion;
    public int diia, mess, ano;
     private String datoempresa4_5;
     private ProgressDialog pd;
    private String mensajePercapita="", tituloPercapitaGeneral="";

     private boolean pendingTicket = false;
     private Preferences preferencesTicketPending;
     private float cashPreferenceBeforeChange = 0;

     //public GPSProvider gpsProviderAportacion;

    private long dateAux;
    private Timer timer;

    private int typeStatus;

    private TSCActivity TscDll;

    private Button btNoAporto;

    private AlertDialog.Builder tryAgainDialog;

    private EditText etAmount;

    private String amount;

    private int copies = 0;
    private android.support.v7.app.AlertDialog.Builder dialogo;
    private MainActivity mainActivity;
    private SpiceManager spiceManagerOffliine;
    private SpiceHelper<Boolean> spicePrintVisitTicket;
    private SpiceHelper<Boolean> spicePrintAccountStatusTicket;
    private SpiceHelper<Boolean> spicePrintPaymentTicket;

    private OnFragmentResult fragmentResult;

    private int requestCode = -1;

    private String paymentReferenceString = "";
    private TextView cancelSavingPaymentReference;
    private TextView savePaymentReference;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activiity_aportacion, container, false);

        mainActivity = (MainActivity) getActivity();
        myDialog = new Dialog(mainActivity);
        myDialog2 = new Dialog(mainActivity);
        myDialog3 = new Dialog(mainActivity);
        dialogo_suspencion = new Dialog(getContext());
        spiceManagerOffliine = mainActivity.getSpiceManagerOffline();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        requestCode = getArguments().getInt("requestCode", -1);

        Button buscarCoincidencias = view.findViewById(R.id.btn_buscar_coincidencias);
        ImageView back = view.findViewById(R.id.imageViewBack);
        Button buttonImprimir = view.findViewById(R.id.buttonImprimir);

        buttonImprimir.setOnClickListener(v -> {
            //Click botón imprimir aportación *CABG
            amountButtonsAvailable(false);
            enviarImprimir();
        });

        back.setOnClickListener(v -> {
            mthis = null;
            if (MainActivity.CURRENT_TAG.equals(MainActivity.TAG_APORTACION))
                getFragmentManager().popBackStack();
        });

        buscarCoincidencias.setOnClickListener(v -> {
            if (mainActivity.isConnected) {
                Util.Log.ih("si hay internet");
                buscarCoincidencias();
            } else {
                try {
                    Util.Log.ih("No hay internet");
                         /*
                         showAlertDialog("",
                                 getString(R.string.message_verify_internet), false);
                         */
                    AlertDialog.Builder internetAlert = getAlertDialogBuilder()
                            .setMessage(R.string.error_message_network)
                            .setPositiveButton(R.string.accept, null)
                            .setNegativeButton(R.string.salir, (dialog1, which) -> {
                                mthis = null;
                                //finish();
                                if (MainActivity.CURRENT_TAG.equals(MainActivity.TAG_APORTACION))
                                    getFragmentManager().popBackStack();
                            });
                    internetAlert.setCancelable(false);
                    internetAlert.create().show();
                } catch (Exception e) {
                    Toast toast = Toast.makeText(getContext(), R.string.error_message_network, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    e.printStackTrace();
                }
            }
        });

        spicePrintVisitTicket = new SpiceHelper<Boolean>(spiceManagerOffliine, "printVisitTicketCache", Boolean.class, 1000 * 30) {
            @Override
            protected void onSuccess(Boolean result) {
                super.onSuccess(result);
                Util.Log.ih("onSuccess - spicePrintVisitTicket");
                if (result != null && result) {
                    Util.Log.ih("processNoAporto");
                    processNoAporto();
                } else {
                    Util.Log.ih("error message printing");
                    mainActivity.toastL(R.string.error_message_printing);
                }
            }

            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                fail();
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                progressUpdate(progress);
            }
        };

        spicePrintAccountStatusTicket = new SpiceHelper<Boolean>(spiceManagerOffliine, "printAccountStatusTicket", Boolean.class, DurationInMillis.ONE_HOUR) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                fail();
            }

            @Override
            protected void onSuccess(Boolean result) {
                super.onSuccess(result);
                if (!result) {
                    mainActivity.toastL(R.string.error_message_printing);
                }
            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                progressUpdate(progress);
            }
        };

        spicePrintPaymentTicket = new SpiceHelper<Boolean>(spiceManagerOffliine, "print_payment_ticket", Boolean.class, 1000 * 30) {
            @Override
            protected void onFail(SpiceException exception) {
                super.onFail(exception);
                onAportacionFailure(exception);
            }

            @Override
            protected void onSuccess(Boolean result) {

                try {

                    if (result != null) {
                        Util.Log.ih("result != null");

                        ConstantsPabs.finishPrinting = false;

                        super.onSuccess(result);
                        Util.Log.ih("onSuccess - spicePrintPaymentTicket");
                        onAportacionSuccess(result);
                    } else {
                        Util.Log.ih("result == null");

                        super.onSuccess(ConstantsPabs.finishPrinting);
                        Util.Log.ih("onSuccess - spicePrintPaymentTicket");
                        onAportacionSuccess(ConstantsPabs.finishPrinting);

                        ConstantsPabs.finishPrinting = false;
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();

                    ConstantsPabs.finishPrinting = false;

                    //setPaymentNotSync();
                    if (!preferencesTicketPending.isThereAPaymentAndIsSynchronized()) {
                        setPaymentNotSync();
                    }

                    super.onSuccess(true);
                    Util.Log.ih("NullPointerException - onSuccess - spicePrintPaymentTicket");
                    onAportacionSuccess(true);
                }

            }

            @Override
            protected void onProgressUpdate(RequestProgress progress) {
                progressUpdate(progress);
            }
        };

        //init

        //GPSProvider instance
        if (ApplicationResourcesProvider.getLocationProvider().getGPSPosition() == null) {
            //gpsProviderAportacion = new GPSProvider(this);
            //Util.Log.ih("gpsProvider = " + gpsProviderAportacion.isConnected);//will return false because was just created
        }

        mainActivity.hasActiveInternetConnection(getContext(), mainActivity.mainLayout);
        mthis = this;
        ExtendedActivity.setEstoyLogin(false);
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
                .getDefaultAdapter();

        etAmount = (EditText) view.findViewById(R.id.et_cantidad_aportacion);

        // //Best practice point, the string messages would be at
        // res/values/string.xml
        if (mBluetoothAdapter == null) {
            try {
                mainActivity.showAlertDialog(
                        "Bluetooth",
                        getResources().getString(
                                R.string.error_message_bluetooth_unsoported), true);
            } catch (Exception e) {
                Toast toast = Toast.makeText(getContext(), R.string.error_message_bluetooth_unsoported, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                e.printStackTrace();
            }
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                try {
                    mainActivity.showAlertDialog(
                            "Bluetooth",
                            getResources().getString(
                                    R.string.error_message_bluetooth_unactivated), true);
                } catch (Exception e) {
                    Toast toast = Toast.makeText(getContext(), R.string.error_message_bluetooth_unactivated, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    e.printStackTrace();
                }
            }
        }

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey("datosCobrador")) {
                try {
                    datosCobrador = new JSONObject(
                            bundle.getString("datosCobrador"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        mensaje = DatabaseAssistant.getCollector().getMessage();

        setListenersOpciones();

        preferencesTicketPending = new Preferences(getContext());
    }

     @Override
     public void onStart() {
         super.onStart();

         //spiceManagerOffliine.start(this);

         if (spicePrintPaymentTicket.isRequestPending()){
             Util.Log.ih("::::::::spicePrintPaymentTicket.isRequestPending()");
             spicePrintPaymentTicket.executePendingRequest();
         }
         else if (spicePrintVisitTicket.isRequestPending()){
             Util.Log.ih("spicePrintVisitTicket.isRequestPending()");
             spicePrintVisitTicket.executePendingRequest();
         }
     }

     @Override
     public void onStop() {
         //spiceManagerOffliine.shouldStop();
         super.onStop();
         // Apphance.onStop(this);
     }

     /*@Override
     public boolean onKeyDown(int keyCode, KeyEvent event) {
         //when true
         //a ticket is being printed
         if (goBack) {
             if (keyCode == KeyEvent.KEYCODE_BACK) {
                 mthis = null;
             }
             return super.onKeyDown(keyCode, event);
         }
         return false;
     }*/

     @Override
     public void onResume() {
         super.onResume();
         mthis = this;

         MainActivity.CURRENT_TAG = MainActivity.TAG_APORTACION;
     }

     @Override
     public void onActivityResult(int requestCode, int resultCode, Intent data) {
         super.onActivityResult(requestCode, resultCode, data);
         //came back from BuscarCliente activity
         //if the search brought results, fills fields
         if (requestCode == 1) {
             if (resultCode == Activity.RESULT_OK) {
                 try {
                     jsonInfo = new JSONObject(data.getStringExtra("json_info"));
                     llenarCampos();
                     inhabilitarCampos();
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
             }
         }
     }

    @Override
     public void onDestroy() {
        mthis = null;
         /*
         if (imprimio) {
             // /TODO change harcoded string by final strings
             SharedPreferences preference = getSharedPreferences("folios",
                     Context.MODE_PRIVATE);
             Editor editor = preference.edit();
             SqlQueryAssistant query = new SqlQueryAssistant(
                     getApplicationContext());
             try {
                 editor.putString(
                         query.mostrarEmpresa(
                                 jsonInfo.getString("id_grupo_base")).getString(
                                 Empresa.nombre_empresa), new_folio);
             } catch (JSONException e) {
                 e.printStackTrace();
             }
             editor.commit();
         }
        */
         super.onDestroy();
     }

    private void progressUpdate(RequestProgress progress){
        switch ( (int) progress.getProgress() ){
            case ConstantsPabs.START_PRINTING:
                showMyCustomDialog();
                break;
            case ConstantsPabs.FINISHED_PRINTING:
                dismissMyCustomDialog();
                break;
            default:
                //nothing
        }
    }

    private void fail(){
        dismissMyCustomDialog();
        mainActivity.toastL(R.string.error_message_printing);
    }

     /**
      * sets listeners of resources treated as buttons
      * - search
      * - go back
      * - print ticket
      */
     public void onClickFromXML(View v) {
         switch (v.getId()) {
             case R.id.btn_buscar_coincidencias:
                 if (mainActivity.isConnected) {
                     Util.Log.ih("si hay internet");
                     buscarCoincidencias();
                 } else {
                     try {
                         Util.Log.ih("No hay internet");
                         /*
                         showAlertDialog("",
                                 getString(R.string.message_verify_internet), false);
                         */
                         AlertDialog.Builder internetAlert = getAlertDialogBuilder()
                                 .setMessage(R.string.error_message_network)
                                 .setPositiveButton(R.string.accept, null)
                                 .setNegativeButton(R.string.salir, (dialog1, which) -> {
                                     mthis = null;
                                     //finish();
                                     getFragmentManager().popBackStack();
                                 });
                         internetAlert.setCancelable(false);
                         internetAlert.create().show();
                     } catch (Exception e){
                         Toast toast = Toast.makeText(getContext(), R.string.error_message_network, Toast.LENGTH_LONG);
                         toast.setGravity(Gravity.CENTER, 0, 0);
                         toast.show();
                         e.printStackTrace();
                     }
                 }
                 break;
             case R.id.imageViewBack:
                 mthis = null;
                 //finish();
                 getFragmentManager().popBackStack();
                 break;
             /*
             case R.id.buttonBorrar:

                 if ( !pendingTicket ) {
                     limpiarDatos();
                     habilitarCampos();
                 }

                 break;
             */
             case R.id.buttonImprimir:
                 //Click bot�n imprimir aportaci�n *CABG
                 amountButtonsAvailable(false);
                 enviarImprimir();
                 break;
             default:
                 break;
         }
     }

    /**
     * starts a new intent to BuscarCliente activity with name and contract as
     * params for looking for coincidences with given name or contract number
     */
     private void buscarCoincidencias() {
         String nombre = ((EditText) getView().findViewById(R.id.et_nombre_apellidos))
                 .getText().toString();
         String contrato = ((EditText) getView().findViewById(R.id.et_no_contrato))
                 .getText().toString();
         if (nombre.length() > 2 || contrato.length() > 2) {
             Intent intent = new Intent(getContext(), BuscarCliente.class);
             intent.putExtra("nombre", "" + nombre);
             intent.putExtra("contrato", "" + contrato);
             startActivityForResult(intent, 1);
         } else {
             // /TODO change harcoded string
             try {
                 mainActivity.showAlertDialog("",
                         "Llena al menos un campo con 3 letras para buscar.", false);
             } catch (Exception e){
                 Toast toast = Toast.makeText(getContext(), "Llena al menos un campo con 3 letras para buscar.", Toast.LENGTH_LONG);
                 toast.setGravity(Gravity.CENTER, 0, 0);
                 toast.show();
                 e.printStackTrace();
             }
         }

     }

    /**
     * deprecated in 3.0
     */
     private void limpiarDatos() {
         ((EditText) getView().findViewById(R.id.et_nombre_apellidos)).setText("");
         ((EditText) getView().findViewById(R.id.et_no_contrato)).setText("");
         ((TextView) getView().findViewById(R.id.et_direccion)).setText("");
         ((EditText) getView().findViewById(R.id.et_cantidad_aportacion)).setText("");
     }

    /**
     * enables or disables amount buttons
     *
     * @param flag
     *          boolean type, true or false to set amount buttons
     *          availability
     */
    private void amountButtonsAvailable(boolean flag){
        if (getView() != null) {
            ImageView amountButton50 = getView().findViewById(R.id.imageViewOp1);
            ImageView amountButton100 = getView().findViewById(R.id.imageViewOp2);
            ImageView amountButton200 = getView().findViewById(R.id.imageViewOp3);
            ImageView amountButton400 = getView().findViewById(R.id.imageViewOp4);
            ImageView amountButton75 = getView().findViewById(R.id.imageViewOp5);
            ImageView amountButton150 = getView().findViewById(R.id.imageViewOp6);
            ImageView amountButton300 = getView().findViewById(R.id.imageViewOp7);

            amountButton50.setEnabled(flag);
            amountButton100.setEnabled(flag);
            amountButton200.setEnabled(flag);
            amountButton400.setEnabled(flag);
            amountButton75.setEnabled(flag);
            amountButton150.setEnabled(flag);
            amountButton300.setEnabled(flag);
        }
    }

    /**
     * checks that a client was selected and an amount given and
     * that cash is under cash limit, if so
     * shows ticket preview
     */
     private void enviarImprimir() {
         if (camposLlenos()) {
             //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
             amount = etAmount.getText().toString();
             String saldo = "";

             try {
                 saldo = jsonInfo.getString("Saldo");
             }
             catch (JSONException ex)
             {
                 ex.printStackTrace();
             }

             if (!saldo.equals("")) {
                 Double balance = Double.parseDouble(saldo);

                 if (Double.parseDouble(amount) > balance)
                 {
                     android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mainActivity);
                     builder.setMessage("Verifica que la cantidad a cobrar no exceda el saldo pendiente.\n\nSaldo pendiente: $" + balance);
                     builder.setNeutralButton("Aceptar", null);
                     builder.setCancelable(false);
                     builder.show();
                     amountButtonsAvailable(true);
                     return;
                 }

                 DecimalFormat df = new DecimalFormat();
                 df.setMaximumFractionDigits(2);
                 df.setGroupingUsed(false);
                 etAmount.setText(df.format(Float.parseFloat(amount)));

                 double max_efectivo = 0;
                 try {
                     max_efectivo = datosCobrador.getDouble("max_efectivo");
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
                 if (mainActivity.isMoneyType(amount)) {
                     double apor = Double.parseDouble(amount);
                     if (apor <= max_efectivo) {
                         //if (isConnected) {
                         //registroImpresionFuera();
                         imprimir();
                         //} else {
                         //    showAlertDialog("",
                         //            getString(R.string.message_verify_internet),
                         //            false);
                         //}
                     } else {
                         amountButtonsAvailable(true);
                         try {
                             mainActivity.showAlertDialog("",
                                     getString(R.string.error_message_over_amount), false);
                         } catch (Exception e) {
                             Toast toast = Toast.makeText(getContext(), R.string.error_message_over_amount, Toast.LENGTH_LONG);
                             toast.setGravity(Gravity.CENTER, 0, 0);
                             toast.show();
                             e.printStackTrace();
                         }
                     }
                 } else {
                     amountButtonsAvailable(true);
                     try {
                         mainActivity.showAlertDialog("",
                                 getString(R.string.error_message_amount),
                                 true);
                     } catch (Exception e) {
                         Toast toast = Toast.makeText(getContext(), R.string.error_message_amount, Toast.LENGTH_LONG);
                         toast.setGravity(Gravity.CENTER, 0, 0);
                         toast.show();
                         e.printStackTrace();
                     }
                 }
             }

         } else {
             amountButtonsAvailable(true);
             try {
                 mainActivity.showAlertDialog("", "Llena todos los campos.", false);
             } catch (Exception e){
                 Toast toast = Toast.makeText(getContext(), "Llena todos los campos.", Toast.LENGTH_LONG);
                 toast.setGravity(Gravity.CENTER, 0, 0);
                 toast.show();
                 e.printStackTrace();
             }
         }
     }

    /**
     * deprecated in 3.0
     */
     private void registroImpresionFuera() {
         JSONObject json = new JSONObject();

         try {
             json.put("no_cobro", no_cobro);
         } catch (Exception e) {
             e.printStackTrace();
         }
         NetHelper.getInstance().ServiceExecuter(
                 new NetService(ConstantsPabs.urlImpresion, json), getContext(),
                 new onWorkCompleteListener() {

                     @Override
                     public void onError(Exception e) {

                     }

                     @Override
                     public void onCompletion(String result) {
                     }
                 });
     }

     //Dialogo con el resumen de lo que se quiere imprimir, presionar aceptar para imprimir. *CABG
     @SuppressLint("SimpleDateFormat")
     public void imprimir() {

         try {
             setEmpresa(jsonInfo.getString("id_grupo_base"));
         } catch (Exception e1) {
             e1.printStackTrace();
             mthis = null;
             mainActivity.toastL("Hubo un error. Por favor intentalo de nuevo");
             //finish();
             getFragmentManager().popBackStack();
         }
         try {
             final SimpleDateFormat dateFormat = new SimpleDateFormat(
                     "dd/MM/yyyy");
             final SimpleDateFormat dateHora = new SimpleDateFormat("HH:mm");
             final Date date = new Date();
             mainActivity.runOnUiThread(() -> {
                 dialog = getDialog();
                 dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                 dialog.setContentView(R.layout.ticketpreview);
                 dialog.setCancelable(false);
                 dialog.getWindow().setBackgroundDrawable(
                         new ColorDrawable(
                                 Color.TRANSPARENT));
                 dialog.show();
             });
             //fills ticket preview's fields with client's info
             TextView serie = (TextView) dialog.findViewById(R.id.textViewSerie);
             serie.setVisibility(View.INVISIBLE);
             TextView fecha = (TextView) dialog.findViewById(R.id.textViewFecha);
             fecha.setText(dateFormat.format(date).toString());
             TextView hora = (TextView) dialog.findViewById(R.id.textViewHora);
             hora.setText(dateHora.format(date).toString());
             TextView Malba = (TextView) dialog.findViewById(R.id.textViewMalba);
             Malba.setText( ExtendedActivity.getCompanyNameFormatted( empresaString, false ) );
             TextView NoRegistro = (TextView) dialog
                     .findViewById(R.id.textViewRegistroCobro);
             String seriecobrador = "";
             //SqlQueryAssistant query = null;
             try {
                 // setEmpresa(jsonInfo.getInt("empresa"));
                 //query = new SqlQueryAssistant(this);

                 switch (com.jaguarlabs.pabs.util.BuildConfig.getTargetBranch()) {
                     case NAYARIT:case MEXICALI:case MORELIA:case TORREON:
                         if (jsonInfo.getString("id_grupo_base").equals("01")) {
                             seriecobrador = datosCobrador
                                     .getString("serie_programa");
                         } else if (jsonInfo.getString("id_grupo_base").equals("03")) {
                             seriecobrador = datosCobrador
                                     .getString("serie_malba");
                         } else if (jsonInfo.getString("id_grupo_base").equals("04")) {
                             seriecobrador = datosCobrador
                                     .getString("serie_empresa4");
                         } else {
                             //seriecobrador = datosCobrador.getString(
                             //        DatabaseAssistant.getSingleCompany(empresa).getName());
                         }
                         break;
                     default:
                         if (jsonInfo.getString("id_grupo_base").equals("01")) {
                             seriecobrador = datosCobrador.getString("serie_programa");
                         } else {
                             seriecobrador = datosCobrador
                                     .getString(
                                             DatabaseAssistant.getSingleCompany(jsonInfo.getString("id_grupo_base")).getName());
                             //query.mostrarEmpresa(
                             //jsonInfo.getString("id_grupo_base"))
                             //.getString(Empresa.nombre_empresa));
                         }
                 }

                 /*
                 if (jsonInfo.getString("id_grupo_base").equals("01")) {
                     seriecobrador = datosCobrador.getString("serie_programa");
                 } else {
                     seriecobrador = datosCobrador
                             .getString(
                                     DatabaseAssistant.getSingleCompany(jsonInfo.getString("id_grupo_base")).getName());
                                     //query.mostrarEmpresa(
                                     //jsonInfo.getString("id_grupo_base"))
                                     //.getString(Empresa.nombre_empresa));
                 }
                 */

                 if (seriecobrador.equals("3M") && new_folio.equals("71935"))
                 {
                     new_folio = "72155";
                 }
                 else if (seriecobrador.equals("3M,") && new_folio.equals("24280"))
                 {
                     new_folio = "24410";
                 }
                 else if (seriecobrador.equals("V1") && new_folio.equals("59211"))
                 {
                     new_folio = "59291";
                 }
                 else if (seriecobrador.equals("6C") && new_folio.equals("63063"))
                 {
                     new_folio = "63071";
                 }
                 else if (seriecobrador.equals("1A1") && new_folio.equals("96874"))
                 {
                     new_folio = "96896";
                 }
             } catch (JSONException e) {
                 e.printStackTrace();
                 // ///TODO change logs by usefil log
                 seriecobrador = "=D";
             } finally {
                 /*
                 if (query.db != null){
                     query.db.close();
                 }
                 query = null;
                 */
             }


             /**
              * If there's a pending ticket to print
              * will get folio from last payment registered
              */
             if ( pendingTicket ){
                 JSONObject lastPayment = preferencesTicketPending.getPayment();

                 try {

                     /**
                      * Gets folio from last payment registered
                      */
                     NoRegistro.setText("Folio: " + seriecobrador + lastPayment.getString(getString(R.string.key_folio)));
                 } catch (JSONException e){
                     e.printStackTrace();
                 }
             }
             else{
                 NoRegistro.setText("Folio: " + seriecobrador + new_folio);
             }

             //NoRegistro.setText("Folio: " + seriecobrador + (new_folio));
             TextView Cliente = (TextView) dialog
                     .findViewById(R.id.textViewCliente);
             Cliente.setText(jsonInfo.getString("nombre") + " " + jsonInfo.getString("apellido_pat") + " " + jsonInfo.getString("apellido_mat"));
             TextView monto = (TextView) dialog.findViewById(R.id.textViewMonto);
             TextView datofiscal = (TextView) dialog.findViewById(R.id.textViewDatosFiscales);
             String companyNameMask = ExtendedActivity.getCompanyNameFormatted( datoempresa1 );
             datofiscal.setText(companyNameMask + "\n" + datoempresa2 + "\n" + rfc + "\n" + datoempresa3 + "\n" + datoempresa4 + "\n" + datoempresa5 + "\n" + datoempresa6 + "\n " + datoempresa7);
             TextView total = (TextView) dialog.findViewById(R.id.textViewTotal);
             TextView txcontacto = (TextView) dialog.findViewById(R.id.textViewContacto);
             txcontacto.setText("No. de Contrato: " + jsonInfo.getString("serie") + jsonInfo.getString("no_contrato"));
             total.setText("Total: $" + ((EditText) getView().findViewById(R.id.et_cantidad_aportacion)).getText());
             monto.setText(getResources().getString(R.string.amount_given) + " $" + amount);

             String montoToLog = amount;

             final TextView cancelar = (TextView) dialog
                     .findViewById(R.id.textView12);
             cancelar.setOnClickListener(view -> {
                 dialog.dismiss();
                 amountButtonsAvailable(true);
             });
             /*
             cancelar.setOnTouchListener(new OnTouchListener() {

                 @Override
                 public boolean onTouch(View arg0, MotionEvent arg1) {
                     if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
                         //cancelar.setBackgroundColor(Color.RED);
                         return true;
                     }
                     if (arg1.getAction() == MotionEvent.ACTION_UP) {
                         cancelar.setBackgroundColor(Color.WHITE);
                         dialog.dismiss();
                         return true;
                     }
                     return false;
                 }

             });
             */
             TextView aceptar = (TextView) dialog.findViewById(R.id.textView13);
             aceptar.setOnClickListener(new OnClickListener() {

                 String serieCobrador;
                 String monto;

                 @Override
                 public void onClick(View v) {

                     final LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                     final boolean isGPSEnabled = locationManager
                             .isProviderEnabled(LocationManager.GPS_PROVIDER);
                     if (isGPSEnabled) {
                         if (mainActivity.checkBluetoothAdapter()){
                             dialog.dismiss();

                             //Ejecutar Aportaci�nNoCarteraRequest *CABG
                             //mthis.total = amount;

                             //register payment backup
                             try{

                                 if(!pendingTicket) { //No tiene ticket pendiente
                                     //folio, contrato, monto, empresa, periodo, tipo de cobro
                                     LogRegister.paymentRegistrationBackup(
                                             serieCobrador + new_folio,
                                             jsonInfo.getString("serie") + jsonInfo.getString("no_contrato"),
                                             monto,
                                             jsonInfo.getString("id_grupo_base"),
                                             "" + mainActivity.getEfectivoPreference().getInt("periodo", 0),
                                             "Fuera de cartera",
                                             "" + String.valueOf(jsonInfo.getDouble("Saldo") - Float.parseFloat(monto))
                                     );
                                 }
                                 else
                                 {
                                     Log.i("IMPRESION", "TIENES un TICKET PENDOENTE, no podemos guardar el Log Register de nuevo.");
                                 }

                             }catch (JSONException e){
                                 e.printStackTrace();
                             }
                             //end register payment backup



                             /*
                              START ADD PAYMENT REFERENCE
                               */
                             if ( BuildConfig.getTargetBranch() == BuildConfig.Branches.QUERETARO
                                     || BuildConfig.getTargetBranch() == BuildConfig.Branches.IRAPUATO
                                     || BuildConfig.getTargetBranch() == BuildConfig.Branches.SALAMANCA
                                     || BuildConfig.getTargetBranch() == BuildConfig.Branches.CELAYA
                                     || BuildConfig.getTargetBranch() == BuildConfig.Branches.LEON) {
                                 showPaymentReferenceDialog();
                             } else {
                                /*
                                END ADD PAYMENT REFERENCE
                                */

                                 /**
                                  * Checks if there's a pendint ticket to print
                                  */
                                 if (pendingTicket) {

                                     /**
                                      * Gets last payment registered
                                      */
                                     JSONObject lastPayment = preferencesTicketPending.getPayment();

                                     try {

                                         /**
                                          * Gets folio from last payment registered
                                          */
                                         new_folio = lastPayment.getString(getString(R.string.key_folio));


                                         /**
                                          * Prints ticket
                                          */
                                         if (dialog != null) {
                                             dialog.dismiss();
                                         }
                                         executePrintPaymentTicket();


                                         //register payment backup
                                         try {
                                             if(pendingTicket) //No hay ticket pendiente
                                             {
                                                 LogRegister.paymentRegistrationBackup(
                                                         serieCobrador + new_folio,
                                                         jsonInfo.getString("serie") + jsonInfo.getString("no_contrato"),
                                                         monto,
                                                         jsonInfo.getString("id_grupo_base"),
                                                         "" + mainActivity.getEfectivoPreference().getInt("periodo", 0),
                                                         "Reimpresion de folio",
                                                         "-");
                                             }
                                             else
                                             {
                                                 Log.i("IMPRESION", "NO TIENES TICKET PENDIENTE.");
                                             }
                                         } catch (JSONException e) {
                                             e.printStackTrace();
                                         }
                                         //end register payment backup




                                     } catch (JSONException e) {
                                         e.printStackTrace();
                                     }
                                 } else {
                                     if (guardarPago()) {

                                         //checkEverythingWasSaved();

                                         executePrintPaymentTicket();
                                     } else {
                                         Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_call, Toast.LENGTH_LONG);
                                         toast.setGravity(Gravity.CENTER, 0, 0);
                                         toast.show();
                                     }
                                 }
                             }

                         }

                     } else {
                         mainActivity.toastL(R.string.info_message_gps);
                     }

                 }

                 public OnClickListener setData(String monto, String serieCobrador){
                     this.serieCobrador = serieCobrador;
                     this.monto = monto;

                     return this;
                 }

             }.setData(amount, seriecobrador));

         } catch (Exception e) {
             e.printStackTrace();
         }

     }

    /**
     * loads values which will be sent to android service
     * @return Values object with all info
     */
     private ModelPrintPaymentRequest loadValuesToRequest() {
         ModelPrintPaymentRequest modelPrintPaymentRequest = new ModelPrintPaymentRequest();
         modelPrintPaymentRequest = new ModelPrintPaymentRequest();
         modelPrintPaymentRequest.setJsonInfo(jsonInfo);
         modelPrintPaymentRequest.setDatosCobrador(datosCobrador);
         modelPrintPaymentRequest.setNew_folio(new_folio);
         modelPrintPaymentRequest.setEmpresaString(empresaString);
         modelPrintPaymentRequest.setDatoempresa1(datoempresa1);
         modelPrintPaymentRequest.setDatoempresa2(datoempresa2);
         modelPrintPaymentRequest.setRfc(rfc);
         modelPrintPaymentRequest.setDireccion(direccion);
         modelPrintPaymentRequest.setDatoempresa6(datoempresa6);
         modelPrintPaymentRequest.setDatoempresa7(datoempresa7);
         modelPrintPaymentRequest.setImpreso(impreso);
         modelPrintPaymentRequest.setMensaje(mensaje);
         modelPrintPaymentRequest.setImprimio(imprimio);
         modelPrintPaymentRequest.setTotal( amount );
         modelPrintPaymentRequest.setMensajepercapita(mensajePercapita);
         modelPrintPaymentRequest.setTituloPercapita(tituloPercapitaGeneral);
         try {
             modelPrintPaymentRequest.setSaldo( jsonInfo.getString("Saldo") );
         } catch (JSONException e){
             modelPrintPaymentRequest.setSaldo("0.0");
         }
         return modelPrintPaymentRequest;
     }

    /**
     * Gets info of given company (company related to client's account, set as id)
     * as well as gets new folio plus one, so that it will be new folio for the
     * new payment that is ongoing
     * @param nempresa company of client's account
     *                 01, 03, 04 or 05
     */
     private void setEmpresa(String nempresa) {
         SharedPreferences preference = getContext().getSharedPreferences("folios",
                 Context.MODE_PRIVATE);
         //SqlQueryAssistant query = new SqlQueryAssistant(this);
         //try {
             //JSONObject empresa = query.mostrarEmpresa(nempresa);
             Companies company = DatabaseAssistant.getSingleCompany(nempresa);
             Log.e("empresa", company.toString());
             empresaString = company.getName();
             rfc = company.getRfc();
             setRazon(company.getBusinessName());
             setDireccion(company.getAddress());
             setTel(company.getPhone());
         //} catch (JSONException e) {
           //  e.printStackTrace();
         //}

         try {
             ult_folio = preference.getString(company.getName(), "0");
         } catch (Exception e) {
             e.printStackTrace();
             mthis = null;
             mainActivity.toastL("Hubo un error. Por favor intentalo de nuevo");
             //finish();
             getFragmentManager().popBackStack();
         }
         try {
            new_folio = "" + (Integer.parseInt(ult_folio) + 1);
         } catch (NumberFormatException e){
            e.printStackTrace();
            mthis = null;
            mainActivity.toastL("Hubo un error. Por favor intentalo de nuevo");
            //finish();
             getFragmentManager().popBackStack();
        }
     }

     /**
      * sets company's tel info
      */
     public void setTel(String tel) {

       /*if(BuildConfig.getTargetBranch()== BuildConfig.Branches.TAMPICO)
       {
           Log.d("IMPRESION -- > ", "SIN TELEFONO");
       }
       else
       {*/
           datoempresa5 = "INFORMES Y SERVICIOS";
           datoempresa6 = "TELS. " + tel;
           datoempresa7 = "";
       //}

     }

     /**
      * sets company's 'razon' info
      */
     public void setRazon(String razon) {
         if (razon.length() > 34) {
             for (int x = 30; x > 0; x--) {
                 if (razon.charAt(x) == ' ') {
                     datoempresa1 = razon.substring(0, x + 1);
                     datoempresa2 = razon.substring(x + 1);
                     x = 0;
                 }
             }

         } else {
             datoempresa1 = razon;
             datoempresa2 = "";
         }
     }

    /**
     * sets company's address info
     *
     * NOTE*
     *      ADDS COMPANIES
     *
     * @param direccion company's address
     */
     public void setDireccion(String direccion) {
         datoempresa3 = direccion;
         this.direccion = new String[7];
         switch (BuildConfig.getTargetBranch()) {
             case GUADALAJARA:
                 try {
                     if (jsonInfo.getString("id_grupo_base").equals("05")) {
                         this.direccion[0] = "WASHINGTON No. 404";
                         this.direccion[1] = "COL. LA AURORA";
                         this.direccion[2] = "C.P.44460, GUADALAJARA,";
                         this.direccion[3] = "Jalisco, México";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     } else {
                         this.direccion[0] = "Av. Federalismo Norte 1485";
                         this.direccion[1] = "Piso 1 Oficina 3 Colonia ";
                         this.direccion[2] = "San Miguel de Mezquitan";
                         this.direccion[3] = "CP 44260   Guadalajara";
                         this.direccion[4] = "Jalisco, México";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     }
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }

                 break;

             case TAMPICO:
                 try {
                     if (jsonInfo.getString("id_grupo_base").equals("03")) {
                         this.direccion[0] = "AV. HIDALGO NO. 3402 ENTRE";
                         this.direccion[1] = "CALLE VIOLETA Y CALLE ROSA,";
                         this.direccion[2] = "COLONIA FLORES TAMPICO,";
                         this.direccion[3] = "TAMAULIPAS, MÉXICO";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     } else /*empresa 01*/ {
                         this.direccion[0] = "AV. HIDALGO NO. 3402 ENTRE";
                         this.direccion[1] = "CALLE VIOLETA Y CALLE ROSA,";
                         this.direccion[2] = "COLONIA FLORES TAMPICO,";
                         this.direccion[3] = "TAMAULIPAS, MÉXICO";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     }
                    /*
                    Av. Hidalgo No. 3402 , entre la calle Violeta y la calle Rosa en la Colonia Flores Tampico , Tamaulipas.
                    En el edificio Planta alta .
                    Entrada por Av. Hidalgo.
                    */
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
                 break;
             case TOLUCA:
                 try {
                     if (jsonInfo.getString("id_grupo_base").equals("03")) {
                         this.direccion[0] = "PASEO TOLLOCAN 101";
                         this.direccion[1] = "COL. SANTA MARIA DE LAS ROSAS";
                         this.direccion[2] = "CP. 50140, TOLUCA,";
                         this.direccion[3] = "ESTADO DE MÉXICO, MÉXICO";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     } else /*empresa 01*/ {
                         this.direccion[0] = "PASEO TOLLOCAN 101";
                         this.direccion[1] = "SANTA MARIA DE LAS ROSAS";
                         this.direccion[2] = "CP. 50140, TOLUCA,";
                         this.direccion[3] = "ESTADO DE MÉXICO, MÉXICO";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     }
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
                 break;
             case QUERETARO:
                     this.direccion[0] = "2DA PRIVADA DE 20 DE NOVIEMBRE ";
                     this.direccion[1] = "No.15; 2DO PISO; COL. SAN ";
                     this.direccion[2] = "FRANCISQUITO; C.P. 76058, ";
                     this.direccion[3] = "QUERÉTARO, QUERÉTARO, MÉXICO";
                     this.direccion[4] = "";
                     this.direccion[5] = "";
                     this.direccion[6] = "";
                 break;



             case IRAPUATO:
                 this.direccion[0] = "BLVD. DIAZ ORDAZ # 1370; M2; ";
                 this.direccion[1] = "INTERIOR 2, C.P. 36660, ";
                 this.direccion[2] = "JARDINES DE IRAPUATO, ";
                 this.direccion[3] = "IRAPUATO GUANAJUATO, MÉXICO";
                 this.direccion[4] = "";
                 this.direccion[5] = "";
                 this.direccion[6] = "";
                 break;



             case LEON:
                 this.direccion[0] = "NICARAGUA #917; LOCAL 12; ";
                 this.direccion[1] = "2DO NIVEL, VILLA RESIDENCIAL ";
                 this.direccion[2] = "ARBIDE, C.P. 37366, LEON, ";
                 this.direccion[3] = "GUANAJUATO, MÉXICO";
                 this.direccion[4] = "";
                 this.direccion[5] = "";
                 this.direccion[6] = "";
                 break;


             case SALAMANCA:
                 this.direccion[0] = "JUAN ALDAMA #1008; 2DO PISO,";
                 this.direccion[1] = "CENTRO, C.P. 36860, ";
                 this.direccion[2] = "SALAMANCA, GUANAJUATO, MÉXICO";
                 this.direccion[3] = "";
                 this.direccion[4] = "";
                 this.direccion[5] = "";
                 this.direccion[6] = "";
                 break;

                  /*
                DatabaseAssistant.insertCompany(
						"03",
						"",
						"Cooperativa",
						"GRUPO ASISTENCIAL MEMORY SCP de RL de C.V.",
						"PROLONGACIÓN HIDALGO #705-A, C.P. 38040, CENTRO, CELAYA, GUANAJUATO, MÉXICO",
						"4616158702 - 4616158703");
                 */
             case CELAYA:
                 this.direccion[0] = "PROLONGACIÓN HIDALGO #705-A";
                 this.direccion[1] = "CENTRO, C.P. 38040, ";
                 this.direccion[2] = "CELAYA, GUANAJUATO, MÉXICO";
                 this.direccion[3] = "";
                 this.direccion[4] = "";
                 this.direccion[5] = "";
                 this.direccion[6] = "";
                 break;

             case PUEBLA:
                 try{
                     if (jsonInfo.getString("id_grupo_base").equals("03")){// || jsonInfo.getString("id_grupo_base").equals("01")
                         this.direccion[0] = "FRANCISCO I. MADERO No. 428";
                         this.direccion[1] = "COL. SAN BALTAZAR CAMPECHE";
                         this.direccion[2] = "C.P. 72550, PUEBLA";
                         this.direccion[3] = "PUEBLA, MÉXICO";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     }
                     else{
                         this.direccion[0] = "FRANCISCO I. MADERO No. 428";
                         this.direccion[1] = "COL. SAN BALTAZAR CAMPECHE";
                         this.direccion[2] = "C.P. 72550, PUEBLA";
                         this.direccion[3] = "PUEBLA, MÉXICO";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     }
                 } catch (JSONException e){
                     e.printStackTrace();
                 }
                 break;
             case CUERNAVACA:
                 try{
                     if (jsonInfo.getString("id_grupo_base").equals("04")){// || jsonInfo.getString("id_grupo_base").equals("01")
                         this.direccion[0] = "FRANCISCO I. MADERO No. 719";
                         this.direccion[1] = "COL. MIRAVAL C.P. 62270.";
                         this.direccion[2] = "DELEGACIÓN BENITO JUÁREZ,";
                         this.direccion[3] = "CUERNAVACA, MÉXICO";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     }
                     else{
                         this.direccion[0] = "FRANCISCO I. MADERO No. 719";
                         this.direccion[1] = "COL. MIRAVAL C.P. 62270.";
                         this.direccion[2] = "DELEGACIÓN BENITO JUÁREZ,";
                         this.direccion[3] = "CUERNAVACA, MÉXICO";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     }
                 } catch (JSONException e){
                     e.printStackTrace();
                 }
                 break;
             case NAYARIT:
                 try{
                     if (jsonInfo.getString("id_grupo_base").equals("04")) {
                         this.direccion[0] = "AV. MEXICO N° 393 NTE.";
                         this.direccion[1] = "CALLE NIÑOS HÉROES Nº 82";
                         this.direccion[2] = "JAVIER MINA Nº 69 ORIENTE";
                         this.direccion[3] = "COL. CENTRO";
                         this.direccion[4] = "CP. 63000, TEPIC,";
                         this.direccion[5] = "NAYARIT, MEXICO";
                         this.direccion[6] = "TEL 212 1080 Y 216 9102";
                     } else {
                         this.direccion[0] = "AV. MEXICO N° 393 NTE.";
                         this.direccion[1] = "COL. CENTRO";
                         this.direccion[2] = "CP. 63000, TEPIC,";
                         this.direccion[3] = "NAYARIT, MEXICO";
                         this.direccion[4] = "TEL 212 1080 Y 216 9102";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     }
                 } catch (JSONException e){
                     e.printStackTrace();
                 }
                 break;
             case MERIDA:
                 try {
                     if (jsonInfo.getString("id_grupo_base").equals("01")) {
                         this.direccion[0] = "Calle 33 No. 503C";
                         this.direccion[1] = "Col. Alcala Martin";
                         this.direccion[2] = "CP 97000 Merida";
                         this.direccion[3] = "Yucatan, México";
                         this.direccion[4] = "TELS: 9201269 y 9201268";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     } else {
                         this.direccion[0] = "AV. MEXICO N° 393 NTE.";
                         this.direccion[1] = "COL. CENTRO";
                         this.direccion[2] = "CP. 63000, TEPIC,";
                         this.direccion[3] = "NAYARIT, MEXICO";
                         this.direccion[4] = "TELS 212 1080 Y 216 9102";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     }
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
                 break;
             case SALTILLO:
                 try {
                     if (jsonInfo.getString("id_grupo_base").equals("01")) {
                         this.direccion[0] = "BOULEVARD NAZARIO ORTIZ GARZA";
                         this.direccion[1] = "NO 1265 COL. ALPES";
                         this.direccion[2] = "CP 25270";
                         this.direccion[3] = "SALTILLO COAHUILA";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     } else {
                         this.direccion[0] = "BOULEVARD NAZARIO ORTIZ GARZA";
                         this.direccion[1] = "NO 1265 COL. ALPES";
                         this.direccion[2] = "CP 25270";
                         this.direccion[3] = "SALTILLO COAHUILA";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     }
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
                 break;
             case CANCUN:
                 try {
                     if (jsonInfo.getString("id_grupo_base").equals("01")) {
                         this.direccion[0] = "CALLE CEDRO MZ. 41";
                         this.direccion[1] = "LOTE 1-08";
                         this.direccion[2] = "SM. 311";
                         this.direccion[3] = " CANCÚN, QUINTANA ROO";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     } else {
                         this.direccion[0] = "CALLE CEDRO MZ. 41";
                         this.direccion[1] = "LOTE 1-08";
                         this.direccion[2] = "SM. 311";
                         this.direccion[3] = " CANCÚN, QUINTANA ROO";
                         this.direccion[4] = "";
                         this.direccion[5] = "";
                         this.direccion[6] = "";
                     }
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
                 break;
             case MEXICALI:
                     this.direccion[0] = "Blvd Anahuac #800 Int.A";
                     this.direccion[1] = "Col. Esperanza C.P. 21140";
                     this.direccion[2] = "Mexicali, B.C., MÉXICO";
                     this.direccion[3] = "";
                     this.direccion[4] = "";
                     this.direccion[5] = "";
                     this.direccion[6] = "";
                 break;

             case TORREON:
                 this.direccion[0] = "CLZ. MATIAS ROMAN RIOS # 425, ";
                 this.direccion[1] = "L-17, C.P 27000, PLAZA PABELLON ";
                 this.direccion[2] = "HIDALGO, TORREON COAHUILA, MÉXICO";
                 this.direccion[3] = "";
                 this.direccion[4] = "";
                 this.direccion[5] = "";
                 this.direccion[6] = "";
                 break;


             case MORELIA:
                 this.direccion[0] = "AV. GUADALUPE VICTORIA";
                 this.direccion[1] = "NO. 364 COL. CENTRO";
                 this.direccion[2] = "C.P. 58000 MORELIA, MICHOACAN";
                 this.direccion[3] = "";
                 this.direccion[4] = "";
                 this.direccion[5] = "";
                 this.direccion[6] = "";
                 break;
         }
     }

    /**
     * fills fields with the result of the coincidence (client's info)
     */
     private void llenarCampos() {
         EditText nombre = (EditText) getView().findViewById(R.id.et_nombre_apellidos);
         try {
             nombre.setText(jsonInfo.getString("nombre") + " "
                     + jsonInfo.getString("apellido_pat") + " "
                     + jsonInfo.getString("apellido_mat"));
         } catch (JSONException e) {
             e.printStackTrace();
         }
         EditText contrato = (EditText) getView().findViewById(R.id.et_no_contrato);
         try
         {
             contrato.setText(jsonInfo.getString("serie") + jsonInfo.getString("no_contrato"));

             //lanzar la verificacion de los datos de los meses de pago.----------------------------------------------------------------------------------------------------

                 idContrato = jsonInfo.getString("id_contrato");
                 no_contrato = jsonInfo.getString("no_contrato");
                 verificarCreditoPercapita(idContrato);

         } catch (JSONException e) {
             e.printStackTrace();
         }


         /**
          * Checks if it is the same contract number
          * and
          * checks if payment was made and printed (is synchronized)
          * receives false: is not synchronized
          * receives true: is synchronized
          */
         if ( !preferencesTicketPending.isThereAPaymentAndIsSynchronized() ) {

             /**
              * Checks if payment was made and printed (is synchronized)
              * receives false: is not synchronized
              * receives true: is synchronized
              */
             //if ( !preferencesTicketPending.isSynchronized() ) {

                 /**
                  * If it's not synchronized
                  * use folio from last payment
                  */

                 /**
                  * Gets last payment registered
                  * and checks if same contract number was chosen
                  */
                 JSONObject lastPayment = preferencesTicketPending.getPayment();

                 try {

                     if (lastPayment.getString(getString(R.string.key_contract_number)).equals(jsonInfo.getString("serie") + jsonInfo.getString("no_contrato"))) {

                         try {

                             /**
                              * Gets amount from last payment registered
                              * and disables EditText
                              */
                             //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
                             etAmount.setText(lastPayment.getString(getString(R.string.key_amount)));
                             etAmount.setEnabled(false);

                             pendingTicket = true;

                         } catch (JSONException e) {
                             e.printStackTrace();
                         }
                     }
                 } catch (JSONException e){
                     e.printStackTrace();
                 }
             //}
         }


         TextView ultimoAbono = (TextView) getView().findViewById(R.id.textViewUltimoAbono);
         try {
             if (jsonInfo.getString("tiempo").equals("null")) {
                 ultimoAbono.setText("");
             } else {
                 ultimoAbono.setText("Último abono "
                         + jsonInfo.getString("tiempo"));
             }
         } catch (JSONException e) {
             e.printStackTrace();
         }
         TextView direccion = (TextView) getView().findViewById(R.id.et_direccion);
         try {
             direccion.setText(jsonInfo.getString("calle_cobro") + " "
                     + jsonInfo.getString("no_ext_cobro") + " "
                     + jsonInfo.getString("colonia") + " "
                     + jsonInfo.getString("localidad") + " "
                     + jsonInfo.getString("entre_calles"));
         } catch (JSONException e) {
             e.printStackTrace();
         }

         //SqlQueryAssistant query = null;
         try {
             RadioButton radioPrograma = (RadioButton) getView().findViewById(R.id.radioPrograma);
             //query = new SqlQueryAssistant(this);
             tipoEmpresa = jsonInfo.getString("id_grupo_base");
             radioPrograma.setChecked(true);
             radioPrograma.setText( ExtendedActivity.getCompanyNameFormatted( DatabaseAssistant.getSingleCompany(tipoEmpresa).getName() ) );

         } catch (JSONException e) {
             e.printStackTrace();
         } finally {
             /*
             if (query.db != null){
                 query.db.close();
             }
             query = null;
             */
         }

     }















    public void verificarCreditoPercapita(String contratillo)
    {
        String montoPagoActualJSON="", SaldoJSON="", idContratoJSON="", noContratoJSON="", fechaPrimerAbonoJSON="", montoAtrasadoJSON="", abonadoJSON="";
        String numeroPagos = "", saldo = "", montoPagoActual = "", mensaje = "", montoConcatenado = "", tituloPercapita = "", mensajeCobradorPercapita = "";



        String arregloDatosCobrador[] = DatabaseAssistant.consultarDatosDeCobrador();
        if(arregloDatosCobrador.length>0)
        {
            mensaje = arregloDatosCobrador[0];
            tituloPercapita = arregloDatosCobrador[1];
            mensajeCobradorPercapita = arregloDatosCobrador[2];
            numeroPagos = arregloDatosCobrador[6];

            if(numeroPagos.equals("vacio"))
            {
                Log.d("IMPRESION --->", "Vacio");
            }
            else
            {
                try
                {
                    montoPagoActualJSON = jsonInfo.getString("monto_pago_actual");
                    SaldoJSON = jsonInfo.getString("Saldo");

                    //int saldillo = (int) Integer.parseInt(SaldoJSON.replace(".00", "").replace(".0", ""));
                    int montillo = (int) Integer.parseInt(montoPagoActualJSON.replace(".00", "").replace(".0", ""));

                    String saldoaAux = SaldoJSON.substring( 0, SaldoJSON.lastIndexOf("."));
                    int saldillo = Integer.parseInt(saldoaAux);

                    int diasResultado = saldillo / montillo;
                    //int diasResultado = Integer.parseInt(SaldoJSON.replace(".00", "")) / ((int) Integer.parseInt(montoPagoActualJSON.replace(".00", "")));
                    if (diasResultado <= Integer.parseInt(numeroPagos))
                    {
                        if(jsonInfo.getString("id_grupo_base").equals("05"))
                        {
                            Log.d("IMPRESION --->", "CONTRATO CON EMPRESA 05");
                        }
                        else
                        {
                            mostrarCreditoPercapita(etAmount, mensaje, mensajeCobradorPercapita, tituloPercapita);
                            mensajePercapita = mensaje;
                            tituloPercapitaGeneral = tituloPercapita;
                        }
                    }
                    else
                    {
                        Log.d("IMPRESION -- >", "NO ALCANZÓ LOS DIAS DE PAGO :(");
                    }

                }
                catch (JSONException ex)
                {
                    ex.printStackTrace();
                }
            }

        }
        else
        {
            Log.d("IMPRESION -- >", "NO SE RETORNARON DATOS");
        }



        /*List<Collectors> lista = Collectors.findWithQuery(Collectors.class, "SELECT * FROM COLLECTORS");
        if (lista.size() > 0)
        {
            numeroPagos = lista.get(0).getNumeropagos();
            mensaje = lista.get(0).getMensajeaviso();
            tituloPercapita = lista.get(0).getTitulopercapita();
            mensajeCobradorPercapita = lista.get(0).getMensajecobrador();
        } else {
            Util.Log.d("NO EXISTEN DIAS DE PAGOS PARA CREDITO PERCAPITA");
        }*/




        /*try
        {
            montoPagoActualJSON = jsonInfo.getString("monto_pago_actual");
            SaldoJSON = jsonInfo.getString("Saldo");

            int diasResultado = Integer.parseInt(SaldoJSON.replace(".00", "")) / ((int) Integer.parseInt(montoPagoActualJSON.replace(".00", "")));
            if (diasResultado <= Integer.parseInt(numeroPagos)) {
                mostrarCreditoPercapita(etAmount, mensaje, mensajeCobradorPercapita, tituloPercapita);
                mensajePercapita = mensaje;
                tituloPercapitaGeneral = tituloPercapita;
            }
            else
                {
                Util.Log.d("TODAVIA NO ALCANZA EL CREDITO DE PERCAPITA");
                Log.d("NO HAY CREDITO---->", "TODAVIA NO ALCANZA EL CREDITO DE PERCAPITA");
            }

        }
        catch (JSONException ex)
        {
            ex.printStackTrace();
        }*/
    }


    private void mostrarCreditoPercapita(View view, String mensaje, String mensajeCobradorPercapita, String tituloPercapita)
    {
        Button btEnterado;
        TextView tvMensaje, tvMensajeCobradorPercapita, tvTitulo;
        myDialog3.setContentView(R.layout.aviso_credito_percapita);
        myDialog3.setCancelable(true);
        btEnterado = (Button) myDialog3.findViewById(R.id.btEnterado);
        tvMensaje = (TextView) myDialog3.findViewById(R.id.tvMensaje);
        tvTitulo = (TextView) myDialog3.findViewById(R.id.tvTitulo);
        tvMensajeCobradorPercapita = (TextView) myDialog3.findViewById(R.id.tvMensajeCobradorPercapita);
        tvMensaje.setText(mensaje);
        tvMensajeCobradorPercapita.setText(mensajeCobradorPercapita);
        tvTitulo.setText(tituloPercapita);

        btEnterado.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog3.dismiss();
            }
        });

        myDialog3.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog3.show();
    }















































    /**
     * sets listener of resources on screen as
     * amount resources
     * - $50.00
     * - $75.00
     * - $100.00
     * - $200.00
     * - $400.00
     * account status
     * visit
     * imageviews treated as buttons
     *
     * when one of the amount resources touched, puts on the aomunt field the amount
     * of the resource touched and changes its image so that it looks as pressed
     */
     void setListenersOpciones() {
         final ImageView op1 = (ImageView) getView().findViewById(R.id.imageViewOp1);
         final ImageView op2 = (ImageView) getView().findViewById(R.id.imageViewOp2);
         final ImageView op3 = (ImageView) getView().findViewById(R.id.imageViewOp3);
         final ImageView op4 = (ImageView) getView().findViewById(R.id.imageViewOp4);
         final ImageView op5 = (ImageView) getView().findViewById(R.id.imageViewOp5);
         final ImageView op6 = (ImageView) getView().findViewById(R.id.imageViewOp6);
         final ImageView op7 = (ImageView) getView().findViewById(R.id.imageViewOp7);
         ImageView ivEstado = (ImageView) getView().findViewById(R.id.ivEstado);
         ImageView imgSuspension = (ImageView) getView().findViewById(R.id.imgSuspesion);
         btNoAporto = (Button) getView().findViewById(R.id.btNoAporto);

         //sets it as pressed
         op1.setOnClickListener(view -> {
             if (!pendingTicket) {
                 ((EditText) getView().findViewById(R.id.et_cantidad_aportacion))
                         .setText("50");
                 if (selected != 1) {
                     selectOption(1);
                 } else {
                     selectOption(0);
                 }
             }
         });
         //sets it as pressed
         op2.setOnClickListener(view -> {
             if (!pendingTicket) {
                 ((EditText) getView().findViewById(R.id.et_cantidad_aportacion))
                         .setText("100");
                 if (selected != 2) {
                     selectOption(2);
                 } else {
                     selectOption(0);
                 }
             }
         });
         //sets it as pressed
         op3.setOnClickListener(view -> {
             if (!pendingTicket) {
                 ((EditText) getView().findViewById(R.id.et_cantidad_aportacion))
                         .setText("200");
                 if (selected != 3) {
                     selectOption(3);
                 } else {
                     selectOption(0);
                 }
             }
         });
         //sets it as pressed
         op4.setOnClickListener(view -> {
             if (!pendingTicket) {
                 ((EditText) getView().findViewById(R.id.et_cantidad_aportacion))
                         .setText("400");
                 if (selected != 4) {
                     selectOption(4);
                 } else {
                     selectOption(0);
                 }
             }
         });
         //sets it as pressed
         op5.setOnClickListener(view -> {
             if (!pendingTicket) {
                 ((EditText) getView().findViewById(R.id.et_cantidad_aportacion))
                         .setText("75");
                 if (selected != 5) {
                     selectOption(5);
                 } else {
                     selectOption(0);
                 }
             }
         });
         //sets it as pressed
         op6.setOnClickListener(view -> {
             if (!pendingTicket) {
                 ((EditText) getView().findViewById(R.id.et_cantidad_aportacion))
                         .setText("150");
                 if (selected != 6) {
                     selectOption(6);
                 } else {
                     selectOption(0);
                 }
             }
         });
         //sets it as pressed
         op7.setOnClickListener(view ->
         {
             if (!pendingTicket) {
                 ((EditText) getView().findViewById(R.id.et_cantidad_aportacion))
                         .setText("300");
                 if (selected != 7) {
                     selectOption(7);
                 } else {
                     selectOption(0);
                 }
             }
         });


         imgSuspension.setOnClickListener(new OnClickListener() {
             @Override
             public void onClick(View v)
             {

                 String sintax = "SELECT * FROM NOTICE";
                 List<Notice> listNotice = Notice.findWithQuery(Notice.class, sintax);
                 if(listNotice.size()>0)
                 {
                     String tiketSuspencionTEXTO = listNotice.get(0).getTiketsuspencion();
                     if(tiketSuspencionTEXTO.equals("vacio"))
                     {
                         Toast.makeText(mainActivity, "Lo sentimos no existe mensaje de suspensión de pagos", Toast.LENGTH_LONG).show();
                     }
                     else
                     {
                         EditText nombre = (EditText) getView().findViewById(R.id.et_nombre_apellidos);
                         EditText contrato = (EditText) getView().findViewById(R.id.et_no_contrato);
                         TextView direccion = (TextView) getView().findViewById(R.id.et_direccion);
                         EditText aportacion = (EditText) getView().findViewById(R.id.et_cantidad_aportacion);
                         if (nombre.getText().toString().length() > 0 && contrato.getText().toString().length() > 0 && direccion.getText().toString().length() > 0 && mainActivity.isConnected)
                         {
                             try
                             {
                                 if(jsonInfo.getString("estatus").equals("1"))
                                 {
                                     mostrarAvisoSuspencionTemporalDePagos(etAmount);
                                 }
                                 else
                                 {
                                     Toast.makeText(mainActivity, "Lo sentimos este contrato no se encuentra activo", Toast.LENGTH_SHORT).show();
                                     Toast.makeText(mainActivity, "Fecha de reactivación: "+ jsonInfo.getString("fecha_reactiva"), Toast.LENGTH_LONG).show();
                                 }
                             }catch (JSONException e)
                             {
                                 e.printStackTrace();
                             }

                         }
                         else {
                             // Toast.makeText(mainActivity, "Suspensión no disponible por el momento", Toast.LENGTH_SHORT).show();

                             AlertDialog.Builder internetAlert = getAlertDialogBuilder()
                                     .setMessage("Suspensión no disponible, selecciona un cliente y verifica tu conexión a internet")
                                     .setPositiveButton(R.string.accept, null)
                                     .setNegativeButton(R.string.salir, (dialog1, which) -> {
                                         mthis = null;
                                         if (MainActivity.CURRENT_TAG.equals(MainActivity.TAG_APORTACION))
                                             getFragmentManager().popBackStack();
                                     });
                             internetAlert.setCancelable(false);
                             internetAlert.create().show();
                         }
                     }
                 }
             }
         });

         //shows a preview of the account status ticket
         ivEstado.setOnClickListener(view -> {
             if (((TextView) getView().findViewById(R.id.et_direccion)).length() > 0) {
                 final Dialog secondDialog = getDialog();
                 SimpleDateFormat dateFormat = new SimpleDateFormat(
                         "dd/MM/yyyy");
                 Date date = new Date();

                 secondDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                 secondDialog.setContentView(R.layout.previewsaldo);
                 secondDialog.setCancelable(true);
                 secondDialog.getWindow().setBackgroundDrawable(
                         new ColorDrawable(
                                 Color.TRANSPARENT));
                 secondDialog.show();
                 TextView contrato = (TextView) secondDialog
                         .findViewById(R.id.textViewContrato);
                 TextView titulor = (TextView) secondDialog
                         .findViewById(R.id.textViewTitulor);
                 TextView fecha = (TextView) secondDialog
                         .findViewById(R.id.textViewFecha);
                 TextView saldo = (TextView) secondDialog
                         .findViewById(R.id.textViewSaldo);
                 TextView nombre = (TextView) secondDialog
                         .findViewById(R.id.textViewNombre);
                 try {
                     contrato.setText(
                             jsonInfo.getString("serie")
                                     + jsonInfo.getString("no_contrato"));
                     titulor.setText(jsonInfo.getString("nombre") + " "
                             + jsonInfo.getString("apellido_pat") + " "
                             + jsonInfo.getString("apellido_mat"));
                     fecha.setText(dateFormat.format(date));
                     saldo.setText(jsonInfo.getString("Saldo"));
                     nombre.setText(datosCobrador.getString("nombre"));

                 } catch (JSONException e) {
                     e.printStackTrace();
                 }

                 final TextView imprimir = (TextView) secondDialog
                         .findViewById(R.id.textViewImprimir);
                 imprimir.setOnClickListener(v -> {
                     final LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                     final boolean isGPSEnabled = locationManager
                             .isProviderEnabled(LocationManager.GPS_PROVIDER);
                     if (isGPSEnabled) {
                         mthis = Aportacion.this;
                         imprimir.setBackgroundColor(Color.WHITE);
                         //AsyncPrint asyncSaldo = getAsyncPrintCaseSeven();
                         //asyncSaldo.execute();
                         //secondDialog.dismiss();
                         if (!mainActivity.isFinishing()) {
                             if (secondDialog != null && secondDialog.isShowing()) {
                                 secondDialog.dismiss();
                             }
                         }
                         executePrintAccountStatus();
                     } else {
                         Toast.makeText(getContext(),
                                 "Enciende el GPS para continuar.",
                                 Toast.LENGTH_LONG).show();
                     }
                 });
                         /*
                         imprimir.setOnTouchListener(new OnTouchListener() {

                             @Override
                             public boolean onTouch(View arg0, MotionEvent arg1) {
                                 if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
                                     imprimir.setBackgroundColor(Color.BLUE);
                                     return true;
                                 }
                                 if (arg1.getAction() == MotionEvent.ACTION_UP) {
                                     final LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                                     final boolean isGPSEnabled = locationManager
                                             .isProviderEnabled(LocationManager.GPS_PROVIDER);
                                     if (isGPSEnabled) {
                                         mthis = Aportacion.this;
                                         imprimir.setBackgroundColor(Color.WHITE);
                                         AsyncPrint asyncSaldo = getAsyncPrintCaseSeven();
                                         asyncSaldo.execute();
                                         secondDialog.dismiss();
                                         return true;
                                     } else {
                                         Toast.makeText(getApplicationContext(),
                                                 "Enciende el GPS para continuar.",
                                                 Toast.LENGTH_LONG).show();
                                         return false;
                                     }

                                 }
                                 return false;
                             }

                         });
                         */
                 final TextView cancelar = (TextView) secondDialog
                         .findViewById(R.id.textViewCancelar);
                 cancelar.setOnClickListener(v -> secondDialog.dismiss());
             }
         });
         //shows a list of reasons of why is trying to make a visit
         btNoAporto.setOnTouchListener((view, event) -> {
             if (((TextView) getView().findViewById(R.id.et_direccion)).length() > 0) {
                 if (!pendingTicket) {
                     if (event.getAction() == MotionEvent.ACTION_DOWN) {
                         btNoAporto.setBackgroundResource(R.drawable.no_aporto_pressed);
                         return true;
                     }
                     if (event.getAction() == MotionEvent.ACTION_UP) {
                         final LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                         final boolean isGPSEnabled = locationManager
                                 .isProviderEnabled(LocationManager.GPS_PROVIDER);
                         if (isGPSEnabled) {
                             btNoAporto.setBackgroundResource(R.drawable.no_aporto);
                             dialog = getRazonesDialog();
                             dialog.getWindow().setBackgroundDrawable(
                                     new ColorDrawable(
                                             Color.TRANSPARENT));
                             dialog.show();
                         } else {
                             Toast.makeText(getContext(),
                                     "Activa el GPS para continuar.",
                                     Toast.LENGTH_SHORT).show();
                         }

                     }
                     return false;
                 }
             }

             return false;
         });
     }

    /**
     * prints account status
     * @return
     */
    public boolean printAccountStatus() {
        try {
            Looper.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //TSCActivity TscDll = new TSCActivity();
        //SqlQueryAssistant query = new SqlQueryAssistant(this);
        String mac = DatabaseAssistant.getPrinterMacAddress();
        /*
        if (query.db != null){
            query.db.close();
        }
        query = null;
        */

        boolean blue = true;
        int intentos = 0;
        while (blue && intentos < 100) {

            TSCActivity TscDll = new TSCActivity();

            try {

                dateAux = new Date().getTime();

                //timer that closes printer port if got stuck
                //trying to print
                //has one minute of limit before closing port
                //closing port causes NullPointerException on openPort()
                //so it tries again to print
                timer = new Timer();
                timer.schedule(new TimerTask() {

                    String mac;
                    int attempt;
                    TSCActivity tscDll;

                    @Override
                    public void run() {
                        if (checkDate()) {
                            Util.Log.ih("Closing port");
                            //disableBluetooth();
                            try {
                                tscDll.closeport();
                            } catch (Exception e) {
                                e.printStackTrace();
                                timer.cancel();
                            }
                        }
                        Util.Log.ih("attempt: " + attempt);
                    }

                    public TimerTask setData(int attempt, TSCActivity tscDll) {
                        this.attempt = attempt;
                        this.tscDll = tscDll;
                        return this;
                    }
                }.setData(intentos, TscDll), (1000 * 60));

                TscDll.openport(mac);
                Util.Log.ih("despues de openPort");
                timer.cancel();

                //Log.i("PRINT STATUS", TscDll.status());

                TscDll.setup(Printer.width,
                        50,
                        Printer.speed,
                        Printer.density,
                        Printer.sensorType,
                        Printer.gapBlackMarkVerticalDistance,
                        Printer.gapBlackMarkShiftDistance);

                TscDll.clearbuffer();
                TscDll.sendcommand("GAP 0,0\n");
                TscDll.sendcommand("CLS\n");
                TscDll.sendcommand("CODEPAGE UTF-8\n");
                TscDll.sendcommand("SET TEAR ON\n");
                TscDll.sendcommand("SET COUNTER @1 1\n");

                for (int conter = 0; conter < 600; conter = conter + 30) {
                    TscDll.printerfont(conter, 10, "3", 0, 1, 1, "-");
                }

                TscDll.printerfont(10, 55, "3", 0, 1, 1, jsonInfo.getString("serie")
                        + jsonInfo.getString("no_contrato"));
                String nombre = jsonInfo.getString("nombre") + " "
                        + jsonInfo.getString("apellido_pat") + " "
                        + jsonInfo.getString("apellido_mat");
                TscDll.printerfont(10, 100, "3", 0, 1, 1, nombre);
                final SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "dd/MM/yyyy");
                Date date = new Date();
                Log.i("PRINT STATUS", TscDll.status());

                TscDll.printerfont(10, 145, "3", 0, 1, 1,
                        "El saldo de su contrato al día ");
                TscDll.printerfont(10, 190, "3", 0, 1, 1,
                        dateFormat.format(date) + " a las 6 am, es de $"
                                + jsonInfo.getString("Saldo"));
                TscDll.printerfont(180, 240, "3", 0, 1, 1, "Atentamente");
                String nombre_cobrador = datosCobrador.getString("nombre");
                TscDll.printerfont(50, 285, "2", 0, 1, 1, nombre_cobrador);

                Log.i("PRINT STATUS", TscDll.status());
                TscDll.printlabel(1, 1);
                Log.i("PRINT STATUS", TscDll.status());
                //Log.e("saldo imprimiendo",
                //        "aaaa" + jsonInfo.getString("Saldo"));
                TscDll.closeport();

                return true;
            } catch (Exception e) {

                Util.Log.ih("intento = " + intentos);
                enableBluetooth();

                intentos++;
                e.printStackTrace();
            }
        }
        Toast.makeText(getContext(),
                getString(R.string.error_message_printing), Toast.LENGTH_SHORT)
                .show();
        return false;
    }

    /**
     * checks if more than 59 minutes have passed
     * @return
     */
    public boolean checkDate() {
        long newTime = new Date().getTime();
        long differences = Math.abs(dateAux - newTime);
        return (differences > (1000 * 59));
    }

    /**
     * disables bluetooth
     */
    public void disableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
                .getDefaultAdapter();
        mBluetoothAdapter.disable();
    }

    /**
     * enables bluetooth
     */
    public void enableBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Util.Log.ih("enabling bluetooth");
            mBluetoothAdapter.enable();
        }
    }

    /**
     * sets default amounts as pressed
     * @param selected default amount pressed
     */
     void selectOption(int selected) {
         this.selected = selected;
         final ImageView op1 = (ImageView) getView().findViewById(R.id.imageViewOp1);
         final ImageView op2 = (ImageView) getView().findViewById(R.id.imageViewOp2);
         final ImageView op3 = (ImageView) getView().findViewById(R.id.imageViewOp3);
         final ImageView op4 = (ImageView) getView().findViewById(R.id.imageViewOp4);
         final ImageView op5 = (ImageView) getView().findViewById(R.id.imageViewOp5);
         final ImageView op6 = (ImageView) getView().findViewById(R.id.imageViewOp6);
         final ImageView op7 = (ImageView) getView().findViewById(R.id.imageViewOp7);
         op1.setImageResource(R.drawable.btn_50);
         op2.setImageResource(R.drawable.btn_100);
         op3.setImageResource(R.drawable.btn_200);
         op4.setImageResource(R.drawable.btn_400);
         op5.setImageResource(R.drawable.btn_75);
         op6.setImageResource(R.drawable.btn_150);
         op7.setImageResource(R.drawable.btn_300_middle);
         switch (selected) {
             case 1:
                 op1.setImageResource(R.drawable.btn_50_pressed);
                 break;
             case 2:
                 op2.setImageResource(R.drawable.btn_100_pressed);
                 break;
             case 3:
                 op3.setImageResource(R.drawable.btn_200_pressed);
                 break;
             case 4:
                 op4.setImageResource(R.drawable.btn_400_pressed);
                 break;
             case 5:
                 op5.setImageResource(R.drawable.btn_75_pressed);
                 break;
             case 6:
                 op6.setImageResource(R.drawable.btn_150_pressed);
                 break;
             case 7:
                 op7.setImageResource(R.drawable.btn_300_pressed_middle);
                 break;
             default:
                 break;
         }
     }

    /**
     * deprecated in 3.0
     */
     private void habilitarCampos() {
         getView().findViewById(R.id.et_nombre_apellidos).setEnabled(true);
         getView().findViewById(R.id.et_no_contrato).setEnabled(true);
         getView().findViewById(R.id.et_direccion).setEnabled(true);

         RadioButton radioPrograma = (RadioButton) getView().findViewById(R.id.radioPrograma);

         // radioPrograma.setEnabled(true);
         // radioMalba.setEnabled(true);
         radioPrograma.setText("");
         radioPrograma.setChecked(false);
     }

    /**
     * disables name field, contract number field and address field
     * after chosing a coincidence when looking for a client on the server
     */
     private void inhabilitarCampos() {
         EditText nombre = (EditText) getView().findViewById(R.id.et_nombre_apellidos);
         EditText contrato = (EditText) getView().findViewById(R.id.et_no_contrato);
         TextView direccion = (TextView) getView().findViewById(R.id.et_direccion);

         nombre.setEnabled(false);
         contrato.setEnabled(false);
         direccion.setEnabled(false);

         RadioButton radioPrograma = (RadioButton) getView().findViewById(R.id.radioPrograma);

         radioPrograma.setEnabled(false);
     }

     /**
      * Este metodo verifica si los campos estan llenos TODO validate that the
      * actual views contains the object, to make sure send the view
      *
      * @return Un booleano con la respuesta
      */
     private boolean camposLlenos() {
         boolean isValido = false;
         EditText nombre = (EditText) getView().findViewById(R.id.et_nombre_apellidos);
         EditText contrato = (EditText) getView().findViewById(R.id.et_no_contrato);
         TextView direccion = (TextView) getView().findViewById(R.id.et_direccion);
         EditText aportacion = (EditText) getView().findViewById(R.id.et_cantidad_aportacion);

         if ((nombre.getText().toString().length() > 0)
                 && (nombre.getText().toString().length() > 0)
                 && (contrato.getText().toString().length() > 0)
                 && (direccion.getText().toString().length() > 0)
                 && (aportacion.getText().toString().length() > 0)) {
             isValido = true;

         }
         return isValido;
     }

     /**
      * deprecated in 3.0
      */
     public void manage_CobroFueraCartera(final JSONObject json) {
         if (json.has("result")) {
             try {
                 mensaje = json.getJSONObject("result").getString("mensaje");

             } catch (JSONException e1) {
                 e1.printStackTrace();
             }
             try {
                 no_cobro = json.getJSONObject("result").getJSONObject("cobro")
                         .getString("no_cobro");
             } catch (JSONException e) {

                 e.printStackTrace();
             }
         }
     }

    /**
     * deprecated in 3.0
     * @param caso
     */
     public void mostrarMensajeError(final int caso) {
         AlertDialog.Builder alert = getAlertDialogBuilder();
         alert.setMessage("Hubo un error de comunicación con el servidor, verifique sus ajustes de red o intente más tarde.");
         alert.setPositiveButton("Salir", (dialog1, which) -> {
             mthis = null;
             //finish();
             getFragmentManager().popBackStack();
         });
         alert.setNegativeButton("Reintentar", (dialog1, which) -> {
             if (!mainActivity.isConnected) {
                 mostrarMensajeError(caso);
             }
         });
         alert.create();
         alert.setCancelable(false);
         alert.show();
     }

    /**
     * writes payment into database
     * writes cash plus amount of ongoing payment into sharedPreferences file
     * writes new folio with the company into sharePreferences file
     */
     public boolean guardarPago() {
         boolean paymentSaved = false;
         int attempts = 0;
         //do {
             try
             {
                 Util.Log.ih("guardando pago...");

                 String empresa = "0";
                 RadioButton radioPrograma = (RadioButton) getView().findViewById(R.id.radioPrograma);
                 double lat = 20.682413;
                 SimpleDateFormat dateFormat = new SimpleDateFormat(
                         "yyyy-MM-dd HH:mm:ss");
                 final Date date = new Date();
                 double longi = -103.381570;
                 //EditText editText = (EditText) findViewById(R.id.et_cantidad_aportacion);
                 EditText contrato = (EditText) getView().findViewById(R.id.et_no_contrato);
                 //SqlQueryAssistant query = new SqlQueryAssistant(this);

                 //Util.Log.ih("gpsProvider = " + gpsProviderAportacion.isConnected);//will return false because was just created

                 if ( ApplicationResourcesProvider.getLocationProvider().getGPSPosition() != null ) {

                     mainActivity.log("lat: "
                             + ApplicationResourcesProvider.getLocationProvider().getGPSPosition()
                             .getLatitude(), "long: "
                             + ApplicationResourcesProvider.getLocationProvider().getGPSPosition()
                             .getLongitude());

                     lat = ApplicationResourcesProvider.getLocationProvider().getGPSPosition()
                             .getLatitude();
                     longi = ApplicationResourcesProvider.getLocationProvider().getGPSPosition()
                             .getLongitude();
                 }
                 else // si el metodo nos retorna nullos
                 {
                     lat = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLatitude();
                     longi = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLongitude();
                     ApplicationResourcesProvider.setLastKnownLocation(lat, longi);
                 }
                 /*
                 else if (gpsProviderAportacion.getPosicion() != null) {
                     Util.Log.ih("getting location from gpsProviderAportacion");
                     lat = gpsProviderAportacion.getPosicion().getLatitude();
                     longi = gpsProviderAportacion.getPosicion().getLongitude();
                 }
                 */

                 if (radioPrograma.isChecked()) {
                     empresa = tipoEmpresa;

                 }
                 String seriecobrador = "";
                 try {

                     switch (com.jaguarlabs.pabs.util.BuildConfig.getTargetBranch()) {
                         case NAYARIT:case MEXICALI:case MORELIA: case TORREON:
                             if (jsonInfo.getString("id_grupo_base").equals("01")) {
                                 seriecobrador = datosCobrador
                                         .getString("serie_programa");
                             } else if (jsonInfo.getString("id_grupo_base").equals("03")) {
                                 seriecobrador = datosCobrador
                                         .getString("serie_malba");
                             } else if (jsonInfo.getString("id_grupo_base").equals("04")) {
                                 seriecobrador = datosCobrador
                                         .getString("serie_empresa4");
                             } else {
                                 //seriecobrador = datosCobrador.getString(
                                 //      DatabaseAssistant.getSingleCompany(empresa).getName());
                             }
                             break;
                         default:
                             if (jsonInfo.getString("id_grupo_base").equals("01")) {
                                 seriecobrador = datosCobrador.getString("serie_programa");
                             } else {
                                 seriecobrador = datosCobrador
                                         .getString(
                                                 DatabaseAssistant.getSingleCompany(jsonInfo.getString("id_grupo_base")).getName());
                                 //query.mostrarEmpresa(
                                 //jsonInfo.getString("id_grupo_base"))
                                 //.getString(Empresa.nombre_empresa));

                             }
                     }



                     if (seriecobrador.equals("3M") && new_folio.equals("71935"))
                     {
                         new_folio = "72155";
                     }
                     else if (seriecobrador.equals("3M,") && new_folio.equals("24280"))
                     {
                         new_folio = "24410";
                     }
                     else if (seriecobrador.equals("V1") && new_folio.equals("59211"))
                     {
                         new_folio = "59291";
                     }
                     else if (seriecobrador.equals("6C") && new_folio.equals("63063"))
                     {
                         new_folio = "63071";
                     }
                     else if (seriecobrador.equals("1A1") && new_folio.equals("96874"))
                     {
                         new_folio = "96896";
                     }
                 } catch (JSONException e) {
                     paymentSaved = false;
                     e.printStackTrace();
                     seriecobrador = "=D";
                 }

                 if (DatabaseAssistant.checkIfPaymentExists(new_folio, seriecobrador))
                 {
                     AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);

                     builder.setTitle("Folio");
                     builder.setMessage("El folio se encuentra repetido");
                     builder.setNeutralButton("Aceptar",(dialog1, which) -> {
                         if (dialog != null && dialog.isShowing())
                             dialog.dismiss();
                         getFragmentManager().popBackStack();
                     });
                     builder.setCancelable(false);

                     builder.show();

                     return false;
                 }

                 String no_cliente = contrato.getText().toString();
                 String serie = jsonInfo.getString("serie");

                 if (no_cliente.contains(serie)) {
                     no_cliente = no_cliente.substring(serie.length());
                 }

                 String id_cliente = jsonInfo.getString("serie") + jsonInfo.getString("no_contrato");
                 String nombreEmpresa = DatabaseAssistant.getSingleCompany(jsonInfo.getString("id_grupo_base")).getName();


                 try {
                     DecimalFormat df = new DecimalFormat();
                     df.setMaximumFractionDigits(2);
                     df.setGroupingUsed(false);



                     DatabaseAssistant.insertPayment(
                             empresa,
                             "" + lat,
                             "" + longi,
                             "1",
                             datosCobrador.getString("no_cobrador"),
                             df.format(Float.parseFloat(amount)),
                             no_cliente,
                             dateFormat.format(date),
                             jsonInfo.getString("serie"),
                             new_folio,
                             seriecobrador,
                             jsonInfo.getString("tipo_bd"),
                             mainActivity.getEfectivoPreference().getInt("periodo", 0),
                             paymentReferenceString,
                             ""+ DatabaseAssistant.setSaldoPorCobro(jsonInfo.getDouble("Saldo"), Float.parseFloat(amount)),
                             "Fuera de cartera"
                     );
                     //*********actualizacion de cobrosMasCancelados
                     float cobrosMasCanceladosMasMontoACobrar = DatabaseAssistant.getTotalCobrosPorCompania(empresa) + Float.valueOf(amount);
                     String queryActualizacion="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET cobrosMasCancelados ='"+ cobrosMasCanceladosMasMontoACobrar +"' WHERE periodo='"+mainActivity.getEfectivoPreference().getInt("periodo", 0)+"' and empresa='"+ empresa+"'";
                     MontosTotalesDeEfectivo.executeQuery(queryActualizacion);

                     //*********actualizacion de totalCobrosMenosDepositos
                     float total_Cobros_Menos_Depositos_Menos_Monto_A_Cobrar = DatabaseAssistant.getTotalCobrosMenosDepositosPorEmpresa(empresa) - Float.valueOf(amount);
                     String queryActualizacion2="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET totalCobrosMenosDepositos ='"+ total_Cobros_Menos_Depositos_Menos_Monto_A_Cobrar +"' WHERE periodo='"+mainActivity.getEfectivoPreference().getInt("periodo", 0)+"' and empresa='"+ empresa +"'";
                     MontosTotalesDeEfectivo.executeQuery(queryActualizacion2);

                     //*********actualizacion de cobrosMenosDepositos
                     double cobros_Menos_Depositos_Mas_Monto_A_Cobrar_Menos_Depositos = (DatabaseAssistant.getCobrosMenosDepositosPorEmpresa(empresa) + Float.valueOf(amount) - DatabaseAssistant.getTotalDepositsByCompany(empresa)) ;//depositos);
                     String queryActualizacion3="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET cobrosMenosDepositos ='"+ cobros_Menos_Depositos_Mas_Monto_A_Cobrar_Menos_Depositos +"' WHERE periodo='"+mainActivity.getEfectivoPreference().getInt("periodo", 0)+"' and empresa='"+ empresa +"'";
                     MontosTotalesDeEfectivo.executeQuery(queryActualizacion3);

                 } catch (Exception e){
                     e.printStackTrace();
                     Util.Log.ih("No se guardo el pago");

                     return false;
                 }



                 //double efectivo = DatabaseAssistant.getTotalEfectivoAcumulado();
                 //cashPreferenceBeforeChange = efectivo;
                 double nuevoEfectivo = DatabaseAssistant.getTotalEfectivoAcumulado() + Double.parseDouble(amount);
                 DecimalFormat df = new DecimalFormat();
                 df.setMaximumFractionDigits(2);
                 df.setGroupingUsed(false);

                 mainActivity.getEfectivoEditor().putFloat(empresaString, Float.parseFloat(df.format(nuevoEfectivo))).apply();
                 // /TODO change harcoded string by final strings
                 SharedPreferences preference = getContext().getSharedPreferences("folios",
                         Context.MODE_PRIVATE);
                 Editor editor = preference.edit();

                 try {
                     editor.putString(nombreEmpresa, new_folio);
                     editor.apply();
                 } catch (Exception e) {
                     paymentSaved = false;
                     e.printStackTrace();
                     Util.Log.ih("No se guardo el pago");

                     return false;
                 } finally {
                     /*
                     if (query.db != null){
                         query.db.close();
                     }
                     query = null;
                     */
                 }

                 //String monto = amount;
                 //String id_cliente = jsonInfo.getString("serie") + jsonInfo.getString("no_contrato");

                 //save payment into SharedPreference file
                 savePaymentToSharedPreferences(new_folio,  String.valueOf(df.format(Float.parseFloat(amount))), id_cliente);

                 paymentSaved = true;

                 readyToPrint = true;

                 Util.Log.ih("PaymentSaved = " + paymentSaved);
                 Util.Log.ih("se guardo el pago");

                 return true;
             } catch (JSONException e) {

                 Util.Log.ih("No se guardo el pago");

                 //write stacktrace into a .txt file
                 //StackTraceHandler writeStackTrace = new StackTraceHandler();
                 //writeStackTrace.uncaughtException("APORTACION.GuardarPago", e);

                 paymentSaved = false;
                 attempts++;
                 e.printStackTrace();

                 return false;
             }

         //}while(!paymentSaved && attempts < 5);

     }

    /**
     * This method saves important info from ongoing payment in another
     * sharedPreferences file, so that if something goes wrong during dialog_copies ticket (dialog_copies failed
     * for some reason), it will be possible to print that ticket again, chosing account from Clientes
     * activity and pressing 'print'
     * @param folio folio from ongoing payment
     * @param monto amount from ongoing payment
     * @param id_cliente cliente id from ongoing payment
     */
     public void savePaymentToSharedPreferences( String folio, String monto, String id_cliente ){
        preferencesTicketPending.savePayment(folio, monto, id_cliente);
     }

     //Dialogo para imprimir copia *CABG
     @SuppressLint("SimpleDateFormat")
     public void showSecondPrintingDialog() {
         mainActivity.runOnUiThread(() -> {
             Log.e("runnable", "entrando al runnable");

             copies++;

             if (copies > 1) {
                 try {
                     Log.e("saliendo", "de aportación fuera de cartera");

                     Intent intent = new Intent();
                     double cantidad = datosCobrador.getDouble("efectivo");
                     EditText editText = (EditText) getView().findViewById(R.id.et_cantidad_aportacion);
                     cantidad = cantidad
                             + Double.parseDouble(editText.getText()
                             .toString());
                     intent.putExtra("nuevo_efectivo", "" + cantidad);
                     //setResult(RESULT_OK, intent);
                     fragmentResult.onFragmentResult(requestCode, Activity.RESULT_OK, intent);
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
                 mthis = null;
                 //finish();
                 getFragmentManager().popBackStack();
             } else {

                 Log.e("dialogo", "de aportación fuera de cartera");

                 final Dialog newsecondDialog = getDialog();
                 newsecondDialog
                         .requestWindowFeature(Window.FEATURE_NO_TITLE);
                 newsecondDialog.setContentView(R.layout.dialog_copies);
                 newsecondDialog.setCancelable(false);
                 newsecondDialog.show();
                 TextView text = (TextView) newsecondDialog
                         .findViewById(R.id.textView2);
                 text.setText("¿Deseas imprimir una copia?");
                 TextView textc = (TextView) newsecondDialog
                         .findViewById(R.id.textViewCancel);
                 textc.setText("No");
                 textc.setGravity(Gravity.CENTER_HORIZONTAL);
                 textc.setVisibility(View.VISIBLE);
                 textc.setOnClickListener(view -> {
                     try {
                         Intent intent = new Intent();
                         double cantidad = datosCobrador
                                 .getDouble("efectivo");
                         EditText editText = (EditText) getView().findViewById(R.id.et_cantidad_aportacion);
                         cantidad = cantidad
                                 + Double.parseDouble(editText.getText()
                                 .toString());
                         intent.putExtra("nuevo_efectivo", "" + cantidad);
                         //setResult(RESULT_OK, intent);
                         fragmentResult.onFragmentResult(requestCode, Activity.RESULT_OK, intent);
                         newsecondDialog.dismiss();
                         mthis = null;
                         //finish();
                         getFragmentManager().popBackStack();

                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                     mthis = null;
                     //finish();
                     getFragmentManager().popBackStack();
                 });

                 TextView test = (TextView) newsecondDialog
                         .findViewById(R.id.textViewAceptar);
                 test.setText("Sí");
                 test.setGravity(Gravity.CENTER_HORIZONTAL);
                 test.setOnClickListener(view -> {
                     newsecondDialog.dismiss();
                     executePrintPaymentTicket();
                     //							AsyncPrint obj = new AsyncPrint(6, mthis,
                     //									new ProgressDialog(mthis));
                     //							obj.execute();
                 });
             }
         });
     }

    /**
     * Gets context from this class.
     * In some cases you cannot just call 'this', because another context is envolved
     * or you want a reference to an object of this class from another class,
     * then this context is helpful.
     *
     * NOTE
     * Not recommended, it may cause NullPointerException though
     *
     * @return context from Aportacion
     */
     public static Aportacion getMthis() {
         return mthis;
     }

    /**
     * Sets 'mthis' global variable as Aportacion's context
     * @param mthis Aportacion's context
     */
     public static void setMthis(Aportacion mthis) {
         Aportacion.mthis = mthis;
     }

    /**
     * returns an AsyncPrint instance with '2' as first param
     * @return AsyncPrint instance
     */
    /*
    private AsyncPrint getAsyncPrintCaseTwo(){
        return new AsyncPrint(2, this, getProgressDialog());
    }
    */

    /**
     * returns an AsyncPrint instance with '7' as first param
     * @return AsyncPrint instance
     */
    /*
    private AsyncPrint getAsyncPrintCaseSeven(){
        return new AsyncPrint(7, this, getProgressDialog());
    }
    */

    @Override
    public void posicionSeleccionada(int position) {
        /*
        typeStatus = 1 -> cobro
        typeStatus = 2 -> no se encontró al cliente
        typeStatus = 3 -> no tenía dinero
        typeStatus = 4 -> acordó el plazo con el promotor
        typeStatus = 5 -> diririó el pago
        typeStatus = 6 -> cambió de domicilio
        typeStatus = 7 -> cancelado
        typeStatus = 8 -> no existe el cliente
        typeStatus = 9 -> no existe dirección
         */
        /*if (position == 5 || position == 6){
            typeStatus = position + 3;
        }
        else {
            typeStatus = 2 + position;
        }*/
        switch (position) {
            case 0:
            case 1:
                typeStatus = 2 + position;
                break;
            case 2:
            case 3:
                typeStatus = 3 + position;
                break;
            case 4:
            case 5:
            case 6:
                typeStatus = 4 + position;
                break;
        }
        Log.e("typeee", "" + typeStatus);
        if (typeStatus == 2 && BuildConfig.getTargetBranch()!= BuildConfig.Branches.MORELIA) {
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
                    .getDefaultAdapter();
            if (mBluetoothAdapter == null)
            {
                try {
                    mainActivity.showAlertDialog("Bluetooth",
                            getString(R.string.error_message_bluetooth_unsoported), true);
                } catch (Exception e){
                    Toast toast = Toast.makeText(getContext(), R.string.error_message_bluetooth_unsoported, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    e.printStackTrace();
                }
            } else {
                if (!mBluetoothAdapter.isEnabled()) {
                    try {
                        mainActivity.showAlertDialog("Bluetooth",
                                getString(R.string.error_message_bluetooth_unactivated),
                                true);
                    } catch (Exception e){
                        Toast toast = Toast.makeText(getContext(), R.string.error_message_bluetooth_unactivated, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        e.printStackTrace();
                    }
                } else {
                    try {
                        setEmpresa(jsonInfo.getString("id_grupo_base"));
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                    //shows a dialog
                    //dialog filled with info that will be printed on the ticket
                    final Dialog secondDialog = getDialog();
                    secondDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    secondDialog.setContentView(R.layout.previewnoaporto);
                    secondDialog.setCancelable(false);
                    secondDialog.getWindow().setBackgroundDrawable(
                            new ColorDrawable(
                                    Color.TRANSPARENT));
                    secondDialog.show();
                    final SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "dd/MM/yyyy");
                    SimpleDateFormat dateHora = new SimpleDateFormat("HH:mm");
                    final Date date = new Date();
                    TextView razon = (TextView) secondDialog
                            .findViewById(R.id.textView2Razon);
                    razon.setText(datoempresa1 + " " + datoempresa2 + "\n"
                            + datoempresa3 + " " + datoempresa4);
                    TextView fecha = (TextView) secondDialog
                            .findViewById(R.id.textViewFecha);
                    fecha.setText(dateHora.format(date) + " " + dateFormat.format(date));
                    TextView cliente = (TextView) secondDialog
                            .findViewById(R.id.textViewCliente);
                    TextView nocontrato = (TextView) secondDialog
                            .findViewById(R.id.textViewContrato);
                    TextView cobrador = (TextView) secondDialog
                            .findViewById(R.id.textViewCobrador);

                    try {
                        cobrador.setText(datosCobrador.getString("nombre"));
                        cliente.setText(jsonInfo.getString("nombre"));
                        nocontrato.setText(jsonInfo.getString("serie") + "-"
                                + jsonInfo.getString("no_contrato"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    final TextView aceptar = (TextView) secondDialog
                            .findViewById(R.id.textViewAceptar);

                    mthis = this;

                    aceptar.setOnClickListener(view -> {
                        //pendingCobro = true;
                        //AsyncPrint obj = getAsyncPrintCaseTwo();
                        //obj.execute();
                        aceptar.setBackgroundColor(Color.WHITE);
                        secondDialog.dismiss();

                        executePrintVisitTicket();
                    });
                    /*
                    aceptar.setOnTouchListener(new OnTouchListener() {

                        @Override
                        public boolean onTouch(View arg0, MotionEvent arg1) {
                            if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
                                aceptar.setBackgroundColor(Color.BLUE);
                                return true;
                            }
                            if (arg1.getAction() == MotionEvent.ACTION_UP) {
                                //pendingCobro = true;
                                AsyncPrint obj = getAsyncPrintCaseTwo();
                                obj.execute();
                                aceptar.setBackgroundColor(Color.WHITE);
                                secondDialog.dismiss();
                                return true;
                            }
                            return false;
                        }

                    });
                    */
                    final TextView cancelar = (TextView) secondDialog
                            .findViewById(R.id.textViewCancelar);
                    cancelar.setOnClickListener(v -> secondDialog.dismiss());
                    /*
                    cancelar.setOnTouchListener(new OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                cancelar.setBackgroundColor(Color.RED);
                                return true;
                            }
                            if (event.getAction() == MotionEvent.ACTION_UP) {
                                //pendingCobro = false;
                                cancelar.setBackgroundColor(Color.WHITE);
                                secondDialog.dismiss();
                            }
                            return false;
                        }

                    });
                    */
                }
            }
        } else {
            realizarNoAportacion();
        }
    }

    /**
     * writes visit into '.txt' file
     * writes visit into database
     * and finishes activity returning to main activity (Clientes)
     */
    private void realizarNoAportacion() {

        double latitud = 20.682413;
        double longitud = -103.381570;

        EditText editText = (EditText) getView().findViewById(R.id.et_cantidad_aportacion);
        editText.setText("0");
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
                .getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            try {
                mainActivity.showAlertDialog("Bluetooth",
                        getString(R.string.error_message_bluetooth_unsoported), true);
            } catch (Exception e){
                Toast toast = Toast.makeText(getContext(), R.string.error_message_bluetooth_unsoported, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                e.printStackTrace();
            }
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                try {
                    mainActivity.showAlertDialog("Bluetooth",
                            getString(R.string.error_message_bluetooth_unactivated), true);
                } catch (Exception e){
                    Toast toast = Toast.makeText(getContext(), R.string.error_message_bluetooth_unactivated, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    e.printStackTrace();
                }
            } else {
                if ( ApplicationResourcesProvider.getLocationProvider().getGPSPosition() != null ) {
                    mainActivity.log("lat: "
                                    + ApplicationResourcesProvider.getLocationProvider().getGPSPosition()
                                    .getLatitude(),
                            "long: "
                                    + ApplicationResourcesProvider.getLocationProvider()
                                    .getGPSPosition().getLongitude());

                    latitud = ApplicationResourcesProvider.getLocationProvider().getGPSPosition()
                            .getLatitude();
                    longitud = ApplicationResourcesProvider.getLocationProvider().getGPSPosition()
                            .getLongitude();
                }
                else // si el metodo nos retorna nullos
                {
                    latitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLatitude();
                    longitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLongitude();
                    ApplicationResourcesProvider.setLastKnownLocation(latitud, longitud);
                }
                /*
                else if (gpsProviderAportacion.getPosicion() != null)
                {
                    Util.Log.ih("getting location from gpsProviderClienteDetalle");
                    latitud = gpsProviderAportacion.getPosicion().getLatitude();
                    longitud = gpsProviderAportacion.getPosicion().getLongitude();
                }
                */

                //SqlQueryAssistant query = new SqlQueryAssistant(this);
                String id_cobrador = "";
                String id_cliente = "";
                String serieCliente = "";
                try {
                    id_cobrador = datosCobrador.getString("no_cobrador");
                    id_cliente = jsonInfo.getString("no_contrato");
                    serieCliente = jsonInfo.getString("serie");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    String seriecobrador = "=(";
                    try {
                        if (jsonInfo.getString("id_grupo_base").equals("01")) {
                            seriecobrador = datosCobrador
                                    .getString("serie_programa");
                        } else {
                            seriecobrador = datosCobrador.getString(
                                    DatabaseAssistant.getSingleCompany(jsonInfo.getString("id_grupo_base")).getName());
                                    //query
                                    //.mostrarEmpresa(jsonInfo.getString("id_grupo_base")).getString(
                                    //        Empresa.nombre_empresa));
                        }

                        if (seriecobrador.equals("3M") && new_folio.equals("71935"))
                        {
                            new_folio = "72155";
                        }
                        else if (seriecobrador.equals("3M,") && new_folio.equals("24280"))
                        {
                            new_folio = "24410";
                        }
                        else if (seriecobrador.equals("V1") && new_folio.equals("59211"))
                        {
                            new_folio = "59291";
                        }
                        else if (seriecobrador.equals("6C") && new_folio.equals("63063"))
                        {
                            new_folio = "63071";
                        }
                        else if (seriecobrador.equals("1A1") && new_folio.equals("96874"))
                        {
                            new_folio = "96896";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        seriecobrador = "=D";
                    }
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    int periodo = mainActivity.getEfectivoPreference().getInt("periodo", 0);

                    //register payment backup
                    try {

                        if(!pendingTicket) { //No tiene ticket pendiente
                            //folio, contrato, monto, empresa, periodo, tipo de cobro
                            LogRegister.paymentRegistrationBackup(
                                    seriecobrador + new_folio,
                                    jsonInfo.getString("serie") + jsonInfo.getString("no_contrato"),
                                    "----",
                                    jsonInfo.getString("id_grupo_base"),
                                    "" + periodo,
                                    "Fuera de Cartera",
                                    "" + String.valueOf(jsonInfo.getDouble("Saldo") - 0)
                            );
                        }
                        else
                        {
                            Log.i("IMPRESION", "TIENES un TICKET PENDOENTE, no podemos guardar el Log Register de nuevo.");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //end register payment backup

                    DatabaseAssistant.insertPayment(
                            jsonInfo.getString("id_grupo_base"),
                            "" + latitud,
                            "" + longitud,
                            "" + typeStatus,
                            id_cobrador,
                            "" + 0,
                            id_cliente,
                            dateFormat.format(date).toString(),
                            serieCliente,
                            "----",
                            seriecobrador,
                            jsonInfo.getString("tipo_bd"),
                            periodo,
                            ""+ DatabaseAssistant.setSaldoPorCobro(jsonInfo.getDouble("Saldo"), 0)
                    );

                    mthis = null;
                    getFragmentManager().popBackStack();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * prints visit ticket
     * @return
     */
    /*
    public boolean printNoAporto() {
        try {
            Looper.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //SqlQueryAssistant query = null;
        Toast.makeText(this, "Favor de encender la impresora.",
                Toast.LENGTH_LONG).show();

        boolean x = true;
        int cont = 0;

        while (cont < 100) {

            TscDll = new TSCActivity();

            try {

                //query = new SqlQueryAssistant(this);

                String empresa = jsonInfo.getString("id_grupo_base");
                String nombre = jsonInfo.getString("nombre");
                String apellido = jsonInfo.getString("apellido_pat") + " "
                        + jsonInfo.getString("apellido_mat");
                String serie = jsonInfo.getString("serie");
                String no_contrato = jsonInfo.getString("no_contrato");

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat dateHora = new SimpleDateFormat("HH:mm");
                Date date = new Date();


                dateAux = new Date().getTime();

                //timer that closes printer port if got stuck
                //trying to print
                //has one minute of limit before closing port
                //closing port causes NullPointerException on openPort()
                //so it tries again to print
                timer = new Timer();
                timer.schedule(new TimerTask() {

                    String mac;
                    int attempt;
                    TSCActivity tscDll;

                    @Override
                    public void run() {
                        if (checkDate()) {
                            Util.Log.ih("Closing port");
                            //disableBluetooth();
                            try {
                                tscDll.closeport();
                            } catch (Exception e){
                                e.printStackTrace();
                                timer.cancel();
                            }
                        }
                        Util.Log.ih("Hora, attempt: " + attempt);
                    }

                    public TimerTask setData(int attempt, TSCActivity tscDll) {
                        this.attempt = attempt;
                        this.tscDll = tscDll;
                        return this;
                    }
                }.setData(cont, TscDll), (1000 * 60));


                TscDll.openport(DatabaseAssistant.getPrinterMacAddress());
                Util.Log.ih("despues de openPort");
                timer.cancel();

                TscDll.setup(Printer.width,
                        155,
                        Printer.speed,
                        Printer.density,
                        Printer.sensorType,
                        Printer.gapBlackMarkVerticalDistance,
                        Printer.gapBlackMarkShiftDistance);

                Log.i("PRINT STATUS", TscDll.status());

                TscDll.clearbuffer();
                TscDll.sendcommand("GAP 0,0\n");
                TscDll.sendcommand("CLS\n");
                TscDll.sendcommand("CODEPAGE UTF-8\n");
                TscDll.sendcommand("SET TEAR ON\n");
                TscDll.sendcommand("SET COUNTER @1 1\n");
                for (int conter = 0; conter < 500; conter = conter + 30) {
                    TscDll.printerfont(conter, 0, "3", 0, 1, 1, "-");
                }
                TscDll.printerfont(400, 45, "3", 0, 1, 1,
                        dateFormat.format(date));
                TscDll.printerfont(300, 45, "3", 0, 1, 1,
                        dateHora.format(date));
                TscDll.printerfont(10, 115, "2", 0, 1, 1, datoempresa1);
                TscDll.printerfont(10, 160, "2", 0, 1, 1, datoempresa2);

                //377_address_dependency
                 mpresión de la dirección de la empresa soporta 3 lineas exactamente
                //Se debe dividir la dirección en 3 lineas y cada linea debe de tener un máximo de 35 caracteres
                //de lo contrario la información no se verá plasmada en el ticket

                switch (com.jaguarlabs.pabs.util.BuildConfig.getTargetBranch()) {
                    case GUADALAJARA:
                        if (!empresa.equals("05")) {
                            printDireccion("Av. Federalismo Norte 1485 Piso 1",
                                    "Oficina 3 Colonia San Miguel de",
                                    "Mezquitan CP 44260 Guadalajara");
                        }
                        else {
                            printDireccion("WASHINGTON No. 404",
                                    "COL. LA AURORA",
                                    "C.P.44460, Guadalajara, Jalisco");
                        }
                        break;
                    case TOLUCA:
                        //Las empresas con id 01 y 03 comparten la misma dirección
                        //Si se necesitara agregar más empresas entonces esta condición if debe soportar
                        //los idenfiticadores de esas nuevas emprsas para elegir qué dirección imprimir

                        if (empresa.equals("03") || empresa.equals("01")) {
                            printDireccion("PASEO TOLLOCAN 101",
                                    "COL. SANTA MARIA DE LAS ROSAS",
                                    "CP. 50140, TOLUCA");
                        }
                        break;
                    case PUEBLA:
                        if (empresa.equals("03") || empresa.equals("01")) {
                            printDireccion("FRANCISCO I. MADERO No. 428",
                                    "COL. SAN BALTAZAR CAMPECHE",
                                    "C.P. 72550, PUEBLA");
                        }
                        else{
                            printDireccion("FRANCISCO I. MADERO No. 428",
                                    "COL. SAN BALTAZAR CAMPECHE",
                                    "C.P. 72550, PUEBLA");
                        }
                        break;
                    case NAYARIT:
                        if (empresa.equals("04")) {
                            printDireccion("AV. MEXICO N° 393 NTE.",
                                    "CALLE NIÑOS HÉROES Nº 82",
                                    "JAVIER MINA Nº 69 ORIENTE",
                                    "COL. CENTRO",
                                    "CP. 63000, TEPIC, NAYARIT");
                        } else {
                            printDireccion("AV. MEXICO N° 393 NTE.",
                                    "COL. CENTRO",
                                    "CP. 63000, TEPIC, NAYARIT");
                        }
                        break;
                }
                int y = 325;
                TscDll.printerfont(200, y, "3", 0, 1, 1, "Notificación");
                y += 45;
                TscDll.printerfont(10, y, "3", 0, 1, 1, nombre + " " + apellido);
                Log.i("PRINT STATUS", TscDll.status());
                y += 45;
                TscDll.printerfont(10, y, "3", 0, 1, 1, "CONTRATO:" + serie
                        + no_contrato);
                y += 45;
                TscDll.printerfont(10, y, "3", 0, 1, 1, "Estimado Afiliado:");
                y += 45;
                TscDll.printerfont(10, y, "2", 0, 1, 1,
                        "Estuve en su domicilio para recoger su");
                y += 45;
                TscDll.printerfont(10, y, "2", 0, 1, 1,
                        "aportación; como no se encontró, pasaré");
                y += 45;
                TscDll.printerfont(10, y, "2", 0, 1, 1,
                        "de nueva cuenta el día ______, por lo ");
                y += 45;
                TscDll.printerfont(10, y, "2", 0, 1, 1,
                        "que le ruego esperar o en su caso dejar ");
                y += 45;
                TscDll.printerfont(10, y, "2", 0, 1, 1,
                        "el importe de su aportación.");
                y += 45;
                TscDll.printerfont(10, y, "2", 0, 1, 1,
                        "Para cualquier aclaración o recado le");
                y += 45;
                TscDll.printerfont(10, y, "2", 0, 1, 1,
                        "suplicamos comunicarse con nosotros a ");
                y += 45;

                //GUADALAJARA
                if(BuildConfig.getTargetBranch() == BuildConfig.Branches.GUADALAJARA) {
                    TscDll.printerfont(10, y, "2", 0, 1, 1,
                            "los siguientes teléfonos: 36-15-43-30 ");
                    y += 45;
                    TscDll.printerfont(10, y, "2", 0, 1, 1, "y 36-15-02-17.");
                    y += 45;
                }
                TscDll.printerfont(10, y, "2", 0, 1, 1,
                        "En caso de pasar a la oficina favor ");
                y += 45;
                TscDll.printerfont(10, y, "2", 0, 1, 1,
                        "de presentar este documento.");
                y += 50;
                TscDll.printerfont(150, y, "3", 0, 1, 1, "Respetuosamente");
                //x = false;
                y += 55;
                try {
                    TscDll.printerfont(0, y, "3", 0, 1, 1,
                            datosCobrador.getString("nombre"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                y += 55;
                for (int conter = 0; conter < 500; conter = conter + 30) {
                    TscDll.printerfont(conter, y, "3", 0, 1, 1, "-");
                }
                Log.i("printer status", TscDll.status());
                TscDll.printlabel(1, 1);
                Log.i("PRINT STATUS", TscDll.status());

                TscDll.closeport();
                //TscDll.closeport();
                return true;
            } catch (Exception e) {

                enableBluetooth();
                Util.Log.ih("Intento = " + cont);

                e.printStackTrace();
                cont++;
            }
        }

        return false;
    }
    */

    /**
     * writes visit into '.txt' file
     * writes visit into database
     * and finishes activity returning to main activity (Clientes)
     */
    @SuppressLint("SimpleDateFormat")
    public void processNoAporto() {
        mainActivity.runOnUiThread(() -> {
            double latitud = 20.682413;
            double longitud = -103.381570;

            if ( ApplicationResourcesProvider.getLocationProvider().getGPSPosition() != null ) {

                mainActivity.log("lat: "
                                + ApplicationResourcesProvider.getLocationProvider().getGPSPosition()
                                .getLatitude(),
                        "long: "
                                + ApplicationResourcesProvider.getLocationProvider()
                                .getGPSPosition().getLongitude());

                latitud = ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLatitude();
                longitud = ApplicationResourcesProvider.getLocationProvider().getGPSPosition().getLongitude();
            }
            else // si el metodo nos retorna nullos
            {
                latitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLatitude();
                longitud = ApplicationResourcesProvider.getLocationProvider().getLastKnownLocation().getLongitude();
                ApplicationResourcesProvider.setLastKnownLocation(latitud, longitud);
            }
                /*
                else if (gpsProviderAportacion.getPosicion() != null)
                {
                    Util.Log.ih("getting location from gpsProviderClienteDetalle");
                    latitud = gpsProviderAportacion.getPosicion().getLatitude();
                    longitud = gpsProviderAportacion.getPosicion().getLongitude();
                }
                */

            //SqlQueryAssistant query = new SqlQueryAssistant(getApplicationContext());
            String id_cobrador = "";
            String id_cliente = "";
            String serieCliente = "";
            try {
                id_cobrador = datosCobrador.getString("no_cobrador");
                id_cliente = jsonInfo.getString("no_contrato");
                serieCliente = jsonInfo.getString("serie");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String seriecobrador = "=(";
            try {
                if (jsonInfo.getString("id_grupo_base").equals("01")) {
                    seriecobrador = datosCobrador
                            .getString("serie_programa");
                } else {
                    seriecobrador = datosCobrador.getString(
                            DatabaseAssistant.getSingleCompany(jsonInfo.getString("id_grupo_base")).getName());
                    //query
                    //.mostrarEmpresa(jsonInfo.getString("id_grupo_base")).getString(
                    //        Empresa.nombre_empresa));
                }

                if (seriecobrador.equals("3M") && new_folio.equals("71935"))
                {
                    new_folio = "72155";
                }
                else if (seriecobrador.equals("3M,") && new_folio.equals("24280"))
                {
                    new_folio = "24410";
                }
                else if (seriecobrador.equals("V1") && new_folio.equals("59211"))
                {
                    new_folio = "59291";
                }
                else if (seriecobrador.equals("6C") && new_folio.equals("63063"))
                {
                    new_folio = "63071";
                }
                else if (seriecobrador.equals("1A1") && new_folio.equals("96874"))
                {
                    new_folio = "96896";
                }
            } catch (JSONException e) {
                e.printStackTrace();
                seriecobrador = "=D";
            }
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "yyyy-MM-dd HH:mm:ss");
                Date date = new Date();
                int periodo = mainActivity.getEfectivoPreference().getInt("periodo", 0);

                //register payment backup
                try {

                    if(!pendingTicket) { //No tiene ticket pendiente
                        //folio, contrato, monto, empresa, periodo, tipo de cobro
                        LogRegister.paymentRegistrationBackup(
                                seriecobrador + new_folio,
                                jsonInfo.getString("serie") + jsonInfo.getString("no_contrato"),
                                "----",
                                jsonInfo.getString("id_grupo_base"),
                                "" + periodo,
                                "Fuera de Cartera",
                                "" + String.valueOf(jsonInfo.getDouble("Saldo") - 0)

                        );
                    }
                    else
                    {
                        Log.i("IMPRESION", "TIENES un TICKET PENDOENTE, no podemos guardar el Log Register de nuevo.");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //end register payment backup

                DatabaseAssistant.insertPayment(
                        jsonInfo.getString("id_grupo_base"), // empresa
                        "" + latitud, // latitud
                        "" + longitud, // longitud
                        "" + typeStatus, // status
                        "" + id_cobrador, // id_cobrador
                        "" + 0, // monto
                        "" + id_cliente, // id_cliente
                        dateFormat.format(date).toString(), // fecha
                        serieCliente, // serie
                        "----", // folio
                        seriecobrador // serie_cobro
                        , jsonInfo.getString("tipo_bd"),
                        periodo,
                        ""+ DatabaseAssistant.setSaldoPorCobro(jsonInfo.getDouble("Saldo"), 0)
                );

                    /*
                    query.insertarCobro(jsonInfo.getString("id_grupo_base"), // empresa
                            "" + latitud, // latitud
                            "" + longitud, // longitud
                            "" + typeStatus, // status
                            "" + id_cobrador, // id_cobrador
                            "" + 0, // monto
                            "" + id_cliente, // id_cliente
                            dateFormat.format(date).toString(), // fecha
                            serieCliente, // serie
                            "----", // folio
                            seriecobrador // serie_cobro
                            , jsonInfo.getString("tipo_bd"), jsonInfo.getString("no_contrato"),
                            "" + getEfectivoPreference().getInt("periodo", 0));
                    */

                    /*
                    if (query.db != null){
                        query.db.close();
                    }
                    query = null;
                    */
                mthis = null;
                //finish();
                getFragmentManager().popBackStack();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

    }

    /**
     * prints address on the ticket
     * this address is divided so that it fits on the ticket
     * @param line1 first line
     * @param line2 second line
     * @param line3 third line
     */
    private void printDireccion(String line1, String line2, String line3){
        TscDll.printerfont(10, 205, "2", 0, 1, 1, line1);
        TscDll.printerfont(10, 250, "2", 0, 1, 1, line2);
        TscDll.printerfont(10, 295, "2", 0, 1, 1, line3);
    }

    /**
     * prints address on the ticket
     * this address is divided so that it fits on the ticket
     * @param line1 first line
     * @param line2 second line
     * @param line3 third line
     * @param line4 fourth line
     * @param line5 fifth line
     */
    private void printDireccion(String line1, String line2, String line3, String line4, String line5){
        TscDll.printerfont(10, 205, "2", 0, 1, 1, line1);
        TscDll.printerfont(10, 250, "2", 0, 1, 1, line2);
        TscDll.printerfont(10, 295, "2", 0, 1, 1, line3);
        TscDll.printerfont(10, 340, "2", 0, 1, 1, line4);
        TscDll.printerfont(10, 385, "2", 0, 1, 1, line5);
    }

    /**
     * Class with variables that helps to print when are sent
     * to android service
     */
    /*
    public class ModelPrintPaymentRequest {
         public JSONObject jsonInfo;
         public JSONObject datosCobrador;
         public String new_folio;
         public String empresaString;
         public String datoempresa1;
         public String datoempresa2;
         public String rfc;
         public String[] direccion;
         public String datoempresa6;
         public String datoempresa7;
         public int impreso;
         public String mensaje;
         public boolean imprimio;
         public String total;
         public String saldo;
    }
    */

    /**
     * starts android service to print ticket in another thread
     */
    private void executePrintAccountStatus(){
        try{
            String collectorname = datosCobrador.getString("nombre");
            String clientName = jsonInfo.getString("nombre");
            String clientLastName = jsonInfo.getString("apellido_pat") + " "
                    + jsonInfo.getString("apellido_mat");
            String clientSerie = jsonInfo.getString("serie");
            String clientContractNumber = jsonInfo.getString("no_contrato");
            String clientBalance = jsonInfo.getString("Saldo");

            PrintAccountStatusTicket printAccountStatusTicket = new PrintAccountStatusTicket(
                    collectorname, clientName, clientLastName, clientContractNumber,
                    clientSerie, clientBalance);

            spicePrintAccountStatusTicket.executeRequest(printAccountStatusTicket);

        } catch (JSONException e){
            mainActivity.toastL(R.string.error_message_call);
            e.printStackTrace();
        }
    }

    /**
     * starts android service to print ticket in another thread
     */
     private void executeAportacionNoCarteraService() {
         /*
         pendingAportacionNoCarteraRequest = true;
         clearAportacionCarteraCache();
         AportacionNoCarteraRequest request = new AportacionNoCarteraRequest(loadValuesToRequest());
         getSpiceManager().execute(request, KEY_CACHE, DurationInMillis.ONE_DAY, new AportacionNoCarteraListener());
         */
     }

    /**
     * starts android service to print ticket in another thread
     */
    private void executePrintPaymentTicket() {
        PrintPaymentTicketRequest printPaymentTicketRequest = new PrintPaymentTicketRequest(loadValuesToRequest());
        spicePrintPaymentTicket.executeRequest(printPaymentTicketRequest);
    }

    /**
     * starts android service to print ticket in another thread
     */
    private void executePrintVisitTicket(){

        try {
            String collectorName = datosCobrador.getString("nombre");
            String clientName = jsonInfo.getString("nombre");
            String clientLastName = jsonInfo.getString("apellido_pat") + " "
                    + jsonInfo.getString("apellido_mat");
            String clientContractNumber = jsonInfo.getString("no_contrato");
            String clientSerie = jsonInfo.getString("serie");
            String idCompany = jsonInfo.getString("id_grupo_base");
            String businessNameFirstLine = datoempresa1;
            String businessNameSecondLine = datoempresa2;
            String telefono = "";
            String no_cobrador="";

            if (datosCobrador.has("telefono"))
                telefono = datosCobrador.getString("telefono");
                no_cobrador = datosCobrador.getString("no_cobrador");


            PrintVisitTicketRequest printVisitTicketRequest = new PrintVisitTicketRequest(
                    collectorName,
                    clientName,
                    clientLastName,
                    clientContractNumber,
                    clientSerie,
                    idCompany,
                    businessNameFirstLine,
                    businessNameSecondLine,
                    telefono,
                    no_cobrador
            );

            spicePrintVisitTicket.executeRequest(printVisitTicketRequest);
        } catch (JSONException e){
            mainActivity.toastL(R.string.error_message_call);
            e.printStackTrace();
        }
    }

    /**
     * returns an instance of Dialog
     * @return instance of Dialog
     */
    private Dialog getDialog(){
        return new Dialog(getContext());
    }

    /**
     * returns an instance of RazonesDialog
     * @return instance of RazonesDialog
     */
    private RazonesDialog getRazonesDialog(){
        RazonesDialog mRazonesDialog = new RazonesDialog(getContext(), this);
        return mRazonesDialog;
    }

    /**
     * returns an instance of AlertDialogBuilder
     * @return instance of AlertDialogBuilder
     */
    private AlertDialog.Builder getAlertDialogBuilder(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            return new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Light_Dialog_Alert);
        }
        else {
            return new AlertDialog.Builder(getContext());
        }
    }

    private void showPaymentReferenceDialog() {

        try {

            dialog = getDialog();

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_payment_reference);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(
                            Color.TRANSPARENT));
            dialog.show();

            EditText paymentReferenceGiven = (EditText) dialog.findViewById(R.id.paymentReference);

            savePaymentReference = (TextView) dialog
                    .findViewById(R.id.savePaymentReference);
            savePaymentReference.setOnClickListener(v1 -> {

                try {

                    paymentReferenceString = paymentReferenceGiven.getText().toString();

                    if ( !paymentReferenceString.equals("") ) {

                        if (!mainActivity.isFinishing()) {

                            /*
                            SAME PROCESS AS BEFORE
                             */
                            /**
                             * Checks if there's a pendint ticket to print
                             */
                            if (pendingTicket) {

                                /**
                                 * Gets last payment registered
                                 */
                                JSONObject lastPayment = preferencesTicketPending.getPayment();

                                try {

                                    /**
                                     * Gets folio from last payment registered
                                     */
                                    new_folio = lastPayment.getString(getString(R.string.key_folio));


                                    /**
                                     * Prints ticket
                                     */
                                    if (dialog != null) {
                                        dialog.dismiss();
                                        dialog = null;
                                    }
                                    executePrintPaymentTicket();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (guardarPago()) {
                                    //if ( showPaymentReferenceDialog() ) {

                                    //checkEverythingWasSaved();

                                    executePrintPaymentTicket();
                                } else {
                                    Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_call, Toast.LENGTH_LONG);
                                    //toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                }
                            }
                            /*
                            END PROCESS
                             */

                            if (dialog != null && dialog.isShowing()) {
                                Util.Log.ih("dismissing progress dialog on estado");
                                dialog.dismiss();
                                dialog = null;
                            }
                        }
                    } else {
                        Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_add_payment_reference, Toast.LENGTH_SHORT);
                        //toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            cancelSavingPaymentReference = (TextView) dialog
                    .findViewById(R.id.cancelAddingPaymentReference);
            cancelSavingPaymentReference.setOnClickListener(v1 -> {
                if (!mainActivity.isFinishing()) {
                    if (dialog != null && dialog.isShowing()) {
                        Util.Log.ih("dismissing progress dialog on estado");
                        dialog.dismiss();
                        dialog = null;
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            mainActivity.toastS("Hubo un error. Por favor intentalo de nuevo");
        }
    }

    /**
     * returns an instance of ProgressDialog
     * @return instance of ProgressDialog
     */
    private ProgressDialog getProgressDialog(){
        return new ProgressDialog(getContext());
    }

    /**
     * this method gets called when android service for dialog_copies tickets finishes
     * either with an error or with success
     * @param result response with the result of the android service
     *                 what is needed is response.result which is a boolean
     *                 true -> successfull
     *                 false -> error
     */
     private void onAportacionSuccess(boolean result) {

         mainActivity.LogIh(R.string.printing_finished);

         if (result) {

             if (BuildConfig.isPrintingCopiesEnabled()) {
                 showSecondPrintingDialog();
             }

             /*
             if (pd != null && pd.isShowing()) {
                 pd.dismiss();
             }
             */
             //dismissMyCustomDialog();

             //when copies are no enabled
             //finishes antivity going back to main screen
             if (!BuildConfig.isPrintingCopiesEnabled()) {
                 try {
                     Log.e("saliendo", "de aportación fuera de cartera");

                     Intent intent = new Intent();
                     double cantidad = datosCobrador.getDouble("efectivo");
                     EditText editText = (EditText) getView().findViewById(R.id.et_cantidad_aportacion);
                     cantidad = cantidad
                             + Double.parseDouble(editText.getText()
                             .toString());
                     intent.putExtra("nuevo_efectivo", "" + cantidad);
                     //setResult(RESULT_OK, intent);
                     fragmentResult.onFragmentResult(requestCode, Activity.RESULT_OK, intent);
                     mthis = null;
                     //finish();
                     getFragmentManager().popBackStack();
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
                 mthis = null;
                 //finish();
                 getFragmentManager().popBackStack();
             }
         } else {

             dismissMyCustomDialog();

             //an error happened
             //show up a dialog with option to retry to print
             try {
                 mainActivity.LogEh(R.string.error_message_printing_try_again);

                 tryAgainDialog = getAlertDialogBuilder()
                         .setMessage(R.string.error_message_printing_try_again)
                         .setPositiveButton(R.string.error_title_try_again, (dialog1, which) -> executePrintPaymentTicket());
                 tryAgainDialog.setCancelable(false);
                 tryAgainDialog.create().show();
             } catch (Exception e){

                 Util.UI.toastL(getContext(), getString(R.string.error_message_printing_try_again));
                 e.printStackTrace();
             }
         }
         //if (pd != null && pd.isShowing()) {
         //    pd.dismiss();
         //}

     }

    /**
     * android service
     * an error happened
     * show up a dialog with option to retry to print
     * @param exception error thrown
     */
     private void onAportacionFailure(SpiceException exception) {

         mainActivity.LogEh(R.string.payment_failed);

         AlertDialog.Builder tryAgainDialog = getAlertDialogBuilder()
                 .setMessage(R.string.error_message_printing_try_again)
                 .setPositiveButton(R.string.error_title_try_again, (dialog1, which) -> executePrintPaymentTicket());
         tryAgainDialog.create().show();

     }

    private boolean goBack = true;

    /**
     * shows framelayout as progress dialog
     */
    private void showMyCustomDialog(){

        goBack = false;

        if (getView() != null) {
            final FrameLayout flLoading = getView().findViewById(R.id.flLoading);
            flLoading.setVisibility(View.VISIBLE);
        }

        //final Animation qwe = AnimationUtils.loadAnimation(this, R.animator.rotate_around_center_point);
        //final ImageView ivLoadingIcon = (ImageView) ClienteDetalle.this.findViewById(R.id.ivLoading);
        //ivLoadingIcon.startAnimation(qwe);
    }

    /**
     * hides framelayout used as progress dialog
     */
    private void dismissMyCustomDialog(){

        goBack = true;

        if (getView() != null) {
            final FrameLayout flLoading = getView().findViewById(R.id.flLoading);
            flLoading.setVisibility(View.GONE);
        }

        //final ImageView ivLoadingIcon = (ImageView) ClienteDetalle.this.findViewById(R.id.ivLoading);
        //ivLoadingIcon.clearAnimation();
    }

    /**
     * set payment as not sync
     * when something went wrong while printing
     */
    public void setPaymentNotSync(){
        Preferences preferencesPendingTciet = new Preferences(ApplicationResourcesProvider.getContext());
        preferencesPendingTciet.setPaymentNotSync();
    }

    public void setFragmentResult(OnFragmentResult fragmentResult) {
        this.fragmentResult = fragmentResult;
    }

    public void mostrarAvisoSuspencionTemporalDePagos(View view)
    {
        final Button btCancelar, btImprimir;
        EditText etMotivo, etFecha;
        ImageView imgCalendario;
        ArrayList<String> esquemas;
        Spinner spLista;


        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(mainActivity);
        mBottomSheetDialog.setContentView(R.layout.dialog_aviso_suspencion);

        //dialogo_suspencion.setContentView(R.layout.dialog_aviso_suspencion);
        //dialogo_suspencion.setCancelable(false);
        btCancelar = (Button) mBottomSheetDialog.findViewById(R.id.btCancelar);
        btImprimir = (Button) mBottomSheetDialog.findViewById(R.id.btImprimir);
        etMotivo=(EditText) mBottomSheetDialog.findViewById(R.id.etMotivo);
        etFecha=(EditText)mBottomSheetDialog.findViewById(R.id.etFecha);
        spLista=(Spinner)mBottomSheetDialog.findViewById(R.id.spLista);
        imgCalendario=(ImageView) mBottomSheetDialog.findViewById(R.id.imgCalendario);

        esquemas = new ArrayList<String>();
        esquemas.add("Selecciona una opción");
        List<Suspensionitem> lista = Suspensionitem.listAll(Suspensionitem.class);
        if(lista.size()>0) {
            for (int i = 0; i < lista.size(); i++) {
                try {
                    esquemas.add(lista.get(i).getDescripcion().toUpperCase());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            Snackbar.make(spLista, "Registros menores a 0", Snackbar.LENGTH_LONG).show();
        }

        ArrayAdapter<CharSequence> adaptador = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, esquemas);
        spLista.setAdapter(adaptador);



        btCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                mBottomSheetDialog.dismiss();
            }
        });

        btImprimir.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (etFecha.getText().toString().equals(""))
                {
                    Toast.makeText(mainActivity, "Completa los campos correctamente", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    //String fechaReanudacion=diia+"/"+mess+"/"+ano;
                    //String fechaReanudacion=diia+"/"+mess+"/"+ano;
                    String fechaReanudacion=etFecha.getText().toString();
                    try
                    {
                        String nombreCompletoCliente = jsonInfo.getString("nombre") + " " + jsonInfo.getString("apellido_pat") + " " + jsonInfo.getString("apellido_mat");
                        String serialAndContract = jsonInfo.getString("serie") + jsonInfo.getString("no_contrato");

                        String sintax = "SELECT * FROM NOTICE";
                        List<Notice> listNotice = Notice.findWithQuery(Notice.class, sintax);
                        if(listNotice.size()>0)
                        {
                            try
                            {

                                if(spLista.getSelectedItemPosition()==0)
                                {
                                    Toast.makeText(getContext(), "SELECCIONA UNA OPCIÓN", Toast.LENGTH_LONG).show();
                                }
                                else
                                {


                                    Date fechaSeleccionada = new Date();
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                                    Date dateActual = new Date();
                                    dateFormat.format(dateActual);
                                    System.out.println(dateActual);
                                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
                                    try {
                                        fechaSeleccionada= dateFormatter.parse(fechaReanudacion);
                                    }catch (ParseException e)
                                    {
                                        e.printStackTrace();
                                    }

                                    if (fechaSeleccionada.compareTo(dateActual)<0)
                                    {
                                        Log.d("NO ENTRO", "Fecha seleccionada es menor a la actual");
                                        Toast.makeText(mainActivity, "Debes seleccionar una fecha posterior a la actual.", Toast.LENGTH_SHORT).show();
                                        fechaReanudacion="";
                                        diia=0;
                                        mess=0;
                                        ano=0;
                                        etFecha.setText("");
                                    }
                                    else
                                    {
                                        String nombreCobrador = datosCobrador.getString("nombre");
                                        String tiketSuspencionTEXTO = listNotice.get(0).getTiketsuspencion();
                                        PrintTemporalSuspensionNotice printTemporalSuspensionNotice = new PrintTemporalSuspensionNotice(
                                                nombreCompletoCliente,
                                                serialAndContract,
                                                spLista.getSelectedItem().toString(),
                                                fechaReanudacion,
                                                tiketSuspencionTEXTO,
                                                nombreCobrador,
                                                jsonInfo.getString("monto_atrasado"),
                                                jsonInfo.getString("ultimo_abono"),
                                                jsonInfo.getString("Abonado"));

                                        System.out.println(fechaReanudacion);
                                        spicePrintAccountStatusTicket.executeRequest(printTemporalSuspensionNotice);
                                        mBottomSheetDialog.dismiss();
                                    }

                                }


                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                    catch (JSONException ex) {
                        Log.d("IMPRESION --> ", "Error en recolectar datos JSON");
                    }

                }
            }
        });

        etFecha.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(mainActivity, android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListener, year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;

                String date = dayOfMonth + "/" + month + "/" + year;
                etFecha.setText(date);
            }
        };

        spLista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                opcionSeleccionada=spLista.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        mBottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mBottomSheetDialog.show();

    }


}