package com.jaguarlabs.pabs.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.tscdll.TSCActivity;
import com.google.gson.Gson;
import com.jaguarlabs.pabs.R;
import com.jaguarlabs.pabs.adapter.CierreAdapter;
import com.jaguarlabs.pabs.bluetoothPrinter.BluetoothPrinter;
import com.jaguarlabs.pabs.database.Actualizados;
import com.jaguarlabs.pabs.database.Companies;
import com.jaguarlabs.pabs.database.CompanyAmounts;
import com.jaguarlabs.pabs.database.DatabaseAssistant;
import com.jaguarlabs.pabs.database.MontosTotalesDeEfectivo;
import com.jaguarlabs.pabs.database.MotivosDeCancelacion;
import com.jaguarlabs.pabs.database.Notice;
import com.jaguarlabs.pabs.database.Payments;
import com.jaguarlabs.pabs.database.Suspensionitem;
import com.jaguarlabs.pabs.models.ModelDeposit;
import com.jaguarlabs.pabs.models.ModelPayment;
import com.jaguarlabs.pabs.models.ModelPeriod;
import com.jaguarlabs.pabs.net.NetHelper;
import com.jaguarlabs.pabs.net.NetHelper.onWorkCompleteListener;
import com.jaguarlabs.pabs.net.NetService;
import com.jaguarlabs.pabs.rest.RestService;
import com.jaguarlabs.pabs.rest.SpiceHelper;
import com.jaguarlabs.pabs.rest.models.ModelCancelPaymentResponse;
import com.jaguarlabs.pabs.rest.models.ModelPrintMoreThan400PaymentsCierreResponse;
import com.jaguarlabs.pabs.rest.request.CancelPaymentRequest;
import com.jaguarlabs.pabs.rest.request.offline.PrintCierreInformativoOPeriodoTicketRequest;
import com.jaguarlabs.pabs.rest.request.offline.PrintMoreThan400PaymentsCierreInformativoRequest;
import com.jaguarlabs.pabs.rest.request.offline.PrintTemporalSuspensionNotice;
import com.jaguarlabs.pabs.util.ApplicationResourcesProvider;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.CerrarSesionCallback;
import com.jaguarlabs.pabs.util.ConstantsPabs;
import com.jaguarlabs.pabs.util.ExtendedActivity;
import com.jaguarlabs.pabs.util.LogRegister;
import com.jaguarlabs.pabs.util.MyCallBack;
import com.jaguarlabs.pabs.util.OnFragmentResult;
import com.jaguarlabs.pabs.util.SharedConstants.Printer;
import com.jaguarlabs.pabs.util.Util;
import com.jaguarlabs.pabs.util.VolleySingleton;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.PendingRequestListener;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.listener.RequestProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import pl.tajchert.nammu.Nammu;

/**
 * This class shows up all payments and visits made within a period.
 * It's posible to print ticket with them all.
 * Also, from this class you can cancel any payment made using as an
 * option, a folio search field which scrolls to given folio, so that
 * it's easy to cancel a payment.
 */
@SuppressWarnings("SpellCheckingInspection")
@SuppressLint({ "Wakelock", "SimpleDateFormat" })
public class CierreInformativo extends Fragment implements MyCallBack,
		CerrarSesionCallback, AdapterView.OnItemSelectedListener
{

	private SpiceManager spiceManager = new SpiceManager(RestService.class);

	ProgressDialog progress;
	private static CierreInformativo mthis = null;;
	private static final int TIME_TOAST = 2000;
	public static double deposito = 0;
	private String cancelAmount;
	private String cancelCompany;
	private String cancelCustomerNumber;
	private String cancelDate;

	private boolean cancelingTicket = false;
	private boolean ticketWasCancelled = false;
	private String TAG = "CACHE_CANCELTICKET";
	private boolean pendingCancellationRequest;
	private Spinner spinnerPeriodSelector;
	private boolean showToast = true;
	public String opcionSeleccionada="";
	Dialog dialogo_cancelacion_de_ticket;

	private ListView lvCierreWithCancelledTickets;
	private Parcelable state;
	private FrameLayout frameLayoutForSpinner;
	public static CierreInformativo getMthis() {
		return mthis;
	}

	public static void setMthis(CierreInformativo mthis) {
		CierreInformativo.mthis = mthis;
	}

	private boolean dontexit = true;
	private String noCobrador;
	private String fechaOperacion;
	public static String coCobrador = "";
	public static String mac;

	//This variables are used when dialog_copies payments greater than 400
	public static JSONArray arrayCierreArranged;
	public static int printCounterToSetSize = 0;
	public static int printCounter = 0;
	public static int organizationCounter = 0;
	public static int xValue;
	public static int ticketPosValue;
	//public static JSONArray empresasValues;
	public static List<Companies> companiesValues;

	public static JSONObject totalesValues;
	//public static SqlQueryAssistant queryValues;
	public static double montoValue;
	public static boolean restartMonto = true;
	public static int ticketsValue;
	public static int limitToPrint = 400;
	public static int sectionOfArray = 1;
	public static JSONArray arrayCierreCut;
	private static final String KEY_CACHE = "print_more_than_400_payments_informativo_cache";
	private ProgressDialog pd;

	private long dateAux;
	private Timer timer;

	public boolean goBack = true;

	private List<ModelPayment> offlinePayments;
	private List<Payments> recordPymentsList;

	private MainActivity mainActivity;
	private SpiceManager spiceManagerOffline;
	private SpiceHelper<Boolean> spicePrintCierreInformativo;
	private SpiceHelper<ModelPrintMoreThan400PaymentsCierreResponse> spicePrintMoreThan400Payments;

	//private Spinner spinnerPeriodSelector;
	private int periodsCounter = 0;
	private int periodsActive = 0;

	private Button cierre;
	private ImageView back;

	private OnFragmentResult fragmentResult;
	private int requestCode = -1;

	private RelativeLayout cierreLayout;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_cierre_, container, false);
		dialogo_cancelacion_de_ticket = new Dialog(getActivity());
		mainActivity = (MainActivity) getActivity();
		spiceManagerOffline = mainActivity.getSpiceManagerOffline();


		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		spinnerPeriodSelector = (Spinner) view.findViewById(R.id.spinnerPeriods);
		frameLayoutForSpinner = (FrameLayout) view.findViewById(R.id.flSpinner);


		if (getArguments()!= null)
			requestCode = getArguments().getInt("requestCode", -1);

		cierreLayout = view.findViewById(R.id.cierreLayout);

		cierre = view.findViewById(R.id.btn_efectuar_cierre);
		back = view.findViewById(R.id.imageViewBack);

		cierre.setOnClickListener(v -> {
			restartVariablesToPrint();
			realizarCierre();
			showMyCustomDialog();
		});

		back.setOnClickListener(v -> {
			if (dontexit) {
				if (ticketWasCancelled){
					Util.Log.ih("posicionCancelacion != 0 -> onClickFromXML");
					//setResult(99);
					fragmentResult.onFragmentResult(requestCode, 99, null);
				} else {
					Util.Log.ih("posicionCancelacion == 0");
				}
				mthis = null;
				//finish();
				getFragmentManager().popBackStack();
			}
		});

		spicePrintCierreInformativo = new SpiceHelper<Boolean>(spiceManagerOffline, "printCierreInformativo", Boolean.class, DurationInMillis.ONE_HOUR) {
			@Override
			protected void onFail(SpiceException exception) {
				super.onFail(exception);
				fail();
			}

			@Override
			protected void onSuccess(Boolean result) {
				try {
					if (result != null) {
						super.onSuccess(result);
						if (result) {
							copiaImpresion();
						} else {
							fail();
						}
					} else {
						super.onSuccess(true);
						copiaImpresion();
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
			}

			@Override
			protected void onProgressUpdate(RequestProgress progress) {
				progressUpdate(progress);
			}
		};

		spicePrintMoreThan400Payments = new SpiceHelper<ModelPrintMoreThan400PaymentsCierreResponse>(spiceManagerOffline, "print_more_than_400_payments", ModelPrintMoreThan400PaymentsCierreResponse.class, DurationInMillis.ONE_HOUR) {
			@Override
			protected void onFail(SpiceException exception) {
				super.onFail(exception);
				onPrintingFailure(exception);
			}

			@Override
			protected void onSuccess(ModelPrintMoreThan400PaymentsCierreResponse result) {
				try {
					if (result != null) {
						super.onSuccess(result);
						onPrintingSuccess(result);
					} else {
						ModelPrintMoreThan400PaymentsCierreResponse m = new ModelPrintMoreThan400PaymentsCierreResponse();
						m.setResult(false);
						m.setFinishPrinting(false);
						super.onSuccess(m);
						onPrintingSuccess(m);
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
			}

			@Override
			protected void onProgressUpdate(RequestProgress progress) {
				progressUpdate(progress);
			}
		};

		//init

		/*
		spiceCancelar = new SpiceHelper<ModelCancelarCobroResponse>(getSpiceManager(), "", ModelCancelarCobroResponse.class) {
			@Override
			protected void onFail(SpiceException exception) {
				super.onFail(exception);
			}

			@Override
			protected void onSuccess(ModelCancelarCobroResponse response) {
				super.onSuccess(response);
			}
		};
		*/

		pendingCancellationRequest = false;

		Util.Log.ih("Cierre Informativo - setInternetAsReachable = " + ExtendedActivity.setInternetAsReachable);

		//canceladoPreference   = getSharedPreferences("cancelado", Context.MODE_PRIVATE);
		// Clientes.getMthis().hasActiveInternetConnection(Clientes.getMthis());
		TextView title = (TextView) view.findViewById(R.id.tv_mensaje_loading);
		title.setText("Cierre Informativo");

		mainActivity.hasActiveInternetConnection(getContext(), mainActivity.mainLayout);

		/**
		 * EditText to Search Folios
		 * This edittext searches through ArrayCierre array using complete
		 * folio (with serie, comma, dot and everything it has) or part of
		 * the folio spelled.
		 * If there's a coincidence, scrolls to that position.
		 * Always takes the first coincidence, so if you are looking for an
		 * specific folio, got to give complete folio.
		 *
		 * Jordan Alvarez
		 */
		TextView etSearch = (TextView) view.findViewById(R.id.etSearch);
		etSearch.setVisibility(View.VISIBLE);
		etSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void afterTextChanged(Editable changedText) {

				if (arrayCierre != null) {

					if (changedText.length() != 0) {
						String arrayCierreRegistry;

						for (int i = 0; i < arrayCierre.length(); i++) {
							try {
								arrayCierreRegistry = arrayCierre.getJSONObject(i).getString("folio");
								if (arrayCierreRegistry.equalsIgnoreCase( changedText.toString() )){
									lvCierreWithCancelledTickets.setSelection(i);
									break;
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		});

		mthis = this;
		ExtendedActivity.setEstoyLogin(false);
		Button cierre = (Button) view.findViewById(R.id.btn_efectuar_cierre);
		cierre.setEnabled(false);
		cierre.setEnabled(false);
		cierre.setEnabled(false);
		cierre.setVisibility(View.GONE);

		arrayCierreList = new HashMap<String,JSONObject>();


		/**
		 * Deprecated in 3.0
		 * This method was used on previous version
		 * it is not need it anymore
		 */
		//saveListaDepositos();
			/*try {
				//deposito = new SqlQueryAssistant(ApplicationResourcesProvider.getContext()).getTotalDepositos();
				deposito = DatabaseAssistant.getTotalDeposits();
			} catch (Exception e) {
				deposito = 0;
				e.printStackTrace();
			}*/


		try {
			//SharedPreferences efectivoPreference = ApplicationResourcesProvider.getContext().getSharedPreferences("efecitvo", Context.MODE_PRIVATE);
			//int periodo = efectivoPreference.getInt("periodo", 0);

			//int periodo = mainActivity.getEfectivoPreference().getInt("periodo", 0);
			deposito = DatabaseAssistant.getTotalDepositosPorPeriodo(mainActivity.getEfectivoPreference().getInt("periodo", 0));
			Log.d("IMPRESION -->", "Deposito: "+ deposito);


		} catch (Exception e) {
			deposito = 0;
			e.printStackTrace();
		}


		coCobrador = DatabaseAssistant.getCollector().getIdCollector();

		pm = (PowerManager) getContext().getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "tag");
		wl.acquire();

		Bundle bundle = getArguments();
		if (bundle != null) {
			if (bundle.containsKey("iniciar_time_sesion")) {
				boolean inicarTimer = bundle.getBoolean("iniciar_time_sesion");
				if (inicarTimer) {
					callSuperOnTouch = true;
					//iniciarHandlerInactividad();

				}
			}
			if (bundle.containsKey("mac")) {
				mac = bundle.getString("mac");
			}
		}

		Snackbar.make(cierreLayout, R.string.info_message_getting_ready, Snackbar.LENGTH_SHORT).show();

		offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();
		if (offlinePayments != null) {
			//arrayCobrosOffline = json.getJSONArray("cobros");
			if (offlinePayments.size() > 0) {
				//if (Clientes.getMthis() != null && Clientes.getMthis().isConnected) {
				if (mainActivity.isConnected) {
					registerPagos();
				}
				else {
					try {
						AlertDialog.Builder alertInternet = getAlertDialogBuilder();
						alertInternet.setMessage(getResources().getString(
								R.string.error_message_network));
						alertInternet.setPositiveButton("Salir", (dialog, which) -> {
							mthis = null;
							//finish();
							getFragmentManager().popBackStack();
						});
						alertInternet.create().show();
					} catch (Exception e){
						e.printStackTrace();
						Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_network, Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						mthis = null;
						//finish();
						getFragmentManager().popBackStack();
					}
				}


			} else {

				if (mainActivity.isConnected) {

					Util.Log.ih("enviando depositos...");
					enviarDepositos();
					//checkPayments();
					//descargarHistorial();
				} else {
					try {
						AlertDialog.Builder alertInternet = getAlertDialogBuilder();
						alertInternet.setMessage(getResources().getString(
								R.string.error_message_network));
						alertInternet.setPositiveButton("Salir", (dialog, which) -> {
							mthis = null;
							//finish();
							getFragmentManager().popBackStack();
						});
						alertInternet.create().show();
					} catch (Exception e){
						e.printStackTrace();
						Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_network, Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						mthis = null;
						//finish();
						getFragmentManager().popBackStack();
					}
				}
			}
		} else {

			//if (Clientes.getMthis() != null && Clientes.getMthis().isConnected) {
			if (mainActivity.isConnected) {
				enviarDepositos();
			}
			else {
				try{
					AlertDialog.Builder alertInternet = getAlertDialogBuilder();
					alertInternet.setMessage(getResources().getString(
							R.string.error_message_network));
					alertInternet.setPositiveButton("Salir", (dialog, which) -> {
						mthis = null;
						//finish();
						getFragmentManager().popBackStack();
					});
					alertInternet.create().show();
				} catch (Exception e){
					e.printStackTrace();
					Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_network, Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					mthis = null;
					//finish();
					getFragmentManager().popBackStack();
				}
			}
		}

		totalesValues = new JSONObject();
		generadorJSON();
		requestGetMotivosDeCancelacion();
	}









	public void generadorJSON()
	{
		JSONObject jsonDatosGenerales = new JSONObject();
		JSONArray arrayJSON = new JSONArray();
		JSONObject jsonDomicilio = new JSONObject();

		String aux= "SELECT * FROM ACTUALIZADOS WHERE sync=0";
		List<Actualizados> listaDatosActualizados = Actualizados.findWithQuery(Actualizados.class, aux);
		if(listaDatosActualizados.size()>0)
		{
			for(int i=0; i<listaDatosActualizados.size();i++)
			{
				try
				{
					arrayJSON.put(i, new JSONObject(new Gson().toJson(listaDatosActualizados.get(i))));
				} catch (JSONException ex) {
					ex.printStackTrace();
				}
			}
		}
		else
		{
			Log.d("SIN DATOS --->", "NO HAY DATOS POR ACTUALZAR");
		}

		try
		{
			jsonDatosGenerales.put("general", arrayJSON);
		}catch (JSONException ex)
		{
			Log.d("SIN DATOS --->", "NO HAY DATOS POR ACTUALZAR");
		}

		cargaDeDatosAServidor(jsonDatosGenerales);
	}



	public void cargaDeDatosAServidor(JSONObject jsonParams)
	{
		//progress = ProgressDialog.show(mainActivity, "Cargando datos", "Por favor espera...", true);
		String hola = ConstantsPabs.URL_CARGA_ACTUALIZACIONES;
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.URL_CARGA_ACTUALIZACIONES, jsonParams,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response)
					{
						try
						{
							String numeroContrato="", numeroContratoFail="";

							if (!response.has("error"))
							{

								JSONArray jsonSuccess = response.getJSONArray("success");
								JSONArray jsonFail = response.getJSONArray("fail");

								if(jsonSuccess.length()>0)
								{

									for(int i=0; i<=jsonSuccess.length()-1; i++)
									{
										JSONObject object = jsonSuccess.getJSONObject(i);
										numeroContrato = object.getString("numerocontrato");

										String query="UPDATE ACTUALIZADOS SET sync ='2' WHERE numerocontrato='" + numeroContrato +"'";
										Actualizados.executeQuery(query);
									}

									//progress.dismiss();
								}
								else if (jsonFail.length()>0)
								{

									for(int y=0; y<=jsonFail.length()-1;y++)
									{
										JSONObject object = jsonFail.getJSONObject(y);
										//JSONObject solicitud = object.getJSONObject("solicitud");
										numeroContratoFail = object.getString("numerocontrato");
										String result = object.getString("RESULTADO");
										Log.d("FAIL RESULT-->", result);
									}
									//progress.dismiss();
								}
								else
								{
									String entroAqui="";
								}
								//progress.dismiss();
							}
							else
							{
								String error = response.getString("error");
								//progress.dismiss();
							}

						} catch (JSONException e) {
							e.printStackTrace();
							Log.d("ERROR--->", e.toString());
						}
						//progress.dismiss();
					}
				},

				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						//progress.dismiss();
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
	}























	private void fail(){
		dismissMyCustomDialog();
		mainActivity.toastL(R.string.error_message_printing);
	}

	private void progressUpdate(RequestProgress progress){
		Util.Log.ih("inside progressUpdate method");
		switch ( (int) progress.getProgress() ){
			case ConstantsPabs.START_PRINTING:
				Util.Log.ih("case START_PRINTING");
				showMyCustomDialog();
				break;
			case ConstantsPabs.FINISHED_PRINTING:
				Util.Log.ih("case FINISHED_PRINTING");
				dismissMyCustomDialog();
				break;
			default:
				Util.Log.ih("default");
				//nothing
		}
	}

	/**
	 * Sets all payments stored in arrayCobrosOffline variable which were just sent
	 * to server as sync (sync <- 1) so that does not resend payments made
	 * causing a lot of data usage.
	 */
	public void sincronizarPagosRegistrados(JSONObject json) {
		try {
			//SqlQueryAssistant assistant = new SqlQueryAssistant(this);
			//for (int i = 0; i < arrayCobrosOffline.length(); i++) {
			//	JSONObject cobro = arrayCobrosOffline.getJSONObject(i);
			//	assistant.actualizarSincronizacion(cobro.getString("fecha"));
			//}
			//assistant = null;
			//arrayCobrosOffline = new JSONArray();

			/*for (ModelPayment p: offlinePayments){
				DatabaseAssistant.updatePaymentsAsSynced(p.getDate(), json);
			}*/

			DatabaseAssistant.updatePaymentsAsSynced(json);


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String nombre;

	//private List<JSONObject> arrayCierreList;
	private HashMap<String, JSONObject> arrayCierreList;
	private JSONArray arrayCierre;
	public static int efectivoInicial = 0;
	private int posicionCancelacion;
	private JSONArray arrayCobrosOffline;
	TSCActivity TscDll = new TSCActivity();
	private PowerManager pm;
	private WakeLock wl;
	//public SharedPreferences canceladoPreference;
	private boolean callSuperOnTouch = false;

	private int ticketsPrograma = 0;

	private int ticketsMalba = 0;

	//private SpiceHelper<ModelCancelarCobroResponse> spiceCancelar;

	/**
	 * calls android service {@link #executeCancelPayment(int, LogRegister.LogCancelacion)}
	 * to cancel payment
	 *
	 * @param no_cobro
	 * 			'no_cobro' of selected ticket
	 */
	private void cancelarPago(int no_cobro) {

		JSONObject json = new JSONObject();
		LogRegister.LogCancelacion cancelacion = new LogRegister.LogCancelacion();

		try {
			cancelacion.no_cobro = ""+no_cobro;
			cancelacion.fechaDeOperacion = fechaOperacion;
			json.put("no_cobro", no_cobro);
			json.put("fecha", fechaOperacion);
			json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));

		} catch (Exception e) {
			cancelingTicket = false;
			e.printStackTrace();
		}
		//canceladoPreference.edit().putString("pendiente", json.toString()).commit();
		LogRegister.registrarCancelacion(cancelacion);



		/*final Toast toastWait = Toast.makeText(getContext(), "", Toast.LENGTH_LONG);
		//toastWait = Toast.makeText(this, "", Toast.LENGTH_SHORT);
		toastWait.setGravity(Gravity.CENTER, 0, 0);
		//for (int i = 0; i < 50; i++){
		toastWait.setText("Cancelando Cobro... Espera por favor...");
		toastWait.show();*/

		Snackbar.make(cierreLayout, "Cancelando Cobro... Espera por favor...", Snackbar.LENGTH_SHORT).show();
		//}

		/**
		 * RoboSpice Web Service
		 * to
		 * cancel payments
		 * @param params: payment number
		 */
		Util.Log.ih("EXECUTE CANCEL PAYMENT ROBOSPICE");
		executeCancelPayment(no_cobro, cancelacion);

		//TODO: adrian_life: Suplir con Robospice
		//TODO: Jordan Alvare, did already

//		{
		/**
		 * Working code to request 'Cancelar cobro'
		 */
		/*
			NetHelper.getInstance().ServiceExecuter(
					new NetService(ConstantsPabs.urlCancelarPago, json), this,
					new onWorkCompleteListener() {
						LogRegister.LogCancelacion cancelacion;
						@Override
						public void onCompletion(String result) {

							toastWait.setText("Cancelado");
							toastWait.cancel();
							*/
		/**
		 * Forcing a error to happened
		 */
							/*
							Handler handler = new Handler();
							handler.postDelayed(new Runnable() {
								String result;
								public Runnable setData(String result){
									this.result = result;
									return this;
								}
								@Override
								public void run() {
										Log.w("Cobro", "Cobro cancelado");
							try {
								canceladoPreference.edit().clear().commit();
								manage_CancelarPago(new JSONObject(result), cancelacion);
							} catch (JSONException e) {
								e.printStackTrace();
							}
								}
							}.setData(result), 1000);
							*/
		/**
		 * Working code
		 */
//							try {
//								canceladoPreference.edit().clear().commit();
//								manage_CancelarPago(new JSONObject(result));
//							} catch (JSONException e) {
//								e.printStackTrace();
//							}
							/*
						}

						@Override
						public void onError(Exception e) {

							cancelingTicket = false;

							Toast.makeText(ApplicationResourcesProvider.getContext(), "Error al cancelar", Toast.LENGTH_LONG).show();

							// TODO Auto-generated method stub

						}

						public onWorkCompleteListener setData(LogRegister.LogCancelacion cancelacion){
							this.cancelacion = cancelacion;
							return this;
						}
					}.setData(cancelacion));

		}
		*/

	}

	/**
	 * starts android service to print cierre informativo
	 */
	private void executePrintCierreInformativo(){
		boolean printCierrePeriodo = getArguments() != null && getArguments().getBoolean("printCierrePerido", false);

		PrintCierreInformativoOPeriodoTicketRequest printCierreInformativoRequest = new PrintCierreInformativoOPeriodoTicketRequest(
				!printCierrePeriodo,
				arrayCierre,
				coCobrador,
				nombre,
				efectivoInicial,
				mainActivity.getEfectivoPreference().getInt("periodo", 0),
				deposito);
		spicePrintCierreInformativo.executeRequest(printCierreInformativoRequest);
	}

	/**
	 * sorts made payments by contract number
	 */
	private JSONArray sortArrayCierreByContractNumber(){
		JSONArray arrayCierreSortByContractNumber = arrayCierre;

		try {
			for (int i = 0; i < arrayCierreSortByContractNumber.length() - 1; i++) {
				for (int j = 0; j < arrayCierreSortByContractNumber.length() - 1; j++) {

					JSONObject firstItem = arrayCierreSortByContractNumber.getJSONObject(j);
					JSONObject secondItem = arrayCierreSortByContractNumber.getJSONObject(j + 1);

					int firstItemContract = Integer.parseInt(firstItem.getString("no_cliente").replace("C", ""));
					int secondItemContract = Integer.parseInt(secondItem.getString("no_cliente").replace("C", ""));

					if (firstItemContract < secondItemContract) {
						arrayCierreSortByContractNumber.put(j+1, firstItem);
						arrayCierreSortByContractNumber.put(j, secondItem);
					}
				}
			}
		} catch (JSONException e){
			e.printStackTrace();
		}
		return arrayCierreSortByContractNumber;
	}

	/**
	 * starts android service to cancel payment
	 */
	private void executeCancelPayment( int paymentNumber, LogRegister.LogCancelacion cancelacion ){
		pendingCancellationRequest = true;
		clearCancelPaymentCache();
		CancelPaymentRequest cancelPaymentRequest = new CancelPaymentRequest(paymentNumber, fechaOperacion, cancelacion, cancelAmount, cancelCompany, cancelCustomerNumber, cancelDate, mainActivity.getEfectivoPreference().getInt("periodo", 0), opcionSeleccionada);
		spiceManager.execute(cancelPaymentRequest, TAG, DurationInMillis.ONE_DAY, new CancelPaymentListener());
	}

	/**
	 * pending android service
	 */
	private void executePendingCancellationRequest(){
		spiceManager.addListenerIfPending(ModelCancelPaymentResponse.class, TAG, new PendingCancelPaymentListener());
	}

	/**
	 * cleans cache with info of android service
	 */
	private void clearCancelPaymentCache(){
		try{

			Future<?> future = spiceManager.removeAllDataFromCache();
			if ( future != null ){
				future.get();
			}

		} catch ( InterruptedException e){
			e.printStackTrace();
		} catch ( ExecutionException e ){
			e.printStackTrace();
		}
	}

	/**
	 * Helpful class to manage android service's response
	 * android service which is called when is trying to cancel a payment
	 *
	 * Use
	 * implements RequestListener<ModelCancelPaymentResponse>, RequestProgressListener
	 * if progress is needed
	 */
	private class CancelPaymentListener implements RequestListener<ModelCancelPaymentResponse>{

		@Override
		public void onRequestFailure(SpiceException spiceException) {
			onCancelPaymentFailure();
		}

		@Override
		public void onRequestSuccess(ModelCancelPaymentResponse modelCancelPaymentResponse) {
			if ( modelCancelPaymentResponse.getResult() != null ) {
				onCancelPaymentSuccess();
			} else {
				pendingCancellationRequest = false;
				cancelingTicket = false;
				Util.Log.ih("Error al cancelar pago");
				try {
					Toast toast = Toast.makeText(getContext(), modelCancelPaymentResponse.getError(), Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				} catch ( Exception e ) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Helpful class to manage android service's response when app was minimized and then maximized
	 */
	private class PendingCancelPaymentListener implements PendingRequestListener<ModelCancelPaymentResponse> {

		@Override
		public void onRequestFailure(SpiceException spiceException) {
			onCancelPaymentFailure();
		}

		@Override
		public void onRequestSuccess(ModelCancelPaymentResponse modelCancelPaymentResponse) {
			onCancelPaymentSuccess();
		}

		@Override
		public void onRequestNotFound() {
			if ( pendingCancellationRequest ){
				spiceManager.getFromCache(ModelCancelPaymentResponse.class, TAG, DurationInMillis.ONE_DAY, new CancelPaymentListener());
			}
		}
	}

	/**
	 * android service
	 * an error happened
	 */
	private void onCancelPaymentFailure(){

		pendingCancellationRequest = false;
		cancelingTicket = false;

		AlertDialog.Builder errorCancelPaymentDialog = getAlertDialogBuilder()
				.setTitle("Error")
				.setMessage("Hubo un error de comunicacion con el servidor.\nVuelve a intentarlo.")
				.setPositiveButton("Aceptar", null);

		errorCancelPaymentDialog.create().show();
	}

	/**
	 * this method gets called when android service for canceling a ticket finishes
	 * either with an error or with success
	 */
	private void onCancelPaymentSuccess()
	{
		Snackbar.make(cierreLayout, "Ticket cancelado correctamente", Snackbar.LENGTH_SHORT).show();
		pendingCancellationRequest = false;

		try {
			((CierreAdapter) lvCierreWithCancelledTickets.getAdapter()).updateStatus(posicionCancelacion, "7");

			/*JSONObject params = new JSONObject();
			try {
				if (DatabaseAssistant.isThereMoreThanOnePeriodActive()) {
					params.put("periodo", new JSONArray(new Gson().toJson(DatabaseAssistant.getAllPeriodsActive())));
				} else {
					params.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
				}
				params.put("no_cobrador", mainActivity.getEfectivoPreference().getString("no_cobrador", ""));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			requestBalanceEmpresaPorCobradorVolley(params);*/



			((TextView) getView().findViewById(R.id.et_monto_cierre)).setText(mainActivity.formatMoney(DatabaseAssistant.getTotalEfectivoAcumulado()));



			cancelingTicket = false;
			ticketWasCancelled = true;


		} catch (JSONException e){
			cancelingTicket = false;
			e.printStackTrace();
		}
		//lvCierreWithCancelledTickets.setSelection(posicionCancelacion);
		//end marking cancelled ticket as "cancelado"
	}

	@Override
	public void cerrarSesionTimeOut() {
		if (mainActivity.handlerSesion != null) {
			mainActivity.handlerSesion.removeCallbacks(mainActivity.myRunnable);
			mainActivity.handlerSesion = null;

		}

		mthis = null;
		//finish();
		getFragmentManager().popBackStack();
	}

	/**
	 * counts how many tickets per company the collector has, and shows dialog with
	 * the info
	 *
	 * @throws JSONException
	 */
	public void contarTicketsDialogo() throws JSONException {
		//SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
		//JSONArray empresas = query.mostrarEmpresas();
		List<Companies> companies = DatabaseAssistant.getCompanies();

		int size;
		switch (BuildConfig.getTargetBranch()){
			case GUADALAJARA: case TAMPICO:
				size = companies.size();
				break;
			case TOLUCA:
				size = 4;
				break;
			case QUERETARO:case IRAPUATO:case SALAMANCA:case CELAYA: case LEON:
				size = 2;
				break;
			case PUEBLA:case CUERNAVACA:
				size = 4;
				break;
			case MERIDA:
				size = 4;
				break;
			case SALTILLO:
				size = 4;
				break;
			case CANCUN:
				size = 4;
				break;
			case MEXICALI: case TORREON:
				size = 3;
				break;
			case MORELIA:
				size = 3;
				break;
			default:
				size = 4;
		}

		String dialog = "Periodo: " + mainActivity.getEfectivoPreference().getInt("periodo", 0) + "\n";
		for (int x = 0; x < size ; x++) {//for (int x = 0; x < companies.size(); x++) {
			int tickets = 0;
			for (int y = 0; y < arrayCierre.length(); y++) {
				if (arrayCierre
						.getJSONObject(y)
						.getString("empresa")
						.equals(companies.get(x).getIdCompany())){
					//empresas.getJSONObject(x).get(
					//Empresa.id_empresa))) {
					if (arrayCierre.getJSONObject(y).getDouble("monto") != 0
							|| arrayCierre.getJSONObject(y).getInt("status") == 2) {
						tickets++;

					}

				}
			}
			dialog += "Tickets " + ExtendedActivity.getCompanyNameFormatted( companies.get(x).getName() ) + " : " + tickets + "\n";
		}
		try {
			mainActivity.showAlertDialog("Tickets totales", dialog, true);
		} catch (Exception e){
			Toast toast = Toast.makeText(getContext(), "Error: vuelve a intentarlo", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			e.printStackTrace();
		}
	}

	public void showPeriodOnDialog() throws JSONException {

		String dialog = "Periodo: " + mainActivity.getEfectivoPreference().getInt("periodo", 0) + "\n";
		try {
			mainActivity.showAlertDialog("", dialog, true);
		} catch (Exception e){
			Toast toast = Toast.makeText(getContext(), dialog, Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			e.printStackTrace();
		}
	}

	/**
	 * shows a dialog asking for a copie
	 */
	public void copiaImpresion() {

		mthis = this;

		mainActivity.runOnUiThread(() -> {
			final Dialog newsecondDialog = getDialog();
			newsecondDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			newsecondDialog.setContentView(R.layout.dialog_copies);
			newsecondDialog.setCancelable(false);
			newsecondDialog.show();
			TextView text = (TextView) newsecondDialog.findViewById(R.id.textView2);
			text.setText("¿Deseas imprimir una copia?");
			final TextView textc = (TextView) newsecondDialog
					.findViewById(R.id.textViewCancel);
			textc.setVisibility(View.VISIBLE);
			textc.setText("No");
			textc.setGravity(Gravity.CENTER_HORIZONTAL);
			//final SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
			textc.setOnClickListener(view -> {
				//textc.setEnabled(false);
				if (ticketWasCancelled) {
					//setResult(99);
					fragmentResult.onFragmentResult(requestCode, 99, null);
				}
				//finish();
				newsecondDialog.dismiss();
				if (MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CIERRE_INFORMATIVO) && getFragmentManager() != null)
					getFragmentManager().popBackStack();
			});
				/*
				textc.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View arg0, MotionEvent arg1) {
						if (arg1.getAction() == MotionEvent.ACTION_DOWN) {
							textc.setBackgroundColor(Color.RED);
							return true;
						}
						if (arg1.getAction() == MotionEvent.ACTION_UP) {
							textc.setEnabled(false);
							textc.setBackgroundColor(Color.WHITE);
							finish();
							return true;
						}
						return false;
					}

				});
				*/
			final TextView texta = (TextView) newsecondDialog
					.findViewById(R.id.textViewAceptar);
			texta.setText("Sí");
			texta.setOnTouchListener((view, event) -> {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					texta.setBackgroundColor(Color.RED);
					return true;
				} else {
					texta.setEnabled(false);
					texta.setBackgroundColor(Color.WHITE);

					if (arrayCierre != null && arrayCierre.length() > 400) {
						//todo service migrated to robospice

						restartVariablesToPrint();

						sortArrayCierre();
						splitArrayCierreArrangedBy400();

						if (!mainActivity.isFinishing()) {
							if (newsecondDialog != null && newsecondDialog.isShowing()) {
								newsecondDialog.dismiss();
							}
						}

						executePrintMoreThan400Payments();
					} else {
						//AsyncPrint obj = new AsyncPrint(4, mthis, getProgressDialog());
						//obj.execute();
						if (!mainActivity.isFinishing()) {
							if (newsecondDialog != null && newsecondDialog.isShowing()) {
								newsecondDialog.dismiss();
							}
						}
						executePrintCierreInformativo();
					}


					return true;
				}
			});
		});

	}

	/**
	 * gets 'noCobrador', 'nombre' and 'fechaOperacion' to end up
	 * calling {@link #listaPagosCobrador()} method where payments are downloaded
	 */
	private void descargarHistorial()
	{
		Bundle bundle = getArguments();
		if (bundle != null) {
			if (bundle.containsKey("no_cobrador")) {
				noCobrador = bundle.getString("no_cobrador");
			}
			if (bundle.containsKey("nombre")) {
				nombre = bundle.getString("nombre");
				if (nombre.equals("null")) {
					nombre = DatabaseAssistant.getCollector().getName();
				}
			}
			if (bundle.containsKey("fecha")) {
				try {
					fechaOperacion = new SimpleDateFormat("yyyy-MM-dd").format(Long.parseLong(bundle.getString("fecha")));
				} catch (NumberFormatException e) {
					fechaOperacion = bundle.getString("fecha");
				}

				getView().findViewById(R.id.imageViewBack).setVisibility(View.GONE);
			} else {
				Date date = new Date(System.currentTimeMillis());
				fechaOperacion = new SimpleDateFormat("yyyy-MM-dd")
						.format(date);
			}
			mainActivity.runOnUiThread(this::listaPagosCobrador);

		}
	}

	/**
	 * prints ticket with all payments
	 * and visits
	 *
	 * @return
	 * 		true -> succeed
	 * 		false -> failed
	 */
	public boolean imprimir() {
		try {
			Looper.prepare();
		} catch (Exception e) {
			e.printStackTrace();
		}

		//TscDll = new TSCActivity();

		float empresa4 = 0;
		float empresa5 = 0;
		float malba = 0;
		float programa = 0.0f;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat dateHora = new SimpleDateFormat("HH:mm");
		//SqlQueryAssistant query = new SqlQueryAssistant(ApplicationResourcesProvider.getContext());
		Date date = new Date();
		int power = 0;
		boolean ban = mthis != null;
		while (ban && power < 100) {

			TscDll = new TSCActivity();

			try {
				dontexit = false;
				if (arrayCierre == null) {
					arrayCierre = new JSONArray();
				}
				int size = arrayCierre.length();
				// size = ;
				size = size * 4 + 130;

				dateAux = new Date().getTime();

				//timer that closes printer port if got stuck
				//trying to print
				//has one minute of limit before closing port
				//closing port causes NullPointerException on openPort()
				//so it tries again to print
				timer = new Timer();
				timer.schedule(new TimerTask() {

					String mac;
					int attempt;
					TSCActivity tscDll;

					@Override
					public void run() {
						if (checkDate()) {
							Util.Log.ih("Closing port");
							//disableBluetooth();
							try {
								tscDll.closeport();
							} catch (Exception e){
								e.printStackTrace();
								timer.cancel();
							}
						}
						Util.Log.ih("Hora, attempt: " + attempt);
					}

					public TimerTask setData(int attempt, TSCActivity tscDll) {
						this.attempt = attempt;
						this.tscDll = tscDll;
						return this;
					}
				}.setData(power, TscDll), (1000 * 60));

				TscDll.openport(mac);
				Util.Log.ih("despues de openPort");
				timer.cancel();

				TscDll.setup(Printer.width,
						size,
						2,
						Printer.density,
						Printer.sensorType,
						Printer.gapBlackMarkVerticalDistance,
						Printer.gapBlackMarkShiftDistance);

				//JSONArray empresas = query.mostrarEmpresas();
				List<Companies> companies = DatabaseAssistant.getCompanies();
				TscDll.clearbuffer();
				Log.i("PRINT STATUS", TscDll.status());

				TscDll.sendcommand("GAP 0,0\n");
				TscDll.sendcommand("CLS\n");
				TscDll.sendcommand("CODEPAGE UTF-8\n");
				TscDll.sendcommand("SET TEAR ON\n");

				TscDll.sendcommand("SET COUNTER @1 1\n");
				JSONObject totales = new JSONObject();

				TscDll.printerfont(50, 10, "4", 0, 1, 1, "CIERRE INFORMATIVO");
				int x = 70;
				for (int contE = 0; contE < 1; contE++) {//for (int contE = 0; contE < companies.sise(); contE++) {
					double monto = 0;
					;
					//JSONObject empresa = empresas.getJSONObject(contE);
					Companies company = companies.get(contE);
					Log.e("empresa", company.toString());
					TscDll.printerfont(400, x + 10, "2", 0, 1, 1,
							dateFormat.format(date));
					TscDll.printerfont(480, x + 70, "2", 0, 1, 1,
							dateHora.format(date));
					TscDll.printerfont(10, x + 10, "2", 0, 1, 1, coCobrador);
					TscDll.printerfont(10, x + 40, "2", 0, 1, 1, nombre);
					TscDll.printerfont(10, x + 70, "2", 0, 1, 1, "Periodo "
							+ (mainActivity.getEfectivoPreference().getInt("periodo", 0)));

					x += 150;
					int tickets = 0;
					TscDll.printerfont(10, x, "3", 0, 1, 1,
							//empresa.getString(Empresa.nombre_empresa)
							company.getName().toUpperCase());
					int ticketpos = x;
					x += 30;
					TscDll.printerfont(10, x, "2", 0, 1, 1, "No. Contrato");
					TscDll.printerfont(200, x, "2", 0, 1, 1, "Folio");
					TscDll.printerfont(350, x, "2", 0, 1, 1, "Importe");
					TscDll.printerfont(480, x, "2", 0, 1, 1, "Copias");

					x += 30;
					for (int cont = 0; cont < arrayCierre.length(); cont++) {
						JSONObject json = arrayCierre.getJSONObject(cont);
						Log.e(json.getString("empresa"),
								company.getIdCompany());
						//empresa.getString(Empresa.id_empresa));

						if (json.getString("empresa").equals(
								company.getIdCompany())) { // Programa
							//empresa.getString(Empresa.id_empresa))) {// Programa

							Log.e("json", json.toString());
							if (json.getDouble("monto") != 0) {
								tickets++;

								TscDll.printerfont(10, x, "2", 0, 1, 1,
										json.getString("no_cliente"));
								TscDll.printerfont(200, x, "2", 0, 1, 1,
										json.getString("folio"));
								TscDll.printerfont(480, x, "2", 0, 1, 1,
										json.getString("copias"));
								if (json.getInt("status") == 7) {
									TscDll.printerfont(350, x, "2", 0, 1, 1,
											"Cancelado");

								} else {
									TscDll.printerfont(
											350,
											x,
											"2",
											0,
											1,
											1,
											"$"
													+ mainActivity.formatMoney(Double.parseDouble(json
													.getString("monto"))));

								}

							} else {
								tickets++;
								TscDll.printerfont(10, x, "2", 0, 1, 1,
										json.getString("no_cliente"));
								//if (json.getInt("status") == 2) {
								//	TscDll.printerfont(200, x, "2", 0, 1, 1,
								//			"NO APORTÓ");
								if (json.getInt("status") == 8) {
									TscDll.printerfont(200, x, "2", 0, 1, 1,
											"NO EXISTE CLIENTE");
								} else if (json.getInt("status") == 9) {
									TscDll.printerfont(200, x, "2", 0, 1, 1,
											"NO EXISTE DIRECCION");
								} else {
									TscDll.printerfont(200, x, "2", 0, 1, 1,
											"NO APORTÓ");
								}
							}

							x = x + 30;
							if (json.getInt("status") == 1) {
								monto += json.getDouble("monto");
							}

						}
					}
					TscDll.printerfont(350, ticketpos, "2", 0, 1, 1, "Tickets "
							+ tickets);
					TscDll.printerfont(225, x, "2", 0, 1, 1, "Subtotal $"
							+ mainActivity.formatMoney(Double.parseDouble("" + monto)));
					x = x + 70;
					totales.put(company.getName(), monto);

				}
				x += 50;
				TscDll.printerfont(10, x, "2", 0, 1, 1, "Efectivo Inicial");
				TscDll.printerfont(350, x, "2", 0, 1, 1, "$"
						+ mainActivity.formatMoney(Double.parseDouble("" + efectivoInicial)));

				x += 50;
				TscDll.printerfont(350, x, "2", 0, 1, 1, "TOTALES");
				x += 30;
				TscDll.printerfont(10, x, "2", 0, 1, 1, "DEPOSITOS");

				TscDll.printerfont(350, x, "2", 0, 1, 1, "$"
						+ mainActivity.formatMoney(Double.parseDouble("" + deposito)));
				x += 30;
				TscDll.printerfont(10, x, "2", 0, 1, 1, "EFECTIVO INICIAL");
				TscDll.printerfont(350, x, "2", 0, 1, 1, "$"
						+ mainActivity.formatMoney(Double.parseDouble("" + efectivoInicial)));
				x += 30;
				TscDll.printerfont(10, x, "2", 0, 1, 1, "EFECTIVO FINAL");

				float totalempresas = 0;
				for (int i = 0; i < 1; i++) {//for (int i = 0; i < companies.size(); i++) {
					//JSONObject empresa = empresas.getJSONObject(i);
					Companies company = companies.get(i);
					totalempresas += totales.getDouble(company.getName());
					//.getString(Empresa.nombre_empresa));
				}
				double sumaTotal = deposito
						+ (totalempresas + efectivoInicial - deposito);
				TscDll.printerfont(350, x, "2", 0, 1, 1, "$"
						+ mainActivity.formatMoney(sumaTotal - deposito));
				x += 30;

				TscDll.printerfont(250, x, "2", 0, 1, 1, "TOTAL: $"
						+ mainActivity.formatMoney(Double.parseDouble("" + sumaTotal)));
				x += 30;
				TscDll.printerfont(150, x, "2", 0, 1, 1, "TOTALES POR EMPRESA");
				x += 30;
				Log.i("PRINT STATUS", TscDll.status());
				float totalfinal = 0;
				for (int i = 0; i < 1; i++) {//for (int i = 0; i < companies.size(); i++) {
					//JSONObject empresa = empresas.getJSONObject(i);
					Companies company = companies.get(i);

					TscDll.printerfont(10, x, "2", 0, 1, 1,
							company.getName());
					TscDll.printerfont(
							350,
							x,
							"2",
							0,
							1,
							1,
							"$"
									+ mainActivity.formatMoney(Double.parseDouble(""
									+ totales.getDouble(company.getName()))));
					totalfinal += totales.getDouble(company.getName());
					x += 30;
				}

				TscDll.printerfont(150, x, "2", 0, 1, 1, "TOTAL DEL DIA: $"
						+ mainActivity.formatMoney(Double.parseDouble("" + totalfinal)));
				x += 30;
				TscDll.printerfont(150, x, "2", 0, 1, 1,
						"DEPOSITOS POR EMPRESA");
				x += 30;

				for (int i = 0; i < 1; i++) {//for (int i = 0; i < companies.sise(); i++) {
					//JSONObject empresa = empresas.getJSONObject(i);
					Companies company = companies.get(i);

					TscDll.printerfont(10, x, "2", 0, 1, 1,
							company.getName());
					TscDll.printerfont(
							350,
							x,
							"2",
							0,
							1,
							1,
							"$"
									+ mainActivity.formatMoney(Double.parseDouble(""
									+ DatabaseAssistant.getTotalDepositsByCompany(company.getIdCompany()))));
					//+ query.getTotalDepositosPorEmpresa(empresa
					//.getString(Empresa.id_empresa)))));
					x += 30;

				}
				TscDll.printlabel(1, 1);

				//TscDll.clearbuffer();

				Log.i("PRINT STATUS", TscDll.status());

				//TscDll.closeport();
				ban = false;
				return true;
			} catch (Exception e) {

				dontexit = true;
				Util.Log.ih("intento = " + power);
				enableBluetooth();

				e.printStackTrace();
				power++;
			}
		}
		dontexit = true;
		return false;
	}

	/**
	 * disables bluetooth
	 */
	public void disableBluetooth() {

		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
				.getDefaultAdapter();
		mBluetoothAdapter.disable();
	}

	/**
	 * enables bluetooth
	 */
	public void enableBluetooth() {

		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (!mBluetoothAdapter.isEnabled()) {
			Util.Log.ih("enabling bluetooth");
			mBluetoothAdapter.enable();
		}
	}

	/**
	 * checks if more than 59 minutes have passed
	 * @return
	 *          true -> difference is more than limit
	 *          false -> difference is under limit
	 */
	public boolean checkDate() {
		long newTime = new Date().getTime();
		long differences = Math.abs(dateAux - newTime);
		return (differences > (1000 * 59));
	}

	/**
	 * sorts 'arrayCierreArranged' variable by
	 * companies id, so that they'll be printed
	 * in order
	 */
	public void sortArrayCierre(){
		JSONArray temp = new JSONArray();
		JSONObject objectFromArray = new JSONObject();
		arrayCierreArranged = new JSONArray();
		String empresaId = "";

		List<Companies> companies;
		Companies company;

		temp = arrayCierre;
		companies = DatabaseAssistant.getCompanies();

		for( int j = 0; j < companies.size(); j++ )
		{
			company = companies.get(j);
			empresaId = company.getIdCompany();

			for (int i = 0; i < temp.length(); i++)
			{
				try {
					objectFromArray = temp.getJSONObject(i);

					if (objectFromArray.getString("empresa").equals(empresaId)) {
						arrayCierreArranged.put(objectFromArray);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		Util.Log.ih(arrayCierreArranged.toString());
	}

	/**
	 * splits 'arrayCierreArranged' variable
	 * by 400, so that only 400 payments and
	 * visits are printed per ticket
	 */
	public void splitArrayCierreArrangedBy400(){
		arrayCierreCut = new JSONArray();

		int limitToCut = sectionOfArray * 400;
		int index = limitToCut - 400;

		for( ; index < limitToCut && index < arrayCierreArranged.length(); index++ ){
			try{
				arrayCierreCut.put(arrayCierreArranged.getJSONObject(index));

			} catch (JSONException e){
				e.printStackTrace();
			}
		}

		Util.Log.ih(arrayCierreCut.toString());
	}

	/**
	 * starts android service to print ticket in another thread
	 */
	private void executePrintMoreThan400Payments(){
		PrintMoreThan400PaymentsCierreInformativoRequest printMoreThan400PaymentsCierreInformativoRequest = new PrintMoreThan400PaymentsCierreInformativoRequest();
		spicePrintMoreThan400Payments.executeRequest(printMoreThan400PaymentsCierreInformativoRequest);
	}

	/**
	 * starts android service to print ticket in another thread
	 */
	/*
	private void executePrintMoreThan400PaymentsService(){
		//clearPrintMoreThan400PaymentsCache();
		PrintMoreThan400PaymentsInformativoRequest request = new PrintMoreThan400PaymentsInformativoRequest();
		getSpiceManager().execute(request, new PrintMoreThan400PaymentsListener());
	}
	*/

	@Override
	public void onStart() {
		super.onStart();

		//spiceManagerOffline.start(ApplicationResourcesProvider.getContext());
		spiceManager.start(ApplicationResourcesProvider.getContext());

		if ( pendingCancellationRequest ){
			executePendingCancellationRequest();
		}
		else if (spicePrintCierreInformativo.isRequestPending()) {
			spicePrintCierreInformativo.executePendingRequest();
		}
		else if (spicePrintMoreThan400Payments.isRequestPending()) {
			spicePrintMoreThan400Payments.executePendingRequest();
		}
	}

	@Override
	public void onStop() {

		//spiceManagerOffline.shouldStop();
		spiceManager.shouldStop();

		if (DatabaseAssistant.isThereMoreThanOnePeriodActive())
		{
			if (spinnerPeriodSelector != null && spinnerPeriodSelector.getAdapter() != null) {
				ArrayAdapter<String> adapter = (ArrayAdapter<String>) spinnerPeriodSelector.getAdapter();
				int size = adapter.getCount();
				List<String> periods = new ArrayList<>();

				for (int i = 0; i < size; i++) {
					periods.add(adapter.getItem(i));
				}

				Collections.sort(periods, Collections.reverseOrder());

				if (periods.size() > 0) {
					Log.d("PeriodSpinnerItem", periods.get(0));
					mainActivity.getEfectivoEditor().putInt("periodo", Integer.parseInt(periods.get(0))).apply();
				}
			}
		}

		super.onStop();
	}

	@Override
	public void onPause() {
		super.onPause();

	}

	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
		super.onSaveInstanceState(outState);
	}

	int updateCompanyCashFlag = 0;

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
		Util.Log.ih("spinner selected = " + adapterView.getItemAtPosition(i));
		updateCompanyCashFlag ++;
		try {
			mainActivity.getEfectivoEditor().putInt("periodo", Integer.parseInt((String)adapterView.getItemAtPosition(i))).apply();
			manage_GetListaPagosByCobrador( arrayCierreList.get( adapterView.getItemAtPosition(i) ) );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {
		//nothing here...
	}

	private int arrayListSelected = 0;

	/**
	 * checks cash of companies compared with server
	 */
	private void checkPayments() {
		//Preferences preferencesSetCompleteAmounts = new Preferences(ApplicationResourcesProvider.getContext());
		//preferencesSetCompleteAmounts.saveSetCompleteAmount(true);

		JSONObject json = new JSONObject();

		try {
			if (DatabaseAssistant.isThereMoreThanOnePeriodActive()) {
				List<ModelPeriod> modelPeriodList = DatabaseAssistant.getAllPeriodsActive();
				json.put("periodo", new JSONArray(new Gson().toJson(modelPeriodList)));

				periodsActive = DatabaseAssistant.getNumberOfPeriodsActive();

			} else {
				json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
			}
			json.put("no_cobrador", mainActivity.getEfectivoPreference().getString("no_cobrador", ""));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		requestBalanceEmpresaPorCobradorYPeriodo(json);
	}
	private void requestBalanceEmpresaPorCobradorYPeriodo(JSONObject jsonParams)
	{
		//progress = ProgressDialog.show(mainActivity, "Cargando balance", "Por favor espera...", true);
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlGetBalanceEmpresaPorCobradorYPeriodo, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				Log.e("result", response.toString());
				JSONArray jsonDatosPorEmpresa;
				JSONObject jsonResult;
				try
				{
					jsonResult = response;
					Iterator<String> iter;
					for (iter = jsonResult.keys(); iter.hasNext(); arrayListSelected++)
					{
						Util.Log.ih("arrayListSelected" + arrayListSelected);
						String key;
						if (DatabaseAssistant.isThereMoreThanOnePeriodActive())
						{
							DatabaseAssistant.deleteAllCanceled();
							mainActivity.getEfectivoEditor().putInt("periodo", DatabaseAssistant.getAllPeriodsActive().get(arrayListSelected).getPeriodNumber()).apply();
							key = "result" + DatabaseAssistant.getAllPeriodsActive().get(arrayListSelected).getPeriodNumber();
							iter.next();
						} else {
							key = iter.next();
						}

						jsonDatosPorEmpresa = jsonResult.getJSONArray(key);
						Util.Log.ih("empresas = " + jsonDatosPorEmpresa);
						Util.Log.ih("key period = " + key.substring(6));
						SharedPreferences.Editor efectivoEditor = mainActivity.getEfectivoEditor();

						try
						{
							for (int i = 0; i < jsonDatosPorEmpresa.length(); i++)
							{

								JSONObject objetoEmpresa;
								try
								{
									objetoEmpresa = jsonDatosPorEmpresa.getJSONObject(i);
									Util.Log.ih("empresa.getString(empresa) -> " + objetoEmpresa.getString("empresa"));

									objetoEmpresa = jsonDatosPorEmpresa.getJSONObject(i);
									String empresaRespuesta= objetoEmpresa.getString("empresa");
									float cobrosRespuesta = (float) objetoEmpresa.getDouble("cobros");
									float depositosRespuesta = (float) objetoEmpresa.getDouble("depositos");
									float cobrosCanceladosRespuesta = (float) objetoEmpresa.getDouble("cobros_cancelados");
									float cobrosSumandoCanceladosRespuesta = (float) objetoEmpresa.getDouble("cobros_sumando_cancelados");
									float totalCobrosMenosDepositosRespuesta = (float) objetoEmpresa.getDouble("total_cobros_menos_depositos");
									float cobrosMenosDepositosRespuesta = (float) objetoEmpresa.getDouble("cobros_menos_depositos");

									String nombre_de_la_empresa_registrada_en_base_de_datos = DatabaseAssistant.getSingleCompany(empresaRespuesta).getName();

									if(DatabaseAssistant.hayRegistrosConElPeriodoActual(mainActivity.getEfectivoPreference().getInt("periodo", 0), empresaRespuesta))
									{
										String queryActualizacion="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET cobros ='"+cobrosRespuesta+"', depositos='"+depositosRespuesta
												+"', cancelados='"+cobrosCanceladosRespuesta+"', cobrosMasCancelados='"+cobrosSumandoCanceladosRespuesta+"', totalCobrosMenosDepositos='"+totalCobrosMenosDepositosRespuesta
												+"', cobrosMenosDepositos='"+cobrosMenosDepositosRespuesta+"' WHERE periodo='"+mainActivity.getEfectivoPreference().getInt("periodo", 0)+"' and empresa='"+empresaRespuesta+"'";
										MontosTotalesDeEfectivo.executeQuery(queryActualizacion);
									}
									else
									{
										DatabaseAssistant.insertarMontosTotalesPorEmpresaYPeriodo(
												""+ empresaRespuesta,
												""+ cobrosRespuesta,
												""+depositosRespuesta,
												""+cobrosCanceladosRespuesta,
												""+ cobrosSumandoCanceladosRespuesta,
												""+totalCobrosMenosDepositosRespuesta,
												""+ cobrosMenosDepositosRespuesta,
												""+ mainActivity.getEfectivoPreference().getInt("periodo", 0));
									}
									efectivoEditor.putFloat("cobros "+nombre_de_la_empresa_registrada_en_base_de_datos, cobrosRespuesta).apply();
									efectivoEditor.putFloat("depositos" +nombre_de_la_empresa_registrada_en_base_de_datos, depositosRespuesta).apply();
									efectivoEditor.putFloat("cancelados " +nombre_de_la_empresa_registrada_en_base_de_datos, cobrosCanceladosRespuesta).apply();
									efectivoEditor.putFloat("cobrosMasCancelados "+nombre_de_la_empresa_registrada_en_base_de_datos, cobrosSumandoCanceladosRespuesta).apply();
									efectivoEditor.putFloat("cobrosMenosCanceladosMenosDepositos "+nombre_de_la_empresa_registrada_en_base_de_datos, totalCobrosMenosDepositosRespuesta).apply();
									efectivoEditor.putFloat("cobrosMenosDepositos "+nombre_de_la_empresa_registrada_en_base_de_datos, cobrosMenosDepositosRespuesta).apply();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							descargarHistorial();

						} catch (NullPointerException e) {
							e.printStackTrace();
							Log.i("Error-->", e.toString());
						}




					}
				} catch (JSONException e) {
					//nothing here...
				}


				if (DatabaseAssistant.isThereMoreThanOnePeriodActive())
				{
					Util.Log.dh("spinner");
					//spinnerPeriodSelector = (Spinner) getView().findViewById(R.id.spinnerPeriods);

					frameLayoutForSpinner.setVisibility(View.VISIBLE);

					List<ModelPeriod> modelPeriodList = DatabaseAssistant.getAllPeriodsActive();

					String[] items = new String[modelPeriodList.size()];

					int i;
					for (i = 0; i < modelPeriodList.size(); i++) {
						items[i] = "" + modelPeriodList.get(i).getPeriodNumber();
					}

					ArrayAdapter<String> adapter = new ArrayAdapter<String>(mainActivity, android.R.layout.simple_spinner_item, items); //anteriormente tenia getContext()
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnerPeriodSelector.setAdapter(adapter);
					spinnerPeriodSelector.setOnItemSelectedListener(CierreInformativo.this);
				}
				//progress.dismiss();
				Log.i("Request-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(null, "Error: checkPayments");
						mostrarMensajeError(ConstantsPabs.listaPagosCobrador);
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
	}


	/**
	 * downloads all payment and visits made
	 *
	 * Web Service
	 * 'http://50.62.56.182/ecobro/controlpagos/cobrosPeriodo'
	 *
	 * JSONObject param
	 * {
	 *     "no_cobrador": "",
	 *     ""periodo":
	 * }
	 */
	public void listaPagosCobrador()
	{
		JSONObject json = new JSONObject();
		try {
			json.put("no_cobrador", noCobrador);
			json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
		} catch (Exception e) {
			e.printStackTrace();
		}
		requestListaPagosCobrador(json);
	}

	private void requestListaPagosCobrador(JSONObject jsonParams)
	{
		progress = ProgressDialog.show(mainActivity, "Cargando lista", "Por favor espera...", true);
		progress.setCancelable(false);
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlListaPagosCobrador, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				try {

					Util.Log.eh("onCompletion");
					Util.Log.eh(response.toString());

					try {
						arrayCierreList.put(response.getString("periodo"), response);
						if(progress!=null)
							progress.dismiss();
					} catch (JSONException e) {
						e.printStackTrace();
						if(progress!=null)
							progress.dismiss();
					}

					if ( !DatabaseAssistant.isThereMoreThanOnePeriodActive() ) {

						manage_GetListaPagosByCobrador(response);
						if(progress!=null)
							progress.dismiss();
					} else {
						periodsCounter += 1;
						if (periodsCounter == periodsActive) {
							Util.Log.eh("setSelection " + (periodsActive - 1));
							spinnerPeriodSelector.setSelection( periodsActive - 1 );
							Util.Log.ih("spinnerPeriodSelector.getSelectedItem() = " + spinnerPeriodSelector.getSelectedItem());
							if(progress!=null)
								progress.dismiss();
						}
						Util.Log.eh("no entro en manage_GetListaPagosByCobrador");
						if(progress!=null)
							progress.dismiss();
					}
				} catch (Exception e) {
					e.printStackTrace();
					if(progress!=null)
						progress.dismiss();
				}
				if(progress!=null)
					progress.dismiss();
				Log.i("Request LOGIN-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(null, "Error: listaPagosCobrador");
						mostrarMensajeError(ConstantsPabs.listaPagosCobrador);
						if(progress!=null)
							progress.dismiss();
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
	}

	/**
	 * fills ListView with payments and visits made
	 * within period
	 *
	 * @param json
	 * 			Web Service response
	 */
	private void llenarLista(JSONObject json)
	{
		/**
		 * {
		 *   "result": [
		 *     {
		 *       "no_contrato": "777777",
		 *       "nombre": "Prueba7  ",
		 *       "no_cobro": "47473857",
		 *       "no_cliente": "J777777",
		 *       "folio": "NNM,1",
		 *       "copias": "0",
		 *       "monto": "100",
		 *       "status": "1",
		 *       "empresa": "04",
		 *       "tiempo": "2020-01-15 13:21:06",
		 *       "tipo_cobro": "1"
		 *     },
		 *     {
		 *       "no_contrato": "999999",
		 *       "nombre": "Prueba  ",
		 *       "no_cobro": "47473847",
		 *       "no_cliente": "J999999",
		 *       "folio": "NNM1",
		 *       "copias": "0",
		 *       "monto": "100",
		 *       "status": "1",
		 *       "empresa": "01",
		 *       "tiempo": "2020-01-15 13:20:59",
		 *       "tipo_cobro": "1"
		 *     }
		 *   ],
		 *   "result_active_period": [
		 *     {
		 *       "id": "330379",
		 *       "no_cobrador": "99999",
		 *       "fecha_inicio_periodo": "2019-12-20 13:35:13",
		 *       "fecha_fin_periodo": null,
		 *       "no_periodo": "1"
		 *     }
		 *   ],
		 *   "periodo": "1"
		 * }
		 */
		if (arrayCierre != null) {
			arrayCierre = null;
		}
		try {

			DatabaseAssistant.deleteAllCanceled();
			arrayCierre = json.getJSONArray("result");

			boolean cancellationAvailable = true;
			try {
				if (json.has("result_active_period")) {
					JSONObject resultActivePeriod = json.getJSONArray("result_active_period").getJSONObject(0);
					if (resultActivePeriod.getString("fecha_fin_periodo").length() > 4) {
						Util.Log.ih("cancellation not available");
						cancellationAvailable = false;
					}
				} else {
					Util.Log.ih("no tiene result_active_period");
				}
			} catch (JSONException e){
				e.printStackTrace();
			}

			CierreAdapter adapter = new CierreAdapter(getContext(), this, arrayCierre, cancellationAvailable);
			lvCierreWithCancelledTickets = (ListView) getView().findViewById(R.id.lv_cierre);
			lvCierreWithCancelledTickets.setAdapter(adapter);
			lvCierreWithCancelledTickets.setSelection(0);
			posicionCancelacion = 0;
			ticketsPrograma = 0;
			ticketsMalba = 0;

			//Preferences preferencesCompleteAmounts = new Preferences(ApplicationResourcesProvider.getContext());

			for (int cont = 0; cont < arrayCierre.length(); cont++)
			{
				JSONObject jsonTmp = arrayCierre.getJSONObject(cont);

				// Log.e("incierre Programa" + jsonTmp.getInt("inCierre"),
				// jsonTmp.getString("no_contrato"));
				if (jsonTmp.getInt("status") == 7)
				{
					// ESTO ENTRA CUANDO EL TICKET ESTA CANCELADO, tenemos que actualizar el monto del cancelado en caso de asi hacerlo
					Log.d("tickeCancelado Programa", "" + jsonTmp.toString(1));
					String monto = jsonTmp.getString("monto");
					String empresa = jsonTmp.getString("empresa");
					String noCliente = jsonTmp.getString("no_cliente");
					String paymentDate = jsonTmp.getString("tiempo");
					//query.insertarTicketCancelado(monto, empresa, noCliente);
					DatabaseAssistant.insertCanceledPaymentFromCierre(monto, empresa, noCliente, paymentDate, mainActivity.getEfectivoPreference().getInt("periodo", 0), "");


					/**
					 * Sums the amount of the cancelled payment to the company it belongs.
					 * This process is necesary when the application data is erased
					 * because if not, there will be a problem with the getTotalEfectivo() method
					 * which calculates final cash to deposit either on a bank or office.
					 *
					 * This process will be only done if application data was erased
					 */

					/*if ( DatabaseAssistant.isThereMoreThanOnePeriodActive())
					{
						Util.Log.ih("suming up canceled amount");
						if ( updateCompanyCashFlag < 4 )
						{
							DatabaseAssistant.updateTotalByCompanyWhenMoreThanOnePeriodIsActive(
									DatabaseAssistant.getSingleCompany(empresa).getName(),
									Float.parseFloat(monto),
									mainActivity.getEfectivoPreference().getInt("periodo", 0));


							String queryActualizacion="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET cancelados='"+monto+"' WHERE periodo='"+ mainActivity.getEfectivoPreference().getInt("periodo", 0) +"' and empresa='"+ empresa +"'";
							MontosTotalesDeEfectivo.executeQuery(queryActualizacion);
						}

					}
					else {
						float efectivo = mainActivity.getEfectivoPreference().getFloat(DatabaseAssistant.getSingleCompany(empresa).getName(), 0);
						efectivo += Float.parseFloat(monto);

						mainActivity.getEfectivoEditor().putFloat(DatabaseAssistant.getSingleCompany(empresa).getName(), efectivo);
						mainActivity.getEfectivoEditor().apply();
					}*/

					//float sumaCancelados = DatabaseAssistant.getTotalCanceledByCompany(empresa) + Float.valueOf(monto);
					//String queryActualizacion="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET cancelados='"+sumaCancelados+"' WHERE periodo='"+ mainActivity.getEfectivoPreference().getInt("periodo", 0) +"' and empresa='"+ empresa +"'";
					//MontosTotalesDeEfectivo.executeQuery(queryActualizacion);

				}
			}

			//preferencesCompleteAmounts.saveSetCompleteAmount(false);

			Util.Log.ih("showing gettotalefectibvo");

			if ( DatabaseAssistant.isThereMoreThanOnePeriodActive() )
			{
				Util.Log.ih("showing gettotalefectibvo PERIODOS");

				((TextView) getView().findViewById(R.id.et_monto_cierre))
						.setText(mainActivity.formatMoney(ExtendedActivity.getTotalEfectivoWhenMoreThanOnePeriodIsActive(mainActivity.getEfectivoPreference().getInt("periodo", 0))));
			} else {
				Util.Log.ih("showing gettotalefectibvo NORMAL");

				((TextView) getView().findViewById(R.id.et_monto_cierre))
						.setText(mainActivity.formatMoney(DatabaseAssistant.getTotalEfectivoAcumulado()));
			}

		} catch (JSONException e) {

			//write stacktrace into a .txt file
			//StackTraceHandler writeStackTrace = new StackTraceHandler();
			//writeStackTrace.uncaughtException("CIERREINFORMATIVO.LlenarLista", e);

			e.printStackTrace();
		}catch (NullPointerException e){
			e.printStackTrace();
		}
	}

	/**
	 * gets and processes response from Web Service
	 * if response is successful, calls {@link #llenarLista(JSONObject)}
	 * to fill ListView with all payments and visits, then sets listener
	 * for 'imageViewTicketButton' with number of tickets calling
	 * {@link #contarTicketsDialogo()}
	 *
	 * @param json Web Service response
	 */
	public void manage_GetListaPagosByCobrador(JSONObject json)
	{
		Util.Log.ih("result = " + json.toString());
		if (json.has("result")) {

			llenarLista(json);

			if (getView() != null) {
				LinearLayout ticketButton = (LinearLayout) getView().findViewById(R.id.linearLayoutImageViewTicketButton);
				ticketButton.setOnClickListener(view -> {
					try {
						contarTicketsDialogo();
					} catch (JSONException e) {
						e.printStackTrace();
					}
				});
				Button cierre = (Button) getView().findViewById(R.id.btn_efectuar_cierre);
				cierre.setEnabled(true);
				cierre.setVisibility(View.VISIBLE);
			}

		} else if (json.has("error")) {

			//Preferences preferencesCompleteAmounts = new Preferences(ApplicationResourcesProvider.getContext());
			//preferencesCompleteAmounts.saveSetCompleteAmount(false);

			try {
				String motivo = json.getString("error");
				if (motivo.equals("No se obtuvieron registros") && getView() != null) {

					/////
					arrayCierre = new JSONArray();

					CierreAdapter adapter = new CierreAdapter(ApplicationResourcesProvider.getContext(), this, arrayCierre,
							true);
					lvCierreWithCancelledTickets = (ListView) getView().findViewById(R.id.lv_cierre);
					lvCierreWithCancelledTickets.setAdapter(adapter);
					/////

					mostrarMensajeSinCoincidencias(motivo);
					Button cierre = (Button) getView().findViewById(R.id.btn_efectuar_cierre);
					cierre.setEnabled(true);
					cierre.setVisibility(View.VISIBLE);

					((TextView) getView().findViewById(R.id.et_monto_cierre))
							.setText(
									mainActivity.formatMoney(
											ExtendedActivity.getTotalEfectivoWhenMoreThanOnePeriodIsActive(
													mainActivity.getEfectivoPreference().getInt("periodo", 0))));

					LinearLayout ticketButton = (LinearLayout) getView().findViewById(R.id.linearLayoutImageViewTicketButton);
					ticketButton.setOnClickListener(view -> {
						try {
							showPeriodOnDialog();
						} catch (JSONException e) {
							e.printStackTrace();
						}
					});
				} else {
					mostrarMensajeError(ConstantsPabs.listaPagosCobrador);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			mostrarMensajeSinCoincidencias("Hubo un error.");
		}

	}

	/**
	 * returns an instance of Dialog
	 * @return instance of Dialog
	 */
	private Dialog getDialog(){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
			return new Dialog(getContext());
		}
		else {
			return new Dialog(getContext());
		}
	}

	/**
	 * returns an instance of AlertDialogBuilder
	 * @return instance of AlertDialogBuilder
	 */
	private AlertDialog.Builder getAlertDialogBuilder(){
		if (getContext() != null) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				return new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Light_Dialog_Alert);
			} else {
				return new AlertDialog.Builder(getContext());
			}
		}

		return null;
	}

	/**
	 * returns an instance of ProgressDialog
	 * @return instance of ProgressDialog
	 */
	private ProgressDialog getProgressDialog(){
		return new ProgressDialog(getContext());
	}

	/**
	 * Manages response of web service
	 * if failed to send payments, it will try to send them again,
	 * if not, will set them as sync
	 *
	 * @param json
	 * 			JSONObject with response of web service
	 */
	public void manage_RegisterPagosOffiline(JSONObject json) {
		Log.e("result", json.toString());
		if (json.has("result")) {
			try {
				if(progress!= null)
					progress.dismiss();
				JSONArray arrayPagosFallidos = json.getJSONArray("result");
				if (arrayPagosFallidos.length() > 0) {
					AlertDialog.Builder alert = getAlertDialogBuilder();
					alert.setMessage("No se pudieron sincronizar los cobros hechos en modo offline.");
					alert.setPositiveButton("Reintentar", (dialog, which) -> registerPagos());
					alert.setNegativeButton("Regresar", (dialog, which) -> {
						mthis = null;
						//finish();
						getFragmentManager().popBackStack();
					});
					alert.create().show();
				} else {
					sincronizarPagosRegistrados(json);

					//if (Clientes.getMthis().isConnected) {
					if (mainActivity.isConnected) {
						enviarDepositos();
					}
					else {
						try {
							AlertDialog.Builder alertInternet = getAlertDialogBuilder();

							if (alertInternet != null) {
								alertInternet.setMessage(getResources().getString(
										R.string.error_message_network));
								alertInternet.setPositiveButton("Salir", (dialog, which) -> {
									mthis = null;
									//finish();

									if (getFragmentManager() != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CIERRE_INFORMATIVO)) {
										getFragmentManager().popBackStack();
									}
								});
								alertInternet.show();
							}
						} catch (Exception e){
							e.printStackTrace();
							Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_network, Toast.LENGTH_LONG);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							mthis = null;
							//finish();
							if (getFragmentManager() != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CIERRE_INFORMATIVO))
								getFragmentManager().popBackStack();
						}
					}
					//checkPayments();
					//descargarHistorial();
						/*
					} else {
						try {
							showAlertDialog(
									"",
									getResources().getString(
											R.string.message_verify_internet),
									false);
						} catch (Exception e){
							Toast toast = Toast.makeText(getApplicationContext(), R.string.message_verify_internet, Toast.LENGTH_LONG);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							e.printStackTrace();
						}
					}
					*/
				}

			} catch (JSONException e) {
				e.printStackTrace();
				if(progress!= null)
					progress.dismiss();
			}

		} else {
			if(progress!= null)
				progress.dismiss();
			Util.Log.eh("ERROR: manage_RegisterPagosOffiline");
			mostrarMensajeError(ConstantsPabs.registerPagosOffiline);
		}
	}

	/**
	 * deprecated in 3.0
	 */
	public void manageObtenerDepositos(JSONObject rs) {
		try {
			//deposito = new SqlQueryAssistant(ApplicationResourcesProvider.getContext()).getTotalDepositos();
			deposito = DatabaseAssistant.getTotalDeposits();
		} catch (Exception e) {
			deposito = 0;
			e.printStackTrace();
		}
	}

	/**
	 * shows error message
	 * @param caso constant when failed
	 */
	public void mostrarMensajeError(final int caso) {
		AlertDialog.Builder alert = getAlertDialogBuilder();
		if (alert != null) {
			alert.setMessage("Hubo un error de comunicación con el servidor, verifique sus ajustes de red o intente más tarde.");
			alert.setPositiveButton("Salir", (dialog, which) -> {
				View refMain = null;
				if (Clientes.getMthis() != null && Clientes.getMthis().getView() != null) {
					/*refMain = Clientes.getMthis().getView().findViewById(
							R.id.listView_clientes);*/
					refMain = Clientes.getMthis().getView().findViewById(
							R.id.rv_clientes);
				}

				View refLogin = null;
				if (Login.getMthis() != null) {
					refLogin = Login.getMthis().findViewById(
							R.id.editTextUser);
				}

				View refSplash = null;
				if (Splash.mthis != null) {
					refSplash = Splash.mthis
							.findViewById(R.id.linear_splash);
				}

				if (refMain != null) {
					mthis = null;
					//finish();
					if (getFragmentManager() != null)
						getFragmentManager().popBackStack();
				} else if (refLogin != null) {
					Bundle bundle = getArguments();
					if (bundle != null) {
						if (bundle.containsKey("iniciar_time_sesion")) {
							if (mainActivity.handlerSesion != null) {
								mainActivity.handlerSesion
										.removeCallbacks(mainActivity.myRunnable);
								mainActivity.handlerSesion = null;

							}
						}
					}

					Login.getMthis().finish();
					if (refSplash != null) {
						Splash.mthis.finish();
					}
					mthis = null;
					//finish();
					if (getFragmentManager() != null)
						getFragmentManager().popBackStack();
				} else if (refSplash != null) {
					Bundle bundle = getArguments();
					if (bundle != null) {
						if (bundle.containsKey("iniciar_time_sesion")) {
							if (mainActivity.handlerSesion != null) {
								mainActivity.handlerSesion
										.removeCallbacks(mainActivity.myRunnable);
								mainActivity.handlerSesion = null;

							}
						}
					}
					Splash.mthis.finish();
					mthis = null;
					//finish();
					if (getFragmentManager() != null)
						getFragmentManager().popBackStack();
				}
			});
		/*
		alert.setNegativeButton("Reintentar",
				new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (Clientes.getMthis().isConnected) {

						} else {
							mostrarMensajeError(caso);
						}
					}
				});
		*/
			alert.setCancelable(false);
			alert.create();
		}
		if (this != null) {
			//alert.show();
			Toast.makeText(ApplicationResourcesProvider.getContext(), "Hubo un problema de conexión, por favor vuelve a intentarlo", Toast.LENGTH_LONG).show();
			Util.Log.ih("SERVICIO COMPLETADO CON ERROR");
			mthis = null;
			//finish();


			if (getFragmentManager() != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CIERRE_INFORMATIVO))
				getFragmentManager().popBackStack();



			/*FragmentManager fm = getFragmentManager();
			fm= null;

			if (fm != null && MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CIERRE_INFORMATIVO))
			{
				fm.popBackStack();
				//Toast.makeText(mainActivity, "Entro a if", Toast.LENGTH_SHORT).show();
			}
			else if (fm == null && isAdded())
			{
				fm = ((AppCompatActivity)getActivity()).getSupportFragmentManager();
				//Toast.makeText(mainActivity, "Entro a else es NULLO", Toast.LENGTH_SHORT).show();
				fm.popBackStack();
			}*/


		}
		else{
			Util.Log.ih("NULL");
			mthis = null;
			//finish();
			getFragmentManager().popBackStack();
		}
	}

	/**
	 * deprecated in 3.0
	 * @param caso
	 */
	/*
	public void mostrarMensajeNoSePudoCancelar(final int caso) {
		AlertDialog.Builder alert = new AlertDialog.Builder(mthis);
		alert.setMessage("Hubo un error de comunicación con el servidor, verifique sus ajustes de red o intente más tarde.");
		alert.setPositiveButton("Cancelar",
				new OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

					}
				});
		alert.setNegativeButton("Reintentar",
				new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (Clientes.getMthis().isConnected) {
							try {

								cancelarPago(arrayCierre.getJSONObject(
										posicionCancelacion).getInt("no_cobro"));

							} catch (JSONException e) {
								e.printStackTrace();
							}
						} else {
							mostrarMensajeError(caso);
						}
					}
				});
		alert.create();
		alert.setCancelable(false);
		alert.show();
	}
	*/

	/**
	 * this method gets launched when there's no payments and visits
	 * made yet
	 * shows a dialog with message from server
	 *
	 * @param motivo
	 * 			response from server that contains a message
	 */
	private void mostrarMensajeSinCoincidencias(final String motivo) {
		//if (CierreInformativo.getMthis() != null) {
		try {
			AlertDialog.Builder alert = getAlertDialogBuilder();
			alert.setMessage(motivo);
			alert.setPositiveButton("Aceptar", (dialog, which) -> {
				if(progress!=null)
					progress.dismiss();
				//getFragmentManager().popBackStack();}
			});
			alert.create().show();
		} catch (WindowManager.BadTokenException e){
			e.printStackTrace();

			Toast.makeText(ApplicationResourcesProvider.getContext(), "No se obtuvieron registros", Toast.LENGTH_LONG).show();
			mthis = null;
			//finish();
			getFragmentManager().popBackStack();
		} catch (Exception e){
			e.printStackTrace();

			Toast.makeText(ApplicationResourcesProvider.getContext(), "No se obtuvieron registros", Toast.LENGTH_LONG).show();
			mthis = null;
			//finish();
			if (MainActivity.CURRENT_TAG.equals(MainActivity.TAG_CIERRE_INFORMATIVO))
				getFragmentManager().popBackStack();
		}
		//}
	}

	/*@Override
	public void onBackPressed() {
		if (dontexit) {
			if (ticketWasCancelled){
				Util.Log.ih("posicionCancelacion != 0");
				setResult(99);
			} else {
				Util.Log.ih("posicionCancelacion == 0");
			}
			mthis = null;
			finish();
		}
	}*/

	@Override
	public void onClickCancel(final int position)
	{
		if ( !cancelingTicket )
		{
			try {
				final JSONObject json = arrayCierre.getJSONObject(position);
				TextView etMonto = (TextView) getView().findViewById(R.id.et_monto_cierre);
				double montoacancelar = json.getDouble("monto");
				double monto = Double.parseDouble(etMonto.getText().toString());

				cancelAmount = json.getString("monto");
				cancelCompany = json.getString("empresa");
				cancelCustomerNumber = json.getString("no_cliente");
				cancelDate = json.getString("tiempo");

				state = lvCierreWithCancelledTickets.onSaveInstanceState();

				Util.Log.ih("company = " + cancelCompany);

				float companyAmount = 0;

				if ( DatabaseAssistant.isThereMoreThanOnePeriodActive() )
				{
					companyAmount = DatabaseAssistant.getTotalCompanyWhenMoreThanOnePeriodIsActive( DatabaseAssistant.getSingleCompany(cancelCompany).getName(), mainActivity.getEfectivoPreference().getInt("periodo", 0) );
				} else {
					companyAmount = (float) DatabaseAssistant.getTotalEfectivoAcumuladoPorEmpresa(  DatabaseAssistant.getSingleCompany(cancelCompany).getIdCompany()  ); //(DatabaseAssistant.getSingleCompany(cancelCompany).getName(), 0);
				}

				Util.Log.ih("companyAmount = " + companyAmount);
				companyAmount -= montoacancelar;
				Util.Log.ih("companyAmount after = " + companyAmount);

				//monto -= montoacancelar;

				if (companyAmount >= 0)
				{

					showCancelationReasons(etMonto, json, position);
					/*AlertDialog.Builder alert = getAlertDialogBuilder();
					if (alert != null)
					{
						alert.setMessage("Se cancelará el siguiente ticket\nFolio: " + json.getString("folio") + "\nContrato: " + json.getString("no_cliente") + "\nAportación: $" + json.getDouble("monto"));
						alert.setPositiveButton("Aceptar", (dialog, which) ->
						{
							cancelingTicket = true;

							if (mainActivity.isConnected) {
								try {
									if(!opcionSeleccionada.equals(""))
									{
										int noCobro = json.getInt("no_cobro");
										posicionCancelacion = position;
										cancelarPago(noCobro, opcionSeleccionada);
									}
									else
										Toast.makeText(mainActivity, "Selecciona la opción de cancelación", Toast.LENGTH_LONG).show();


								} catch (JSONException e) {
									e.printStackTrace();
								}
							} else {
								try {
									mainActivity.showAlertDialog("", getResources().getString(R.string.error_message_network), false);
								} catch (Exception e) {
									Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_network, Toast.LENGTH_LONG);
									toast.setGravity(Gravity.CENTER, 0, 0);
									toast.show();
									e.printStackTrace();
									mthis = null;
									//finish();
									getFragmentManager().popBackStack();
								}

								cancelingTicket = false;
							}
						});
						alert.setNegativeButton("Cancelar", (dialog, which) -> cancelingTicket = false);
						alert.create().show();
					}*/



				} else {
					Toast.makeText(
							ApplicationResourcesProvider.getContext(),
							"No se puede cancelar este cobro por que el monto excede el efectivo actual.",
							Toast.LENGTH_LONG).show();
				}

			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	}

	public void showCancelationReasons(View view, JSONObject json, final int position)
	{
		final Button btCancelarBoton;
		Spinner spLista;
		ImageView btSalir;
		TextView info_text, tvFolio, tvContrato, tvMontoAportacion;

		ArrayList<String> esquemas;
		dialogo_cancelacion_de_ticket.setContentView(R.layout.dialogo_motivos_de_cancelacion);
		dialogo_cancelacion_de_ticket.setCancelable(false);

		btCancelarBoton = (Button) dialogo_cancelacion_de_ticket.findViewById(R.id.btCancelarBoton);
		info_text = (TextView) dialogo_cancelacion_de_ticket.findViewById(R.id.info_text);
		tvFolio = (TextView) dialogo_cancelacion_de_ticket.findViewById(R.id.tvFolio);
		tvContrato = (TextView) dialogo_cancelacion_de_ticket.findViewById(R.id.tvContrato);
		tvMontoAportacion = (TextView) dialogo_cancelacion_de_ticket.findViewById(R.id.tvMontoAportacion);
		btSalir = (ImageView) dialogo_cancelacion_de_ticket.findViewById(R.id.btSalir);


		btSalir.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogo_cancelacion_de_ticket.dismiss();
			}
		});

		spLista=(Spinner)dialogo_cancelacion_de_ticket.findViewById(R.id.spLista);

		try
		{
			tvFolio.setText(json.getString("folio"));
			tvContrato.setText(json.getString("no_cliente"));
			String montoString= "$" + json.getDouble("monto");
			tvMontoAportacion.setText(montoString);

			//info_text.setText("Se cancelará el siguiente ticket\nFolio: " + json.getString("folio") + "\nContrato: " + json.getString("no_cliente") + "\nAportación: $" + json.getDouble("monto"));
		}catch (JSONException ex)
		{
			tvFolio.setText("No se encuentra");
			tvContrato.setText("No se encuentra");
			tvMontoAportacion.setText("No se encuentra");
			ex.printStackTrace();
		}


		esquemas = new ArrayList<String>();
		esquemas.add("MOTIVO DE CANCELACIÓN...");
		List<MotivosDeCancelacion> lista = MotivosDeCancelacion.listAll(MotivosDeCancelacion.class);
		if(lista.size()>0) {
			for (int i = 0; i < lista.size(); i++) {
				try {
					esquemas.add(lista.get(i).getMotivo().toUpperCase());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		else {
			Snackbar.make(spLista, "Registros menores a 0", Snackbar.LENGTH_LONG).show();
		}

		ArrayAdapter<CharSequence> adaptador = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, esquemas);
		spLista.setAdapter(adaptador);


		btCancelarBoton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(spLista.getSelectedItemPosition()==0)
				{
					Toast.makeText(mainActivity, "SELECCIONA UNA OPCIÓN", Toast.LENGTH_LONG).show();
				}
				else
				{
					//Hacer accciones
					cancelingTicket = true;

					if (mainActivity.isConnected) {
						try {
							if(!opcionSeleccionada.equals(""))
							{
								int noCobro = json.getInt("no_cobro");
								posicionCancelacion = position;
								cancelarPago(noCobro);
							}
							else
								Toast.makeText(mainActivity, "Selecciona la opción de cancelación", Toast.LENGTH_LONG).show();


						} catch (JSONException e) {
							e.printStackTrace();
						}
					} else {
						try {
							mainActivity.showAlertDialog("", getResources().getString(R.string.error_message_network), false);
						} catch (Exception e) {
							Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_network, Toast.LENGTH_LONG);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
							e.printStackTrace();
							mthis = null;
							//finish();
							getFragmentManager().popBackStack();
						}

						cancelingTicket = false;
					}
					dialogo_cancelacion_de_ticket.dismiss();
				}
			}
		});

		spLista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				try
				{
					//opcionSeleccionada=spLista.getItemAtPosition(position).toString();
					opcionSeleccionada = DatabaseAssistant.getIdWebForCancelationReason(spLista.getItemAtPosition(position).toString());
				}catch (Exception e)
				{
					e.printStackTrace();
					opcionSeleccionada="";
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
		dialogo_cancelacion_de_ticket.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialogo_cancelacion_de_ticket.show();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (mainActivity.handlerSesion != null) {
			mainActivity.handlerSesion.removeCallbacks(mainActivity.myRunnable);
			mainActivity.handlerSesion = null;
			if (Clientes.getMthis() != null) {
				Clientes.getMthis().getActivity().finish();
			}
		}
		wl.release();
	}



	@Override
	public void onResume() {
		super.onResume();
		mthis = this;
		MainActivity.CURRENT_TAG = MainActivity.TAG_CIERRE_INFORMATIVO;
	}

	/**
	 * calls {@link #sortArrayCierre()} and {@link #splitArrayCierreArrangedBy400()}
	 * to after call {@link #executePrintMoreThan400Payments()} wichi is an
	 * android service if payments and visits are more than 400,
	 * else, calls {@link #executePrintCierreInformativo()}
	 */

	private void realizarCierre() {
		//SqlQueryAssistant query = new SqlQueryAssistant(this);
		TextView etMonto = (TextView) getView().findViewById(R.id.et_monto_cierre);
		if (etMonto.getText().length() == 0) {
			etMonto.setText("0.00");
		}

		if (etMonto.getText().length() > 0)
		{
			if( mainActivity.checkBluetoothAdapter() )
			{
				int cont = 0;
				boolean ban = true;
				while (cont < 5 && ban) {
					try {
						if (mac == null)
						{
							mac = DatabaseAssistant.getPrinterMacAddress();
						}
						if (arrayCierre != null && arrayCierre.length() > 400)
						{
							if (mthis != null)
							{
								sortArrayCierre();
								splitArrayCierreArrangedBy400();
								executePrintMoreThan400Payments();
								ban = false;
							}
						} else {
							if (mthis != null)
							{
								//getAmountDepositFromPreviousPeriod();
								executePrintCierreInformativo();
								ban = false;
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						cont++;
					}
					if (cont > 4) {
						Toast.makeText(getContext(), "No se ha encontrado la impresora", Toast.LENGTH_SHORT).show();
					}
				}
			}
		} else {
			try {
				mainActivity.showAlertDialog("",
						getResources().getString(R.string.error_message_fields),
						false);
			} catch (Exception e){
				Toast toast = Toast.makeText(ApplicationResourcesProvider.getContext(), R.string.error_message_fields, Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				e.printStackTrace();
			}
		}

	}

	void getAmountDepositFromPreviousPeriod()
	{
		JSONObject params = new JSONObject();

		try {
			params.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
			params.put("no_cobrador", mainActivity.getEfectivoPreference().getString("no_cobrador", ""));

		} catch (JSONException e) {
			e.printStackTrace();
		}

		NetService service = new NetService(ConstantsPabs.urlGetListaDepositosPorPeriodo, params);
		NetHelper helper = NetHelper.getInstance();

		helper.ServiceExecuter(service, getContext(), new onWorkCompleteListener()
		{
			@Override
			public void onCompletion(String result) {
				try {
					if (new JSONObject(result).has("error")) {
						Log.d("IMPRESION -->", "ERROR");
					}
					JSONArray results = new JSONObject(result).getJSONArray("result");
					System.out.println(results);

					for (int q = 0; q < results.length(); q++)
					{
						JSONObject depositoJSON = results.getJSONObject(q);
						Log.e("DEPOSITO JSON --> " + q, depositoJSON.toString());

						if (depositoJSON.getString("status").equals("1"))
						{
							try {
								deposito =  Double.parseDouble(depositoJSON.getString("monto"));
							} catch (Exception e) {
								deposito = 0;
								e.printStackTrace();
							}
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
					Log.i("IMPRESION -->", " Error onCompletation getAmountDepositFromPreviousPeriod");
				}
			}

			@Override
			public void onError(Exception e) {
				// TODO Auto-generated method stub
				Log.d("IMPRESION -->", "ERROR");
				System.out.println(e.toString());
			}
		});
	}

	/**
	 * sends to server all payments made that are not sync yet
	 *
	 * Web Service
	 * 'http://50.62.56.182/ecobro/controlpagos/registerPagos'
	 * JSONObject param
	 * {
	 *     "no_cobrador": "",
	 *     "pagos": {}
	 *     "periodo":
	 * }
	 */
	private void registerPagos()
	{
		JSONObject json = new JSONObject();

		try {
			Bundle bundle = getArguments();
			if (bundle != null) {
				if (bundle.containsKey("no_cobrador")) {
					noCobrador = mainActivity.getEfectivoPreference().getString(
							"no_cobrador", "");
				}
			}

			if (offlinePayments == null){
				offlinePayments = DatabaseAssistant.getAllPaymentsNotSynced();
			}

			json.put("no_cobrador", noCobrador);
			json.put("pagos", new JSONArray( new Gson().toJson( offlinePayments ) ));
			json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));
			try {
				TelephonyManager manager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
				String deviceid = null;
				if (Nammu.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
						deviceid = manager.getImei();
					else
						deviceid = manager.getDeviceId();
				}
				json.put("imei", deviceid);
			} catch (Exception e) {
				e.printStackTrace();
			}


		} catch (Exception e) {
			e.printStackTrace();
		}
		requestRegisterPagosOffline(json);

       /* NetHelper.getInstance().ServiceExecuter(new NetService(ConstantsPabs.urlregisterPagosOffline, json), getContext(), new onWorkCompleteListener() {

					@Override
					public void onCompletion(String result) {
						try {
							manage_RegisterPagosOffiline(new JSONObject(result));
						} catch (JSONException e) {
							Toast.makeText(
									ApplicationResourcesProvider.getContext(),
									"Problema con la conexión, intenta de nuevo más tarde.",
									Toast.LENGTH_SHORT).show();
							e.printStackTrace();
						}
					}

					@Override
					public void onError(Exception e) {
						Log.e(null, "RegisterPagos");
						mostrarMensajeError(ConstantsPabs.listaPagosCobrador);
					}
				});*/
	}

	private void requestRegisterPagosOffline(JSONObject jsonParams)
	{
		progress = ProgressDialog.show(mainActivity, "Actualizando información de pagos...", "Por favor espera...", true);
		progress.setCancelable(true);
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlregisterPagosOffline, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				try {
					manage_RegisterPagosOffiline(response);
					if(progress!=null)
						progress.dismiss();
				} catch (Exception e) {
					Toast.makeText(ApplicationResourcesProvider.getContext(), "Problema con la conexión, intenta de nuevo más tarde.", Toast.LENGTH_SHORT).show();
					e.printStackTrace();
					if(progress!=null)
						progress.dismiss();
				}
				if(progress!=null)
					progress.dismiss();
				Log.i("Request LOGIN-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(null, "RegisterPagos");
						mostrarMensajeError(ConstantsPabs.listaPagosCobrador);
						if(progress!=null)
							progress.dismiss();
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
	}


	/**
	 * Sends to server all deposits made
	 *
	 * Web Service
	 * 'http://50.62.56.182/ecobro/controldepositos/registerDepositosOffline'
	 *
	 * JSONObject param
	 * {
	 *     "no_cobrador": "";
	 *     "depositos": {},
	 *     "periodo": ,
	 * }
	 */
	public void enviarDepositos() {
		if (DatabaseAssistant.isThereAnyDepositNotSynced()) {

			List<LogRegister.LogDeposito> logDepositos = null;
			JSONObject json = new JSONObject();

			try {
				//String noCobrador = query.MostrarDatosCobrador().getString("no_cobrador");
				String noCobrador = DatabaseAssistant.getCollector().getNumberCollector();
				//JSONArray depositos = query.mostrarTodosLosDepositosDesincronizados().getJSONArray("DEPOSITO");

				List<ModelDeposit> deposits = DatabaseAssistant.getAllDepositsNotSynced();
				if (deposits != null) {

					json.put("no_cobrador", noCobrador);
					json.put("depositos", new JSONArray( new Gson().toJson( deposits ) ));
					json.put("periodo", mainActivity.getEfectivoPreference().getInt("periodo", 0));

					//Crear arraylist de depositos y registrarlos en log si el server responde
					logDepositos = new ArrayList<>();
					for (ModelDeposit d: deposits){
						LogRegister.LogDeposito logDeposito;

						logDeposito = new LogRegister.LogDeposito(
								"" + mainActivity.getEfectivoPreference().getInt("periodo", 0),//Periodo
								d.getAmount(),//Monto
								d.getCompany(), //Empresa
								mainActivity.getEfectivoPreference().getString("no_cobrador", ""),                                                                                    //Cobrador
								d.getNumberBank(),//Banco
								d.getReference(),//Referencia
								true//Ya fue enviado al server
						);
						logDepositos.add(logDeposito);
					}
					Util.Log.ih("depositos json = " + json.toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			Util.Log.ih("depositos a enviar = " + json.toString());

			requestRegisterDepositosOffline(json);

			/*NetService service = new NetService(
					ConstantsPabs.urlRegisterDepositosOffline, json);
			NetHelper.getInstance().ServiceExecuter(service, getContext(), new onWorkCompleteListener() {

				List<LogRegister.LogDeposito> logDepositos;

				@Override
				public void onCompletion(String result) {
					try {
						manageRegisterDepositosOffline(new JSONObject(result), logDepositos);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onError(Exception e) {
					Log.e(null, "Error: enviardepositos");
					mostrarMensajeError(ConstantsPabs.listaPagosCobrador);
				}

				public onWorkCompleteListener setData(List<LogRegister.LogDeposito> logDepositos) {
					this.logDepositos = logDepositos;
					return this;
				}
			}.setData(logDepositos));*/

		} else {
			checkPayments();
		}
	}

	private void requestRegisterDepositosOffline(JSONObject jsonParams)
	{
		progress = ProgressDialog.show(mainActivity, "Cargando depositos", "Por favor espera...", true);
		progress.setCancelable(true);
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlRegisterDepositosOffline, jsonParams, new Response.Listener<JSONObject>() {
			List<LogRegister.LogDeposito> logDepositos;
			@Override
			public void onResponse(JSONObject response)
			{
				try {
					manageRegisterDepositosOffline(response, logDepositos);
					if(progress!=null)
						progress.dismiss();
				} catch (Exception e) {
					e.printStackTrace();
					if(progress!=null)
						progress.dismiss();
				}
				if(progress!=null)
					progress.dismiss();
				Log.i("Request LOGIN-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(null, "RegisterPagos");
						mostrarMensajeError(ConstantsPabs.listaPagosCobrador);
						if(progress!=null)
							progress.dismiss();
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
	}

	/**
	 * Manages response of web service
	 * if failed to send deposits, it will try to send them again,
	 *
	 * @param json
	 * 			JSONObject with response of web service
	 * @param logDepositos
	 * 			List<LogRegister.LogDeposito> instance.
	 * 			Deprecated in 3.0
	 */
	public void manageRegisterDepositosOffline(JSONObject json, List<LogRegister.LogDeposito> logDepositos) {
		if (json.has("result")) {
			Log.e("result", json.toString());
			try {
				JSONObject result = json.getJSONObject("result");
				JSONArray almacenados = result.getJSONArray("almacenados");
				Util.Log.ih("almacenados = " + almacenados.toString());
				if (almacenados.length() > 0) {
					syncDepositsSavedToServer(almacenados);
				}
				JSONArray repetidos = result.getJSONArray("repetidos");
				Util.Log.ih("repetidos = " + repetidos.toString());
				if (repetidos.length() > 0) {
					syncDepositsSavedToServer(repetidos);
				}
				JSONArray noAlmacenados = result.getJSONArray("no_almacenados");
				if (noAlmacenados.length() > 0) {
					Toast.makeText(ApplicationResourcesProvider.getContext(), "Enviando depositos bancarios", Toast.LENGTH_LONG).show();
					enviarDepositos();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		Util.Log.eh("manageRegisterDepositosOffline");
		checkPayments();
	}

	/**
	 * Sets all deposits that were just sent
	 * to server as sync (sync <- 1) so that does not resend deposits made
	 * causing data usage.
	 */
	public void syncDepositsSavedToServer(JSONArray almacenados) {
		try {
			//SqlQueryAssistant assistant = new SqlQueryAssistant(this);
			for (int i = 0; i < almacenados.length(); i++) {
				JSONObject deposito = almacenados.getJSONObject(i);
				//assistant.updateDepositsSync(deposito.getString("fecha"));
				DatabaseAssistant.updateDepositsAsSynced(deposito.getString("fecha"));
			}
			//assistant = null;
			//arrayCobrosOffline = new JSONArray();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Helpful class to manage android service's response
	 * android service which is called when is trying to print more than 400 payments and visits ticket
	 */
	/*
	private class PrintMoreThan400PaymentsListener implements RequestListener<PrintMoreThan400PaymentsInformativoResponse>, RequestProgressListener {

		@Override
		public void onRequestFailure(SpiceException exception) {
			onPrintingFailure(exception);
		}

		@Override
		public void onRequestSuccess(PrintMoreThan400PaymentsInformativoResponse response) {
			onPrintingSuccess(response);
		}

		@Override
		public void onRequestProgressUpdate(RequestProgress progress) {
			onPrintingProgressUpdate(progress);
		}

	}
	*/

	/**
	 * cleans up cache with info of android service
	 */
	/*
	private void clearPrintMoreThan400PaymentsCache() {
		try {
			Future<?> future = getSpiceManager().removeDataFromCache(PrintMoreThan400PaymentsInformativoResponse.class, KEY_CACHE);
			if (future != null) {
				future.get();
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
	*/

	/**
	 * android service
	 * an error happened
	 * show up a dialog with option to retry to print
	 *
	 * @param exception
	 * 			error thrown
	 */
	private void onPrintingFailure(SpiceException exception){

		mainActivity.LogEh(R.string.cierreInformativo_ticket_printed_failed);

		AlertDialog.Builder tryAgainDialog = getAlertDialogBuilder()
				.setMessage(R.string.error_message_printing_try_again)
				.setPositiveButton(R.string.error_message_try_again, (dialog, which) -> {
					restartVariablesToPrint();

					sortArrayCierre();
					splitArrayCierreArrangedBy400();

					executePrintMoreThan400Payments();
				});
		tryAgainDialog.create().show();
	}

	/**
	 * this method gets called when android service for dialog_copies tickets finishes
	 * either with an error or with success
	 *
	 * @param response
	 * 			response with the result of the android service
	 *          what is needed is response.result which is a boolean
	 *          true -> successfull
	 *          false -> error
	 */
	private void onPrintingSuccess(ModelPrintMoreThan400PaymentsCierreResponse response){
		mainActivity.LogIh(R.string.printing_service_ended);

		if( response.isResult() ){

			/*
			if (pd != null && pd.isShowing()) {
				try {
					pd.dismiss();
					pd = null;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			*/
			//dismissMyCustomDialog();

			if( !response.isFinishPrinting() ){

				AlertDialog.Builder tryAgainDialog = getAlertDialogBuilder()
						.setMessage(R.string.success_message_continue_printing)
						.setPositiveButton(R.string.info_message_continue, (dialog, which) -> {
							CierreInformativo.sectionOfArray++;

							splitArrayCierreArrangedBy400();

							executePrintMoreThan400Payments();
						})
						.setCancelable(false);
				tryAgainDialog.create().show();
			}
			else{

				copiaImpresion();
				//finish();
			}
		}
		else{

			/*
			if (pd != null && pd.isShowing()) {
				try {
					pd.dismiss();
					pd = null;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			*/
			dismissMyCustomDialog();

			mainActivity.LogEh(R.string.error_message_printing_try_again);

			AlertDialog.Builder tryAgainDialog = getAlertDialogBuilder()
					.setMessage(R.string.error_message_printing_try_again)
					.setPositiveButton(R.string.error_title_try_again, (dialog, which) -> {
						restartVariablesToPrint();

						sortArrayCierre();
						splitArrayCierreArrangedBy400();

						executePrintMoreThan400Payments();
					});
			tryAgainDialog.create().show();


		}
		//if (pd != null) {
		//	pd.dismiss();
		//}
	}

	/**
	 * restarts variables to print more than 400
	 * payments and visits when failed trying
	 * to print ticket
	 */
	private void restartVariablesToPrint(){
		CierreInformativo.printCounterToSetSize = 0;
		CierreInformativo.printCounter = 0;
		CierreInformativo.organizationCounter = 0;
		CierreInformativo.restartMonto = true;
		CierreInformativo.limitToPrint = 400;
		CierreInformativo.sectionOfArray = 1;
	}

	/**
	 * shows framelayout as progress dialog
	 */
	private void showMyCustomDialog(){

		goBack = false;

		final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
		flLoading.setVisibility(View.VISIBLE);

		//final Animation qwe = AnimationUtils.loadAnimation(this, R.animator.rotate_around_center_point);
		//final ImageView ivLoadingIcon = (ImageView) CierreInformativo.this.findViewById(R.id.ivLoading);
		//ivLoadingIcon.startAnimation(qwe);
	}

	/**
	 * hides framelayout used as progress dialog
	 */
	private void dismissMyCustomDialog(){

		goBack = true;

		final FrameLayout flLoading = (FrameLayout) getView().findViewById(R.id.flLoading);
		flLoading.setVisibility(View.GONE);

		//final ImageView ivLoadingIcon = (ImageView) CierreInformativo.this.findViewById(R.id.ivLoading);
		//ivLoadingIcon.clearAnimation();
	}

	public void setFragmentResult(OnFragmentResult fragmentResult) {
		this.fragmentResult = fragmentResult;
	}

	private void requestGetMotivosDeCancelacion()
	{
		JSONObject json = new JSONObject();

		try {
			json.put("periodo", "1");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlgetMotivosDeCancelacion, json, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				try
				{
					if(response.has("motivos_de_cancelacion_de_pago"))
					{
						JSONArray arregloSolicitudes = response.getJSONArray("motivos_de_cancelacion_de_pago");

						if(arregloSolicitudes.length() > 0)
						{
							MotivosDeCancelacion.deleteAll(MotivosDeCancelacion.class);
							for(int i=0; i<=arregloSolicitudes.length()-1;)
							{
								JSONObject jsonTmp = arregloSolicitudes.getJSONObject(i);
								String motivo = jsonTmp.getString("motivo_de_cancelacion");
								String id_motivo_cancelacion_de_pago = jsonTmp.getString("id_motivo_cancelacion_de_pago");

								Log.d("IMPRESION --> "+ i, "Motivos de cancelacion obtenidas CORRECTAMENTE");
								DatabaseAssistant.insertarMotivoDeCancelacionDePago(motivo, id_motivo_cancelacion_de_pago);
								i++;
							}
						}
					}
				}catch (JSONException ex)
				{
					ex.printStackTrace();
				}
				Log.i("Request LOGIN-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		postRequest.setRetryPolicy(new DefaultRetryPolicy(ExtendedActivity.DURATION_REQUEST_VOLLEY_IN_MILLIS, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
	}





	/*private void requestBalanceEmpresaPorCobradorVolley(JSONObject jsonParams)
	{
		JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ConstantsPabs.urlGetBalanceEmpresaPorCobradorYPeriodo, jsonParams, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				JSONArray jsonDatosPorEmpresa;
				try {
					for(Iterator<String> iter = response.keys(); iter.hasNext(); )
					{
						String key = iter.next();
						jsonDatosPorEmpresa = response.getJSONArray(key);
						SharedPreferences.Editor efectivoEditor = mainActivity.getEfectivoEditor();
						try
						{
							for (int i = 0; i < jsonDatosPorEmpresa.length(); i++)
							{
								JSONObject objetoEmpresa;
								try
								{
									objetoEmpresa = jsonDatosPorEmpresa.getJSONObject(i);
									String empresaRespuesta= objetoEmpresa.getString("empresa");
									float cobrosRespuesta = (float) objetoEmpresa.getDouble("cobros");
									float depositosRespuesta = (float) objetoEmpresa.getDouble("depositos");
									float cobrosCanceladosRespuesta = (float) objetoEmpresa.getDouble("cobros_cancelados");
									float cobrosSumandoCanceladosRespuesta = (float) objetoEmpresa.getDouble("cobros_sumando_cancelados");
									float totalCobrosMenosDepositosRespuesta = (float) objetoEmpresa.getDouble("total_cobros_menos_depositos");
									float cobrosMenosDepositosRespuesta = (float) objetoEmpresa.getDouble("cobros_menos_depositos");

									String nombre_de_la_empresa_registrada_en_base_de_datos = DatabaseAssistant.getSingleCompany(empresaRespuesta).getName();
									if(DatabaseAssistant.hayRegistrosConElPeriodoActual(mainActivity.getEfectivoPreference().getInt("periodo", 0), empresaRespuesta))
									{
										String queryActualizacion="UPDATE MONTOS_TOTALES_DE_EFECTIVO SET cobros ='"+cobrosRespuesta+"', depositos='"+depositosRespuesta
												+"', cancelados='"+cobrosCanceladosRespuesta+"', cobrosMasCancelados='"+cobrosSumandoCanceladosRespuesta+"', totalCobrosMenosDepositos='"+totalCobrosMenosDepositosRespuesta
												+"', cobrosMenosDepositos='"+cobrosMenosDepositosRespuesta+"' WHERE periodo='"+mainActivity.getEfectivoPreference().getInt("periodo", 0)+"' and empresa='"+empresaRespuesta+"'";
										MontosTotalesDeEfectivo.executeQuery(queryActualizacion);
									}
									else
									{
										Log.i("No hay registros", "No insertamos nada aqui");
									}

									efectivoEditor.putFloat("cobros "+nombre_de_la_empresa_registrada_en_base_de_datos, cobrosRespuesta).apply();
									efectivoEditor.putFloat("depositos" +nombre_de_la_empresa_registrada_en_base_de_datos, depositosRespuesta).apply();
									efectivoEditor.putFloat("cancelados " +nombre_de_la_empresa_registrada_en_base_de_datos, cobrosCanceladosRespuesta).apply();
									efectivoEditor.putFloat("cobrosMasCancelados "+nombre_de_la_empresa_registrada_en_base_de_datos, cobrosSumandoCanceladosRespuesta).apply();
									efectivoEditor.putFloat("cobrosMenosCanceladosMenosDepositos "+nombre_de_la_empresa_registrada_en_base_de_datos, totalCobrosMenosDepositosRespuesta).apply();
									efectivoEditor.putFloat("cobrosMenosDepositos "+nombre_de_la_empresa_registrada_en_base_de_datos, cobrosMenosDepositosRespuesta).apply();
								} catch (JSONException e) {
									e.printStackTrace();
									Log.i("Error -->", "requestBalanceEmpresaPorCobradorVolley: "+ e.toString());
								}
							}

						} catch (NullPointerException e) {
							e.printStackTrace();

						}
					}
				} catch (JSONException e) {
					//nothing here...
					Log.i("Error -->", "requestBalanceEmpresaPorCobradorVolley: "+ e.toString());
				}
				((TextView) getView().findViewById(R.id.et_monto_cierre)).setText(mainActivity.formatMoney(DatabaseAssistant.getTotalEfectivoAcumulado()));
				cancelingTicket = false;
				ticketWasCancelled = true;
				//Snackbar.make(mainActivity.mainLayout, "Efectivo sincronizado", Snackbar.LENGTH_SHORT).show();
				//Log.i("Request balance-->", new Gson().toJson(response));
			}
		},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						error.printStackTrace();
						Log.e("balanceEmpresas", "Error al obtener balance por empresas");
					}
				}) {

		};
		Log.d("POST REQUEST-->", postRequest.toString());
		com.jaguarlabs.pabs.util.VolleySingleton.getIntanciaVolley(mainActivity).addToRequestQueue(postRequest);
	}*/
}