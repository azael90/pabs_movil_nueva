package com.jaguarlabs.pabs.MyWallet;

import android.content.Context;

import com.jaguarlabs.pabs.components.PreferencesToShowWallet;
import com.jaguarlabs.pabs.util.BuildConfig;
import com.jaguarlabs.pabs.util.Util;

import java.util.Calendar;

/**
 * Class that manages info about a contract that is about
 * to be inserted into the database.
 * This contract will be used with the wallet synchronization.
 *
 * @see {@link BuildConfig#isWalletSynchronizationEnabled()}
 * @author Jordan Alvarez
 * @version 04/09/15
 */
public class MySyncedWallet {

    /*
    private PreferencesToShowWallet preferencesToShowWallet;
    private Context context;
    private Calendar calendar;
    //private SqlQueryAssistant sqlQueryAssistant;

    private boolean weekly;
    private boolean fortnightly;
    private boolean montly;
    private int nextPayday;
    private int paymentOption;

    public MySyncedWallet(Context context, SqlQueryAssistant sqlQueryAssistant, int paymentOption){
        preferencesToShowWallet = new PreferencesToShowWallet(context);
        calendar = Calendar.getInstance();
        this.sqlQueryAssistant = sqlQueryAssistant;
        this.paymentOption = paymentOption;

        init();
    }

    /**
     * initializes variables
     *//*
    private void init(){
        //paymentOption();
        weekly = false;
        fortnightly = false;
        montly = false;
        nextPayday();
    }


    /**
     * inserts a given contract into database calling
     * {@link SqlQueryAssistant#insertContractIntoMyWallet(MyWalletContract)}.
     * Contract to be used with wallet synchronization
     *//*
    public void insertIntoMyWalletSynchronization(String id_contrato,
                                                  String contractNumber,
                                                  boolean visit,
                                                  boolean payment,
                                                  String nombre,
                                                  String apellidoPaterno,
                                                  String apellidoMaterno,
                                                  String localidad,
                                                  String calleCobro,
                                                  String no_ext_cobro,
                                                  String entre_calles,
                                                  String colonia,
                                                  String latitud,
                                                  String longitud,
                                                  String abono,
                                                  String saldo,
                                                  String monto_pago_actual,
                                                  String serieCliente,
                                                  String forma_pago_actual,
                                                  String id_grupo_base,
                                                  String tipo_bd,
                                                  String estatus,
                                                  String tiempo,
                                                  String saldo_atrasado){
        Util.Log.ih("payday = " + nextPayday);

        MyWalletContract myWalletContract = new MyWalletContract()
                .setId_contrato(id_contrato)
                .setContractNumber(contractNumber)
                .setPayday(nextPayday)
                .setWeekly(weekly)
                .setFortnightly(fortnightly)
                .setMonthly(montly)
                .setVisit(visit)
                .setPayment(payment)
                .setSync(0)
                .setNewPayday(0)
                .setNewPaydayCounter(0)
                .setNombre(nombre)
                .setApellidoPaterno(apellidoPaterno)
                .setApellidoMaterno(apellidoMaterno)
                .setLocalidad(localidad)
                .setCalleCobro(calleCobro)
                .setNo_ext_cobro(no_ext_cobro)
                .setEntre_calles(entre_calles)
                .setColonia(colonia)
                .setLatitud(latitud)
                .setLongitud(longitud)
                .setAbono(abono)
                .setSaldo(saldo)
                .setMonto_pago_actual(monto_pago_actual)
                .setSerieCliente(serieCliente)
                .setForma_pago_actual(forma_pago_actual)
                .setId_grupo_base(id_grupo_base)
                .setTipo_bd(tipo_bd)
                .setEstatus(estatus)
                .setTiempo(tiempo)
                .setSaldo_atrasado(saldo_atrasado);

        sqlQueryAssistant.insertContractIntoMyWallet(myWalletContract);

    }

    /**
     * calculates next payday according to
     * payment option
     *//*
    private void nextPayday(){
        nextPayday = calendar.get(Calendar.DAY_OF_YEAR);
        int daysOfYear = checkLeapYear();
        switch (paymentOption){
            case 1:
                //Semanal
                for (int y = 0; y < 7; y++){
                    if (nextPayday == daysOfYear){
                        nextPayday = 0;
                    }
                    nextPayday++;
                }
                //nextPayday += 7;
                weekly = true;
                break;
            case 2:
                //Quincenal
                for (int y = 0; y < 14; y++){
                    if (nextPayday == daysOfYear){
                        nextPayday = 0;
                    }
                    nextPayday++;
                }
                //nextPayday += 14;
                fortnightly = true;
                break;
            case 3:
                for (int y = 0; y < 30; y++){
                    if (nextPayday == daysOfYear){
                        nextPayday = 0;
                    }
                    nextPayday++;
                }
                //Mensual
                //nextPayday += 30;
                montly = true;
                break;
            default:
                //nothing here...
        }
    }

    /**
     * method that returns number of days in current year
     *
     * @return
     * 		integer type - number of days in current year
     */
    private int checkLeapYear(){
        int year = Calendar.getInstance().get(Calendar.YEAR);
        if (year%4==0){
            if (year%100==0){
                if (year%400==0){
                    return 366;
                }
                else{
                    return 365;
                }
            }
            else{
                return 366;
            }
        }
        else{
            return 365;
        }
    }

}
