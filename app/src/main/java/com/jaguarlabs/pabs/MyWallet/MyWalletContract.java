package com.jaguarlabs.pabs.MyWallet;

import com.jaguarlabs.pabs.util.BuildConfig;

/**
 * MyWalletContract class to manage a single contract info
 *
 * @see {@link BuildConfig#isWalletSynchronizationEnabled()}
 * @author Jordan Alvarez
 * @version 04/09/15
 */
public class MyWalletContract {

    private String id_contrato;
    private String contractNumber;
    private int payday;
    private boolean weekly;
    private boolean fortnightly;
    private boolean monthly;
    private boolean visit;
    private boolean payment;
    private boolean sync;
    private int newPayday;
    private int newPaydayCounter;

    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String localidad;
    private String calleCobro;
    private String no_ext_cobro;
    private String entre_calles;
    private String colonia;
    private String latitud;
    private String longitud;
    private String abono;
    private String saldo;
    private String monto_pago_actual;
    private String serieCliente;
    private String forma_pago_actual;
    private String id_grupo_base;
    private String tipo_bd;
    private String estatus;
    private String tiempo;
    private String saldo_atrasado;


    /**
     * .........................................
     * .          GETTERS AND SETTERS          .
     * .........................................
     */

    public String getId_contrato() {
        return id_contrato;
    }

    public MyWalletContract setId_contrato(String id_contrato) {
        this.id_contrato = id_contrato;
        return this;
    }

    public String getLatitud() {
        return latitud;
    }

    public MyWalletContract setLatitud(String latitud) {
        this.latitud = latitud;
        return this;
    }

    public String getLongitud() {
        return longitud;
    }

    public MyWalletContract setLongitud(String longitud) {
        this.longitud = longitud;
        return this;
    }

    public boolean isMonthly() {
        return monthly;
    }

    public MyWalletContract setMonthly(boolean monthly) {
        this.monthly = monthly;
        return this;
    }

    public MyWalletContract setMonthly(int monthly){
        this.monthly = monthly == 1;
        return this;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public MyWalletContract setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
        return this;
    }

    public int getPayday() {
        return payday;
    }

    public MyWalletContract setPayday(int payday) {
        this.payday = payday;
        return this;
    }

    public boolean isWeekly() {
        return weekly;
    }

    public MyWalletContract setWeekly(boolean weekly) {
        this.weekly = weekly;
        return this;
    }

    public boolean isFortnightly() {
        return fortnightly;
    }

    public MyWalletContract setFortnightly(boolean fortnightly) {
        this.fortnightly = fortnightly;
        return this;
    }

    public boolean isVisit() {
        return visit;
    }

    public MyWalletContract setVisit(boolean visit) {
        this.visit = visit;
        return this;
    }

    public boolean isPayment() {
        return payment;
    }

    public MyWalletContract setPayment(boolean payment) {
        this.payment = payment;
        return this;
    }

    public boolean isSync() {
        return sync;
    }

    public MyWalletContract setSync(boolean sync) {
        this.sync = sync;
        return this;
    }

    public int getNewPayday() {
        return newPayday;
    }

    public MyWalletContract setNewPayday(int newPayday) {
        this.newPayday = newPayday;
        return this;
    }

    public int getNewPaydayCounter() {
        return newPaydayCounter;
    }

    public MyWalletContract setNewPaydayCounter(int newPaydayCounter) {
        this.newPaydayCounter = newPaydayCounter;
        return this;
    }

    public MyWalletContract setWeekly(int weekly){
        this.weekly = weekly == 1;
        return this;
    }

    public MyWalletContract setFortnightly(int fortnightly){
        this.fortnightly = fortnightly == 1;
        return this;
    }

    public MyWalletContract setVisit(int visit){
        this.visit = visit == 1;
        return this;
    }

    public MyWalletContract setPayment(int payment){
        this.payment = payment == 1;
        return this;
    }

    public MyWalletContract setSync(int sync){
        this.sync = sync == 1;
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public MyWalletContract setNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public MyWalletContract setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
        return this;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public MyWalletContract setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
        return this;
    }

    public String getLocalidad() {
        return localidad;
    }

    public MyWalletContract setLocalidad(String localidad) {
        this.localidad = localidad;
        return this;
    }

    public String getCalleCobro() {
        return calleCobro;
    }

    public MyWalletContract setCalleCobro(String calleCobro) {
        this.calleCobro = calleCobro;
        return this;
    }

    public String getNo_ext_cobro() {
        return no_ext_cobro;
    }

    public MyWalletContract setNo_ext_cobro(String no_ext_cobro) {
        this.no_ext_cobro = no_ext_cobro;
        return this;
    }

    public String getEntre_calles() {
        return entre_calles;
    }

    public MyWalletContract setEntre_calles(String entre_calles) {
        this.entre_calles = entre_calles;
        return this;
    }

    public String getColonia() {
        return colonia;
    }

    public MyWalletContract setColonia(String colonia) {
        this.colonia = colonia;
        return this;
    }

    public String getAbono() {
        return abono;
    }

    public MyWalletContract setAbono(String abono) {
        this.abono = abono;
        return this;
    }

    public String getSaldo() {
        return saldo;
    }

    public MyWalletContract setSaldo(String saldo) {
        this.saldo = saldo;
        return this;
    }

    public String getMonto_pago_actual() {
        return monto_pago_actual;
    }

    public MyWalletContract setMonto_pago_actual(String monto_pago_actual) {
        this.monto_pago_actual = monto_pago_actual;
        return this;
    }

    public String getSerieCliente() {
        return serieCliente;
    }

    public MyWalletContract setSerieCliente(String serieCliente) {
        this.serieCliente = serieCliente;
        return this;
    }

    public String getForma_pago_actual() {
        return forma_pago_actual;
    }

    public MyWalletContract setForma_pago_actual(String forma_pago_actual) {
        this.forma_pago_actual = forma_pago_actual;
        return this;
    }

    public String getId_grupo_base() {
        return id_grupo_base;
    }

    public MyWalletContract setId_grupo_base(String id_grupo_base) {
        this.id_grupo_base = id_grupo_base;
        return this;
    }

    public String getTipo_bd() {
        return tipo_bd;
    }

    public MyWalletContract setTipo_bd(String tipo_bd) {
        this.tipo_bd = tipo_bd;
        return this;
    }

    public String getEstatus() {
        return estatus;
    }

    public MyWalletContract setEstatus(String estatus) {
        this.estatus = estatus;
        return this;
    }

    public String getTiempo() {
        return tiempo;
    }

    public MyWalletContract setTiempo(String tiempo) {
        this.tiempo = tiempo;
        return this;
    }

    public String getSaldo_atrasado() {
        return saldo_atrasado;
    }

    public MyWalletContract setSaldo_atrasado(String saldo_atrasado) {
        this.saldo_atrasado = saldo_atrasado;
        return this;
    }
}
