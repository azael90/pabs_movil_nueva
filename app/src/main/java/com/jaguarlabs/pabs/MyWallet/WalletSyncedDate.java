package com.jaguarlabs.pabs.MyWallet;

/**
 * WalletSyncedDate class to manage date to measure
 * when working with wallet synchronization
 *
 * @author Jordan Alvarez
 * @version 04/09/15
 */
public class WalletSyncedDate {

    private int dayNumber;
    private int monthNumber;
    private int yearNumber;
    private int dayOfYear;

    public WalletSyncedDate(int dayNumber, int monthNumber, int yearNumber, int dayOfYear){
        this.dayNumber = dayNumber;
        this.monthNumber = monthNumber;
        this.yearNumber = yearNumber;
        this.dayOfYear = dayOfYear;
    }

    /**
     * .........................................
     * .          GETTERS AND SETTERS          .
     * .........................................
     */

    public int getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(int dayNumber) {
        this.dayNumber = dayNumber;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public int getYearNumber() {
        return yearNumber;
    }

    public void setYearNumber(int yearNumber) {
        this.yearNumber = yearNumber;
    }

    public int getDayOfYear() {
        return dayOfYear;
    }

    public void setDayOfYear(int dayOfYear) {
        this.dayOfYear = dayOfYear;
    }
}
