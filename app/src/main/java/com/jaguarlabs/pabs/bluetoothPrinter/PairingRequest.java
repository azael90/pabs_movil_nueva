package com.jaguarlabs.pabs.bluetoothPrinter;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.jaguarlabs.pabs.util.Util;

/**
 * Created by jordan on 12/09/16.
 */
public class PairingRequest extends BroadcastReceiver {
    public PairingRequest() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
            try {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String pin = "0000";
                Util.Log.ih("Bonded" + device.getName());
                byte[] pinBytes;
                pinBytes = (pin).getBytes("UTF-8");
                device.setPin(pinBytes);
                //setPairing confirmation if neeeded
                device.setPairingConfirmation(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}